import os
import time
import re
SYS_TST_RESP_TERM="Enter selection: "

def getCTNumberFromScriptName() :
    return os.path.basename(__file__).split('_')[0]

def parseBeaconPwrResponse(response) :
    """ find the index of searchKey split on : then comma
    """
    pwrList = [0.0]
    searchKey = 'Loop Measurements'
    index = response.find(searchKey)
    rtnStr = ''
    if index < 0 :
        rtnStr = f"-35.01, Error '{searchKey}' not found {response}"
    else :
        measLst = response[index:].split(':')[1].strip().split(',')
        dSum = 0.0
        cnt = 0
        # return a list that only has the pwr data in it.
        for _, m in enumerate(measLst) :
            try :
                pwr = round(float(m.strip()),2)
            except :
                pass
            else :
                dSum += pwr
                cnt += 1
                pwrList.append(pwr)
        avg = -35.00
        if cnt > 0 :
            avg = round((dSum / cnt), 2)
        pwrList[0] = avg
    return pwrList

def parseBatteryStatResponse(response) :
    """ Collect the returned values into a dictionary
    """
    lineLst = response.split("\n")
    rtnStr = ""
    for line in lineLst :
        if ":" in line :
            key, data = tuple(line.split(':'))
            rtnStr += f"{key.strip()},{data.strip()},"
    return rtnStr.rstrip(',')

def parseSlowFastPowerValues(response) :
    tm = time.time()
    searchKey = 'Measured Power:'
    index = response.find(searchKey)
    cnt = 0
    accum = 0.0
    valuesLst = list()
    while index >= 0 :
        if (time.time() - tm) > 3.0 :
            break
        index += len(searchKey)
        measVal = response[index:].split(',')[0].strip()
        try :
            measFloat = round(float(measVal), 2)
        except :
            measFloat = 0.0
        else :
            accum += measFloat
            cnt += 1
        valuesLst.append(measFloat)
        response = response[index:]
        index = response.find(searchKey)
    cnt = cnt if cnt > 0 else 1
    return round(accum/cnt, 2), accum, cnt, valuesLst

def checkRxResetResponse(response) :
    modeOfOp       = 'Mode of operation'
    modeOfOpOut    = ''
    carrierFreq    = 'Carrier frequency'
    carrierFreqOut = ''
    respList = re.split('\r|\n', response)
    mo = cf = -1
    for i, v in enumerate(respList) :
        if mo < 0 :
            mo = v.find(modeOfOp)
            if mo >= 0 :
                modeOfOpOut = v.split(':')[-1].strip()
        if cf < 0 :
            cf = v.find(carrierFreq)
            if cf >= 0 :
                carrierFreqOut = v.split(':')[-1].strip()
        if mo >= 0 and cf >= 0 :
            break
    return modeOfOpOut, carrierFreqOut

