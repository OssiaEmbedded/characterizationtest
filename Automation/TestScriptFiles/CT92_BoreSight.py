from UtilsAndConsts import *
import time
# run this test with the following frequency list
freq_list = [5725, 5750, 5775, 5800, 5825, 5850]
PassFailCriteria =  "{} > 28.5"

class TestClass() :
    def __init__(self, **InArgs) :
        self.printStr      = InArgs.get("outputStringUpdater")
        self.txObj         = InArgs.get("txSerialObj")
        self.stopScriptEv  = InArgs.get("stopScriptEvent")

    def main(self) :
        # first setup the RX and TX which requires setting the freq so we have our loop
                 
        # register the RX ID with the TX
        response = self.txObj.sendArbCommand(f"reg", SYS_TST_RESP_TERM).strip()
        resp = response.split('\n')[-1]
        self.printStr(f"Register rx == {resp}")

        for freq in freq_list :
            self.printStr(f"Testing at frequency {freq}")
            # check if the stop button was selected.
            if self.stopScriptEv.wait(0.5) :
                self.printStr(f"StopEvent")
                break
            # set the transmit/beacon frequency - system_test has this logic
            response = self.txObj.sendArbCommand(f"freq {freq}", SYS_TST_RESP_TERM)
            self.printStr(f"Set Frequency TX/RX == {response}") # print out 
            response = self.txObj.sendArbCommand('s', SYS_TST_RESP_TERM, 20)
            self.printStr(f"Setup TX/RX == {response}")
            # reset the RX. Using system_test we send the rx reset command
            response = self.txObj.sysTestRXCommand('reset', SYS_TST_RESP_TERM, 15)
            self.printStr("Waiting 12 seconds for RX Reset Bootloader check")
            time.sleep(12)

            # request 3 beacons - system_test runs a loop of beacon,power,measure steps
            response = self.txObj.sendArbCommand(f'l 3', SYS_TST_RESP_TERM, 18)
            if len(response) > 0 and "Error" not in response :
                resp = parseBeaconPwrResponse(response) # returns a list of pwr values. index zero is the average of the rest
                rslt = eval(PassFailCriteria.format(resp[0]))
                self.printStr(f"Frequency {freq} {resp} {'</green/>PASS/>' if rslt else '</red/>FAIL/>'}") # print out 
            else :
                self.printStr(f"Frequency {freq} Error RF Power, {response}") # print out 
        #end for loop 
        response = self.txObj.sendArbCommand(f"unreg", SYS_TST_RESP_TERM).strip()
        resp = response.split('\n')[-1]
        self.printStr(f"Unregister rx == {resp}")
        response = self.txObj.sendArbCommand(f"rest", SYS_TST_RESP_TERM)
        self.printStr(f"Restore tx == {response}")
 

