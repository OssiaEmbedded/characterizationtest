import time
from UtilsAndConsts import *
# run this test with the following frequency list
freq_list = [5725, 5785, 5850]
PassFailCriteria =  "{} > 28.1"

class TestClass() :
    def __init__(self, **InArgs) :
        self.printStr      = InArgs.get("outputStringUpdater")
        self.txObj         = InArgs.get("txSerialObj")
        self.stopScriptEv  = InArgs.get("stopScriptEvent")

    def main(self) :
        # first setup the RX and TX which requires setting the freq so we have our loop
                 
        # register the RX ID with the TX
        response = self.txObj.sendArbCommand(f"reg", SYS_TST_RESP_TERM).strip()
        resp = response.split('\n')[-1]
        self.printStr(f"Register rx == {resp}")
        for freq in freq_list :
            self.printStr(f"Testing at frequency {freq}")
            # check if the stop button was selected.
            if self.stopScriptEv.wait(0.5) :
                self.printStr(f"StopEvent")
                break
            # set the transmit/beacon frequency - system_test has this logic
            response = self.txObj.sendArbCommand(f"freq {freq}", SYS_TST_RESP_TERM)
            self.printStr(f"Set Frequency TX/RX == {response}") # print out 
            # set the TX/RX mode 
            response = self.txObj.sendArbCommand('txrxm 1', SYS_TST_RESP_TERM)
            self.printStr(f"TX/RX mode == {response}")
            # initialize the TX and RX for this mode this has a 10 sec wait in it after a reset is sent to the RX
            response = self.txObj.sendArbCommand('s', SYS_TST_RESP_TERM, 20)
            self.printStr(f"Setup TX/RX == {response}")
            # reset the RX. Using system_test command 
            response = self.txObj.sysTestRXCommand('reset', SYS_TST_RESP_TERM, 20)
            modeOp, carFreq = checkRxResetResponse(response)
            self.printStr(f"</blue/>Reset rx == OpMode is '{modeOp}' Frequency is '{carFreq}'/{freq}/>")
            self.printStr(f"\nRX reset == {response}")
            self.printStr("Waiting 12 seconds for RX Reset Bootloader check")
            time.sleep(12) # wait 10 secs after the reset
            # set the cfgmode in the RX
            #response = self.txObj.sysTestRXCommand('cfgmode 3 1', SYS_TST_RESP_TERM, 10)
            #self.printStr(f"cfgmode == {response}")
            # enable charging on the TX
            # get power responses as RX beacons every two seconds
            response = self.txObj.sendArbCommand('e', SYS_TST_RESP_TERM)
            self.printStr(f"Enable Charging == {response}")

            response = self.txObj.readNumLines(20, 60)
            self.printStr(f"READ NUM LINES\n{response}")
            average, accum, cnt, valLst = parseSlowFastPowerValues(response)
            rslt = valLst.sort()[-1] > 0.0 and cnt > 15
            #rslt = eval(PassFailCriteria.format(average))
            self.printStr(f"Frequency {freq} {average} {cnt} {'</green/>PASS/>' if rslt else '</red/>FAIL/>'}") # print out 
            self.printStr(f"accum {accum} -- valLst {valLst}")

            # disable charging on the TX
            response = self.txObj.sendArbCommand('d', SYS_TST_RESP_TERM, 10)
            self.printStr(f"Disable Charging")
            # set the cfgmode in the RX
            response = self.txObj.sysTestRXCommand('cfgmode 3 0', SYS_TST_RESP_TERM)
            self.printStr(f"cfgmode == {response}")
            response = self.txObj.sysTestRXCommand('opmodeset 2', SYS_TST_RESP_TERM)
            self.printStr(f"opmodeset == {response}")
        #end for loop 
        # set the TX/RX mode 
        response = self.txObj.sendArbCommand('txrxm 0', SYS_TST_RESP_TERM)
        self.printStr(f"TX/RX mode == {response}")
        response = self.txObj.sendArbCommand(f"unreg", SYS_TST_RESP_TERM).strip()
        resp = response.split('\n')[-1]
        self.printStr(f"Unregister rx == {resp}")
        response = self.txObj.sendArbCommand(f"rest", SYS_TST_RESP_TERM)
        self.printStr(f"Restore tx == {response}")
 

