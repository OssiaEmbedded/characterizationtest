import base64
import time
import yaml
from UtilsAndConsts import *

class TestClass():
    def __init__(self, **InArgs):
        self.outputUpdater = InArgs.get("outputStringUpdater", None)
        self.txObj         = InArgs.get("txSerialObj", None)
        self.stopScriptEv  = InArgs.get("stopScriptEvent", None)

    def set_eval_mode(self, mode=True):
        cmd = f"eeprom -y -E {1 if mode else 0}"
        self.txObj.sendArbCommand(cmd, longread=10)
        time.sleep(1)

    def restart_boardrevision(self):
        cmd = "systemctl restart boardRevision"
        self.txObj.sendArbCommand(cmd, longread=10)
        time.sleep(1)

    def restart_apps(self):
        self.outputUpdater("Restarting software applications")
        cmd = "supervisorctl reload"
        self.txObj.sendArbCommand(cmd, longread=15)
        self.wait_for_apps()

    def check_eval_mode(self):
        resp = self.txObj.sendArbCommand("cat /run/platform_rev").splitlines()
        for line in resp:
            if "EVAL_MODE" in line:
                eval_mode = line.split('=')[-1].strip('\n\r" ')
                return eval_mode == '1'

    def wait_for_apps(self):
        cnt = 10
        while not self.txObj.isTxSwRunning() and cnt > 0 :
            self.outputUpdater(f"Waiting for TX Apps to start....")
            time.sleep(5)
            cnt -= 1

    def check_if_app_running(self, appname):
        cmd = f"supervisorctl status {appname} | grep -c RUNNING"
        response = self.txObj.sendArbCommand(cmd, longread=5).splitlines()
        for line in response:
            if len(line) == 1:
                return line == '1'

    def get_config_file(self, file_id):
        if file_id == "HALPROC_CONFIG":
            cmd = "conf halproc"
        elif file_id == "SCHEDULER_CONFIG":
            cmd = "conf scheduler"
        else:
            return
        response = self.txObj.sendArbCommand(cmd, SYS_TST_RESP_TERM).split()[-1]
        if file_id in response:
            try:
                encoded = eval(response.split('=', 1)[-1])
                decoded = base64.b64decode(encoded)
                config = yaml.safe_load(decoded)
                return config
            except Exception as e:
                self.outputUpdater(f"Config exception - {e}")

    def test_eval_mode(self):
        self.outputUpdater("Setting Eval Mode = 1")
        self.set_eval_mode(True)
        self.restart_boardrevision()
        eval_mode = self.check_eval_mode()
        time.sleep(1)
        self.outputUpdater("Checking /run/platform_rev file")
        self.outputUpdater(f"EVAL_MODE={1 if eval_mode else 0} {'</green/>PASS/>' if eval_mode == True else '</red/>FAIL/>'}")
        self.restart_apps()
        presence_running = self.check_if_app_running("sense:presence")
        aoa_running = self.check_if_app_running("sense:py_angleofarrival")
        self.outputUpdater(f"presence_running = {presence_running} {'</green/>PASS/>' if not presence_running else '</red/>FAIL/>'}")
        self.outputUpdater(f"aoa_running = {aoa_running} {'</green/>PASS/>' if not aoa_running else '</red/>FAIL/>'}")
        self.txObj.startSystemTest()
        config = self.get_config_file("HALPROC_CONFIG")
        if not config:
            self.outputUpdater("</red/>Error! No config file received!/>")
            return
        freq = config.get('rx_freq_mhz')
        use_dac = config.get('use_dac_buffer_file')
        dac_file = config.get('dac_buffer_file_name')
        expected_file = '/opt/ossia/etc/dac/fmcw_04_0_sawtooth'
        self.outputUpdater(f"rx_freq_mhz = {freq} {'</green/>PASS/>' if freq == 5850 else '</red/>FAIL/>'}")
        self.outputUpdater(f"use_dac_buffer_file = {use_dac} {'</green/>PASS/>' if not use_dac else '</red/>FAIL/>'}")
        config = self.get_config_file("SCHEDULER_CONFIG")
        tx_mode = config.get('tx_mode')
        self.outputUpdater(f"tx_mode = {tx_mode} {'</green/>PASS/>' if tx_mode == 2 else '</red/>FAIL/>'}")
        self.txObj.stopSystemTest()

    def test_non_eval_mode(self):
        self.outputUpdater("Setting Eval Mode = 0")
        self.set_eval_mode(False)
        self.restart_boardrevision()
        eval_mode = self.check_eval_mode()
        time.sleep(1)
        self.outputUpdater("Checking /run/platform_rev file")
        self.outputUpdater(f"EVAL_MODE={1 if eval_mode else 0} {'</green/>PASS/>' if eval_mode == False else '</red/>FAIL/>'}")
        self.restart_apps()
        presence_running = self.check_if_app_running("sense:presence")
        aoa_running = self.check_if_app_running("sense:py_angleofarrival")
        self.outputUpdater(f"presence_running = {presence_running} {'</green/>PASS/>' if presence_running else '</red/>FAIL/>'}")
        self.outputUpdater(f"aoa_running = {aoa_running} {'</green/>PASS/>' if aoa_running else '</red/>FAIL/>'}")
        self.txObj.startSystemTest()
        config = self.get_config_file("HALPROC_CONFIG")
        if not config:
            self.outputUpdater("</red/>Error! No config file received!/>")
            return
        self.outputUpdater(f"Eval Mode Detected: {eval_mode} {'</green/>PASS/>' if eval_mode == False else '</red/>FAIL/>'}")
        freq = config.get('rx_freq_mhz')
        use_dac = config.get('use_dac_buffer_file')
        dac_file = config.get('dac_buffer_file_name')
        expected_file = '/opt/ossia/etc/dac/fmcw_04_0_sawtooth'
        self.outputUpdater(f"rx_freq_mhz = {freq} {'</green/>PASS/>' if freq == 5775 else '</red/>FAIL/>'}")
        self.outputUpdater(f"use_dac_buffer_file = {use_dac} {'</green/>PASS/>' if use_dac else '</red/>FAIL/>'}")
        self.outputUpdater(f"dac_buffer_file_name = {dac_file} {'</green/>PASS/>' if dac_file == expected_file else '</red/>FAIL/>'}")
        config = self.get_config_file("SCHEDULER_CONFIG")
        tx_mode = config.get('tx_mode')
        self.outputUpdater(f"tx_mode = {tx_mode} {'</green/>PASS/>' if tx_mode == 4 else '</red/>FAIL/>'}")
        self.txObj.stopSystemTest()


    def main(self):
        self.txObj.stopSystemTest()
        self.txObj.lsblkCommand()
        self.txObj.mountCommand()
        
        original_mode = self.check_eval_mode()
        time.sleep(1)

        self.test_non_eval_mode()
        self.test_eval_mode()

        self.outputUpdater("Restoring original mode")
        self.set_eval_mode(original_mode)
        self.restart_boardrevision()
        self.restart_apps()
        


