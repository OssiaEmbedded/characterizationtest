from UtilsAndConsts import *

class TestClass() :  # REQUIRED
    def __init__(self, **InArgs) :  # REQUIRED
        self.outputUpdater = InArgs.get("outputStringUpdater", None)  # REQUIRED
        self.txObj         = InArgs.get("txSerialObj", None)  # REQUIRED
        self.stopScriptEv  = InArgs.get("stopScriptEvent", None)

    def main(self) :  # REQUIRED
        cnt = 10 # sample
        # Check your necessary objects here and adjust your test as required. 
        while cnt > 0 : # sample
            self.outputUpdater(f"cnt={cnt}") # sample
            if self.stopScriptEv.wait(1) : # sample
                self.outputUpdater(f"StopEvent") # sample
                break # sample
            cnt -= 1 # sample
        self.outputUpdater(f"Cnt expired") # sample

