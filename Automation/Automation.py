#! /usr/bin/env python3

r"""
THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2024 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
"""


import glob
import os
import platform
import serial
import sys
from threading import Thread, Event
from time import time, sleep
import PySimpleGUI as sg
from serial.tools.list_ports import comports

windoz = platform.system() == "Windows"

TX_hostname_possibilities = ['mars-tx', 't4-a', 't4-b', 't4-c', 't5-a']
##### BEGINNING OF HELPER METHODS #########
# check for serial ports
# a tx port and an actuator port

def uniqueFileName(fnamein, lastFm=False) :
    """  create a unique file name using the base name of the fnamein argument. 
        returns a filename string
    """
    fnameout = fnamein
    fn, ext = os.path.splitext(fnameout)
    n=1
    lastFname = None
    while  os.path.exists(fnameout) :
        lastFname = fnameout
        fnameout = "{}-{}{}".format(fn, n, ext)
        n+=1
    if lastFm : # the last non unique file name for reading
        fnameout = lastFname
    return fnameout

def openFileName(guiObj, filenamein, howtoOpen='w') :
    """ Uses uniqueFileName then opens that file according to the howtoOpen argument
        returns the File Descriptor, opened file name string
    """
    fd = None
    lastFm = howtoOpen != 'w'
    try :
        # make sure we have a unique file name
        fnameout = uniqueFileName(filenamein, lastFm)
        if fnameout and howtoOpen != 'a' : # only None if reading and no file exists
            fd = open(fnameout, howtoOpen)
        elif howtoOpen == 'a' : # make sure we can append to a non existent file
            fd = open(filenamein, howtoOpen)
    except Exception as ee :
        tmpstr = "Exception when opening out file {} -- {}".format(filenamein, repr(ee))
        guiObj.outputStringUpdater(tmpstr, outputobj=guiObj.window["TxTalkOut"], clear_first=True)
        fd = None
    return fd, fnameout

def DECODEBYTES(bytestr) :
    """ decodes a bytestr testing and correcting bytes that are out of range and not in order
    """
    if isinstance(bytestr, bytes) :
        gg = b''
        for dd in bytestr :
           gg += (dd&0x7f).to_bytes(1, sys.byteorder)
        return gg.decode('utf-8')
    else :
        return bytestr

def checkTxHostname(resp, typ=False) :
    """  checks a TX hostname against a standard list of hostname strings
         returns True or False if in or not in the list
    """
    resp = DECODEBYTES(resp)
    rtnval = False
    for tx in TX_hostname_possibilities :
        if not typ :
            print(f"{tx} in {resp} {tx in resp}")
            if tx in resp :
                rtnval = True
                break
        else :
            if resp.startswith(tx) :
                rtnval = True
                break
    return rtnval

def getSerialPorts(force=False) :
    """ find the open port and check to see if any match the
        below criteria.
        A TX port should return a value that's in TX_hostname_possibilities
    """
    serial_ports = comports()
    tx_serial = None
    for i, p in enumerate(serial_ports) :
        if p.manufacturer is None or p.manufacturer != "FTDI" :
            continue
        thePort = f"/dev/{p.name}"
        if windoz :
            thePort = f"{p.name}"
        s = Transmitter(inport=thePort, expectVal=' ', timeoutin=0.5)
        s.serial.write(b"\r")
        s.serial.write(b"\r")
        resp = s.serial.readall()
        s.serial.close()
        del s
        try :
            resp = DECODEBYTES(resp)
        except :
            print(f"{thePort} Not responding as expected.")
        if checkTxHostname(resp, typ=False) :
            tx_serial = thePort 
            break
    return tx_serial

##### END OF HELPER METHODS #########

##### BEGINNING OF TRANSMITTER SERIAL  CLASS #########
class Transmitter() :

    def __init__(self, inport, expectVal='$ ', timeoutin=1.0) :
        """ Init the class and open the serial port
        """
        self.serial         = None
        self.expectVal      = expectVal.encode('utf-8')
        self.isMounted      = False
        self.deviceToMount  = None
        self.mntPoint       = '/mnt/'
        self.expectValFound = False
        self._txIpAddresses = {}
        self.txPromptStr = ''
        try :
            if inport is None :
                raise serial.serialutil.PortNotOpenError
            self.serial = serial.Serial(port=inport, baudrate=115200, timeout=timeoutin)
        except Exception as ee :
            print(f"Exception in serial connection to {inport} --- {ee}")
            self.serial = None
        else :
            if not self.serial.is_open :
                print(f"Could not open device {inport}")

    def __del__(self) :
        """ close the serial port and delete the class
        """
        if self.serial and self.serial.is_open :
            self.serial.close()

    def setTxPrompt(self) :
        """ Set the class variable to the prompt. The prompt should be of the form
            root@<hostname>:
        """
        self.serial.write(b"\r")
        resp = self.serial.readall().decode().strip()
        p = resp.split()[-1]
        c=p.find(':') # need to get rid of the current directory name
        self.txPromptStr = p[:c]
        return self.txPromptStr

    def sendReboot(self) :
        """ Send a reboot through the serial console and return
            This assumes Linux command line.
        """
        self.serial.readall()
        self.serial.write(b"\r\r reboot \r\r")

    def sendExit(self, num=2) :
        """ send the exit command to the serial console. Assumes Linux command line.
            The default number is 2 because of the ossia login and the sudo root login
            returns nothing
        """
        self.serial.readall()
        while num > 0 :
            self.serial.write(b"exit\r\r")
            v = self.serial.readall()
            num -= 1

    def sendShutdown(self) :
        """ Send a shutdown -h now through the serial console and return.
            This assumes Linux command line.
        """
        self.serial.readall()
        self.serial.write(b"\rshutdown -h now\r")

    def checkForLogin(self) :
        """ Get a response from the interface to check
            for four different stats
                login prompt
                Password prompt
                ossia logged in prompt
                root logged in prompt
                system test running prompt
            quits out of all states to the login prompt and returns True for that condition else
            False.
        """
        rtnval = False
        self.serial.reset_output_buffer()
        self.serial.reset_input_buffer()
        self.serial.write(b'\r')
        for i in range(7) :
            if not self.serial.in_waiting :
                sleep(1)
            v = self.serial.read(self.serial.in_waiting)
            if b'login:' in v :
                rtnval = True
                break
            if b'Password:' in v :
                for _ in range(3) :
                    self.serial.write(b'\r') # one for the password
                    sleep(3) # wait three secs for password search
                    x=self.serial.readall()
                    self.serial.write(b'\r') # one for the login:
                    sleep(0.5)
                    x=self.serial.readall()
                rtnval = True
                break
            elif SYSTST_RESP_PROMPT in v :
                self.stopSystemTest()
                self.umountCommand()
                self.sendExit()
                self.serial.readall()
                self.serial.write(b'\r')
                rtnval = True
                break
            elif b"root" in v :
                self.umountCommand()
                self.sendExit()
                self.serial.readall()
                self.serial.write(b'\r')
            elif b'$' in v :
                self.sendExit(1)
                self.serial.readall()
                self.serial.write(b'\r')
            else :
                pass
        return rtnval

    def moveToSudoRoot(self, password, expectVal='# ') :
        """ Should only be use when logged in as ossia or some non root
            user. Uses sudo to move the login to user root.
        """
        self.serial.write("sudo -i\r".encode('utf-8'))
        self.serial.write(password.encode('utf-8')+'\r'.encode('utf-8'))
        self.expectVal = expectVal.encode('utf-8')
        sleep(2.5)
        self.serial.write("\r".encode('utf-8'))
        r=self.serial.read_until(self.expectVal)
        self.serial.write("export TERM=\r".encode('utf-8'))
        r=self.serial.read_until(self.expectVal)
        #print(f"SUDO={r.decode('utf-8')}")

    def sendUsernamePassword(self, username, password=None) :
        """ login in as username with password
            returns the resulting command line prompt.
        """
        self.serial.write(username.encode('utf-8')+"\r".encode('utf-8'))
        sleep(0.5)
        if password and len(password) > 0 :
            self.serial.write(password.encode('utf-8')+"\r".encode('utf-8'))
            sleep(2.5)
            self.serial.write("\r".encode('utf-8'))
        r=self.serial.read_until(self.expectVal)
        #print(f"UNPW={r}")
        try :
            #r = r.decode().strip()
            r = DECODEBYTES(r).strip()
        except Exception as ee :
            print(f"sendUsernamePassword -- {repr(r)} {ee}\nexpectval = {self.expectVal}\n") 
        return r # .decode().strip()

    def readNumLines(self, Number=1, maxTime=10, sendoutput=None) :
        """ Read Number of line unless maxTime finishes first.
            If sendoutput is not None then each read response line is sent to the function
            The function must accept a string as its single argument.
        """
        response = ''
        tm = time()
        _Number = Number + 1 # to miss the leading prompt
        while _Number > 0 :
            resp = self.serial.readline()
            if resp :
                resp = resp.decode()
                if sendoutput :
                    sendoutput(f"{resp.strip()}")
                    #print(f"{_Number} -- ''{resp.strip()}'' ##{self.txPromptStr}##")
                    if _Number < Number and self.txPromptStr in resp :
                        break
                else :
                    response += resp
                _Number -= 1
            if (time() - tm) > maxTime :
                response += "MaxTime Exceeded"
                break
        return response

    def writeReadUntil(self, cmd, expVal=None, longread=None) :
        """ takes a string command and sends it to the console
            it reads the response looking for the "expVal" or if None the standard value set at init.
            longread in Seconds is used for return values that take a long time to be returned.
            returns a string not bytes
        """
        ts = 0
        expectVal = self.expectVal
        if expVal :
            expectVal = expVal
            if isinstance(expectVal, str) :
                expectVal = expectVal.encode()
        if longread is not None :
            ts = self.serial.timeout
            self.serial.timeout = longread
            self.serial.write("\r".encode('utf-8'))
            sleep(0.1)
        self.serial.read(self.serial.in_waiting)
        cmd += '\r'
        self.serial.write(cmd.encode('utf-8'))
        data = self.serial.readline()
        data += self.serial.read_until(expectVal)
        data =  DECODEBYTES(data)
        self.expectValFound = expectVal.decode('utf-8') in data
        #print(f"2  {expectVal} -- {cmd.strip()} == {data} {self.expectValFound}\n")
        if longread is not None :
            self.serial.timeout = ts
            self.serial.write("\r".encode('utf-8'))
            sleep(0.1)
            self.serial.read(self.serial.in_waiting)
        return data.strip(expectVal.decode('utf-8')).strip()

    def lsblkCommand(self) :
        """ finds the device name for the first partition of the sd[a,b,c,d, etc] device
            by eliminating those that start with mccb or disk
            Sets the isMounted Flag if the device is already mounted.
            data is the response from the "lsblk -r" command
            returns True
        """

        # data is a CR delimited set of lines so split it on \n
          # NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
          # sda 8:0 1 1004.5M 0 disk 
          # sda1 8:1 1 1004.5M 0 part 
          # mmcblk1 179:0 0 3.6G 0 disk 
          # mmcblk1p1 179:1 0 3.6G 0 part /
          # mmcblk1boot0 179:8 0 2M 1 disk 
          # mmcblk1boot1 179:16 0 2M 1 disk 

        # send command
        cmd = "lsblk -r\r"
        data = self.writeReadUntil(cmd)

        # process return
        dataLst = data.split('\n')
        for dev in dataLst :
            if "disk" in dev or "mmc" in dev or 'part' not in dev :
                continue
            d = dev.split(' ')
            self.deviceToMount = f"/dev/{d[0].strip()}"
            if "/" in d[-1] :
                self.isMounted = True
                self.mntPoint = d[-1].strip()
            break
        return True

    def isTxSwRunning(self) :
        """ send commands to determine if the Application software is up and running
            supervisorctl status | grep -c RUNNING > 7
        """
        num = 0
        rtnval = False
        cmd = "supervisorctl status | grep -c RUNNING"
        datalst = self.writeReadUntil(cmd, longread=2.5).split() #, longread=2.5)
        for data in datalst :
            try :
                num = int(data.strip())
            except :
                pass
            else :
                if num > 7 :
                    rtnval = True
                    break
        return rtnval

    def disconnectAndClose(self) :
        """ disconnects and closes the serial console connection
        """
        if self.serial :
            self.checkForLogin()
            self.serial.close()
        self.serial = None

    def waitForUpdateFinish(self, hostname) :
        """ Look at the serial console for a command line prompt to determine
            if the update has finished running. This can take quite awhile. This method will spin for up to 
            2 hours looking for the prompt.
        """
        cnt = 3600
        lenHN = len(hostname)
        while cnt > 0 :
            resp = self.serial.readall().decode('utf-8')
            resplst = list(resp)
            #print(f"RESP=='{resp}'\n{resplst}")
            locHN = resp.find(hostname)
            if cnt == 3600 :
                if locHN < (len(resp) / 2) :
                    locHN = resp.find(hostname, lenHN+locHN)
            locEV = resp.find('# ', locHN+lenHN)
            if locEV > -1 and locHN > -1 :
                break
            sleep(2)
            cnt -= 1
        return cnt > 0

    def waitForBoot(self) :
        """ wait for a specific substring from the console output.
            When found return True if cnt has not expired else False
            Max time to execute is 120 seconds.
        """
        cnt = 12
        breakout = 0
        while cnt > 0 :
            resp = self.serial.readall()
            #print(f"RESPONSE {cnt} {resp.decode('utf-8')}")
            if b"Arago Project" in resp : 
                breakout += 1
            if b"Diagnostics Completed" in resp :
                breakout += 1
            if breakout >= 2 :
                break
            sleep(10)
            cnt -= 1
        return True if cnt > 0 else False

    def startSystemTest(self) :
        """ moves to the mounted USB drive and starts executing the
            system_test.py app. -i -> interactive -n -> no command menu printed
            Assumes Linux command line.
        """
        self.sendArbCommand('cd /mnt')
        self.sendArbCommand("python3 system_test.py -i -n", SYSTST_RESP_PROMPT, 15)

    def stopSystemTest(self) :
        """ stops the system test app with the 'x' command
            moves back to root's home directory
            Tests if system_test is running.
        """
        resp = self.sendArbCommand('', SYSTST_RESP_PROMPT_STR, 3)
        if self.expectValFound :
            self.sendArbCommand('x')
            self.sendArbCommand('cd')

    def umountCommand(self) :
        """ umount the /mnt mount point and unsets the isMounted flag
            returns True
        """
        if not self.mntPoint :
            return False
        # send command to un-mount mounted device
        cmd = f"umount {self.mntPoint}\r"
        data = self.writeReadUntil(cmd)
        dataLst = data.split('\n')
        for dev in dataLst[1:] :
            if not dev.startswith('umount') :
                self.isMounted = False
            elif dev.startswith('umount') and "not mounted" in dev :
                #umount: /mnt: not mounted.
                self.isMounted = False
        return True

    def mountCommand(self) :
        """ sends the mount command to the console and sets the isMounted flag
            returns True
        """
        cmd = f"mount {self.deviceToMount} {self.mntPoint}\r"
        data = self.writeReadUntil(cmd)
        dataLst = data.split('\n')
        for dev in dataLst[1:] :
            if not dev.startswith('mount') :
                self.isMounted = True
            elif dev.startswith('mount') and "mounted" in dev :
                #mount: /mnt: /dev/sda1 already mounted on /mnt.
                self.isMounted = True
            elif dev.startswith('mount') and "not exist" in dev :
               # mount: /mnt: special device /dev/sda2 does not exist.
                self.deviceToMount = None
        return True
    
    def getIpAddresses(self, ifname=None) :
        """ sends an ifconfig to the console and parses out
            the network interfaces that have IP addresses assigned to them.
            if ifname is set returns the ipaddres of the requested interface
            if ifname is None it returns a dictionary of all collected interfaces.
        """
        if not self._txIpAddresses :
            ifconfiglist = self.sendArbCommand("ifconfig").split('\n')
            found = False
            name = ''
            for line in ifconfiglist :
                if 'Link' in line :
                    found = True
                    name = line.split()[0]
                elif 'inet ' in line and found :
                    found = False
                    ipadr = line.split()[1] # split on white space. Empty strings removed
                    if not '127' in ipadr :
                        self._txIpAddresses.update({name : ipadr.split(':')[1]})
                    name = ''
        if ifname :
            return self._txIpAddresses.get(ifname, None)
        else :
            return self._txIpAddresses

    def getVersions(self) :
        """ sends console commands to collect the osVersion, the swVersion, and the buildId
            returns all three.
            if not found a variable is set to Not Found
        """
        osVersion = "Not Found"
        swVersion = "Not Found"
        buildId   = "Not Found"
        osVersionlst=self.sendArbCommand("cat /etc/os-release | grep BUILD_VERSION").split('\n')
        #print(f"osVersionlst=={osVersionlst}")
        for osv in osVersionlst :
            if "BUILD_VERSION=" in osv :
                osVersion = osv.split('=')[-1].strip('\n\r" ')
                break
        buildId=self.sendArbCommand("cat /etc/os-release | grep BUILD_ID")
        buildIdlst=buildId.split('\r\n')
        for osv in buildIdlst :
            if "BUILD_ID=" in osv :
                buildId = osv.split('=')[-1].strip('\n\r" ')
                break
        swVersionlst=self.sendArbCommand("cat /opt/ossia/include/marstx_version.h | grep 'MARSTX_VERSION '").split('\n')
        for swv in swVersionlst :
            if "MARSTX_VERSION" in swv and '=' in swv :
                swVersion = swv.split('=')[-1].strip('\r\n ;"')
                break
        self.sendArbCommand("myip='ifconfig | grep '\''\<inet\>'\'' | awk '\''{print $2}'\'")
        return osVersion, swVersion, buildId

    def getHostname(self) :
        """ read the hostname file and parse the hostname
            returns the parsed hostname if found in the standard list of hostname types
            else it returns the raw read value.
        """
        cmd = "cat /etc/hostname\r"
        data = self.writeReadUntil(cmd)
        dlst = data.split('\n')
        brknow = False
        for v in dlst : # data loop
            # hostname possibility loop
            if checkTxHostname(v, typ=True) :
                data = v.strip()
                break
        return data

    def sendArbCommand(self, cmdstr, expVal=None, longread=None) :
        """ takes the command string, sends it to the serial console port.
            If system_test is running the string needs to meet the system_test's 
            list of commands. Otherwise a Linux command line receives the command.
            The command is executed, and the response is returned as a string.
        """
        cmd = f"{cmdstr}"
        data = self.writeReadUntil(cmd, expVal, longread)
        return data

    def sysTestRXCommand(self, cmdstr, expVal=None, longread=None) :
        """ takes a command string formats it to use the System Test "t" command,
            then uses writeReadUntil to send it and read the response
            returning the results.
        """
        cmd = f"t {cmdstr}"
        data = self.writeReadUntil(cmd, expVal, longread)
        return data
##### END OF TRANSMITTER CLASS #########

BTNSIZE               = 12
CONNECTBTN            = "Connect"
UNCONNECTBTN          = "Disconnect"
TAB_AUTOMATE_TEXT     = "Automate"

TXTALKNAME             = "Display"
TX_TALK_PROMPT         = "Enter command:"
SYSTST_RESP_PROMPT     = b"Enter selection:"
SYSTST_RESP_PROMPT_STR = "Enter selection:"
SELECTTESTFILE         = 'Test Script Select'
STARTTESTBTN           = 'Start Test'
STOPTESTBTN            = 'Stop Test'

BUTTON_COLOR          = ('black',   '#EEEEEE') # black on gray
NEXT_BUTTON_COLOR     = ('green',   '#EEEEEE') # green on gray
DISABLED_BUTTON_COLOR = ('#AAAAAA', '#EEEEEE') # gray on gray
FRAME_TITLE_COLOR     = '#808F00' # darkish green

FRAME_TITLE_LOCATION_DC  = sg.TITLE_LOCATION_TOP_LEFT
FRAME_TITLE_LOCATION_CB  = sg.TITLE_LOCATION_TOP_LEFT

sg.theme('Default1')   # Add a touch of color
CommonButtonDef = {
                   'button_color'          : BUTTON_COLOR,
                   'mouseover_colors'      : BUTTON_COLOR,
                   'highlight_colors'      : BUTTON_COLOR,
                   'disabled_button_color' : DISABLED_BUTTON_COLOR,
                   'use_ttk_buttons'       : True,
                   's'                     : BTNSIZE, # use the alias for size so individual sizes can be changed by setting size
                   'enable_events'         : True,
                   }
CommonMultiLineDef = {
                      'write_only'   : True,
                      'no_scrollbar' : False,
                      'auto_refresh' : True,
                      'autoscroll'   : True,
                      'horizontal_scroll' : False,
                     }

CommonInputDef = {
                   'readonly'                 : False,
                   'text_color'               : "black",
                   'use_readonly_for_disable' : False,
                   'do_not_clear'             : True,
                 }
TAB_COLOR_BG_SELECT    = '#EEEEEE' # keep the backgrounds the same
TAB_COLOR_TITLE_SELECT = '#000000' # only change the (Words, foreground, title)
TAB_COLOR_TITLE_UN     = '#777777'
TAB_COLOR_BG_UN        = '#cccccc'
TAB_COLOR_BG           = '#EEEEEE'
theFont = ('Courier', 18)
RX_TALK_START          = ":"
CKBOX_TEXT_ON  = 'green'
CKBOX_TEXT_OFF = 'red'

CommonTabGrpDef = {
                  'selected_background_color' : TAB_COLOR_BG_SELECT,    # sets the background color of the selected tab
                  'selected_title_color'      : TAB_COLOR_TITLE_SELECT, # sets the Words color of the selected tab
                  'title_color'               : TAB_COLOR_TITLE_UN,     # sets the Words color of an un-selected tab
                  'tab_background_color'      : TAB_COLOR_BG_UN,        # sets the background color of an un-selected tab
                  'background_color'          : TAB_COLOR_BG,           # sets the background color that's behind all tabs
                  'font'                      : theFont,
                  'tab_location'              : 'topleft',
               }
                       
connection_devices_btns = [
                            [
                            sg.Text("Connect to TX Serial Console", key='SetIpAddr',   pad=(0,10), size=(30,1), justification='left'),
                            sg.Button(CONNECTBTN, key='ConnectBtn', disabled=False, pad=(5,0), **CommonButtonDef)
                            ],
                            [
                            sg.Button("Install URL", key='InstallURLBtn', disabled=True, pad=(5,0), **CommonButtonDef),
                            sg.Input("", key="installurl", pad=(5,0), disabled=True, size=(91,1), **CommonInputDef)
                            ],
                          ]
feature_checkboxes_cks = [[ #sg.Text("Required Features", key='sstsr', pad=(0,10), size=(18,1), justification='left'),
                        sg.Checkbox("Apps Running",      default=True, key="AR_Checked", pad=((15,0),(0,0)), enable_events=True, text_color=CKBOX_TEXT_ON),
                        sg.Checkbox("Mount Drive",       default=True, key="MD_Checked", pad=((15,0),(0,0)), enable_events=True, text_color=CKBOX_TEXT_ON),
                        sg.Checkbox("Start System Test", default=True, key="ST_Checked", pad=((15,0),(0,0)), enable_events=True, text_color=CKBOX_TEXT_ON),
                          ]]
feature_checkboxes = [
                     sg.Frame("Required Features", [[sg.Column(feature_checkboxes_cks,  element_justification='center',key='CB_col') ]],element_justification='center', key='RF_Frm',vertical_alignment='top', relief=sg.RELIEF_FLAT, title_color=FRAME_TITLE_COLOR, title_location=FRAME_TITLE_LOCATION_CB),
                     ]
layout_inits    = [
                     sg.Frame("Device Connect",    [[sg.Column(connection_devices_btns, element_justification='left',key='DC_col')]], key='DC_Frm',vertical_alignment='top', relief=sg.RELIEF_FLAT, title_color=FRAME_TITLE_COLOR, title_location=FRAME_TITLE_LOCATION_DC),
                  ]

exit_Btn    = [sg.Button('Exit', key='Exit', size=10, **CommonButtonDef)]
layout_exit = [sg.Frame("", [exit_Btn], title_location=sg.TITLE_LOCATION_TOP, expand_x=True, relief=sg.RELIEF_FLAT, vertical_alignment='top', element_justification='right',key='exit_frm')]

layout_testscript = [
                [sg.Frame(SELECTTESTFILE, 
                          [
                            [
                                sg.Listbox(values=["testscript",], key="TestScripts", disabled=False, expand_y=True, expand_x=True, enable_events=True, select_mode=sg.LISTBOX_SELECT_MODE_SINGLE)
                            ],
                            [
                             sg.Button(STARTTESTBTN, key='StartStopScriptBtn',   pad=((5,0),(5,10)), disabled=True, **CommonButtonDef),
                            ]
                         ], expand_y=True, expand_x=True)
                ],
              ]

layout_tx_talk = [
                   [sg.Frame(TXTALKNAME,
                             [[
                                 sg.Multiline('', key="TxTalkOut", enable_events=False,
                                              expand_y=True, expand_x=True, auto_refresh=True,
                                              write_only=True, rstrip=True, autoscroll=True, disabled=True
                                             )
                            ]],expand_y=True, expand_x=True
                           )
                    ],
                   [sg.Text(TX_TALK_PROMPT, key='TxTalkPrompt',  pad=(5,0), size=(len(TX_TALK_PROMPT),1)),
                    sg.Input("",            key="TxTalkIn",      pad=(5,0), size=(50,1), **CommonInputDef, disabled=True),
                    # this button is hidden and responds to the Return Key of the main keyboard
                    sg.Button("Rtn Key",    key='TxTalkBtn', enable_events=True, bind_return_key=True, visible=False)]
                 ]
layout_automate = [
                    sg.Column( layout_testscript,expand_y=True, expand_x=True),
                    sg.VSeparator(color="#000000", key='VS1'),
                    sg.Column( layout_tx_talk, key="col2", expand_y=True, expand_x=True)
                  ]
layout = [
               [ sg.Text("Automation", text_color='green', font=('Courier', 20), key='textAUTO')],
               [sg.HSeparator(color="#000000", key="HSx")],
               [
                   sg.TabGroup([
                                [
                                 sg.Tab(TAB_AUTOMATE_TEXT, [feature_checkboxes, layout_inits, layout_automate], key="AutomateTab", disabled=False), 
                                ]
                               ],key='TG', **CommonTabGrpDef, expand_x=True, expand_y=True),
              ],
               [sg.HSeparator(color="#000000", key="HS2")],
               layout_exit,
           ]

def playScript(selfobj) :
    """ Uses selfobj to import and execute a script found in an expected location.
        testScriptLocation, executionTimeStart, GUI window, and objects pass to the script
        are part of the test
        This is where new arguments must be added for any new test scripts
    """
    from importlib import import_module, reload
    # now import the test list currently one list per testlist Location
    objMod = import_module(selfobj.testScriptLocation)
    # reload the module to pick up any changes
    objMod = reload(objMod)
    # To simplify the test list dictionary is called testlist
    modTestClass = getattr(objMod, 'TestClass')
    namedObjs = {
                'outputStringUpdater' : selfobj.outputStringUpdater,
                'stopScriptEvent'     : selfobj.stopScriptThreadEvent,
                'txSerialObj'         : selfobj.txSerialObj,
                }
    selfobj.executionTimeStart = time()
    if modTestClass is not None :
        obj = modTestClass(**namedObjs)
        obj.main()
    selfobj.window.write_event_value("ScriptDone", 0)
    del(obj)
    del(modTestClass)

LOG_FILE_DIRECTORY    = "/LogFiles/"
SCRIPT_FILE_DIRECTORY = "/TestScriptFiles/"

class Automation() :
    def __init__(self) :
        """ init the class variables and create the
            GUI window.
        """
        self.thdId = None 
        self.executionTimeStart = 0.0
        self.logFileFd = None
        self.testScriptLocation = None
        self.txOsVersion = None
        self.txSwVersion = None
        self.txBuildId   = None
        self.txHostname  = "mar-tx-stub"
        self.stopScriptThreadEvent = Event()
        self.StartSysTest  = True
        self.CheckAppsRun  = True
        self.MountUsbDrive = True
        self.window = sg.Window('Automation Test Console',
                                layout,
                                finalize=True,
                                font = theFont,
                                size=(1200,900),
                                location=(10,10),
                                margins=(2,2),
                                resizable=True,)
        self.Event_Dict = {
                'ScriptDone'         : self.onScriptDoneEvent,
                'StartStopScriptBtn' : self.onStartStopScriptBtnEvent,
                'TestScripts'        : self.onTestScriptEvent,
                'TxTalkBtn'          : self.onTxTalkBtnEvent,
                'ConnectBtn'         : self.onConnectDisconnectBtnEvent,
                'ST_Checked'         : self.onCheckBoxEvent,
                'AR_Checked'         : self.onCheckBoxEvent,
                'MD_Checked'         : self.onCheckBoxEvent,
                # add any new check boxes
                'Exit'               : self.onExitEvent,
                'default'            : self.onPassEvent,
                'InstallURLBtn'      : self.onInstallUrlBtnEvent,
                }
        self.txSerialObj = None
        scriptsDirectory = os.path.dirname(__file__)+SCRIPT_FILE_DIRECTORY
        # get the list of tests ready for display
        listdir =  glob.glob(scriptsDirectory+"CT*.py")
        listfname = []
        if not listdir :
            listfname = ["None found in:",f"{scriptsDirectory}"]
        else :
            # remove the directory from each file
            sys.path.append(scriptsDirectory)
            for pname in listdir :
                bname = os.path.basename(pname)
                listfname.append(bname)
        # fill the test scripts list box with the test files any .py file in this directory can be executed.
        self.window['TestScripts'].update(values=listfname)
        self.window['TestScripts'].update(disabled=True)
        self.logFileDirectory = os.path.dirname(__file__)+LOG_FILE_DIRECTORY

    def closeLogFileFd(self) :
        """ Closes the log file and sets the class variable to None.
        """
        if self.logFileFd :
            self.logFileFd.close()
            self.logFileFd = None

    def writeLogFileFd(self, strout) :
        """  If the log file is open, write the string strout to the file
             and flush the stream.
        """
        if self.logFileFd :
            self.logFileFd.write(strout)
            self.logFileFd.flush()

    def outputStringUpdater(self, strval, outputobj=None, clear_first=False) :
        """ Output a string to the Output Multiline element. Parse the string
            for a small subset of format commands. 'bold, italic' for character shape
            'red, blue, green, etc. etc. " for character color.
            Format strings are preceded by '</' and ended by '/>' the format is
            continued to the end of the string or another '/>'
        """
        strval = strval.replace('\r','')
        if outputobj is None :
            outputobj = self.window["TxTalkOut"]
        if clear_first :
            outputobj.update(value='')
        fgColor = None
        textFontChg = () 
        textFontStart = strval.find("</")
        while textFontStart >= 0 :
            textFontStop  = strval.find("/>")
            if textFontStart >= 0 and textFontStop >= 0 :
                textFontList = strval[textFontStart+2:textFontStop].split(',')
                for tmp in textFontList :
                    tmp = tmp.lower().strip()
                    if tmp in ['bold','italic'] :
                        textFontChg += (tmp,)
                    else :
                        fgColor = tmp
                # print out the string to the first font change chars
                outputobj.print(strval[:textFontStart], end='')
                self.writeLogFileFd(strval[:textFontStart])
                # now get and print the chars with the changed formatting
                strval = strval[textFontStop+2:]
                newFont = theFont+(textFontChg,)
                newFontStop  = strval.find("/>") # the end of the current format change
                if newFontStop < 0 :
                    newFontStop = len(strval)
                else :
                    textFontChg = ()
                outputobj.print(strval[:newFontStop], font=newFont, text_color=fgColor, end='')
                self.writeLogFileFd(strval[:newFontStop])
                if newFontStop >= 2 and newFontStop != len(strval) :
                    strval = strval[newFontStop+2:]
                    fgColor = None
                else :
                    strval = ''
                # check for more
                textFontStart = strval.find("</")
        # print what remains
        outputobj.print(strval)
        self.writeLogFileFd(strval+"\n")

###################### On Events ###########################
#
    def onCheckBoxEvent(self, _event, _values) :
        """ Process a checkbox change event paying attention to mutual exclusivity.
            Update class variables with the current state.
        """
        if not self.window['MD_Checked'].get() :
            self.window['ST_Checked'].update(value=False)
        self.MountUsbDrive = self.window['MD_Checked'].get() 
        self.StartSysTest  = self.window['ST_Checked'].get()
        self.CheckAppsRun  = self.window['AR_Checked'].get()
        self.window['MD_Checked'].update(text_color=CKBOX_TEXT_ON if self.MountUsbDrive else CKBOX_TEXT_OFF) 
        self.window['ST_Checked'].update(text_color=CKBOX_TEXT_ON if self.StartSysTest  else CKBOX_TEXT_OFF)
        self.window['AR_Checked'].update(text_color=CKBOX_TEXT_ON if self.CheckAppsRun  else CKBOX_TEXT_OFF)
        return True
    def onScriptDoneEvent(self, _event, _values) :
        """ Send the termination value to the output window to inform the tester.
        """
        tm = time()
        mins = int(tm-self.executionTimeStart)//60
        secs = tm-self.executionTimeStart - (mins * 60)
        self.outputStringUpdater("Script Done!")
        self.outputStringUpdater(f"Execution Time {mins} mins, {secs:5.2f} secs")
        self.window['StartStopScriptBtn'].update(disabled=True, text=STARTTESTBTN)
        self.txSerialObj.stopSystemTest()
        self.txOsVersion = None
        self.txSwVersion = None
        self.txBuildId   = None
        self.thdId       = None
        self.testScriptLocation = None
        self.closeLogFileFd()
        return True
    def onStartStopScriptBtnEvent(self, _event, _values) :
        """ Process a Start script or Stop script event. A class variable holding
            the script player's thread ID is used to determine if it's start or
            stop. A thread Event flag is used to inform the thread that a stop
            request was issued.
        """
        if self.thdId :
            self.stopScriptThreadEvent.set()
            self.window["StartStopScriptBtn"].update(text=STARTTESTBTN)
            self.thdId = None
            self.txSerialObj.stopSystemTest()
        else :
            self.stopScriptThreadEvent.clear()
            self.thdId = Thread(name='playScript', target=playScript, args=(self,), daemon=True)
            self.thdId.start()
            self.window["StartStopScriptBtn"].update(text=STOPTESTBTN)
        return True
    def onTestScriptEvent(self, _event, _values) :
        """ Process the test script select event to finish the environment initialization.
            This is where the Check box values are used to modify the environment.
        """
        testScriptName = _values[_event][0]
        sfd = f"{SCRIPT_FILE_DIRECTORY.strip('/')}."
        testScriptBasename = os.path.splitext(testScriptName)[0]
        self.testScriptLocation = sfd+testScriptBasename
        self.outputStringUpdater(f"Executing test script {self.testScriptLocation}")
        self.txOsVersion, self.txSwVersion, self.txBuildId = self.txSerialObj.getVersions()
        tmpTxAppRun = f"TX Apps Running - Unknown"
        if self.CheckAppsRun :
            cnt = 7
            while not self.txSerialObj.isTxSwRunning() and cnt > 0 :
                self.outputStringUpdater(f"Waiting 15 secs for TX Apps to start....")
                sleep(15)
                cnt -= 1
            tmpTxAppRun = f"TX Apps {'' if cnt > 0 else 'NOT'}Running"
        self.outputStringUpdater(f"{tmpTxAppRun}")
        self.outputStringUpdater(f"TX Host Name  = {self.txHostname}")
        self.outputStringUpdater(f"TX OS Version = {self.txOsVersion}")
        self.outputStringUpdater(f"TX SW Version = {self.txSwVersion}")
        self.outputStringUpdater(f"TX Build ID   = {self.txBuildId}")
        self.outputStringUpdater("Network Interfaces :")
        for n, v in self.txSerialObj.getIpAddresses().items() :
            self.outputStringUpdater(f"    {n} = {v}")
        if self.MountUsbDrive :
            self.outputStringUpdater("Mounting USB Drive")
            self.txSerialObj.lsblkCommand()
            self.txSerialObj.mountCommand()
            if not self.txSerialObj.isMounted :
                self.outputStringUpdater("The USB Drive is not ready (not mounted)")
            elif self.StartSysTest :
                self.outputStringUpdater("Starting system_test script")
                self.txSerialObj.startSystemTest()
                if self.txSerialObj.expectValFound :
                    self.outputStringUpdater("system_test script Started")
        logFileFd, logFilename = openFileName(self, self.logFileDirectory+testScriptBasename+".log")
        if logFileFd :
            self.outputStringUpdater(f"Opened {logFilename} for test output logging.")
            self.logFileFd = logFileFd
            self.writeLogFileFd(f"{tmpTxAppRun}")
            self.writeLogFileFd(f"TX OS Version = {self.txOsVersion}\n")
            self.writeLogFileFd(f"TX SW Version = {self.txSwVersion}\n")
            self.writeLogFileFd(f"TX Build ID   = {self.txBuildId}\n")
            self.writeLogFileFd("Network Interfaces :")
            for n, v in self.txSerialObj.getIpAddresses().items() :
                self.writeLogFileFd(f"    {n} = {v}")
        self.window["StartStopScriptBtn"].update(disabled=False)
        return True
    def onTxTalkBtnEvent(self, _event, _values) :
        """ Accept a command from the user to send to the tx console. If system_test
            is running the commands need to be one of its list. If not running then
            Linux commands are required.
            Type the Enter key to see which prompt is returned system_test or Linux.
        """
        p = _values['TxTalkIn']+"\r"
        self.txSerialObj.serial.readall()
        self.txSerialObj.serial.write(p.encode('utf-8'))
        num = 1 if _values['TxTalkIn'] == '' else 36000
        resp=self.txSerialObj.readNumLines(Number=num, maxTime=3600, sendoutput=self.outputStringUpdater)
        self.outputStringUpdater(resp)
        self.window['TxTalkIn'].update(value='')
        return True
    def onConnectDisconnectBtnEvent(self, _event, _values) :
        """ Process a connect event to find and open a serial connection to the
            TX console port. The port is found, ossia user is logged in, then sudo is
            used to become root.
            The txSerialObj is used to determine if this is a connect or disconnect
            event.
        """
        if not self.txSerialObj :
            self.outputStringUpdater("", clear_first=True)
            txPort = None
            for i in range(3) :
                txPort = getSerialPorts()
                if txPort :
                    break
                name = "TxPort"
                self.outputStringUpdater(f"Try {i} -- {name} not yet available.")
                sleep(5)
            self.txSerialObj  = Transmitter(txPort)
            if self.txSerialObj.serial and self.txSerialObj.checkForLogin() :
                self.txSerialObj.sendUsernamePassword('ossia','OssiaUser!')
                self.txSerialObj.moveToSudoRoot('OssiaUser!')
                self.txSerialObj.setTxPrompt()
                self.window['TestScripts'].update(disabled=False)
                self.window['TxTalkIn'].update(disabled=False)
                self.window['InstallURLBtn'].update(disabled=False)
                self.window["installurl"].update(disabled=False)
                self.txHostname = self.txSerialObj.getHostname()
                self.window['ConnectBtn'].update(text=UNCONNECTBTN)
                self.outputStringUpdater(f"Connected to {self.txHostname}")
            else :
                if self.txSerialObj.serial :
                    self.outputStringUpdater(f"Connection failed Try again please.")
                    self.txSerialObj.serial.close()
                    self.txSerialObj = None
                else :
                    self.outputStringUpdater(f"No TX Serial connection found.")

        else :
            if self.txSerialObj :
                self.txSerialObj.disconnectAndClose()
                self.txSerialObj = None
            self.window['ConnectBtn'].update(text=CONNECTBTN)
            self.outputStringUpdater(f"Disconnected TX Serial Console port")
            self.window['TestScripts'].update(disabled=True)
            self.window['TxTalkIn'].update(disabled=True)
            self.window['InstallURLBtn'].update(disabled=True)
            self.window["installurl"].update(disabled=True)
            self.txOsVersion = None
            self.txSwVersion = None
            self.txBuildId   = None
            self.txHostname  = "mar-tx-stub"
        return True
    def onExitEvent(self, _event, _values) :
        """ Process the Exit button and Application X button to exit the Automation
            application. Disconnect and close the txSerialObj and log file before
            exiting the GUI window event loop.
        """
        if self.txSerialObj :
            self.txSerialObj.disconnectAndClose()
        self.closeLogFileFd()
        return False
    def onPassEvent(self, _event, _values) :
        """ This should never be called but if it is, it does nothing. 
        """
        return True
    def onInstallUrlBtnEvent(self, _event, _values) :
        """ Process the Install URL button Event. Create a command string to run
            the update script on the TX.
            update -y -n <URL>
            The display screen will not collect and display the output of running
            the update script but it will monitor it for completion.
        """
        cmd = f"update -y -n {_values['installurl']}\r"
        #print(f"{cmd}")
        self.outputStringUpdater(f"Install Command '{cmd}'")
        tm = time()
        self.txSerialObj.serial.write(cmd.encode('utf-8'))
        self.outputStringUpdater("Waiting for </green,bold/>update -n/> to finish")
        self.txSerialObj.waitForUpdateFinish(self.txHostname)
        tmm = time() - tm
        minss = int(tmm / 60.0)
        secss = int(tmm - (minss * 60))
        self.outputStringUpdater(f"</green/>update -n/> finished in </bold/>{minss} mins {secss} secs/>")
        self.outputStringUpdater(f"Switching rootfs partition and rebooting.")
        cmd = "bootpart_switch -s && reboot"
        #print(f"{cmd}")
        rtn=self.txSerialObj.sendArbCommand(cmd, None, 10)
        self.outputStringUpdater(f"Waiting for boot to finish .....")
        #print(f"Waiting for boot to finish .....")
        if not self.txSerialObj.waitForBoot() :
            self.outputStringUpdater(f"Boot not finished after 2 mins.")
        else :
            self.outputStringUpdater(f"Boot finished.")
            #print(f"Boot finished.")
            self.outputStringUpdater(f"Logging back in.")
            self.txSerialObj.sendUsernamePassword('ossia','OssiaUser!')
            self.txSerialObj.moveToSudoRoot('OssiaUser!')
        return True
###################### END On Events ###########################


if __name__ == "__main__" :

    a=Automation()
    keepGoing = True
    while keepGoing :
        event, values = a.window.read() # waits for an event
        if event is None :
            break
        func = a.Event_Dict.get(event, a.onPassEvent)
        keepGoing = func(event, values)
    a.window.close()
    a.closeLogFileFd()


        

