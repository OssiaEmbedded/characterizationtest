# README #

charSerialGui.py is used to provide a simple interface to setup and run
a characterization test. It uses a GUI that provides a step by step process with instructions.

https://omnilectric.atlassian.net/wiki/spaces/EN/pages/2823389206/Mars-TX+Semi-Automated+Characterization+Test+System#

*** 
## What is needed ##

* A test computer
    * Create an OssiaDev user. (best to include IT)
    * Wire connection to the Intranet
    * A USB 2.0 or 3.0 Type A connector
    * git installed
        *  Git-2.41.0-64-bit.exe
    * python 3.7 or above, Plus
        * python-3.11.4-amd64.exe (Make sure to add python to the path.)
        * matplotlib
        * PySimpleGUI
        * numpy
        * pyserial
        * smbus2
        * screeninfo
* A 16 Gbyte or greater, USB flash drive
    * Formatted as a FAT 32 file system
*** 

## How do I get set up? ##

* Summary of set up
    * login as OssiaDev 
    * Install the above programs and modules
    * Open a Git bash Terminal window (similar to cmd but acts like linux/unix)
        * Change directory to ~ (cd ~) the following steps are all done in this directory.
    * clone the CharacterizationTest repository
        * First open the repository in a browser and download the OssiaDevSSHKeys
        * Create a directory mkdir -m 777 .ssh
        * Move the downloaded ssh keys into .ssh changing their names to remove the "OssiaDev_"
        * Execute git clone git@bitbucket.org:OssiaEmbedded/CharacterizationTest.git

* Configuration
    * Copy the bashrc cp ~/CharacterizationTest/OssiaDevSSHKeys/bashrc ~/.bashrc
    * Open a new Git bash window and close the first one (if asked say yes)
    * Type alias. You will see many aliases that can make things easier.
    * Insert the USB Flash drive into a connector on the Test Computer
    * Copy the contents of ~/CharacterizationTest/system_test/*  to the root of the flash drive.
    * Eject and move the flash drive to the USB Hub that will be connected to the TX.
*** 
## Running tests ##

* How to run tests
    * To run a test place an Ossia Cota 5.8 GHz Transmitter on the table in the test room
    * Connect the USB A cable to one of the exposed USB ports on the TX. 
    * Connect the Micro USB connector to the Serial Console port of the TX (closest to the RJ45 port)
    * Connect power to the TX and connect to 120 volts AC. 
    * Give the TX approximately 5 mins to boot up and become ready.
    * Ensure there is an Ossia Cota 5.8 GHz Receiver connected to the pole and power meter and all the USB connections are secure.
    * Open a Git Bash terminal and type ctst (alias to move into the clone directory)
    * Type CCC and follow the green buttons and instructions in the opened application.

*** 
## Contribution guidelines ##

* Writing tests
    * Tests consist of a TestList.py file. Review an existing one for example and format.
* Code review
    * Test lists have little need for code review. 
    * Adding features to the GUI will require a branch and Pull Request (PR).


*** 
## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact
