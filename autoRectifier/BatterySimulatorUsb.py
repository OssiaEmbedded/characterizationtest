import serial
from serial.tools.list_ports import comports
from time import sleep, time
import platform
windoz = platform.system() == "Windows"

# the setup and loop for the Battery Simulator (BS) (power analyzer)
def getSerialPort(serialName, force=False) :
    """ find the open port and check to see if any match the
        below criteria.
        must equal serial name
    """
    serial_ports = comports()
    print(f"ser p {serial_ports}")
    dev_serial = None
    for i, p in enumerate(serial_ports) :
        sserial = None
        print(f"hwid {p.hwid} -- vid={p.vid:X} pid={p.pid:X}\n")
        if p.hwid is None or serialName not in p.hwid :
            continue
        thePort = f"/dev/{p.name}"
        if windoz :
            thePort = f"{p.name}"
        try :
            if thePort is None :
                raise serial.serialutil.PortNotOpenError
            sserial = serial.Serial(port=thePort, baudrate=115200, timeout=0.5)
        except Exception as ee :
            print(f"Exception in serial connection to {thePort} creating stub == {ee}")
        else :
            if not sserial.is_open :
                print(f"Could not open device {thePort}")
            else :
                sserial.serial.write(b"\r")
                sserial.serial.readall()
                dev_serial = sserial
            break
    return dev_serial

class BatterySimUsb() :

   # batterySimCmds = {
   #         'reset'             : "*RST",
   #         'test type'         : ":ENTRy1:FUNCtion TEST", # Set BS to Battery Test 
   #         'set batt volt'     : ":BATT:TEST:VOLT {}", # CURVolt-Set Set BS V-set to 5V
   #         'set end current'   : ":BATT:TEST:CURR:END {}", # Set BS End Current to 0
   #         'set output on/off' : ":BATT:OUTP {}", #ON Set BS Output on 
   #         'measure V and I'   : ":MEASure:CONcurrent?", #Measure V and I
   #         }
    def __init__(self) :

        VID_PID  = 'VID:PID=05e6:2281'
        self.serial = getSerialPort(VID_PID)

    def sendCommand(self, cmd) :
        self.serial.write(cmd)
        return self.serial.readall().decode('utf-8')

    def reset(self) :
        self.sendCommand(f"*RST")
    def test_type(self) :
        self.sendCommand(f":ENTRy1:FUNCtion TEST") # Set BS to Battery Test 
    def set_batt_volt(self, voltage) :
        self.sendCommand(f":BATT:TEST:VOLT {voltage}") # CURVolt-Set Set BS V-set to 5V
    def set_end_current(self, end_current) :
        self.sendCommand(f":BATT:TEST:CURR:END {end_current}") # Set BS End Current to 0
    def set_output_on(self) :
        self.sendCommand(":BATT:OUTP 1") #ON Set BS Output on 
    def set_output_off(self) :
        self.sendCommand(":BATT:OUTP 0") #ON Set BS Output on 
    def measure_V_and_I(self) :
        V = 0
        I = 0
        resp = self.sendCommand(f":MEASure:CONcurrent?") #Measure V and I
        print(f"CONCurrent response '{resp}'")
        return V, I
if __name__ == "__main__" :
    bat = BatterySimUsb()
    print(f"meas v i = {bat.measure_V_and_I()}")

