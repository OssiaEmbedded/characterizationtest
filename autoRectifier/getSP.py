import serial
from serial.tools.list_ports import comports
from time import sleep, time
import platform
windoz = platform.system() == "Windows"

def getSerialPort(serialName, force=False) :
    """ find the open port and check to see if any match the
        below criteria.
        must equal serial name
    """
    serial_ports = comports()
    print(f"SP={serial_ports}")
    dev_serial = None
    for i, p in enumerate(serial_ports) :
        sserial = None
        print(f"manufacturer {p.manufacturer} -- p={p}\n")
        if p.manufacturer is None or p.manufacturer != "FTDI" or "V3500A" not in f"{p}" :
            continue
        thePort = f"/dev/{p.name}"
        if windoz :
            thePort = f"{p.name}"
        try :
            if thePort is None :
                raise serial.serialutil.PortNotOpenError
            sserial = serial.Serial(port=thePort, baudrate=9600, timeout=0.25)
        except Exception as ee :
            print(f"Exception in serial connection to {thePort} creating stub == {ee}")
        else :
            if not sserial.is_open :
                print(f"Could not open device {thePort}")
            else :
                dev_serial = sserial
            break
    return dev_serial

if __name__ == "__main__" :
    getSerialPort('')

