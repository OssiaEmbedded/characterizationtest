# @file      MCUsbSG.py
# @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
#            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
#            STRICTLY PROHIBITED.  COPYRIGHT 2023 OSSIA INC. (SUBJECT TO LIMITED
#            DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
#

import logging

try:
    import usb.core
except:
    raise RuntimeError('pyusb not installed -- resolve using the preceding instructions')

class MiniCircuits_SG() :
    """Operate a Mini Circuits Signal Generator"""

    _VENDOR_ID = 0x20CE
    _PRODUCT_ID = 0x0012 # SSG-6000RC
    _MSG_LEN = 64
    _ENDPOINT_READ = 0x81
    _ENDPOINT_WRITE = 0x1

    _READ_TIMEOUT = 5000
    _WRITE_TIMEOUT = 500

    _GET_DEVICE_MODEL_NAME      = 40
    _GET_DEVICE_SERIAL_NUMBER   = 41
    _SET_FREQUENCY              = 101
    _SET_POWER                  = 102
    _SET_FREQ_POWER             = 103
    _SET_OUTPUT_ONOFF           = 104

    def __init__(self) :
        """Connect to the USB sig gen and set it up"""
        self.dev = usb.core.find(
            idVendor=MiniCircuits_SG._VENDOR_ID,
            idProduct=MiniCircuits_SG._PRODUCT_ID
        )
        if not self.dev:
            raise FileNotFoundError("MiniCircuits SigGen not found!")
        if self.dev.is_kernel_driver_active(0) :
            self.dev.detach_kernel_driver(0)
        self.dev.set_configuration()
        self.dev.reset()
        self.model = self.get_model()
        self.sernum = self.get_serial()
        self.set_output_off()

    def _query(self, cmd) :
        """Write the padded command bytes and read the response"""
        pad_len = MiniCircuits_SG._MSG_LEN - len(cmd)
        self._write(cmd + [0 for i in range(pad_len)])
        response = self._read(MiniCircuits_SG._MSG_LEN)
        if len(response) != MiniCircuits_SG._MSG_LEN:
            logging.warning("Did not receive expected number of bytes")
        return response

    def _query_string(self, cmd, response_length=None) :
        """Issue the provided command and return the string response from the device.
        The first 0 character indicates the end of the response
        """
        resp = self._query(cmd)
        if response_length is None:
            result = resp[1:resp.index(0)]
        else:
            result = resp[1:response_length-1]
        return ''.join([chr(i) for i in result])

    def _read(self, len) :
        """Interrupt task required to read from the USB device."""
        response = []
        try:
            response = self.dev.read(
                MiniCircuits_SG._ENDPOINT_READ,
                len,
                MiniCircuits_SG._READ_TIMEOUT
            )
            return response
        except Exception as e:
            logging.error(f"Exception in _read: {e}")

    def _write(self, cmd) :
        """Interrupt task required to write to the USB device."""
        try:
            nsent = self.dev.write(
                MiniCircuits_SG._ENDPOINT_WRITE,
                cmd,
                MiniCircuits_SG._WRITE_TIMEOUT
            )
            return nsent
        except Exception as e:
            logging.error(f"Exception in _write: {e}")

    def get_model(self) :
        """Get the device model number as a string"""
        return self._query_string([MiniCircuits_SG._GET_DEVICE_MODEL_NAME])

    def get_serial(self) :
        """Get the device serial number as a string"""
        return self._query_string([MiniCircuits_SG._GET_DEVICE_SERIAL_NUMBER])

    def set_power(self, powerVal) :
        """set the power output value"""
        if isinstance(powerVal, str) :
            powerVal = float(powerVal)
        power_list = list(int(powerVal*100).to_bytes(2, 'big'))
        sign_val = 1 if powerVal < 0.0 else 0
        resp = self._query_string(
            [MiniCircuits_SG._SET_POWER, sign_val]+power_list+[0],
            response_length=2)

    def set_frequency(self, freqMHz) :
        """set the frequency value"""
        if isinstance(freqMHz, str) :
            freqMHz = float(freqMHz)
        freq_val = int(freqMHz * 1000000.0)
        freq_list = list(freq_val.to_bytes(5, 'big'))
        resp = self._query_string(
            [MiniCircuits_SG._SET_FREQUENCY]+[freq_list]+[0],
            response_length=2)

    def set_freq_power(self, freqMHz, powerVal) :
        """Set both the frequency and power values."""
        # freq
        if isinstance(freqMHz, str) :
            freqMHz = float(freqMHz)
        freq_val = int(freqMHz * 1000000.0)
        freq_list = list(freq_val.to_bytes(5, 'big'))
        #power
        if isinstance(powerVal, str) :
            powerVal = float(powerVal)
        power_list = list(int(powerVal*100).to_bytes(2, 'big'))
        sign_val = 1 if powerVal < 0.0 else 0
        # send them
        resp = self._query_string(
            [MiniCircuits_SG._SET_FREQ_POWER]+freq_list+[sign_val]+power_list+[0],
            response_length=2)

    def set_output_off(self) :
        """send a 0 to turn off the RF output"""
        resp = self._query_string(
            [MiniCircuits_SG._SET_OUTPUT_ONOFF, 0],
            response_length=2)

    def set_output_on(self) :
        """send a 1 to turn on the RF output"""
        resp = self._query_string(
            [MiniCircuits_SG._SET_OUTPUT_ONOFF, 1],
            response_length=2)

if __name__ == "__main__" :

    mcsg = MiniCircuits_SG()
    print(f"{mcsc.model}")
    print(f"{mcsc.sernum}")

