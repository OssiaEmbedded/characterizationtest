import sys
import socket
import select
from time import sleep

class TelnetClient() :
    def __init__(self, ipaddr, port=5025) :
        """ init the class
            ipaddr required at first arg
            port defaults to 5025 at second arg.
        """
        self.lsock = None
        self.portnum = None
        try :
            self.ipaddress = socket.gethostbyname(ipaddr)
            self.portnum = port if isinstance(port, int) else 5025 if port is None else int(port, 0)
        except Exception as ee :
            print("Exception IPAddress -- {}".format(ee))
            exit(1)
        else :
            self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.inputs = []

    def __del__(self) :
        """ leave the class
        """
        self.closeSock()

    def closeSock(self) :
        """ check for a valid socket then close it
        """
        if self.lsock :
            self.lsock.close()
        self.lsock = None

    def connectToServer(self) :
        """ Use the class variables of ipaddress and port number to
            connect to the expected telnet server.
        """
        try :
            self.lsock.connect((self.ipaddress, self.portnum))
        except Exception as ee :
            print("Server at {} port {} not available for connection.".format(self.ipaddress, self.portnum))
            return None
        else :
            self.inputs = [self.lsock] # setup for accessing socket using select
            self.lsock.setblocking(0)
        return self.lsock

    def getReceivedData(self, timeout=0.1) :
        """ get data from the socket as long as the socket has readable data
            returns the decoded data or None
        """
        val = ''
        readable, _, _ = select.select(self.inputs, [], [], timeout)
        if readable :
            for sock in readable :
                dat = sock.recv(0x100)
                if dat  :
                    try :
                        val = dat.decode('utf-8')
                    except :
                        val = "Exception"
                        break
        return val

    def sendCommand(self, cmdString, value=None) :
        """ send the cmdString to the socket encoded as bytes
        """
        fullCmdString = None
        if cmdString :
            fullCmdString = cmdString if not value else cmdString.format(value)
            fullCmdString += "\n"
            self.lsock.sendall(fullCmdString.encode('utf-8'))


class BatterySimNet() :

    def __init__(self, ipaddr, port=5025) :
        """ init the class by connecting to the instrument's socket server
            ipaddr required at first arg
            port defaults to 5025 at second arg.
        """
        self.client = TelnetClient(ipaddr, port)
        if self.client.connectToServer() :
            pass
        else :
            exit()

    def sendCommandReceiveData(self, cmd) :
        """ send the "cmd" string to the server and expect a return value
        """
        fcmd = cmd + "\n"
        print(f"fcmd='{fcmd}'")
        self.client.sendCommand(fcmd)
        sleep(0.25)
        val = self.client.getReceivedData().strip()
        return val

    def getIdentity(self) :
        """ Return the Identity response from the instrument
        """
        return self.sendCommandReceiveData("*IDN?")
    def getLan(self) :
        """ Return the LAN configuration of the instrument
        """
        return self.sendCommandReceiveData(":SYSTem:COMMunication:LAN:CONFigure?")
    def getDate(self) :
        """ Get the current Date as believed by the instrument
        """
        return self.sendCommandReceiveData(":SYSTem:DATE?")
    def getTime(self) :
        """ Get the current Time as believed by the instrument
        """
        return self.sendCommandReceiveData(":SYSTem:TIME?")
    def getVersion(self) :
        """ Return the current SCPI Version
        """
        return self.sendCommandReceiveData(":SYSTem:VERSion?")
    def getTracePnts(self) :
        """ Return the current number of points in a trace
        """
        return self.sendCommandReceiveData(":TRACe:POINts?")
    def getDataPnts(self) :
        """ Return the current number of points in channel 1's data
        """
        return self.sendCommandReceiveData(":DATA1:POINts?")
    def getMacAdr(self) :
        """ Return the MAC address from the instrument's Network interface card
        """
        return self.sendCommandReceiveData(":SYSTem:COMMunication:LAN:MACaddress?")
    def reset(self) :
        """ Reset the instrument
        """
        return self.sendCommandReceiveData(f"*RST")
    def setFuncAsTest(self) :
        """ set the instrument's entry function to TEST
        """
        return self.sendCommandReceiveData(f":ENTRy1:FUNCtion TEST") # Set BS to Battery Test 
    def setBatVolt(self, voltage) :
        """ set the instrument's simulation battery voltage
        """
        return self.sendCommandReceiveData(f":BATT:TEST:VOLT {voltage}") # CURVolt-Set Set BS V-set to 5V
    def setEndCurrent(self, end_current) :
        """ set the instrument's End Current simulation value
        """
        return self.sendCommandReceiveData(f":BATT:TEST:CURR:END {end_current}") # Set BS End Current to 0
    def setOutputOn(self) :
        """ set the instrument's output to ON
        """
        return self.sendCommandReceiveData(":BATT:OUTP 1") #ON Set BS Output on 
    def setOutputOff(self) :
        """ set the instrument's output to OFF
        """
        return self.sendCommandReceiveData(":BATT:OUTP 0") #ON Set BS Output on 
    def measureVandI(self) :
        """ measure the instrument's Voltage and Current
        """
        V = 0
        I = 0
        resp = self.sendCommandReceiveData(f":MEASure:CONcurrent?") #Measure V and I
        print(f"CONCurrent response '{resp}'")
        return V, I

if __name__ == "__main__" :
    ipaddr = "10.10.0.133"
    if len(sys.argv) > 1 :
        ipaddr = sys.argv[1]
    bat = BatterySimNet(ipaddr, 5025)
    idval = bat.getIdentity()
    print(f"Instrument Identity is : {idval}")
    idval = bat.getMacAdr()
    print(f"Mac Address         is : {idval}")

