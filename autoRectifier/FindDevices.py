################################################################################
# © Keysight Technologies 2016
#
# You have a royalty-free right to use, modify, reproduce and distribute
# the Sample Application Files (and/or any modified version) in any way
# you find useful, provided that you agree that Keysight Technologies has no
# warranty, obligations or liability for any Sample Application Files.
#
################################################################################

import pyvisa as visa


def find(searchString):
    """ find -- uses searchString to search the visa resource
                manager for the requested devices if present.
    """
    resourceManager = visa.ResourceManager()

    devices = resourceManager.list_resources(searchString)
    dev = None
    if len(devices) > 0:
        for device in devices:
            print(f'Device {device}')
            try :
                session = resourceManager.open_resource(device)
                session.write("*IDN?")
                print(f"ID = {session.read()}")
                resourceManager.close()
            except visa.Error as ex :
                print(f"Error connecting to '{device}' or getting *IDN : ERROR {ex}")
            except Exception as exx :
                print(f"Exception Error connecting to '{device}' or getting *IDN : ERROR {exx}")
    return dev

if __name__ == "__main__" :
    # Finding all devices and interfaces is straightforward
    #find('?*')
    # You can specify other device types using different search strings. Here are some common examples:
    # All instruments (no INTFC, BACKPLANE or MEMACC)
    #find('?*INSTR')
    # PXI modules
    #find('PXI?*INSTR')
    # USB devices
    print('Find USB devices and interfaces:\n')
    find('USB?*INSTR')
    # GPIB instruments
    #find('GPIB?*')
    # GPIB interfaces
    #find('GPIB?*INTFC')
    # GPIB instruments on the GPIB0 interface
    #find('GPIB0?*INSTR')
    # LAN instruments
    #find('TCPIP?*')
    # SOCKET (::SOCKET) instruments
    #find('TCPIP?*SOCKET')
    # VXI-11 (inst) instruments
    #find('TCPIP?*inst?*INSTR')
    # HiSLIP (hislip) instruments
    #find('TCPIP?*hislip?*INSTR')
    # RS-232 instruments
    #find('ASRL?*INSTR')
    print('Done.')
