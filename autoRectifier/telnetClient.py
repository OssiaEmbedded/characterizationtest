import sys
import socket
import select
from time import sleep

class TelnetClient() :
    def __init__(self, ipaddr, port=5025) :
        """ init the class
            ipaddr required at first arg
            port defaults to 5025 at second arg.
        """
        self.lsock = None
        self.portnum = None
        try :
            self.ipaddress = socket.gethostbyname(ipaddr)
            self.portnum = port if isinstance(port, int) else 5025 if port is None else int(port, 0)
        except Exception as ee :
            print("Exception IPAddress -- {}".format(ee))
            exit(1)
        else :
            self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.inputs = []

    def __del__(self) :
        """ leave the class
        """
        self.closeSock()

    def closeSock(self) :
        """ check for a valid socket then close it
        """
        if self.lsock :
            self.lsock.close()
        self.lsock = None

    def connectToServer(self) :
        """ Use the class variables of ipaddress and port number to
            connect to the expected telnet server.
        """
        try :
            self.lsock.connect((self.ipaddress, self.portnum))
        except Exception as ee :
            print("Server at {} port {} not available for connection.".format(self.ipaddress, self.portnum))
            return None
        else :
            self.inputs = [self.lsock] # setup for accessing socket using select
            self.lsock.setblocking(0)
        return self.lsock

    def getReceivedData(self, timeout=0.1) :
        """ get data from the socket as long as the socket has readable data
            returns the decoded data or None
        """
        val = ''
        readable, _, _ = select.select(self.inputs, [], [], timeout)
        if readable :
            for sock in readable :
                dat = sock.recv(0x100)
                if dat  :
                    #print("###########  {}".format(type(dat)))
                    try :
                        val = dat.decode('utf-8')
                    except :
                        val = "Exception"
                        break
        return val

    def sendCommand(self, cmdString, value=None) :
        """ send the cmdString to the socket encoded as bytes
        """
        fullCmdString = None
        if cmdString :
            fullCmdString = cmdString if not value else cmdString.format(value)
            fullCmdString += "\n"
            self.lsock.sendall(fullCmdString.encode('utf-8'))

if __name__ == "__main__" :

    ipaddr = None
    port = None
    command = None
    args = len(sys.argv)
    if args > 1 :
        ipaddr = sys.argv[1] # get the ip address
    if args > 2 :
        try :
            port   = int(sys.argv[2], 0) # get the port number
        except :
            port = 5025
    if args > 3 :
        command = ""
        for n in range(3, args) :
            command += sys.argv[n] # get the command

    if ipaddr :
        client = TelnetClient(ipaddr, port)
        client.connectToServer()
        client.sendCommand(command)
        sleep(0.25)
        val = client.getReceivedData().strip()
        print(f"{ipaddr} {port} '{command}' = {val}")
    else :
        print("No Ip address given.")

