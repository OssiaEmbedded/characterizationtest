import sys
import socket
import select
from time import sleep

class MiniCircuits() :
    mcSignalGenCmds = {
                        "Get Model" : ":MN?",
                        "Get Serial Number" : ":SN?",
                        "Get Mac Address" : ":MAC?",
                        "Set Frequency" : ":FREQ:{}",
                        "Get Frequency" : ":FREQ?",
                        #"SerNum" : ":FREQ:MAX",
                        "Set Power Level" : ":PWR:{}",
                        "Get Power Level" : ":PWR?",
                        #"SerNum" : ":PWR:MAX",
                        "RF Output OnOff" : ":PWR:RF:{}",
                        "Get RF Output State:" : ":PWR:RF?",
                        "Get On Off Count" : ":ONOFFCOUNTER?",
                        "Get Power On Time" : ":OPERATIONTIME?",
                        "Get FW Version" : ":FIRMWARE?", # last two characters
                        #"SerNum" : ":FSWEEP:STARTFREQ:{}",
                        #"SerNum" : ":FSWEEP:STARTFREQ?",
                        #"SerNum" : ":FSWEEP:POWER:{}",
                        #"SerNum" : ":FSWEEP:POWER?",
                        #"SerNum" : ":FSWEEP:DWELL:{}",
                        #"SerNum" : ":FSWEEP:DWELL?",
                        #"SerNum" : ":FSWEEP:DIRECTION:{}",
                        #"SerNum" : ":FSWEEP:DIRECTION?",
                        #"SerNum" : ":FSWEEP:TRIGGERIN:{}",
                        #"SerNum" : ":FSWEEP:TRIGGEROUT:{}",
                        #"SerNum" : ":FSWEEP:TRIGGERIN?",
                        #"SerNum" : ":FSWEEP:TRIGGEROUT?",
                        #"SerNum" : ":FSWEEP:MODE:O",
                        "Power Sweep Start" : ":PSWEEP:STARTPOWER:{}",
                        "Get Power Sweep Start" : ":PSWEEP:STARTPOWER?",
                        "Set Power Sweep Frequency" : ":PSWEEP:FREQ:{}",
                        "Get Power Sweep Frequency" : ":PSWEEP:FREQ?",
                        "Set Power Sweep Dwell" : ":PSWEEP:DWELL:{}",
                        "Get Power Sweep Dwell" : ":PSWEEP:DWELL?",
                        #"SerNum" : ":PSWEEP:MINDWELL",
                        "Set Power Sweep Direction" : ":PSWEEP:DIRECTION:{}",
                        "Get Power Sweep Direction" : ":PSWEEP:DIRECTION?",
                        #"SerNum" : ":PSWEEP:TRIGGERIN:",
                        #"SerNum" : ":PSWEEP:TRIGGEROUT:",
                        #"SerNum" : ":PSWEEP:TRIGGERIN",
                        #"SerNum" : ":PSWEEP:TRIGGEROUT",
                        "Set Power Sweep Mode" : ":PSWEEP:MODE:{}",
                        #"SerNum" : ":HOP:POINTS:5",
                        #"SerNum" : ":HOP:POINTS",
                        #"SerNum" : ":HOP:MAXPOINTS",
                        #"SerNum" : ":HOP:POINT:",
                        #"SerNum" : ":HOP:POINT",
                        #"SerNum" : ":HOP:FREQ:1000.",
                        #"SerNum" : ":HOP:FREQ",
                        #"SerNum" : ":HOP:POWER:1",
                        #"SerNum" : ":HOP:POWER",
                        #"SerNum" : ":HOP:DWELL:10",
                        #"SerNum" : ":HOP:DWELL",
                        #"SerNum" : ":HOP:MINDWELL",
                        #"SerNum" : ":HOP:DIRECTION:",
                        #"SerNum" : ":HOP:DIRECTION",
                        #"SerNum" : ":HOP:TRIGGERIN:",
                        #"SerNum" : ":HOP:TRIGGEROUT:",
                        #"SerNum" : ":HOP:TRIGGERIN",
                        #"SerNum" : ":HOP:TRIGGEROUT",
                        #"SerNum" : ":HOP:MODE:O",
                        #"SerNum" : ":PULSE:TIMEON:1",
                        #"SerNum" : ":PULSE:TIMEUNITS=USE",
                        #"SerNum" : ":PULSE:MODE:FREERUN:O",
                        #"SerNum" : ":DFS:NoOfPulses:5",
                        #"SerNum" : ":DFS:NoOfPulses",
                        #"SerNum" : ":DFS:Pulse_IDX:",
                        #"SerNum" : ":DFS:Pulse_IDX",
                        #"SerNum" : ":DFS:RF:FREQ:1000.",
                        #"SerNum" : ":DFS:RF:FREQ",
                        #"SerNum" : ":DFS:RF:POWER:1",
                        #"SerNum" : ":DFS:RF:POWER",
                        #"SerNum" : ":DFS:PulseWidth:10",
                        #"SerNum" : ":DFS:PulseWidth",
                        #"SerNum" : ":DFS:Interval:20",
                        #"SerNum" : ":DFS:Interval",
                        #"SerNum" : ":DFS:NoOfCycles:10",
                        #"SerNum" : ":DFS:NoOfCycles",
                        #"SerNum" : ":DFS:CONT:",
                        #"SerNum" : ":DFS:CONT",
                        #"SerNum" : ":DFS:MODE:O",
                        }
    def __init__(self, ipaddr=None, port=23) :
        try :
            self.ipaddress = socket.gethostbyname(ipaddr)
            self.portnum = port if isinstance(port, int) else int(port)
        except Exception as ee :
            print("Exception IPAddress -- {}".format(ee))
            exit(1)
        self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.inputs = []
        self.connectToServer()

    def __del__(self) :
        self.closeSock()

    def getCmdDict(self) :
        return self.mcSignalGenCmds

    def closeSock(self) :
        if self.lsock :
            self.lsock.close()
        self.lsock = None

    def connectToServer(self) :
        try :
            self.lsock.connect((self.ipaddress, self.portnum))
        except Exception as ee :
            print("Server at {} port {} not available for connection.".format(self.ipaddress, self.portnum))
            return None
        else :
            self.inputs = [self.lsock] # setup for accessing socket using select
            self.lsock.setblocking(0)
            self.getReceivedData()
        return self.lsock

    def getReceivedData(self, timeout=0.1) :
        jval = None
        readable, _, _ = select.select(self.inputs, [], [], timeout)
        if readable :
            for sock in readable :
                dat = sock.recv(0x100)
                if dat  :
                    #print("###########  {}".format(type(dat)))
                    try :
                        jval = dat.decode('utf-8')
                    except :
                        jval = "Exception"
                        break
        return jval

    def sendCommand(self, cmdName, value=None) :
        fullCmdString = None
        cmdString = self.mcSignalGenCmds.get(cmdName, None)
        if cmdString :
            fullCmdString = cmdString if not value else cmdString.format(value)
            fullCmdString += "\n"
            self.lsock.sendall(fullCmdString.encode('utf-8'))
        val = self.getReceivedData()
        #print(f"sendCommand {fullCmdString}")
        #print(f"sendCommand {val}")

        try :
            fullCmdString = fullCmdString.strip()
            val = val.strip()
        except :
            fullCmdString = "EXception"
            val = "ExceptioN"

        return fullCmdString, val
    
    def getModel(self) :
        """ returns the model number of the device.
        """
        val = self.sendCommand(":MN?")
        return val
    def getSerialNum(self) :
        """ returns the serial number of the device.
        """
        val = self.sendCommand(":SN?")
        return val
    def getMacAdr(self) :
        """ returns the mac address number of the device.
        """
        val = self.sendCommand(":MAC?")
        return val
    def setFrequency(self, freq) :
        """ sets the output frequency to 'freq'.
        """
        self.sendCommand(f":FREQ:{freq} MHz")
    def getFrequency(self) :
        """ returns the current output frequency.
        """
        val = self.sendCommand(":FREQ?")
        return val
    def setPowerLevel(self, pwrdbm) :
        """ set the output power level in dBm.
        """
        self.sendCommand(f":PWR:{pwrdbm} dBm")
    def getPowerLevel(self) :
        """ return the output power level.
        """
        val = self.sendCommand(":PWR?")
        return val
    def setOutputOn(self) :
        """ enable the output to ON at the current frequency
            and power level.
        """
        self.sendCommand(":PWR:RF:ON")
    def setOutputOff(self) :
        """ disable the output to OFF without modifying frequency or 
            output power.
        """
        self.sendCommand(":PWR:RF:OFF")
    def getOutputState(self) :
        """ returns the output state on or off
        """
        val = self.sendCommand(":PWR:RF?")
        return val

if __name__ == "__main__" :

    ipaddr = None
    port = '23'
    if len(sys.argv) > 1 :
        ipaddr = sys.argv[1]
    if len(sys.argv) > 2 :
        port   = sys.argv[2]

    if ipaddr :
        mc = MiniCircuits(ipaddr,port)
        cmdNameList = list(mc.getCmdDict().keys())
        for n in cmdNameList :
            if n.startswith('Get') :
                cmd, val = mc.sendCommand(n)
                print("{} = {}".format(cmd, val))

        if len(sys.argv) > 3 :
            onoff  = sys.argv[3].upper()
            pwrcmd = "Set Frequency"
            #print(f"{pwrcmd}", end='')
            if len(sys.argv) > 4 :
                freq = sys.argv[4]
                freq = f"{freq}e6"
            else :
                freq = "5751e6"
            cmd, val = mc.sendCommand(pwrcmd, freq)
            print(f"\n{pwrcmd}")
            print(f"{cmd}")
            print(f"{val}")
            pwrcmd = "Get Frequency"
            cmd, val = mc.sendCommand(pwrcmd)
            print(f"\n{pwrcmd}")
            print(f"{cmd}")
            print(f"{val}")

            #sleep(2)
            print(f"{onoff}")
            if onoff not  in ['ON', 'OFF'] :
                onoff = 'OFF'
            pwrcmd = "RF Output OnOff"
            #print(f"{pwrcmd}", end='')
            cmd, val = mc.sendCommand(pwrcmd, onoff)
            print(f"{pwrcmd}")
            print(f"{cmd}")
            print(f"{val}")
            #sleep(2)
        if len(sys.argv) > 3 :
            pwrlvl = sys.argv[3]
            pwrcmd = "Set Power Level"
            #print(f"{pwrcmd} {pwrlvl}", end='')
            cmd, val = mc.sendCommand(pwrcmd, pwrlvl)
            print("\nPWRLVL")
            print(f"{pwrcmd}")
            print(f"{cmd}")
            print(f"{val}\n\n")

    else :
        print("No Ip address given.")

