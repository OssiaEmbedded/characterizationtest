#! /usr/bin/env python3
""" 
sshCom.py: A class to connect using ssh to execute remote executables

Version : 0.0.1
Date : Apr 15 2024
Copyright Ossia Inc. 2024

"""
import paramiko, sys

def sshPrint(msg) :
    if SshCom.debug :
        print(msg)

class SshCom() :
    debug = False
    def __init__(self, value=None, idebug=False) :
                
        self.client =  paramiko.SSHClient()
        self.client.load_system_host_keys()
        #Auto add policy is not a good practice in production/untrusted env
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        self.hostname = 'ossiabbb9.local'
        self.hostport = 22
        self.username = "root"
        self.password = ''
        
        self.printstdout = False
        self.commandssent = None
        self.linesreceived = None
        self.errors = None
        
        self.connected = False
        self.debug = idebug
        SshCom.debug = self.debug

        sshPrint(f"\nSSH client object created for the host {self.username}@{self.hostname}")
        if not self.connect() :
            exit()
        if value is not None :
            self.command(value)
            self.close()
            
    def connect(self) :
        result_flag = True
        if not self.connected :
            result_flag = False
            try :
                sshPrint("Establishing SSH connection.")
                self.client.connect(self.hostname, self.hostport,
                             self.username, self.password)
            except paramiko.ssh_exception.NoValidConnectionsError as e :
                sshPrint(f'SSH transport is not ready... {repr(e)}')
            except paramiko.AuthenticationException as e :
                sshPrint(f"Authentication failed, please verify your credentials {repr(e)}")
            except paramiko.SSHException as sshException :
                sshPrint(f"Could not establish SSH connection : {repr(sshException)}")
            except Exception as e :
                sshPrint("Exception in connecting to the server")
                sshPrint(f"PYTHON SAYS : {repr(e)}")
                self.client.close()
            else :
                result_flag = True
        
        if result_flag :
            sshPrint(f"\nSSH connection to {self.username}@{self.hostname} established.")
        self.connected = result_flag

        return result_flag
    
    def close(self) :
        self.client.close()
        sshPrint(f"\nSSH connection terminated : {self.username}@{self.hostname} is no longer connected.")
        self.connected = False
        
    def command(self, command_str, otherpw=None) :
        #Execute command
        command_str = f"/root/bin/sendspi ioset pu {command_str}"
        sshPrint(f"\nusername@hostname: {self.username}@{self.hostname}")
        sshPrint(f"Command string: '{command_str}")
        try :
            stdin, stdout, stderr = self.client.exec_command(command_str,timeout=20)
        except paramiko.SSHException as ss :
            print(f"SSHException in sshcomm.command -- {repr(ss)}")
        except Exception as ee : 
            print(f"Unknown Exception in sshcomm.command -- {repr(ee)}")
            return 1
        else :
            self.commandssent =  tuple([command_str])

        self.linesreceived = ()
        self.errors = ()
        try :
            self.errors = tuple(stderr.readlines()) 
        except Exception as ee :
            print(f"Exception on stdERR readlines -- {repr(ee)}")
        try :
            self.linesreceived = tuple(stdout.readlines()) 
        except Exception as ee :
            print(f"Exception on stdOUT readlines -- {repr(ee)}")
        
        if self.errors :
            sshPrint("ERROR:")
            sshPrint("".join(self.errors))

        sshPrint("Returned strings:")
        sshPrint("".join(self.linesreceived))
            
        return stdout.channel.recv_exit_status()
    
if __name__ == "__main__" :

    import time
    argc = len(sys.argv)
    if argc == 2 : # for use as a standalone program to command the pwr supply to turn on or off 0==on 1==off
        SshCom(sys.argv[1], False)
    else :
        # an example of using it as an imported module.
        sc = SshCom()
        sc.command(1) # turn power off
        time.sleep(2)
        sc.command(0) # turn power on
        time.sleep(2)
        sc.command(1) # turn power off
        time.sleep(2)
        sc.command(0) # turn power on
        sc.close()

    
