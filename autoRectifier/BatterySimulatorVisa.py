from time import sleep, time
import platform
windoz = platform.system() == "Windows"
import pyvisa as visa
VISA_ADDRESS = "USB0::0x05E6::0x2281::?*::INSTR"
import sys

# the setup and loop for the Battery Simulator (BS) (power analyzer)

class BatterySimVisa() :

   # batterySimCmds = {
   #         'reset'             : "*RST",
   #         'test type'         : ":ENTRy1:FUNCtion TEST", # Set BS to Battery Test 
   #         'set batt volt'     : ":BATT:TEST:VOLT {}", # CURVolt-Set Set BS V-set to 5V
   #         'set end current'   : ":BATT:TEST:CURR:END {}", # Set BS End Current to 0
   #         'set output on/off' : ":BATT:OUTP {}", #ON Set BS Output on 
   #         'measure V and I'   : ":MEASure:CONcurrent?", #Measure V and I
   #         }
    def __init__(self, requestedResource=None) :
        """ initialize the VISA session to the provided device
            Open the session and record the resources name
        """

        try:
            # Create a connection (session) to the instrument
            self.resourceManager = visa.ResourceManager()
            devices = self.resourceManager.list_resources(VISA_ADDRESS)
            #print(f"devices = {devices}")
            for device in devices :
                if requestedResource and requestedResource not in device :
                    continue
                self.session = self.resourceManager.open_resource(device)
        except visa.Error as ex:
            print(f"Couldn't connect to '{requestedResource}',  {ex} exiting now...")
            sys.exit()
        except Exception as ex :
            print(f"Couldn't connect to the '{requestedResource}', {ex}  exiting now...")
            sys.exit()
    

    def sendCommand(self, cmd) :
        resp = ''
        print(f"Command = '{cmd}'   ", end='')
        fcmd=cmd+"\r"
        self.session.write(fcmd)
        if "?" in cmd :
            try :
                resp = self.session.read()
            except visa.errors.VisaIOError as rv :
                print(f"read failed for {cmd}")
        sleep(0.5)
        return resp

    def getMacAdr(self) :
        return self.sendcommand(":SYSTem:COMMunication:LAN:MACaddress?")
    def getId(self) :
        return self.sendCommand(f"*IDN?")
    def reset(self) :
        return self.sendCommand(f"*RST")
    def setEntryFunc(self) :
        return self.sendCommand(f":ENTRy1:FUNCtion TEST") # Set BS to Battery Test 
    def setBattVolt(self, voltage) :
        return self.sendCommand(f":BATT:TEST:VOLT {voltage}") # CURVolt-Set Set BS V-set to 5V
    def getVsetVoltage(self) :
        return self.sendCommand(f":BATT:TEST:VOLT?")
    def setVsetVoltage(self, voltage) :
        return self.sendCommand(f":BATT:TEST:VOLT {voltage}")
    def setEndCurrent(self, end_current) :
        return self.sendCommand(f":BATT:TEST:CURR:END {end_current}") # Set BS End Current to 0
    def setOutputOn(self) :
        return self.sendCommand(":BATT:OUTP 1") #ON Set BS Output on 
    def setOutputOff(self) :
        return self.sendCommand(":BATT:OUTP 0") #ON Set BS Output on 
    def getOutput(self) :
        return self.sendCommand(":BATT:OUTP?") #Get BS Output  
    def setTestCurLimit(self, current) :
        return self.sendCommand(f":batt:test:sens:ah:ilim {current}")
    def setTestFullVolt(self, voltage) :
        return self.sendCommand(f":batt:test:sens:ah:vful {voltage}")
    def setTestESRSamp(self, sample) :
        return self.sendCommand(f":batt:test:sens:ah:esri {sample}")
    def setTestGmodRange(self, low, high) :
        return self.sendCommand(f":batt:test:sens:ah:gmod:rang {low}, {high}")
    def setTestExecStart(self) :
        return self.sendCommand(f":batt:test:sens:ah:exec STAR")
    def getSenseFunction(self) :
        return self.sendCommand(f':sense1:function?')
    def setSenseFunction(self, func="CONC") :
        return self.sendCommand(f':sense1:function "{func}"')
    def measureVI(self) :
        V = None
        I = None
        resp = self.sendCommand(f":MEAS:CONC?") #Measure V and I
        ivlst = resp.split(',')
        if len(ivlst) > 1 :
            I = ivlst[0].rstrip('A')
            V = ivlst[1].rstrip('V')
        return V, I
    def testmod(self) :
        bat = BatterySimVisa()
        print(f"ID = {bat.getId()}")

    def getErrorCode(self) :
        return self.sendCommand(f":syst:err:code?")
    def getErrorCount(self) :
        return self.sendCommand(f":syst:err:coun?")
    def getError(self) :
        return self.sendCommand(f":syst:err?")

if __name__ == "__main__" :
    bat = BatterySimVisa()
    print(f"ID = {bat.getId().strip()}")

    cnt = bat.getErrorCount()
    print(f"Error Count = {cnt}")
    cnt = int(cnt,0)
    for c in range(cnt) :
        print(f"Error code {c} = {bat.getError().strip()}")
        print(f"Error code {c} = {bat.getErrorCode().strip()}")

    print(f"Sense Function = {bat.getSenseFunction().strip()}")
    bat.setSenseFunction()
    print(f"Error      = {bat.getError().strip()}\n")
    bat.setEntryFunc()
    print(f"Error      = {bat.getError().strip()}")

    #new setup
    bat.setVsetVoltage(0.2)
    print(f"Error      = {bat.getError().strip()}")
    bat.setTestCurLimit(0.0)
    print(f"Error      = {bat.getError().strip()}")
    bat.setEndCurrent(0.0)
    print(f"Error      = {bat.getError().strip()}")
    bat.setOutputOn()
    print(f"Error      = {bat.getError().strip()}")
    x=bat.measureVI()
    print(f"Error      = {bat.getError().strip()}")
    print(f"VI         = {x}")
    bat.setOutputOff()
    print(f"Error      = {bat.getError().strip()}")
    exit()

    bat.setTestFullVolt(3.3)
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    bat.setTestCurLimit(0.5)
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    bat.setTestESRSamp("S30")
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    bat.setBattVolt(2.4)
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    bat.setEndCurrent(0.1)
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    bat.setOutputOn()
    print(f"Error      = {bat.getError().strip()}")
    #print(f"Error code = {bat.getErrorCode().strip()}")
    x=bat.measureVI()
    print(f"Error      = {bat.getError().strip()}")
    print(f"VI         = {x}")
    #print(f"Error code = {bat.getErrorCode().strip()}")

