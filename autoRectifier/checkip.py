from subprocess import check_output as co
import mac_vendor_lookup as mvl
import re

class checkip() :
    
    def __init__(self) :
        m=mvl.MacLookup()
        m.update_vendors()
        lst=co('arp -a', shell=True)
        lstlines = lst.decode('utf-8').split("\n")
        print(lstlines)
        for i, line in enumerate(lstlines) :
            llst = re.split('\s+', line)
            if len(llst) != 5 or "Inter" in llst[0] :
                continue
    
            try :
                lu = m.lookup(llst[2])
            except :
                pass
            else :
                print(f"{llst[1]} {lu}")
                pass
if __name__ ==  "__main__" :
    checkip()
