################################################################################
# © Ossia Inc. 2024
#
################################################################################

import pyvisa as visa
import sys
from time import time, sleep
from math import log10

VISA_ADDRESS = 'USB0::0x0AAD::0x00E2::101521::INSTR'
VISA_ADDRESS = 'USB0::0x2A8D::0x5418::MY63290005::INSTR'
def find() :

    searchString = '?*INSTR'
    resourceManager = visa.ResourceManager()

    print('Find with search string \'%s\':' % searchString)
    devices = resourceManager.list_resources(searchString)
    if len(devices) > 0:
        for device in devices:
            print('\t%s' % device)
    else:
        print('... didn\'t find anything!')

    resourceManager.close()

class PowerMeter() :
    def __init__(self, requestedResource=VISA_ADDRESS) :
        """ initialize the VISA session to the provided device
            Open the session and record the resources name
        """

        try:
            # Create a connection (session) to the instrument
            self.resourceManager = visa.ResourceManager()
            self. session = self.resourceManager.open_resource(requestedResource)
        except visa.Error as ex:
            print(f"Couldn't connect to '{requestedResource}', exiting now...")
            sys.exit()

        print(f"{self.session.resource_name}")
        self.resource_name = self.session.resource_name

    def sendRst(self) :
        """ send the RST command to the device to reset it to initial conditions
        """
        self.session.write('*RST')

    def getId(self) :
        """ send the *IDN? command to get the ID data for this device
            return the response
        """
        # Send *IDN? and read the response
        self.session.write('*IDN?')
        idn = self.session.read()
        return idn

    def getMeasurement(self, waittime=0.5) :
        """ send a request for a measurement and return the results
            An optional wait time can be entered. The time to wait is
            dependent on the measurement mode and settings.
        """
        self.session.write('INITiate:CONTinuous ON')
        self.session.write('FETCh?')
        sleep(waittime)
        val = -88.88
        try :
            val=self.session.read()
        except :
            pass
        return val

    def setUnits(self, units='dBm') :
        """ set the output data units. RST sets it to W
            This defaults to dBm
        """
        self.session.write(f"UNIT:POWer {units}")

    def setFrequencyMHz(self, freqmhz) :
        """ set the devices operational center frequency.
            requires frequency in MHz
        """
        if isinstance(freqmhz, str) :
            freq = float(freqmhz)

        elif isinstance(freqmhz, float) or isinstance(freqmhz, int) :
            freq = freqmhz
        freq *= 1000000.0 # convert to Hz
        ffreq = f"{freq:0.5g}"

        self.session.write(f"FREQuency {ffreq}")
        val = self.sendGetArbScpiString('FREQuency?')
        return val


    def deviceEnable(self, value) :
        """ set the enable of the device to value.
            If value is a string it will be expected to be ON/OFF
            If value is an int it will be expected to be 1/0
            returns the enable value after the set command
        """
        expvalue={'on' : 1, 'ON' : 1, "off" : 0, "OFF" : 0, "1" : 1, "0" : 0, 1 : 1, 0 : 0}
        outval  = expvalue.get(value, 0)
        self.session.write(f'STATus:DEVice:ENABle {outval}')
        val = self.sendGetArbScpiString('STATus:DEVice:ENABle?')
        return val

    def sendGetArbScpiString(self, scpiString) :
        """ Accept and send an arbitrary scpi command string valid for
            this device.
            Return any result from the action
        """
        val = None
        self.session.write(scpiString)
        try :
            val = self.session.read()
        except :
            pass
        return val

    def sendWait(self) :
        """ sends the *WAI scpi command to the device
        """
        self.session.write("*WAI")


# Close the connection to the instrument
    def closeSession(self) :
        """ close the visa session to the device
        """
        self.session.close()
        self.resourceManager.close()

    def setAverage(self, avgonoff='OFF', avgcnt=1) :
        """ set the averaging values of merit
            default is off and the cnt doesn't matter
            Averaging On the count can be set up to 65535
        """
        self.session.write(f"AVERage:COUNt {avgcnt}")
        self.session.write(f"AVERage {avgonoff}")

    def testmod(self) :
        pm = PowerMeter()
        idn = pm.getId()
        print('*IDN? returned: %s' % idn.rstrip('\n'))
        de = pm.deviceEnable("ON")
        print(f"device enable {de}")
        f=pm.setFrequencyMHz(5725)
        print(f"{f}")

if __name__ == "__main__" :
    find()
    pm = PowerMeter()
    idn = pm.getId()
    print('*IDN? returned: %s' % idn.rstrip('\n'))
    de = pm.deviceEnable("ON")
    print(f"device enable {de}")

    pm.sendRst()
    sleep(2)
    pm.setUnits()
    pm.setAverage() # take the defaults
    pm.deviceEnable("ON")
    sleep(0.5)
    f=pm.setFrequencyMHz(5725)
    print(f"{f}")
    sleep(0.5)
    val=pm.getMeasurement()
    print(f'val1={val}')
    val=pm.getMeasurement()
    print(f'val2={val}')

    pm.closeSession()
