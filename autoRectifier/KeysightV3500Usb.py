import serial
from serial.tools.list_ports import comports
from time import sleep, time
import platform
windoz = platform.system() == "Windows"

def getSerialPort(serialName, force=False) :
    """ find the open port and check to see if any match the
        below criteria.
        must equal serial name
    """
    serial_ports = comports()
    dev_serial = None
    for i, p in enumerate(serial_ports) :
        sserial = None
        print(f"manufacturer {p.manufacturer} -- p={p}\n")
        if p.manufacturer is None or p.manufacturer != "FTDI" : #or "V3500A" not in f"{p}" :
            continue
        thePort = f"/dev/{p.name}"
        if windoz :
            thePort = f"{p.name}"
        try :
            if thePort is None :
                raise serial.serialutil.PortNotOpenError
            sserial = serial.Serial(port=thePort, baudrate=9600, timeout=0.25)
        except Exception as ee :
            print(f"Exception in serial connection to {thePort} creating stub == {ee}")
        else :
            if not sserial.is_open :
                print(f"Could not open device {thePort}")
            else :
                dev_serial = sserial
            break
    return dev_serial

class KS_PwrMeterUsbSerial() :
    PowerMeterCommandsV3500 = {
    # 
    # This command resets the V3500A to its factory 
    # power-on state. 
    # Syntax *RST<LF> 
        "Reset" : "*RST\n",
        "RST"   : "*RST\n",
    # --
    # Command Name Non-triggered power read 
    # Description This command returns the present reading of the V3500A. 
    # Syntax PWR? <LF> 
        "GetPWR" : "PWR?\n",
        "GP"     : "PWR?\n",
    # --
    # Command Name Zero command 
    # Description This command zeroes the V3500A. 
    # Syntax ZERO<LF> 
        "ZeroPm" : "ZERO\n",
        "ZP"     : "ZERO\n",
    # --
    # Command Name Set frequency 
    # Description This command sets the operating frequency of the V3500A. 
    # Syntax FREQ<Value><LF> 
        "SetFREQ" : "FREQ{}\n", 
        "SF"      : "FREQ{}\n", 
    # --
    # This command returns the numeric serial number of the 
    # instrument. 
    # Syntax SN?<LF> 
        "GetSN" : "SN?\n",
        "GSN"   : "SN?\n",
    # --
    # Command Name Retrieve firmware revision 
    # Description This command returns the firmware revision. 
    # Syntax FWREV?<LF> 
        "Get FW Rev" : "FWREV?\n",
        "GFWR"       : "FWREV?\n",
    # --
    # This command checks for and returns the set relative offset 
    # value. 
    # Syntax RELVAL?<LF> 
        "GetPwrOffset" : "RELVAL?\n",
        "GPO"          : "RELVAL?\n",
    # --
    # Command Name Turn relative offset on 
    # Description This command sets the relative offset to ON. 
    # Syntax RELON<LF> 
        "SetUseOffsetOn" :  "RELON\n",
        "SUOON"          :  "RELON\n",
    # --
    # Command Name Set relative offset 
    # Description This command sets a relative offset value. 
    # Syntax SETREL<Value><LF> 
        "SetPwrOffset" : "SETREL{}\n", 
        "SPO"          : "SETREL{}\n", 
    # --
    # Command Name Turn relative offset off 
    # Description This command sets the relative offset to OFF. 
    # Syntax RELOFF<LF> 
        "SetUseOffsetOff" : "RELOFF\n",
        "SUOOFF"          : "RELOFF\n",
    # --
    # Command Name Check status of the relative offset 
    # This command checks the status of the relative offset. If it is 
    # Description ON, a “1” is returned, and if it is OFF, a “0” is returned. If it is 
    # in EDIT mode, a “2” is returned. 
    # Syntax REL?<LF> 
        "GetOffsetStatus" : "REL?\n",
        "GOS"             : "REL?\n",
    # Command Name dBm mode 
    # Description This command sets the V3500A to dBm mode. 
    # Syntax UDBM<LF> 
        "Set Units DBM" : "UDBM\n",
        "SUDBM"         : "UDBM\n",
    # Command Name watts mode 
    # Description This command sets the V3500A to watts mode. 
    # Syntax UMW<LF> 
        "Set Units mW" : "UMW\n",
        "SUMW"         : "UMW\n",
    }

    def __init__(self) :
        self.serialObj = getSerialPort("V3500A")
        self.cmd_dict = KS_PwrMeterUsbSerial.PowerMeterCommandsV3500
        self.sernum = self.getSN()
        self.setUdBm()
        self.model = "V3500A"
        self.temperature = "25.0"

    def makeCommandStringToSend(self, cmd, Value) :
        rtn = None
        if cmd is not None :
            cmd = self.cmd_dict.get(cmd, None)
            if cmd is not None :
                rtn = cmd.format(Value).encode('utf-8')
        return rtn
    def sendCommand(self, cmd, value='') :
        cmd = self.makeCommandStringToSend(cmd, value)
        self.serialObj.write(cmd)
        rtn = self.serialObj.readall().decode('utf-8').strip()
        return rtn

    def getPower(self) :
        return self.sendCommand("GP")
    def reset(self) :
        return self.sendCommand("RST")
    def setOffset(self, value) :
        self.sendCommand("SUOON")
        return self.sendCommand("SPO", value)
    def getSN(self) :
        return self.sendCommand("GSN")
    def getFwRev(self) :
        return self.sendCommand("GFWR")
    def setFreq(self, value) :
        return self.sendCommand("SF", value)
    def zeroDev(self) :
        return self.sendCommand("ZP")
    def setUmW(self) :
        return self.sendCommand("SUMW")
    def setUdBm(self) :
        return self.sendCommand("SUDBM")

if __name__ == "__main__" :
    obj = KS_PwrMeterUsbSerial()
    print(f"get SN        {obj.getSN()}")
    print(f"get FW REV    {obj.getFwRev()}")
    print(f"set Freq Str  {obj.setFreq('5760')}")
    print(f"set Freq Int  {obj.setFreq(5780)}")
    print(f"get Power     {obj.getPower()}")
    print(f"set Units mW  {obj.setUmW()}")
    print(f"get Power     {obj.getPower()}")
    print(f"set Units dBm {obj.setUdBm()}")
    print(f"get Power     {obj.getPower()}")

