import BatterySimulatorVisa as batt
#import KeysightV3500Usb as pm
# this is a Keysight PM but using the same code as he Rhoads and schwarts
# both connect as VISA items
import RS_PwrMtr_vcom as pm
#import MCUsbSG as siggen
import MCUsbDllSG  as siggen
from time import sleep, time
from optparse import OptionParser

def printDbg(msg) :
    if AutoRectifier.debug :
        print(f"DEBUG Msg == '{msg}'")

# the setup and loop for the Battery Simulator (BS) (power analyzer)
class AutoRectifier() :
    debug = False
    def __init__(self, debug=False) :
        self.batt = None
        self.batt = batt.BatterySimVisa()
        self.batt.setOutputOff()
        self.pwrmeter = pm.PowerMeter()
        self.siggen = siggen.MiniCircuits()
        self.siggen.setOutputOff()
        AutoRectifier.debug = debug
        self.batt.testmod()
        self.pwrmeter.testmod()
        self.siggen.testmod()

    def main(self, freqMHz) :
        # Send *RST to Batt
        printDbg("reset batt sim")
        self.batt.reset()
        # Send *RST to Power Meter
        printDbg("reset power meter")
        self.pwrmeter.reset()
        # set offset of PM to 20 dB -- make CLI prog.
        printDbg("set power meter offset to 20")
        self.pwrmeter.setOffset(20)
        # Set BS to Battery Test  :ENTRy1:FUNCtion TEST
        printDbg("set the batt sim to TEST function")
        self.batt.setFuncAsTest()
        siggen_pwr = 0.0
        # the op freq will become programmable from the cmd line
        printDbg(f"set the sig gen to output frequency {freqMHz}")
        self.siggen.setFrequency(freqMHz)
        printDbg(f"set the pwr meter to input frequency {freqMHz}")
        self.pwrmeter.setfreq(freqMhz)
        printDbg("Turn sig gen ON")
        self.siggen.setOutputOn()

        # measure Power Meter value PMVAL = 0.0
        PMVAL = self.pwrmeter.getPower()
        printDbg(f"got power meter value -- {PMVAL}")
        printDbg("Loop to get the power meter value to 0.0 dBm")
        exit()
        while True :
            if PMVAL <= 0.5 and PMVAL >= -0.5 : # somewhere close to 0.0 dBm
                break
            siggen_pwr += 0.1 # may have to make this self adjustable
            # adjust signal source to achieve.
            self.siggen.setPowerLevel(siggen_pwr)
            sleep(0.5)
            PMVAL = self.pwrmeter.getPower()
            printDbg(f"new power meter value -- {PMVAL} with sig get power set to {siggen_pwr}")

        printDbg("loop to setting sig gen power higher until the Power meter is over 25 dBm ")
        while PMVAL < 25.0 :
            CURVolt_Set = 5.0
            # Set BS V-set to 5V      :BATT:TEST:VOLT CURVolt_Set 
            self.batt.setBatVolt(CURVolt_Set)

            #Set BS End Current to 0 :BATT:TEST:CURR:END 0.0 
            self.batt.setEndCurrent(0.0)

            #Set BS Output on        :BATT:OUTP ON 
            self.batt.setOutputOn()
            #Measure V and I         :MEASure:CONcurrent? 
            V, I = self.batt.measureVandI()
            LAST_VI = V * I
            CUR_VI = LAST_VI + 1.0
            LV = V
            LI = I
            cnt = 1
            printDbg(f"Find max power at power meter while decreasing the bat voltage from 5V down")
            while LAST_VI < CUR_VI : # this is looking for a decreasing value. The expected is an increasing one
                printDbg(f"CNT={cnt}  last_vi {LAST_VI} cur_vi {CUR_VI} lv {LV} li {LI} bat volt {CURVolt_Set}")
                CURVolt_Set -= 0.1
                #Set BS V-set to CURVolt-Set   :BATT:TEST:VOLT CURVolt-Set 
                self.batt.setBatVolt(CURVolt_Set)
                LAST_VI = CUR_VI
                LV = V
                LI = I
                #Measure V and I :MEASure:CONcurrent? 
                sleep(0.5)
                V, I = self.batt.measureVandI()
                CUR_VI = V * I
                cnt += 1
            #PRINT LAST_VI, LV, LI, PMVAL
            print(f"Power {LAST_VI} Voltage {LV} Current {LI} Power Meter {PMVAL}")
            # Set signal source output 1 dB higher.
            siggen_pwr += 1.0
            self.siggen.setPowerLevel(siggen_pwr)
            printDbg(f"new sig gen power {siggen_pwr} loop again")
            #measure Power Meter value PMVAL
            sleep(0.5)
            PMVAL = self.pwrmeter.getPower()

if __name__ == "__main__" :
    parser = OptionParser()

    parser.add_option("-f","--freq", dest='freq',     action='store',     default='5750',          help="Current testing frequency def='%default'")
    #parser.add_option("-i","--ipsg", dest='ipsgaddr', action='store',     default='localhost',     help="SigGen Network Addrs. def='%default'")
    #parser.add_option("-I","--ipbs", dest='ipbsaddr', action='store',     default='localhost',     help="BatSim Network Addrs. def='%default'")
    #parser.add_option("-P","--pm",   dest='pwrmtr',   action='store',     default=pm.VISA_ADDRESS, help="Power Meter visa address. def='%default'")
    parser.add_option("-F","--find", dest='finddev',  action='store_true',default=False,           help="Show listing ow possiable devices. def='%default'")
    parser.add_option("-d","--debug",dest='debug',    action="store_true",default=False,           help="Print debug info. def='%default'")

    (options, args) = parser.parse_args()

    if options.finddev :
        pm.find()
        import checkip 
        checkip.checkip()
        exit()

    ar = AutoRectifier(debug=options.debug)
    ar.main(options.freq)
