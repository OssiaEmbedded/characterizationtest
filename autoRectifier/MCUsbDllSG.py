# Copyright Ossia Inc. 2024
""" This module uses the DLL from Mini-Circuts to communicate to a
Signal Generator over USB.
"""
try :
    import clr
except Exception as e :
    print(f"{e}")
    print("""clr is not installed so no mcl_gen64.dll usage is available.
Get clr from installing pythonnet and the dlls from Mini-circuits.
Put the dlls in the runtime directory of pythonnet.""")
    exit()
else :
    try :
        clr.AddReference("mcl_gen64")
        from mcl_gen64 import usb_gen
    except Exception as e :
        print(f"{e}")
        exit()
import sys
from time import sleep

class MiniCircuits() :
    mcSignalGenCmds = {
                        "Get Model" : ":MN?",
                        "Get Serial Number" : ":SN?",
                        "Get Mac Address" : ":MAC?",
                        "Set Frequency" : ":FREQ:{}MHz",
                        "Get Frequency" : ":FREQ?",
                        "Set Power Level" : ":PWR:{}",
                        "Get Power Level" : ":PWR?",
                        "RF Output OnOff" : ":PWR:RF:{}",
                        "Get RF Output State:" : ":PWR:RF?",
                        "Get On Off Count" : ":ONOFFCOUNTER?",
                        "Get Power On Time" : ":OPERATIONTIME?",
                        "Get FW Version" : ":FIRMWARE?", # last two characters
                        "Power Sweep Start" : ":PSWEEP:STARTPOWER:{}",
                        "Get Power Sweep Start" : ":PSWEEP:STARTPOWER?",
                        "Set Power Sweep Frequency" : ":PSWEEP:FREQ:{}",
                        "Get Power Sweep Frequency" : ":PSWEEP:FREQ?",
                        "Set Power Sweep Dwell" : ":PSWEEP:DWELL:{}",
                        "Get Power Sweep Dwell" : ":PSWEEP:DWELL?",
                        "Set Power Sweep Direction" : ":PSWEEP:DIRECTION:{}",
                        "Get Power Sweep Direction" : ":PSWEEP:DIRECTION?",
                        "Set Power Sweep Mode" : ":PSWEEP:MODE:{}",
                        # to use these commands, simply uncomment them
                        # give them an appropriate name and then create a
                        # class method and name it appropriately for each scpi command
                        # or you can create a single method and pass in the dict name
                        #"SerNum" : ":PSWEEP:MINDWELL",
                        #"SerNum" : ":FREQ:MAX",
                        #"SerNum" : ":PWR:MAX",
                        #"SerNum" : ":FSWEEP:STARTFREQ:{}",
                        #"SerNum" : ":FSWEEP:STARTFREQ?",
                        #"SerNum" : ":FSWEEP:POWER:{}",
                        #"SerNum" : ":FSWEEP:POWER?",
                        #"SerNum" : ":FSWEEP:DWELL:{}",
                        #"SerNum" : ":FSWEEP:DWELL?",
                        #"SerNum" : ":FSWEEP:DIRECTION:{}",
                        #"SerNum" : ":FSWEEP:DIRECTION?",
                        #"SerNum" : ":FSWEEP:TRIGGERIN:{}",
                        #"SerNum" : ":FSWEEP:TRIGGEROUT:{}",
                        #"SerNum" : ":FSWEEP:TRIGGERIN?",
                        #"SerNum" : ":FSWEEP:TRIGGEROUT?",
                        #"SerNum" : ":FSWEEP:MODE:O",
                        #"SerNum" : ":PSWEEP:TRIGGERIN:",
                        #"SerNum" : ":PSWEEP:TRIGGEROUT:",
                        #"SerNum" : ":PSWEEP:TRIGGERIN",
                        #"SerNum" : ":PSWEEP:TRIGGEROUT",
                        #"SerNum" : ":HOP:POINTS:5",
                        #"SerNum" : ":HOP:POINTS",
                        #"SerNum" : ":HOP:MAXPOINTS",
                        #"SerNum" : ":HOP:POINT:",
                        #"SerNum" : ":HOP:POINT",
                        #"SerNum" : ":HOP:FREQ:1000.",
                        #"SerNum" : ":HOP:FREQ",
                        #"SerNum" : ":HOP:POWER:1",
                        #"SerNum" : ":HOP:POWER",
                        #"SerNum" : ":HOP:DWELL:10",
                        #"SerNum" : ":HOP:DWELL",
                        #"SerNum" : ":HOP:MINDWELL",
                        #"SerNum" : ":HOP:DIRECTION:",
                        #"SerNum" : ":HOP:DIRECTION",
                        #"SerNum" : ":HOP:TRIGGERIN:",
                        #"SerNum" : ":HOP:TRIGGEROUT:",
                        #"SerNum" : ":HOP:TRIGGERIN",
                        #"SerNum" : ":HOP:TRIGGEROUT",
                        #"SerNum" : ":HOP:MODE:O",
                        #"SerNum" : ":PULSE:TIMEON:1",
                        #"SerNum" : ":PULSE:TIMEUNITS=USE",
                        #"SerNum" : ":PULSE:MODE:FREERUN:O",
                        #"SerNum" : ":DFS:NoOfPulses:5",
                        #"SerNum" : ":DFS:NoOfPulses",
                        #"SerNum" : ":DFS:Pulse_IDX:",
                        #"SerNum" : ":DFS:Pulse_IDX",
                        #"SerNum" : ":DFS:RF:FREQ:1000.",
                        #"SerNum" : ":DFS:RF:FREQ",
                        #"SerNum" : ":DFS:RF:POWER:1",
                        #"SerNum" : ":DFS:RF:POWER",
                        #"SerNum" : ":DFS:PulseWidth:10",
                        #"SerNum" : ":DFS:PulseWidth",
                        #"SerNum" : ":DFS:Interval:20",
                        #"SerNum" : ":DFS:Interval",
                        #"SerNum" : ":DFS:NoOfCycles:10",
                        #"SerNum" : ":DFS:NoOfCycles",
                        #"SerNum" : ":DFS:CONT:",
                        #"SerNum" : ":DFS:CONT",
                        #"SerNum" : ":DFS:MODE:O",
                        }
    def __init__(self, ipaddr=None, port=23) :
        """Connect to the USB sig gen and set it up"""
        self.ssg = usb_gen()
        if not self.ssg.Connect() : # Connect the sig gen (pass the SN if need, more than one Siggen attached)
            # if the return was zero then the SIG gen wasn't found
            raise FileNotFoundError("MiniCircuits SigGen not found!")
        self.connected = True
        #self.reset()
        self.model = self.getModel()
        self.sernum = self.getSerialNum()
        self.setOutputOff()

    def __del__(self) :
        self.closeConnection()

    def closeConnection(self) :
        if self.connected :
            self.ssg.Disconnect()
        self.connected = False

    def sendCommand(self, cmdString, value=None) :
        val = ''
        fullCmdString = None
        if cmdString :
            val = self.ssg.SCPI_Query(cmdString, "")
            if isinstance(val, tuple) and len(val) >= 3 :
                val = val[2]
            else :
                val = val[1]
        return val
    
    def getModel(self) :
        """ returns the model number of the device.
        """
        val = self.sendCommand(":MN?")
        return val
    def getSerialNum(self) :
        """ returns the serial number of the device.
        """
        val = self.sendCommand(":SN?")
        return val
    def getMacAdr(self) :
        """ returns the mac address number of the device.
        """
        val = self.sendCommand(":MAC?")
        return val
    def setFrequency(self, freq, units='Mhz') :
        """ sets the output frequency to 'freq'.
        """
        self.sendCommand(f":FREQ:{freq}{units}")
    def getFrequency(self) :
        """ returns the current output frequency.
        """
        val = self.sendCommand(":FREQ?")
        return val
    def setPowerLevel(self, pwrdbm) :
        """ set the output power level in dBm.
        """
        self.sendCommand(f":PWR:{pwrdbm}")
    def getPowerLevel(self) :
        """ return the output power level.
        """
        val = self.sendCommand(":PWR?")
        return val
    def setOutputOn(self) :
        """ enable the output to ON at the current frequency
            and power level.
        """
        self.sendCommand(":PWR:RF:ON")
    def setOutputOff(self) :
        """ disable the output to OFF without modifying frequency or 
            output power.
        """
        self.sendCommand(":PWR:RF:OFF")
    def getOutputState(self) :
        """ returns the output state on or off
        """
        val = self.sendCommand(":PWR:RF?")
        return val
    def testmod(self) :
        mc = MiniCircuits()
        print(f"MODEL      {mc.getModel()}")
        print(f"SERIAL NUM {mc.getSerialNum()}")
        freq = 5.6e9
        mc.setFrequency(freq, 'Hz')
        val = mc.getFrequency()
        print(f"\n{freq}MHz {val}")
        freq = 5.5e7
        mc.setFrequency(freq, 'Hz')
        val = mc.getFrequency()
        print(f"\n{freq}MHz {val}")
        freq = 5650
        mc.setFrequency(freq, 'MHz')
        val = mc.getFrequency()
        print(f"\n{freq}MHz {val}")

if __name__ == "__main__" :

    mc = MiniCircuits()
    print(f"MODEL      {mc.getModel()}")
    print(f"SERIAL NUM {mc.getSerialNum()}")
    cmdNameList = list(mc.mcSignalGenCmds.keys())
    for n in cmdNameList :
        if n.startswith('Get') :
            val = mc.sendCommand(mc.mcSignalGenCmds[n])
            print(f"{n} = {val}")

    for freq in [5700, 5750, 5800, 5850] :
        mc.setFrequency(f"{freq}MHz")
        val = mc.getFrequency()
        print(f"\n{freq}MHz {val}")
    freq = 5.6e9
    mc.setFrequency(freq, 'Hz')
    val = mc.getFrequency()
    print(f"\n{freq}MHz {val}")
    freq = 5.6e7
    mc.setFrequency(freq, 'Hz')
    val = mc.getFrequency()
    print(f"\n{freq}MHz {val}")
    pwrdbm = -13.5
    mc.setPowerLevel(pwrdbm)
    print(f"PwrLevel= {mc.getPowerLevel()}")
    pwrdbm = 0.0
    mc.setPowerLevel(pwrdbm)
    print(f"PwrLevel= {mc.getPowerLevel()}")

    mc.closeConnection()
