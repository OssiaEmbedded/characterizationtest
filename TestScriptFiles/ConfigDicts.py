# each of the entries in Config is the basename without extension of the script file name.
Config = {'TestCharacterizeExample' : {'testList' :'cEtestList', 'spec_ana' : "hostname", 'loops' : 10,},
          'TestAnotherTest1' : {},
          'TestAnotherTest2' : {},
         }

# this name is up to you but must appear in the above Config list
# content is TestScript dependent.
cEtestList = [
                {'linearpos' : 1000, 'rotarypos' : 0.0,   'dwelltime' : 11},
                {'linearpos' : 1100, 'rotarypos' : 90.0,  'dwelltime' : 10},
                {'linearpos' : 1200, 'rotarypos' : 45.0,  'dwelltime' : 10},
                {'linearpos' : 1300, 'rotarypos' : 0.0,   'dwelltime' : 10},
                {'linearpos' : 1400, 'rotarypos' : -45.0, 'dwelltime' : 10},
                {'linearpos' : 1500, 'rotarypos' : -90.0, 'dwelltime' : 10},
            ]
