
TestInstructions = """
This is a test list example. Its name is just the prefix for any test lists.
The name for a real test will have some descriptive information in the TestList name.
"""

testlist=[
           {"linposition" : 500, "rotposition" : 45.0, 'dwelltime' : 2,}, # 'enablepwr' : True},
           {"linposition" : 600, "rotposition" : 90.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
           {"linposition" : 700, "rotposition" : 45.0, 'dwelltime' : 2},
           {"linposition" : 800, "rotposition" :  0.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
           {"linposition" : 900, "rotposition" : -45.0, 'dwelltime' : 2},

           {"linposition" : 500, "rotposition" : -90.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
           {"linposition" : 600, "rotposition" : -45.0, 'dwelltime' : 2},
           {"linposition" : 700, "rotposition" :   0.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
           {"linposition" : 800, "rotposition" : -45.0, 'dwelltime' : 2},
           {"linposition" : 900, "rotposition" : -90.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
           {"linposition" :   0, "rotposition" :   0.0, 'dwelltime' : 2,}, # 'measurepwr' : True},
         ]

