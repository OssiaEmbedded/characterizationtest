#import the modules needed for this test
import sys
from time import sleep

TestInstructions="""
This is an example test file. It will simply update the output display box with 600 lines of data.
A real test would explain the test and state any specific setup or test steps necessary to complete this test.
"""

class TestClass() :
    # required class name for now
    def __init__(self, outputStringUpdater, actObj, TxIp, RxId) :
        self.printStr = outputStringUpdater

    def main(self) :
        # just test the feedback to the gui
        self.printStr("All things great and small")
        self.printStr("</red/>All things great and small/>")
        # text formatting 'bold' and 'italic' are the choices 
        # text colors are 'red', 'blue', 'green' etc.
        for i in range(20) :
            if (i%6) == 0 :
                self.printStr("0 Index </bold/>{}/> not bold".format(i))
            elif (i%6) == 1 :
                self.printStr("1 Index {} </red/>not/> bold".format(i))
            elif (i%6) == 2 :
                self.printStr("2 Index {} </bold,red/>not/> bold".format(i))
            elif (i%6) == 3 :
                self.printStr("3 Index {} </bold,red,italic/>not/> bold".format(i))
            elif (i%6) == 4 :
                self.printStr("4 Index {} </bold,green/>not/> bold".format(i))
            elif (i%6) == 5 :
                self.printStr("5 Index {} </bold,green/>not/> blue </bold,blue/>bold/>".format(i))
        del(self)


        


