
TestInstructions = """
This is a test list is the main test list for characterizing Mars TXs
It has been adjusted for the current environment and need.
There are two receiver values that may not always be required.
The "transtxpwr" and the "trandatarate".

You can simple comment those out with a # char as the first character in the line.
"""

testlist=[
        {"setuptxrx"   : True}, # should always be the first line of any TestList
        {"registerrx"  : True},
        {"rxcommand"   : "trantxpwr 15"},
        #{"rxcommand"   : "trandatarate 2"},
        {"rxcommand"   : "reset"}, # 
        {"dwelltime"   : 7}, # 
        #{"measurerxbat"   : True}, # 

        {"linposition" : 1000, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" : -60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" : -45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" : -30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" : -15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" :   0.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" :  15.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" :  30.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" :  45.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" : 1000, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1200, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1400, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1600, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 1800, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},
        {"linposition" : 2000, "rotposition" :  60.0, 'dwelltime' : 1, 'fastloop' : 3, "measurerxbat" : True},

        {"linposition" :    0, "rotposition" :   0.0, 'dwelltime' : 1},

        {"unregisterrx"  : True},
        {"restoretx"     : True},
      ]

