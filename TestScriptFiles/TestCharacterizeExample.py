r"""
THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2022 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
"""

# the ConfigDicts file will contain two dictionaries. testList and configList
# The testList dict provides control information for each iteration/step/loop of the 
# test script (this file) 
# The configList has opening information about the environment. What test equipment to us
# where to find it e.g. network address and modules needed to support it. Optionally the support modules can be 
# imported here rather than be specified in the configList.
import ConfigDicts as CD

# add in any imports required for the script to be operational
import sys
import os
from time import sleep

TestInstructions="""
This is an </bold,green/>example/> test file. It will setup and control the Linear an rotary actuators (not currently implemented due to Sharing conflicts).  It requires a </bold/>testList/> to be identified in the ConfigDicts file.

There are two requirements in this file, the character constant </bold/>TestInstructions/>, and the class name </bold/>TestClass/>, you've already got the file name convention correct!!

This example defines and uses a </bold/>testList/> but a test script is not constrained to this. It can execute what ever code it needs to the only constraints are those listed above.

The </bold/>testList/> contains the control steps of the test. It will/can contain the data needed for each step. e.g.  Positions, Counts, Limits

The </bold/>testList/> is a List of Dictionaries, where each entry in the List defines a step or iteration. The dictionary contained in that list entry will have named input/control parameters.

"""

class TestClass() :
    # required class name for now
    def __init__(self, outputStringUpdater, ActObj, TxIP, RxID) :
        self.printStr = outputStringUpdater
        # lets get the test list name from the config data dictionary
        self.actuator = ActObj
        self.txIp = TxIP
        self.rxId = RxID

        # ConfigList is organized by script basename e.g. characterizeExample
        # so get the base name
        filename = os.path.basename(__file__)
        fname = os.path.splitext(filename)[0] # just the base not the extension.
        testListName = CD.Config[fname].get('testList', None) # testList name and data is defined by you for your test
        self.printStr("testListName = {}".format(testListName)) # print it out in this example
        self.testList = eval("CD."+testListName) # this is python way to get the testList from the file.
        self.SAPwrMeter = CD.Config[fname].get("spec_ana", None)
        self.printStr("spec_ana = {}".format(self.SAPwrMeter)) # print it out in this example
        self.printStr("#####################################\n") # print it out in this example
        self.loops = CD.Config[fname].get('loops', 1)

    def main(self) :
        linpos = self.actuator.getLinPosition()
        rotpos = self.actuator.getRotPosition()
        self.printStr("Linear position is </red/>{}".format(linpos)) # red to the delimiter or the end of line
        self.printStr("Rotary position is </green/>{}/> not green".format(rotpos))
        self.printStr("TX IP is </blue/>{}/>".format(self.txIp))
        self.printStr("RX ID is </red/>{}/>".format(self.rxId))
        self.printStr("Loops is </green/>{}/>".format(self.loops))
        while self.loops > 0 :
            for testDict in self.testList :
                self.loops -= 1
                self.printStr("testDict = {}".format(repr(testDict))) # print it out in this example
                self.actuator.sendPosLin(testDict['linearpos'])
                self.printStr("Linear Position = {}".format(testDict['linearpos'])) # print it out in this example
                self.actuator.sendPosRot(testDict['rotarypos'])
                self.printStr("Rotary Position = {}".format(testDict['rotarypos'])) # print it out in this example
                sleep(testDict['dwelltime'])
                if self.actuator.getStop() :
                    self.printStr("Stopping script loop {}".format(self.loops))
                    self.loops = 0
                    break

