import sys
import os
import re
import glob
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
from optparse import OptionParser
import screeninfo
import platform
import PySimpleGUI as sg

FigureNum = 0
def figureNum(f=None) :
    """ return a number that increments on each call
    """
    global FigureNum
    if f is not None :
        FigureNum = 0
    else :
        FigureNum += 1
    return FigureNum


DIST=1
ANGLE=2
AVG=4
BEACONSRX = "Beacons Received"

def readBatStatsFromLog(filename) :
    """ create a dictionary of named data of the filtered lines
        top name is distance, then angle then the named bat stats data 
        {
          dist1 : {angle1 : {"name1" :data,"name2" : data, etc}, angle2 : {"name1" : data, "name2" : data, etc}},
          dist2 : {angle1 : {"name1" :data,"name2" : data, etc}, angle2 : {"name1" : data, "name2" : data, etc}},
        }
    """
    NODATALST = "Battery Voltage,2222 mV,Battery Ave Current,-222 uA,Battery SOC,-78 %,FUEL_ALRT pin,De-asserted,Temp TH Low,-10 deg C,Temp TH High,-40 deg C,Status,0x0000,Config,0xA210,Config2,0x0010,Enter selection".split(',')
    filterStr='RXBATTERYSTAT'
    retDict = None
    fd = open(filename, 'r')
    if fd :
        retDict = {}
        line = "a"
        while line :
            line = fd.readline()
            if line.find(filterStr) > 0 :
                linelst = line.strip().split(',')
                lstlen = len(linelst)
                if lstlen <= 5 :
                    linelst[:4] += NODATALST
                if linelst[DIST].lower() == 'none' or linelst[ANGLE].lower() == 'none' : 
                    continue
                for i,l in enumerate(linelst) :
                    try :
                        if '.' in l :
                            linelst[i] = float(l)
                        else :
                            linelst[i] = int(l)
                    except :
                        pass
                if linelst[DIST] not in retDict :
                    retDict.update({linelst[DIST] : {}})
                tmpDict = {}
                lastV = 'none'
                for i, v in enumerate(linelst[AVG:]) :
                    if (i % 2) > 0 :
                        tmplst = v.split(' ', 1)
                        for j,w in enumerate(tmplst) :
                            try :
                                if '.' in w :
                                    tmplst[j] = float(w)
                                else :
                                    tmplst[j] = int(w)
                            except :
                                pass
                        tmpDict.update({lastV : tmplst})
                    lastV = v
                retDict[linelst[DIST]].update({linelst[ANGLE] : tmpDict})
        fd.close()
    return retDict

DEFAULT_DESIGNATED_DATA = {
'ANTENNATYPE' : '“B”,6.6 dBi*,Retired',
'OPFREQUENCY' : '5751',
'OS Version'  : "",
'SW Version'  : "",
'Build ID'    : "",
}
def readDesignatedDataFromLog(filename, designated=['ANTENNATYPE','OPFREQUENCY','OS Version','SW Version','Build ID']) :
    """ reads the line from the file with the below string. 
    """
    fd = open(filename, 'r')
    break1 = break2 = False
    outDict = DEFAULT_DESIGNATED_DATA
    if fd :
        for findData in designated :
            fd.seek(0)
            line = 'a'
            while line :
                line = fd.readline()
                if line.find(findData) >= 0 :
                    data = line.strip().split('=')[1].strip()
                    if "ANTENNA" in findData : # special processing for Antenna
                        data = ' '.join(data.split(',',3)[:3])
                    if not findData in outDict :
                        outDict.update({findData : data})
                    else :
                        outDict[findData] = data
                    break
        fd.close()
    return f"{outDict['ANTENNATYPE']} @{outDict['OPFREQUENCY']}", outDict

def readPowerDataFromLog(filename) :
    """ create a dictionary of named data of the filtered lines
        top name is distance, then angle the power data
        {
          dist1 : {angle1 : data, angle2 : data, etc..},
          dist2 : {angle1 : data, angle2 : data, etc..},
        }
    """
    filterStr='POWERDATA'
    retDict = None
    beaconDict = None
    fd = open(filename, 'r')
    if fd :
        retDict = {}
        beaconDict = {}
        line = "a"
        while line :
            line = fd.readline()
            if line.find(filterStr) > 0 :
                linelst = line.strip().split(',')
                for i,l in enumerate(linelst) :
                    try :
                        if '.' in l :
                            linelst[i] = float(l)
                        else :
                            linelst[i] = int(l)
                    except Exception :
                        linelst[i] = 35.0
                if linelst[DIST] not in retDict :
                    retDict.update({linelst[DIST] : {}})
                    beaconDict.update({linelst[DIST] : {}})
                retDict[linelst[DIST]].update({linelst[ANGLE] : linelst[AVG:]+['dBm']})
                beaconDict[linelst[DIST]].update({linelst[ANGLE] : [len(linelst[AVG:])-1, "Beacons"]})
        fd.close()
    return retDict, beaconDict

def plotData(ValDict, plotTitle='Power Data', datakey=None, titlestr=None, plt3d=True, A=None) :
    """ default is Power data.
        Creates a plot of the data contained in ValDict, ValDict hast dist, angle and named data
    """
    colorlist = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    yticks = list(ValDict.keys())
    fig = plt.figure(num=titlestr)

    if plt3d :
        ax = fig.add_subplot(projection='3d')
    else :
        ax = fig.add_subplot()
    units = ' None'
    loc = 50.0
    for  ii, k in enumerate(yticks) :
        colr = colorlist[ii%len(colorlist)]
        klst = list(ValDict[k].keys())
        vlst = []
        units = ''
        for kl in klst :
            if datakey is None :
                vlst.append(ValDict[k][kl][0])
                units = " "+ValDict[k][kl][-1]
            else :
                try :
                    valStr = ValDict[k][kl].get(datakey, [0,'data'])[0]
                except KeyError as kkk :
                    print(f"==== {k} {kl} {datakey} ||| {ValDict[k]} +++ {kkk}")
                    sys.exit()
                if isinstance(valStr, str) and valStr.startswith('0x') :
                    valStr = int(valStr, 0)
                vlst.append(valStr)
                if len(ValDict[k][kl].get(datakey, '')) > 1 :
                    units = " "+ValDict[k][kl][datakey][-1]
                else :
                    units = " Value"
        xs = np.array(klst)
        ys = np.array(vlst)
        if plt3d :
            ax.plot(xs, ys, zs=k, zdir='y', color=colr, alpha=1.0)
        else :
            ax.plot(xs, ys,  color=colr, alpha=1.0)
            ax.annotate(f'{k}', xy=(loc, vlst[-1]))
    ax.set_xlabel('Angle Deg')
    if plt3d :
        ax.set_ylabel('Distance mm')
        ax.set_zlabel(units)
        ax.set_yticks(yticks)
        ax.text2D(0.10, 0.85, f"Ant:{A}", transform=ax.transAxes)
    else :
        ax.set_ylabel(units)
        ax.text(0.0,0.95, f"Ant:{A}", transform=ax.transAxes)
    # On the y-axis let's only label the discrete values that we have data for.
    ax.set_title(plotTitle)
    return plt.get_current_fig_manager() 

def createNextWindowLocation(locationIn, win_loc=None, win_index=-1) :
    """ takes a window location and creates a new one that's just to the right of it.
        XxY+x+y where X is the horizontal size of the window, Y is the vertical size 
        Positive x is the distance from the left edge of the window to the left edge of the screen
        Negative x is the distance from the right edge of the window to the right edge of the screen
        Positive y is the distance from the top edge of the window to the top edge of the screen
        Negative y is the distance from the bottom edge of the window to the bottom edge of the screen
    """
    lst = re.split(r"\+|x", locationIn)
    for i, v in enumerate(lst) :
        lst[i] = int(v)
    w, h, idx = (0, 0, 0)
    if win_loc is not None :
        w, h, idx = win_loc
    plotWidth=int(screeninfo.get_monitors()[idx].width/2.1)
    plotHeight=int(screeninfo.get_monitors()[idx].height/2.1)

    lst[0] = plotWidth
    lst[1] = plotHeight
    nX = lst[0] + lst[2] + 5 
    nY = lst[1] + lst[3] + 5 
    if win_index == 1 :
        newLoc = f"{lst[0]}x{lst[1]}+{nX}+{lst[3]}"
    elif win_index == 2 :
        newLoc = f"{lst[0]}x{lst[1]}+{lst[2]}+{nY}"
    elif win_index == 3 :
        newLoc = f"{lst[0]}x{lst[1]}+{nX}+{nY}"
    else :
        newLoc = f"{lst[0]}x{lst[1]}+{5+w}+{5+h}"
    return  newLoc

def plotLogFile(filename, pwrdata, win_curloc=None, batstatLst=[None], dataonly=False, plot3d=True) :
    """ get the data from the file plot it and place the plots
    """
    aNT, desigDict = readDesignatedDataFromLog(filename)
    valDict, beaconDict = readPowerDataFromLog(filename)
    Namelst, ValDictStats = getBatStatListAndData(filename)
    fname = os.path.splitext(os.path.basename(filename))[0]
    mlst = []
    figureNum(0)
    if not dataonly :
        try :
            if pwrdata :
                mlst.append(plotData(valDict, titlestr=f"{fname} Fig. {figureNum()}", plt3d=plot3d, A=aNT))
        
            if batstatLst[0] :
                for bstat in batstatLst :
                    if bstat in Namelst :
                        if bstat == BEACONSRX :
                            mlst.append(plotData(beaconDict, BEACONSRX, None, titlestr=f"{fname} Fig. {figureNum()}", plt3d=plot3d, A=aNT))
                        else :
                            mlst.append(plotData(ValDictStats, bstat, bstat, titlestr=f"{fname} Fig. {figureNum()}", plt3d=plot3d, A=aNT))
            if (pwrdata or batstatLst[0]) :
                plt.draw()
                plt.pause(0.02)
            firstLoc = mlst[0].window.wm_geometry()
            win_curloc = (0,0,0)
            firstLoc = createNextWindowLocation(firstLoc, win_curloc)
            if mlst :
                for i, m in enumerate(mlst) :
                    nextGeometry=createNextWindowLocation(firstLoc, win_curloc, win_index=i)
                    m.window.wm_geometry(newGeometry=nextGeometry)
        except :
            pass
    try :
        batstatLst.pop(batstatLst.index(BEACONSRX))
    except :
        pass
    if len(batstatLst) > 0 :
        retdic =  {'Avg Power' : valDict, BEACONSRX : beaconDict, "Stat data" : ValDictStats, "Antenna" : aNT}
    else :
        retdic =  {'Avg Power' : valDict, BEACONSRX : beaconDict, "Antenna" : aNT}
    return retdic, desigDict 

def pauseToShow() :
    input("\nCR to finish")

def getLogFilenames(directoryname) :
    listdir =  glob.glob(directoryname+"*.log")
    listdir.sort(key=os.path.getmtime, reverse=True)
    for i, v in enumerate(listdir) :
        listdir[i] = os.path.basename(v)
    return listdir

def getBatStatListAndData(filename) :
    namelst = None
    ValDictStats = None
    try :
        ValDictStats = readBatStatsFromLog(filename)
    except Exception :
        pass
    else :
        try :
            kd = list(ValDictStats.keys())[0]
            ka = list(ValDictStats[kd].keys())[0]
            namelst = list(ValDictStats[kd][ka].keys())
            namelst.append(BEACONSRX)
        except Exception :
            pass
    return namelst, ValDictStats

def closeLogs(whatstr) :
    plt.close(whatstr)

def printDataOnly(ValDict, bstatname=[None]) :
    """ print the data in the dictionary. It is a nested Dict.
        For each angle there is a set of distances
    """
    for datk, dataValues in ValDict.items() : #dataValues, beaconsValues, namelst, statsValues
        if isinstance(dataValues, str) :
            print(f"{datk}\n{dataValues}")
            continue
        print(f"{datk}")

        try :
            distKeys = list(dataValues.keys())
        except Exception :
            continue
        angleKeys = list(dataValues[distKeys[0]].keys())
        print(f"{distKeys}\n\n{angleKeys}")
        for ai,ak in enumerate(angleKeys) :
            header = ''
            data = f'{ak}'
            for dk in distKeys :
                header += f",{dk}"
                if isinstance(dataValues[dk][ak], list) :
                    data += f",{dataValues[dk][ak][0]}"
                else :
                    if len(bstatname) == 1 :
                        if bstatname[0] is not None and bstatname[0] != BEACONSRX :
                            data += f",{dataValues[dk][ak][bstatname[0]][0]}"
                    else :
                        data += ",["
                        for bsn in bstatname :
                            if bsn != BEACONSRX :
                                data += f"{dataValues[dk][ak][bsn]};"
                        data += "]"
            if ai == 0 :
                print(f"{header}")
            print(f"{data}")

def getScreenInfo() :
    ms = screeninfo.get_monitors()
    screenX = [None for _ in range(len(ms))]
    screenY = [None for _ in range(len(ms))]
    index = 1
    for m in ms :
        if m.is_primary :
            screenX[0] = (m.x, m.width+m.x)
            screenY[0] = (m.y, m.height+m.y)
        else :
            screenX[index] = (m.x, m.width+m.x)
            screenY[index] = (m.y, m.height+m.y)
            index +=1
    return screenX, screenY


def getWindowPlacement(winobj, start_loc=(500,100)) :
    screenX, screenY = getScreenInfo()
    x_loc, y_loc = start_loc
    w_loc = (0, 0, 0)
    if winobj :
        w_loc = winobj.current_location()
        if w_loc[0] > screenX[0][0] and w_loc[0] < screenX[0][1] :
            w_loc += (0,)
        else :
            w_loc += (1,)
        x_loc += w_loc[0]
        y_loc += w_loc[1]
    return (x_loc, y_loc), w_loc

theFont = ('Courier', 18)
if platform.system() == "Linux" :
    theFont = ('Courier', 12)
    EXIT_FUDGE = 19
elif platform.system() == "Windows" :
    theFont = ('Courier', 9)
    EXIT_FUDGE = 26
PLOTLOGBTN       = 'Plot Log'
CLOSEPLOTLOGBTN  = 'Close Plot'
BUTTON_COLOR          = ('black',   '#EEEEEE') # black on gray
DISABLED_BUTTON_COLOR = ('#AAAAAA', '#EEEEEE') # gray on gray
BTNSIZE = 10
CommonButtonDef = {
                   'button_color'          : BUTTON_COLOR,
                   'mouseover_colors'      : BUTTON_COLOR,
                   'highlight_colors'      : BUTTON_COLOR,
                   'disabled_button_color' : DISABLED_BUTTON_COLOR,
                   'use_ttk_buttons'       : True,
                   's'                     : BTNSIZE, # use the alias for size so individual sizes can be changed by setting size
                   'enable_events'         : True,
                   }

def makePopupListBoxWindow(textval, messageLst, multi=False, rtn_data_only=False, plot2d=False, winobj=None) :

    if messageLst is None :
        messageLst = ["No info available"]
    if textval is None or textval == '' :
        textval = "No input provided"

    popup_loc = (0,0)
    if winobj :
        popup_loc, _ = getWindowPlacement(winobj, start_loc=(800,10))

    thisSelectMode = sg.LISTBOX_SELECT_MODE_MULTIPLE if multi else sg.LISTBOX_SELECT_MODE_SINGLE
    lengOfList = min(10,len(messageLst))
    widthOfList = max(25, len(messageLst[0]))
    layout = []
    tmp = []
    layout.append([sg.Text(textval, auto_size_text=True, text_color=None, background_color=None)])
    layout.append([sg.Listbox(values=messageLst, key="Names", pad=((5,0),(0,0)), size=(widthOfList, lengOfList), expand_y=False, enable_events=True, select_mode=thisSelectMode)])
    tmp.append(sg.Button('Ok', size=(6, 1), bind_return_key=True))
    tmp.append(sg.Button('Cancel', size=(6, 1)))
    if rtn_data_only :
        tmp.append(sg.Checkbox("Data Only", key="dataOnlyChecked", pad=(0,0), enable_events=True))
    if plot2d :
        tmp.append(sg.Checkbox("Plot 2D", key="plot2dChecked", pad=(0,0), enable_events=True))
    layout.append(tmp)

    window = sg.Window(title="", layout=layout, icon=None, auto_size_text=True, button_color=None, no_titlebar=True,
                        background_color=None, grab_anywhere=True, keep_on_top=True,
                        location=popup_loc, relative_location=(None, None), finalize=True, modal=True, font=theFont,
                        disable_close=True)

    textLst = [None]
    while True :
        button, values = window.read()
        if rtn_data_only and button == 'dataOnlyChecked' :
            if window['dataOnlyChecked'].get() :
                window['plot2dChecked'].update(value=False)
                window['plot2dChecked'].update(disabled=True)
            else :
                window['plot2dChecked'].update(disabled=False)
            continue
        if plot2d and button == 'plot2dChecked' :
            continue
        if button != 'Ok' and button != "Cancel" :
            textLst = values['Names'] 
        else:
            if button == 'Cancel' :
                textLst = ['Canceled']
            dataOnlyck = False
            plot2dck = False
            if rtn_data_only and window['dataOnlyChecked'].get() :
                dataOnlyck = True
            if plot2d and window['plot2dChecked'].get() :
                plot2dck = True

            window.close()
            del window
            return textLst, dataOnlyck, plot2dck

def plotLogEvent(window, _myEvent, values, logFileDirectory) :
    """ plots a selected log file
    """
    # open a popup with the contents of the logfile directory but only show the last 4 
    # can scroll to others
    if window[_myEvent].ButtonText == PLOTLOGBTN :
        filelst = getLogFilenames(logFileDirectory) # returns the base names
        fnameLst, dataOnly, plot2d = makePopupListBoxWindow("Select a log file to process", filelst, rtn_data_only=True, plot2d=True, winobj=window)
        fname = fnameLst[0]
        if fname and fname != "Canceled" :
            filename = logFileDirectory+fname # make is a full path
            namelst, _ = getBatStatListAndData(filename)
            bstatname, _, _ = makePopupListBoxWindow("Select up to 3 Battery Stats", namelst, multi=True, winobj=window)
            bstatname = bstatname[:min(3,len(bstatname))]
            if bstatname[0] != "Canceled" :
                _, win_curloc = getWindowPlacement(window)
                rtnDict, desigDict  = plotLogFile(filename, True, win_curloc, batstatLst=bstatname, dataonly=dataOnly, plot3d=not plot2d)
                window['DataOnlyOut'].update(value='')
                window['DataOnlyOut'].print(f'{filename}')
                for k, v in desigDict.items() :
                    window['DataOnlyOut'].print(f"{k}={v}")
                if dataOnly : 
                    for datk, dataValues in rtnDict.items() : #dataValues, beaconsValues, namelst, statsValues
                        if isinstance(dataValues, str) :
                            window['DataOnlyOut'].print(f"{datk}\n{dataValues}")
                            continue
                        else :
                            window['DataOnlyOut'].print(f"{datk}")
    
                        try :
                            distKeys = list(dataValues.keys())
                        except :
                            continue
                        angleKeys = list(dataValues[distKeys[0]].keys())
                        for ai,ak in enumerate(angleKeys) :
                            header = ''
                            data = f'{ak}'
                            for dk in distKeys :
                                header += f",{dk}"
                                if isinstance(dataValues[dk][ak], list) :
                                    data += f",{dataValues[dk][ak][0]}"
                                else :
                                    if len(bstatname) == 1 :
                                        if bstatname[0] is not None :
                                            data += f",{dataValues[dk][ak][bstatname[0]][0]}"
                                    else :
                                        data += ",["
                                        for bsn in bstatname :
                                             data += f"{dataValues[dk][ak][bsn]};"
                                        data += "]"
                            if ai == 0 :
                                window['DataOnlyOut'].print(f"{header}")
                            window['DataOnlyOut'].print(f"{data}")
                else :
                    window[_myEvent].update(text=CLOSEPLOTLOGBTN)
    elif window[_myEvent].ButtonText == CLOSEPLOTLOGBTN :
        closeLogs('all')
        window[_myEvent].update(text=PLOTLOGBTN)

def main() :

    logFileDirectory = "./LogFiles/"
    layout = [
              [sg.Text('./LogFiles/', key='displayDir')],
              [sg.FolderBrowse(initial_folder=logFileDirectory, key='logFileDir', enable_events=True)], #, **CommonButtonDef),
              [
                  sg.Button(PLOTLOGBTN, key='PlotLogBtn', **CommonButtonDef),
                  sg.Button('Exit', key='Exit', **CommonButtonDef)
              ],
                  [sg.Multiline('', key="DataOnlyOut", pad=((5,0),(0,0)), #size=(300, 10),\
                          expand_y=True, expand_x=True, enable_events=False)]
             ]

    theYSize = 500
    sx, sy = getScreenInfo()
    loc_x = 20
    loc_y = sy[0][1] - theYSize - 80 # fudge factor
    window = sg.Window(title="", layout=layout, icon=None, auto_size_text=True, button_color=None, no_titlebar=False,
                        background_color=None, grab_anywhere=True, keep_on_top=True, size=(700,theYSize),
                        location=(loc_x, loc_y), relative_location=(None, None), finalize=True, modal=False, font=theFont,
                        disable_close=False, use_ttk_buttons=False, resizable=True)
    while True :
        myevent, values = window.read()
        if not (myevent == 'Exit' or myevent == sg.WIN_CLOSED) :
            if myevent == "logFileDir" :
                logFileDirectory = values['logFileDir']
                logFileDirectory += "/" if logFileDirectory[-1] != '/' else ''
                window['displayDir'].update(value=logFileDirectory)
                continue
            plotLogEvent(window, myevent, values,logFileDirectory)
        else:
            window.close()
            del window
            break


def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest : myParse.option_list[i].default})
    return rtndict

if __name__ == "__main__" :

    usage = """ Plots or lists the data in the given log file.

    The --batstat option can look like "name one","name two",noSpaceName The first three names will be displayed. 
    It can be the string "list", case sensitive, to list all the battery stat names in the file.
    """
    parser = OptionParser(usage)

    parser.add_option("-f","--file",      dest='filename', action='store',     default=None,  help="Log file to filter and display (REQUIRED). def='%default'")
    parser.add_option("-p","--pwrdata",   dest='pwrdata',  action='store_true',default=False, help="Flag to display Power data from log file. def='%default'")
    parser.add_option("-b","--batstat",   dest='batstat',  action='store'     ,default=None,  help="Battery stat to display. 'list' show stat name (comma separated). def='%default'")
    parser.add_option("-d","--twoD ",     dest='twoD',     action='store_true',default=False, help="Plot 2D. def='%default'")
    parser.add_option("-o","--dataonly",  dest='dataonly', action='store_true',default=False, help="Print data only. def='%default'")

    (options, args) = parser.parse_args()
    defValsD = makeOptionTypeDict(parser)
    optValsD = dict(options.__dict__)

    if defValsD == optValsD :
        main()
        sys.exit()


    if options.filename is None :
        sys.exit("Please provide a log file name.")
    ANT, _ = readDesignatedDataFromLog(options.filename)
        
    bstatnameLst = [None]
    maxnum = 3 if options.pwrdata else 4
    if options.batstat is not None :
        if options.batstat == 'list' :
            namelst, _ = getBatStatListAndData(options.filename)
            for name in namelst :
                print(f'"{name}"')
            sys.exit("Quote three of these names separated by commas.")
        bargs = options.batstat.split(',')
        bstatnameLst = bargs[:min(maxnum,len(bargs))]

    if options.pwrdata or options.batstat :
        rtnDict, desigDict = plotLogFile(options.filename, options.pwrdata, (0,0,0),\
                batstatLst=bstatnameLst, dataonly=options.dataonly, plot3d=not options.twoD)
        if options.dataonly :
            printDataOnly(rtnDict, bstatnameLst)
        else :
            pauseToShow()
        sys.exit("Good Plotting")
    if not options.pwrdata and options.batstat is None :
        sys.exit("Please provide at least one data display type from the log file.")
    if (options.pwrdata or options.batstat) and options.batstat != 'list' :
        plt.ion()
        plt.show()

    valDict, beaconDict = readPowerDataFromLog(options.filename)
    if options.pwrdata :
        if options.dataonly :
            printDataOnly({'Power Data' : valDict})
        else :
            plotData(valDict, plotTitle='Power Data', datakey=None, titlestr=None, plt3d=not options.twoD, A=ANT)

    if options.batstat is not None :
        namelst, valDictStats = getBatStatListAndData(options.filename)
        for batstat in bstatnameLst :
            if batstat in namelst :
                if BEACONSRX == batstat :
                    if options.dataonly :
                        printDataOnly({BEACONSRX : beaconDict})
                    else :
                        plotData(beaconDict, plotTitle=BEACONSRX, datakey=None, titlestr=None, plt3d=not options.twoD, A=ANT)
                else :
                    if options.dataonly :
                        printDataOnly({batstat : valDictStats},[batstat])
                    else :
                        plotData(valDictStats, plotTitle=batstat, datakey=batstat, titlestr=None, plt3d=not options.twoD, A=ANT)

    if (options.pwrdata or options.batstat) and options.batstat != 'list' and not options.dataonly :
        plt.draw()
        plt.pause(0.02)
        pauseToShow()
        sys.exit("Good Plotting")

