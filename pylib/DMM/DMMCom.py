""" DMMCom.py: A class to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the DMMCommStrings dictionary.

Version : 1.0.0
Date : July 10 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
if '/home/ossiadev/pylib' not in sys.path :
    sys.path.append('/home/ossiadev/pylib')
from time import sleep, time
from DMM.DMMCommStrings import DMM_CMD_DICT as dmmcd

import LIB.InstrumentBase as IBase
from LIB.utilmod import *

class DMMCom(IBase.InstrumentBase) :
    ''' DMMCom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='k-34465a-01969.ossiainc.local', port=5025, debug=False, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(DMMCom, self).__init__(addr=addr, port=port, name='DMM', debug=debug, timethis=timethis)
        self.dmmcd = dict(dmmcd)
        self.dmmcd.update(self.ibcmddict)
        self.measurecurrRange = ""
        self.currRanges = {"100uA" : "100 uA", 
                           "1mA"   : "1 mA", 
                           "10mA"  : "10 mA", 
                           "100mA" : "100 mA", 
                           "1A"    : "1", 
                           "3A"    : "3", 
                           ""      : "", 
                           "10A"   : "10"}

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    @property
    def getconfig(self) :
        ''' getconfig -- get the DMM config 
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.dmmcd['getconfig']['get'], True)
        self.instrumentClose()
        d = d.strip()
        #print("getconfig return val = {}".format(repr(d)))
        l=list()
        if len(d) > 0 :
            try :
                l = d.split()
                r = l[1].split(',')
            except :
                pass
        cfg = "{}".format(l[0].replace('"',''))
        rng = "{}".format(r[0].replace('"',''))
        res = "{}".format(r[1].replace('"',''))
        #print("\nDMM.GETCONFIG={}, {}:{}\n".format(cfg, rng, res))
        return cfg, rng, res

    @property
    def cfgvolt(self) :
        ''' cfgvolt -- get/set pair to configure a DMM for voltage measurments
        '''
        #print("1 in cfgvolt get")
        d, t, _ = self.getconfig
        #print("2 in cfgvolt get")
        return d, t
    @cfgvolt.setter
    def cfgvolt(self, value) :
        ''' cfgvolt -- get/set pair to configure a DMM for voltage measurments
        '''
        #print("in cfgvolt set")
        self.instrumentOpen()
        self.senddata(self.dmmcd['cfgvolt']['set'].format(value))
        self.instrumentClose()
        #print("in cfgvolt set")

    @property
    def cfgcurr(self) :
        ''' cfgcurr -- get/set pair to configure a DMM for voltage measurments
        '''
        #print("1 in cfgcurr get")
        d, t, _ = self.getconfig
        #print("2 in cfgcurr get")
        return d, t 
    @cfgcurr.setter
    def cfgcurr(self, value) :
        ''' cfgcurr -- get/set pair to configure a DMM for voltage measurments
        '''
        #print("in cfgcurr set")
        self.instrumentOpen()
        self.senddata(self.dmmcd['cfgcurr']['set'].format(value))
        self.instrumentClose()
        #print("in cfgcurr set")

    @property
    def measurevolt(self) :
        ''' measurevolt -- get the measured voltage
        '''
        #print("in meas volt")
        self.instrumentOpen()
        d = self.sendgetdata(self.dmmcd['measurevolt']['get'], True)
        self.instrumentClose()
        try :
            d = float(d)
        except :
            pass
        #print("in meas volt '{}'".format(d))
        return d 

    @property
    def measurecurr(self) :
        ''' measurecurr -- get the the measure current
        '''
        #print("in meas curr")
        self.instrumentOpen()
        d = self.sendgetdata(self.dmmcd['measurecurr']['get'].format(self.measurecurrRange), True)
        self.instrumentClose()
        try :
            d = float(d)
        except :
            pass
        #print("in meas curr '{}'".format(d))
        return d 
    @measurecurr.setter
    def measurecurr(self, value="") :
        ''' measurecurr -- get the the measure current
        '''
        #print("in meas curr")
        val = self.currRanges.get(value, "")
        self.measurecurrRange = val
        return self.measurecurrRange 


if __name__ == '__main__' :

    #dmm = DMMCom(debug=True)
    dmm = DMMCom()
    d, t, _ = dmm.getconfig
    print("\nd={}, t={}".format(d, repr(t)))

    dmm.cfgvolt = "10,1"
    sleep(0.5)
    d, t = dmm.cfgvolt
    print("\nd={}, t={}".format(d, repr(t)))

    sleep(0.5)
    d = dmm.measurevolt
    print("\nd={} Volts".format(d))

    dmm.cfgcurr = "10,1"
    sleep(0.5)
    d, t = dmm.cfgcurr
    print("\nd={}, t={}".format(d, repr(t)))
    d = dmm.measurecurr
    print("\nd={} Amps".format(d))

