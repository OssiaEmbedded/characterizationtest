""" 
DMMCommStrings.py contains the data to setup and read the DMM configuration

Version : 1.0.0
Date : July 12 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals

DMM_CMD_DICT = {

'getconfig'      : {'get' : ':conf?',
                    'set' : '',
                    'argc': 0},

'cfgvolt'        : {'get' : '',
                    'set' : ':conf:volt {}',
                    'argc': 1},

'cfgcurr'        : {'get' : '',
                    'set' : ':conf:curr {}',
                    'argc': 1},

'measurevolt'    : {'get' : ':meas:volt?',
                    'set' : '',
                    'argc': 0},

'measurecurr'    : {'get' : ':meas:curr? {}',
                    'set' : '',
                    'argc': 1},

'measureavgvolt' : {'get' : 'c?',
                    'set' : 'STATE?',
                    'argc': 0},

'measureavgcurr' : {'get' : 'FSTA?',
                    'set' : 'FSTA?',
                    'argc': 0},

}


if __name__ == "__main__" :

    print('Module only')
