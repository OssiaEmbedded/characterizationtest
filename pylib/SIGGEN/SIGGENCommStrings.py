""" SACommStrings.py: Ossia specific dictionary of the Agilent A-N9020A Signal Analizer's SCPI
commands that are currently being used or likely to be used.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals

SIGGEN_CMD_DICT = {

'rfonoff' : {'get' : ':output?',
             'set' : ':output {}',
             'argc': 1},

'powerLevel' : {'get' : ':POWer?',
                'set' : ':POWer {}{}',
                'argc': 2},

'plunits' : {'get' : ':unit:power?',
             'set' : ':unit:power {}',
             'argc': 1},

'frequency' : {'get' : ':FREQuency?',
               'set' : ':FREQuency {}{}',
               'argc': 2},

'phaseadj' : {'get' : ':phase:adj?',
              'set' : ':phase:adj {}{}',
              'argc': 2},

'phaserefset' : {'get' : ':phase:ref',
                 'set' : ':phase:ref',
                 'argc': 0},

'autoselextref' : {'get' : ':rosc:sour:auto?',
                   'set' : ':rosc:sour:auto {}',
                   'argc': 1},

'frequencyEnable' : {'get'  : ':FREQuency:CHANnels?',
                     'set'  : ':FREQuency:CHANnels {}',
                     'argc' : 1},

'frequencyMode' : {'get'  : ':FREQuency:MODE?',
                   'set'  : ':FREQuency:MODE {}',
                   'argc' : 1},

'capability' : {'get'  : ':SYST:CAPability?',
                'set'  : ':SYST:CAPability?',
                'argc' : 0},

'lanconfig' : {'get'  : ':SYST:COMM:LAN:CONFig?',
               'set'  : ':SYST:COMM:LAN:CONFig {}',
               'argc' : 1},

'landnscap' : {'get'  : ':SYST:COMM:LAN:DNS?',
               'set'  : ':SYST:COMM:LAN:DNS {}',
               'argc' : 1},

'lanip'     : {'get'  : ':SYST:COMM:LAN:IP?',
               'set'  : ':SYST:COMM:LAN:IP {}',
               'argc' : 1},

'gateway'   : {'get'  : ':SYST:COMM:LAN:GATeway?',
               'set'  : ':SYST:COMM:LAN:GATeway {}',
               'argc' : 1},

'hostname'   : {'get'  : ':SYST:COMM:LAN:host?',
                'set'  : ':SYST:COMM:LAN:host {}',
                'argc' : 1},

'landhcptimeout' : {'get' : 'syst:comm:lan:dhcp:tim?',
                    'set' : 'syst:comm:lan:dhcp:tim {}',
                    'argc' : 1},

}


if __name__ == "__main__" :
    mylist = list()
    for k in SIGGEN_CMD_DICT :
        for gs in ['get', 'set'] :
            dictval = SIGGEN_CMD_DICT[k][gs]
            if isinstance(dictval, dict) :
                for kk in dictval :
                    if isinstance(dictval[kk], dict) :
                        for kkk in dictval[kk] :
                            mylist.append(dictval[kk][kkk])
                    else :
                        mylist.append(dictval[kk])

            else :
                mylist.append(dictval)
    print(repr(mylist))

    fd = open("SIGGENCmdString.py",'w')
    fd.write(repr(mylist))
    fd.close()

