""" SIGGENCom.py: A class to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the SIGGENCommStrings dictionary.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
if '/home/ossiadev/pylib' not in sys.path :
    sys.path.append('/home/ossiadev/pylib')
from time import sleep, time
from SIGGEN.SIGGENCommStrings import SIGGEN_CMD_DICT as siggencd

import LIB.InstrumentBase as IBase
from LIB.utilmod import *

class SIGGENCom(IBase.InstrumentBase) :
    ''' SIGGENCom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='', port=5025, debug=False, marker=1, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(SIGGENCom, self).__init__(addr=addr, port=port, name='Signal Generator', debug=debug, timethis=timethis)
        self.marker  = marker
        self.siggenOpen  = self.instrumentOpen
        self.siggenClose = self.instrumentClose
        self.siggencd = dict(siggencd)
        self.siggencd.update(self.ibcmddict)

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    @property
    def phaseadj(self) :
        ''' phaseadj -- getter
            the getter will get current condition of the RF output control.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['phaseadj']['get'], True)
        self.siggenClose()
        try :
            d = float(d)
        except :
            pass
        return d
    @phaseadj.setter
    def phaseadj(self, valuelst) :
        ''' phaseadj -- setter
            The setter will take a value, str or int, and 
            set the sig gen RF output on (1) or off (0)
        '''
        phase = valuelst
        units = "DEG"
        if type(valuelst) == list and len(valuelst) > 1 :
            phase = valuelst[0]
            units = valuelst[1]
        try :
            phase = float(phase)
            units = str(units).upper()
        except ValueError :
            phase = 0.0 
            units = "DEG"

        self.siggenOpen()
        d = self.senddata(self.siggencd['phaseadj']['set'].format(phase, units))
        self.siggenClose()

    @property
    def phaserefset(self) :
        ''' phaserefset -- getter
            the getter will get current condition of the RF output control.
        '''
        self.siggenOpen()
        self.senddata(self.siggencd['phaserefset']['get'])
        self.siggenClose()
        return "" 
    @phaserefset.setter
    def phaserefset(self, value) :
        ''' phaserefset -- setter
            The setter will take a value, str or int, and 
            set the sig gen RF output on (1) or off (0)
        '''
        self.siggenOpen()
        self.senddata(self.siggencd['phaserefset']['set'].format(value))
        self.siggenClose()

    @property
    def autoselextref(self) :
        ''' autoselextref -- getter
            the getter will get current condition of the RF output control.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['autoselextref']['get'], True)
        self.siggenClose()
        try :
            d = str(d)
        except :
            pass
        return d
    @autoselextref.setter
    def autoselextref(self, value) :
        ''' autoselextref -- setter
            The setter will take a value, str or int, and 
            set the sig gen RF output on (1) or off (0)
        '''
        try :
            val = str(value)
        except ValueError :
            val = 'OFF'
        else :
            val = val.upper()
            if val not in self.validOnOfflst :
                val = "OFF"
        self.siggenOpen()
        d = self.senddata(self.siggencd['autoselextref']['set'].format(val))
        self.siggenClose()

    @property
    def rfonoff(self) :
        ''' rfonoff -- getter
            the getter will get current condition of the RF output control.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['rfonoff']['get'], True)
        self.siggenClose()
        try :
            d = str(d)
        except :
            pass
        return d
    @rfonoff.setter
    def rfonoff(self, value) :
        ''' rfonoff -- setter
            The setter will take a value, str or int, and 
            set the sig gen RF output on (1) or off (0)
        '''
        try :
            val = str(value)
        except ValueError as ve :
            print("ERROR: rfonoff -- {}".format(repr(ve)))
            val = 'OFF'
        else :
            val = val.upper()
            if val not in self.validOnOfflst :
                val = "OFF"
        self.siggenOpen()
        d = self.senddata(self.siggencd['rfonoff']['set'].format(val))
        self.siggenClose()

    @property
    def powerLevel(self) :
        ''' powerLevel -- getter
            the getter will get the current RF signal level
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['powerLevel']['get'], True)
        self.siggenClose()
        try :
            d = float(str(d))
        except :
            pass
        return d
    @powerLevel.setter
    def powerLevel(self, valuelist) :
        ''' powerLevel -- setter
            The setter will take a tuple/list of two values level and units
        '''
        try :
            level = float(valuelist[0])
            units = str(valuelist[1])
        except ValueError :
            level = 0.0
            units = "DBM"
        except IndexError :
            level = 0.0
            units = "DBM"
        else :
            units = units.upper()
            if units not in ['DBM', 'DBUV', 'DBUVEMF', 'MV', 'MVEMF'] :
                units = 'DBM'
        self.siggenOpen()
        d = self.senddata(self.siggencd['powerLevel']['set'].format(level, units))
        self.siggenClose()

    @property
    def frequency(self) :
        ''' frequency -- get/set pair to set the frequency.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['frequency']['get'])
        self.siggenClose()
        try :
            d = float(str(d))
        except :
            pass
        return d
    @frequency.setter
    def frequency(self, valuelist) :
        ''' frequency -- get/set pair to set the frequency.
        '''
        try :
            freq = float(valuelist[0])
            hz = str(valuelist[1])
        except ValueError :
            freq = 2.451
            hz = "GHZ"
        except IndexError :
            freq = 2.451
            hz = "GHZ"
        else :
            hz = hz.upper()
            if hz not in ['GHZ', 'MHZ', 'KHZ', 'HZ'] :
                hz = 'GHZ'
        self.siggenOpen()
        d = self.senddata(self.siggencd['frequency']['set'].format(freq, hz))
        self.siggenClose()

    @property
    def frequencyEnable(self) :
        ''' frequencyEnable -- get/set pair to enable the frequency output.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['frequencyEnable']['get'].format(self.marker))
        self.siggenClose()
        return d
    @frequencyEnable.setter
    def frequencyEnable(self, value) :
        ''' frequencyEnable -- get/set pair to enable the frequency output.
        '''
        val = str(value).upper()
        if val not in self.validOnOfflst :
            val = 'OFF'
        self.siggenOpen()
        d = self.senddata(self.siggencd['frequencyEnable']['set'].format(val))
        self.siggenClose()

    @property
    def frequencyMode(self) :
        ''' frequencyMode -- get/set pair to enable the frequency mode.
        '''
        self.siggenOpen()
        d = self.sendgetdata(self.siggencd['frequencyMode']['get'].format(self.marker))
        self.siggenClose()
        return d
    @frequencyMode.setter
    def frequencyMode(self, value) :
        ''' frequencyMode -- get/set pair to enable the frequency mode.
        '''
        mlst = ['CW', 'FIXED', 'LIST']
        if value not in mlst :
            value = mlst[0]

        self.siggenOpen()
        d = self.senddata(self.siggencd['frequencyMode']['set'].format(value))
        self.siggenClose()

    def _setgetSIGGENValue(self, setget, arglist) :
        ''' _setgetSIGGENValue -- Set or Get the value from the SIGGEN for the named SIGGEN function.
        '''
        rtnval = None
        if self.siggenOpen() :
            name = arglist[0]
            if self.debug :
                print("name='{}'  argslist={}".format(name, repr(arglist)))
            argsC = len(arglist[1:])
            argc = self.siggencd[name].get('argd', 0)
            if setget == 'set' :
                argc = self.siggencd[name].get('argc', 0)
            if self.debug :
                print("name='{}' args='{}' arglist={}".format(self.siggencd[name][setget], argc, repr(arglist)))
            if argc == argsC :
                if argc == 1 :
                    cmdval = self.siggencd[name][setget].format(arglist[1])
                elif argc == 2 :
                    cmdval = self.siggencd[name][setget].format(arglist[1], arglist[2])
                elif argc == 3 :
                    cmdval = self.siggencd[name][setget].format(arglist[1], arglist[2], arglist[3])
                else :
                    cmdval = self.siggencd[name][setget]
                if self.debug :
                    print("cmdval={}".format(cmdval))
                rtnval = self.sendgetdata(cmdval, waitforit=False)

            self.siggenClose()
        return rtnval

    def getSIGGENValue(self, arglist) :
        ''' getSIGGENValue -- Get the value from the SIGGEN for the named SIGGEN function.
        '''
        return self._setgetSIGGENValue('get', arglist)

    def setSIGGENValue(self, arglist) :
        ''' getSIGGENValue -- Set the value of the SIGGEN for the named SIGGEN function.
        '''
        return self._setgetSIGGENValue('set', arglist)

if __name__ == '__main__' :

   # from LIB.logResults import LogResults

    siggen = SIGGENCom(addr=sys.argv[1], port=5025, debug=True)
    name = sys.argv[2]
    args = list(sys.argv[2:]) 
    print("name={} args={}".format(name, repr(args)))

    if ":" in name :
        strval = makeListToString(args, delimit="")
        siggen.siggenOpen()
        r = siggen.sendgetdata(strval, waitforit=True)
        siggen.siggenClose()
    else :
        if siggen.siggencd[name]['argc'] == len(args)-1 :
            r=siggen.setSIGGENValue(args)
        else :
            r=siggen.getSIGGENValue(args)

    try :
        r = float(r)
    except ValueError :
        pass

    print(repr(r))

