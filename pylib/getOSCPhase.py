#!/usr/bin/python
import sys
HOMEROOT="/home/ossiadev/pylib/"
if HOMEROOT not in sys.path :
    sys.path.insert(0, HOMEROOT)
from time import sleep, time, localtime
from optparse import OptionParser, OptionGroup

import OSC.OSCCom as osccom

dbug  = False
dbuga = False
ipadr = sys.argv[1]
name  = sys.argv[2]
args  = list(sys.argv[2:])

if 'debug' in sys.argv[3] :
    dbug = True
    args = list(sys.argv[3:])
if 'debugall' == sys.argv[3] :
    dbuga = True

osc = osccom.OSCCom(addr=ipadr, port=5025, debug=dbuga)

if dbug :
    print("sys.argv = {}\n".format(repr(sys.argv)))
    print("name={} args={}".format(name, repr(args)))

rr = 0.0
if name == 'measurePhase' :
    strval = ""
    for x in range(3) :
        osc.measurePhase = args[1:]
        vv = osc.measurePhase
        rr+=float(str(vv[-1]))
        strval += "{} ".format(vv[-1])
    r = rr/3.0

    if dbug :
       print("Avg of 3 {} for {} = {}".format(strval, repr(args[1:]), r))
    else :
       print("{}".format(r))
else :
    print("Not implemented\n")
