import sys
import json
from time import sleep
from TXJSON.JSonCmdStrings import JSON_CMD_DICT as jcd
import TXJSON.JSonCmdStrings
#import SA.SACommunicationBase as sacom
import SA.SACom as sacom
from optparse import OptionParser
import socket

'''
SA_SETUP_LIST = {
'frequencySpan': "100.0 MHz", # sa_span="100.0 MHz"
'centerFrequency': '2.45 GHZ',  # sa_center='2.45 GHZ'
'referenceLevel': '0.00 dBm', # sa_reflevel='31.47 dBm'
'resolutionBW': '910 KHz',   # sa_resBW='910 KHz'
'videoBW': '50 MHz',    # sa_vidBW='50 MHz'
'resolutionBWAuto': 'OFF',
'singleContinuousSweep': 'ON',
'videoBWAuto': 'OFF',
}
'''
SA_SETUP_LIST = [
['resetsa', ''],
['resolutionBWAuto', 'OFF'],
['videoBWAuto', 'OFF'],
['frequencySpan', "100.0 MHz"], # sa_span="100.0 MHz"
['centerFrequency', '2.45 GHZ'],  # sa_center='2.45 GHZ'
['referenceLevel', '0.00 dBm'], # sa_reflevel='31.47 dBm'
['resolutionBW', '910 KHz'],   # sa_resBW='910 KHz'
['videoBW', '50 MHz'],    # sa_vidBW='50 MHz'
['singleContinuousSweep', 'ON'],
]



HOST='localhost'
PORT=50000
SAIP='10.10.0.191'

def sendCommand(cmd, addr=HOST, port=PORT) :
    if options.debug :
        print "CMD = {}".format(cmd)
    ipaddr = socket.gethostbyname(addr)
    ipport = int(port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ipaddr, ipport))
    sock.settimeout(1.0)
    sock.sendall(cmd+"\r\n")
    sleep(0.5)
    retry = 0
    data = ""
    while retry < 6 : #and len(data) == 0 :
        try :
            d = sock.recv(8048)
            data += d
        except socket.timeout :
            print("timeout retry={}".format(retry))
            retry += 1
        except socket.error :
            print("socket Error retry={}".format(retry))
            break
    if sys.version_info[0] == 3 :
        data = data.decode('utf-8')
    sock.close()
    return data

def sendGetCharger(argv, addr=HOST, port=PORT) :
    cmd = jcd.get(argv[0], None)
    if options.debug :
        print "CMD = {}".format(cmd)
    pyVal = None
    if cmd is not None :
        argc = len(argv[1:])
        argsNeeded = cmd.get('numofargs', -1)
        if argsNeeded == argc :
            jstr = cmd.get('jstr', "")
            jstr_cmd = jstr % (tuple(argv[1:]))
            if options.debug :
                print "jstr_cmd="+jstr_cmd
            jval = sendCommand(jstr_cmd, addr, port)
            if options.debug :
                print "jstr_rtn="+jval
            pyVal = json.loads(jval)
            if options.debug :
                print "pyVal="+pyVal
        else :
            print "Arg count incorrect need exactly {} ({})".format(argsNeeded, argc)
    return pyVal

def myLogInit() :
    fd = open("./myLogFile.csv","w")
    if fd :
        line = "clientId, shortID, powerLevel, version, netCurrent, batteryVoltage, queryFailedCount, mRSSI, RSSI_1\n"
        fd.write(line)
        fd.close()

def myLog(clientId, shortID, powerLevel, version, netCurrent, batteryVoltage, queryFailedCount, mRSSI, RSSI_1) :
    fd = open("./myLogFile.csv", 'a')
    if fd :
        line = "{},{},{},{},{},{},{},{},{}\n".format(clientId, shortID, powerLevel, version, netCurrent, batteryVoltage, queryFailedCount, mRSSI, RSSI_1)
        fd.write(line)
        fd.close()
            
def runthecharacter(saip, psa, chgrip, pchgr) :
    print "The Characteriztion program is executing"
    if options.debug :
        print saip
        print psa
        print chgrip
        print pchgr
    # setup the SA
    sa=sacom.SACom(addr=saip, port=psa, debug=1)
    for cmd_data in SA_SETUP_LIST :
        sa.setSAValue(cmd_data[0], cmd_data[1])
    # init the transmitter (charger) and receiver (client)
    return
    myLogInit()
    v = sendGetCharger("client_list", addr=chgrip, port=pchgr) 
    v = v['Result']['Devices']
    l = len(v)
    if l > 1 :
        while l > 0 :
            l -= 1
            print v[l]['Client ID']
    elif l == 1 :
        clientId = v[0]['Client ID']
        # first register the client
        argv = ['register_client', str(clientId)]
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        if v['Result']['Status'] != "SUCCESS" :
            print "Result Status = " + repr(v['Result']['Status'])
            return
        # configure the client
        argv = ['client_config', str(clientId), '0x6']
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        if v['Result']['Status'] != "SUCCESS" :
            print "Result Status = " + repr(v['Result']['Status'])
            return
        # check for a non zero Short ID
        argv = ['client_detail', str(clientId)]
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        shortID = getClientShortID(v)
        if shortId == "0x0000" :
            print "Short ID shows device is not registered: " + repr(v['Result']['Short ID'])
            return
        #
        argv = ['get_power_level']
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        if v['Result']['Status'] != "SUCCESS" :
            print "Power Level request Failed: " + repr(v['Result']['Status'])
            return
        PowerLevel =  getChargerPowerLevel(v)

        argv = ['start_charging', str(clientId)]
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        if v['Result']['Devices'][0]['Status'] != "CHARGING" :
            print "Charging did not Start: " + repr(v['Result']["Devices"][0]['Status'])
            return

        # wait 10 seconds
        sleep(10)
        # get charging client details
        argv = ['client_detail', str(clientId)]
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        version =  getClientVersion(v)
        netCurrent =  getClientNetCurrent(v)
        queryFileCount =  getClientQueryFailed(v)

        # get charging client data
        argv = ['client_data', str(clientId)]
        v = sendGetCharger(argv, addr=chgrip, port=pchgr) 
        if v['Result']['Status'] != "SUCCESS" :
            print "Client Data request Failed: " + repr(v['Result']['Status'])
        batteryVoltage = getClientBatteryVoltage(v)
        mRSSI, RSSI_1, thelist =  getClientMaxRFPower(v)

        sa.setSAValue()
        myLog(clientId, shortID, powerLevel, version, netCurrent, batteryVoltage, queryFailedCount, mRSSI, RSSI_1, SAPower, SAFreq)

    else :
        print "No clients found"
        

def main(options, argv) :
    """ main -- provides logic to send commands to the charger or the Signal Analyzer
    """
    # process the IP and Port options
    try :
        i1, i2 = options.ipaddr.split(',')
    except :
        i1 = options.ipaddr
        i2 = options.ipaddr
    try :
        p1, p2 = options.port.split(',')
    except :
        p1 = options.port
        p2 = options.port
    if (options.charger ^ (options.sar or options.saw)) : # just sa or chgr
        if options.sar or options.saw : # just sa
            psa = int(p1)
            saip = i1
        elif options.charger : # just charger
            pchgr = int(p2)
            chgrip = i2
    else : # neither one or both
        psa, pchgr = (int(p1), int(p2))
        saip, chgrip = (i1, i2)

    # now process the command arguments
    args = argv
    if options.saw : # read from device
        sa = sacom.SACom(addr=saip, port=psa, debug=options.debug)
        print sa.setSAValue(args[0], args[1])
        args = args[2:]
        sa.saClose()
    if options.sar :
        sa = sacom.SACom(addr=saip, port=psa, debug=options.debug)
        print "getting {}".format(args[0])
        print sa.getSAValue(args[0])
        args = args[1:]
        sa.saClose()
    if options.charger :
        cmd = jcd.get(args[0], None)
        if cmd is not None :
            argsNeeded = cmd.get('numofargs', -1)
            if argsNeeded <= len(args) - 1 :
                jstr = cmd.get('jstr', "")
                jstr_cmd = jstr % (tuple(args[1:1+argsNeeded]))
                args = args[1+argsNeeded:]
                if options.debug :
                    print "jstr_cmd="+jstr_cmd
            else :
                print "Arg count incorrect need exactly {} -- {} **  {}".format(argsNeeded, len(args), repr(args))
            
            v = sendCommand(jstr_cmd, addr=chgrip, port=pchgr)
            if options.debug :
                print "\nJSAON "+repr(v)
                print ""

            vv =  json.loads(v)
            if options.debug :
                print "PY "+repr(vv)
                print ""
                print "ARGUMENTS "+repr(args)
                print ""
            argptr = 0
            while argptr < len(args) :
                if type(vv) == list :
                    vv = vv[int(args[argptr])]
                elif type(vv) == dict :
                    vv = vv.get(args[argptr],"Not available - '{}'".format(args[argptr]))
                if argptr == (len(args) - 1) :
                    if options.debug :
                        print "data of arg['%d'] %s %s\n" %(argptr,args[argptr],repr(vv))
                    print "\n{} = {}".format(args[argptr], vv)
                argptr = argptr + 1
                if type(vv) == list :
                    for lstarg in vv :
                        if options.debug :
                            print "vv is list lstarg={} -- vv={} -- args[argptr]={}".format(lstarg, vv, args[argptr])
                        if type(lstarg) == dict :
                            print " {} = {}".format(arg[argptr], lstarg.get(arg[argptr], "not exit"))
                        else :
                            print "\n {} = {}".format(arg[argptr], lstarg)

                    break
                if argptr < len(args) :
                    if unicode(args[argptr], 'utf-8').isnumeric() :
                        vv = vv[int(args[argptr])]
                    else :
                        vv = vv[args[argptr]]
                if argptr == (len(args) - 1) :
                    print "\n{}[{}] = {}".format(args[argptr-1], args[argptr], vv)
                argptr = argptr + 1
             
    if not options.sar and not options.saw and not options.charger  : # default is to run the characterization script
       runthecharacter(saip, psa, chgrip, pchgr)
        

if __name__ == '__main__' :
    parser = OptionParser()

    parser.add_option("","--debug",dest='debug',action="store_true",default=False,help="print debug info.")
    parser.add_option("","--sar",dest='sar',action="store_true",default=False,help="sa first arg read command.")
    parser.add_option("","--saw",dest='saw',action="store_true",default=False,help="""sa first arg write command all required args for command.
 If both sa and chgr, sa args are first""")
    parser.add_option("","--chgr",dest='charger',action="store_true",default=False,help="""charger first arg = cmd, next args required by the command.
 If both sa and chgr, sa args are first""")
    parser.add_option("-p","--port",dest='port',action='store',default='5023,50000',help="device ports: comma separated list of ports."
            " If -s option -p is SA"
            ", if -c option -p is Chgr"
            ", if neither or both -p is list of two")
    parser.add_option("-i","--ip",dest='ipaddr',action='store',default='A-N9020A-11404.ossiainc.local,localhost',help="device Network Addrs: comma separated list of network addrs."
            " If -s option -i is SA"
            ", if -c option -i is Chgr"
            ", if neither or both -i is list of two")

    (options, args) = parser.parse_args()
    if options.debug :
        print repr(options)
        print repr(args)
    main(options, args)


