""" PWRAMPCom.py: A class to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the PWRAMPCommStrings dictionary.

Version : 1.0.0
Date : July 10 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
if '/home/ossiadev/pylib' not in sys.path :
    sys.path.append('/home/ossiadev/pylib')
from time import sleep, time
from PWRAMP.PWRAMPCommStrings import PWRAMP_CMD_DICT as pwrampcd

import LIB.InstrumentBase as IBase
from LIB.utilmod import *

class PWRAMPCom(IBase.InstrumentBase) :
    ''' PWRAMPCom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='ca914f3.ossiainc.local', port=10001, debug=False, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(PWRAMPCom, self).__init__(addr=addr, port=port, name='Power Amplifier', debug=debug, timethis=timethis)
        self.pwrampcd = dict(pwrampcd)
        self.pwrampcd.update(self.ibcmddict)
        self.sendCR = True 
        self.sendLF = False

        self.faultValues = ["No Fault", "Interlock", "DC Fault PS1", "Thermal Final", "Thermal Driver", "Amp A2", "Amp A3"]

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    @property
    def poweronoff(self) :
        ''' poweronoff -- get/set pair to set the PAs power on or off.
            the getter will read the state and parse the bit.
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['state']['get'])
        self.instrumentClose()
        d = d.strip()
        if len(d) > 0 :
            try :
                d = (int(d.split()[-1], 16) >> 8) & 1
            except :
                pass
        return d
    @poweronoff.setter
    def poweronoff(self, value) :
        ''' poweronoff -- get/set pair to set the power on or off
        '''
        try :
            val = str(value).upper()
        except ValueError :
            val = "0"
        else :
            if val not in  self.validOnOfflst :
                val = "0"
            elif not unicode(val).isnumeric() :
                val = "1" if val == "ON" else "0"
            self.instrumentOpen()
            self.senddata(self.pwrampcd['pwron']['set'].format(val))
            self.instrumentClose()

    @property
    def gainraw(self) :
        ''' gainraw -- getter
            the getter will read the gainraw G0-G4095 strip the G and return the value
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['gain']['get'])
        self.instrumentClose()
        try :
            d = str(d)
            d = d.strip()[1:]
        except :
            pass
        return d
    @gainraw.setter
    def gainraw(self, value) :
        ''' gainraw -- setter
            the setter will take in an int of 0-4095
            and send the data.
        '''
        valint = 0
        try :
            val = int(value)
        except ValueError :
            pass
        else :
            valint = min(val, 4095)
            valint = max(valint, 0)
            self.instrumentOpen()
            self.senddata(self.pwrampcd['gain']['set'].format(valint))
            self.instrumentClose()

    @property
    def gain(self) :
        ''' gain -- getter
            the getter will read the gain G0-G4095 strip the G and convert to a %
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['gain']['get'])
        self.instrumentClose()
        try :
            d = str(d)
            d = d.strip()[1:]
            if unicode(d).isnumeric()  :
                d = round((float(d) / 4095.0) * 100.0, 2)
        except :
            pass
        return d
    @gain.setter
    def gain(self, value) :
        ''' gain -- setter
            the setter will take in a percentage convert to 0-4095
            and send the data.
        '''
        valint = 0
        try :
            val = float(value)
        except ValueError :
            pass
        else :
            valint = int(((val / 100.0) * 4096.0) + 0.5)
            valint = min(valint, 4095)
            valint = max(valint, 0)
            self.instrumentOpen()
            self.senddata(self.pwrampcd['gain']['set'].format(valint))
            self.instrumentClose()

    @property
    def resetpa(self) :
        ''' reset -- get/set pair to reset the PA and clear faults
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['reset']['get'])
        self.instrumentClose()
        return d 
    @resetpa.setter
    def resetpa(self, value) :
        ''' reset -- get/set pair to reset the PA and clear faults
        '''
        self.instrumentOpen()
        d = self.senddata(self.pwrampcd['reset']['set'])
        self.instrumentClose()
        return value

    @property
    def iobfw(self) :
        ''' iobfw -- get/set pair to get the IO Board FW Revision
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['iobfw']['get']).strip()
        self.instrumentClose()
        return d 
    @iobfw.setter
    def iobfw(self, value) :
        ''' iobfw -- get/set pair to get the IO Board FW Revision
        '''
        self.instrumentOpen()
        d = self.senddata(self.pwrampcd['iobfw']['set']).strip()
        self.instrumentClose()
        return d

    @property
    def state(self) :
        ''' state -- get/set pair to get the PAs state info
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['state']['get'])
        self.instrumentClose()
        d = d.strip()
        rc = 0
        ps = 0
        fs = 0
        if len(d) > 0 :
            try :
                d = int(d.split()[-1], 16)
                rc = (d >> 15) & 1
                ps = (d >>  8) & 1
                fs = (d >> 11) & 1
            except :
                pass
        return rc, ps, fs 
    @state.setter
    def state(self, value) :
        ''' state -- get/set pair to get the PAs state info
        '''
        self.instrumentOpen()
        d = self.senddata(self.pwrampcd['state']['set'])
        self.instrumentClose()
        d = d.strip()
        rc = 0
        ps = 0
        fs = 0
        if len(d) > 0 :
            try :
                d = int(d.split()[-1], 16)
                rc = (d >> 15) & 1
                ps = (d >>  8) & 1
                fs = (d >> 11) & 1
            except :
                pass
        return rc, ps, fs 

    @property
    def faults(self) :
        ''' state -- get/set pair to get the PAs state info
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwrampcd['fault']['get'])
        self.instrumentClose()
        d = d.strip()
        val = d
        if len(d) > 0 :
            try :
                d = int(d.split()[-1], 16)
                val = self.faultValues[d]
            except :
                pass
        return val
    @faults.setter
    def faults(self, value) :
        ''' state -- get/set pair to get the PAs state info
        '''
        self.instrumentOpen()
        d = self.senddata(self.pwrampcd['fault']['set'])
        self.instrumentClose()
        d = d.strip()
        val = d
        if len(d) > 0 :
            try :
                d = int(d.split()[-1], 16)
                val = self.faultValues[d]
            except :
                pass
        return val

if __name__ == '__main__' :

    import sys

    pa = PWRAMPCom()
    if len(sys.argv) > 1 :
        pwr = int(sys.argv[1])
        pa.gainraw = pwr
        sleep(1.5)
    #pa.poweronof = "ON"
    d = pa.gainraw
    p = pa.gain
    print("raw counts {} percent {}".format(d, p))

