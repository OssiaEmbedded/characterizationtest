""" SACommStrings.py: Ossia specific dictionary of the Agilent A-N9020A Signal Analizer's SCPI
commands that are currently being used or likely to be used.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals

PWRAMP_CMD_DICT = {

'pwron' : {'get' : '',
           'set' : 'P{}',
           'argc': 1},

'gain' : {'get' : 'G?',
          'set' : 'G{:04d}',
          'argc': 1},

'reset' : {'get' : 'R',
           'set' : 'R',
           'argc': 0},

'iobfw' : {'get' : '*IOB?',
           'set' : '*IOB?',
           'argc': 0},

'state' : {'get' : 'STATE?',
           'set' : 'STATE?',
           'argc': 0},

'fault' : {'get' : 'FSTA?',
           'set' : 'FSTA?',
           'argc': 0},

}


if __name__ == "__main__" :
    mylist = list()
    for k in PWRAMP_CMD_DICT :
        for gs in ['get', 'set'] :
            dictval = PWRAMP_CMD_DICT[k][gs]
            if isinstance(dictval, dict) :
                for kk in dictval :
                    if isinstance(dictval[kk], dict) :
                        for kkk in dictval[kk] :
                            mylist.append(dictval[kk][kkk])
                    else :
                        mylist.append(dictval[kk])

            else :
                mylist.append(dictval)
    print(repr(mylist))

    fd = open("PWRAMPCmdString.py",'w')
    fd.write(repr(mylist))
    fd.close()

