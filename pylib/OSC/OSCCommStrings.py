""" SACommStrings.py: Ossia specific dictionary of the Agilent A-N9020A Signal Analizer's SCPI
commands that are currently being used or likely to be used.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals

OSC_CMD_DICT = {

# Control Commands
'run' : {'get'  : ':RUN',
         'set'  : ':RUN',
         'argc' : 0},

'single' : {'get'  : ':SINGLe',
            'set'  : ':SINGLe',
            'argc' : 0},

'stop' : {'get'  : ':STOP',
          'set'  : ':STOP',
          'argc' : 0},

# measurment commands
'analyzeAllEdges' : {'get' : ':ANALyze:AEDGes?',
                     'set' : ':ANALyze:AEDGes {}',
                     'argc': 1},

'measurePhase' : {'get' : ':MEASure:PHASe? CHAN{},CHAN{},{}',
                  'set' : ':MEASure:PHASe CHAN{},CHAN{},{}',
                  'argc': 3,
                  'argd': 3},

'measureJiter' : {'get' : ':measure:pjitter? PNO, {}, {}',
                  'set' : ':measure:pjitter PNO, {}, {}',
                  'argc' : 2,
                  'argd' : 2},

'displayChan' : {'get'  : ':CHANnel{}:DISPlay?',
                 'set'  : ':CHANnel{}:DISPlay {}',
                 'argc' : 2,
                 'argd' : 1},

# Channel/Vertical setup commands

'inputChanCouple' : {'get'  : ':CHANnel{}:INPut?',
                     'set'  : ':CHANnel{}:INPut {}', # AC, DC, DC50
                     'argc' : 2,
                     'argd' : 1},

'displayChanOffset' : {'get'  : ':CHANnel{}:OFFSet?',
                       'set'  : ':CHANnel{}:OFFSet {}',
                       'argc' : 2,
                       'argd' : 1},

'displayChanScale' : {'get'  : ':CHANnel{}:SCALe?',
                      'set'  : ':CHANnel{}:SCALe {}',
                      'argc' : 2,
                      'argd' : 1},

'displayChanRange' : {'get'  : ':CHANnel{}:RANGe?',
                      'set'  : ':CHANnel{}:RANGe {}',
                      'argc' : 2,
                      'argd' : 1},

'displayChanUnits' : {'get'  : ':CHANnel{}:UNITs?', # VOLT AMP WATT or UNKN
                      'set'  : ':CHANnel{}:UNITs {}',
                      'argc' : 2,
                      'argd' : 1},

# Timebase/Y-axis Horizontal setup commands

'timeBaseScale' : {'get'  : ':TIMebase:SCALe?',
                   'set'  : ':TIMebase:SCALe {}',
                   'argc' : 1},

'timeBasePosition' : {'get'  : ':TIMebase:POSition?',
                      'set'  : ':TIMebase:POSition {}',
                      'argc' : 1},


# Trigger Commands 
'triggerSource' : {'get'  : ':TRIGger:AND{}:SOURce? CHAN{}', # HIGH, LOW, DONTcare
                   'set'  : ':TRIGger:AND{}:SOURce CHAN{},{}',
                   'argc' : 3,
                   'argd' : 2},

'triggerLevel' : {'get'  : ':TRIGger:LEVel? CHAN{}',
                  'set'  : ':TRIGger:LEVel CHAN{},{}',
                  'argc' : 2,
                  'argd' : 1},

'triggerSweep' : {'get'  : ':TRIGger:SWEep?',
                  'set'  : ':TRIGger:SWEep {}',
                  'argc' : 1},

'triggerMode' : {'get'  : ':TRIGger:MODE?',
                 'set'  : ':TRIGger:MODE {}',
                 'argc' : 1},

'triggerSlope' : {'get'  : ':TRIGger:EDGE:SLOPe?',
                  'set'  : ':TRIGger:EDGE:SLOPe {}',
                  'argc' : 1},

'systemHeader' : {'get'  : ':SYSTem:HEADer?',
                  'set'  : ':SYSTem:HEADer {}',
                  'argc' : 1},
}


if __name__ == "__main__" :
    for propName in OSC_CMD_DICT :
        vv = """    @property
    def {0}(self) :
        ''' {0} -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(osccd['{0}']['get'])
        self.oscClose()
        return d
    @{0}.setter
    def {0}(self, value) :
        ''' {0} -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(osccd['{0}']['set'].format(value))
        self.oscClose()
        return d
             """.format(propName)
        print(vv)

