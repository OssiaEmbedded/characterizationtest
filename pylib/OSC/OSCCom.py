""" OSCCom.py: A class to connect to and communicate with an Agilent Osciloscope.
It makes use of the OSCCommStrings dictionary.

Version : 1.0.0
Date : May 11 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
if '/home/ossiadev/pylib' not in sys.path :
    sys.path.append('/home/ossiadev/pylib')
from time import sleep, time
from OSC.OSCCommStrings import OSC_CMD_DICT as osccd
import LIB.InstrumentBase as IBase
from LIB.utilmod import makeListToString

class OSCCom(IBase.InstrumentBase) :
    ''' OSCCom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='', port=5025, debug=False, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(OSCCom, self).__init__(addr=addr, port=port, name='Oscilloscope', debug=debug, timethis=timethis, delimit="\n")
        self.oscOpen  = self.instrumentOpen
        self.oscClose = self.instrumentClose
        self.oscPhaseCh1 = 1
        self.oscPhaseCh2 = 2
        self.oscPhaseDir = "RISing"
        self.lstChan = 1
        self.osccd = dict(osccd)
        self.osccd.update(self.ibcmddict)

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    def getSaveImage(self, filename) :

        url = bytearray(b'GET /Agilent.OSC.WebInstrument/Screen.png HTTP/1.1\r\nHost: 10.10.0.83:7081\r\nUser-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\n\r\n')
        osc = IBase.InstrumentBase(port=80)
        osc.instrumentOpen()
        oscResponse = osc.sendgetimage(url)
        osc.instrumentClose()

        startOfImageData = bytearray(b'\r\n\r\n') 
        imageIndex = oscResponse.find(startOfImageData) + 4
        with open(filename, "wb") as fd :
            fd.write(oscResponse[imageIndex:])
            fd.close

    # mesure getter/setters
    @property
    def analyzeAllEdges(self) :
        ''' analyzeAllEdges -- get/set pair 
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['analyzeAllEdges']['get'])
        self.oscClose()
        return d
    @analyzeAllEdges.setter
    def analyzeAllEdges(self, onoff) :
        ''' analyzeAllEdges -- get/set pair
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['analyzeAllEdges']['set'].format(onoff))
        self.oscClose()
        return d

    @property
    def measurePhase(self) :
        ''' measurePhase -- get/set pair to measure Phase of two channelss.
            the getter will set the all edges to on.
        '''
        self.oscOpen()
        self.senddata(self.osccd['single']['set'])
        sleep(1.0)
        rtnval = list()
        self.senddata(self.osccd['measurePhase']['set'].format(self.oscPhaseCh1, 1, self.oscPhaseDir))
        sleep(1.25)
        self.sendgetdata(self.osccd['measurePhase']['get'].format(self.oscPhaseCh1, 1, self.oscPhaseDir))
        for ch in self.oscPhaseCh2 :
            self.senddata(self.osccd['measurePhase']['set'].format(self.oscPhaseCh1, ch, self.oscPhaseDir))
            #sleep(1.25)
            rtnval += [self.sendgetdata(self.osccd['measurePhase']['get'].format(self.oscPhaseCh1, ch, self.oscPhaseDir))]
        self.senddata(self.osccd['run']['set'])
        if self.debug :
            print("OSCCom measurePHASE -- rtnval={}".format(repr(rtnval)))
        self.oscClose()
        return rtnval
    @measurePhase.setter
    def measurePhase(self, value) :
        ''' measurePhase -- get/set pair
            The setter will take a tuple/list of three values
            and set class variables to those values
        '''
        #print("measurePHASE value = {}".format(repr(value)))
        self.oscPhaseCh1 = value[0]
        self.oscPhaseCh2 = value[1]
        self.oscPhaseDir = value[2]
        #self.oscOpen()
        #self.sendgetdata(self.osccd['single']['set'])
        #d = self.senddata(self.osccd['measurePhase']['set'].format(self.oscPhaseCh1, self.oscPhaseCh2, self.oscPhaseDir))
        #d = self.sendgetdata(self.osccd['measurePhase']['get'].format(self.oscPhaseCh1, self.oscPhaseCh2, self.oscPhaseDir))
        #self.senddata(self.osccd['run']['set'])
        #self.oscClose()

    @property
    def displayChan(self) :
        ''' displayChan -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['displayChan']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @displayChan.setter
    def displayChan(self, value) :
        ''' displayChan -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['displayChan']['set'].format(value[0], value[1]))
        self.lstChan = 0
        self.oscClose()
        return d
             
    @property
    def triggerSource(self) :
        ''' triggerSource -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['triggerSource']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @triggerSource.setter
    def triggerSource(self, valuelist) :
        ''' triggerSource -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['triggerSource']['set'].format(valuelist[0], valuelist[1]))
        self.lstChan = valuelist[1]
        self.oscClose()
        return d
             
    @property
    def systemHeader(self) :
        ''' systemHeader -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['systemHeader']['get'])
        self.oscClose()
        return d
    @systemHeader.setter
    def systemHeader(self, value) :
        ''' systemHeader -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['systemHeader']['set'].format(value))
        self.oscClose()
        return d
             
    @property
    def displayChanScale(self) :
        ''' displayChanScale -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['displayChanScale']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @displayChanScale.setter
    def displayChanScale(self, value) :
        ''' displayChanScale -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['displayChanScale']['set'].format(value[0], value[1]))
        self.lstChan = value[0]
        self.oscClose()
        return d
             
    @property
    def displayChanRange(self) :
        ''' displayChanRange -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['displayChanRange']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @displayChanRange.setter
    def displayChanRange(self, value) :
        ''' displayChanRange -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['displayChanRange']['set'].format(value[0], value[1]))
        self.lstChan = value[0]
        self.oscClose()
        return d
             
    @property
    def triggerLevel(self) :
        ''' triggerLevel -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['triggerLevel']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @triggerLevel.setter
    def triggerLevel(self, value) :
        ''' triggerLevel -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['triggerLevel']['set'].format(value))
        self.lstChan = value
        self.oscClose()
        return d
             
    @property
    def displayChanOffset(self) :
        ''' displayChanOffset -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['displayChanOffset']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @displayChanOffset.setter
    def displayChanOffset(self, value) :
        ''' displayChanOffset -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['displayChanOffset']['set'].format(value[0], value[1]))
        self.lstChan = value[0]
        self.oscClose()
        return d
             
    def setLastChan(self, value) :
        self.lstChan = value

    @property
    def displayChanUnits(self) :
        ''' displayChanUnits -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['displayChanUnits']['get'].format(self.lstChan))
        self.oscClose()
        return d
    @displayChanUnits.setter
    def displayChanUnits(self, valuelist) :
        ''' displayChanUnits -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['displayChanUnits']['set'].format(valuelist[0], valuelist[1]))
        self.lstChan = valuelist[0]
        self.oscClose()
        return d
             
    @property
    def triggerSlope(self) :
        ''' triggerSlope -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['triggerSlope']['get'])
        self.oscClose()
        return d
    @triggerSlope.setter
    def triggerSlope(self, value) :
        ''' triggerSlope -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['triggerSlope']['set'].format(value))
        self.oscClose()
        return d
             
    @property
    def timeBaseScale(self) :
        ''' timeBaseScale -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['timeBaseScale']['get'])
        self.oscClose()
        return d
    @timeBaseScale.setter
    def timeBaseScale(self, value) :
        ''' timeBaseScale -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['timeBaseScale']['set'].format(value))
        self.oscClose()
        return d
             
    @property
    def timeBasePosition(self) :
        ''' timeBasePosition -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['timeBasePosition']['get'])
        self.oscClose()
        return d
    @timeBasePosition.setter
    def timeBasePosition(self, value) :
        ''' timeBasePosition -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['timeBasePosition']['set'].format(value))
        self.oscClose()
        return d
             
    @property
    def triggerMode(self) :
        ''' triggerMode -- get/set pair
        '''
        self.oscOpen()
        d = self.sendgetdata(self.osccd['triggerMode']['get'])
        self.oscClose()
        return d
    @triggerMode.setter
    def triggerMode(self, value) :
        ''' triggerMode -- get/set pair 
        '''
        self.oscOpen()
        d = self.senddata(self.osccd['triggerMode']['set'].format(value))
        self.oscClose()
        return d
             

    def _setgetOSCValue(self, setget, argslist) :
        ''' _setgetOSCValue -- Set or Get the value from the OSC for the named OSC function.
            argslist will be name, arg0, arg1, arg2 etc.
        '''
        rtnval = None
        if self.oscOpen() :
            name = argslist[0]
            if self.debug :
                print("name='{}'  argslist={}".format(name, repr(argslist)))
            argsC = len(argslist[1:])
            argc = self.osccd[name].get('argd', 0)
            if setget == 'set' :
                argc = self.osccd[name].get('argc', 0)
            if self.debug :
                print("name='{}' argsc='{}'-- {} argslist={}".format(self.osccd[name][setget], argc, argsC, repr(argslist)))
            if argc == argsC :
                if argc == 1 :
                    cmdval = self.osccd[name][setget].format(argslist[1])
                elif argc == 2 :
                    cmdval = self.osccd[name][setget].format(argslist[1], argslist[2])
                elif argc == 3 :
                    cmdval = self.osccd[name][setget].format(argslist[1], argslist[2], argslist[3])
                else :
                    cmdval = self.osccd[name][setget]
                if self.debug :
                    print("cmdval={}".format(cmdval))
                rtnval = self.sendgetdata(cmdval, waitforit=False)

            self.oscClose()
        return rtnval

    def getOSCValue(self, argslist) :
        ''' getOSCValue -- Get the value from the OSC for the named OSC function.
        '''
        return self._setgetOSCValue('get', argslist)

    def setOSCValue(self, argslist) :
        ''' getOSCValue -- Set the value of the OSC for the named OSC function.
        '''
        return self._setgetOSCValue('set', argslist)

    def makeListToString(self, thelist) :
        strval = None
        if isinstance(thelist, list) :
            strval = ""
            for a in thelist :
                strval += a
        print("strval = '{}' input = '{}'".format(strval, repr(thelist)))
        return strval


if __name__ == '__main__' :

   # from LIB.logResults import LogResults

    osc = OSCCom(addr=sys.argv[1], port=5025, debug=False)
    name = sys.argv[2]
    args = list(sys.argv[2:])
    #print("name={} args={}".format(name, repr(args)))
    rr = 0.0
    if name == 'measurePhase' :
        osc.senddata(osc.osccd['single']['set'])
        sleep(1.0)
        osc.measurePhase = [1, [2,3,4], '']
        r =  osc.measurePhase
        #p =":measure:results?"
        #osc.oscOpen()
        #s = osc.sendgetdata(p, waitforit=True)
        #r = s.split(",Phase")
        #osc.oscClose()
        #print("{:7.3f}, {:7.3f}, {:7.3f}".format(float(r[0]),float(r[1]),float(r[2]))) 
        #for x in r :
        #    pp = x.split(",")
        print(r)
    elif ":" in name :
        strval = makeListToString(args, delimit="")
        osc.oscOpen()
        r = osc.sendgetdata(strval, waitforit=False)
        osc.oscClose()
        print(repr(r))
    else :
        if osc.osccd[name]['argc'] == len(args)-1 :
            r=osc.setOSCValue(args)
        else :
            r=osc.getOSCValue(args)

    #try :
        #r = float(r)
    #except :
        #pass

    #print(repr(r))

