""" TandomXY.py: A class to connect to an Ossia tandom X Y  actuator server set. There are two servers
to control both the X and Y axis one for each.

Version : 1.0.0
Date : Nov 20 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function #, unicode_literals
import socket, sys
from time import sleep

sys.path.insert(0,"/home/ossiadev/pylib/TanXY")

COMMANDVAL = "COMMANDVAL"

class TandomXYZ(object) :

    def __init__(self, ipaddr=("10.10.1.46", "10.10.1.45"), delimit=('/','/'), portnumber=("7000","7000"), debug=False) :
        self.ipaddressx = socket.gethostbyname(ipaddr[0])
        self.ipaddressy = socket.gethostbyname(ipaddr[1])
        if len(ipaddr) == 3 :
            self.ipaddressz = socket.gethostbyname(ipaddr[2])
        self.mySocketx = None
        self.mySockety = None
        self.mySocketz = None

        if type(delimit) is tuple :
            self.delimiterx = delimit[0]
            self.delimitery = delimit[1]
            self.delimiterz = delimit[0]
            if len(delimit) == 3 :
                self.delimiterz = delimit[2]
        else :
            self.delimiterx =  self.delimitery = self.delimiterz = delimit

        self.portnumx = 7000 if portnumber[0] is None else portnumber[0]
        self.portnumy = 7000 if portnumber[1] is None else portnumber[1]
        self.portnumz = 7000
        if len(portnumber) == 3 and portnumber[2] is not None :
            self.portnumz = portnumber[2]
        try :
            self.portnumx = int(self.portnumx)
            self.portnumy = int(self.portnumy)
            self.portnumz = int(self.portnumz)
        except ValueError as ve :
            print("ValueError on portnumber '{}' -- {}\n".format(portnumber, repr(ve)))
            self.portnumx = 7000
            self.portnumy = 7000
            self.portnumz = 7000
        self.debug = debug

    def sendData(self, sendthisx, sendthisy) :
        """ sendData -- Send the input string to the ipaddress and port
                        of the actuator.
        """
        retvalx = True
        retvaly = True
        if self.mySocketx is None and sendthisx is not None :
            self.mySocketx = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                self.mySocketx.connect((self.ipaddressx, self.portnumx))
                self.mySocketx.settimeout(0.1)
            except :
                if self.debug :
                    print("There was a connect exception ")
                retvalx = False
        if self.mySockety is None and sendthisy is not None :
            self.mySockety = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                self.mySockety.connect((self.ipaddressy, self.portnumy))
                self.mySockety.settimeout(0.1)
            except :
                if self.debug :
                    print("There was a connect exception ")
                retvaly = False
        if self.debug :
            print("Xsock={} -- Ysock={}".format(repr(self.mySocketx), repr(self.mySockety)))

        if retvalx and self.mySocketx is not None and sendthisx is not None :
            self.mySocketx.send( sendthisx + "\r\n" )
            if self.debug :
                print("sent X")
        if retvaly and self.mySockety is not None and sendthisy is not None :
            self.mySockety.send( sendthisy + "\r\n" )
            if self.debug :
                print("sent Y")

        return retvalx, retvaly

    def getData(self, x=None, y=None, nonBlock=False) :
        datax = "" 
        datay = ""
        dx = bytearray()
        dy = bytearray()
        cnt = 600
        if x is not None and y is not None :
            while cnt > 0 :
                try :
                    if self.mySocketx is not None and x is not None :
                        dx = bytearray(self.mySocketx.recv(8192))
                        datax += dx.decode("utf-8")
                    if self.mySockety is not None and y is not None :
                        dy = bytearray(self.mySockety.recv(8192))
                        datay += dy.decode("utf-8")
                except socket.timeout :
                    if self.debug :
                        print(cnt)
                    cnt -= 1
                except socket.error as se :
                    data = "Error -- {}".format(repr(se))
                    if self.debug :
                        print("SocketError -- error No. {} -- {}\n".format(repr(se), data))
                    sys.stdout.flush()
                if (self.mySocketx is None or 'SUCCESS' in datax) and (self.mySockety is None or 'SUCCESS' in datay) :
                    cnt = 0
                    break
                if nonBlock :
                    break

            if self.debug :
                print(datax)
                print(datay)
            datax = datax.lstrip().rstrip()
            datay = datay.lstrip().rstrip()
            if not nonBlock or cnt <= 0 :
                if self.mySocketx is not None :
                    self.mySocketx.close()
                    self.mySocketx = None
                if self.mySockety is not None :
                    self.mySockety.close()
                    self.mySockety = None

        return (datax, datay)

    def sendGetData(self, sendthisx, sendthisy, nonblock=False) :
        rtnx, rtny = self.sendData(sendthisx,sendthisy)
        valx = sendthisx if rtnx else None
        valy = sendthisy if rtnx else None
        datax, datay = self.getData(valx, valy, nonblock)
        return datax, datay

    def sendGetDataX(self, sendthis, nonblock=False) :
        if type(sendthis) is list :
            sendTHIS = self.makeCommandStringFromList(sendthis, 0)
        else :
            sendTHIS = sendthis
        rtn, _ = self.sendData(sendTHIS, None)
        val = sendTHIS if rtn else None
        data, _ = self.getData(val, None, nonblock)
        return data

    def sendGetDataY(self, sendthis, nonblock=False) :
        if type(sendthis) is list :
            sendTHIS = self.makeCommandStringFromList(sendthis, 0)
        else :
            sendTHIS = sendthis
        _, rtn = self.sendData(None, sendTHIS)
        val = sendTHIS if rtn else None
        _, data = self.getData(None, val, nonblock)
        return data

    def makeCommandStringFromList(self, strlist, y=None) :
        """ sendCommandString -- Create a command string from a list of strings and return it.
                                 input arg is a list containing strings.
        """
        strval = None
        if self.debug :
            print("makeCommandStringFromList -- Argument list strlist={}".format(repr(strlist)))
        if len(strlist) > 0 and strlist[0] is not None :
            strval = COMMANDVAL
            delimit = self.delimiterx if y is None else self.delimitery
            for i in range(len(strlist)) :
                strval += delimit + strlist[i]
        return strval

    def makeCommandString(self, **kwargs) :
        """ sendCommandString -- Create a command string from input args and return it.
                                 input args are arg0=, arg1=, etc.
        """
        if self.debug :
            print("Argument list kwargs={}".format(repr(kwargs)))
        strval = COMMANDVAL
        y = kwargs.get("y", None)
        delimit = self.delimiterx if y is None else self.delimitery
        for i in range(len(kwargs)) :
            arg = 'arg{}'.format(i)
            if kwargs.get(arg, None) is not None :
                strval += delimit+kwargs.get(arg, "")
        return strval


    def sendGetCommandLists(self, strlists) :
        Lists = strlists
        debugVal = type(Lists[0])
        print("debugval = {}".format(debugVal))
        if debugVal is not list :
            Lists = [strlists]
        lstStr = [None, None]
        for i in range(len(Lists)) :
            try :
                if self.debug :
                    print("TandomXY List of str vals = '{}'".format(Lists[i]))
                lstStr[i] = self.makeCommandStringFromList(Lists[i])
                if self.debug :
                    print("TandomXY String val from list = '{}'".format(lstStr[i]))
            except IndexError as ie :
                pass
        datax, datay = self.sendGetData(lstStr[0], lstStr[1])
        return datax, datay

    def sendDataZ(self, sendthis) :
        """ sendData -- Send the input string to the ipaddress and port
                        of the actuator.
        """
        retval = True
        self.mySocketz = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        try :
            self.mySocketz.connect((self.ipaddressz, self.portnumz))
            self.mySocketz.settimeout(0.1)
        except :
            if self.debug :
                print("There was a connect exception ")
            retval = False

        if self.debug :
            print("Zsock={}".format(repr(self.mySocketz)))

        if retval and self.mySocketz is not None and sendthis is not None :
            self.mySocketz.send( sendthis + "\r\n" )
            if self.debug :
                print("sent Z")

        return retval

    def sendGetDataZ(self, sendthis, nonblock=False) :
        if type(sendthis) is list :
            sendTHIS = self.makeCommandStringFromList(sendthis, 0)
        else :
            sendTHIS = sendthis
        data = ""
        if self.sendDataZ(sendTHIS) :
            data = self.getDataZ(nonblock)
        return data

    def getDataZ(self, nonBlock=False) :
        dataz = "" 
        dz = bytearray()
        cnt = 600
        while cnt > 0 :
            try :
                if self.mySocketz is not None and x is not None :
                    dz = bytearray(self.mySocketz.recv(8192))
                    dataz += dz.decode("utf-8")
            except socket.timeout :
                if self.debug :
                    print(cnt)
                cnt -= 1
            except socket.error as se :
                dataz = "Error -- {}".format(repr(se))
                if self.debug :
                    print("SocketError -- error No. {} -- {}\n".format(repr(se), dataz))
                sys.stdout.flush()
            if (self.mySocketz is None or 'SUCCESS' in dataz) :
                cnt = 0
                break
            if nonBlock :
                break

        if self.debug :
            print(dataz)
        dataz = dataz.lstrip().rstrip()
        if not nonBlock or cnt <= 0 :
            if self.mySocketz is not None :
                self.mySocketz.close()
                self.mySocketz = None

        return dataz

if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        a = TandomXYZ(debug=True)

        strxy = a.makeCommandStringFromList(['getType'])
        vx, vy = a.sendGetData(strxy, strxy)
        print ("X type is '{}'".format(vx))
        print ("Y type is '{}'".format(vy))

        from TanXY.unittestlist import testlist

        for testdict in testlist :
            try :
                xList = testdict.get("xlist", [None])
                yList = testdict.get("ylist", [None])
                print("xList='{}'".format(xList))
                print("yList='{}'".format(yList))
                x, y = a.sendGetCommandLists([xList, yList])
                print("x='{}'".format(x))
                print("y='{}'".format(y))
                sleep(1)
            except KeyboardInterrupt :
                break

        
   

