""" JSonCmdStrings.py: A dictionary and set of methods to provide communication command strings
and interpretation of the returned value with the Message Manager software running on an Ossia Inc.
Transmiter.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import json
import traceback, sys

''' Set of strings take from the Message Manager server C-Code to communicate with the Message Manager.
'''
JSON_CMD_DICT_NEWD = {
'start_charging'        : {'jstr' : '{"Command" : {"Type": "StartChargingDevices", "Devices": ["%s"]},                   "Result" : ""}', 'numofargs' : 1},
'proxy_command'         : {'jstr' : '{"Command" : {"Type": "Proxy Command", "Data": ["%s"]},                             "Result" : ""}', 'numofargs' : 1},
'client_data'           : {'jstr' : '{"Command" : {"Type": "GetClientData", "Client ID": "%s"},                          "Result" : ""}', 'numofargs' : 1},
'shutdown'              : {'jstr' : '{"Command" : {"Type": "Shutdown CCB"},                                              "Result" : ""}', 'numofargs' : 0},
'get_power_level'       : {'jstr' : '{"Command" : {"Type": "GetPowerLevel"},                                             "Result" : ""}', 'numofargs' : 0},
'pause'                 : {'jstr' : '{"Command" : {"Type": "Pause"},                                                     "Result" : ""}', 'numofargs' : 0},
'send_tps'              : {'jstr' : '{"Command" : {"Type": "Send TPS"},                                                  "Result" : ""}', 'numofargs' : 0},
'motion_config'         : {'jstr' : '{"Command" : {"Type": "SetMotionEngine", "Sensitivity": "%s"},                      "Result" : ""}', 'numofargs' : 1},
'send_cqt'              : {'jstr' : '{"Command" : {"Type": "Send CQT"},                                                  "Result" : ""}', 'numofargs' : 0},
'devices_in_range'      : {'jstr' : '{"Command" : {"Type": "GetAllDevicesInRange"},                                      "Result" : ""}', 'numofargs' : 0},
'send_tpc'              : {'jstr' : '{"Command" : {"Type": "Send TPC", "Client ID": "%s"},                               "Result" : ""}', 'numofargs' : 1},
'send_disc'             : {'jstr' : '{"Command" : {"Type": "Send Discovery Message"},                                    "Result" : ""}', 'numofargs' : 0},
'reset'                 : {'jstr' : '{"Command" : {"Type": "Reset Array"},                                               "Result" : ""}', 'numofargs' : 0},
'normalize_phase'       : {'jstr' : '{"Command" : {"Type": "NormalizePhase", "Client ID": "%s", "QueueSize": "%s"},      "Result" : ""}', 'numofargs' : 2},
'motion_devices'        : {'jstr' : '{"Command" : {"Type": "TrackMotionForDevices", "Devices": "%s"},                    "Result" : ""}', 'numofargs' : 1},
'get_good_channels'     : {'jstr' : '{"Command" : {"Type": "Get Good Channels"},                                         "Result" : ""}', 'numofargs' : 0},
'info'                  : {'jstr' : '{"Command" : {"Type": "Get System Info"},                                           "Result" : ""}', 'numofargs' : 0},
'versions'              : {'jstr' : '{"Command" : {"Type": "GetChargerFirmwareVersion"},                                 "Result" : ""}', 'numofargs' : 0},
'reset_proxy'           : {'jstr' : '{"Command" : {"Type": "Reset Proxy"},                                               "Result" : ""}', 'numofargs' : 0},
'client_command'        : {'jstr' : '{"Command" : {"Type": "Client Command", "Data": ["%s"], "Client ID": "%s"},         "Result" : ""}', 'numofargs' : 2},
'client_config'         : {'jstr' : '{"Command" : {"Type": "SetClientConfig", "QueryType": "%s", "Client ID": "%s"},     "Result" : ""}', 'numofargs' : 2},
'identify_charger'      : {'jstr' : '{"Command" : {"Type": "IdentifyCharger"},                                           "Result" : ""}', 'numofargs' : 0},
'channel_info'          : {'jstr' : '{"Command" : {"Type": "Get Channel Info", "Channel Number": "%s"},                  "Result" : ""}', 'numofargs' : 1},
'proxy_command_data'    : {'jstr' : '{"Command" : {"Type": "Proxy Command Data"},                                        "Result" : ""}', 'numofargs' : 0},
'start_sending_data'    : {'jstr' : '{"Command" : {"Type": "StartSendingData", "Devices": ["%s"], "Interval": "%s"},     "Result" : ""}', 'numofargs' : 2},
'add_client'            : {'jstr' : '{"Command" : {"Type": "Add Client", "Client ID": "%s"},                             "Result" : ""}', 'numofargs' : 1},
'client_command_data'   : {'jstr' : '{"Command" : {"Type": "Client Command Data", "Client ID": "%s"},                    "Result" : ""}', 'numofargs' : 1},
'get_system_state'      : {'jstr' : '{"Command" : {"Type": "GetSystemState"},                                            "Result" : ""}', 'numofargs' : 0},
'calibrate'             : {'jstr' : '{"Command" : {"Type": "Calibrate"},                                                 "Result" : ""}', 'numofargs' : 0},
'debug_log'             : {'jstr' : '{"Command" : {"Type": "Debug Log", "Level": "%s", "Mask": "%s", "Component": "%s"}, "Result" : ""}', 'numofargs' : 3},
'reset_FPGA'            : {'jstr' : '{"Command" : {"Type": "Reset FPGA"},                                                "Result" : ""}', 'numofargs' : 0},
'get_devices_status'    : {'jstr' : '{"Command" : {"Type": "GetDevicesStatus", "Devices": ["%s"]},                       "Result" : ""}', 'numofargs' : 1},
'get_system_temp'       : {'jstr' : '{"Command" : {"Type": "GetSystemTemp"},                                             "Result" : ""}', 'numofargs' : 0},
'client_list'           : {'jstr' : '{"Command" : {"Type": "Get List of Clients"},                                       "Result" : ""}', 'numofargs' : 0},
'remove_client'         : {'jstr' : '{"Command" : {"Type": "Remove Client", "Client ID": "%s"},                          "Result" : ""}', 'numofargs' : 1},
'reboot'                : {'jstr' : '{"Command" : {"Type": "Reboot CCB"},                                                "Result" : ""}', 'numofargs' : 0},
'set_power_level'       : {'jstr' : '{"Command" : {"Type": "SetPowerLevel", "PowerLevel": "%s"},                         "Result" : ""}', 'numofargs' : 1},
'client_leave'          : {'jstr' : '{"Command" : {"Type": "Client Leave", "Client ID": "%s"},                           "Result" : ""}', 'numofargs' : 1},
'stop_disc'             : {'jstr' : '{"Command" : {"Type": "Stop Discovery Message"},                                    "Result" : ""}', 'numofargs' : 0},
'run'                   : {'jstr' : '{"Command" : {"Type": "Run"},                                                       "Result" : ""}', 'numofargs' : 0},
'proxy_info'            : {'jstr' : '{"Command" : {"Type": "Get Proxy Info"},                                            "Result" : ""}', 'numofargs' : 0},
'stop_charging'         : {'jstr' : '{"Command" : {"Type": "StopChargingDevices", "Devices": ["%s"]},                    "Result" : ""}', 'numofargs' : 1},
'get_charging_info'     : {'jstr' : '{"Command" : {"Type": "GetDeviceChargeInfo", "Devices": "%s"},                      "Result" : ""}', 'numofargs' : 1},
'channel_off'           : {'jstr' : '{"Command" : {"Type": "Channel OFF", "Channel Number": "%s"},                       "Result" : ""}', 'numofargs' : 1},
'identify_client'       : {'jstr' : '{"Command" : {"Type": "Identify Client", "Duration": "15", "Client ID": "%s"},      "Result" : ""}', 'numofargs' : 2},
'client_sleep'          : {'jstr' : '{"Command" : {"Type": "Client Sleep", "Client ID": "%s"},                           "Result" : ""}', 'numofargs' : 1},
'get_charger_id'        : {'jstr' : '{"Command" : {"Type": "GetChargerId"},                                              "Result" : ""}', 'numofargs' : 0},
'register_client'       : {'jstr' : '{"Command" : {"Type": "Register Client", "Client ID": "%s"},                        "Result" : ""}', 'numofargs' : 1},
'client_detail'         : {'jstr' : '{"Command" : {"Type": "Get Client Detail", "Client ID": "%s"},                      "Result" : ""}', 'numofargs' : 1},
'stop_sending_data'     : {'jstr' : '{"Command" : {"Type": "StopSendingData", "Devices": ["%s"]},                        "Result" : ""}', 'numofargs' : 1},
'channel_on'            : {'jstr' : '{"Command" : {"Type": "Channel ON", "Channel Number": "%s"},                        "Result" : ""}', 'numofargs' : 1},
'set_good_channels'     : {'jstr' : '{"Command" : {"Type": "Set Good Channels", "Good Channels": "%s"},                  "Result" : ""}', 'numofargs' : 1},
}

JSON_CMD_DICT_org = {
'channel_on': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Channel ON"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'channel_off': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Channel OFF"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'calibrate': {'jstr': '{"Command":{"Type":"Calibrate"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'add_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Add Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'register_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Register Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'remove_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Remove Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'identify_client': {'jstr': '{"Command":{"Client ID":"%s","Duration":15,"Type":"Identify Client"},"Result":{"Status":"NULL"}}', 'numofargs': 2},
'client_command': {'jstr': '{"Command":{"Client ID":"%s","Data":"%s","Type":"Client Command"},"Result":{"Status":"NULL"}}', 'numofargs': 2},
'client_command_data': {'jstr': '{"Command":{"Client ID":"%s","Type":"Client Command Data"},"Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'set_good_channels': {'jstr': '{"Command":{"Good Channels":"%s","Type":"Set Good Channels"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'run': {'jstr': '{"Command":{"Type":"Run"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'pause': {'jstr': '{"Command":{"Type":"Pause"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reboot': {'jstr': '{"Command":{"Type":"Reboot CCB"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_charger_id': {'jstr': '{"Command":{"Type":"GetChargerId"},"Result":{"Status":"NULL", "ChargerId": "xxxx"}}', 'numofargs': 0},
'reset': {'jstr': '{"Command":{"Type":"Reset Array"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_FPGA': {'jstr': '{"Command":{"Type":"Reset FPGA"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_proxy': {'jstr': '{"Command":{"Type":"Reset Proxy"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'shutdown': {'jstr': '{"Command":{"Type":"Shutdown CCB"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_bbs': {'jstr': '{"Command":{"Type":"Send BBS"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_cqt': {'jstr': '{"Command":{"Type":"Send CQT"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_disc': {'jstr': '{"Command":{"Type":"Send Discovery Message"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_tpc': {'jstr': '{"Command":{"Client ID":"%s","Type":"Send TPC"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'send_tps': {'jstr': '{"Command":{"Type":"Send TPS"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'stop_disc': {'jstr': '{"Command":{"Type":"Stop Discovery Message"},"Result":{"Status":"NULL", "AMBS":[]}}', 'numofargs': 0},
'info': {'jstr': '{"Command":{"Type":"Get System Info"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_good_channels': {'jstr': '{"Command":{"Type":"Get Good Channels"},"Result":{"Good Channels":"NULL"}}', 'numofargs': 0},
'channel_info': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Get Channel Info"},"Result":{"AMB Revision":0,"Status":"Off"}}', 'numofargs': 1},
'proxy_info': {'jstr': '{"Command":{"Type":"Get Proxy Info"},"Result":{"Proxy FW Revision":0}}', 'numofargs': 0},
'versions': {'jstr': '{"Command":{"Type":"GetChargerFirmwareVersion"},"Result":{"Daemon Build Info":{"Date":0,"Number":0},"Driver Lib Build Info":{"Date":0,"Number":0},"FPGA Revision":0,"Message Manager Build Info":{"Date":0,"Number":0},"OS Version":"0","Proxy FW Revision":0,"Release Version":0}}', 'numofargs': 0},
'client_list': {'jstr': '{"Command":{"Type":"Get List of Clients"},"Result":{"Clients":[{"Short ID":"1","Client ID":"2","Version":"3","Status":"0","LinkQuality":"34"}]}}', 'numofargs': 0},
'client_detail': {'jstr': '{"Command":{"Client ID":"%s","Type":"Get Client Detail"},"Result":{"LinkQuality": 0, "QueryFailedCount": 0, "Status": "", "AveragePower": 0, "PeakPower": 0, "NetCurrent": 0, "Short ID": "0", "ProxyLinkQuality": 0, "RSSIValue": 0, "QueryTime": 0, "QueryType": 0, "DeviceStatus": 0, "Client ID": "", "State": 0, "Version": 0, "TPSMissed": 0, "Model": 0, "BatteryLevel": 0, "ProxyRSSIValue": 0}}', 'numofargs': 1},
'client_data': {'jstr': '{"Command":{"Client ID":"%s","Type":"GetClientData"},"Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'client_config': {'jstr': '{"Command":{"Client ID":"%s","QueryType":"%s","Type":"SetClientConfig"},"Result":{"Status":"NULL"}}', 'numofargs': 2},
'devices_in_range': {'jstr': '{"Command":{"Type":"GetAllDevicesInRange"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 0},
'check_in_range': {'jstr': '{"Command":{"Client ID":"%s","Type":"CheckDeviceInRange"},"Result":{"BatteryLevel":0,"Client ID":"0x0","Status":"NULL"}}', 'numofargs': 1},
'check_status': {'jstr': '{"Command":{"Client ID":"%s","Type":"CheckDeviceChargingStatus"},"Result":{"BatteryLevel":0,"Client ID":"0x0","Status":"NULL"}}', 'numofargs': 1},
'start_charging': {'jstr': '{"Command":{"Devices":["%s"],"Type":"StartChargingDevices"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'stop_charging': {'jstr': '{"Command":{"Devices":["%s"],"Type":"StopChargingDevices"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'get_charging_info': {'jstr': '{"Command":{"Devices":"%s","Type":"GetDeviceChargeInfo"},"Result":{"Devices":[{"AveragePower":0,"Client ID":"0x0","DeviceStatus":0,"LinkQuality":0,"NetCurrent":0,"PeakPower":0,"RSSIValue":0}],"Status":"NULL"}}', 'numofargs': 1},
'motion_config': {'jstr': '{"Command":{"Sensitivity":"%s","Type":"SetMotionEngine"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'motion_devices': {'jstr': '{"Command":{"Devices":"%s","Type":"TrackMotionForDevices"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'set_power_level': {'jstr': '{"Command":{"PowerLevel":"%s","Type":"SetPowerLevel"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'get_power_level': {'jstr': '{"Command":{"Type":"GetPowerLevel"},"Result":{"PowerLevel":22,"Status":"NULL"}}', 'numofargs': 0},
'get_system_temp': {'jstr': '{"Command":{"Type":"GetSystemTemp"},"Result":{"Status":"NULL","Temp":0}}', 'numofargs': 0},
'identify_charger': {'jstr': '{"Command":{"Type":"IdentifyCharger"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
}

JSON_CMD_DICT = {
'channel_on'          : {'jstr': '{"Command":{"Type":"Channel ON","Channel Number":"%s"},                    "Result":{"Status":"NULL"}}', 'numofargs': 1},
'channel_off'         : {'jstr': '{"Command":{"Type":"Channel OFF","Channel Number":"%s"},                   "Result":{"Status":"NULL"}}', 'numofargs': 1},
'calibrate'           : {'jstr': '{"Command":{"Type":"Calibrate"},                                           "Result":{"Status":"NULL"}}', 'numofargs': 0},
'add_client'          : {'jstr': '{"Command":{"Type":"Add Client","Client ID":"%s"},                         "Result":{"Status":"NULL"}}', 'numofargs': 1},
'register_client'     : {'jstr': '{"Command":{"Type":"Register Client","Client ID":"%s"},                    "Result":{"Status":"NULL"}}', 'numofargs': 1},
'remove_client'       : {'jstr': '{"Command":{"Type":"Remove Client","Client ID":"%s"},                      "Result":{"Status":"NULL"}}', 'numofargs': 1},
'identify_client'     : {'jstr': '{"Command":{"Type":"Identify Client","Client ID":"%s","Duration":"15"},    "Result":{"Status":"NULL"}}', 'numofargs': 2},
'client_command'      : {'jstr': '{"Command":{"Type":"Client Command","Client ID":"%s","Data":["%s"]},       "Result":{"Status":"NULL"}}', 'numofargs': 2},
'client_command_data' : {'jstr': '{"Command":{"Type":"Client Command Data","Client ID":"%s"},                "Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'set_good_channels'   : {'jstr': '{"Command":{"Type":"Set Good Channels","Good Channels":"%s"},              "Result":{"Status":"NULL"}}', 'numofargs': 1},
'run'                 : {'jstr': '{"Command":{"Type":"Run"},                                                 "Result":{"Status":"NULL"}}', 'numofargs': 0},
'pause'               : {'jstr': '{"Command":{"Type":"Pause"},                                               "Result":{"Status":"NULL"}}', 'numofargs': 0},
'reboot'              : {'jstr': '{"Command":{"Type":"Reboot CCB"},                                          "Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_charger_id'      : {'jstr': '{"Command":{"Type":"GetChargerId"},                                        "Result":{"Status":"NULL", "ChargerId": "xxxx"}}', 'numofargs': 0},
'reset'               : {'jstr': '{"Command":{"Type":"Reset Array"},                                         "Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_FPGA'          : {'jstr': '{"Command":{"Type":"Reset FPGA"},                                          "Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_proxy'         : {'jstr': '{"Command":{"Type":"Reset Proxy"},                                         "Result":{"Status":"NULL"}}', 'numofargs': 0},
'shutdown'            : {'jstr': '{"Command":{"Type":"Shutdown CCB"},                                        "Result":{"Status":"NULL"}}', 'numofargs': 0},
'client_leave'        : {'jstr': '{"Command":{"Type":"Client Leave","Client ID":"%s"},                       "Result":{"Status":"NULL"}}', 'numofargs': 1},
'client_sleep'        : {'jstr': '{"Command":{"Type":"Client Sleep","Client ID":"%s"},                       "Result":{"Status":"NULL"}}', 'numofargs': 1},
'send_cqt'            : {'jstr': '{"Command":{"Type":"Send CQT"},                                            "Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_disc'           : {'jstr': '{"Command":{"Type":"Send Discovery Message"},                              "Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_tpc'            : {'jstr': '{"Command":{"Type":"Send TPC","Client ID":"%s"},                           "Result":{"Status":"NULL"}}', 'numofargs': 1},
'send_tps'            : {'jstr': '{"Command":{"Type":"Send TPS"},                                            "Result":{"Status":"NULL"}}', 'numofargs': 0},
'stop_disc'           : {'jstr': '{"Command":{"Type":"Stop Discovery Message"},                              "Result":{"Status":"NULL", "AMBS":[]}}', 'numofargs': 0},
'info'                : {'jstr': '{"Command":{"Type":"Get System Info"},                                     "Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_good_channels'   : {'jstr': '{"Command":{"Type":"Get Good Channels"},                                   "Result":{"Good Channels":"NULL"}}', 'numofargs': 0},
'channel_info'        : {'jstr': '{"Command":{"Type":"Get Channel Info","Channel Number":"%s"},              "Result":{"AMB Revision":0,"Status":"Off"}}', 'numofargs': 1},
'proxy_info'          : {'jstr': '{"Command":{"Type":"Get Proxy Info"},                                      "Result":{"Proxy FW Revision":0}}', 'numofargs': 0},
'versions'            : {'jstr': '{"Command":{"Type":"GetChargerFirmwareVersion"},                           "Result":{"Daemon Build Info":{"Date":0,"Number":0},"Driver Lib Build Info":{"Date":0,"Number":0},"FPGA Revision":0,"Message Manager Build Info":{"Date":0,"Number":0},"OS Version":"0","Proxy FW Revision":0,"Release Version":0}}', 'numofargs': 0},
'client_list'         : {'jstr': '{"Command":{"Type":"Get List of Clients"},                                 "Result":{"Clients":[{"Short ID":"1","Client ID":"2","Version":"3","Status":"0","LinkQuality":"34"}]}}', 'numofargs': 0},
'client_detail'       : {'jstr': '{"Command":{"Type":"Get Client Detail","Client ID":"%s"},                  "Result":{"LinkQuality": 0, "QueryFailedCount": 0, "Status": "", "AveragePower": 0, "PeakPower": 0, "NetCurrent": 0, "Short ID": "0", "ProxyLinkQuality": 0, "RSSIValue": 0, "QueryTime": 0, "QueryType": 0, "DeviceStatus": 0, "Client ID": "", "State": 0, "Version": 0, "TPSMissed": 0, "Model": 0, "BatteryLevel": 0, "ProxyRSSIValue": 0}}', 'numofargs': 1},
'normalize_phase'     : {'jstr': '{"Command":{"Type":"NormalizePhase","Client ID":"%s","QueueSize":0},       "Result":{"Status": "NULL"}}', 'numofargs': 1},
'client_data'         : {'jstr': '{"Command":{"Type":"GetClientData","Client ID":"%s"},                      "Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'client_config'       : {'jstr': '{"Command":{"Type":"SetClientConfig","Client ID":"%s","QueryType":"%s"},   "Result":{"Status":"NULL"}}', 'numofargs': 2},
'debug_log'           : {'jstr': '{"Command":{"Type":"Debug Log","Component":"%s","Level":"%s","Mask":"%s"}, "Result":{"Status":"NULL"}}', 'numofargs': 3},
'devices_in_range'    : {'jstr': '{"Command":{"Type":"GetAllDevicesInRange"},                                "Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 0},
'get_system_state'    : {'jstr': '{"Command":{"Type":"GetSystemState"},                                      "Result":{"State":0,"Status":"NULL"}}', 'numofargs': 0},
'get_devices_status'  : {'jstr': '{"Command":{"Type":"GetDevicesStatus","Devices":["%s"]},                   "Result":{"BatteryLevel":0,"Client ID":"0x0","Status":"NULL"}}', 'numofargs': 1},
'start_charging'      : {'jstr': '{"Command":{"Type":"StartChargingDevices","Devices":["%s"]},               "Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'stop_charging'       : {'jstr': '{"Command":{"Type":"StopChargingDevices","Devices":["%s"]},                "Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'get_charging_info'   : {'jstr': '{"Command":{"Type":"GetDeviceChargeInfo","Devices":"%s"},                  "Result":{"Devices":[{"AveragePower":0,"Client ID":"0x0","DeviceStatus":0,"LinkQuality":0,"NetCurrent":0,"PeakPower":0,"RSSIValue":0}],"Status":"NULL"}}', 'numofargs': 1},
'motion_config'       : {'jstr': '{"Command":{"Type":"SetMotionEngine","Sensitivity":"%s"},                  "Result":{"Status":"NULL"}}', 'numofargs': 1},
'motion_devices'      : {'jstr': '{"Command":{"Type":"TrackMotionForDevices","Devices":"%s"},                "Result":{"Status":"NULL"}}', 'numofargs': 1},
'set_power_level'     : {'jstr': '{"Command":{"Type":"SetPowerLevel","PowerLevel":"%s"},                     "Result":{"Status":"NULL"}}', 'numofargs': 1},
'get_power_level'     : {'jstr': '{"Command":{"Type":"GetPowerLevel"},                                       "Result":{"PowerLevel":22,"Status":"NULL"}}', 'numofargs': 0},
'get_system_temp'     : {'jstr': '{"Command":{"Type":"GetSystemTemp"},                                       "Result":{"Status":"NULL","Temp":0}}', 'numofargs': 0},
'identify_charger'    : {'jstr': '{"Command":{"Type":"IdentifyCharger"},                                     "Result":{"Status":"NULL"}}', 'numofargs': 0},
'proxy_command'       : {'jstr': '{"Command":{"Type":"Proxy Command","Data":["%s"]},                         "Result":{"Status":"NULL"}}', 'numofargs': 1},
'proxy_command_data'  : {'jstr': '{"Command":{"Type":"Proxy Command Data"},                                  "Result":{"Status":"NULL","Data":["%s"]}}', 'numofargs': 0},
'start_sending_data'  : {'jstr': '{"Command":{"Type":"StartSendingData","Devices":["%s"],"Interval":"%s"},   "Result":{"Status":"NULL"}}', 'numofargs': 2},
'stop_sending_data'   : {'jstr': '{"Command":{"Type":"StopSendingData","Devices":["%s"]},                    "Result":{"Status":"NULL"}}', 'numofargs': 1},
'get_distance'        : {'jstr': '{"Command":{"Type":"Get Distance","Client ID":"%s"},                       "Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'set_com_channel'     : {'jstr': '{"Command":{"Type":"Set COM Channel","COM Channel":"%s","Client ID" : "%s" },"Result" : { "Status" : "NULL" }}', 'numofargs' : 2},
'get_com_channel'     : {'jstr': '{"Command":{"Type":"Get COM Channel"},                                     "Result" : { "COM Channel" : 0 }}', 'numofargs' : 0},
'lpm_assign'          : {'jstr': '{"Command":{"Type":"Lpm Assign","Client ID":"%s","Slot Number":"%s"},      "Result" : { "Status" : "NULL" }}', 'numofargs' : 2},
'lpm_free'            : {'jstr': '{"Command":{"Type":"Lpm Free","Slot Number":"%s"},                         "Result" : { "Status" : "NULL" }}', 'numofargs' : 1},
'lpm_list'            : {'jstr': '{"Command":{"Type":"Lpm List"},                                            "Result" : { "Status" : "NULL", "Number" : "0", "Slots":[] }}', 'numofargs' : 0},
'client_lpm'          : {'jstr': '{"Command":{"Type":"Client Lpm","Client ID":"%s","State":"%s"},            "Result" : { "Status" : "NULL" }}', 'numofargs' : 2},
'lpm'                 : {'jstr': '{"Command":{"Type":"Lpm","State":"%s"},                                    "Result" : { "Status" : "NULL" }}', 'numofargs' : 1},
'lpm_slots'           : {'jstr': '{"Command":{"Type":"Lpm Slots","Slots":"%s"},                              "Result" : { "Status" : "NULL" }}', 'numofargs' : 1},
'rssi_spew'           : {'jstr': '{"Command":{"Type":"RSSI Spew","Slot Number":"%s"},                        "Result" : { "Status" : "NULL" }}', 'numofargs' : 1},
'restart'             : {'jstr': '{"Command":{"Type":"Restart"},                                             "Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 0},

}

def try_exceptDec(func) :
    ''' Decorator to wrap all these functions in a try except pair
    '''
    def wrapper(*args, **kwargs) :
        try :
            return func(*args, **kwargs)
        except Exception as ex :
            print("Unexpected Exception --- {} -- args={}\n kwargs={}\n".format(repr(ex), repr(args), repr(kwargs)))
            print("Traceback -- {} \n".format(traceback.format_exc()))
            print("Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    return wrapper

def getTxId(pyData) :
    ''' getTxId -- requires data from a previous call to get_charger_id
    returns the Transmitters idetification string.
    '''
    rtnval = pyData['Result'].get('ChargerId', "NotFound")
    return rtnval

def checkForSuccess(pyData, ckVal='SUCCESS') :
    ''' checkForSuccess -- returns true if the status of in input data does NOT match the ckVal.
    False otherwise.
    '''
    rtnval = False
    if processSingleReturn(pyData, 'Status') != ckVal :
        rtnval =  True
    return rtnval

client_stat_dict = {
    0 : 'UNKNOWN - Not found or out of range.',
    1 : 'NOT_REGISTERED - Discovered but not registered yet.',
    2 : 'NOT_READY - Registered but not ready to receive power; likely client configuration failed.',
    3 : 'POWER_NOT_REQUESTED - The client is not requesting power; or the client is requesting power delivery to stop.',
    4 : 'NOT_CHARGING - Ready to receive power, but not being charged currently.',
    5 : 'CHARGING - In power cycle and charging.',
    6 : 'LIMITED_CHARGING - Power is being delivered but the client reported not receiving power or low power.',
    7 : 'PAUSED - The system is temporarily paused by users.',
    8 : 'SYSTEM_ERROR - The system has encountered an error condition that it could not continue; users intervention is needed.',
    9 : 'COMM_ERROR - Communication error. The system is not able to communicate with the client to check on its routine status thus temporarily stop delivering power to the client until communication can be re-established.',
   10 : 'NOT_CHARGING',
   11 : 'CHARGING',
   12 : 'LIMITED_CHARGING',
   13 : 'Unknown Client status',
   14 : 'Error: No Status entry in returned json',
   15 : 'SUCCESS: Old Status entry in returned json',
    }
status_good_list = [1, 3, 4, 5, 6, 10, 11, 12]
def checkStatusReturn(pyData) :
    """ requires a return dict from one of the following commands:
        start_charging *
        stop_charging *
        get_device_status
        devices_in_range
        client_detail *
        client_list 
        * are the ones currently used
        returns False indicates that the check failed
                True  indicates all is well.
    """
    aStat = 15
    rtnval = False
    aClient = pyData['Result'].get('Devices', pyData['Result'])
    if type(aClient) is list :
        aClient = aClient[0] # assumption: only one in the list

    try :
        aStat = int(aClient.get('Status', 14))
    except ValueError :
        if aClient['Status'] == "NOT_CHARGING" :
            aStat = 10
        elif aClient['Status'] == "CHARGING" :
            aStat = 11
        elif aClient['Status'] == "LIMITED_CHARGING" :
            aStat = 12
        elif aClient['Status'] == "SUCCESS" :
            aStat = 15
            rtnval = True
        else :
            aStat = 13

    if aStat in status_good_list :
        rtnval = True 

    return rtnval, aStat, client_stat_dict[aStat]

def processClientList(pyData, clientList, newold=False) :
    ''' processClientList -- Checks the requested clients against what the charger finds.
    '''
    rtnval = True
    rtndict = dict()
    try :
        availList = pyData['Result']['Clients']
        l_clientList = list()
        l_clientStat = dict()
        for aClient in availList :
            try :
                aCid  = str(aClient['Client ID'].strip())
                aLnkQ = aClient['LinkQuality']
            except ValueError as ve :
                rtnval = False
                print("ValueError in processClientList -- {}".format(repr(ve)))
                break

            try :
                aStat = int(aClient['Status'])
            except ValueError :
                if newold :
                    rtnval = False
                if aClient['Status'] == "NOT_CHARGING" :
                    aStat = 10
                elif aClient['Status'] == "CHARGING" :
                    aStat = 11
                else :
                    aStat = 12
                    aLnkQ = aClient['Status']

            l_clientList.append(aCid)
            l_clientStat.update({aCid : [aStat, aLnkQ]})

        if rtnval :
            rtnval = False
            for cidl in clientList :
                cid = cidl
                als = ""
                if type(cidl) is list or type(cidl) is tuple :
                    cid = cidl[0]
                    als = cidl[1] 
                if cid in l_clientList :
                    theStat = l_clientStat.get(cid, [0,0])[0] 
                    if theStat in status_good_list :
                        rtnval = True
                    rtndict.update({cid : l_clientStat.get(cid, [0, 0]) + [als]})
                else :
                    rtndict.update({cid : [0, 0, als]})
    except Exception as ex :
        print("Unexpected Exception in jcs.processClientList --- {}\n".format(repr(ex)))
        print("Traceback -- {} \n".format(traceback.format_exc()))
        print("Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    return rtnval, rtndict

def processSingleReturn(pyData, strval) :
    rtnval = -1234 
    try :
        tmp = pyData.get('Result', None)
        if tmp is not None :
            rtnval = tmp.get(strval, -12345)
    except Exception as ex :
        rtnval =  -4567 
        print("Unexpected Exception in jcs.processSingleReturn --- {}\n".format(repr(ex)))
        print("Traceback -- {} \n".format(traceback.format_exc()))
        print("Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    return rtnval

def getComChannel(pyData) :
    """ getComChannel -- Requires data from a "get_com_channel" command
        @param: pyData the result of a json loads operation
        returns: Current COM Channel
    """

    com_chan = int(processSingleReturn(pyData, "COM Channel"))
    return com_chan

def getClientVersion(pyData) :
    """ getClientVersion -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """

    version = int(processSingleReturn(pyData, "Version"))
    return version

def getClientComRSSI(pyData) :
    """ getClientComRSSI -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """

    rssival = int(processSingleReturn(pyData, "RSSIValue"))
    return rssival

def getProxyComRSSI(pyData) :
    """ getProxyComRSSI -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """

    rssival = int(processSingleReturn(pyData, "ProxyRSSIValue"))
    return rssival

def getBatteryLevel(pyData) :
    """ getBatteryLevel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Battery Level
    """
    val =  processSingleReturn(pyData, "BatteryLevel")
    return val

def getDeviceStatus(pyData) :
    """ getDeviceStatus -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Status
    """
    val =  processSingleReturn(pyData, "DeviceStatus")
    return val

def getChanStatRev(pyData) :
    """ getChanStatRev -- Requires data from a "channel_info" operation
        @param: pyData the result of a json loads operation
        returns: a tuple of status and revision
    """
    status = processSingleReturn(pyData, "Status")
    rev    = processSingleReturn(pyData, "AMB Revision")
    return (status, rev)

def getDeviceModel(pyData) :
    """ getDeviceModel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Model number 
    """
    val =  processSingleReturn(pyData, "Model")
    return val

def getTPSMissed(pyData) :
    """ getTPSMissed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: TPSMissed
    """
    val =  processSingleReturn(pyData, "TPSMissed")
    return val

def getClientShortID(pyData) :
    """ getClientShortID -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Short ID
    """
    shortID =  processSingleReturn(pyData, "Short ID")
    return shortID

def getClientLinkQuality(pyData) :
    """ getClientLinkQuality -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: LinkQuality
    """
    linkQuality =  processSingleReturn(pyData, "LinkQuality")
    return int(linkQuality)

def getClientNetCurrent(pyData) :
    """ getClientNetCurrent -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns Net current as a float in units mA
    """
    return float(processSingleReturn(pyData, "NetCurrent"))

def getClientQueryTime(pyData) :
    """ getClientQueryTime -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns Net current as a float in units mA
    """
    return float(processSingleReturn(pyData, "QueryTime"))

def getClientQueryFailed(pyData) :
    """ getClientQueryFailed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the number of failed queries as an int 
    """
    return int(processSingleReturn(pyData, "QueryFailedCount"))

def getAveragePower(pyData) :
    """ getAveragePower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the Average power as an int 
    """
    return float(processSingleReturn(pyData, "AveragePower"))

def getPeakPower(pyData) :
    """ getPeakPower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the peak power as an float
    """
    return float(processSingleReturn(pyData, "PeakPower"))

@try_exceptDec
def getClientBatteryVoltage(pyData) :
    """ getClientBatteryVoltage -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns the Battery Voltage as a float in units Volts
    """
    bv = (int(pyData['Result']['Data'][1]) << 8) + int(pyData['Result']['Data'][0])
    return float(bv) / 1000.0

def getCommandData(pyData, prevCmdNum, offset, num, byteval=True) :
    """ getCommandData -- Requires the data from a client_command client_command_data pair
        returns an integer
    """
    rawData = processSingleReturn(pyData, 'Data')
    rtnval = 0
    try :
        for i in range(num) :
            ii = i + offset
            if byteval :
                rtnval += (int(rawData[ii]) << (8 * i))
            else :
                rtnval += (int(rawData[ii]) << i)
    except ValueError :
        print("ValueError in client command {} data {}".format(prevCmdNum, repr(rawData)))
        rtnval = -2
    except IndexError :
        print("IndexError in client command {} data {}".format(prevCmdNum, repr(rawData)))
        rtnval = -3
    return rtnval

#INDUCTANCE_UH = 8.2
#FREQUENCY_HZ = 187500
#EFFICIENCY = 0.34

#def useFormula(count, batVolt) :
    #value = (1000.0 * (count**2) * (batVolt**2))/(0x8000 * FREQUENCY_HZ * INDUCTANCE_UH * EFFICIENCY * (10**(-6)))
    #if value > 0 :
        #value = 10 * math.log10(value)
    #return value

@try_exceptDec
def processClientData(pyData) :
    """ getClientRFPower -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns: data list
    """
    rawData = processSingleReturn(pyData, 'Data')
    tLen = len(rawData)
    data = list()
    if (tLen % 2) != 0 : # just in case
        tLen -= 1
    # this takes a data list of 34 and combines hi and low order bytes
    # into a list of 17. The zeroth value is the battery voltage
    for i in range(0, tLen, 2) :
        if i < tLen :
            tmp = ((int(rawData[i+1]) << 8) + int(rawData[i]))
        else :
            tmp = -1230
        data.append(tmp)
    return data

@try_exceptDec
def getClientRFPowerMars(data, whichone) : # returns a tuple of RSSI values and the entire list
    """ getClientRFPower -- Requires a pre-processed data listfrom a "client_data" operation
        @param: data the result of a json loads operation pre-processed into a list of ints
        @param: whichone - is an index from 1 to 9. Designating a port on the antenna.
        returns: four values, as a tuple:
                 a tuple of maxZCSCount and CalcPower for max and avg.
    """

    # the avg RF values are at  1/10,  2/11,  3/12, and  4/13 etc.
    # the max RF values are at 19/28, 20/29, 21/30, and 22/31 etc.
    if len(data) > (18 + whichone) :
        avgCNT     = data[0 + whichone]
        avgCalcPwr = float(data[9 + whichone])/1000.0
        maxCNT     = data[18 + whichone]
        maxCalcPwr = float(data[27 + whichone])/1000.0
    else :
        maxCNT     = -1
        avgCNT     = -1
        maxCalcPwr = -1.0
        avgCalcPwr = -1.0
    return (maxCNT, maxCalcPwr, avgCNT, avgCalcPwr)

@try_exceptDec
def getClientRFPower(data, whichone) : # returns a tuple of RSSI values and the entire list
    """ getClientRFPower -- Requires a pre-processed data listfrom a "client_data" operation
        @param: data the result of a json loads operation pre-processed into a list of ints
        @param: whichone - is an index from 1 to 4. Designating a port on the antenna.
        returns: four values, as a tuple:
                 a tuple of maxZCSCount and CalcPower for max and avg.
    """

    # the avg RF values are at 1/5,   2/6,   3/7,  and  4/8
    # the max RF values are at 9/13, 10/14, 11/15, and 12/16
    if len(data) > (12 + whichone) :
        avgCNT     = data[0 + whichone]
        avgCalcPwr = float(data[4 + whichone])/1000.0
        maxCNT     = data[8 + whichone]
        maxCalcPwr = float(data[12 + whichone])/1000.0
    else :
        maxCNT     = -1
        avgCNT     = -1
        maxCalcPwr = -1.0
        avgCalcPwr = -1.0
    return (maxCNT, maxCalcPwr, avgCNT, avgCalcPwr)

def getTransmitterPowerLevel(pyData) :
    """ getTransmitterPowerLevel -- Requires data from a "get_power_level" operation
        @param: pyData the result of a json loads operation
        returns: the Transmitter power level as an int in units dBm
    """
    val = float(processSingleReturn(pyData, 'PowerLevel'))
    if val > 1000.0 :
        val = val / 1000.0
    return val

def getSystemTemp(pyData) :
    """ getTransmitterTemp -- Requires data from a "get_system_temp" operation
        @param: pyData the result of a json loads operation
        returns: the system Temperature in Deg C.
    """
    return int(processSingleReturn(pyData, 'Temp'))

client_distance_indexs = [
                          'XPolrDistMeters',
                          'XPolxDistMeters',
                          'XPolyDistMeters',
                          'XPolzDistMeters',
                          'XPolphiValDegrees',
                          'XPolthetaValDegrees',
                          'XPolXGradCoeffs_0',
                          'XPolXGradCoeffs_1',
                          'XPolXGradCoeffs_2',
                          'XPolYGradCoeffs_0',
                          'XPolYGradCoeffs_1',
                          'XPolYGradCoeffs_2',
                          'XThreshHoldCalc',

                          'YPolrDistMeters',
                          'YPolxDistMeters',
                          'YPolyDistMeters',
                          'YPolzDistMeters',
                          'YPolphiValDegrees',
                          'YPolthetaValDegrees',
                          'YPolXGradCoeffs_0',
                          'YPolXGradCoeffs_1',
                          'YPolXGradCoeffs_2',
                          'YPolYGradCoeffs_0',
                          'YPolYGradCoeffs_1',
                          'YPolYGradCoeffs_2',
                          'YThreshHoldCalc',
                          ]

client_distance_names = {# Log Name             : index name
                          "XPolrDistMeters"     : ["xPolMetaData", "rDistMeters"],
                          "XPolxDistMeters"     : ["xPolMetaData", "xDistMeters"],
                          "XPolyDistMeters"     : ["xPolMetaData", "yDistMeters"],
                          "XPolzDistMeters"     : ["xPolMetaData", "zDistMeters"],
                          "XPolphiValDegrees"   : ["xPolMetaData", "phiValDegrees"],
                          "XPolthetaValDegrees" : ["xPolMetaData", "thetaValDegrees"],

                          "XPolXGradCoeffs_0"   : ["debug"       , "xPolXGradCoeffs", 0],
                          "XPolXGradCoeffs_1"   : ["debug"       , "xPolXGradCoeffs", 1],
                          "XPolXGradCoeffs_2"   : ["debug"       , "xPolXGradCoeffs", 2],

                          "XPolYGradCoeffs_0"   : ["debug"       , "xPolYGradCoeffs", 0],
                          "XPolYGradCoeffs_1"   : ["debug"       , "xPolYGradCoeffs", 1],
                          "XPolYGradCoeffs_2"   : ["debug"       , "xPolYGradCoeffs", 2],
                          "XThreshHoldCalc"     : ["debug"       , 'xTresh'],
                          #"XPolShortID"         : ["xPolMetaData", "shortID"],

                          "YPolrDistMeters"     : ["yPolMetaData", "rDistMeters"],
                          "YPolxDistMeters"     : ["yPolMetaData", "xDistMeters"],
                          "YPolyDistMeters"     : ["yPolMetaData", "yDistMeters"],
                          "YPolzDistMeters"     : ["yPolMetaData", "zDistMeters"],
                          "YPolphiValDegrees"   : ["yPolMetaData", "phiValDegrees"],
                          "YPolthetaValDegrees" : ["yPolMetaData", "thetaValDegrees"],

                          "YPolXGradCoeffs_0"   : ["debug"       , "yPolXGradCoeffs", 0],
                          "YPolXGradCoeffs_1"   : ["debug"       , "yPolXGradCoeffs", 1],
                          "YPolXGradCoeffs_2"   : ["debug"       , "yPolXGradCoeffs", 2],

                          "YPolYGradCoeffs_0"   : ["debug"       , "yPolYGradCoeffs", 0],
                          "YPolYGradCoeffs_1"   : ["debug"       , "yPolYGradCoeffs", 1],
                          "YPolYGradCoeffs_2"   : ["debug"       , "yPolYGradCoeffs", 2],
                          "YThreshHoldCalc"     : ["debug"       , 'yTresh'],
                          #"YPolShortID"         : ["yPolMetaData", "shortID"],
                      }
def getClientDistanceLogNames() :
    return client_distance_indexs

def getClientDistanceValue(pyData, logname) :
    indexName = client_distance_names[logname]
    data = 0.0
    dat  = ''
    errorString = None
    l = len(indexName)
    try :
        if l == 2 :
            dat  = pyData['Result'][indexName[0]][indexName[1]]
        else :
            dat  = pyData['Result'][indexName[0]][indexName[1]][indexName[2]]
    except IndexError :
       errorString = "IndexError in client distance -- len={} indexName={} pyData={}".format(l, repr(indexName), repr(pyData))
    else :
        try :
            data = float(dat)    
        except ValueError :
            try :
                data = int(dat,0)
            except ValueError :
                errorString = "ValueError in client distance -- value {}".format(repr(dat))
                data = 0.0
    return data, errorString

if __name__ == '__main__' :
    
    used_commands=["versions",         "info",           "start_charging",      "stop_charging",   "reset",
                   "calibrate",        "client_command", "client_command_data", "get_system_temp", "client_data",
                   "client_list",      "get_charger_id", "register_client",     "client_config",   "client_detail", "set_power_level",
                   "get_power_level", 'lpm_assign',      'lpm_free',            'lpm_list',        'client_lpm',    'lpm', 
                   'lpm_slots',       'get_com_channel', 'pause',               'remove_client',    'send_disc',    'set_com_channel',
                   'client_leave',    'client_sleep',    'run']

#'register_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Register Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
    #print("A        CLI                  Command                 Args                                  Result")
    maxLen = 0
    for k, d in JSON_CMD_DICT.iteritems() :
        #print("k={}, jstr=={}".format(k, d['jstr']))
        D = json.loads(d['jstr'])
        #jcmdstr = json.dumps(D['Command'])
        #maxLen = max(maxLen, len(jcmdstr))
        #print("len={} {} -- max={}".format(len(jcmdstr), jcmdstr, maxLen))
        #print("{} -- Dict {}".format(k, repr(pyD['Command'])))
        strval = " "
        for kk, vv in D['Command'].iteritems() :
            if kk != "Type" :
                strval += '"{}" : "{}", '.format(kk, vv)
        rstrval = "{}".format(repr(D['Result']))
        if len(D['Result']) > 3 :
            rstrval = " len {}".format(len(D['Result']))

        #print("   {:20s} {:25s} {:42}  {}".format(k, D['Command']['Type'], strval, rstrval))
#        print("{} ##################".format(D['Command']['Type']))

    for k, d in JSON_CMD_DICT.iteritems() :
        D = json.loads(d['jstr'])
        jcmdstr = json.dumps(D['Command'])
        jrltstr = json.dumps(D['Result'])
#        print("%-23s : {'jstr' : '{\"Command\" : %-70s \"Result\" : \"\"}', 'numofargs' : %s}," % ("'"+k+"'",jcmdstr+",",d['numofargs']))

    for k in used_commands :
        #print("k={}".format(k))
        tmp1 = JSON_CMD_DICT.get(k, "NEW Not there")
        pytmp1 = json.loads(tmp1['jstr'])
        #tmp2 = JSON_CMD_DICT_org.get(k, "_org Not There")
        #pytmp2 = json.loads(tmp2['jstr'])
        #tmp3 = JSON_CMD_DICT_NEWD.get(k, "_NEWD Not There")
        #pytmp3 = json.loads(tmp3['jstr'])
        print('"{}"'.format(pytmp1['Command']['Type']))
        #print("ORG  {:23} -- {:30} {}".format(k,pytmp1['Command']['Type'],len(pytmp1['Command'])))
        #print("NEW  {:23} -- {:30} {}".format(k,pytmp2['Command']['Type'],len(pytmp2['Command'])))
        #print("NEWD {:23} -- {:30} {}".format(k,pytmp3['Command']['Type'],len(pytmp3['Command'])))

#        print ("@@@@ " + k)
#        print (JSON_CMD_DICT[k].keys())
#        print (JSON_CMD_DICT[k]['numofargs'])
#        print (JSON_CMD_DICT[k]['jstr'])
#
#        pyData = json.loads(JSON_CMD_DICT[k]['jstr'])
#        print ("\n###################\n{}\n###################\n".format(repr(pyData)))
#        if k == 'client_detail' :
#            print( "NetCurnt  " + repr(getClientNetCurrent(pyData)))
#            print( "Fail Qry  " + repr(getClientQueryFailed(pyData)))
#        elif k == 'client_list' :
#            print( "Short ID  " + repr(getClientShortID(pyData)))
#            print( "Client ID " + repr(getClientID(pyData, requestedClientID=None)))
#            print( "Version   " + repr(getClientVersion(pyData)))
#        elif k == 'client_data' :
#            print( "BatVolt   " + repr(getClientBatteryVoltage(pyData)))
#            print( "MaxRFPwr  " + repr(getClientRFPower(pyData, 1)))
#        elif k == 'get_power_level' :
#            print( "Chgr Pwr  " + repr(getTransmitterPowerLevel(pyData)))

