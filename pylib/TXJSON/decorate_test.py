from __future__ import absolute_import, division, print_function, unicode_literals
import time
TIME = time.time()

import json
import traceback, sys

def try_exceptDec(func) :
    ''' Decorator to wrap all these functions in a try except pair
    '''
    print("in try_exceptDec  {:1.5f}".format(time.time() - TIME))
    def wrapper(*args, **kwargs) :
        try :
            print('do func {:1.5f}'.format(time.time() - TIME))
            return func(*args, **kwargs)
        except Exception as ex :
            print('do Exception {:1.5f}'.format(time.time() - TIME))
            #print("Unexpected Exception --- {} -- args={}\n kwargs={}\n".format(repr(ex), repr(args), repr(kwargs)))
            #print("Traceback -- {} \n".format(traceback.format_exc()))
            #print("Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    print("Returing wrapper func={} {:1.5f}".format(repr(func), time.time() - TIME))
    return wrapper

@try_exceptDec
def getClientBatteryVoltage(pyData) :
    """ getClientBatteryVoltage -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns the Battery Voltage as a float in units Volts
    """
    print("in getClientBat {:1.5f}".format(time.time() - TIME))
    bv = (int(pyData['Result']['Data'][1]) << 8) + int(pyData['Result']['Data'][0])
    return float(bv) / 1000.0

@try_exceptDec
def getClientBatteryVoltage1(pyData) :
    """ getClientBatteryVoltage -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns the Battery Voltage as a float in units Volts
    """
    print("in getClientBat1 {:1.5f}".format(time.time() - TIME))
    bv = (int(pyData['Result']['Data'][1]) << 8) + int(pyData['Result']['Data'][0])
    return float(bv) / 1000.0

if __name__ == "__main__" :

    print("Calling getClient {:1.5f}".format(time.time() - TIME))
    getClientBatteryVoltage(None)
    getClientBatteryVoltage1(None)

