""" testServerMod.py: A class to install or remove the testServer from a tile

Version : 1.0.0
Date : Nov 20 2018
Copyright Ossia Inc. 2018

"""
from __future__ import absolute_import, division, print_function #, unicode_literals

import paramiko
import LIB.sshcomm as sshc
import scpclient as scpc
from optparse import OptionParser
from time import sleep

REBOOT_SLEEP_TIME = 30

class testServerServicing(object) :

    def __init__(self, option, pprint=None, port=2222, username='gumstix', password='gumstix') :

        self.scpc = scpc
        self.debug = option.debug
        self.com = sshc.SSHlink(option.txipaddr, int(port), username, password, self.debug)
        self.eprint = self.mprint if pprint is None else pprint
        self.target = option.txipaddr
        self.rebootsleep = REBOOT_SLEEP_TIME
 
    def executeCmd(self, cmd) :
        rtnval = 1
        if not self.com.connect() :
            self.eprint("ERROR: Could not connect to target '{}'".format(options.txipaddr))
        else :
            rtnval = self.com.command(cmd)
            lines = self.com.getlastreadlines()
            self.com.close()
        return rtnval, lines

    def installTestServer(self) :
        rtnval = 1

        cmd = "sudo mkdir -p -m 777 testServer"
        rtnval, rtnlines = self.executeCmd(cmd)
        self.eprint("INFO: '{}'\n".format(rtnlines))

        if rtnval != 0 :
            self.eprint("ERROR: Command : '{}' returned {}".format(cmd, rtnval))
        else :
            self.com.connect()
            cmd = "Copy server files to target testServer on '{}'".format(self.target)
            try :
                with self.scpc.closing(self.scpc.WriteDir(self.com.client.get_transport(), '/home/gumstix/testServer')) as scp :
                    scp.send_dir('/home/ossiadev/gumstixServer/')
                self.eprint("INFO: {}".format(cmd))
            except Exception as e :
                self.eprint("ERROR: {} -- '{}'".format(cmd, repr(e)))
                rtnval = 1
            finally :
                self.com.close()

            if rtnval == 0 :
                cmd = "sudo ~/testServer/gumstixInit.sh"
                rtnval, rtnlines = self.executeCmd(cmd)
                self.eprint("INFO: '{}'\n".format(rtnlines))

                if rtnval != 0 :
                    self.eprint("ERROR: Command : '{}' returned {}".format(cmd, rtnval))
                else :
                    cmd = "sudo reboot"
                    rtnval, rtnlines = self.executeCmd(cmd)
                    self.eprint("INFO: '{}'\n".format(rtnlines))
                    sleep(self.rebootsleep)
                    if self.debug :
                        self.eprint("ERROR: Command : '{}' returned {}".format(cmd, rtnval))
        return rtnval

    def removeTestServer(self) :
        rtnval = 1

        cmd = "sudo ~/testServer/gumstixUnInit.sh"
        rtnval, rtnlines = self.executeCmd(cmd)
        self.eprint("INFO: '{}'\n".format(rtnlines))

        if rtnval != 0 :
            self.eprint("ERROR: Command : '{}' returned {}".format(cmd, rtnval))
        else :
            cmd = "sudo reboot"
            rtnval, rtnlines = self.executeCmd(cmd)
            self.eprint("INFO: '{}'\n".format(rtnlines))
            sleep(self.rebootsleep)
            if self.debug :
                self.eprint("ERROR: Command : '{}' returned {}".format(cmd, rtnval))

        return rtnval

    def mprint(self, msg) :
        if self.debug :
            print(msg)

if __name__ == "__main__" :
    
    parser = OptionParser()
    parser.add_option("-i","--install", dest='install',            action='store_true', default=False, help="Install the testServer")
    parser.add_option("-r","--remove",  dest='remove',             action='store_true', default=False, help="Remove the testServer")
    parser.add_option("-t","--target",  dest='txipaddr', type=str, action='store',      default=None,  help="Target Name/IPAddress")
    parser.add_option("-p","--port",    dest='port',     type=int, action='store',      default=2222,  help="Target Port")
    parser.add_option("-s","--rbsleep", dest='rbsleep',  type=int, action='store',      default=30,    help="Set the debug flag")
    parser.add_option("-d","--debug",   dest='debug',              action='store_true', default=False, help="Set the debug flag")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        print("ERROR: Extra command line args, exiting")
    else :
        tss =  testServerServicing(options, port=options.port)
        tss.rebootsleep = 5
        if options.install :
            tss.installTestServer()
        elif options.remove :
            tss.removeTestServer()
