import json
from optparse import OptionParser
from operator import itemgetter
from time import time

functiontypes = ['clock', 'names', 'pulse_rise', 'pulse_fall', 'spi', 'spiall', 'stats', 'relative']
timescalenumonic = ['ns', 'us', 'ms', 's']
timescalevalues  = {'ns': 1e9, 'us': 1e6 , 'ms' : 1e3, 's': 1}
usagestr = '''
Usage : python SaleaeVcd.py <filename> <typeOfProcessing> [arg0 [arg1 arg2 arg3]]
     filename           : <name>.vcd only a VCD type output file is expected
     Type of processing : clock, pulse_rise, pulse_fall, relative
         clock      : no args
         pulse_rise : a signal name, number of pulses to check for e.g. sig1 50
         pulse_fall : a signal name, number of pulses to check for e.g. sig1 35
         relative   : signal name rise or fall relative to signal name rise or fall
                      e.g. "sig1 rise sig2 fall" means time from the rise of sig1 to the fall of sig2
'''
xx = '''
$timescale 1 ns $end
$scope module top $end
$var wire 1 ! MOsI $end
$var wire 1 " carmaEn $end
$var wire 1 # sclk $end
$var wire 1 $ CS $end
$upscope $end
$enddefinitions $end
#7739380620
0!
0"
1#
1$
#7739424480
1"
#7739462480
0"
#7739499020
1"
#7739534920
0"
#7739723800
1"
#7739769540
0"
#7739806620
1"
'''

class SaleaeVcd(object) :

    filestring = ""
    filepath   = ""
    timelist   = None
    debug      = 0

    def __new__(self, filepath, tScaleNumonic='ms', debug=0) :

        newSelf = super(SaleaeVcd, self).__new__(self, filepath, tScaleNumonic, debug)

        newSelf.filepath = filepath
        newSelf.timelist = None

        try :
            fd = open(filepath, "r")
            newSelf.filestring = fd.read()
            fd.close()
        except Exception as ee :
            print("ERROR: File not found '{}'".format(filepath))
            newSelf = None
        return newSelf

    def __init__(self, filepath, tScaleNumonic='ms', debug=0) :
        self.firsttransition = None
        self.timelist    = self.getTimeList()
        self.signames    = self.getSignalNames()
        self.timescale   = self.getTimeScale(timescalevalues[tScaleNumonic])
        self.lastResults = None
        self.debug       = debug
        self.timescalenumonic = tScaleNumonic
        self.timelistleng = len(self.timelist)
        if self.debug > 0 :
            print("DEBUG: timelist length {} last line '{}'".format(self.timelistleng, self.timelist[-1]))

    def getTimeScale(self, tscale=1000.0) :
        self.lastResults = None
        mtp = 1
        val = 1
        line = self.timelist[0].strip()
        if not line.startswith("$timescale") :
           mtp = 0 
        else :
            tslst = line.split()
            if tslst[2].strip() == 'ns' :
                mtp = 1e-09
            elif tslst[2].strip() == 'ms' :
                mtp = 1e-03
            elif tslst[2].strip() == 'us' :
                mtp = 1e-06

            mtp *= tscale
            try :
                val = float(tslst[1])
            except Exception :
                val = 1.0
            self.lastResults = dict({'arg0' : mtp * val, 'timescale' : mtp * val})
        return mtp * val
    
    def getSignalNames(self) :
        self.lastResults = None
        rtndict = dict()
        for i in range(len(self.timelist)) :
            line =  self.timelist[i].strip()
            if line.startswith("$enddef") :
                self.firsttransition = i + 1
                break
            elif line.startswith("$var") :
                y = line.split()
                rtndict.update({y[4] : y[3]})
        self.lastResults = dict({"arg0" : rtndict, "rtndict" : rtndict})
        return rtndict
    
    
    def getTimeList(self) :
    
        self.lastResults = None
        timesplit = self.filestring.split("\n", 800000)[:-1]
        if self.debug > 0 :
            print("DEBUG: timesplit list length = {}".format(len(timesplit)))
        timelist = [x for x in timesplit if x != '']
        if self.debug > 0 :
            print("DEBUG: timelist list length = {}".format(len(timelist)))
        return timelist

    def getTimeListStats(self) :
        self.lastResults = None
        firstTran = 0
        lastTime  = 0
       
        i = 0
        j = 0
        # the first iteration finds the abitrary start time of the data it's not absolute
        for i in range(self.timelistleng) :
            if self.timelist[i].startswith("#") :
                if self.debug > 0 :
                    print("DEBUG: BT index={} time={}".format(i, self.timelist[i]))
                lastTime = int(self.timelist[i][1:]) * self.timescale
                break
        # this time is the relative time fo the TRIGGER signal
        i += 1
        for j in range(i, self.timelistleng) :
            if self.timelist[j].startswith("#") :
                if self.debug > 1 :
                    print("DEBUG: NT index={} time={}".format(j, self.timelist[j]))
                firstTran = int(self.timelist[j][1:]) * self.timescale - lastTime
                lastTime  = int(self.timelist[j][1:]) * self.timescale
                break
        # Now we start getting the list of transition times
        timeTranList = list()
        j += 1
        once = True
        for i in range(j, self.timelistleng) :
            if self.timelist[i].startswith("#") :
                if (once and self.debug > 1) or self.debug > 2 :
                    print("DEBUG: TT index={} time={}".format(i, self.timelist[i]))
                    once = False
                curTime   = int(self.timelist[i][1:]) * self.timescale
                timeTranList.append([curTime - lastTime, i, curTime])
                lastTime  = curTime
        timeTranList.sort(key=itemgetter(0))
        minv = timeTranList[0][0]
        maxv = timeTranList[-1][0]
        leng = len(timeTranList)
        leng2 = int(leng/2.0)
        median = timeTranList[leng2+1][0] if (leng % 2) == 1 else float(timeTranList[leng2][0] + timeTranList[leng2+1][0])/2.0
        stats = dict({"firsttran" : firstTran, "min" : minv, "max" : maxv, "len" : leng, "median" : median, "min5" : timeTranList[:4], "max5" : timeTranList[-5:]})
        statstr = "firsttran:{:7.6f},min:{:7.6f},max:{:7.6f},length:{},median:{:7.6f}".format(firstTran, minv, maxv, leng, median)
        self.lastResults = stats
    
        return stats, statstr
    
    def getSpiMOSI(self, indexIn, DATAname=None, CLKname=None, CSname=None) :
        index = 0
        rtnindex = 0
        if DATAname is None :
            self.lastResults = None
            cnt = 2
            for index in range(indexIn, len(self.timelist)) :
                line = self.timelist[index].strip()
                if  line.startswith("#") :
                    cnt -= 1
                    if cnt == 0 :
                        break
            if self.debug > 2 :
                print("DEBUG: Skip line={} index={}".format(line, index))
            num = None
            dataLen = 0
            rtnindex = index
        else :
            if self.debug > 2 :
                print("DEBUG: Begin line={} indexIn={}".format(self.timelist[indexIn], indexIn))
            DATA = self.signames[DATAname]
            CLK  = self.signames[CLKname]
            CS   = self.signames[CSname]
            CSState = False
            DATAState = "0"
            CLKState = False
            self.lastResults = None
            # index is the index in timelist at the second time line
            # we don't care about the time just what is the state of the DATA CLK and CS
            firstTime = False
            dataList = ""
            dataLen = 0
            lastTimeindex = 0
            for index in range(indexIn, len(self.timelist)) :
                tm = time()
                line = self.timelist[index].strip()
                if line.startswith("#") or (not CSState and firstTime) :
                    lastTimeindex = index
                    if self.debug > 2 :
                        t = self.timelistleng - index
                        print("DEBUG: lineT = {} CSState={} firstTime={} index={}".format(line, CSState, firstTime, index))
                        if t > 1 :
                            print("DEBUG: line1 = {}".format(self.timelist[index+1]))
                        if t > 2 :
                            print("DEBUG: line2 = {}".format(self.timelist[index+2]))
                    if firstTime and CLKState :
                        dataList += DATAState
                        dataLen += 1
                        CLKState = False
                    if not CSState and firstTime :
                        break
                    elif CSState : # wait for CSState to become True (low)
                        firstTime = True
                elif line.endswith(CS) :
                    CSState = line[:-1] == '0'
                    if not CSState :
                        rtnindex = lastTimeindex 
                elif line.endswith(CLK) :
                    CLKState = line[:-1] == '1'
                elif line.endswith(DATA) :
                    DATAState = line[:-1]
            if self.debug > 1 :
                print("DEBUG: dataList ={}".format(dataList))
            num = int(dataList, 2)
            self.lastResults = dict({'arg0'   : num, 'arg1'   : dataLen, 'arg2'  : rtnindex,
                                     'number' : num, 'length' : dataLen, 'index' : rtnindex})
        return num, dataLen, rtnindex
        
    
    def checkOnePulse(self, indexIn, sigName, updn=0) :
        self.lastResults = None
        name = self.signames[sigName]
        curTime = 0
        pWidth = 0
        curState = 1 - updn
        startTime = None
        index = 0
        toggle = False
        for index in range(indexIn, len(self.timelist)) :
            line = self.timelist[index].strip()
            if line.startswith("#") :
                curTime = int(line[1:])
                if self.debug > 1 :
                    print("DEBUG: T curTime={}, curState={}, startTime={}, pWidth={}, line={}\n".format(curTime, curState, startTime, pWidth, line))
            elif line.endswith(name) :
                curState = int(line[:-1])
                if self.debug > 2 :
                    print("DEBUG: C cs={} ud={}".format(curState, updn))
                if curState == updn :
                    startTime = curTime
                elif startTime is not None :
                    pWidth = curTime - startTime
                    break
                if self.debug > 1 :
                    print("DEBUG: S index={}, curTime={}, curState={}, startTime={}, pWidth={}, line={}\n".format(index, curTime, curState, startTime, pWidth, line))
        pWidth    *= self.timescale
        curTime   *= self.timescale
        startTime *= self.timescale
        self.lastResults =  dict({'arg0'   : pWidth, 'arg1'    : curTime, 'arg2'      : startTime, 'arg3'  : index,
                                  'pWidth' : pWidth, 'curTime' : curTime, 'startTime' : startTime, 'index' : index})
        return pWidth, index
    
    def checkForClock(self, sigName, startindex=0, stopindex=None) :
        self.lastResults = None
        name = self.signames[sigName]
        curTime  = None
        curState = 0
        toggleup = 0
        toggledn = 0
        upList = list()
        dnList = list()
        lastState = None
        lastTime = 0
        index = 0
        stopindx = len(self.timelist) if stopindex is None else stopindex
        for index in range(startindex, len(self.timelist[:stopindx])) :
            line = self.timelist[index].strip()
            if line.startswith("#") :
                curTime = int(line[1:])
                if self.debug > 1 :
                    print("DEBUG: T -- curTime={}, lastTime={}\ncurState={}, lastState={}, line='{}'".format(curTime, lastTime, curState, lastState, line))
            elif line.endswith(name) and curTime is not None :
                curState = int(line[:-1])
                if lastState is not None :
                    if lastState < curState : # toggled up
                        toggleup += 1
                        dnList.append((curTime - lastTime) * self.timescale) # a toggle up ends a down pulse
                        if self.debug > 2 :
                            print("DEBUG:              -- D new dnlist {} = ({}-{})".format(curTime-lastTime, curTime, lastTime))
                    elif lastState > curState : # toggle down
                        toggledn += 1
                        upList.append((curTime - lastTime) * self.timescale) # a toggle down ends an up pulse
                        if self.debug > 2 :
                            print("DEBUG:              -- U new uplist {} = ({}-{})".format(curTime-lastTime, curTime, lastTime))
                if self.debug > 1 :
                    print("DEBUG: S -- curTime={}, lastTime={}\ncurState={}, lastState={}, line='{}'".format(curTime, lastTime, curState, lastState, line))
                lastTime = curTime
                lastState = curState
            if self.debug > 1 :
                print("")
    
        upl = list(upList)
        dnl = list(dnList)
        upl.sort()
        dnl.sort()
        if self.debug > 2 :
            print("DEBUG: upl={}".format(upl))
            print("DEBUG: dnl={}".format(dnl))
        upAvg = sum(upl[1:-2]) / len(upl[1:-2])
        dnAvg = sum(dnl[1:-2]) / len(dnl[1:-2])
        dutyCyc = upAvg / dnAvg if upAvg < dnAvg else dnAvg / upAvg
        dutyCyc = int(dutyCyc * 100.0 + 0.5000001)

        self.lastResults = dict({'arg0'      : dutyCyc, 'arg1'  : index, 'arg2'  : dnAvg, 'arg3'     : toggledn, 'arg4'   : dnList, 'arg5'  : upAvg, 'arg6'     : toggleup, 'arg7'   : upList,
                                 'dutyCycle' : dutyCyc, 'index' : index, 'dnAvg' : dnAvg, 'toggledn' : toggledn, 'dnlist' : dnList, 'upAvg' : upAvg, 'toggleup' : toggleup, 'uplist' : upList})
        return dutyCyc, index
    
    def getRelativeTime(self, indexin, sigName1, sigName2, updn1, updn2) :
        self.lastResults = None
        name1 = self.signames.get(sigName1, None)
        name2 = self.signames.get(sigName2, None)
        i = 0
        curTime  = 0
        curState = 0
        relTime  = 0
        nameTime = None
        name = name1
        updn = updn1
        namefound = False
        if self.debug > 0 :
            print("DEBUG: name1={}, updn1={} name2={}, updn2={}".format(name1, updn1, name2, updn2))
        for index in range(indexin, self.timelistleng) :
            if namefound :
                name = name2
                updn = updn2
                namefound = False
            line = self.timelist[index].strip()
            if line.startswith("#") :
                curTime = int(line[1:])
                if self.debug > 1 :
                    print("DEBUG: T -- relTime={}, curTime={}, nameTime={}, curState={}, updn={}, name={}, index={} line='{}'".format(relTime, curTime, nameTime, curState, updn, name, index, line))
            elif line.endswith(name) :
                curState = int(line[:-1])
                if curState == updn :
                    namefound = True
                    if self.debug > 2 :
                        print("DEBUG: name found -- line={} index={}".format(line, index))
                    if nameTime is None :
                        nameTime = curTime
                    else :
                        relTime = curTime - nameTime
                        break
                if self.debug > 1 :
                    print("DEBUG: S -- relTime={}, curTime={}, nameTime={}, curState={}, updn={}, name={}, index={} line='{}'".format(relTime, curTime, nameTime, curState, updn, name, index, line))
        self.lastResults = dict({'arg0'    : relTime * self.timescale, 'arg1'  : index,
                                 'relTime' : relTime * self.timescale, 'index' : index})
    
        return relTime * self.timescale, index

def processVcd(cmdlist, debug=0) :
    """ processVcd -- this is a re-implementation to expose the unit tests below
                      the commond list will consist of the command line arguments
                      that are implemented below.
                      order of args:
                      cmdlist = ['vcd file name', 'cmd type string', 'time scale', 'signal CSL', 'arg CSL', 'arg-3', 'arg-2', 'arg-1']
                      arg-3 is quatity name to apply the min max criteria to
                      arg-2 is minimum pass fail
                      arg-1 is maximum pass fail
    """
    rtnval = None
    if cmdlist[1] in functiontypes :
        typeStr = cmdlist[1]
    else :
        rtnval = "ERROR: function type '{}' unknown, not in {}".format(cmdlist[1], functiontypes)

    vcd = SaleaeVcd(cmdlist[0], cmdlist[2], debug)
    if vcd is None :
        rtnval = "ERROR: VCD file not found"
        typeStr = ''

    passfail = True

    if typeStr == 'names' :
        signames = vcd.signames.keys()
        rtnval = dict({'passfail' : True,
                       'namefmt'  : "Signal names:{}",
                       'index'    : 0})
        
        rtnval.update({'arg1' : signames[0]})
        arg0Val = signames[0]
        for i in range(1, len(signames)) :
            name = 'arg{}'.format(i+1)
            rtnval.update({name : signames[i]})
            arg0Val += ",{}".format(signames[i])
        rtnval.update({'arg0' : arg0Val})

    elif typeStr == 'stats' :
        stats, statstr = vcd.getTimeListStats()
        if vcd.debug > 0 :
            print("DEBUG: stats={}".format(stats))
        rtnval = dict({'passfail' : True,
                       'namefmt'  : "File transition time stats{}",
                       'index'    : 0,
                       'arg0'     : statstr,
                       'arg1'     : "Minimum              ={:8.5f}".format(stats['min']),
                       'arg2'     : "Maximum              ={:8.5f}".format(stats['max']),
                       'arg3'     : "Median               ={:8.5f}".format(stats['median']),
                       'arg4'     : "Number of Transitions={}".format(stats['len']),
                       'arg5'     : "5 Minimum times      ={}".format(stats['min5']),
                       'arg6'     : "5 Minimum times      ={}".format(stats['max5'])})

    elif typeStr == 'relative' :
        siglst = cmdlist[3].split(',')
        arglst = cmdlist[4].split(',')
        if len(siglst) != 2 or len(arglst) < 2 :
            rtnval = "Incorrect number of signal names '{}' or args '{}'".format(cmdlist[3], cmdlist[4])
        else :
            sig1   = siglst[0]
            sig2   = siglst[1]
            sig1rf = 0 if arglst[0] == 'fall' else 1
            sig2rf = 0 if arglst[1] == 'fall' else 1
            indexin = 0 if len(arglst) <= 2 else  int(arglst[2])
            relTime, index = vcd.getRelativeTime(indexin, sig1, sig2, sig1rf, sig2rf)

            passfail = vcd.lastResults[cmdlist[-3]] > float(cmdlist[-2]) and vcd.lastResults[cmdlist[-3]] < float(cmdlist[-1])
            rtnval = dict({'passfail' : passfail,
                           'index'    : index,
                           'namefmt'  : "Relative Time={}",
                           'arg0'     : relTime})

    elif typeStr == 'pulse_rise' or typeStr == 'pulse_fall' :
        siglst = cmdlist[3].split(',')
        arglst = cmdlist[4].split(',')
        if len(siglst) != 1 or len(arglst) == 0 :
            rtnval = "Incorrect number of signal names '{}' or args '{}'".format(cmdlist[3], cmdlist[4])
        else :
            sig1        = siglst[0]
            numOfPulses = int(arglst[0])
            index = 0
            if len(arglst) > 1 :
                index = int(arglst[1]) 
            rise_fall = 1 if typeStr == 'pulse_rise' else 0
        
            passnum = 0
            pulselist = list()
            for i in range(numOfPulses) :
                pWidth, index = vcd.checkOnePulse(index, sig1, rise_fall)
                pulselist.append([pWidth, index])
                if vcd.debug > 0 :
                    print("DEBUG: D pulsewidth={}, curTime={}, startTime={}, index={}\n".format(pWidth, vcd.lastResults['curTime'], vcd.lastResults['startTime'], index))
                pf = vcd.lastResults[cmdlist[-3]] > float(cmdlist[-2]) and vcd.lastResults[cmdlist[-3]] < float(cmdlist[-1])
                passfail = passfail and pf
                if pf :
                    passnum += 1
            rtnval = dict({ 'passfail' : passfail,
                            'index'    : index,
                            'namefmt'  : "Pulses passed={}",
                            'arg0'     : passnum,
                            'arg1'     : 'Pulse list={}'.format(pulselist)})
    
    elif typeStr == 'clock' :
        siglst = cmdlist[3].split(',')
        if len(siglst) != 1 :
            rtnval = "Incorrect number of signal names '{}'".format(cmdlist[3])
        else :
            arglst = cmdlist[4].split(',')
            if len(arglst) == 0 :
                if vcd.debug > 0 :
                    print("DEBUG: No args defaulting to 0,30")
                indexstart = 0
                indexend   = 30
            elif len(arglst) == 1 :
                if vcd.debug > 0 :
                    print("DEBUG: Start arg {} defaulting end to 30".format(cmdlist[4]))
                indexstart = int(arglst[0])
                indexend   = 30
            else :
                indexstart = int(arglst[0])
                indexend   = int(arglst[1])

            sig1        = siglst[0]
            indexstart += vcd.firsttransition+1 if indexstart == 0 else 0
            indexend   += vcd.firsttransition   if indexstart == 0 else 0

            dutyCycle, index =  vcd.checkForClock(sig1, indexstart, indexend)
            passfail = vcd.lastResults[cmdlist[-3]] > float(cmdlist[-2]) and vcd.lastResults[cmdlist[-3]] < float(cmdlist[-1])
            rtnval = dict({ 'passfail' : passfail,
                            'index'    : index,
                            'namefmt'  : "Duty cycle={}",
                            'arg0'     : dutyCycle,
                            'arg2'     : "ToggleUP={}".format(vcd.lastResults['toggleup']),
                            'arg3'     : "ToggleDN={}".format(vcd.lastResults['toggledn']),
                            'arg4'     : "AverageUP={}".format(vcd.lastResults['upAvg']),
                            'arg5'     : "AverageDN={}".format(vcd.lastResults['dnAvg'])})


            if vcd.debug > 0 :
                print("DEBUG: duty cycle = {} index = {} upAvg={:8.6f} dnAvg={:8.6f} toggleup={} toggledn={}".format(dutyCycle, index, vcd.lastResults['upAvg'], vcd.lastResults['dnAvg'], vcd.lastResults['toggleup'], vcd.lastResults['toggledn']))
                print("\n\nDEBUG:  -- {}\n".format(json.dumps(vcd.lastResults)))

    elif typeStr == 'spi' :
        siglst = cmdlist[3].split(',')
        if len(siglst) != 3 :
            rtnval = "ERROR: Incorrect number of signal names '{}'".format(cmdlist[3])
        else :
            datanm  = siglst[0]
            clknm   = siglst[1]
            csnm    = siglst[2]
            indx    = 0
            try :
                arglst = cmdlist[4].split(',')
                indexin = arglst[0]
                indx = int(indexin, 0)
            except Exception :
                indx = 0
            pfcriteria = cmdlist[-1].split(":")
            masklist   = cmdlist[-2].split(":")
            mask       = int(masklist[0], 0)
            numbytes   = int(masklist[1], 0)
        
            num, cnt, index = vcd.getSpiMOSI(0)
            indx = index if indx == 0 else indx
            num, cnt, index = vcd.getSpiMOSI(indx, datanm, clknm, csnm)
            shft = cnt - (8 * numbytes)
            shft = max(0, shft)
            #mask = mask << shft
            passfail = False
            value = (vcd.lastResults[cmdlist[-3]] >> shft) & mask
            if vcd.debug :
                print("DEBUG: masklist={} mask=0x{:X} numbytes={} shft={} cnt={} num=0x{:010X} value=0x{:X} index={} pfcriteria={}".format(masklist, mask, numbytes, shft, cnt, num, value, index, pfcriteria))
            for pf in pfcriteria :
                pf = int(pf, 0) & mask
                passfail = passfail or value == pf

            rtnval = dict({ 'passfail' : passfail,
                            'index'    : index,
                            'namefmt'  : "SPI Value=0x{:08X}",
                            'arg0'     : num,
                            'arg1'     : "SPI Length={}".format(cnt)})
            if vcd.debug > 0 :
                print("DEBUG: SPI num=0x{:08X}, cnt={}, index={}".format(num, cnt, index))

    return rtnval

if __name__ == "__main__" :


    parser = OptionParser()

    parser.add_option("-f","--filename",  dest='filename',  type=str, action='store', default=None,  help="VCD file path ")
    parser.add_option("-t","--functype",  dest='functype',            action='store', default=None,  help="func type string", choices=functiontypes)
    parser.add_option("-s","--signames",  dest='signames',  type=str, action='store', default=None,  help="Signal names Comma Separate List CSL. Use func 'names' to show.")
    parser.add_option("-a","--arglist",   dest='arglist',   type=str, action='store', default=None,  help="A CSL of args for the desired function type")
    parser.add_option("-e","--timescale", dest='timescale',           action='store', default='ms',  help="A numonic to define time values display", choices=timescalenumonic)
    parser.add_option("-p","--passfail",  dest='passfail',            action='store', default=None,  help="Pass fail criteria 'min,max'")
    parser.add_option("-d","--debug",     dest='debug',               action="count", default=False, help="Print debug info up to three instances of -d.")

    (opts, args) = parser.parse_args()

    pf=[None, None]
    if opts.passfail is not None :
       pf = opts.passfail.split(",")
    cmdlist = [opts.filename, opts.functype, opts.timescale, opts.signames, opts.arglist,'arg0', pf[0], pf[1]]

    rtnval = processVcd(cmdlist, opts.debug)
    print("rtnval  ={}".format(rtnval))
    print("cmdlist ={}".format(cmdlist))

    if False :
        typeStr = ''
        vcd = None
        if opts.functype in functiontypes :
            typeStr = opts.functype
        else :
            print("ERROR: function type unknown '{}' in {}".format(opts.functype, functiontypes))
    
        if opts.filename is None :
            typeStr = ''
        else :
            vcd = SaleaeVcd(opts.filename, opts.timescale, opts.debug)
            if vcd is None :
                typeStr = ''
                print("ERROR: VCD file not found")
    
        if typeStr == 'names' :
            print("{}".format(vcd.signames.keys()))
    
        elif typeStr == 'stats' :
            stats, statstr = vcd.getTimeListStats()
            if opts.debug > 0 :
                print("stats={}".format(stats))
                print("#########")
            print("min={:8.6f}\nmax={:8.6f}\nlen={}\nmedian={:8.6f}\nmin5={}\nmax5={}".format(stats['min'], stats['max'], stats['len'], stats['median'], stats['min5'], stats['max5']))
    
        elif typeStr == 'relative' :
            siglst = opts.signames.split(',')
            arglst = opts.arglist.split(',')
            if len(siglst) != 2 or len(arglst) < 2 :
                print("Incorrect number of signal names '{}' or args '{}'".format(opts.signames, opts.arglist))
            else :
                sig1   = siglst[0]
                sig2   = siglst[1]
                sig1rf = 0 if arglst[0] == 'fall' else 1
                sig2rf = 0 if arglst[1] == 'fall' else 1
                indexin = 0 if len(arglst) <= 2 else  int(arglst[2])
                relTm, index = vcd.getRelativeTime(indexin, sig1, sig2, sig1rf, sig2rf)
                print("relTime={:8.6f} index={}".format(relTm, index))
    
        elif typeStr == 'pulse_rise' or typeStr == 'pulse_fall' :
            siglst = opts.signames.split(',')
            arglst = opts.arglist.split(',')
            if len(siglst) != 1 or len(arglst) == 0 :
                print("Incorrect number of signal names '{}' or args '{}'".format(opts.signames, opts.arglist))
            else :
                sig1        = siglst[0]
                numOfPulses = int(arglst[0])
                index = 0
                if len(arglst) > 1 :
                    index = int(arglst[1]) 
                rise_fall = 1 if typeStr == 'pulse_rise' else 0
            
                for i in range(numOfPulses) :
                    pWidth, index = vcd.checkOnePulse(index, sig1, rise_fall)
                    if opts.debug > 0 :
                        print("D pulsewidth={}, curTime={}, startTime={}, index={}\n".format(pWidth, vcd.lastResults['curTime'], vcd.lastResults['startTime'], index))
                    print("pulsewidth {:8.6f}, index {}".format(pWidth, index))
        
        elif typeStr == 'clock' :
            siglst = opts.signames.split(',')
            if len(siglst) != 1 :
                print("Incorrect number of signal names '{}'".format(opts.signames))
            else :
                arglst = opts.arglist.split(',')
                if len(arglst) == 0 :
                    print("No args defaulting to 0,30")
                    indexstart = 0
                    indexend   = 30
                elif len(arglst) == 1 :
                    print("Start arg {} defaulting end to 30".format(opts.arglist))
                    indexstart = int(arglst[0])
                    indexend   = 30
                else :
                    indexstart = int(arglst[0])
                    indexend   = int(arglst[1])
    
                sig1        = siglst[0]
                dutyCycle, index =  vcd.checkForClock(sig1, vcd.firsttransition+1+indexstart, vcd.firsttransition+indexend)
                print("duty cycle = {} index = {} upAvg={:8.6f} dnAvg={:8.6f} toggleup={} toggledn={}".format(dutyCycle, index, vcd.lastResults['upAvg'], vcd.lastResults['dnAvg'], vcd.lastResults['toggleup'], vcd.lastResults['toggledn']))
                if opts.debug > 0 :
                    print("\n\n -- {}\n".format(json.dumps(vcd.lastResults)))
    
        elif typeStr == 'spi' :
            siglst = opts.signames.split(',')
            if len(siglst) != 3 :
                print("Incorrect number of signal names '{}'".format(opts.signames))
            else :
                datanm  = siglst[0]
                clknm   = siglst[1]
                csnm    = siglst[2]
                indx    = 0
                try :
                    arglst = opts.arglist.split(',')
                    indexin = arglst[0]
                    indx = int(indexin, 0)
                except Exception :
                    indx = 0
            
                num, cnt, index = vcd.getSpiMOSI(0)
                indx = index if indx == 0 else indx
                num, cnt, index = vcd.getSpiMOSI(indx, datanm, clknm, csnm)
                print("num=0x{:08X}, cnt={}, index={}".format(num, cnt, index))
    
        elif typeStr == 'spiall' :
            siglst = opts.signames.split(',')
            if len(siglst) != 3 :
                print("Incorrect number of signal names '{}'".format(opts.signames))
            else :
                datanm  = siglst[0]
                clknm   = siglst[1]
                csnm    = siglst[2]
    
                num, cnt, index = vcd.getSpiMOSI(0)
                indx = index
                indxdiff = 0
                while indx < vcd.timelistleng - indxdiff :
                    tm = time()
                    num, cnt, index = vcd.getSpiMOSI(indx, datanm, clknm, csnm)
                    indxdiff = index - indx
                    indx = index
                    rw  = num >> (cnt-1) & 0x01
                    adr = num >> (cnt-8) & 0x7F
                    data = num & (1 << (cnt-8)) - 1
                    tm1 = time() - tm
                    print("num=0x{:010X}, rw={}, adr=0x{:02X} {:3d}, data=0x{:08X} cnt={:3d}, index={} time={:8.6f}".format(num, rw, adr, adr, data, cnt, indx, tm1))
    
