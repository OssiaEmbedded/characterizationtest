""" SaleaeCom.py: A module to communcate command strings to the
                  SALEAE software control application.

Version : 1.0.0
Date : Aug 19 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import socket, sys
from time import sleep, time
import LIB.InstrumentBase as IBase

saleae_cmd_dict = {
"set_trigger"         : "SET_TRIGGER",
"get_num_samples"     : "GET_NUM_SAMPLES",
"set_num_samples"     : "SET_NUM_SAMPLES",
"get_sample_rate"     : "GET_SAMPLE_RATE",
"set_sample_rate"     : "SET_SAMPLE_RATE",

"set_capture_seconds" : "SET_CAPTURE_SECONDS",
"capture_to_file"     : "CAPTURE_TO_FILE",
"save_to_file"        : "SAVE_TO_FILE",
"load_from_file"      : "LOAD_FROM_FILE",
"export_data2"        : "EXPORT_DATA2",

"get_all_sample_rates" : "GET_ALL_SAMPLE_RATES",
"get_analyzers"        : "GET_ANALYZERS",
"export_analyzer"      : "EXPORT_ANALYZER",
"get_inputs"           : "GET_INPUTS",
"capture"              : "CAPTURE",
"stop_capture"         : "STOP_CAPTURE",
"get_capture_pretrigger_buffer_size" : "GET_CAPTURE_PRETRIGGER_BUFFER_SIZE",
"set_capture_pretrigger_buffer_size" : "SET_CAPTURE_PRETRIGGER_BUFFER_SIZE",
"get_connected_devices" : "GET_CONNECTED_DEVICES",
"select_active_device"  : "SELECT_ACTIVE_DEVICE",

"get_active_channels"   : "GET_ACTIVE_CHANNELS",
"set_active_channels"   : "SET_ACTIVE_CHANNELS",
"reset_active_channels" : "RESET_ACTIVE_CHANNELS",

"set_performance"        : "SET_PERFORMANCE",
"get_performance"        : "GET_PERFORMANCE",
"is_processing_complete" : "IS_PROCESSING_COMPLETE",
"is_analyzer_complete"   : "IS_ANALYZER_COMPLETE",

"set_digital_voltage_option"  : "SET_DIGITAL_VOLTAGE_OPTION",
"get_digital_voltage_options" : "GET_DIGITAL_VOLTAGE_OPTIONS",

"close_all_tabs" : "CLOSE_ALL_TABS",

"exit" : "EXIT",

"get_capture_range" : "GET_CAPTURE_RANGE",
"get_viewstate"     : "GET_VIEWSTATE",
"set_viewstate"     : "SET_VIEWSTATE",
}

class SALEAECom(IBase.InstrumentBase) :

    def __init__(self, addr='', port=10429, debug=False) :
        ''' __init__ --- Initialization clase for SALEAE
        '''
        # init the base class
        super(SALEAECom, self).__init__(addr=addr, port=port, name="Saleae Software", debug=debug, timethis=False)
        self.sendCR = False 
        self.catEndValue = chr(0)

    @property
    def getNumSamples(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_NUM_SAMPLES'+self.catEndValue)
        self.instrumentClose()
        return d
    @getNumSamples.setter
    def getNumSamples(self, value) :
        cmd = "GET_NUM_SAMPLES {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getConnectedDevices(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_CONNECTED_DEVICES'+self.catEndValue)
        self.instrumentClose()
        return d
    @getConnectedDevices.setter
    def getConnectedDevices(self, value) :
        cmd = "GET_CONNECTED_DEVICES {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def captureToFile(self) :
        self.instrumentOpen()
        d = self.sendgetdata('CAPTURE_TO_FILE'+self.catEndValue)
        self.instrumentClose()
        return d
    @captureToFile.setter
    def captureToFile(self, value) :
        cmd = "CAPTURE_TO_FILE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setDigitalVoltageOption(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_DIGITAL_VOLTAGE_OPTION'+self.catEndValue)
        self.instrumentClose()
        return d
    @setDigitalVoltageOption.setter
    def setDigitalVoltageOption(self, value) :
        cmd = "SET_DIGITAL_VOLTAGE_OPTION {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def isAnalyzerComplete(self) :
        self.instrumentOpen()
        d = self.sendgetdata('IS_ANALYZER_COMPLETE'+self.catEndValue)
        self.instrumentClose()
        return d
    @isAnalyzerComplete.setter
    def isAnalyzerComplete(self, value) :
        cmd = "IS_ANALYZER_COMPLETE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setPerformance(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_PERFORMANCE'+self.catEndValue)
        self.instrumentClose()
        return d
    @setPerformance.setter
    def setPerformance(self, value) :
        cmd = "SET_PERFORMANCE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getAnalyzers(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_ANALYZERS'+self.catEndValue)
        self.instrumentClose()
        return d
    @getAnalyzers.setter
    def getAnalyzers(self, value) :
        cmd = "GET_ANALYZERS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setTrigger(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_TRIGGER'+self.catEndValue)
        self.instrumentClose()
        return d
    @setTrigger.setter
    def setTrigger(self, value) :
        cmd = "SET_TRIGGER {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getViewstate(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_VIEWSTATE'+self.catEndValue)
        self.instrumentClose()
        return d
    @getViewstate.setter
    def getViewstate(self, value) :
        cmd = "GET_VIEWSTATE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getCapturePretriggerBufferSize(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_CAPTURE_PRETRIGGER_BUFFER_SIZE'+self.catEndValue)
        self.instrumentClose()
        return d
    @getCapturePretriggerBufferSize.setter
    def getCapturePretriggerBufferSize(self, value) :
        cmd = "GET_CAPTURE_PRETRIGGER_BUFFER_SIZE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setSampleRate(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_SAMPLE_RATE'+self.catEndValue)
        self.instrumentClose()
        return d
    @setSampleRate.setter
    def setSampleRate(self, value) :
        cmd = "SET_SAMPLE_RATE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setViewstate(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_VIEWSTATE'+self.catEndValue)
        self.instrumentClose()
        return d
    @setViewstate.setter
    def setViewstate(self, value) :
        cmd = "SET_VIEWSTATE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def exportData2(self) :
        self.instrumentOpen()
        d = self.sendgetdata('EXPORT_DATA2'+self.catEndValue)
        self.instrumentClose()
        return d
    @exportData2.setter
    def exportData2(self, value) :
        cmd = "EXPORT_DATA2 {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def isProcessingComplete(self) :
        self.instrumentOpen()
        d = self.sendgetdata('IS_PROCESSING_COMPLETE'+self.catEndValue)
        self.instrumentClose()
        return d
    @isProcessingComplete.setter
    def isProcessingComplete(self, value) :
        cmd = "IS_PROCESSING_COMPLETE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getActiveChannels(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_ACTIVE_CHANNELS'+self.catEndValue)
        self.instrumentClose()
        return d
    @getActiveChannels.setter
    def getActiveChannels(self, value) :
        cmd = "GET_ACTIVE_CHANNELS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def capture(self) :
        self.instrumentOpen()
        d = self.sendgetdata('CAPTURE'+self.catEndValue)
        self.instrumentClose()
        return d
    @capture.setter
    def capture(self, value) :
        cmd = "CAPTURE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def saveToFile(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SAVE_TO_FILE'+self.catEndValue)
        self.instrumentClose()
        return d
    @saveToFile.setter
    def saveToFile(self, value) :
        cmd = "SAVE_TO_FILE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setActiveChannels(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_ACTIVE_CHANNELS'+self.catEndValue)
        self.instrumentClose()
        return d
    @setActiveChannels.setter
    def setActiveChannels(self, value) :
        cmd = "SET_ACTIVE_CHANNELS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getPerformance(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_PERFORMANCE'+self.catEndValue)
        self.instrumentClose()
        return d
    @getPerformance.setter
    def getPerformance(self, value) :
        cmd = "GET_PERFORMANCE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getAllSampleRates(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_ALL_SAMPLE_RATES'+self.catEndValue)
        self.instrumentClose()
        return d
    @getAllSampleRates.setter
    def getAllSampleRates(self, value) :
        cmd = "GET_ALL_SAMPLE_RATES {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setCapturePretriggerBufferSize(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_CAPTURE_PRETRIGGER_BUFFER_SIZE'+self.catEndValue)
        self.instrumentClose()
        return d
    @setCapturePretriggerBufferSize.setter
    def setCapturePretriggerBufferSize(self, value) :
        cmd = "SET_CAPTURE_PRETRIGGER_BUFFER_SIZE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def resetActiveChannels(self) :
        self.instrumentOpen()
        d = self.sendgetdata('RESET_ACTIVE_CHANNELS'+self.catEndValue)
        self.instrumentClose()
        return d
    @resetActiveChannels.setter
    def resetActiveChannels(self, value) :
        cmd = "RESET_ACTIVE_CHANNELS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getCaptureRange(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_CAPTURE_RANGE'+self.catEndValue)
        self.instrumentClose()
        return d
    @getCaptureRange.setter
    def getCaptureRange(self, value) :
        cmd = "GET_CAPTURE_RANGE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setNumSamples(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_NUM_SAMPLES'+self.catEndValue)
        self.instrumentClose()
        return d
    @setNumSamples.setter
    def setNumSamples(self, value) :
        cmd = "SET_NUM_SAMPLES {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def selectActiveDevice(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SELECT_ACTIVE_DEVICE'+self.catEndValue)
        self.instrumentClose()
        return d
    @selectActiveDevice.setter
    def selectActiveDevice(self, value) :
        cmd = "SELECT_ACTIVE_DEVICE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getSampleRate(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_SAMPLE_RATE'+self.catEndValue)
        self.instrumentClose()
        return d
    @getSampleRate.setter
    def getSampleRate(self, value) :
        cmd = "GET_SAMPLE_RATE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def setCaptureSeconds(self) :
        self.instrumentOpen()
        d = self.sendgetdata('SET_CAPTURE_SECONDS'+self.catEndValue)
        self.instrumentClose()
        return d
    @setCaptureSeconds.setter
    def setCaptureSeconds(self, value) :
        cmd = "SET_CAPTURE_SECONDS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getInputs(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_INPUTS'+self.catEndValue)
        self.instrumentClose()
        return d
    @getInputs.setter
    def getInputs(self, value) :
        cmd = "GET_INPUTS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def closeAllTabs(self) :
        self.instrumentOpen()
        d = self.sendgetdata('CLOSE_ALL_TABS'+self.catEndValue)
        self.instrumentClose()
        return d
    @closeAllTabs.setter
    def closeAllTabs(self, value) :
        cmd = "CLOSE_ALL_TABS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def exitApp(self) :
        self.instrumentOpen()
        d = self.sendgetdata('EXIT'+self.catEndValue)
        self.instrumentClose()
        return d
    @exitApp.setter
    def exitApp(self, value) :
        cmd = "EXIT {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def getDigitalVoltageOptions(self) :
        self.instrumentOpen()
        d = self.sendgetdata('GET_DIGITAL_VOLTAGE_OPTIONS'+self.catEndValue)
        self.instrumentClose()
        return d
    @getDigitalVoltageOptions.setter
    def getDigitalVoltageOptions(self, value) :
        cmd = "GET_DIGITAL_VOLTAGE_OPTIONS {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def stopCapture(self) :
        self.instrumentOpen()
        d = self.sendgetdata('STOP_CAPTURE'+self.catEndValue)
        self.instrumentClose()
        return d
    @stopCapture.setter
    def stopCapture(self, value) :
        cmd = "STOP_CAPTURE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def exportAnalyzer(self) :
        self.instrumentOpen()
        d = self.sendgetdata('EXPORT_ANALYZER'+self.catEndValue)
        self.instrumentClose()
        return d
    @exportAnalyzer.setter
    def exportAnalyzer(self, value) :
        cmd = "EXPORT_ANALYZER {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def loadFromFile(self) :
        self.instrumentOpen()
        d = self.sendgetdata('LOAD_FROM_FILE'+self.catEndValue)
        self.instrumentClose()
        return d
    @loadFromFile.setter
    def loadFromFile(self, value) :
        cmd = "LOAD_FROM_FILE {}".format(value)+self.catEndValue
        self.instrumentOpen()
        self.senddata(cmd)
        self.instrumentClose()
    
    @property
    def sendApp(self) :
        return ""
    @sendApp.setter
    def sendApp(self, value) :
        self.instrumentOpen()
        d=self.sendgetdata(value+self.catEndValue)
        self.instrumentClose()
        print("sendApp {} == {}".format(value, d))

    def sendGetSal(self, value) :
        self.instrumentOpen()
        #a = self.mySockLoopTimeoutCount
        #self.mySockLoopTimeoutCount = 30
        d=self.sendgetdata(value+self.catEndValue, True)
        #self.mySockLoopTimeoutCount = a
        #print("a={}".format(a))
        self.instrumentClose()
        return d

    def sendSal(self, value) :
        self.instrumentOpen()
        d=self.senddata(value+self.catEndValue)
        self.instrumentClose()
        return d

if __name__ == "__main__" :

    #from subprocess import call

    #fd = open("/home/ursus/logFileLogic.txt", "w")
    #cmd = "DISPLAY=:1 && /home/ursus/logic/Logic &"
    #exitcode = call(cmd, stdout=fd, stderr=fd, shell=True)
    #fd.close()
    #sleep(5)
    argc = len(sys.argv)
    s = SALEAECom(addr='localhost', debug=False)
    if argc > 1 :
        s.sendApp = sys.argv[1]
    else :

        d = s.getNumSamples
        print("getNumSamples == {}".format(d))
        
        d = s.getConnectedDevices
        print("getConnectedDevices == {}".format(d))
        
        d = s.getAnalyzers
        print("getAnalyzers == {}".format(d))
        
        d = s.getViewstate
        print("getViewstate == {}".format(d))
        
        d = s.getCapturePretriggerBufferSize
        print("getCapturePretriggerBufferSize == {}".format(d))
        
        d = s.getActiveChannels
        print("getActiveChannels == {}".format(d))
        
        d = s.getPerformance
        print("getPerformance == {}".format(d))
        
        d = s.getAllSampleRates
        print("getAllSampleRates == {}".format(d))
        
        d = s.getCaptureRange
        print("getCaptureRange == {}".format(d))
        
        d = s.getSampleRate
        print("getSampleRate == {}".format(d))
        
        d = s.getInputs
        print("getInputs == {}".format(d))
        
        d = s.getDigitalVoltageOptions
        print("getDigitalVoltageOptions == {}".format(d))
    
        s.sendApp = "GET_TRIGGER"
    
        #d = s.exitApp
        #print("exitApp == {}".format(d))
    
        
