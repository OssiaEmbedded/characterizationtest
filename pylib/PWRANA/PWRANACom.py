""" PWRANACom.py: A class to connect to and communicate with
the Agilent A-N6705B Signal Analyzer. It makes use of the PWRANACommStrings dictionary.

Version : 1.0.1
Date : July 10 2019
Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
if '/home/ossiadev/pylib' not in sys.path :
    sys.path.append('/home/ossiadev/pylib')
from time import sleep, time
import LIB.InstrumentBase as IBase
from LIB.utilmod import *

# these only talk to channel 1 of the Power Analyzer
PWRANA_CMD_DICT = {
#Set Power Analyzer to 4-wire mode
'modules'       : {'get'  : '*RDT?',
                   'set'  : '*RDT?',
                   'argc' : 0},

'wire4'       : {'get'  : 'SOUR:VOLT:SENS:SOUR? (@{})',
                 'set'  : 'SOUR:VOLT:SENS:SOUR {}, (@{})',
                 'argd' : 1,
                 'argc' : 2},

'tinterval'   : {'get'  : 'SENS:SWE:TINT? (@{})',
                 'set'  : 'SENS:SWE:TINT {}, (@{})',
                 'argd' : 1,
                 'argc' : 2},

#Set Power Analyzer Emulating to 2 Quadrant PS, voltage priority
'quadrant'    : {'get'  : 'SOUR:EMUL? (@{})',
                 'set'  : 'SOUR:EMUL {}, (@{})',
                 'argc' : 1},
'quadrant2'   : {'get'  : 'SOUR:EMUL? (@{})',
                 'set'  : 'SOUR:EMUL PS2Q, (@{})',
                 'argc' : 0},
'priority'    : {'get'  : 'SOUR:FUNC? (@{})',
                 'set'  : 'SOUR:FUNC {}, (@{})',
                 'argc' : 1},

#Set Power Analyzer current limit to +/-100mA (user-defined in GUI)
'negcurlimit' : {'get' : 'curr:lim:neg? (@{})',
                 'set' : 'curr:lim:neg {}, (@{})',
                 'argc' : 1},
'poscurlimit' : {'get' : 'curr:lim:pos? (@{}) ',
                 'set' : 'curr:lim:pos {}, (@{}) ',
                 'argc' : 1},
'curlimitcouple' : {'get' : 'curr:lim:coup? (@{}) ',
                    'set' : 'curr:lim:coup {}, (@{}) ',
                    'argc' : 1},
'negcurlimitmin' : {'get' : 'curr:lim:neg? min, (@{}) ',
                    'argc' : 0},
'negcurlimitmax' : {'get' : 'curr:lim:neg? max, (@{}) ',
                    'argc' : 0},
'poscurlimitmin' : {'get' : 'curr:lim:pos? min, (@{}) ',
                    'argc' : 0},
'poscurlimitmax' : {'get' : 'curr:lim:pos? max, (@{}) ',
                    'argc' : 0},

# set Power Analyzer output voltage and or range
'setvoltrange' : {'get' : 'SOUR:VOLT:RANG? (@{})',
                  'set' : 'SOUR:VOLT:RANG {}, (@{})',
                  'argc' : 1},
'setvoltout'   : {'get' : 'SOUR:VOLT? (@{})',
                  'set' : 'SOUR:VOLT {}, (@{})',
                  'argc' : 1},
'setvoltlim'   : {'get' : 'SOUR:VOLT:LIM? (@{})',
                  'set' : 'SOUR:VOLT:LIM {}, (@{})',
                  'argc' : 1},
'setvoltslew'  : {'get' : 'SOUR:VOLT:SLEW? (@{})',
                  'set' : 'SOUR:VOLT:SLEW {}, (@{})',
                  'argc' : 1},

# enable a channel or channel list
'enablechan' : {'get' : 'OUTP? (@{})',
                'set' : 'OUTP {}, (@{})',
                'argd' : 1,
                'argc' : 2},

# Questionable condition register for error detection
'quescond' : {'get' : 'STAT:QUES:COND? (@{})',
              'set' : '',
              'argd' : 1,
              'argc' : 0},

'clearoutputs' : {'get' : 'OUTP:PROT:CLE (@{})', # this is not a query but it acts like one
                  'set' : '',
                  'argd' : 1,
                  'argc' : 0},

'voltprotdelay' : {'get' : 'VOLT:PROT:DEL? (@{})',
                   'set' : 'VOLT:PROT:DEL {}, (@{})',
                   'argd' : 1,
                   'argc' : 2},

'voltprotpos' : {'get' : 'VOLT:PROT:REM? (@{})',
                 'set' : 'VOLT:PROT:REM {}, (@{})',
                 'argd' : 1,
                 'argc' : 2},

'voltprotneg' : {'get' : 'VOLT:PROT:REM:NEG? (@{})',
                 'set' : 'VOLT:PROT:REM:NEG {}, (@{})',
                 'argd' : 1,
                 'argc' : 2},
}

class PWRANACom(IBase.InstrumentBase) :
    ''' PWRANACom class -- Can be used as a base class but at this time is used as it.
    '''

    #def __init__(self, addr='a-n6705b-00331.ossiainc.local', port=5025, debug=False, timethis=False) :
    def __init__(self, addr='10.10.0.149', port=5025, debug=False, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(PWRANACom, self).__init__(addr=addr, port=port, name='Power Analyzer', debug=debug, timethis=timethis)
        self.pwranacd = dict(PWRANA_CMD_DICT)
        self.pwranacd.update(self.ibcmddict)
        self.lastChanList = "1"
        self.fourwireremote = "INT"
        self.error_list = {
              1 : [0,  "OV",   "The output is disabled by the over-voltage protection."],
              2 : [1,  "OC",   "The output is disabled by the over-current protection."],
              4 : [2,  "PF",   "The output is disabled by the power-fail - which may be caused by a low-line or brownout condition on the AC line."],
              8 : [3,  "CP+",  "The output is limited (or disabled) by the positive power limit."],
             16 : [4,  "OT",   "The over-temperature protection has tripped."],
             32 : [5,  "CP-",  "The output is limited by the negative power limit."],
             64 : [6,  "OV-",  "The negative over-voltage protection has tripped."],
            128 : [7,  "LIM+", "The output is in positive voltage or current limit."],
            256 : [8,  "LIM-", "The output is in negative voltage or current limit."],
            512 : [9,  "INH",  "The output is inhibited by an external signal."],
           1024 : [10, "UNR",  "The output is unregulated."],
           2048 : [11, "PROT", "The output has been disabled because it is coupled to a protection condition that occurred on another channel."],
           4096 : [12, "OSC",  "The oscillation detector has tripped."],
        }

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    def checkOutErrors(self, n) :
        ''' checkOutErrors -- find n in error_list
        '''
        rtnval = False
        if n > 0 :
            for i in range(len(self.error_list)) :
                mask = 1 << i
                r = self.error_list.get(n&mask, None)
                if r is not None :
                    print("ALERT: {}, bit {}, {}".format(r[1], r[0], r[2]))
                    rtnval = True
        return rtnval

    @property
    def voltageslewrate(self) :
        ''' voltageslewrate -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['setvoltslew']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        try :
            d = float(d)
        except :
            d = 1.0
        return d
    @voltageslewrate.setter
    def voltageslewrate(self, value) :
        ''' fourwire -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        val = 0
        if isinstance(value, list) or isinstance(value, tuple) :
            val = value[0]
            self.lastChanList = value[1]
        elif isinstance(value, str) or isinstance(value, unicode) :
            val = value

        self.instrumentOpen()
        d = self.senddata(self.pwranacd['setvoltslew']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def fourwire(self) :
        ''' fourwire -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['wire4']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        return d
    @fourwire.setter
    def fourwire(self, value) :
        ''' fourwire -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        val = 0
        if isinstance(value, list) or isinstance(value, tuple) :
            val = value[0]
            self.lastChanList = value[1]
        elif isinstance(value, str) or isinstance(value, unicode) :
            val = value

        self.instrumentOpen()
        d = self.senddata(self.pwranacd['wire4']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def sweepinterval(self) :
        ''' fourwire -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['tinterval']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        return d
    @sweepinterval.setter
    def sweepinterval(self, value) :
        ''' fourwire -- get/set pair to set the PAs chan 1 to 4 wire mode
        '''
        val = 0.001
        if isinstance(value, list) or isinstance(value, tuple) :
            val = value[0]
            self.lastChanList = value[1]
        elif isinstance(value, float) or isinstance(value, str) or isinstance(value, unicode) :
            val = value

        self.instrumentOpen()
        d = self.senddata(self.pwranacd['tinterval']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def quadrant(self) :
        ''' quadrant -- get/set pair to set the PAs Emulation mode
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['quadrant']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
        except :
            pass
        return d
    @quadrant.setter
    def quadrant(self, value) :
        ''' quadrant -- get/set pair to set the PAs Emulation mode 
        '''
        if not (isinstance(value, str) or isinstance(value, unicode)) or str(value).upper() not in ['PS4Q', 'PS2Q', 'PS1Q', 'BATT', 'BATTERY', 'CHAR', 'CHARGER', 'CCL', 'CCLOAD', 'CVL', 'CVLOAD', 'VMET', 'VMETER', 'AMET', 'AMETER'] :
            value = 'PS2Q'
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['quadrant']['set'].format(value, self.lastChanList))
        self.instrumentClose()

    @property
    def quadrant2(self) :
        ''' quadrant2 -- get/set pair to set the PAs PS2Q mode ONLY
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['quadrant2']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
        except :
            pass
        return d
    @quadrant2.setter
    def quadrant2(self, value) :
        ''' quadrant2 -- get/set pair to set the PAs PS2Q mode ONLY
        '''
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['quadrant2']['set'].format(value, self.lastChanList))
        self.instrumentClose()

    @property
    def priority(self) :
        ''' priority -- get/set pair to set priority the PA chan 1
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['priority']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
        except :
            pass
        return d 
    @priority.setter
    def priority(self, value) :
        ''' priority -- get/set pair to set priority the PA chan 1
        '''
        self.instrumentOpen()
        val = value 
        if isinstance(value, unicode) or isinstance(value, str) :
            val = str(value)
            if val not in ['volt', 'VOLT', 'curr', 'CURR'] :
                val = 'VOLT'
        else :
            val = 'VOLT'
        d = self.senddata(self.pwranacd['priority']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def poscurlimit(self) :
        ''' poscurlimit -- get/set pair to get the positive current limit
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['poscurlimit']['get'].format(self.lastChanList)).strip()
        self.instrumentClose()
        try :
            d = float(d)
        except :
            pass
        return d 
    @poscurlimit.setter
    def poscurlimit(self, value) :
        ''' poscurlimit -- get/set pair to set the positive current limit
        '''
        try :
            val = float(value)
        except :
            val = 0.1
        else :
            val = min(3.0, val)
            val = max(0.0, val)
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['poscurlimit']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def negcurlimit(self) :
        ''' negcurlimit -- get/set pair to get the neg current limit
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['negcurlimit']['get'].format(self.lastChanList)).strip()
        self.instrumentClose()
        try :
            d = float(d)
        except :
            pass
        return d 
    @negcurlimit.setter
    def negcurlimit(self, value) :
        ''' negcurlimit -- get/set pair to set the neg current limit
        '''
        try :
            val = float(value)
        except :
            val = -0.1
        else :
            val = max(-3.0, val)
            val = min(0.0, val)
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['negcurlimit']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def curlimitcouple(self) :
        ''' curlimitcouple -- get/set pair to get pos/neg current limit tracking
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['curlimitcouple']['get'].format(self.lastChanList)).strip()
        self.instrumentClose()
        try :
            d = float(d)
        except :
            pass
        return d 
    @curlimitcouple.setter
    def curlimitcouple(self, value) :
        ''' curlimitcouple -- get/set pair to set pos/neg current limit tracking
        '''
        try :
            if value.upper() in self.validOnOfflst :
                val = value
            else :
                val = "OFF"
        except :
            val = "OFF" 
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['curlimitcouple']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def poscurrlimminmax(self) :
        ''' poscurrlimminmax -- get/set pair to get pos/neg current limit tracking
            getter is the only one needed
        '''
        self.instrumentOpen()
        dmin = self.sendgetdata(self.pwranacd['poscurlimitmin']['get'].format(self.lastChanList)).strip()
        dmax = self.sendgetdata(self.pwranacd['poscurlimitmax']['get'].format(self.lastChanList)).strip()
        self.instrumentClose()
        d = list()
        try :
            dmin = float(dmin)
            dmax = float(dmax)
        except :
            pass
        d.append(dmin)
        d.append(dmax)
        return d 
    @poscurrlimminmax.setter
    def poscurrlimminmax(self, _) :
        ''' poscurrlimminmax -- get/set pair to set pos/neg current limit tracking
        '''
        return True

    @property
    def negcurrlimminmax(self) :
        ''' negcurrlimminmax -- get/set pair to get pos/neg current limit tracking
            getter is the only one needed
        '''
        self.instrumentOpen()
        dmin = self.sendgetdata(self.pwranacd['negcurlimitmin']['get'].format(self.lastChanList)).strip()
        dmax = self.sendgetdata(self.pwranacd['negcurlimitmax']['get'].format(self.lastChanList)).strip()
        self.instrumentClose()
        d = list()
        try :
            dmin = float(dmin)
            dmax = float(dmax)
        except :
            pass
        d.append(dmin)
        d.append(dmax)
        return d 
    @negcurrlimminmax.setter
    def negcurrlimminmax(self, _) :
        ''' negcurrlimminmax -- get/set pair to set pos/neg current limit tracking
        '''
        return True

    @property
    def setvoltrange(self) :
        ''' setvoltrange -- get/set pair to get the PAs setvoltrange
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['setvoltrange']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        try :
            d = float(d)
        except :
            pass
        return d
    @setvoltrange.setter
    def setvoltrange(self, value) :
        ''' setvoltrange -- get/set pair to set the PAs setvoltrange
        '''
        try :
            val = float(value)
        except :
            val = 20.4

        self.instrumentOpen()
        d = self.senddata(self.pwranacd['setvoltrange']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def setvoltout(self) :
        ''' setvoltout -- get/set pair to get the PAs setvoltout 
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['setvoltout']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        try :
            d = float(d)
        except :
            pass
        return d
    @setvoltout.setter
    def setvoltout(self, value) :
        ''' setvoltout -- get/set pair to set the PAs setvoltout
        '''
        val = 0.0
        try :
            val = float(value)
        except :
            val = 0.0
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['setvoltout']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def setvoltlim(self) :
        ''' setvoltlim -- get/set pair to get the PAs setvoltlim 
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['setvoltlim']['get'].format(self.lastChanList))
        self.instrumentClose()
        d = d.strip()
        try :
            d = float(d)
        except :
            pass
        return d
    @setvoltlim.setter
    def setvoltlim(self, value) :
        ''' setvoltlim -- get/set pair to set the PAs setvoltlim
        '''
        val = 0.0
        try :
            val = float(value)
        except :
            val = 0.0
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['setvoltlim']['set'].format(val, self.lastChanList))
        self.instrumentClose()

    @property
    def enablechan(self) :
        ''' enablchan -- getter
            return the wheter the lastChanList chan is on or off
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['enablechan']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
        except :
            pass
        return d
    @enablechan.setter
    def enablechan(self, valuelst) :
        ''' enablechan -- setter
            Enables a channel or set of channels
            valuelst is a tuple/list of two values On/Off value, channel (list)
            This will assume the channel list is correct "1" or "1,2" or "1-3"  etc.
        '''
        if type(valuelst) is not list or len(valuelst) < 2 :
            valuelst = [valuelst, 1]

        try :
            onoff = str(valuelst[0]).upper()
            chan  = str(valuelst[1])
        except :
            onoff = 0
            chan  = 1
        if onoff not in self.validOnOfflst :
            onoff = "0"

        self.lastChanList = chan
        self.instrumentOpen()
        d = self.senddata(self.pwranacd['enablechan']['set'].format(onoff, chan))
        self.instrumentClose()

    @property
    def quescond(self) :
        ''' quescond -- getter
            return the wheter the lastChanList chan is on or off
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['quescond']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
            d = int(d)
        except :
            pass
        return d
    @quescond.setter
    def quescond(self, valuelst) :
        pass

    @property
    def clearoutputs(self) :
        ''' clearoutputs -- getter
            This will only return None.
            It wants to act like a getter but really isn't one.
        '''
        self.instrumentOpen()
        self.senddata(self.pwranacd['clearoutputs']['get'].format(self.lastChanList))
        self.instrumentClose()
        return None
    @quescond.setter
    def quescond(self, valuelst) :
        pass

    @property
    def voltprotdelay(self) :
        ''' voltprotdelay -- getter
            return the voltprotdelay value
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['voltprotdelay']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
            d = float(d)
        except :
            pass
        return d
    @voltprotdelay.setter
    def voltprotdelay(self, valuelst) :
        ''' voltprotdelay -- setter
            set the voltage protection delay value for 
            the supplied channel
        '''
        if type(valuelst) is not list or len(valuelst) < 2 :
            valuelst = [valuelst, self.lastChanList]

        try :
            timev = float(valuelst[0])
            chan  = str(valuelst[1])
        except :
            timev = 0
            chan  = 1

        self.lastChanList = chan
        self.instrumentOpen()
        self.senddata(self.pwranacd['voltprotdelay']['set'].format(timev, chan))
        self.instrumentClose()

    @property
    def modules(self) :
        ''' modules -- getter
            return the voltprotpos value
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['modules']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
        except :
            pass
        return d
    @modules.setter
    def modules(self, valuelst) :
        ''' modules -- setter
            set the voltage protection delay value for 
            the supplied channel
        '''
        pass

    @property
    def voltprotpos(self) :
        ''' voltprotpos -- getter
            return the voltprotpos value
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['voltprotpos']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
            if d == "MIN" :
                d = 0.0
            elif d == "MAX" :
                d = 1000.0
            else :
                d = float(d)
        except :
            pass
        return d
    @voltprotpos.setter
    def voltprotpos(self, valuelst) :
        ''' voltprotpos -- setter
            set the voltage protection delay value for 
            the supplied channel
        '''
        if type(valuelst) is not list or len(valuelst) < 2 :
            valuelst = [valuelst, self.lastChanList]

        level = valuelst[0] 
        try :
            chan  = str(valuelst[1])
        except :
            chan  = 1
        if not (type(valuelst[0]) == str and (valuelst[0].upper() == "MIN" or valuelst[0].upper() == "MAX")) :
            try :
                level = float(valuelst[0])
            except :
                level = "MAX"

        self.lastChanList = chan
        self.instrumentOpen()
        self.senddata(self.pwranacd['voltprotpos']['set'].format(level, chan))
        self.instrumentClose()

    @property
    def voltprotneg(self) :
        ''' voltprotneg -- getter
            return the voltprotneg value
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.pwranacd['voltprotneg']['get'].format(self.lastChanList))
        self.instrumentClose()
        try :
            d = str(d).strip()
            if d == "MIN" :
                d = 0.0
            elif d == "MAX" :
                d = 1000.0
            else :
                d = float(d)
        except :
            pass
        return d
    @voltprotneg.setter
    def voltprotneg(self, valuelst) :
        ''' voltprotneg -- setter
            set the voltage protection delay value for 
            the supplied channel
        '''
        if type(valuelst) is not list or len(valuelst) < 2 :
            valuelst = [valuelst, self.lastChanList]

        level = valuelst[0] 
        try :
            chan  = str(valuelst[1])
        except :
            chan  = 1
        if not (type(valuelst[0]) == str and (valuelst[0].upper() == "MIN" or valuelst[0].upper() == "MAX")) :
            try :
                level = float(valuelst[0])
            except :
                level = "MAX"

        self.lastChanList = chan
        self.instrumentOpen()
        self.senddata(self.pwranacd['voltprotneg']['set'].format(level, chan))
        self.instrumentClose()

    @property
    def lastchan(self) :
        ''' lastchan -- gettert 
            returns the current lastChanList
        '''
        return self.lastChanList
    @lastchan.setter
    def lastchan(self, chan) :
        ''' lastchan -- setter
            sets the class variable lastChanList so enablechan can read channels 
            randomly
        '''
        self.lastChanList = chan

if __name__ == '__main__' :

    pa = PWRANACom(debug=False)
    pa.clrstatus = ""
    n = pa.quescond
    d = pa.voltprotdelay
    f = pa.clearoutputs 
    g = pa.voltprotpos
    gn = 0 #pa.voltprotneg
    vv = pa.modules
    print("vv={}".format(vv))
    gg = pa.sweepinterval
    pa.sweepinterval = (123.4567, 1)
    ggg = pa.sweepinterval

    print('gg={} ggg={}'.format(gg, ggg))

    #if type(g) == float :
        #g = "{:5.2f}".format(g)
    #if type(gn) == float :
        #gn = "{:5.2f}".format(gn)
    #print("ques cond = 0x{:03X} voltprotdelay = {:5.2f} voltprotpos = {} voltprotneg = {}".format(n, d, g, gn))

    #for i in range(15) :
        #n = 0
        #if i > 0 :
           #n = 1 << (i-1)
        #print("{} : ".format(i), end="")
        #r = pa.checkOutErrors(n)
        #print(" {}".format(repr(r)))
