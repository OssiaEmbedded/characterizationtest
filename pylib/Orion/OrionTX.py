#!/usr/bin/python3

""" OrionTx.py: An interface module to provide communication to
                the Orion Message Manager

THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.

"""
import re
import json
import socket
import select
import signal
from threading  import Thread

from optparse import OptionParser
from time     import sleep, time
import traceback
import sys

from Orion.jcdDict import *
state_good_list = [2, 4, 8, 16, 17]

RCVR_STATE = {

    'RCVR_STATE_UNKNOWN'    : 0, 
    'RCVR_STATE_READY'      : 1,  
    'RCVR_STATE_CHARGING'   : 2,
    'RCVR_STATE_DISCOVERY'  : 3,
    'RCVR_STATE_REGISTERED' : 4,
    'RCVR_STATE_JOINED'     : 5,
    'RCVR_STATE_SILENT'     : 6,
    'RCVR_STATE_DISCONNECT' : 7,

            }


def getCommandData(pyData, prevCmdNum, offset, num, byteval=True) :
    """ getCommandData -- Requires the data from a client_command client_command_data pair
        returns an integer
    """
    rawDataValues = pyData['Result'].get('Values', [])
    rawDataStr = ''
    for adict in rawDataValues :
        if adict.get("Data", None) is not None :
            rawDataStr = adict['Data']
            break
    rawData = rawDataStr.split("(", 1)[0].split(' ')
    rtnval = 0
    try :
        for i in range(num) :
            ii = i + offset
            if byteval :
                rtnval += (int(rawData[ii], 0) << (8 * i))
            else :
                rtnval += (int(rawData[ii], 0) << i)
    except ValueError :
        print("ValueError in client command {} data {}".format(prevCmdNum, repr(rawData)))
        rtnval = -2
    except IndexError :
        print("IndexError in client command {} data {}".format(prevCmdNum, repr(rawData)))
        rtnval = -3
    return rtnval

def getVersions(pyDict) :
    ''' getVersions -- gets the know software version info from the return dict
    '''
    vers = pyDict['Result']
    swRelMajor  = int(vers.get("Major Version", "-1"))
    swRelMinor  = int(vers.get("Minor Version", "-1"))
    gitComt     = int(vers.get("Git Commit", "aabbccddeeff"), 16)
    swRel = "SWRel:{:d}.{:d}.{:x}\n".format(swRelMajor, swRelMinor, gitComt)
    timestamp = vers.get('Timestamp', "Today")
    proxyM = int(vers.get("Proxy Major Revision", "-1"))
    proxym = int(vers.get("Proxy Minor Revision", "-1"))
    versionstr = swRel + timestamp + "\nProxyVer:{:d}.{:d}\n".format(proxyM, proxym)
    return versionstr, swRel

def processClientList(pyData, clientList) :
    ''' processClientList -- Checks the requested clients against what the charger finds.
    '''
    rtnval = True
    rtndict = dict()
    try :
        availList = pyData['Result']['Receivers']
        l_clientList = list()
        l_clientStat = dict()
        for aClient in availList :
            try :
                aCid  = str(aClient['RX ID'].strip())
                aLnkQ = aClient['LinkQuality']
            except ValueError as ve :
                rtnval = False
                print("ValueError in processClientList -- {}".format(repr(ve)))
                break

            try :
                aStatLst = aClient['State']
                aStat = RCVR_STATE.get(aStatLst, 0)
                aStatMsg = aStatLst
            except ValueError :
                rtnval = False
                sStat = 0
                aStatMsg = "Unknown"
                    
            l_clientList.append(aCid)
            l_clientStat.update({aCid : [aStat, aLnkQ, aStatMsg]})

        if rtnval :
            # now check the input client list against the
            # data that came back from the Tile
            rtnval = False
            for cidl in clientList :
                cid = cidl
                als = ""
                # check for a clientID, alias list
                if type(cidl) is list or type(cidl) is tuple :
                    cid = cidl[0] # client ID
                    als = cidl[1] # alias 
                if cid in l_clientList :
                    theStat = l_clientStat.get(cid, [0,0,""])[0] 
                    if theStat in state_good_list : # good state list copied from data in Host MCU code
                        rtnval = True
                    rtndict.update({cid : l_clientStat.get(cid, [0, 0, ""]) + [als]}) # rtndict (cid : [state, linkQ, alias]}
                else :
                    rtndict.update({cid : [0, 0, "", als]})
    except Exception as ex :
        print("Unexpected Exception in jcs.processClientList --- {}\n".format(repr(ex)))
        print("Traceback -- {} \n".format(traceback.format_exc()))
        print("Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    return rtnval, rtndict

def getClientState(pyData) :
    """ requires a client_detail return value as input
    """
    rtnval = True
    statelst = pyData.get("Result", {}).get("State", "RCVR_STATE_UNKNOWN" )
    state    = RCVR_STATE.get(statelst, 0)
    msglabel = statelst
    if "error" in msglabel.lower() :
        rtnval = False
    return rtnval, state, msglabel


def checkStatusReturn(pyData) :
    """ requires a return dict from one of the following commands:
        start_charging *
        client_list 
        * are the ones currently used
        returns False indicates that the check failed
                True  indicates all is well.
    """
    STOP_CHARGING = 17
    aStat = 8
    rtnval = False
    aClient = pyData['Result'].get('Receivers', pyData['Result'])
    if type(aClient) is list :
        aClient = aClient[0] # assumption: only one in the list
        try :
            aStatLst = aClient.get('State', "RCVR_STATE_UNKNOWN")
            aStat = RCVR_STATE.get(aStatLst, 0)
            aStatMsg = aStatLst
        except (ValueError, IndexError) :
            rtnval = True
            aStat = 0
    else :
        rstatus = aClient.get('Status Flags', "ERROR")
        aStat = STOP_CHARGING
        aStatMsg = "Stopped charging" if rstatus == 'SUCCESS' else "Failed to stop charging"

    if aStat in state_good_list :
        rtnval = True 

    return rtnval, aStat, aStatMsg

def getComChannel(pyData) :
    """ getComChannel -- Requires data from a "get_com_channel" command
        @param: pyData the result of a json loads operation
        returns: Current COM Channel
    """
    com_chan = int(pyData['Result'].get("COM Channel", '0'))
    return com_chan

def getClientVersion(pyData) :
    """ getClientVersion -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """
    version = pyData['Result'].get("Version", '0')
    return str(version)

def getClientComRSSI(pyData) :
    """ getClientComRSSI -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """
    rssival = int(pyData['Result'].get("RSSI", '-255'))
    return rssival

def getBatteryLevel(pyData) :
    """ getBatteryLevel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Battery Level
    """
    val = int(pyData['Result'].get("State of Charge", '0'))
    return val

def getDeviceStatus(pyData) :
    """ getDeviceStatus -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Status
    """
    aStatLst = pyData['Result'].get('State', "RCVR_STATE_UNKNOWN")
    val = RCVR_STATE.get(aStatLst, 0)
    return val

def getChanStatRev(pyData) :
    """ getChanStatRev -- Requires data from a "channel_info" operation So this is now a dummy method
        @param: pyData the result of a json loads operation
        returns: a tuple of status and revision
    """
    return ("ON", "0.0")

def getDeviceModel(pyData) :
    """ getDeviceModel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Model number as string
    """
    val = pyData['Result'].get("Model", '0')
    return str(val)

def getTPSMissed(pyData) :
    """ getTPSMissed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: TPSMissed
    """
    val = int(pyData['Result'].get("Status Flags", {}).get("missedTpsCycles", '0'))
    return val

def getClientShortID(pyData) :
    """ getClientShortID -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Short ID as a string
    """
    val = pyData['Result'].get("Short ID", '0')
    return val

def getClientLinkQuality(pyData) :
    """ getClientLinkQuality -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: LinkQuality
    """
    val = int(pyData['Result'].get("LinkQuality", '0'))
    return val

def getClientNetCurrent(pyData) :
    """ getClientNetCurrent -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns Net current as a float in units mA
    """
    val = float(pyData['Result'].get("Net Current", '0'))
    return val

def getClientQueryTime(pyData) :
    """ getClientQueryTime -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns query time a string
    """
    return pyData['Result']['Query Time']

def getClientQueryFailed(pyData) :
    """ getClientQueryFailed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the number of failed queries as an int 
    """
    return 0

def getProxyComRSSI(pyData) :
    """ getProxyComRSSI -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """
    
    return -12

def getAveragePower(pyData) :
    """ getAveragePower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the Average power as an int 
    """
    val = int(pyData['Result'].get('Avg Power', 0))
    return val

def getPeakPower(pyData) :
    """ getPeakPower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the peak power as an float
    """
    val = int(pyData['Result'].get('Peak Power', 0))
    return val

def getClientBatteryVoltage(pyData) :
    """ getClientBatteryVoltage -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns the Battery Voltage as a float in units Volts
    """
    bv = int(pyData['Result'].get('Battery Voltage', '-1000'))
    return float(bv) / 1000.0

def processClientData(pyData) :
    """ processClientData -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns: data list
    """
    return pyData['Result']

def getClientRFPower(data, whichone) : # returns a tuple of RSSI values and the entire list
    """ getClientRFPower -- Requires a pre-processed data listfrom a "client_data" operation
        @param: data the result of a json loads operation pre-processed into a list of ints
        @param: whichone - is an index from 1 to 4. Designating a port on the antenna.
        returns: four values, as a tuple:
                 a tuple of maxZCSCount and CalcPower for max and avg.
    """

    maxRfPwr = int(data.get('MaxRFPower{}'.format(whichone), -1))
    maxRfdBm = int(data.get('MaxRFPower{} mDBm'.format(whichone), -1))
    avgRfPwr = int(data.get('AvgRFPower{}'.format(whichone), -1))
    avgRfdBm = int(data.get('AvgRFPower{} mDBm'.format(whichone), -1))
    return (maxRfPwr, maxRfdBm, avgRfPwr, avgRfdBm)

def getTransmitterPowerLevel(pyData) :
    """ getTransmitterPowerLevel -- Requires data from a "get_power_level" operation
        @param: pyData the result of a json loads operation
        returns: the Transmitter power level as an int in units dBm
    """
    val = pyData['Result'].get('PowerLevel', 0)
    return int(val)

def getSystemTemp(pyData, sensor=0) :
    """ getTransmitterTemp -- Requires data from a "get_system_temp" operation
        @param: pyData the result of a json loads operation
        returns: the system Temperature in Deg C.
    """
    namefmt = "CCB{}"
    if sensor == 1 :
        namefmt = "AMB{}"
    index = 0
    divid = 0
    temp = 0.0
    templst = list()
    while True :
        keyname = namefmt.format(index)
        val = pyData['Result'].get(keyname, None)
        index += 1
        if  val is None :
            if divid > 0 :
                temp /= divid
            break
        templst.append(val)
        val = float(val.split(" ", 1)[0])
        if val > 0.0 :
            temp += val
            divid += 1
    return [temp, templst]

def checkForSuccess(pyData, ckValList=['SUCCESS']) :
    ''' checkForSuccess -- returns true if the status of in input data does NOT match the ckVal.
    False otherwise.
    '''
    rtnval = True
    statusVal = pyData['Result'].get('Status', "Error")
    for ckVal in ckValList :
        if ckVal in statusVal :
            rtnval = False
            break
    return rtnval

def getTxId(pyData) :
    ''' getTxId -- requires data from a previous call to get_charger_id
    returns the Transmitters identification string.
    '''
    months          = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                       'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    LineThatHasBoth = 3
    hostNameField   = 3
    buildTypeField  = 9
    
    rtnlist = pyData['Result'].get('Start Status', ["Jan", "1", "00:00:00", "OrionXX","","","","","","['DEMO'"])
    linelist = rtnlist[LineThatHasBoth].split(' ')
    rtnval = "Unknown"
    if linelist[0] in months :
        hostname  = linelist[hostNameField]
        buildType = linelist[buildTypeField].rstrip("',").lstrip("['")
        rtnval = "{}_{}".format(hostname, buildType)
    return rtnval

def checkArgs(argsin, argsneeded, deflst) :
    """ checkArgs -- argsin     Is a list of one or more arguments 
                     argsNeeded Is a int or list of the possible num of args.
                                An empty list in this list says to present the remaining args as a list
                                A list specifies optional arguments.
                     deflst     Is a default value or list of values if the user has not specified 
                                a value. Only used when argsneeded is a list.
                     Returns    True if number of args is valid and
                                a new list of arguments
    """
    def mkArgs(argsin, argsneed, deft) :
        """ mkargs -- Consolidates code that is re-used in multiple places in this function only
                      Insures that the entire list of arguments is populated with provided values
                      or default values.
                      argsneed is a list
                      argsin is a list
                      deft is a value of any type
        """
        idx  = 0
        leng = len(argsin)
        big  = leng if not argsneed else max(argsneed)
        argout = list()
        while idx < big and leng >= min(argsneed) : 
            # default arg can be a list different ones for different optional args
            if type(deft) is list :
               defval = deft[idx]
            else :
                defval = deft
            if idx < leng :
                argval = argsin[idx]
                # to support a list of values but still have an optional arg
                if "," in argval :
                    argval = "[{}]".format(argval)
                argout.append(argval) # add the input arg
            else :
                argout.append(defval) # add the default arg
            idx += 1
        return argout, idx

    argsout = list()
    # assume the last arg in argsneeded might be a list
    if type(argsneeded) is list and type(argsneeded[-1]) is list :
        argslst, cnt = mkArgs(argsin, argsneeded[:-1], deflst)
        argsout = argslst
        argsout.append(argsin[cnt:])
    elif type(argsneeded) is list :
        argsout, _ = mkArgs(argsin, argsneeded, deflst)
    else :
        if len(argsin) == argsneeded :
            argsout, _ = mkArgs(argsin, [argsneeded], deflst)
        else :
            argsout = None
    return argsout

class OrionTx(object) :
    """ OrionTx -- A class to provide socket communications to the Orion Message Manager
                   and send the JSON strings populated with expected input pramamters
    """

    def __init__(self, ipaddr, port=50000, debug=False) :
        # init the base class. Not stated in the example online.

        self.debug = debug
        self.ipaddress = socket.gethostbyname(ipaddr)
        self.portnum = int(port)
        self.lsock = None
        self.inputs = []
        self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self.connectToServer() is None :
           return None 

    def __del__(self) :
        if self.lsock :
            self.lsock.close()
            self.lsock = None

    def connectToServer(self, reconnect=False) :
        if reconnect :
            try :
                self.lsock.close()
            except :
                pass
            finally :
                self.lsock = None
                self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try :
            self.lsock.connect((self.ipaddress, self.portnum))
        except Exception as ee :
            print("Could not connect to server at address {} port {} -- {}".format(self.ipaddress, self.portnum, repr(ee)))
            self.lsock = None 
        else :
            self.lsock.setblocking(0)
            self.inputs = [self.lsock]
        return self.lsock

    def sendGetTransmitter(self, arglist) :
        if arglist[0] == 'rx_data' :
            arglist[0] = 'rx_detail'
        strval = arglist[0]
        for val in arglist[1:] :
            strval += " {}".format(val)
        pyrslt = self.talkToServerWithCommand(strval)
        if type(pyrslt) == str :
            print("Error in send -- {}".format(pyrslt))
            if self.connectToServer(True) :
                print("Reconnected -- Resending data -- {}".format(strval))
                pyrslt = self.talkToServerWithCommand(strval)
            else :
                pyrslt = {"Result" : {"Status" : pyrslt}}
        return pyrslt

    def talkToServerWithCommand(self, command) :
        """ This method sends and receives messages with the mmServer running on
            the machine using the ip address presented to the CLI when started.
        """
        pyrslt = {'Result' : {'Status' : "ERROR"}}
        keepgoing = False # support for test method
        argv = command.split()
        if self.debug :
            print("talkTo -- line = '{}' argv = '{}'".format(command, argv))
        cmd = jcd.get(argv[0], None)
        if self.debug :
            print("cmd json = '{}'".format(cmd))
        rtnstr = "Unknown Command {}".format(repr(argv[0]))
        if cmd is not None :
            argsneeded = cmd.get('numofargs', -1)
            deflt = cmd.get("NotSet", "0xFF")

            # go create of list of args from the input values and default values
            args = checkArgs(argv[1:], argsneeded, deflt)
            if args is not None :
                jstr = cmd.get('jstr', "")
                if self.debug :
                    print("args='{}'".format(repr(args)))
                # fill the args list into the JSON string
                jstrcmd = jstr % (tuple(args))
                if self.debug :
                    print("talkTo: jstrcmd="+jstrcmd)
    
                # first remove some unnecessary spaces
                pattern = re.compile('}, *"Result')
                jstrcmd = re.sub(pattern, '}, "Result', jstrcmd)
    
                # send out the data, the easy part
                try :
                    self.lsock.sendall(jstrcmd.encode('utf-8'))
                except Exception as ee :
                    return "Exception while sending data -- {}".format(repr(ee))
    
                # setup variables for the read
                jsonval = ''
                rddataflag = True
                rtnstr = ""
    
                # these two values work together to equal 20 seconds wait before giving up
                cnt     = cmd.get("LongRead", 3800) # the long read is for those commands that
                                                  # take a long time to finish sending data on the spi bus.
                timeout = 0.005 # units of seconds
    
                while rddataflag and cnt > 0 :
                    cnt -= 1
                    readable, _, _ = select.select(self.inputs, [], [], timeout)
                    if readable :
                        if self.debug :
                            print("readable cnt={}".format(cnt))
                        for sock in readable :
                            dat = sock.recv(16384)
                            if dat  :
                                jsonval += dat.decode('utf-8')
                                cnt = 1 # we're reading the data so we should not have to wait any longer
                            else :
                                rddataflag=False
                                break
                    elif cnt <= 0 :
                        rddataflag = False
                    else :
                        if self.debug :
                            print("NOT readable cnt={}".format(cnt))
                if jsonval == '' :
                    jsonval = '{"Result" : {"Status":"No Response from Server"}}'
    
                if self.debug :
                    print("\nJSON {}\n".format(repr(jsonval)))
    
                try :
                    pyrslt =  json.loads(jsonval)
                except ValueError as ve :
                    print("json.loads failure '{}' -- {}".format(jsonval, repr(ve)))
                    print("Server did not respond")
                else :
                    if self.debug :
                        print("PY {}\n".format(repr(pyrslt)))
                        print("ARGUMENTS {}\n".format(repr(args)))
                    if pyrslt is not None :
                        keepgoing = True
                        if type(pyrslt) is dict :
                            rtnstr = pyrslt['Result']['Status']
                        else :
                            rtnstr = "ERROR: Unexpected Result type '{}' -- '{}".format(type(pyrslt), repr(pyrslt))
                            keepgoing = False
                    else :
                        rtnstr = "ERROR: No results returned"
            else :
                rtnstr = "Incorrect number of required arguments got {} needed {}".format(len(argv[1:]), argsneeded)

        if self.debug :
            print("rtnstr = '{}'".format(rtnstr))

        return pyrslt



if __name__ == '__main__' :

    parser = OptionParser()

    parser.add_option("-p","--port", dest='port',     action='store',     default='50000',     help="Device port. def='%default'")
    parser.add_option("-i","--ip",   dest='ipaddr',   action='store',     default='localhost', help="Device Network Addrs def='%default'")
    parser.add_option("-d","--debug",dest='debug',    action="store_true",default=False,       help="Print debug info. def='%default'")

    (options, args) = parser.parse_args()

    if options.debug :
        print(repr(options))
        print(repr(args))

    orion = OrionTx(options.ipaddr, options.port, options.debug)

    rtndict = orion.sendGetTransmitter(["mm_version"])
    #print(repr(rtndict))
    if not checkForSuccess(rtndict) :
        print("Success")
    else :
        print("Not Success")
    rtndict = orion.sendGetTransmitter(["versions"])
    #print(repr(rtndict))
    if not checkForSuccess(rtndict) :
        print("Success")
    else :
        print("Not Success")
    verstr, swRel = getVersions(rtndict)
    print("getVersions verstr '{}' {}".format(verstr, swRel))

    pyData = orion.sendGetTransmitter(["client_detail", "0x124B0007A17049"])
    #print(repr(pyData))
    if not checkForSuccess(pyData) :
        print("Success")
    else :
        print("Not Success")

    rtn, stat, msg  = checkStatusReturn(pyData)
    print("checkStatusReturn rtn = {} {} {}".format(repr(rtn), stat, msg))
    rtn = getClientVersion(pyData)
    print("getClientVersion rtn = {}".format(repr(rtn)))
    rtn = getClientComRSSI(pyData)
    print("getClientComRSSI rtn = {}".format(repr(rtn)))
    rtn = getBatteryLevel(pyData)
    print("getBatteryLevel rtn = {}".format(repr(rtn)))
    rtn = getDeviceStatus(pyData)
    print("getDeviceStatus rtn = {}".format(repr(rtn)))
    rtn = getChanStatRev(pyData)
    print("getChanStatRev rtn = {}".format(repr(rtn)))
    rtn = getDeviceModel(pyData)
    print("getDeviceModel rtn = {}".format(repr(rtn)))
    rtn = getTPSMissed(pyData)
    print("getTPSMissed rtn = {}".format(repr(rtn)))
    rtn = getClientShortID(pyData)
    print("getClientShortID rtn = {}".format(repr(rtn)))
    rtn = getClientLinkQuality(pyData)
    print("getClientLinkQuality rtn = {}".format(repr(rtn)))
    rtn = getClientNetCurrent(pyData)
    print("getClientNetCurrent rtn = {}".format(repr(rtn)))
    rtn = getClientQueryTime(pyData)
    print("getClientQueryTime rtn = {}".format(repr(rtn)))
    rtn = getClientQueryFailed(pyData)
    print("getClientQueryFailed rtn = {}".format(repr(rtn)))
    rtn = getProxyComRSSI(pyData)
    print("getProxyComRSSI rtn = {}".format(repr(rtn)))
    rtn = getAveragePower(pyData)
    print("getAeragePower rtn = {}".format(repr(rtn)))
    rtn = getPeakPower(pyData)
    print("getPeakPower rtn = {}".format(repr(rtn)))
    rtn = getClientBatteryVoltage(pyData)
    print("getClientBatteryVoltage rtn = {}".format(repr(rtn)))
    rtn = processClientData(pyData)
    #print("processClientData rtn = {}".format(repr(rtn)))
    rtn = getClientRFPower(rtn, 1)
    print("getClientRFPower rtn = {}".format(repr(rtn)))

    pyData = orion.sendGetTransmitter(['get_power_level'])
    rtn = getTransmitterPowerLevel(pyData)
    print("getTransmitterPowerLevel rtn = {}".format(repr(rtn)))

    pyData = orion.sendGetTransmitter(['get_comm_channel'])
    rtn = getComChannel(pyData)
    print("getComChannel rtn = {}".format(repr(rtn)))

    pyData = orion.sendGetTransmitter(['get_system_temp', '0'])
    print("\npyData\n{}\n".format(repr(pyData)))
    rtn = getSystemTemp(pyData, sensor=0)
    print("getSystemTemp rtn = {}".format(repr(rtn)))
    pyData = orion.sendGetTransmitter(['get_system_temp', '1'])
    rtn = getSystemTemp(pyData, sensor=1)
    print("getSystemTemp rtn = {}".format(repr(rtn)))
    pyData = orion.sendGetTransmitter(['get_charger_id'])
    rtn = getTxId(pyData)
    print("getTxId rtn = {}".format(repr(rtn)))

    pyData = orion.sendGetTransmitter(['client_list'])
    rtn, rdict = processClientList(pyData, ['0x124B0007BA030D'])
    print("processClientList1 {} {}".format(repr(rtn), repr(rdict)))
    rtn, rdict = processClientList(pyData, [['0x124B0007BA030D',"gold"]])
    print("processClientList2 {} {}".format(repr(rtn), repr(rdict)))

