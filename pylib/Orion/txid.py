
server_data = {'Command': {'Type': 'GetServerStartStatus'
            },
 'Result':  {'Status': 'SUCCESS',
             'Start Status': ['-- Logs begin at Fri 2021-11-19 11:17:01 PST, end at Tue 2021-11-30 12:01:13 PST. --',
                              'Nov 19 11:54:12 Orion9 systemd[1]: Started Message Manager Service at port 50000.',
                              'Nov 19 11:54:14 Orion9 python3[562]: SPI Ready',
                              "Nov 19 11:54:14 Orion9 python3[562]: Set RCVR build type ['DEMO', 'Demo build used for general power delivery']",
                              'Nov 19 11:54:14 Orion9 python3[562]: processMessagesThread Started and ready to process log messages.',
                              'Nov 19 11:54:14 Orion9 python3[562]: Bind Succeeded!!',
                              'Nov 23 15:40:13 Orion9 sudo[1068]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/bin/journalctl --no-pager -u mmserver',
                              'Nov 23 15:40:13 Orion9 sudo[1068]: pam_unix(sudo:session): session opened for user root by (uid=0)',
                              'Nov 23 15:40:13 Orion9 sudo[1068]: pam_unix(sudo:session): session closed for user root',
                              'Nov 23 15:46:44 Orion9 sudo[1079]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/bin/journalctl --no-pager -u mmserver',
                              'Nov 23 15:46:44 Orion9 sudo[1079]: pam_unix(sudo:session): session opened for user root by (uid=0)',
                              'Nov 23 15:46:44 Orion9 sudo[1079]: pam_unix(sudo:session): session closed for user root',
                              'Nov 23 18:16:30 Orion9 sudo[1160]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/bin/journalctl --no-pager -u mmserver',
                              'Nov 23 18:16:30 Orion9 sudo[1160]: pam_unix(sudo:session): session opened for user root by (uid=0)',
                              'Nov 23 18:16:30 Orion9 sudo[1160]: pam_unix(sudo:session): session closed for user root',
                              'Nov 30 12:00:56 Orion9 sudo[8902]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/bin/journalctl --no-pager -u mmserver',
                              'Nov 30 12:00:56 Orion9 sudo[8902]: pam_unix(sudo:session): session opened for user root by (uid=0)',
                              'Nov 30 12:00:56 Orion9 sudo[8902]: pam_unix(sudo:session): session closed for user root',
                              'Nov 30 12:01:13 Orion9 sudo[8912]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/bin/journalctl --no-pager -u mmserver',
                              'Nov 30 12:01:13 Orion9 sudo[8912]: pam_unix(sudo:session): session opened for user root by (uid=0)',
                              ''
                            ]
            }
}

def getTxId(pyData) :
    ''' getTxId -- requires data from a previous call to get_charger_id
    returns the Transmitters idetification string.
    '''
    months          = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                       'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    LineThatHasBoth = 3
    hostNameField   = 3
    buildTypeField  = 9
    
    rtnlist = pyData['Result'].get('Start Status', ["Jan", "1", "00:00:00", "OrionXX","","","","","","DEMO"])
    linelist = rtnlist[LineThatHasBoth].split(' ')
    rtnval = "Unknown"
    if linelist[0] in months :
        hostname  = linelist[hostNameField]
        buildType = linelist[buildTypeField].rstrip("',").lstrip("['")
        rtnval = "{}_{}".format(hostname, buildType)
    return rtnval

if __name__ == "__main__" :
    r=getTxId(server_data)
    print("rtn = {}".format(r))
