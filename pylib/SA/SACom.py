""" SACom.py: A class to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the SACommStrings dictionary.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
from time import sleep, time
from SA.SACommStrings import SA_CMD_DICT as sacd
from LIB.InstrumentBase import InstrumentBase

class SACom(InstrumentBase) :
    ''' SACom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='A-N9020A-11404.ossiainc.local', port=5025, debug=False, marker=1, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        super(SACom, self).__init__(addr, port, "Spectrum Analyzer", False, None, debug, timethis)
        self.marker  = marker
        self.saOpen  = self.instrumentOpen
        self.saClose = self.instrumentClose

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    def getSaveImage(self, filename) :

        url = bytearray(b'GET /Agilent.SA.WebInstrument/Screen.png HTTP/1.1\r\nHost: 10.10.0.83:7081\r\nUser-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\n\r\n')
        sa = InstrumentBase(port=80)
        sa.instrumentOpen()
        saResponse = sa.sendgetimage(url)
        sa.instrumentClose()

        startOfImageData = bytearray(b'\r\n\r\n') 
        imageIndex = saResponse.find(startOfImageData) + 4
        with open(filename, "wb") as fd :
            fd.write(saResponse[imageIndex:])
            fd.close

    @property
    def usesa(self) :
        ''' usesa -- retrieve the base class useinstrument
        '''
        return self.useinstrument 
    @usesa.setter
    def usesa(self, value) :
        ''' usesa -- set the base class bool to the bool of value
                     Any type of value can be passed in but the
                     base class value will remain bool
        '''
        self.useinstrument = bool(value)

    @property
    def getLog(self) :
        ''' getLog -- retrieve the last arg number of log entires
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['getLog']['get'])
        self.saClose()
        return d
    @getLog.setter
    def getLog(self, value) :
        self.saOpen()
        d = self.sendgetdata(sacd['getLog']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resetsa(self) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resetsa']['get'].format(self.marker))
        self.saClose()
        return d
    @resetsa.setter
    def resetsa(self, marker) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resetsa']['set'].format(marker))
        self.saClose()
        self.marker = marker
        return d

    @property
    def selectMarker(self) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['selectMarker']['get'].format(self.marker))
        self.saClose()
        return d
    @selectMarker.setter
    def selectMarker(self, marker) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.senddata(sacd['selectMarker']['set'].format(marker))
        self.saClose()
        self.marker = marker
        return d

    @property
    def centerFrequency(self) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['centerFrequency']['get'])
        self.saClose()
        return d
    @centerFrequency.setter
    def centerFrequency(self, value) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        rtnval =  self.senddata(sacd['centerFrequency']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def frequencySpan(self) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['frequencySpan']['get'])
        self.saClose()
        return d
    @frequencySpan.setter
    def frequencySpan(self, value) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        rtnval = self.senddata(sacd['frequencySpan']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def videoBW(self) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBW']['get'])
        self.saClose()
        return d
    @videoBW.setter
    def videoBW(self, value) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['videoBW']['set'] + str(value))
        self.saClose()
        return d

    @property
    def videoBWAuto(self) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBWAuto']['get'])
        self.saClose()
        return d
    @videoBWAuto.setter
    def videoBWAuto(self, value) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['videoBWAuto']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resolutionBWAuto(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBWAuto']['get'])
        self.saClose()
        return d
    @resolutionBWAuto.setter
    def resolutionBWAuto(self, value) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resolutionBWAuto']['set'] + str(value))
        self.saClose()
        return d

    @property
    def averageCount(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['averageCount']['get'])
        self.saClose()
        return d
    @averageCount.setter
    def averageCount(self, value) :
        ''' averageCount -- get/set pair to set/get the measure average/holt count number
        '''
        self.saOpen()
        d = self.senddata(sacd['averageCount']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resolutionBW(self) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBW']['get'])
        self.saClose()
        return d
    @resolutionBW.setter
    def resolutionBW(self, value) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resolutionBW']['set'] + str(value))
        self.saClose()
        return d

    @property
    def referenceLevel(self) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevel']['get'])
        self.saClose()
        return d
    @referenceLevel.setter
    def referenceLevel(self, value) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['referenceLevel']['set'] + str(value))
        self.saClose()
        return d

    @property
    def referenceLevelOffset(self) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevelOffset']['get'])
        self.saClose()
        return d
    @referenceLevelOffset.setter
    def referenceLevelOffset(self, value) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['referenceLevelOffset']['set'] + str(value))
        self.saClose()
        return d

    @property
    def displayUnits(self) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['displayUnits']['get'])
        self.saClose()
        return d
    @displayUnits.setter
    def displayUnits(self, value) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['displayUnits']['set'] + str(value))
        self.saClose()
        return d

    @property
    def triggerHold(self) :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerHold']['get'])
        self.saClose()
        return ampl
    @triggerHold.setter
    def triggerHold(self, value='') :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        d = self.senddata(sacd['triggerHold']['set'])
        self.saClose()
        return d

    @property
    def triggerClear(self) :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerClear']['get'])
        self.saClose()
        return ampl
    @triggerClear.setter
    def triggerClear(self, value='') :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        d = self.senddata(sacd['triggerClear']['set'])
        self.saClose()
        return d

    @property
    def sweepTime(self) :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepTime']['get'])
        self.saClose()
        return ampl
    @sweepTime.setter
    def sweepTime(self, value='') :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['sweepTime']['set'] + str(value))
        self.saClose()
        return d

    @property
    def sweepPoints(self) :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepPoints']['get'])
        self.saClose()
        return ampl
    @sweepPoints.setter
    def sweepPoints(self, value='') :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['sweepPoints']['set'] + str(value))
        self.saClose()
        return d

    @property
    def traceData(self) :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        self.saOpen()
        trace = " trace{}".format(self.marker)
        print(sacd['traceData']['get'] + trace)
        rtnval = self.sendgetdata(sacd['traceData']['get'] + trace)
        self.saClose()
        return rtnval
    @traceData.setter
    def traceData(self, value='') :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        rtnval = ""
        if sacd['traceData']['argc'] > 0 :
            self.saOpen()
            print(sacd['traceData']['set'] + str(value))
            rtnval = self.senddata(sacd['traceData']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def singleContinuousSweep(self) :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['singleContinuousSweep']['get'])
        self.saClose()
        return ampl
    @singleContinuousSweep.setter
    def singleContinuousSweep(self, value='') :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['singleContinuousSweep']['set'] + str(value))
        self.saClose()
        return d

    @property
    def peakSearch(self) :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        ampll = -12.21
        self.saOpen()
        ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker), waitforit=True)
        self.saClose()
        self.saOpen()
        freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker), waitforit=True)
        try :
            ampll = max(float(ampl), -99.9)
            ampl = str(ampll)
        except ValueError as ve :
            print("ValueError peakSearch ampl={} ampll={} -- mesg -- {}\n".format(repr(ampl), ampll, repr(ve)))
            ampl = "-99.88"
            freq = '2.45E9'
        self.saClose()
        return ampl, freq
    @peakSearch.setter
    def peakSearch(self, value='') :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        d = self.senddata(sacd['peakSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def minSearch(self) :
        ''' minSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['minSearch']['get']['ampl'].format(self.marker))
        freq = self.sendgetdata(sacd['minSearch']['get']['freq'].format(self.marker))
        self.saClose()
        return ampl, freq
    @minSearch.setter
    def minSearch(self, value='') :
        ''' minSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        d = self.senddata(sacd['minSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def continuousPeakSearch(self) :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['continuousPeakSearch']['get'].format(self.marker))
        self.saClose()
        return d
    @continuousPeakSearch.setter
    def continuousPeakSearch(self, value='') :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        d = self.senddata(sacd['continuousPeakSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def clearHold(self) :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['clearHold']['get'])
        self.saClose()
        return rtn
    @clearHold.setter
    def clearHold(self, value='') :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        d = self.senddata(sacd['clearHold']['set'])
        self.saClose()
        return d

    @property
    def avgHold(self) :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['avgHold']['get'])
        self.saClose()
        return rtn
    @avgHold.setter
    def avgHold(self, value='') :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        d = self.senddata(sacd['avgHold']['set'])
        self.saClose()
        return d

    @property
    def maxHold(self) :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['maxHold']['get'])
        self.saClose()
        return rtn
    @maxHold.setter
    def maxHold(self, value='') :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        d = self.senddata(sacd['maxHold']['set'])
        self.saClose()
        return d

    def getSAValue(self, name) :
        ''' getSAValue -- Get the value from the SA for the named SA function.
        '''
        rtnval = None
        self.saOpen()
        #print(sacd[name]['get'])
 
        if name == 'peakSearch' :
            ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker))
            freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker))
            rtnval = (ampl, freq)
        else :
            rtnval = self.sendgetdata(sacd[name]['get'].format(self.marker))
        self.saClose()
        return rtnval

    def setSAValue(self, name, value="") :
        ''' getSAValue -- Set the value of the SA for the named SA function.
        '''
        rtnval = None
        if self.saOpen() :
            argc = sacd[name]['argc']
            if argc > 1 :
                nval = ""
                for v in value :
                    if len(nval) > 0 :
                        nval += ","
                    nval += str(v)
                value = nval
            #print(sacd[name]['set']+str(value)+ " "+str(argc))
            if argc == 0 : 
                rtnval = self.senddata(sacd[name]['set'].format(self.marker))
            else :
                rtnval = self.senddata(sacd[name]['set'].format(self.marker) + str(value))
        self.saClose()
        return rtnval

if __name__ == '__main__' :

    print("Module only!!")
