""" SACommunicationBase.py: A base class of sorts to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the SACommStrings dictionary.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import socket, sys
from time import sleep, time
from SA.SACommStrings import SA_CMD_DICT as sacd

DEFAULTTIMEOUT = 0.20
DEFAULTLOOPTIMEOUT = 3.0

class SACom(object) :
    ''' SACom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='A-N9020A-11404.ossiainc.local', port=5025, debug=False, marker=1, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        self.port  = port if port == 80 else 5025
        self.buflength = 2560
        self.debug = debug
        self.timethis = timethis
        self.usesa = True
        self.mySock = None
        self.addr = 'None Specified'
        self.errorstr = None
        self.marker = marker
        if addr is None :
            self.addr = '127.0.0.1'
            self.usesa = False
        else :
            try :
                self.addr = socket.gethostbyname(addr)
            except AttributeError as ae :
                self.errorstr = repr(ae) 
                self.usesa = False
            except Exception as ee :
                self.errorstr = repr(ee) 
                self.usesa = False
            else :
                self.marker = marker
                self.lastData = None
                self.mySockTimeout = DEFAULTTIMEOUT
                self.mySockLoopTimeoutCount = int(DEFAULTLOOPTIMEOUT / self.mySockTimeout)

        if self.debug :
            print("addr={} port={}\n".format(self.addr, self.port))

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.saClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    def saOpen(self) :
        ''' saOpen -- Open the port connection to the specified Agilent Signal Analizer
        '''
        if self.mySock is None and self.usesa :
            self.ontime = time()
            self.mySock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                err = self.mySock.connect_ex((self.addr, self.port))
            except socket.error as se :
                print("Socket Error on connect_ex -- {}\n".format(repr(se)))
                self.mySock.close()
                self.mySock = None
                self.usesa = False
            if self.debug :
                print("\n\nerr={}, sock={}\n\n".format(err, repr(self.mySock)))
            if err != 0 :
                print("There was an error. Disabling use of SA (err {})".format(repr(err)))
                self.usesa = False
                return False
            self.mySock.settimeout(self.mySockTimeout)
        return self.usesa

    def saClose(self, doit=False) :
        ''' saClose -- Close the port connection to the specified Agilent Signal Analizer
        '''
        if doit :
            if self.mySock is not None and self.usesa:
                self.mySock.close()
            self.mySock = None

    def sendgetimage(self, sendthis):
        ''' sendgetimage -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        data = None
        if self.debug :
            print("SA Stringdata='{}' usesa={}".format(sendthis, repr(self.usesa)))
        if self.mySock is not None and self.usesa :
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)
            data = bytearray()
            d = bytearray()
            try :
                d = self.mySock.recv(256)
                while len(d) > 0 :
                    data += d
                    d = self.mySock.recv(256)
            except socket.timeout :
                if len(data) == 0 :
                    print("Socket Timeout without any data received")
                pass
    
            if self.debug :
                print("rtndat='" + repr(data) + "'")
        return data

    def getSaveImage(self, filename) :

        url = bytearray(b'GET /Agilent.SA.WebInstrument/Screen.png HTTP/1.1\r\nHost: 10.10.0.83:7081\r\nUser-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\n\r\n')
        sa = SACom(port=80)
        sa.saOpen()
        saResponse = sa.sendgetimage(url)
        sa.saClose()

        startOfImageData = bytearray(b'\r\n\r\n') 
        imageIndex = saResponse.find(startOfImageData) + 4
        with open(filename, "wb") as fd :
            fd.write(saResponse[imageIndex:])
            fd.close

    def sendgetdata(self, sendthis, waitforit=False):
        ''' sendgetdata -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        rtndat = None
        if self.debug :
            print("\n1DEBUG:$$$$$$$$$$$$$$$SA Stringdata='{}' usesa={}".format(sendthis, repr(self.usesa)))
        if self.mySock is not None and self.usesa :
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)

            sendCR = "\r\n"
            if sys.version_info[0] == 3 :
                sendCR = bytes(sendCR,encoding="utf-8")
            self.mySock.send(sendCR )
            sleep(0.07)

            cnt = self.mySockLoopTimeoutCount
            datautf8 = bytearray()
            while cnt > 0 :
                cnt -= 1
                try :
                    d = self.mySock.recv(self.buflength)
                    datautf8 += d
                    while len(d) == self.buflength :
                        d = self.mySock.recv(self.buflength)
                        datautf8 += d
                    if not waitforit and len(datautf8) > 0 :
                        break
                except socket.timeout :
                    if len(datautf8.decode('utf-8').strip()) == 0 :
                        if self.debug :
                            print("2DEBUG:Socket Timeout without any data received cnt={}".format(cnt))
                    elif self.debug :
                        print("3DEBUG:sendthis='{}' cnt= {} data='{}' len(data)={}".format(sendthis, cnt, datautf8.decode('utf-8'), len(datautf8.decode('utf-8').strip())))
                    if not waitforit or len(datautf8.decode('utf-8').strip()) > 0 :
                        break

            data = datautf8.decode('utf-8').lstrip()
            if self.debug :
                print("\n4DEBUG:DATA='{}'\n\n".format(data.replace("\r", ";").replace("\n", ";")))
            if cnt == 0 :
                print("\nALERT: SA loop count {} decremented to zero.\n".format(self.mySockLoopTimeoutCount))
            self.lastData = data
            l = data.split('SCPI>')
            #if len(l) < 2 :
                #ll = data.split('34972A')
                #if len(ll) > 1 :
                    #l = ll
            rtndat = l[0].rstrip()
            #m = rtndat.split()
            #if len(m) > 1 :
                #rtndat = m[1]
    
            if self.debug :
                m = l
                print("5DEBUG:l={} m={}".format(repr(l), repr(m)))
                print("6DEBUG:#### rtndat='{}'".format(repr(rtndat)))
        return rtndat

    def senddata(self, sendthis, waitforit=False):
        ''' senddata -- Send the command string to the and don't wait for a response.
        Strip off any un-necessary white space and prompt data.
        '''
        rtndat = None
        if self.debug :
            print("SA Stringdata='{}' usesa={}".format(sendthis, repr(self.usesa)))
        if self.mySock is not None and self.usesa :
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)

            sendCR = "\r\n"
            if sys.version_info[0] == 3 :
                sendCR = bytes(sendCR,encoding="utf-8")
            self.mySock.send(sendCR )

        return rtndat

    @property
    def getLog(self) :
        ''' getLog -- retrieve the last arg number of log entires
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['getLog']['get'])
        self.saClose()
        return d
    @getLog.setter
    def getLog(self, value) :
        self.saOpen()
        d = self.sendgetdata(sacd['getLog']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resetsa(self) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resetsa']['get'].format(self.marker))
        self.saClose()
        return d
    @resetsa.setter
    def resetsa(self, marker) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resetsa']['set'].format(marker))
        self.saClose()
        self.marker = marker
        return d

    @property
    def selectMarker(self) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['selectMarker']['get'].format(self.marker))
        self.saClose()
        return d
    @selectMarker.setter
    def selectMarker(self, marker) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.senddata(sacd['selectMarker']['set'].format(marker))
        self.saClose()
        self.marker = marker
        return d

    @property
    def centerFrequency(self) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['centerFrequency']['get'])
        self.saClose()
        return d
    @centerFrequency.setter
    def centerFrequency(self, value) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        rtnval =  self.senddata(sacd['centerFrequency']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def frequencySpan(self) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['frequencySpan']['get'])
        self.saClose()
        return d
    @frequencySpan.setter
    def frequencySpan(self, value) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        rtnval = self.senddata(sacd['frequencySpan']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def videoBW(self) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBW']['get'])
        self.saClose()
        return d
    @videoBW.setter
    def videoBW(self, value) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['videoBW']['set'] + str(value))
        self.saClose()
        return d

    @property
    def videoBWAuto(self) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBWAuto']['get'])
        self.saClose()
        return d
    @videoBWAuto.setter
    def videoBWAuto(self, value) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['videoBWAuto']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resolutionBWAuto(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBWAuto']['get'])
        self.saClose()
        return d
    @resolutionBWAuto.setter
    def resolutionBWAuto(self, value) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resolutionBWAuto']['set'] + str(value))
        self.saClose()
        return d

    @property
    def averageCount(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['averageCount']['get'])
        self.saClose()
        return d
    @averageCount.setter
    def averageCount(self, value) :
        ''' averageCount -- get/set pair to set/get the measure average/holt count number
        '''
        self.saOpen()
        d = self.senddata(sacd['averageCount']['set'] + str(value))
        self.saClose()
        return d

    @property
    def resolutionBW(self) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBW']['get'])
        self.saClose()
        return d
    @resolutionBW.setter
    def resolutionBW(self, value) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['resolutionBW']['set'] + str(value))
        self.saClose()
        return d

    @property
    def referenceLevel(self) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevel']['get'])
        self.saClose()
        return d
    @referenceLevel.setter
    def referenceLevel(self, value) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['referenceLevel']['set'] + str(value))
        self.saClose()
        return d

    @property
    def referenceLevelOffset(self) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevelOffset']['get'])
        self.saClose()
        return d
    @referenceLevelOffset.setter
    def referenceLevelOffset(self, value) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['referenceLevelOffset']['set'] + str(value))
        self.saClose()
        return d

    @property
    def displayUnits(self) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['displayUnits']['get'])
        self.saClose()
        return d
    @displayUnits.setter
    def displayUnits(self, value) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        d = self.senddata(sacd['displayUnits']['set'] + str(value))
        self.saClose()
        return d

    @property
    def triggerHold(self) :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerHold']['get'])
        self.saClose()
        return ampl
    @triggerHold.setter
    def triggerHold(self, value='') :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        d = self.senddata(sacd['triggerHold']['set'])
        self.saClose()
        return d

    @property
    def triggerClear(self) :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerClear']['get'])
        self.saClose()
        return ampl
    @triggerClear.setter
    def triggerClear(self, value='') :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        d = self.senddata(sacd['triggerClear']['set'])
        self.saClose()
        return d

    @property
    def sweepTime(self) :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepTime']['get'])
        self.saClose()
        return ampl
    @sweepTime.setter
    def sweepTime(self, value='') :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['sweepTime']['set'] + str(value))
        self.saClose()
        return d

    @property
    def sweepPoints(self) :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepPoints']['get'])
        self.saClose()
        return ampl
    @sweepPoints.setter
    def sweepPoints(self, value='') :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['sweepPoints']['set'] + str(value))
        self.saClose()
        return d

    @property
    def traceData(self) :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        self.saOpen()
        trace = " trace{}".format(self.marker)
        print(sacd['traceData']['get'] + trace)
        rtnval = self.sendgetdata(sacd['traceData']['get'] + trace)
        self.saClose()
        return rtnval
    @traceData.setter
    def traceData(self, value='') :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        rtnval = ""
        if sacd['traceData']['argc'] > 0 :
            self.saOpen()
            print(sacd['traceData']['set'] + str(value))
            rtnval = self.senddata(sacd['traceData']['set'] + str(value))
        self.saClose()
        return rtnval

    @property
    def singleContinuousSweep(self) :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['singleContinuousSweep']['get'])
        self.saClose()
        return ampl
    @singleContinuousSweep.setter
    def singleContinuousSweep(self, value='') :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        d = self.senddata(sacd['singleContinuousSweep']['set'] + str(value))
        self.saClose()
        return d

    @property
    def peakSearch(self) :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        ampll = -12.21
        self.saOpen()
        ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker), waitforit=True)
        self.saClose()
        self.saOpen()
        freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker), waitforit=True)
        try :
            ampll = max(float(ampl), -99.9)
            ampl = str(ampll)
        except ValueError as ve :
            print("ValueError peakSearch ampl={} ampll={} -- mesg -- {}\n".format(repr(ampl), ampll, repr(ve)))
            ampl = "-99.88"
            freq = '2.45E9'
        self.saClose()
        return ampl, freq
    @peakSearch.setter
    def peakSearch(self, value='') :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        d = self.senddata(sacd['peakSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def minSearch(self) :
        ''' minSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['minSearch']['get']['ampl'].format(self.marker))
        freq = self.sendgetdata(sacd['minSearch']['get']['freq'].format(self.marker))
        self.saClose()
        return ampl, freq
    @minSearch.setter
    def minSearch(self, value='') :
        ''' minSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        d = self.senddata(sacd['minSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def continuousPeakSearch(self) :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['continuousPeakSearch']['get'].format(self.marker))
        self.saClose()
        return d
    @continuousPeakSearch.setter
    def continuousPeakSearch(self, value='') :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        d = self.senddata(sacd['continuousPeakSearch']['set'].format(self.marker))
        self.saClose()
        return d

    @property
    def clearHold(self) :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['clearHold']['get'])
        self.saClose()
        return rtn
    @clearHold.setter
    def clearHold(self, value='') :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        d = self.senddata(sacd['clearHold']['set'])
        self.saClose()
        return d

    @property
    def avgHold(self) :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['avgHold']['get'])
        self.saClose()
        return rtn
    @avgHold.setter
    def avgHold(self, value='') :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        d = self.senddata(sacd['avgHold']['set'])
        self.saClose()
        return d

    @property
    def maxHold(self) :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['maxHold']['get'])
        self.saClose()
        return rtn
    @maxHold.setter
    def maxHold(self, value='') :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        d = self.senddata(sacd['maxHold']['set'])
        self.saClose()
        return d

    def getSAValue(self, name) :
        ''' getSAValue -- Get the value from the SA for the named SA function.
        '''
        rtnval = None
        self.saOpen()
        #print(sacd[name]['get'])
 
        if name == 'peakSearch' :
            ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker))
            freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker))
            rtnval = (ampl, freq)
        else :
            rtnval = self.sendgetdata(sacd[name]['get'].format(self.marker))
        self.saClose()
        return rtnval

    def setSAValue(self, name, value="") :
        ''' getSAValue -- Set the value of the SA for the named SA function.
        '''
        rtnval = None
        if self.saOpen() :
            argc = sacd[name]['argc']
            if argc > 1 :
                nval = ""
                for v in value :
                    if len(nval) > 0 :
                        nval += ","
                    nval += str(v)
                value = nval
            #print(sacd[name]['set']+str(value)+ " "+str(argc))
            if argc == 0 : 
                rtnval = self.senddata(sacd[name]['set'].format(self.marker))
            else :
                rtnval = self.senddata(sacd[name]['set'].format(self.marker) + str(value))
        self.saClose()
        return rtnval

if __name__ == '__main__' :

    from LIB.logResults import LogResults

    sa = SACom(addr='A-N9020A-11404.ossiainc.local', port=5025, debug=True)
    logger = LogResults("satest", "./", console=True, debug=True)
    logger.initDB("satestDB", hostnm="ossia-build")
    sa.saOpen()
    # specific tests for time sweep mode and data gathering.
    tmp = sa.resetsa
    sa.resolutionBWAuto = "OFF"
    sa.videoBWAuto = "OFF"
    sa.frequencySpan = '0HZ'
    sa.singleContinuousSweep = "ON"
    sa.referenceLevelOffset = "26.4 dB"
    
    secperpoint = 0.03
    st = [30, 20, 10, 5]
    hdr = [{0 : 156, 1 : 554, 2 : 909, 5 :3434},
           {0 : 256, 1 : 654, 2 : 919, 5 :3435},
           {0 : 356, 1 : 754, 2 : 929, 5 :3436},
           {0 : 456, 1 : 854, 2 : 939, 5 :3437}]

    for i in range(len(hdr)) :
        sa.sweepTime = "{}s".format(st[i])
        sa.sweepPoints = int(st[i] / secperpoint) + 1
        sleep(st[i]-0.5)
        data = sa.traceData
        logger.saveTraceData(i, data, hdr[i], "satestdata", True)

    """
    print(sa.resetsa)
    sa.singleContinuousSweep = "ON"
    sa.centerFrequency = '2.45GHZ'
    print("centerFrequency=" + sa.centerFrequency)

    sa.frequencySpan = '2.0MHZ'
    print("frequencySpan=" + sa.frequencySpan)

    sa.sweepPoints = 500
    print("sweep Points {}".format(sa.sweepPoints))

    print("referenceLevel=" + sa.referenceLevel)

    print("resolutionBWAuto=" + sa.resolutionBWAuto)
    sa.resolutionBWAuto = "OFF"
    print("resolutionBWAuto=" + sa.resolutionBWAuto)

    print("selectMarker=" + sa.selectMarker)
    sa.selectMarker = 2
    print("selectMarker=" + sa.selectMarker)

    sa.peakSearch = ''
    print("peakSearch=" + repr(sa.peakSearch))
    print(sa.resetsa)
    print("centerFrequency=" + sa.centerFrequency)
    sa.singleContinuousSweep = "ON"
    """
    sa.saClose()
    
