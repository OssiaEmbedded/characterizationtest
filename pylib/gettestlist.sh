#!/bin/sh

edtst=`ls -1t /tmp/*[0-9] | awk 'NR == 1 {print \$0}' ` 
#echo $edtst
cktsti=`cat $edtst | grep 'list iterations' | tail --lines=1` 
testlistname=`grep "testlistname" $edtst `
testlistfile=`grep "testlistfile" $edtst `
tstname=`echo $testlistname | awk 'BEGIN{RS=" "} $0 ~ /--testlistname/ {split($0,tt,"="); print tt[2]}'`
#echo $tstname
tstfile=`echo $testlistfile | awk 'BEGIN{RS=" "} $0 ~ /--testlistfile/ {split($0,tt,"="); print tt[2]}'`
#echo $tstfile
outdata='["'$tstfile'","'$tstname'","'$cktsti'"]'
echo $outdata

