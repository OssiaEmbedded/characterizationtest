''' LogTempUtil.py -- Setup test equipment, Transmitter, and log temperture data as
defined by commandline options.

Version : 0.0.0
Date : April 3 2018
Copyright Ossia Inc. 2018

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import sys
#if "/home/ursusm/pylib" not in sys.path :
#    sys.path.insert(0, "/home/ursusm/pylib")
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname
import json, datetime

import SA.SACommunicationBase as sacom
#import TXJSON.JSonCmdStrings as jcs
#from LIB.TXComm import TXComm, gPWR_LEVEL_MAP
from LIB.logResults import LogResults
import LIB.parseCommaColon as pcc
#from LIB.sendTestServer import SendTestServer

#DEFAULTDBSERVER = 'ossiadevuwm'
#DEFAULTDBSERVER = 'ossia-build'

#TXTBLNAME = 'TxAmuTemp'
#AMUDBNAME = 'CheckDB'
#AMUCOLNAME = 'AmuTempTable'
TIMESTAMPCOLNAME="DataTimeStamp"


TE_CMS  = [[TIMESTAMPCOLNAME, '%s', 'VARCHAR(35)'],
           ['ConfigNotes',    '%s', 'VARCHAR(512)'],
           ['LogFileName',    '%s', 'VARCHAR(150)'],
           ['DAcqName',       '%s', 'VARCHAR(100)'],
           ['DAcqSN',         '%s', 'VARCHAR(20)'],
           ['DAcqHostName',   '%s', 'VARCHAR(100)'],
           ['DAcqIP',         '%s', 'VARCHAR(16)'],
           ]

def getDuration(START_TM) :
    duration = int(time() - START_TM)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005
    return hrs, mins, secs


class LogTempTest(object):

    def __init__(self, options) :


        self.options = options
        
        self.chanlist   = pcc.parseCommaColon(self.options.chanlist)

        self.tehostname = self.options.teipaddr
        self.teip       = gethostbyname(self.tehostname)
        self.teport     = self.options.teport
        self.debug      = self.options.debug

        self.te = None
        if self.options.teipaddr != "" :
            self.te  = sacom.SACom(addr=self.options.teipaddr, port=int(self.options.teport), debug=self.options.debug)
            self.te.mySockTimeout = 0.45

        logFile = "{}_{}".format(self.options.logfile, self.tehostname)+"_{}.csv"

        self.logger = LogResults(logFile, self.options.directory, console=self.options.console, debug=self.options.debug)

    def setup(self) :

        self.logger.initFile(TE_CMS)
        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('ConfigNotes', self.options.logcomments)
        self.logger.logAddValue('LogFileName', self.logger.filename)

        if self.te is not None and self.te.saOpen() :
            self.logger.logAddValue('DAcqHostName', self.tehostname)
            self.logger.logAddValue('DAcqIP',       self.teip)
            self.te.sendgetdata("*IDN?")
            vallst = self.te.lastData.split(',')
            dacqname = vallst[0]+'_'+vallst[1]+'_'+vallst[3]
            dacqsn   = vallst[2]
            self.logger.logAddValue('DAcqName', dacqname)
            self.logger.logAddValue('DAcqSN',   dacqsn)
            tm = datetime.datetime.now()
            self.te.sendgetdata("system:time {},{},{:5.3f}".format(tm.hour, tm.minute, float(tm.second)))
            self.te.sendgetdata("system:date {},{:02d},{:02d}".format(tm.year, tm.month, tm.day))

            chstr = ""
            for a in self.chanlist :
                if len(chstr) > 0 :
                    chstr += ","
                chstr += str(a)

            self.te.sendgetdata("configure:temp tc,j,1,0.1,(@{})".format(chstr))

            self.te.sendgetdata("format:reading:time:type abs")
            self.te.sendgetdata("format:reading:time on")

            self.te.sendgetdata("format:reading:channel on")

            self.te.saClose()

        self.logger.logResults()

        # make amu table column string (CMS) list
        amu_cms = list()
        amu_cmsts = list()
        if self.te is not None :
            for ch in self.chanlist :
                amu_cms.append(['ChNum{}'.format(ch),'%6.3g','VARCHAR(20)'])
                if self.options.logdacqts :
                    amu_cmsts.append(['ChNum{}TS'.format(ch), '%s','VARCHAR(30)'])

        # now add on the Dacq channel time stamps
        amu_cms.extend(amu_cmsts)

        self.logger.initFile(amu_cms, how='a')

    def getDacqData(self) :
        rtndict = dict()
        if self.te is not None and self.te.saOpen() :
            rtn  = self.te.sendgetdata("read?", True)
            rlst = rtn.split(",")
            # this list is completely dependent on how the unit was setup.
            for i in range(0, len(rlst), 8) :
                # this set of 8 values are
                #  -- temp, yr, mon, day, hrs, mins, secs, chan
                temp  = float(rlst[i+0])
                tsstr = str(rlst[i+4])+"_"+str(rlst[i+5])+"_"+str(rlst[i+6])
                chan  = int(rlst[i+7])
                rtndict.update({chan : [temp, tsstr]}) 
            self.te.saClose()
        return rtndict


    def mainTest(self) :

        setdelay = float(self.options.setdelay)
        # iterate through setcount 
        cnt = int(self.options.setcount)
        while cnt > 0 or cnt < 0 :
            if cnt > 0 :
                cnt -= 1

            # first get data from the Data Acquisition unit

            rtnval = self.getDacqData()
            # iterate through the channel data from the Dacq unit
            if rtnval is not None and len(rtnval) > 0 :
                for ch in self.chanlist :
                    tempName = 'ChNum{}'.format(ch)
                    temp     = rtnval[ch][0]
                    self.logger.logAddValue(tempName, temp)
                    if self.options.logdacqts :
                        tsName   = 'ChNum{}TS'.format(ch)
                        ts       = rtnval[ch][1]
                        self.logger.logAddValue(tsName, ts)

            self.logger.logResults()
            sleep(setdelay)
                    

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("-p","--teport",    dest='teport',     type=int,   action='store',      default=5024,       help="Thermo couple telnet port number (default='%default')")
    parser.add_option("-i","--teip",      dest='teipaddr',   type=str,   action='store',      default="a-34972a-26082",help="Thermo couple network name or address (default='%default')")
    parser.add_option("","--logcomments", dest='logcomments',type=str,   action='store',      default="",         help="Comments to describe important aspects of this test occurances (default='%default')")
 
    parser.add_option("","--setcount",    dest='setcount',   type=int,   action='store',      default=10,         help="Number of temperature sets to collect \
                                                                                                                        0         = setup but don't take data.\
                                                                                                                        Greater   = gather count data.\
                                                                                                                        Less than = gather data forever, Cntl-C to stop (default='%default')")
    parser.add_option("","--setdelay",    dest='setdelay',   type=float, action='store',      default=1.0,        help="Time to wait between temperature set collections (seconds) (default='%default')")

    parser.add_option("","--chanlist",    dest='chanlist',   type=str,   action='store',      default="101:120",  help="List of DAcq Channels to acquire. (default='%default')")

    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store',      default="LogTemp",  help="Log file name. If {} is added in the file name, a timestamp will be added. (default='%default')")
    parser.add_option("","--console",     dest='console',                action='store_true', default=False,      help="Turn on printing to console (default='%default')")
    parser.add_option("","--logdacqts",   dest='logdacqts',              action='store_true', default=False,      help="Log the Dacq data timestamps (default='%default')")
    parser.add_option("-d","--debug",     dest='debug',                  action="store_true", default=False,      help="print debug info. (default='%default')")

    parser.add_option("","--directory",   dest='directory',  type=str,   action='store',      default="./",help="Log file directory. (default='%default')")

    (options, args) = parser.parse_args()

    tA = None
    if options.teipaddr is not "" :
        tA = LogTempTest(options)
        tA.setup()
        try :
            tA.mainTest()
        except KeyboardInterrupt :
            pass

        finally :
            hrs, mins, secs = getDuration(START_TIME)
            Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
            tA.logger.logPutHeadingStr("Duration", Duration)
            print("Test duration = {}\n".format(Duration))
    else :
        print("Please provide the IP address or FQDN of the target Charger (-I).\n")
