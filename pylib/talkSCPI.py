import sys

HOMEROOT="/home/ursusm/pylib/"
if HOMEROOT not in sys.path :
    sys.path.insert(0, HOMEROOT)
from time import sleep, time, localtime
from optparse import OptionParser, OptionGroup
from datetime import datetime

import SA.SACommunicationBase as sacom

SMAPORTLOSS = 19.8 # minus dB
PEAKSEARCHTIME = 2 # seconds

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]


def setupSA(sa, cableloss) :
    
    for cmd_data in SA_SETUP_LIST :
        sa.setSAValue(cmd_data[0], cmd_data[1])
    refLvl = SMAPORTLOSS + cableloss
    sa.referenceLevelOffset = refLvl

    return

def getSAPower(sa, average) :
    """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                      sapower is a float
                      safreq is a float
    """
    sapower = -60.0
    safreq = -2.0
    if sa.usesa :
        if average :
            sa.avgHold = ""
        else :
            sa.maxHold = ""
        # wait for data to get stable
        sleep(PEAKSEARCHTIME)
        # find the freq of the peak
        sa.peakSearch = ""

        # get the SA data
        sa.triggerHold = ''
        SApower, SAfreq = sa.peakSearch

        sa.triggerClear = ''
        sa.clearHold = ''
        try :
            sapower = float(SApower)
        except ValueError as ve :
            print("SApower ValueError on '{}' -- {}".format(SApower, ve))
            sapower = -1.0
        try :
            safreq = float(SAfreq)
        except ValueError as ve :
            print("SAfreq ValueError on '{}' -- {}".format(SAfreq, ve))
            safreq = -1.0
    return sapower, safreq

if __name__ == "__main__" :
    
    parser = OptionParser()
    
    parser.add_option("-i","--saip",
                       dest="saipaddr",
                       action="store",
                       default="A-N9020A-11404.ossiainc.local",
                       help="Network name or IP address of Spectrum analyzer (default='%default')")
    
    parser.add_option("-p","--port",
                       dest="saport",
                       type=int,
                       action="store",
                       default="5025",
                       help="Port number of the Spectrum analyzer telnet server (default='%default')")

    parser.add_option("-c","--cableloss",
                       dest="cableloss",
                       type=float,
                       action="store",
                       default=7.66,
                       help="Cable loss of the cable path from the SA to the Receiver (default='%default')")

    parser.add_option("-a","--average",
                       dest='average',
                       action="store_true",
                       default=False,
                       help="Get Average data rather than peak.")
    
    parser.add_option("-d","--debug",
                       dest='debug',
                       action="store_true",
                       default=False,
                       help="print debug info.")
    
    parser.add_option("-s","--setup",
                       dest='setup',
                       action="store_true",
                       default=False,
                       help="Setup SA or not.")

    parser.add_option("-t","--talk",
                       dest='talk',
                       action="store_true",
                       default=False,
                       help="Talk to host@port.")
    
    parser.add_option("-e","--testtalk",
                       dest='testtalk',
                       action="store_true",
                       default=False,
                       help="Justread host@port.")
    
    (options, args) = parser.parse_args()
    sa = sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
    sa.buflength = 11128
    #sa.mySockTimeout = sa.mySockTimeout * 14.0
    if options.talk :
        strval = ""
        for a in args :
            strval += a.replace("\\", "") + " "
        print("strval = '{}'".format(strval))
        if sa.saOpen() :
            rtn = sa.sendgetdata(strval, True)
            val = rtn.split(",")
            print "RTN={}\n".format(rtn)
            if len(val) == 1 :
                try :
                    val = float(val[0])
                except Exception :
                    pass
                else :
                    print("fval={:8.5f}".format(val))
            sa.saClose()
    elif options.testtalk :
        if sa.saOpen() :
            rtn = sa.sendgetdata("*rst", True)
            print("rtn={}".format(rtn))
            rtn = sa.sendgetdata("*idn?", True)
            print("rtn={}".format(rtn))
            rtn = sa.sendgetdata("inst:dmm?", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("system:time?", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("configure:temp tc,j,1,0.1,(@101:120)", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("format:reading:time:type abs", True)
            print("rtn={}".format(rtn))
            rtn = sa.sendgetdata("format:reading:time on", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("format:reading:channel on", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("read?", True)
            print("rtn={}".format(rtn))
            rlst = rtn.split(",")
            rtnlst = list()
            rn=['temp','year','mon','day','hr','min','sec','chan']
            for i in range(0, len(rlst), 8) :
                r = dict()
                r.update({'temp' : rlst[i]})
                tsstr = str(rlst[i+1])
                for j in range(i+2, i+7, 1) :
                    if (j%8) > 4 :
                        tsstr += ":"+str(rlst[j])
                    else :
                        tsstr += "_"+str(rlst[j])
                r.update({'timestamp' : tsstr})
                r.update({'channel' : rlst[i+7]})
                rtnlst.append(r) 
                print("{} || temp = {:6.3f}".format(r, float(r['temp'])))
            #print("rtn={}".format(repr(rtnlst)))


            rtn = sa.sendgetdata("system:time?", True)
            print("rtn={}".format(rtn))

            rtn = sa.sendgetdata("system:time:scan?", True)
            print("rtn={}".format(rtn))
            sa.saClose()
    else :
        if options.setup :
            setupSA(sa, options.cableloss)
        
        #print("\n\nGETTINGDATA\n\n")
        
        pwr, freq = getSAPower(sa, options.average)
        print("Pwr={:5.2f}, Freq={:5.3g}".format(float(pwr), float(freq)))  
