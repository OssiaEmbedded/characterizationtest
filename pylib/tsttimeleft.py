#!/usr/bin/python

from subprocess import check_output
import json
import imp
import sys

def getTestLogData() :
    val=check_output("sh /home/ossiadev/pylib/gettestlist.sh", shell=True).decode('utf-8')
    vv = json.loads(val)
    testlistname = vv[1]
    testlistfile = vv[0]
    tt = vv[2].split(" ")[-1]
    linesleft = int(tt)
    print repr(vv)
    return (linesleft, testlistname, testlistfile)

def processTLD((linesleft, testlistname, testlistfile)) :
    try :
        tstlstmod = imp.load_source(testlistname, testlistfile)
        testlistE = eval("tstlstmod."+testlistname)
    except Exception as ee :
        eprint("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, fname, repr(ee)))
    else :
    
        curline = 0
        numlines = len(testlistE)
        if linesleft >= 0 :
            curline = numlines - linesleft
        pp = testlistE[curline:]

        wup = 0
        wfd = 0
        for i in pp :
            print i
            wup += int(i['warmup'])
            wfd += int(i['waitfordata']) * int(i['logcount'])
        tm = float(wup + wfd)
        hrs = tm/3600.0
        mins =  (hrs - int(hrs)) * 60.0
        secs =  (mins - int(mins)) * 60.0
    
        print "Aproximately {} seconds or {}:{}:{} left".format(tm, int(hrs), int(mins), int(secs))

if __name__ == "__main__" :
   if len(sys.argv) < 3 :
       processTLD(getTestLogData())
   else :
       xx = (-1, sys.argv[1], sys.argv[2])
       processTLD(xx)


