#! /usr/bin/python
from __future__ import absolute_import, division, print_function, unicode_literals

import socket
from time import sleep
from optparse import OptionParser
import sys
import json
def parseCotaConfigValuesFromList(pyList) :
    CotaConfNames = ["Calibration AMB",     "Calibration ACL",    "Reference AMB",            "Reference ACL",
                     "Client Query Period", "Client COM Channel", "Critical Temperature (C)", "Power Level",
                     "TPS Number of BB",    "TPS TX Duration"]
    fulldict = dict() 
    partdict = dict() 
    rtnstr = ""
    for nDict in pyList :
        name  = nDict['Name']
        value = nDict['Value']
        fulldict.update({name : value})
        if name in CotaConfNames :
            nspname = name.replace(" ","_")
            rtnstr += "{}={}\n".format(nspname, value)
            partdict.update({name : value})
    return rtnstr, partdict, fulldict 

class SendTestServer(object) :
    def __init__(self, ipaddr, port, debug) :
        self.ipaddr = socket.gethostbyname(ipaddr)
        self.port = int(port)
        self.debug = debug

    def sendgetdata(self, sendthis):
        tryAgain = 20
        mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        while tryAgain > 0 :
            try :
                mySocket.connect((self.ipaddr, self.port))
                mySocket.settimeout(1.0)
                tryAgain = 0
            except socket.error as ee :
                tryAgain -= 1
                if tryAgain == 0 :
                    mySocket.close()
                    return "There was a connect exception {}".format(repr(ee))
                sleep(2)
    
        cnt = 60
        cmd = sendthis + "\r\n"
        data = ""
        if sys.version_info[0] == 3 :
            cmd = bytes(sendthis + "\r\n", encoding='utf-8')
            data = bytes("", encoding='utf-8')
        mySocket.sendall( cmd )
        sleep(0.5)
        datack = ""
        while cnt > 0 :
            try :
                d = mySocket.recv(8192)
                data += d
            except socket.timeout :
                cnt -= 1
            datack = data
            if sys.version_info[0] == 3 :
                datack = data.decode('utf-8')
            if 'SUCCESS' in datack :
                break
        dataout =  datack.lstrip().rstrip()
        mySocket.close()
    
        return dataout
    
    def sendCommand(self, args) :
        sDat = "COMMANDVAL"
        for i in range(len(args)) :
            sDat += "/"+args[i].replace('\\','')
        data = self.sendgetdata(sDat)
        if self.debug :
            print("SendTestServer return data = '{}'\n".format(data))
        return data

    def parseCotaConfigValues(self, inVal) :
        # remove the SUCCESS key word

        if self.debug :
            print("SendTestServer parse inVal = '{}'\n".format(inVal))
        jsonVal = inVal.replace("SUCCESS","").rstrip()
        if self.debug :
            print("SendTestServer parse jsonVal = '{}'\n".format(jsonVal))
        pyList = json.loads(jsonVal)
        rtnstr, partdict, fulldict = parseCotaConfigValuesFromList(pyList)
        return rtnstr, partdict, fulldict 

    def parseCotaClientConfigValues(self, inVal) :
        # remove the SUCCESS key word

        if self.debug :
            print("SendTestServer parse inVal = '{}'\n".format(inVal))
        jsonVal = inVal.replace("SUCCESS","").rstrip()
        if self.debug :
                print("SendTestServer parse jsonVal = '{}'\n".format(jsonVal))
        pyList = json.loads(jsonVal)['Devices']
        fulldict = dict() 
        rtnstr = ""
        for nDict in pyList :
            theKeys = nDict.keys()
            theKeys.sort()
            for name in theKeys :
                value = nDict[name]
                fulldict.update({name : value})
                rtnstr += "{}={}\n".format(name, value)
        return rtnstr, fulldict 
     
if __name__ == "__main__" :
    parser = OptionParser()

    parser.add_option("-t","--tstequip",
                       dest="tstequip",
                       action="store",
                       default="ossiatest0.local",
                       help="Network name or IP address of ossiaServer (default='%default')")

    parser.add_option("-p","--port",
                       dest="port",
                       type=int,
                       action="store",
                       default="7000",
                       help="Port number of the ossiaServer telnet server (default='%default')")

    parser.add_option("-d","--debug",
                       dest="debug",
                       action="store_true",
                       default=False,
                       help="Print debug info.")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        rtnval = SendTestServer(options.tstequip, options.port, options.debug).sendCommand(args)
        print(rtnval)
    else :
        sts = SendTestServer(options.tstequip, options.port, options.debug)
        jVal = sts.sendCommand(['getConfigFile'])
        strval, partList, fullList = sts.parseCotaConfigValues(jVal)
        print("strlist -- {}".format(strval))
        print("partlist -- {}".format(partList))
        print("fulllist -- {}".format(fullList))

