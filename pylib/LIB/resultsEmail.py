 # Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

class EmailResults(object) :

    def __init__(self, emailaddress=None, fromaddress=None, debug=False) :
        self.theTo = ["ursusm@ossia.com",'asmitabens@ossia.com']
        if emailaddress is not None :
            if type(emailaddress) is list :
                self.theTo.extend(emailaddress)
            else :
                self.theTo.extend(emailaddress.split(","))
        self.theFrom = "labuser@ossia.com"
        if fromaddress is not None :
            self.theFrom = fromaddress
        self.resultsList = list()
        self.subjectStr = "Test Results"
        self.mailServer = "smtp.office365.com"
        self.port = 587
        self.debug = debug

    def setToAddress(self, strval) :
        if type(strval) is list :
            self.theTo.extend(strval)
        else :
            self.theTo.extend(strval.split(","))

    def setMailServer(self, strval) :
        self.mailServer = strval

    def setFromAddress(self, strval) :
        self.theFrom = strval

    def setSubject(self, strval) :
        self.subjectStr = strval

    def makeSubject(self, infos, errors, alerts, unknowns, debugs) :
        bad = errors + alerts + unknowns
        sbj = "Test Results: "
        if bad  > 0 : 
            sbj += "BAD"
        else :
            sbj += "GOOD"
        sbj += ": ERRORS {} ALERTS {} INFOS {} DEBUGS {}".format(errors, alerts, infos, debugs)
        if unknowns > 0 :
            sbj += " -- unknowns {}".format(unknowns)
        return sbj

    def setResults(self, strtoadd) :
        self.resultsList.append(strtoadd)

    def _makeListToCSV(self, thelist) :
        strval = ""
        for v in thelist :
            strval += v+", "
        strval.rstrip(",")
        return strval

    def emailResults(self, alist=None) :
        locallist = self.resultsList
        if alist is not None :
            locallist = alist

        debugs = 0
        alerts = 0
        errors = 0
        infos = 0
        unknowns = 0
        htmlstr = "<html><head></head><body><pre>"
        for value in locallist :
            try :
                value = str(value)
            except ValueError as ve :
                value = "ValueError in email results processing: value: '{}' -- error: '{}'".format(repr(value), repr(ve))

            if len(value.strip()) == 0 : 
                htmlstr += "<font color='brown'> </font>\n"
            elif value.lstrip().startswith("INFO:") :
                htmlstr += "<font color='black'> {} </font>\n".format(value.lstrip())
                infos += 1
            elif value.lstrip().startswith("ERROR:") :
                htmlstr += "<b><font color='red'> {} </font></b>\n".format(value.lstrip())
                errors += 1
            elif value.lstrip().startswith("ALERT:") :
                htmlstr += "<b><font color='magenta'> {} </font></b>\n".format(value.lstrip())
                alerts += 1
            elif value.lstrip().startswith("DEBUG:") :
                htmlstr += "<b><font color='green'> {} </font></b>\n".format(value.lstrip())
                debugs += 1
            else :
                htmlstr += "<font color='brown'> {} </font>\n".format(value.lstrip())
                unknowns += 1
        htmlstr += "</pre></body></html>\n"
        # Create a multipart/alternative message
        msg = MIMEMultipart('alternative')
        html = MIMEText(htmlstr, 'html')

        msg.attach(html)

        self.setSubject(self.makeSubject(infos, errors, alerts, unknowns, debugs))
        msg['Subject'] = self.subjectStr
        msg['From'] = self.theFrom
        msg['To'] = self._makeListToCSV(self.theTo)

        # Send the message via our own SMTP server, but don't include the
        # envelope header.

        s = smtplib.SMTP(self.mailServer, self.port)
        try :
            if self.debug :
                s.set_debuglevel(1)
            s.ehlo()
            s.starttls()
            s.ehlo()
            s.login("labuser@ossia.com", "lab&imagingpassword")
            s.sendmail(self.theFrom, self.theTo, msg.as_string())
        finally :
            s.quit()

if __name__ == "__main__" :

    import sys
    dbug = False
    if len(sys.argv) > 1  and sys.argv[1] == 'debug' :
        dbug = True
    e = EmailResults(debug=dbug)

    e.setSubject("Default mail list")

    e.setResults("ALERT: Amazing Stories from beyond the stars!!")
    e.setResults("They will inspire and invade your mind.")
    e.emailResults()

    del e

    #e = EmailResults("ursusm@ossia.com")

    #e.setSubject("Add one recipient")

    #e.setResults("INFO: Amazing Stories from beyond the galaxy!!")
    #e.setResults("INFO: They will inspire and invade your mind.")


    #e.emailResults()

    #del e

    #e = EmailResults()
#
    #e.setSubject("Add one recipient")
    #e.setToAddress("ursusm@ossia.com")

    #e.setMailServer("mail.ossia.com")

    #e.setFromAddress("2489158577@vtext.com")

    #e.setResults("ERROR: Amazing Stories from beyond the universe!!")
    #e.setResults("ALERT: They will inspire and invade your mind.")

    #e.emailResults()
