""" logResults.py: A class to log results to a CSV file and to the commandline specified data base server.
The functions included are specific to their application at Ossia Inc.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2019

"""

from __future__ import absolute_import, division, print_function, unicode_literals
from LIB.database import sqlConnect
from LIB.timestamp import createTimeStamp, getTimeNumberFromStr, getTimeStrFromNumber
# CMS Column string
CMS_NAME_FIELD = 0
CMS_FMT_FIELD = 1
CMS_DBFMT_FIELD = 2

class LogResults(object) :
    ''' LogResults Class -- A Ossia specific class to provide CSV log file methods as well
    as DB connections.
    '''

    def __init__(self, filename, directory="./", console=True, debug=False) :
        ''' __init__ -- initialize class variables and create the time stamp for this 
        class instance. The time stamp is important and can be used to time stamp the logged
        data as well.
        '''
        self.timeStamp = createTimeStamp()
        self.timeStampNum = getTimeNumberFromStr(self.timeStamp)
        self.filename = directory+filename.format(self.timeStamp)
        self.timeStampStr = getTimeStrFromNumber(self.timeStampNum)
        self.colformat = ""
        self.debug = debug
        self.logData = dict()
        self.console = console
        self.fmtHd = ""
        self.db = None
        self.db_table_name = 'tsst'

    def mkFormat(self, cms) :
        ''' mkFormat -- creates a 'format' string to align the columns and convert
        the data properly to string for writing to the log file, the console, and
        the database.
        It takes a list of lists. Each sublist is [ name, print format string, DB format string]
        returns a formated column name string and the data formating string.
        '''

        self.cms = cms
        if self.cms is None :
            print("ALERT: mkFormat -- The CMS list is None")
            raise ValueError
    
        l = len(self.cms)
        if self.debug :
            print("cms=" + repr(cms))
        longest = 0
        for i in range(l) :
            longest = max(longest, len(self.cms[i][CMS_NAME_FIELD]))
        if self.debug :
            print("longest={}".format(longest))

        colnm = ""
        colfmt = ""
        self.fmtHd   = "%%%ds" % longest
        for i in range(l) :
            colnm  += self.fmtHd % self.cms[i][CMS_NAME_FIELD]
            colfmt += self.fmtHd
            if i < (l - 1) :
                colnm += ","
                colfmt += ","
        if self.debug :
            print(colnm)
            print(colfmt)
        return colnm, colfmt

    def initDB(self, dbname, hostnm=None, db=None) :
        ''' initDB -- Called only if data is to be sent to the data base as well as console or file.
        An existing 'sqlConnect' object can be passed in or it will create one according to the 
        passed in hostnm.
        Once it has the sqlConnect object, here call "db or self.db" it will seek to create
        the database with the name that was handed to it.
        '''
        self.db = db
        if self.db == None :
            hostName = hostnm
            if hostName is None :
                print("ALERT: DB servers hostname can not be None. Try 'ossia-build'")
                raise ValueError
            if self.debug :
                print("INFO: initDB dbname={}".format(dbname))
            self.db = sqlConnect(hostname=hostName, debug=self.debug)
        if dbname is not None :
            self.db.makeDataBase(dbname)

    def initDBTable(self, dbtable, table_def=None) : 
        ''' initDBTable -- initialize the handed in table name in the data base.
        '''
        db_table_def = list()
        if table_def is None and self.cms is not None :
            for i in range(len(self.cms)) :
                if self.debug :
                    print("INFO: initDBTable i={} cms={} CMS_FIELD={} NAME_FIELD={}".format(i, repr(self.cms[i]), CMS_DBFMT_FIELD, CMS_NAME_FIELD))
                if len(self.cms[i]) >= CMS_DBFMT_FIELD and self.cms[i][CMS_DBFMT_FIELD] is not None :
                    db_table_def.append([self.cms[i][CMS_NAME_FIELD], self.cms[i][CMS_DBFMT_FIELD]])
        elif table_def is not None :
            db_table_def = table_def

        if self.debug :
            print("INFO: initDBTable db_table_def={}".format(repr(db_table_def)))
        self.db_table_name = dbtable
        self.db.makeTable(self.db_table_name, db_table_def)
    
    def initFile(self, cms, how='w') :
        ''' initFile -- initialize the log file name. 'how' allows adding to an existing
        log file by setting it to "a". Any valid file open string can be supplied.
        '''
        self.logData = dict()
        hdline, self.colformat = self.mkFormat(cms)
        if self.console :
            print("HEADER:{}".format(hdline))
        try :
            fd = open(self.filename, how)
            fd.write(hdline+"\n")
            fd.close()
        except IOError as e :
            print("IOError {}".format(repr(e)))
            raise

    def logAddValue(self, name, value) :
        ''' logAddValue -- Add a name, value pair to the log value dictionary. If the name exits
        the value is changed, if the name does not exist then the name is added to the dictionary
        and then expected there after. There is no provision for adding a new name to the column string.
        '''
        testval = isinstance(value, str)
        try :
            testval |= isinstance(value, unicode)
        except NameError :
            pass
        if testval :
            value = str(value).replace(',',';')
        if self.logData.get(name, None) :
            self.logData[name] = value
        else :
            self.logData.update({name : value})

    def logAddMesLine(self, line=" ") :
        if self.console :
            print("{}".format(line))
        # write the data out to the log file
        fd = open(self.filename, 'a')
        rtnval = "Could not write to log file {}".format(self.filename)
        if fd :
            fd.write(line+"\n")
            fd.close()
            rtnval = False
        return rtnval

    def logResults(self) :
        ''' logResults -- write the log dictionary to the log file and to the DB if appropriate.
        This method must be executed for any data to be written to the log file or the data base.
        '''
        if len(self.logData) == 0 :
            return False
        dd = list()
        db_dd = list()
        for i in range(len(self.cms)) :
            hd = self.cms[i][CMS_NAME_FIELD]
            fmt = self.cms[i][CMS_FMT_FIELD]
            value = self.logData.get(hd, None)
            # just in case the entire defined data list is has not been added
            val = 'ND';
            if value == val :
                val = ' '
                del self.logData[hd]
            elif value is not None :
                if self.debug :
                    print("INFO: hd={} v={} t={} fmt={}".format(hd, value, type(value), fmt))
                try :
                    val = fmt % value
                except Exception as ee :
                    print("ERROR: Exception in logResults -- fmt={} value={} {} -- {}".format(fmt, repr(value), type(value), repr(ee)))
                    raise
                del self.logData[hd]

            dd.append(val.replace("\n", ";").replace("\r", ""))
            db_dd.append((hd,val))
        if self.debug :
            print("INFO: logResults -- dd={}".format(repr(dd)))
            print("INFO: logResults -- db_dd={}".format(repr(db_dd)))
        line = self.colformat % (tuple(dd))
        # just in case data fields with New (misspelled) names
        # add theses to the end of the data list.
        # but not the data base list
        for k in self.logData :
            line += ", " + repr(k) + '=' + repr(self.logData[k])
            fmt = "%s"
            if type(self.logData[k]) == int :
                fmt = "%d"
            elif type(self.logData[k]) == float :
                fmt = "%3.2f"
            self.cms.append([k, fmt])
            self.colformat += ","+self.fmtHd
        if self.console :
            print("DATA:{}".format(line))
        
        # write the data out to the log file
        fd = open(self.filename, 'a')
        rtnval = "ERROR: Could not write to log file {}".format(self.filename)
        if fd :
            fd.write(line+"\n")
            fd.close()
            rtnval = False
        self.logData = dict()
        if self.db is not None :
            iStr = self.db.makeInsertStringOL(self.db_table_name, db_dd)
            if self.debug :
                print("INFO: logResults -- Database insert string iStr='{}'".format(iStr))
            try :                
                self.db.setDBData(iStr)
            except Exception as ee :
                print("ERROR: Exception in logResults when setting '{}' into database -- {}".format(iStr, repr(ee)))
                raise
        return rtnval

    def logPutHeadingStr(self, heading, strval) :
        """ add a new heading line and a strval on the next
        """
        fd = open(self.filename, 'a')
        rtnval = "Could not write to log file {}".format(self.filename)
        if fd :
            line = "\n{}\n{}".format(heading, strval)
            fd.write(line+"\n")
            fd.close()
            rtnval = False
        return rtnval

    def saveTraceData(self, indexCnt, traceData, headDict, tablename, logtodb) :
        """ saveTraceData -- Save the trace data in a column. The column name is made here by
                             using the indexCnt value of the headDict. The data is saved as well
                             as the first values of the column. A divide marker will be set between
                             trace data and header.
        """
        # do the log file first put everything on one line.
        line = "TLIndex{},".format(indexCnt)
        hdrLen = len(headDict)
        line += "{},".format(hdrLen)
        headList = list()
        headList.append(hdrLen)
        for i in range(hdrLen) :
            val = headDict.get(i, None)
            if val is not None :
                try :
                    line += str(val) + ","
                except ValueError :
                    pass
            else :
                line += "null,"
            headList.append(val)
        line += traceData 
        fd = open(self.filename, 'a')
        if fd :
            fd.write(line+"\n")
            fd.close()

        # now do the data base if it's inited
        if self.db is not None and logtodb :
            tdlist = traceData.split(",")
            traceDataList = headList + tdlist
            colName = "TraceDataTestList{}".format(indexCnt)
            self.db.makeTable(tablename, [[colName, "VARCHAR(25)"]])
            self.db.addNewColToTable(tablename, colName, "VARCHAR(25)")
            self.db.setNewColData(tablename, colName, traceDataList)

    def logUpdateTableColumn(self, dbname, table, column, data, MorR='R') :
        """ logUpdateTableColum -- Allows data to be modified or replaced in a table and column
                                   at the current row defined by the curent time stamp.
        """
        # create the update query string
        setval = data
        if "M" == MorR :
            # get existing data
            strval = "select {} from {}.{} where DataTimeStamp like '{}';".format(column, dbname, table, self.timeStamp)
            rtnlst = self.db.getDBData(strval)
            for Existdat in rtnlst :
                setval = Existdat + ' ' + data if len(Existdat) > 0 else data
                break;
        # create the update query string
        strval = "update {}.{} set {}='{}' where DataTimeStamp like '{}' limit 1;".format(dbname, table, column, setval, self.timeStamp)
        if self.debug :
            print("update str = {}\n".format(strval))
        self.db.setDBData(strval)

            
if __name__ == '__main__' :

    l = LogResults("testOnly{}.txt", console=False, debug=True)
    dd = [["test1", "%s"],
          ["test2", "%d"],
          ["testtest3", "%d"],
          ["test4", "%3.2f"]]

    l.initFile(dd)
    l.logAddValue('test1', '1')
    l.logAddValue('test2', 2)
    l.logAddValue('testtest3', 3)
    l.logAddValue('test4', 4.3)
    l.logAddValue('test5', 5)

    print("The Filename=" + l.filename)
    print("The ColFormat=" + l.colformat)
    print("The logData=" + repr(l.logData))
    print("The CMS t0=" + repr(l.cms))
    l.logResults()
    print("The CMS t1=" + repr(l.cms))

    l.logAddValue('test4', 4.6)
    l.logAddValue('test5', 6)
    l.logResults()

    fd = open(l.filename,'r')
    line = fd.read()
    fd.close()
    print(line)
