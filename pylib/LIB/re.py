 # Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

class EmailResults(object) :

    def __init__(self, emailaddress=None, fromaddress=None) :
        self.theTo = list(["ursusm@ossia.com"])
        if emailaddress is not None :
            if type(emailaddress) is list :
                self.theTo.extend(emailaddress)
            else :
                self.theTo.extend(emailaddress.split(","))
        self.theFrom = "do-not-reply@ossia.com"
        if fromaddress is not None :
            self.theFrom = fromaddress
        self.resultsList = list()
        self.subjectStr = "Test Results"
        self.mailServer = "mail.ossia.com"

    def setToAddress(self, strval) :
        if type(strval) is list :
            self.theTo.extend(strval)
        else :
            self.theTo.extend(strval.split(","))

    def setMailServer(self, strval) :
        self.mailServer = strval

    def setFromAddress(self, strval) :
        self.theFrom = strval

    def setSubject(self, strval) :
        self.subjectStr = strval

    def setResults(self, strtoadd) :
        self.resultsList.append(strtoadd)

    def _makeListToCSV(self, thelist) :
        strval = ""
        for v in thelist :
            strval += v+", "
        strval.rstrip(",")
        return strval

    def emailResults(self, alist=None) :
        locallist = self.resultsList
        if alist is not None :
            locallist = alist

        strval = ""
        for value in locallist :
            strval += value+"\n"
        # Create a text/plain message
        msg = MIMEText(strval)

        msg['Subject'] = self.subjectStr
        msg['From'] = self.theFrom
        msg['To'] = self._makeListToCSV(self.theTo)

        # Send the message via our own SMTP server, but don't include the
        # envelope header.
        s = smtplib.SMTP(self.mailServer)
        s.sendmail(self.theFrom, self.theTo, msg.as_string())
        s.quit()

if __name__ == "__main__" :

    e = EmailResults()

    e.setSubject("Default mail list")

    e.setResults("Amazing Stories from beyond the stars!!")
    e.setResults("They will inspire and invade your mind.")
    e.emailResults()

    del e

    e = EmailResults("deverdata@ossia.com")

    e.setSubject("Add one recipient")

    e.setResults("Amazing Stories from beyond the galaxy!!")
    e.setResults("They will inspire and invade your mind.")


    e.emailResults()

    del e

    e = EmailResults()

    e.setSubject("Add one recipient")
    e.setToAddress("testdata@ossia.com")

    e.setMailServer("mail.ossia.com")

    e.setFromAddress("ooooyaaay@ossia.com")

    e.setResults("Amazing Stories from beyond the universe!!")
    e.setResults("They will inspire and invade your mind.")

    e.emailResults()
