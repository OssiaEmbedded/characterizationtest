import sys
import json
import MySQLdb as mydb
from database import sqlConnect
from logResults import LogResults

DBNAMEDEF='ClientCharacterizationTest'
TXTABLENAME='TxData'
TransmitterColumnsLoL = [["TxId", "VARCHAR(25)"],
                      ["PowerLevel", "VARCHAR(10)"],
                      ["OnAMBs", "VARCHAR(15)"],
                      ["GoodAMBs", "VARCHAR(15)"],
                      ["WarmupTime", "VARCHAR(6)"],
                      ["TxAngle", "VARCHAR(6)"],
                      ["DataTimeStamp", "VARCHAR(25)"]]

TransmitterColumnsLoT = [("TxId", "VARCHAR(25)"),
                      ("PowerLevel", "VARCHAR(10)"),
                      ("OnAMBs", "VARCHAR(15)"),
                      ("GoodAMBs", "VARCHAR(15)"),
                      ("WarmupTime", "VARCHAR(6)"),
                      ("TxAngle", "VARCHAR(6)"),
                      ("DataTimeStamp", "VARCHAR(25)")]

TransmitterColumnsLoD = [{"TxId": "VARCHAR(25)"},
                      {"PowerLevel": "VARCHAR(10)"},
                      {"OnAMBs": "VARCHAR(15)"},
                      {"GoodAMBs": "VARCHAR(15)"},
                      {"WarmupTime": "VARCHAR(6)"},
                      {"TxAngle": "VARCHAR(6)"},
                      {"DataTimeStamp": "VARCHAR(25)"}]

TransmitterColumnsLoR = [("TxId", "VARCHAR(25)"),
                      ["PowerLevel", "VARCHAR(10)"],
                      {"OnAMBs": "VARCHAR(15)"},
                      {"GoodAMBs": "VARCHAR(15)"},
                      ("WarmupTime", "VARCHAR(6)"),
                      ["TxAngle", "VARCHAR(6)"],
                      ("DataTimeStamp", "VARCHAR(25)")]

TransmitterTestData = {"DATA": {"TxId": "12345678765432",
                      "PowerLevel": "22",
                      "OnAMBs": "0x0000FFFF",
                      "GoodAMBs": "0x0000FFFF",
                      "WarmupTime": "17",
                      "TxAngle": "0",
                      "DataTimeStamp": "TheDayBeforeTomorrow"},
                      "TBLNAME": TXTABLENAME}

def dbTestmain() :
    if len(sys.argv) == 2 :
        # test to check if a database and table are available
        nameList = list()
        sc = sqlConnect(dbname=DBNAMEDEF, hostname='ossia-build')
        sstr = "show columns from {}.TxData;".format(DBNAMEDEF)
        col_names_list = sc.getDBData(sstr)
        print col_names_list
        if col_names_list is not None :
            for col in col_names_list :
                nameList.append(col[0])
            sstr = sc.makeSelectString(TXTABLENAME, "DataTimeStamp", "TheDayBeforeTomorrow")
            data_list = sc.getDBData(sstr)
            outDict = dict()
            for dl in data_list :
                for nm, dat in zip(nameList, dl) :
                    if outDict.get(nm, None) is None :
                        outDict.update({nm: list()})
                    outDict[nm].append(dat)
            print "xx=%s" % repr(outDict)
    elif len(sys.argv) == 3 :
        # test to retrieve data from the database
        sc = sqlConnect(dbname=DBNAMEDEF, hostname='ossia-build')
        x=sc.getTableDataDict(TXTABLENAME, "DataTimeStamp", "TheDayBeforeTomorrow")
        print "x={}".format(repr(x))

    elif len(sys.argv) == 4 :
        sc = sqlConnect(dbname='tst')
        print sc.makeInsertStringOL("gggg",[("a","11111"),("bbbb","22222"),('ccccc','333333')])
        logger = LogResults("tsst.txt", debug=True)
        txcms = [["txId", "%s", 'VARCHAR(25)'],
                 ["PowerLevel", "%d", 'VARCHAR(5)'],
                 ["OnChannels", "%s",'VARCHAR(15)'],
                 ["GoodChannels", "%s",'VARCHAR(15)'],
                 ["WarmupTime", "%d",'VARCHAR(7)'],
                 ["TxAngle", "%d",'VARCHAR(7)'],
                 ["DataTimeStamp", "%s",'VARCHAR(35)']]
 
        logger.initFile(cms=txcms)
        logger.initDB(None, db=sc)
        logger.initDBTable(None)
        logger.logAddValue("txId", "xxxx")
        logger.logAddValue("PowerLevel", 228)
        logger.logResults()

    else : # sys.arv == 1
        # main usage section sys.arv == 1
        sc = sqlConnect(hostname='ossia-build')
        sc.makeDataBase(DBNAMEDEF)
    
        sc.makeTable(TXTABLENAME, TransmitterColumnsLoR)
    
        inst_str = sc.makeInsertString(TransmitterColumnsLoR, TransmitterTestData)
        sc.setDBData(inst_str)
      
        dbDict = {"TBLNAME" : TXTABLENAME, "SEARCHCOL" : "DataTimeStamp", "SEARCHSTR" : "*"}
        sstr = sc.makeSelectString(dbDict)
        print sstr
        xx = sc.getDBData(sstr)
        print "xx=%s" % repr(xx)

if __name__ == "__main__" :
    dbTestmain()
