""" timestamp.py: A set of methods to generate, reverse and regenerate a time stamp
string based on the defined format string.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
import time

TSFORMATSTR = "%a_%b_%d_%Y_%H_%M_%S"
TMFORMATSTR = "%H:%M:%S"

def createTimeStamp(inTime=None) :
    ''' createTimeStamp -- make a time string from the current time or a passed in integer
    returns: a formated date time string according to TSFORMATSTR.
    '''
    if inTime is None :
        inTime = time.time()
    ts = time.strftime(TSFORMATSTR, time.localtime(inTime))
    return str(ts)

def getTimeStrFromNumber(timenumber) :
    tm = time.strftime(TMFORMATSTR, time.localtime(timenumber))
    return tm

def getTimeNumberFromStr(timestamp) :
    ''' getTimeNumberFromStr -- takes a time stamp string in the defined format
                                above and reverses that to get the original time
                                integer from the Epoch.
        input:   A string in the format TSFORMATSTR
        returns: An integer in seconds since the Epoch derived from the string.
    '''
    timeval = int(time.time())
    try :
        st = time.strptime(timestamp, TSFORMATSTR)
        timeval  = time.mktime(st)
    except ValueError :
        print("Time Stamp {} does not match the format '{}'".format(timestamp, time.strftime(TSFORMATSTR, time.localtime(timeval))))
    return timeval

def DurationCalc(START_TIME, incldays=False) :
    duration = int(time.time() - START_TIME)
    hrs = float(duration) / 3600.0
    if incldays :
        days = hrs / 24.0
        hrs  = (days - int(days)) * 24.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005

    if incldays :
        Duration = "{:d}_{:02d}_{:02d}_{:02d}".format(int(days), int(hrs), int(mins), int(secs))
        return days, hrs, mins, secs, Duration

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    return hrs, mins, secs, Duration

if __name__ == "__main__" :

    ts=createTimeStamp()
    rs=createTimeStamp(0)
    tss=getTimeNumberFromStr(ts)
    rss=getTimeNumberFromStr(rs)

    days, hr, mins, secs, Duration = DurationCalc(rss, True)
    hr1, mins1, secs1, Duration1 = DurationCalc(rss)

    print("ts={}\ntss={}\nrs={}\nrss={}\n\ndays/hrs/mins/secs {}_{}_{}_{}\nDuration={}\nhrs1/mins1/secs1 {}_{}_{}\nDuration1={}".format(ts,tss,rs,rss,int(days),int(hr),int(mins),int(secs),Duration,int(hr1),int(mins1),int(secs1),Duration1))

    print("\n##########################\n")

    time.sleep(5)
    ts=createTimeStamp()
    rs=createTimeStamp(tss-3700)
    tss=getTimeNumberFromStr(ts)
    rss=getTimeNumberFromStr(rs)

    days, hr, mins, secs, Duration = DurationCalc(rss, True)
    hr1, mins1, secs1, Duration1 = DurationCalc(rss)

    print("ts={}\ntss={}\nrs={}\nrss={}\n\ndays/hrs/mins/secs {}_{}_{}_{}\nDuration={}\nhrs1/mins1/secs1 {}_{}_{}\nDuration1={}".format(ts,tss,rs,rss,int(days),int(hr),int(mins),int(secs),Duration,int(hr1),int(mins1),int(secs1),Duration1))

