''' utilmod.py -- Utility module to hold common methods to share over Tests and Utils

Version : 0.0.1
Date : May 20 2019
Copyright Ossia Inc. 2019

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback, inspect
if "/home/ossiadev/pylib" not in sys.path  :
    sys.path.insert(0, "/home/ossiadev/pylib")

import json
from time import sleep, time, localtime
from LIB.resultsEmail import EmailResults

# object to Email Results to a defined list 
emailer = EmailResults()

def eprint(msg, ln=False) :
    linemsg =  msg
    if ln :
        linemsg = "{} @@{}".format(msg, inspect.currentframe().f_back.f_lineno)
    print("E {}".format(linemsg))
    emailer.setResults(linemsg)

def _makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict

def parseConfigFile(parser, option, cmdLinePred=False) :

    """ parseConfigFile -- reads in a config file, a python dictionary, and updates the option.__dict__ with
                           the values in the config file and/or adds them to the option.__dict__
                           What's in the option.__dict__ is OVER WRITTEN. Which means that command line paramters
                           have lower priority than config file parameters, UNLESS cmdLinePred is set true. Then the
                           commmand line paramters if not None will take precedence.

    """
    import imp
    try :
        cfgmod = imp.load_source(option.cfgname, option.cfgfile)
    except Exception as ee :
        print("ERROR in loading cfgfile {} nameed {} -- {}".format(option.cfgfile, option.cfgname, repr(ee)))
        raise RuntimeError
    cfg = eval("cfgmod."+option.cfgname)
    optType = _makeOptionTypeDict(parser)
    return _addChangeOptions(optType, option, cfg, cmdLinePred)

def _addChangeOptions(optType, option, cfg, cmdLinePred=False) :

    TTYPE = 0
    TDEF = 1
    for k in cfg.keys() :
        try :
            if optType[k][TTYPE]  == 'int' :
                if str(cfg[k]) == 'DeFault' : # check for a default value
                    option.__dict__[k] = optType[k][TDEF] # set the default value
                else :
                    if option.__dict__[k] is None or not cmdLinePred :
                        option.__dict__[k] = int(str(cfg[k]))
            elif optType[k][TTYPE]  == 'float' :
                if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                    option.__dict__[k] = optType[k][TDEF] # set the default value
                else :
                    if option.__dict__[k] is None or not cmdLinePred :
                        option.__dict__[k] = float(str(cfg[k]))
            elif optType[k][TTYPE]  is None :
                option.__dict__[k] = optType[k][TDEF]  # set the default value
                if option.__dict__[k] is None or not cmdLinePred :
                    try :
                        option.__dict__[k] = eval(cfg[k])
                    except NameError :
                        option.__dict__[k] = False
                    except TypeError :
                        option.__dict__[k] = cfg[k]
            elif optType[k][TTYPE]  == 'string' :
                if str(cfg[k]) == 'DeFault' : # check for a default value
                    option.__dict__[k] = optType[k][TDEF] # set the default value
                elif str(cfg[k]) == 'None' :
                    option.__dict__[k] = None
                else :
                    if option.__dict__[k] is None or not cmdLinePred or option.__dict__[k] == '' :
                        option.__dict__[k] = str(cfg[k])
            else :
                if option.__dict__[k] is None or not cmdLinePred :
                    option.__dict__[k] = str(cfg[k])
        except KeyError :
            eprint("INFO: New Option {} -- Adding option to option dictionary".format(k))
            option.__dict__.update({k : cfg[k]})
        except (ValueError, IndexError) as eee :
            eprint("ERROR: index or value {} -- {}\n".format(k, repr(eee)))
            raise RuntimeError
    return option

def makeDictToString(theDict) :
    rtnstr = "Input is not Dict"
    if type(theDict) is dict :
        rtnstr = ""
        for name, value in theDict.iteritems() :
            rtnstr += "{}={}\n".format(name, value)
    return rtnstr

def optionsDictToString(options) :
    val = ""
    for n, v in options.__dict__.iteritems() :
        val += '{} : {}\n'.format(n, v)
        #val =  repr(option.__dict__).replace("'","").replace('"',"").replace(","," ").replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
    return str(val)

def printTime(msg1, warmupin=None, noprnt=False) :
    tm_obj = localtime(time())
    if warmupin is not None :
        tf_obj = localtime(time()+warmupin)
        fmt = msg1 + " Start at -- {:02d}:{:02d}:{:02d} finish at {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(warmupin, tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec,
                                      tf_obj.tm_hour, tf_obj.tm_min, tf_obj.tm_sec)
    else :
        fmt = msg1 + " -- {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
    if not noprnt :
        eprint(strval)
    return str(strval)

def makeListToString(thelist, delimit=";", takeend=True, debug=False) :
    strval = None
    if isinstance(thelist, list) :
        strval = ""
        for a in thelist :
            strval += str(a)+delimit
        if takeend :
            strval = strval.rstrip(delimit)
    if debug :
        print("makeListToString -- strval = '{}' input = '{}'".format(strval, repr(thelist)))
    return str(strval)

def checkWithin(value1, value2, within) :

    rtnval = False
    diffval = abs(value1 - value2)
    if diffval < within :
        rtnval = True
    return rtnval

def parseDistance(inStr) :
    """ parseDistance -- parses mut fields and returns a list of distances
                         range;step -- looks for a - between two numbers and a semicolon for the step
                         comma separated list of positions
    """
    rtnlist = list()
    if "-" in inStr and ";" in inStr : # do range parsing
        try :
            firstsplit = inStr.split(';')
            step       = int(firstsplit[1])
            secsplit   = firstsplit[0].split('-')
            start      = int(secsplit[0])
            end        = int(secsplit[1])
            expression = " >= "
            if (end - start) < 0 :
                expression = " <= "
            elif (end - start) == 0 :
                return rtnlist
            if eval("{}{}{}".format((end - start), expression,  step)) :
                while eval("{}{}{}".format(end, expression, start)) :
                    rtnlist.append(start)
                    start += step
        except ValueError :
            pass
    else : # do comma parsing
        rtnlist = inStr.split(',')
    return rtnlist

if __name__ == "__main__" :

    from optparse import OptionParser
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None,       help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default=None,       help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default="",         help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default=None,       help="Use the named list from the file (.py).")
    parser.add_option("","--passfailfile", dest='passfailfile', type=str,  action='store',default=None,       help="Use a list file (.py) for data collection positions")
    parser.add_option("","--cmdlinepred",  dest='cmdlinepred',             action='store_true',default=False, help="Used to test cmdLinePred feature.")

    (opts, args) = parser.parse_args()
    val = optionsDictToString(opts)
    print('opts dict = {}'.format(val))

    cmdlinepred = opts.cmdlinepred

    cfg = {'passfailfile' : "xxxxxxxxx", 'testtest' : {'a' : 1, 'b' : 2}, "aaa" : [1,2,3,4], 'bbb' : "1,2,3,4"}
    val = makeDictToString(cfg)
    print("cfg dict = '{}'".format(val))

    optType = _makeOptionTypeDict(parser)
    options = _addChangeOptions(optType, opts, cfg, cmdlinepred)
    val = optionsDictToString(options)
    print("options dict = '{}'".format(val))

