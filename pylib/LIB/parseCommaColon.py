''' parseCommaColon.py -- Set of utility methods to parse comma and colon seperated values in a string

Version : 0.0.0
Date : Aut 31 2019
Copyright Ossia Inc. 2019

'''

#
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
from time import sleep

def parseColon(clstr, floatint=True) :
    rtnlist = list()
    # fisrt try commas
    colonList = clstr.split(':')
    if len(colonList) > 1 :
        step  = 1
        start = int(colonList[0])
        stop  = start + 1
        if len(colonList) >= 2 :
            stop  = int(str(colonList[1])) + 1

        if len(colonList) == 3 :
            step = int(str(colonList[2]))

        #print("start={} stop={} step={}".format(start, stop, step))
        for i in range(start, stop, step) :
            rtnlist.append(i)
    else :
        strval = str(colonList[0])
        val = float(strval) if floatint else int(strval)
        rtnlist.append(val)
    return rtnlist

def expandStartEndStep(strval, floatint=True, precision=4) :
    rtnlist = None
    if strval is not None :
        seslist = parseCommaColon(str(strval), floatint=floatint)
        rtnlist = list()
        start = round(float(seslist[0]), precision) if floatint else int(seslist[0])
        end   = round(float(seslist[1]), precision) if floatint else int(seslist[1])
        step  = round(float(seslist[2]), precision) if floatint else int(seslist[2])
        while True :
            rtnlist.append(start)
            start = start + step if not floatint else round(start + step, precision)
            if (start > end and step > 0) or (start < end and step < 0) :
                break
    return rtnlist    

def parseCommaColon(strval, sortlist=False, floatint=True) :
    rtnlist = list()
    # fisrt try commas
    try :
        commaList = strval.split(',')
    except TypeError, ValueError :
        rtnlist = None
    else :
        #print("commaList = {}".format(repr(commaList)))
        if ":" in strval :
            for cl in commaList :
                clList = parseColon(cl, floatint)
                rtnlist.extend(clList)
        else :
            try :
                if floatint :
                    commaList = [float(x.strip()) for x in commaList]
                else :
                    commaList = [int(x.strip()) for x in commaList]
            #except ValueError as ve :
            except Exception as ve :
                print("ALERT: parseCommaColon {} -- {}".format(commaList, ve))
            rtnlist = commaList
        if sortlist :
            rtnlist.sort()
    return rtnlist

def tstLists(a, b) :

    al = len(a)
    bl = len(b)
    start = 0
    llst = a
    slst = b
    end = al
    short = bl
    if bl > al :
        end = bl
        short = al
        llst = b
        slst = a
    rtnlst = list()
    for i in range(start, end, 1) :
        if i < short :
            if llst[i] != slst[i] :
                rtnlst.append([llst[i], slst[i], i])
        else :
            rtnlst.append([llst[i], None, i]) 
    return rtnlst
        

if __name__ == "__main__" :


    r = parseCommaColon("1:5,6:12:2,15,17,18")
    p = [1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 17, 18]
    if ( r != p) :
        d = tstLists(r, p)
        print("expected {} returned {} diff {}".format(repr(p), repr(r), repr(d)))

    r = parseColon("1:8")
    p = [1, 2, 3, 4, 5, 6, 7, 8]
    if ( r != p) :
        d = tstLists(r, p)
        print("expected {} returned {} diff {}".format(repr(p), repr(r), repr(d)))

    r = parseCommaColon("6:12:2,15,1:5,17,18")
    p = [6, 8, 10, 12, 15, 1, 2, 3, 4, 5, 17, 18]
    if ( r != p) :
        d = tstLists(r, p)
        print("expected {} returned {} diff {}".format(repr(p), repr(r), repr(d)))

    r = parseCommaColon("6:12:2,15,1:5,17,18", sortlist=True)
    p = [1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 17, 18]
    if ( r != p) :
        d = tstLists(r, p)
        print("expected {} returned {} diff {}".format(repr(p), repr(r), repr(d)))

    strval = "0.1,5.3, 0.2"
    p = [0.1, 0.3, 0.5, 0.7, 0.9, 1.1, 1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1, 3.3, 3.5, 3.7, 3.9, 4.1, 4.3, 4.5, 4.7, 4.9, 5.1, 5.3]
    r = expandStartEndStep(strval)
    if ( r != p) :
        d = tstLists(r, p)
        print("ExpandStartEndStep \nexpected {}\nreturned {}\ndiff     {}".format(repr(p), repr(r), repr(d)))

