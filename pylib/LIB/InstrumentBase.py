""" InstrumentBase.py: A base class of sorts to connect to and communicate with
the SCPI test instruments.

Version : 1.0.0
Date : May 11 2019
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import socket, sys
from time import sleep, time

DEFAULTTIMEOUT = 0.20
DEFAULTLOOPTIMEOUT = 3.0

class InstrumentBase(object) :
    ''' InstrumentBase class -- Can be used as a base class but at this time is used as it.
    '''
                  

    def __init__(self, addr=None, port=5025, name='Instrument', sendbinary=False,  delimit='SCPI>',  debug=False, timethis=False) :
        ''' __init__ -- Initialize the class variables
        '''
        #self.port  = port if port == 80 else 5025
        self.port  = port 
        self.buflength = 2560
        self.debug = debug
        self.timethis = timethis
        self.useinstrument = True
        self.mySock = None
        self.addr = 'None Specified'
        self.errorstr = None
        self.name = name
        self.sendBinary = sendbinary
        self.rtndatDelimiter = delimit
        self.sendCR = True
        self.validOnOfflst = ["OFF","ON","0","1"]
        if addr is None :
            self.addr = '127.0.0.1'
            self.useinstrument = False
        else :
            try :
                self.addr = socket.gethostbyname(addr)
            except AttributeError as ae :
                self.errorstr = repr(ae) 
                self.useinstrument = False
            except Exception as ee :
                self.errorstr = repr(ee) 
                self.useinstrument = False
            else :
                self.lastData = None
                self.mySockTimeout = DEFAULTTIMEOUT
                self.mySockLoopTimeoutCount = int(DEFAULTLOOPTIMEOUT / self.mySockTimeout)

        if self.debug :
            print("addr={} port={}\n".format(self.addr, self.port))
        self.ibcmddict = { 'identify': {'get' : '*IDN?',
                                        'set' : '*IDN?',
                                        'argc': 0},

                           'reset': {'get' : '*RST',
                                     'set' : '*RST',
                                     'argc': 0},

                           'learn': {'get' : '*LRN?',
                                     'set' : '*LRN {}',
                                     'argc': 1},

                           'options': {'get' : '*OPT?',
                                       'set' : '*OPT?',
                                       'argc': 0},

                           'clrstatus': {'get' : '*CLS',
                                         'set' : '*CLS',
                                         'argc': 0},
                           'waitdone': {'get' : '*WAI',
                                        'set' : '*WAI',
                                        'argc': 0},
                         }

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.instrumentClose(True)
        if self.timethis :
            print("Overall time = {}".format(time()-self.ontime))

    def instrumentOpen(self) :
        ''' instrumentOpen -- Open the port connection to the specified Agilent Signal Analizer
        '''
        if self.mySock is None and self.useinstrument :
            self.ontime = time()
            self.mySock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                err = self.mySock.connect_ex((self.addr, self.port))
            except socket.error as se :
                print("Socket Error on connect_ex -- {}\n".format(repr(se)))
                self.mySock.close()
                self.mySock = None
                self.useinstrument = False
            if self.debug :
                print("\n\nerr={}, sock={}\n\n".format(err, repr(self.mySock)))
            if err != 0 :
                print("There was an error. Disabling use of {} at {}:{} (err {})".format(self.name, self.addr, self.port, repr(err)))
                self.useinstrument = False
                return False
            self.mySock.settimeout(self.mySockTimeout)
        return self.useinstrument

    def instrumentClose(self, doit=False) :
        ''' instrumentClose -- Close the port connection to the specified Agilent Signal Analizer
        '''
        if doit :
            if self.mySock is not None and self.useinstrument:
                self.mySock.close()
            self.mySock = None

    def sendgetimage(self, sendthis):
        ''' sendgetimage -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        data = None
        if self.debug :
            print("SA Stringdata='{}' useinstrument={}".format(sendthis, repr(self.useinstrument)))
        if self.mySock is not None and self.useinstrument :
            if sys.version_info[0] == 3 and not self.sendBinary :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)
            data = bytearray()
            d = bytearray()
            try :
                d = self.mySock.recv(256)
                while len(d) > 0 :
                    data += d
                    d = self.mySock.recv(256)
            except socket.timeout :
                if len(data) == 0 :
                    print("Socket Timeout without any data received")
                pass
    
            if self.debug :
                print("rtndat='" + repr(data) + "'")
        return data

    def getSaveImage(self, filename) :

        url = bytearray(b'GET /Agilent.SA.WebInstrument/Screen.png HTTP/1.1\r\nHost: 10.10.0.83:7081\r\nUser-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\n\r\n')
        sa = InstrumentBase(port=80)
        sa.instrumentOpen()
        saResponse = sa.sendgetimage(url)
        sa.instrumentClose()

        startOfImageData = bytearray(b'\r\n\r\n') 
        imageIndex = saResponse.find(startOfImageData) + 4
        with open(filename, "wb") as fd :
            fd.write(saResponse[imageIndex:])
            fd.close

    def sendgetdata(self, sendthis, waitforit=True):
        ''' sendgetdata -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        self.mySock.settimeout(self.mySockTimeout)
        rtndat = None
        if self.debug :
            print("\n$$$$$$$$$$$$$$${} Stringdata='{}' useinstrument={}".format(self.name, repr(sendthis), repr(self.useinstrument)))
        if self.mySock is not None and self.useinstrument :
            if sys.version_info[0] == 3 and not self.sendBinary :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)

            if self.sendCR :
                sendCR = "\r\n"
                if sys.version_info[0] == 3 and not self.sendBinary :
                    sendCR = bytes(sendCR,encoding="utf-8")
                self.mySock.send(sendCR )
                sleep(0.07)

            cnt = self.mySockLoopTimeoutCount
            if self.debug :
                print("beginning count = {} timeout = {}".format(cnt, self.mySockTimeout))
            datautf8 = bytearray()
            while cnt > 0 :
                cnt -= 1
                try :
                    d = self.mySock.recv(self.buflength)
                    datautf8 += d
                    while len(d) == self.buflength :
                        d = self.mySock.recv(self.buflength)
                        datautf8 += d
                    if len(datautf8) > 0 :
                        break
                except socket.timeout :
                    dLen = len(datautf8)
                    if not self.sendBinary :
                        dLen = len(datautf8.decode('utf-8').strip())
                    if dLen == 0 :
                        if self.debug :
                            print("Socket Timeout without any data received cnt={}".format(cnt))
                    elif self.debug :
                        if self.sendBinary :
                            print("sendthis='{}' cnt= {} data='{}' len(data)={}".format(repr(sendthis), cnt, repr(datautf8), len(datautf8)))
                        else :
                            print("sendthis='{}' cnt= {} data='{}' len(data)={}".format(sendthis, cnt, datautf8.decode('utf-8'), len(datautf8.decode('utf-8').strip())))
                    if not waitforit or dLen > 0 :
                        break

            l = None
            rtndat = str(datautf8).rstrip()
            if sys.version_info[0] == 3 and not self.sendBinary :
                rtndat = datautf8.decode('utf-8').lstrip()
            if self.debug :
                print("\n{} DATA='{}'".format(self.name, repr(rtndat)))
                print("delimit={} sendBinary={}".format(self.rtndatDelimiter, repr(self.sendBinary)))
            if cnt == 0 :
                print("\nALERT: {} loop count {} decremented to zero.\n".format(self.name, self.mySockLoopTimeoutCount))
            self.lastData = rtndat
            if not self.sendBinary and self.rtndatDelimiter is not None :
                l = rtndat.split(self.rtndatDelimiter)
                rtndat = l[-1].rstrip()
    
            if self.debug :
                if l is not None :
                    print("l={}".format(repr(l)))
                print("#### rtndat='{}'".format(repr(rtndat)))
        return rtndat

    def getdata(self, numOfBytes, waitforit=False):
        ''' getdata -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        rtndat = None
        data = "junk"

        if self.debug :
            print("mySockTimeout={} use={} numOfBytes={}".format(self.mySockTimeout, repr(self.useinstrument), numOfBytes))

        if self.mySock is not None and self.useinstrument :
            self.mySock.settimeout(self.mySockTimeout)

            cnt = self.mySockLoopTimeoutCount
            datautf8 = bytearray()
            tm = time()
            while cnt > 0 :
                cnt -= 1
                try :
                    d = self.mySock.recv(self.buflength)
                    datautf8 += d
                    while len(d) == self.buflength :
                        d = self.mySock.recv(self.buflength)
                        datautf8 += d
                    dLen = len(datautf8)
                    if sys.version_info[0] == 3 and not self.sendBinary :
                        dLen = len(datautf8.decode('utf-8').strip())
                    if dLen >= numOfBytes :
                        break
                except socket.timeout :
                    dLen = len(datautf8)
                    if sys.version_info[0] == 3 and not self.sendBinary :
                        dLen = len(datautf8.decode('utf-8').strip())
                    if dLen == 0 :
                        if self.debug :
                            print("Socket Timeout without any data received cnt={} {}".format(cnt, time()-tm))
                    elif self.debug :
                        print("numofbytes='{}' cnt= {} data='{}' len(data)={}".format(numOfBytes, cnt, repr(datautf8), len(datautf8)))
                    if not waitforit or dLen > 0 :
                        break

            data = str(datautf8)
            if sys.version_info[0] == 3 and not self.sendBinary :
                data = str(datautf8.decode('utf-8').strip())
    
            if self.debug :
                print("data={} datautf8={}".format(repr(data), repr(datautf8)))
        return data

    def senddata(self, sendthis, waitforit=False):
        ''' senddata -- Send the command string to the and don't wait for a response.
        Strip off any un-necessary white space and prompt data.
        '''
        rtndat = None
        if self.debug :
            print("Instrument {} Stringdata='{}' useinstrument={}".format(self.name, repr(sendthis), repr(self.useinstrument)))
        if self.mySock is not None and self.useinstrument :
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)

            if self.sendCR :
                sendCR = "\r\n"
                if sys.version_info[0] == 3 :
                    sendCR = bytes(sendCR,encoding="utf-8")
                self.mySock.send(sendCR )

        return rtndat

    @property
    def clrstatus(self) :
        ''' clrstatus -- get/set pair to clrstatus the Instrument
            only the setter is required for this command
        '''
        return True
    @clrstatus.setter
    def clrstatus(self, _) :
        ''' clrstatus -- get/set pair to clrstatus the Instrument
        '''
        self.instrumentOpen()
        self.senddata(self.ibcmddict['clrstatus']['set'])
        self.instrumentClose()
        
    @property
    def reset(self) :
        ''' reset -- get/set pair to reset the Instrument
            only the setter is required for this command
        '''
        return True
    @reset.setter
    def reset(self, _) :
        ''' reset -- get/set pair to reset the Instrument
        '''
        self.instrumentOpen()
        self.senddata(self.ibcmddict['reset']['set'])
        self.instrumentClose()

    @property
    def options(self) :
        ''' identity -- get/set pair to the Instrument options 
            only the getter is required for this command
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.ibcmddict['options']['get'], True)
        self.instrumentClose()
        return d
    @options.setter
    def options(self) :
        ''' identity -- get/set pair to identify the Instrument
        '''
        pass

    @property
    def identity(self) :
        ''' identity -- get/set pair to identify the Instrument
            only the getter is required for this command
        '''
        self.instrumentOpen()
        d = self.sendgetdata(self.ibcmddict['identify']['get'], True)
        self.instrumentClose()
        return d
    @identity.setter
    def identity(self) :
        ''' identity -- get/set pair to identify the Instrument
        '''
        pass

    @property
    def waitdone(self) :
        ''' waitdone -- get/set pair to waitdone the Instrument
            only the setter is required for this command
        '''
        return True
    @waitdone.setter
    def waitdone(self, _) :
        ''' waitdone -- get/set pair to waitdone the Instrument
        '''
        self.instrumentOpen()
        self.senddata(self.ibcmddict['waitdone']['set'])
        self.instrumentClose()

if __name__ == '__main__' :

   ib = InstrumentBase(addr=sys.argv[1], port=int(sys.argv[2]), debug=False)
   d = ib.identity
   print("identify={}".format(d))
