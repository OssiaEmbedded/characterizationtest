''' NardaSerial.py -- A module to commuicate with a Narda probe NBM-550


Version : 1.0.0
Date :  Oct 22 2017
Copyright Ossia Inc. 2017

'''
from __future__ import absolute_import, division, print_function, unicode_literals

from time import sleep
import serial
from socket import socket, gethostbyname

class NardaSerial(object) :

    nardaErrorCodes = {# "0"   : "no error",
                       "401" : "remote command is not implemented in the remote module",
                       "402" : "invalid parameter",
                       "403" : "invalid count of parameters",
                       "404" : "invalid parameter range",
                       "405" : "last command is not completed",
                       "406" : "answer time between remote module and application module is too high",
                       "407" : "wrong quit message from application module",
                       "408" : "invalid or corrupt data",
                       "409" : "error while accessing the EEPROM",
                       "410" : "error while accessing hardware resources",
                       "411" : "command is not supported in this version of the firmware",
                       "412" : "remote is not activated (please send 'REMOTE ON;' first)",
                       "413" : "command is not supported in the selected mode",
                       "414" : "memory of data logger is full",
                       "415" : "defragmentation of flash file system is required",
                       "416" : "option code is invalid",
                       "417" : "incompatible version",
                       "418" : "no Probe",}

    nardSetupCommands = ["remote on;",
                         "auto_light off;",
                         "auto_power off;",
                         "result_type avg;",
                         "result_unit mw/cm^2;",
                         "meas_view x-y-z;",
                         "sample_rate 50;",
                         "freq 2450000000;",
                         "avg_time 2;"]
    nardGetupCommands = ["remote?;",
                         "auto_light?;",
                         "auto_power?;",
                         "result_type?;",
                         "result_unit?;",
                         "meas_view?;",
                         "sample_rate?;",
                         "freq?;",
                         "avg_time?;"]
        
    def __init__(self, comport, commethod="serial", baudrate=460800, eport=4001, debug=False) :

        self.debug = debug
        if commethod.lower() == 'ethernet' :
            self.sendGetCommand = self.sendGetCommandEther
            ipaddr = gethostbyname(comport)
            self.ethernetadr    = (ipaddr, eport)
        elif commethod.lower() == 'serial' :
            self.sendGetCommand = self.sendGetCommandSerial
            self.serial = serial.Serial()
            self.serial.port = comport
            self.serial.baudrate = baudrate
            self.serial.timeout = 0.5

    def openConnection(self) :
        rtnval = self.serial.is_open
        if not rtnval :
            self.serial.open()
            rtnval =  self.serial.is_open
        if not rtnval :
            print("Serial port {} did not open".format(self.port))
        return rtnval

    def closeConnection(self) :
        self.serial.close()
        rtnval =  self.serial.is_open
        if rtnval :
            print("Serial port {} did not close".format(self.port))
        return rtnval

    def sendGetCommandEther(self, command) :
        retval = True
        mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        try :
            mySocket.connect(self.ethernetadr) # a tuple of ip and port
            mySocket.settimeout(0.1)
        except :
            if self.debug :
                print("There was a connect exception ")
            retval = False

        if self.debug :
            print("sock={}".format(repr(mySocket)))

        if retval and self.mySocket is not None and command is not None :
            mySocket.send( command + "\r\n" )
            if self.debug :
                print("sent {}".format(command))
        data = "" 
        d = bytearray()
        cnt = 100
        while cnt > 0 and retval :
            try :
                if mySocket is not None :
                    d = bytearray(mySocket.recv(800))
                    data += d.decode("utf-8")
            except socket.timeout :
                if self.debug :
                    print(cnt)
                cnt -= 1
            except socket.error as se :
                data = "Error -- {}".format(repr(se))
                if self.debug :
                    print("SocketError -- error No. {} -- {}\n".format(repr(se), data))
                sys.stdout.flush()
            if mySocket is None or len(data) > 0 :
                cnt = 0
                break

        if self.debug :
            print(data)
        if retval and cnt <= 0 and mySocket is not None :
            mySocket.close()
            rvalList = data.split(',')
            rval = rvalList[-1].strip(" ;\n\r\t")
            if self.nardaErrorCodes.get(rval, None) is not None :
                # error
                print("Error returned by probe for command {} == {} {}\n".format(command,rval, self.nardaErrorCodes[rval]))
                retval = False
        return renval, rvalList


    def sendGetCommandSerial(self, command) :
        retvalList = list()
        rtnval = True
        portOpened = self.openConnection()
        if portOpened :
            try :
                numSent = self.serial.write(str(command))
                rtndata = self.serial.read(200).strip(" ;\n\r\t")
                #print("rtndata = {}".format(rtndata))
                rvalList = rtndata.split(',')
                rval = rvalList[-1].strip(" ;\n\r\t")
                if self.nardaErrorCodes.get(rval, None) is not None :
                    # error
                    print("Error returned by probe for command {} == {} {}\n".format(command,rval, self.nardaErrorCodes[rval]))
                    rtnval = False
            except IOError as ioe :
                print("IO Error on write or read -- {}\n".format(repr(ioe)))
                rtnval = False
            portOpened = self.closeConnection()
        return rtnval, rvalList

    def setupNardaProbe(self) :
        for command in self.nardSetupCommands :
            retval, _ = self.sendGetCommand(command)
            if not retval  :
                break
        return retval

    def getupNardaProbe(self) :
        rtnlst = list()
        for command in self.nardGetupCommands :
            retval, param = self.sendGetCommand(command)
            if not retval  :
                break
            rtnlst.append( "{0} {2} -- {1}".format(command, retval, param))
        return rtnlst

    def getSetupString(self) :
        retstr = ""
        for i in range(len(self.nardSetupCommands)) :
            command = self.nardSetupCommands[i]
            if i > 0 :
                retstr += ','
            retstr += command
        return retstr

    def executeCommand(self, command) :
        retval, retvalList = self.sendGetCommand(str(command))
        if not retval :
            retvalList = list()
        else :
            for i in range(len(retvalList)) :
                retvalList[i] = str(retvalList[i]).strip()
                #print("The value returned by command {} == {}  {}\n".format(command, i, retvalList[i]))
        return retval, retvalList

    def resetMMA(self) :
        retval, retvalList = self.executeCommand("reset_mma;")
        return retval, retvalList

    def getDeviceInfo(self) :
        retval, retvalList = self.executeCommand("device_info?;")
        return retval, retvalList

    def getDeviceInfoString(self) :
        rtnstr = ""
        rval, rlist = self.getDeviceInfo()
        for i in range(len(rlist)) :
            if i > 0 :
                rtnstr += ','
            rtnstr += str(rlist[i])
        return rtnstr

    def getMeasuredData(self) :
        retval, retvalList = self.executeCommand("meas?;")
        #print("retvalList[0]={} type={}\n".format(retvalList[0], type(retvalList[0])))
        #print("retvalList[1]={} type={}\n".format(retvalList[1], type(retvalList[1])))
        #print("retvalList[2]={} type={}\n".format(retvalList[2], type(retvalList[2])))
        #print("retvalList[3]={} type={}\n".format(retvalList[3], type(retvalList[3])))
        #print("retvalList[4]={} type={}\n".format(retvalList[4], type(retvalList[4])))
        return retval, retvalList

    def getBattery(self) :
        retval, retvalList = self.executeCommand("battery?;")
        return retval, retvalList

    def zeroProbe(self) :
        retval, retvalList = self.executeCommand("zero switched;")
        sleep(7)
        return retval, retvalList

    def exitCommMode(self) :
        # set the auto power back to 6 secs.
        retval, _ = self.sendGetCommand("auto_power 6;")
        retval, _ = self.sendGetCommand("remote off;")

if __name__ == "__main__" :
    print("Unit tests start\n")
    import sys
    comdev = 'COM9'
    if len(sys.argv) > 1 :
        comdev = sys.argv[1]

    narda = NardaSerial(comport=comdev)
    narda.serial


    print("starting setup\n")
    retval = narda.setupNardaProbe()
    print("setup returned {}\n".format(retval))

    strval = narda.getSetupString()
    print("setup string is '{}'\n".format(strval))

    retlst = narda.getupNardaProbe()
    print("setup list is '{}'\n".format(retlst))

    r, retvallst = narda.getDeviceInfo()
    print("device info returned {} {}\n".format(r, retvallst))

    strval = narda.getDeviceInfoString()
    print("device info string returned {}\n".format(strval))

    r, retvallst = narda.getMeasuredData()
    print("measured data returned {} {}\n".format(r, retvallst))

    r, retvallst = narda.resetMMA()
    print("reset MMA returned {} {}\n".format(r, retvallst))

    r, retvallst = narda.getBattery()
    print("battery returned {} {}\n".format(r, retvallst))

    narda.exitCommMode()
    print("Unit tests done\n")
