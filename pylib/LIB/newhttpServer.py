#!/usr/bin/python

"""
 * @file
 *
 * @copyright Copyright 2017 Ossia, Inc. All rights reserved.
"""

from threading  import Thread
from socket     import gethostname,  socket, AF_INET, SOCK_STREAM
from socket     import timeout as TimeoutExcept
from time       import localtime, sleep, strftime
from os         import _exit, devnull
from sys        import argv 

from subprocess import check_output as spcheck_output

VERSION       = 'Sim 0.0.1'  # current release
STARTVAL      = 'COMMANDVAL'
ENDSTRVAL     = 'SUCCESS'


class httpServer() :

    # multi-thread values
    hostName     = "" # name of the host 
    mylogfd      = None
    ipaddr       = None

    # server communication and control
    listenSocket = None # socket that the accept functions listens on. It is bound to port PORT_NUMBER

    def __init__(self, **kwargs) : #logDirectory=None, debug=False, unittst=False) :

        self.FNULL           = None
        self.STARTVAL        = kwargs.get('startVal', STARTVAL)
        self.ENDSTRVAL       = kwargs.get('endVal', ENDSTRVAL)
        self.DELIMITVAL      = kwargs.get('delimitVal', '/')
        self.PORT_NUMBER     = kwargs.get('portNumber', 7000)
        self.SERVERROOTDIR   = kwargs.get('logDirectory', '/home/ursus/Simulators/SASim/')
        self.debug           = kwargs.get('debug', False)
        self.defaultCommand  = kwargs.get('defaultcmd', None)
        self.GET_Function    = kwargs.get('GET_function', None)
        self.waitForCRLF     = kwargs.get('waitForCRLF', None)
        self.errorFunction   = kwargs.get('errorFunction', None)
        self.doNotCloseSock  = kwargs.get('doNotCloseSock', False) 
        self.client          = None # the current client socket to send and receive from
        self.addr            = None # the address of the current client.
        self.noEndVal        = False
        self.numCharRecv     = 1 if self.doNotCloseSock else 8192

        self.urlDict = { "getVersion"            : [self.getVersion,            "Get the current server version"],
                         "getLog"                : [self.getLog,                "[num or all] [timestamp, prevSentTS] Return the last <num or all or to timestamp> entries in the log file"],
                         "setStartVal"           : [self.setStartVal,           "Set the starting command string val. It's a somewhat unneccessary preamble to the message."],
                         "setEndVal"             : [self.setEndVal,             "Set the End or Success string val. Whoever is talk to the server needs this to know there's no more data and to close the connection."],
                         "setDelimitVal"         : [self.setDelimitVal,         "Set the delimt value. The value between command and arguments. The first is the command the rest are aruguments."],
                         "getHelp"               : [self.getHelp,               "Return this list or any one (getHelp/<cmd>)"]}

        if self.SERVERROOTDIR[-1] != "/" :
                self.SERVERROOTDIR += "/"

        self.LOGFILENAME   = self.SERVERROOTDIR+'logfile.log'

        try :
            self.FNULL = open(devnull,'w')
        except Exception as ee :
            self._printToLog("DEVNULL unavailable, Exiting -- {}".format(repr(ee)))
            _exit(3)
        try :
            self.PORT_NUMBER = int(self.PORT_NUMBER)
        except ValueError as ve :
            self._printToLog("Port number not a number '{}', exiting. -- {}".format(self.PORT_NUMBER, repr(ve)))
            _exit(2) 
        else :
            if not kwargs.get('unittst', False) :
                if self._startServer() == 0 :
                    self._printToLog("exiting")
                    _exit(1) 

    def setStartVal(self, val) :
        self.STARTVAL = val

    def setEndVal(self, val) :
        self.ENDSTRVAL = val

    def setDelimitVal(self, val) :
        self.DELIMITVAL = val

    def addToUrlDict(self, newUrlDict) :
        rtnval = False
        try :
            for k, v in newUrlDict.iteritems() :
                if type(k) is not str or type(v) is not list or len(v) != 2 :
                    raise ValueError
        except ValueError as ve :
                self._printToLog("New URL Dictionary not valid at 'Key = {} value = {}' -- {}".format(k, repr(v), repr(ve)))
        else :
            self.urlDict.update(newUrlDict)
            rtnval = True
        return rtnval

    def getLog(self, **kwargs) :
        """ getLog -- Returns a string of the url entries in the log file.
                      arg0 eq a num or 'all' return the list as requested
                      arg0 eq 'timestamp' arg1 eq 'the prev. timestamp using setTimeStamp'
        """
        arg0 = kwargs.get('arg0', '50')
        try :
            num = int(arg0)
        except ValueError :
            num = 50
        strval = "Last {} log entries\n".format(num)
        try :
            lfd = open(self.LOGFILENAME,'r')
            if arg0 == 'timestamp' :
                arg1 = kwargs.get('arg1', 'timestamp')
                for line in reversed(lfd.readlines()) :
                    strval += line.rstrip()+"\n"
                    if "setTimeStamp" in line and arg1 in line.lower() :
                        break
            else :
                for line in reversed(lfd.readlines()) :
                    strval += line.rstrip()+"\n"
                    num -= 1
                    if num == 0 and arg0 != 'all' :
                        break
            lfd.close()
        except  Exception as ee:
            strval += "Could not open logfile {}\n".format(repr(ee))
            self._printToLog(strval.strip()+" -- {}".format(repr(ee)))
        return strval

    def getVersion(self, **kwargs) :
        return "{}".format(VERSION)

    def getHelp(self, **kwargs) :
        strval = ''
        cmd = kwargs.get('arg0', None)
        if cmd is not None and self.urlDict.get(cmd, None) is not None :
            helplst =  self.urlDict[cmd]
            strval += "{} : {}\n".format(cmd, helplst[1]) 
        else :
            for key in self.urlDict :
                helplst = self.urlDict[key]
                if len(helplst[1]) > 0 :
                    strval += "{} : {}\n".format(key, helplst[1]) 
        return strval

    def _serverThread(self, myClient, myAddr) :

        # reset the shutdown restart flag
        self.shutdownrestart = None

        # check address
        if '10.10.' not in myAddr[0] and '192.168.240.' not in myAddr[0] and '192.168.1.' not in myAddr[0] :
            myClient.close()
            print('Bad Address source {}'.format(myAddr[0]))
            return

        url = ""
        theUrl = ""
        myClient.settimeout(0.1)
        recvDone = True
        cnt = 50
        #print("going to recv")
        while recvDone :
            try :
                dd = " "
                while True :
                    dd = myClient.recv(1).decode('utf-8')
                    if dd == "\r" or dd == "\n" :
                        #print("dd='{}'".format(dd))
                        break
                    if len(dd) == 0 :
                        raise TimeoutExcept
                    url += dd
                recvDone = False
                theUrl = url.strip()
                url = ""
                #print("wrong place")
            except TimeoutExcept as te :
                #print("timeout '{}' {}".format(url, repr(self.waitForCRLF)))
                if len(url) > 0 :
                    cnt = 50
                    if self.waitForCRLF :
                        position = url.find("\r\n") + 2
                        if  position > 0 :
                            theUrl = url[:position]
                            url = url[position:]
                        else :
                            continue
                    else :
                        theUrl = url
                        url = ""
                        recvDone = False
                else :
                    cnt -= 1
                    if cnt <= 0 :
                        break
                    else :
                        continue
            except Exception as ee :
                self._printToLog("Exception in _serverThread (Some ip/port search app?) -- {}".format(repr(ee)))
                break

            if not recvDone :
                if self.doNotCloseSock :
                    if len(theUrl) > 0 :
                        self._serveUrls(myClient, myAddr, theUrl)
                    cnt = 50
                    recvDone = True
                    url = ""
                else :
                    self._serveUrls(myClient, myAddr, theUrl)
                    break
        myClient.close()
        return 

    def _serveUrls(self, myClient, myAddr, url) :
        """
        _serveUrls -- Main thread to receive and interpret data and send data from/to the sender. It also takes care
        of urls from any source that connects to this GUM at port PORT_NUMBER
        """

        if self.debug :
            print('full url="{}"'.format(url))

        url_path = url.strip().split(self.DELIMITVAL)
        if self.STARTVAL == "" :
            url_path.insert(0, "")

        if self.debug :
            print("url_path = {}".format(repr(url_path)))

        # log the url that was sent
        urlDataLog = "From {} : {}".format(str(myAddr[0]), url)

        self._logUrlData(urlDataLog)

        strval = ""
        alreadyClosed = False
        kwargs = dict()
        kwargs.update({'fullurl' : url, 'url_path' : url_path})

        if len(url_path) <= 1 or (url_path[0].rstrip() != self.STARTVAL and url_path[0].rstrip() != "GET") :
            if self.debug :
                print("BAD {}".format(self.STARTVAL))
            strval += 'Unexpected communication {} badly formed command string\n'.format(url)

        elif url_path[0].rstrip() == "GET" :
            if self.GET_Function is not None :
                kwargs.update({'client' : myClient})
                kwargs.update({'ipaddr' : myAddr})
                kwargs.update({'url'    : url})
                (self.GET_Function)(**kwargs)
            alreadyClosed = True
        
        # Empty Command
        elif url_path[1] == "" :
            strval = ''

        elif self.urlDict.get(url_path[1], None) is not None :
            self.noEndVal = False
            func = self.urlDict[url_path[1]][0]

            kwargs.update({"curcmd" : url_path[1]})
            for i in range(2,len(url_path), 1) :
                name = "arg"+str(i-2)
                kwargs.update({name: url_path[i].strip()})
                if 'nonblocking' in url_path[i].lower().strip() :
                    # makes this thread not block Which means the function
                    # will not return a value
                    alreadyClosed = True
            if self.debug :
                print("func={} args={}, help={}\n".format(repr(func), repr(kwargs), repr(self.urlDict[url_path[1]][1])))
            if alreadyClosed :
                try :
                    myClient.send("Shoot\n{}\n".format(self.ENDSTRVAL))
                except Exception as ee :
                    if self.debug :
                        print("Exception on send -- {}".format(repr(ee)))
                if not self.doNotCloseSock :
                    myClient.close()
            #
            # this is where the function object gets called
            #
            strval = func(**kwargs) 
            #print("return string from saServer '{}'".format(strval))
        else :
            if self.defaultCommand is not None :
                if self.debug :
                    print("default url={}".format(url))
                strval = (self.defaultCommand)(**kwargs)
            elif self.errorFunction is not None :
                if self.debug :
                    print("errorFunction url={}".format(url))
                strval = (self.errorFunction)(**kwargs)
            else :
                strval += "Command Not found {}".format(url_path[1])

        if not alreadyClosed :
            if not self.noEndVal :
                strval = '{}\n{}\n\r'.format(strval, self.ENDSTRVAL)

            if self.debug :
                print("func '{}' kwargs={} return string = '{}'".format(url_path[1], repr(kwargs).replace("\n",""), strval))
            try :
                myClient.send('{}'.format(strval).encode())
            except Exception as ee :
                if self.debug :
                    print("Exception on send -- {}".format(repr(ee)))
            if not self.doNotCloseSock :
                myClient.close()
        return
    
    def _printToLog(self, mes) :
        """
        _printToLog -- used to put interesting things in a log for later review
        """
        if not self.mylogfd :
            try :
                self.mylogfd = open(self.SERVERROOTDIR+"mylogPTL.log","w")
            except :
                self.mylogfd = None
        else :
            try :
                self.mylogfd = open(self.SERVERROOTDIR+"mylogPTL.log","a")
            except :
                self.mylogfd = None
        if self.mylogfd :
            self.mylogfd.write(mes+"\n")
            self.mylogfd.close()


    def _logUrlData(self, data) :
        """
        _logUrlData -- Puts the input data into the log file. Checks for number of lines and only keeps the most
        recent 500 lines.
        """

        NumOfLines = 500
        try :
            lfd = open(self.LOGFILENAME,'a+')
        except Exception as ee :
            self._printToLog("Could not open log file {} for append -- {}".format(self.LOGFILENAME, repr(ee)))
        else :
            lines = lfd.readlines()
            if len(lines) >= NumOfLines :
                lfd.close()
                try :
                    lfd = open(self.LOGFILENAME,'w')
                except Exception as ee :
                    self._printToLog("Could not open log file {} for creation -- {}".format(self.LOGFILENAME, repr(ee)))
                else :
                    for nn in range(1, NumOfLines) :
                        lfd.write(lines[nn])
            if lfd :
                dataAry = data.split("\n")
                dataone = ""
                timestamp = strftime("%Y_%m_%d-%H_%M_%S",localtime())
                if len(dataAry) > 1 :
                    dataone = "{}: {} {}\n".format(timestamp, dataAry[0].rstrip().lstrip(), dataAry[1].rstrip().lstrip())
                else :
                    dataone = "{}: {}\n".format(timestamp, dataAry[0].rstrip().lstrip())
                lfd.write(dataone)
                lfd.close()

    def _spawnThread(self) :
        """
        _spawnThread -- as the name implies, this function spawns a new thread using the _serveUrls function above.
        """
        try :
            threadme = Thread(target=self._serverThread, args=(self.client, self.addr))
            threadme.start()
        except Exception as ee :
            self._printToLog("Exception starting thread -- {}".format(repr(ee)))

    def _getIpAddress(self) :
        """
        _getIpAddress -- during startup of the GumStix device it takes some time to get an IP address from the network.
        This checks ifconfig for eth0 and if an IP address has been assigned returns it.
        """
        rtnval = None

        cmdlst = ["ifconfig eth0", "ifconfig ens33"]
        try :
            data = ""
            for cmd in cmdlst :
                try :
                    data = str(spcheck_output(cmd,stderr=self.FNULL,shell=True).decode('utf-8'))
                    break
                except Exception :
                    pass
          
            lines = data.split('\n')
            for line in lines :
                if "inet addr:" in line :
                    t = line.split()
                    ip = t[1].split(":")
                    rtnval =  ip[1]
                    break
        except Exception as ee:
            self._printToLog("Exception in _getIpAddress = {}".format(repr(ee)))
        return rtnval


    def _startServer(self) :
        """
        _startServer -- Start the http server. During development this function tries to bind to the PORT PORT_NUMBER and wait
        if it's not available. This occurs during dev testing and allows for remote testing.
        """
        self._printToLog("\nProgram Name -- {}".format(argv[0]))
        self.ipaddr = self._getIpAddress()
        i=10
        while not self.ipaddr and i > 0 :
            sleep(10)
            self.ipaddr = self._getIpAddress()
            i = i - 1
        if i > 0 :
            self.hostName = gethostname()
            self._printToLog(repr(i))
            self._printToLog(self.hostName)
            self._printToLog(self.ipaddr)

            server = (self.ipaddr, self.PORT_NUMBER)
            self.listenSocket = socket(AF_INET, SOCK_STREAM)
            i = 5
            while i > 0 :
                try :
                    self.listenSocket.bind(server)
                    self._printToLog("got it")
                    break
                except  :
                    # try again
                    self._printToLog("trying again {}".format(i))
                    sleep(15)
                    i = i - 1
            if i > 0 :
                self.listenSocket.listen(5)
        return i
    
    def runServer(self) :
        """
        runServer -- this function is the server. It listens at the port bound and waits for someone to send something.
        """
        while True :
            try :
                self.client, self.addr = self.listenSocket.accept()
            except KeyboardInterrupt as kbi :
                self.listenSocket.close()
                self._printToLog("KeyboardInterrupt, Exiting -- {}".format(repr(kbi)))
                break
            except Exception as ee :
                self._printToLog("Exception in accept, continuing --- {}".format(repr(ee)))
                continue
            self._spawnThread()
            sleep(0.005)

if __name__ == "__main__" :

    dbug = False
    utst = False
    if len(argv) > 1 :
        if 'debug' in argv[1].lower() : 
            dbug = True
        elif 'unit' in argv[1].lower() :
            utst = True
        
    http = httpServer(debug=dbug, unittst=utst)
    if utst :
        print("test1 = {}".format(http.addToUrlDict({"xxx" : [http.runServer, "test1"]})))
        print("test2 = {}".format(http.addToUrlDict({"xxx" : "test2"})))
        print("test3 = {}".format(http.addToUrlDict({1010 :  [http.runServer, "test3"]})))
    else :
        http.runServer()
    http.FNULL.close()
