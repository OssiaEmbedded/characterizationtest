''' MCSwitch.py -- Interface to control a Mini-Circuits 8 port RF Switch

Makes use of a Windows executable.

Date :  Mar 06 2018
Copyright Ossia Inc. 2018

'''

from __future__ import absolute_import, division, print_function, unicode_literals

from subprocess import check_output as spcheck_output

#cmdroot = "c:\\cygwin64\\home\\ursusm\\newGcc\\hidaccess.exe"
cmdroot = "/home/ursusm/hidaccesstest"

VERSION = '0.0.1'

def getSwitch() :

    cmd = cmdroot+" r 1"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[0]
    try :
        rtnval = int(rtnval)
    except ValueError as ve :
        print("ValueError in MCSwitch.getSwitch {} -- {}".format(rtnval, repr(ve)))
        rtnval = 0
    return rtnval

def setSwitch(port) :

    cmd = cmdroot+" w "+str(port)
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    return rtnstr

def getSerialNumber() :

    cmd = cmdroot+" r 41"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[1]
    return rtnval

def getModelNumber() :

    cmd = cmdroot+" r 40"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[1]
    return rtnval

