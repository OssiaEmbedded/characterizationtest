""" SSHlink.py: A class to connect using ssh to execute remote executables

Version : 0.0.0
Date : Nov 20 2018
Copyright Ossia Inc. 2018

"""
from __future__ import absolute_import, division, print_function #, unicode_literals

import paramiko

def sshPrint(msg) :
    if SSHlink.debug :
        print(msg)

class SSHlink() :
    debug = False
    def __init__(self, hostname, hostport, username, password, idebug=False) :
                
        self.client =  paramiko.SSHClient()
        self.client.load_system_host_keys()
        #Auto add policy is not a good practice in production/untrusted env
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        
        self.hostname = hostname
        self.hostport = hostport
        self.username = username
        self.password = password
        
        self.printstdout = False
        self.commandssent = None
        self.linesreceived = None
        self.errors = None
        
        self.connected = False
        self.debug = idebug
        SSHlink.debug = self.debug

        sshPrint ("\nSSH client object created for the host {}@{}".format(self.username, self.hostname))
            
    def connect(self) :
        result_flag = True
        if not self.connected :
            result_flag = False
            try :
                sshPrint("Establishing SSH connection.")
                self.client.connect(self.hostname, self.hostport,
                             self.username, self.password)
            except paramiko.ssh_exception.NoValidConnectionsError as e :
                sshPrint('SSH transport is not ready... {}'.format(repr(e))) 
            except paramiko.AuthenticationException as e :
                sshPrint("Authentication failed, please verify your credentials {}".format(repr(e)))
            except paramiko.SSHException as sshException :
                sshPrint("Could not establish SSH connection : {}".format(repr(sshException)))
            except Exception as e :
                sshPrint("Exception in connecting to the server")
                sshPrint("PYTHON SAYS : {}".format(repr(e)))
                self.client.close()
            else :
                result_flag = True
        
        if result_flag :
            sshPrint ("\nSSH connection to {}@{} established.".format(self.username, self.hostname))
        self.connected = result_flag

        return result_flag
    
    def close(self) :
        self.client.close()
        sshPrint ("\nSSH connection terminated : {}@{} is no longer connected.".format(self.username, self.hostname))
        self.connected = False
        
    def command(self, command_str, otherpw=None) :
        #Execute command
        sshPrint("\nusername@hostname: {}@{}".format(self.username,self.hostname))
        sshPrint ("Command string: '{}'".format(command_str))

        #sudo_flag = False
        #if command_str.startswith("sudo ") and self.username!="root" :
        #    sudo_flag = True
            
        retry = 5
        while retry > 0 :
            try :
                stdin, stdout, stderr = self.client.exec_command(command_str,timeout=20)
            except paramiko.SSHException as ss :
                print("SSHException in sshcomm.command retry={} -- {}".format(retry, repr(ss)))
            except Exception as ee : 
                print("Unknown Exception in sshcomm.command -- {}".format(repr(ee)))
                return 1
            else :
                self.commandssent =  tuple([command_str])
                break
            retry -= 1

        #linesback = stdout.readlines()
        #sshPrint("".join(linesback))
        
        #Password handling this way may not be safe other than dev env (I am not an expert on cyber security)
        #if sudo_flag :
        #    stdin.write(self.password+"\n")
        #    stdin.flush()
        #    sshPrint('sudo password typed automatically')

        #if otherpw is not None  :
        #    stdin.write(otherpw+"\n")
        #    stdin.flush()
        #    sshPrint('Other password typed automatically')
            
        self.linesreceived = ()
        self.errors = ()
        try :
            self.errors = tuple(stderr.readlines()) 
        except Exception as ee :
            print("Exception on stdERR readlines -- {}".format(repr(ee)))
        try :
            self.linesreceived = tuple(stdout.readlines()) 
        except Exception as ee :
            print("Exception on stdOUT readlines -- {}".format(repr(ee)))
        
        if self.errors :
            sshPrint("ERROR:")
            sshPrint("".join(self.errors))

        sshPrint("Returned strings:")
        sshPrint("".join(self.linesreceived))
            
        return stdout.channel.recv_exit_status()
    
    def getlastreadlines(self) :
        return self.linesreceived

    def getlastreaderrors(self) :
        return self.errors

if __name__ == "__main__" :

    import sys
    from LIB.utilmod import makeListToString 

    argc = len(sys.argv)
    if argc > 1 :
        com = SSHlink(sys.argv[1], 22, "ossiadev", 'OssiaDev!', True)
        if argc > 2 :
            rtnval = 1
            lines  = ()
            errors = () 
            cmd = makeListToString(sys.argv[2:])
            print("cmd line = '{}'".format(cmd))
            cmdlst = cmd.split(';')
            print("cmdlst = '{}'".format(cmdlst))
            com.connect()
            for cmd in cmdlst :
                print("cmd = {}".format(cmd))
                rtnval += com.command(cmd)
                lines  += com.getlastreadlines()
                errors += com.getlastreaderrors()
            com.close()
            for x in lines :
                print("line = {}".format(x))
            for x in errors :
                print("error = {}".format(x))
            print("rtnval = {}".format(rtnval))




    
