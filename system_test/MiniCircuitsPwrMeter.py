# @file      MiniCircuitsPwrMeter.py
# @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
#            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
#            STRICTLY PROHIBITED.  COPYRIGHT 2023 OSSIA INC. (SUBJECT TO LIMITED
#            DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
#

import logging

try:
    import usb.core
except:
    raise RuntimeError('pyusb not installed -- resolve using the preceding instructions')

class MiniCircuits_PWR():
    """Operate a Mini Circuits Power Meter."""

    _VENDOR_ID = 0x20CE
    _PRODUCT_ID = 0x0011
    _MSG_LEN = 64
    _ENDPOINT_READ = 0x81
    _ENDPOINT_WRITE = 0x1

    _READ_TIMEOUT = 5000
    _WRITE_TIMEOUT = 500
    _LOW_NOISE_MODE = 0
    _FAST_SAMPLING_MODE = 1

    _SET_MEASUREMENT_MODE       =  15
    _GET_FIRMWARE               =  99
    _READ_POWER                 = 102
    _GET_INTERNAL_TEMPERATURE   = 103
    _GET_DEVICE_MODEL_NAME      = 104
    _GET_DEVICE_SERIAL_NUMBER   = 105

    def __init__(self):
        """Connect to the USB power meter and set it up"""
        self.dev = usb.core.find(
            idVendor=MiniCircuits_PWR._VENDOR_ID,
            idProduct=MiniCircuits_PWR._PRODUCT_ID
        )
        if not self.dev:
            raise FileNotFoundError("Power Meter not found!")
        if self.dev.is_kernel_driver_active(0):
            self.dev.detach_kernel_driver(0)
        self.dev.set_configuration()
        self.dev.reset()
        self.model = self.get_model()
        self.sernum = self.get_serial()
        self.temperature = self.get_temperature()
        self.set_meas_mode(MiniCircuits_PWR._FAST_SAMPLING_MODE)

    def _query(self, cmd):
        """Write the padded command bytes and read the response"""
        pad_len = MiniCircuits_PWR._MSG_LEN - len(cmd)
        self._write(cmd + [0 for i in range(pad_len)])
        response = self._read(MiniCircuits_PWR._MSG_LEN)
        if len(response) != MiniCircuits_PWR._MSG_LEN:
            logging.warning("Did not receive expected number of bytes")
        return response

    def _query_string(self, cmd, response_length=None):
        """Issue the provided command and return the string response from the device.
        The first 0 character indicates the end of the response
        """
        resp = self._query(cmd)
        if response_length is None:
            result = resp[1:resp.index(0)]
        else:
            result = resp[1:response_length-1]
        return ''.join([chr(i) for i in result])

    def _read(self, len):
        """Interrupt task required to read from the USB device."""
        response = []
        try:
            response = self.dev.read(
                MiniCircuits_PWR._ENDPOINT_READ,
                len,
                MiniCircuits_PWR._READ_TIMEOUT
            )
            return response
        except Exception as e:
            logging.error(f"Exception in _read: {e}")

    def _write(self, cmd):
        """Interrupt task required to write to the USB device."""
        try:
            nsent = self.dev.write(
                MiniCircuits_PWR._ENDPOINT_WRITE,
                cmd,
                MiniCircuits_PWR._WRITE_TIMEOUT
            )
            return nsent
        except Exception as e:
            logging.error(f"Exception in _write: {e}")

    def get_model(self):
        """Get the device model number as a string"""
        return self._query_string([MiniCircuits_PWR._GET_DEVICE_MODEL_NAME])

    def get_serial(self):
        """Get the device serial number as a string"""
        return self._query_string([MiniCircuits_PWR._GET_DEVICE_SERIAL_NUMBER])

    def get_temperature(self):
        """Get the internal temperature of the meter"""
        resp = self._query_string(
            [MiniCircuits_PWR._GET_INTERNAL_TEMPERATURE],
            response_length=7
        )
        return float(resp)

    def set_meas_mode(self, mode):
        """Sets the measurement mode"""
        resp = self._query([MiniCircuits_PWR._SET_MEASUREMENT_MODE, mode])
        return mode

    def get_power(self, freq_MHz):
        """Read the power meter configured for the specified frequency."""
        freq_MHz = int(freq_MHz)
        resp = self._query_string(
            [MiniCircuits_PWR._READ_POWER, freq_MHz//256, freq_MHz % 256, ord('M')],
            response_length=7
        )
        return float(resp)
