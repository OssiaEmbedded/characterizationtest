#!/bin/bash
# a script to check for diag tests still running
# and report yes for no

DIAG=`ps aux | grep diag | grep -v grep`
if [ -z $DIAG ]
then
    echo 'yes'
else
    echo 'no'
fi

