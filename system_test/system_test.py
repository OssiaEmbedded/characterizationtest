#!/usr/bin/env python3
#
# @file      system_test.py
# @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
#            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
#            STRICTLY PROHIBITED.  COPYRIGHT 2023 OSSIA INC. (SUBJECT TO LIMITED
#            DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
#

import argparse
import base64
import logging
import os
import pickle
import random
import serial
import sys
import threading
import time
import yaml
from subprocess import check_output as sp_check_output

from serial.tools.list_ports import comports
    
sys.path.append('/opt/ossia/api/eventbus')
sys.path.append('/opt/ossia/api/cotalib')
sys.path.append('/opt/ossia/api/eventbus/event/proto')

from eventbus import EventBus
from BeaconHeaderEvent import BeaconHeaderEvent
from BeaconHeader import BeaconFormat
from CprListUpdate import CprListUpdateEvent, Receiver
from FanSpeedUpdateEvent import FanSpeedUpdateEvent
from LogEvent import LogEvent
from PowerDelivery import PowerDeliveryEvent
from PowerDeliveryDone import PowerDeliveryDoneEvent
from PowerStateUpdate import PowerDeliveryState
from SystemStateUpdate import SystemStateUpdateEvent, SystemState
from TemperatureUpdateEvent import TemperatureUpdateEvent
from user_events import (
    StartCalibrationRequest, CalibrationDoneEvent,
    EnablePowerDeliveryRequest, RegisterCprIdRequest, DeregisterCprIdRequest,
    GetFileRequest, SaveFileRequest, GetFileResponse,
)
from MiniCircuitsPwrMeter import MiniCircuits_PWR as PowerMeter 
#from KeysightV3500Usb import KS_PwrMeterUsbSerial as PowerMeter

RTN_NO_PRINT_MENU = 'trick'
SCHEDULER_IDNAME = "SCHEDULER_CONFIG"
HALPROC_IDNAME   = "HALPROC_CONFIG"

class MarsSystemTest:
    """
    Mars Automated System Test
    
    Controls receiver through serial interface
    Controls power meter over USB
    Controls transmitter over EventBus
    """
    def __init__(self, config_file):
        self.config_file = config_file
        self.client_id = random.randint(1,1000000)
        self.file_response_topic = f"Config/{self.client_id}/GetFileResponse"
        self.rx_serial = None
        self.pwr_meter = None
        self.compensatedStr = ""
        self.beacon_received = False
        self.waiting_for_beacon = False
        self.tx_config = {SCHEDULER_IDNAME : {}, HALPROC_IDNAME : {}}
        self.orig_tx_config = {SCHEDULER_IDNAME : {}, HALPROC_IDNAME : {}}
        self.looping = False
        self.config_received_flag = threading.Event()
        self.cal_done_flag = threading.Event()
        self.beacon_flag = threading.Event()
        self.pod_idle_flag = threading.Event()
        self.meas_complete_flag = threading.Event()
        self.power_done_flag = threading.Event()
        self.tx_rx_frequency = 5751
        self.txrx_mode_test = None
        self.load_config()
        self.init_receiver_port()
        self.init_pwr_meter()
        self.funcIndex = 0
        self.helpIndex = 1
        self.commandDict = {
                "h"      : [self.print_menu,              "request a listing of the command menu"],
                "c"      : [self.interactiveCalibration,  "Send a Calibrate event to TX"],
                "freq"   : [self.interactiveSetFrequency, "<freq> Set the TX RX Operating frequency to freq"],
                "s"      : [self.interactiveTxRxSetup,    "Setup the TX and RX, starts from defaults."],
                "txrxm"  : [self.interactiveSetTxMode,    "<mode> Mode is either 1 or 2 for fast or slow beacons"],
                "b"      : [self.interactiveSendBeacon,   "Send a Beacon"],
                "w"      : [self.interactiveGetBatteryStats, "Get the current Battery Statistics"],
                "v"      : [self.read_battery_voltage,    "Return the current Battery Voltage"],
                "e"      : [self.enable_charging,         "Enable Charging"],
                "d"      : [self.disable_charging,        "Disable Charging"],
                "m"      : [self.measure_rf_power,        "Measure the current RF Power at the Power Meter"],
                "rest"   : [self.restore_original_config, "Restore the original TX configuration"],
                "unreg"  : [self.interactiveUnRegisterRx, "Un-Register the receiver from the TX"],
                "reg"    : [self.interactiveRegisterRx,   "Register the receiver with the TX"],
                "l"      : [self.interactiveBeaconLoop,   "<N> Send N number of beacons measuring RF power for each. Returns the power measurments"],
                "t"      : [self.interactiveSendToRx,     "<cmdstr> Send the command str to the RX Serial port return response"],
                "conf"   : [self.interactiveGetConfig,    "<app> Get the current config from halproc or scheduler (base64 encoded)"],
                }

    # Internal methods

    def _save_beacon_to_file(self, beacon_event, filename):
        '''Pickle a BeaconHeaderEvent and save it to file'''
        with open(filename, 'wb') as fp:
            pickle.dump(beacon_event, fp, pickle.HIGHEST_PROTOCOL)

    def _format_beacon_header(self, beacon_header):
        '''Specialized format for printing a Beacon Header'''
        #print(f"BEACON_HEADER = '{beacon_header}'\nFMT-- {beacon_header.format}\nVOLT-- {beacon_header.beacon_payload}")
        if beacon_header.format == BeaconFormat.BASIC:
            return "Received beacon: 0x{:x}, RSSI = {}, Voltage = {}".format(
                beacon_header.cpr_id,
                beacon_header.beacon_payload.rssi,
                beacon_header.beacon_payload.voltage
            )
        else:
            return "ERROR: Invalid beacon format received"

    def load_config(self):
        with open(self.config_file) as fp:
            self.config = yaml.safe_load(fp)
        for key,val in self.config.items():
            logging.debug(f"{key}: {val}")

    def get_rx_id_from_rx(self) :
        """ gets rxid from RX connected to serial port
            returns a 64 bit int
        """
        resp = self.send_rx_cmd("setrxid", output=False)
        resp = self.send_rx_cmd("setrxid", output=False)
        # resp should have this
        # Usage: setrxid <serial_number>
        # Current RX ID is: 0x8C1F641B-0xF000003A
        # $
        lines = resp.split('\n')
        rxid = None
        for line in lines :
            if "Current" in line :
                rxid = int(line.split(':')[1].strip().replace('-0x', ''), 0)
                break
        return rxid

    def interactiveCalibration(self) :
        self.run_calibration()
        if not self.cal_done_flag.wait(5):
            logging.error("Calibration failed to complete!")
            print("Calibration failed to complete!")
    def interactiveSetTxMode(self, **arg) :
        try :
            tx = arg.get('arg', None).strip()
            if tx in ['1', '2'] :
                self.txrx_mode_test = int(tx)
            else :
                self.txrx_mode_test = None
        except :
            self.txrx_mode_test = None
    def interactiveTxRxSetup(self, **arg) :
        self.setup_receiver()
        self.setup_transmitter()
    def interactiveSendBeacon(self) :
        self.send_beacon()
        if not self.beacon_flag.wait(5):
            logging.warning("Beacon not received")
            print("Beacon Not received")
    def interactiveUnRegisterRx(self) :
        rx_id = self.get_rx_id_from_rx()
        self.unRegisterReceiver(rx_id)
        print(f"Un-Registered RXID 0x{rx_id:X}")
    def interactiveRegisterRx(self) :
        rx_id = self.get_rx_id_from_rx()
        self.registerReceiver(rx_id)
        print(f"Registered RXID 0x{rx_id:X}")
    def interactiveSetFrequency(self, **arg) :
        if arg.get('arg', None) :
            try :
                freq = int(arg['arg'],0)
            except (ValueError, IndexError) :
                print(f"ERROR : Input for frequency is invalid {arg}")
            else :
                freq = max(5725,min(5875,freq))
                self.tx_rx_frequency = freq
                # set CCB clock gen to 1 MHz less because the DAC on the CCB is running at one MHZ and freq + 1 is the TX Freq
        print(f"TxRx Frequency == {self.tx_rx_frequency}")
    def interactiveBeaconLoop(self, **arg) :
        self.compensatedStr = ''
        if arg.get('arg', None) :
            try :
                num_loops = int(arg['arg'])
            except Exception as e :
                print(f"Looping exception {e}")
            else :
                self.looping = True
                # Enable charging
                logging.debug("Enabling charging")
                self.enable_charging()
                time.sleep(1)
                for i in range(num_loops):
                    self.run_measurement_loop(i, num_loops)
                self.looping = False
        else :
            self.compensatedStr = arg
        logging.debug(f"Loop Measurements : {self.compensatedStr}")
        print(f"Loop Measurements : {self.compensatedStr}")
        # Disable charging
        logging.debug("Disabling charging")
        self.disable_charging()
        # To trick the program Not to print the menu ...
        return RTN_NO_PRINT_MENU 
    def interactiveSendToRx(self, **arg) :
        if arg.get('arg', None) :
            print(self.send_rx_cmd(arg['arg'], output=False))
        else :
            print(f"Nothing to send {arg['arg']}")
        return RTN_NO_PRINT_MENU 
    def interactiveGetConfig(self, **arg) :
        if arg.get('arg', None) :
            if arg['arg'].lower() == 'halproc':
                file_id = HALPROC_IDNAME
            elif arg['arg'].lower() == 'scheduler':
                file_id = SCHEDULER_IDNAME
            else:
                print("Invalid config file")
                return RTN_NO_PRINT_MENU 
            self.request_tx_config(file_id)
            if self.config_received_flag.wait(5):
                print(f"{file_id}={base64.b64encode(self.orig_tx_config[file_id])}")
                self.config_received_flag.clear()
        else :
            print("Must specify halproc or scheduler")
        return RTN_NO_PRINT_MENU 
    def interactiveGetBatteryStats(self) :
        self.send_rx_battery_stats()
        return RTN_NO_PRINT_MENU 

    def getFunctionAndArg(self, commandDict, sel) :
        arg = {} # default val
        cmd = ''
        cmdarg = sel.split(maxsplit=1) # split into max two element list
        if cmdarg :
            cmd  = cmdarg.pop(0) # first element is the command name (char)
        if cmdarg :
            arg = {"arg" : cmdarg.pop(0)}
        func = commandDict.get(cmd, [None, ''])
        return func[self.funcIndex], arg

    def print_menu(self, cmdDict=None) :
        NameLen = 7 # rather then program it just count the bigest and add one.
        if not cmdDict :
            cmdDict = self.commandDict
        print("")
        fmt = "({}){:%s}-> {}"
        for k, func in cmdDict.items() :
            ffmt = fmt % (NameLen - len(k))
            print(ffmt.format(k, ' ', func[self.helpIndex]))
        return RTN_NO_PRINT_MENU 

    def interactive_mode(self, NoMenuPrint=False):
        """
        Interactive mode used for automated system testing.
        """
        sel = ''
        fd = open("./inlog.txt",'w')
        cnt=0
        tm = time.time()
        while True:
            if sel is None and not NoMenuPrint : #or not sel.startswith("t") :
                self.print_menu(self.commandDict)
            try:
                fd.write(f"{time.time()-tm:6.2f}\n")
                sel = input("Enter selection: ").lower().strip()
                fd.write(f"{cnt}, '{sel}' ")
                cnt+=1
            except KeyboardInterrupt:
                sel = "x"
            except Exception as e:
                logging.error(f"Exception - {e}")
                sel = "x"
            if sel.startswith("x") :
                logging.debug("Quitting")
                print("Quitting")
                break
            func, arg = self.getFunctionAndArg(self.commandDict, sel)
            if func :
                try :
                    tm = time.time()
                    sel = func(**arg)
                except TypeError :
                    print(f"Unexpected arg for cmd {sel}")
                    sel = None
            elif sel != '' : # if not nill then log the invalid input 
                logging.debug(f"Invalid selection {sel}")
            else : # if sel == nill then just set it to something that will cause the menu to print
                sel = None

    def run_measurement_loop(self, current_loop, num_loops):
        logging.info(f"********** Running loop {current_loop+1} of {num_loops}")
        self.meas_complete_flag.clear()
        self.power_done_flag.clear()
        self.pod_idle_flag.clear()
        self.beacon_flag.clear()
        # Check receiver battery
        receiver_batt_level = self.check_rx_battery()
        if receiver_batt_level < self.config["minimum_batt_volt"]:
            logging.error(f"Aborting test! Battery voltage: {receiver_batt_level}mV")
            return
        else:
            logging.debug(f"Receiver battery: {receiver_batt_level}mV")
        # Calibrate if needed
        if self.config["calibrate_every_loop"]:
            self.run_calibration()
            if not self.cal_done_flag.wait(5):
                logging.error("Calibration failed to complete!")
                return
            # Wait for POD_IDLE
            self.wait_for_pod_idle(20)
        # Send a beacon
        self.send_beacon()
        if not self.beacon_flag.wait(5):
            logging.warning("Did not receive beacon, skipping test")
            self.waiting_for_beacon = False
            return
        # Wait for power measurement
        logging.debug("Waiting for power measurement")
        if not self.meas_complete_flag.wait(5):
            logging.warning("Power measurement did not complete")
        # Wait for power delivery done
        logging.debug("Waiting for PowerDeliveryDone")
        if not self.power_done_flag.wait(10):
            logging.warning("Power delivery did not complete")
        # Wait for POD_IDLE
        if self.config["wait_for_pod_idle"]:
            self.pod_idle_flag.clear()
            self.wait_for_pod_idle(10)

    def main_test_loop(self, num_loops):
        self.looping = True
        logging.debug(f"Starting loop test, loops={num_loops}")
        # Setup receiver if needed
        if self.config["setup_receiver"]:
            logging.debug("Setting up receiver")
            self.setup_receiver()
        else:
            logging.debug("Skipping receiver setup")
        # Setup transmitter if needed
        if self.config["setup_transmitter"]:
            logging.debug("Setting up transmitter")
            self.setup_transmitter()
        else:
            logging.debug("Skipping transmitter setup")
        # Initial calibration if needed
        if self.config["calibrate_once"] and not self.config["calibrate_every_loop"]:
            logging.debug("Running initial calibration")
            self.run_calibration()
            if not self.cal_done_flag.wait(5):
                logging.error("Calibration failed to complete!")
                return
        # Enable charging
        logging.debug("Enabling charging")
        self.enable_charging()
        time.sleep(1)
        # Start test loops
        for i in range(num_loops):
            self.run_measurement_loop(i, num_loops)
        # Disable charging
        logging.debug("Disabling charging")
        self.disable_charging()

    # EventBus methods
    def connect_eventbus(self):
        self.eb = EventBus()
        logging.info("Connecting to localhost")
        self.eb.connect("localhost")
        logging.info("Connected")
        self.subscribe_topics()

    def on_cal_done(self, event):
        logging.info(CalibrationDoneEvent())
        self.cal_done_flag.set()

    def on_beacon_event(self, event):
        if not self.waiting_for_beacon:
            return
        ev = BeaconHeaderEvent.parse(event.payload)
        fmtBeaconHdr = self._format_beacon_header(ev.beacon_header)
        logging.info(fmtBeaconHdr)
        #print(f"BEACON HEADER EVENT '{fmtBeaconHdr}'")
        self._save_beacon_to_file(ev, "last_beacon.phases")
        self.beacon_received = True
        self.waiting_for_beacon = False
        self.beacon_flag.set()

    def on_system_state_update(self, event):
        state = SystemStateUpdateEvent.parse(event.payload).state
        logging.debug(f"In state {state}")
        if state == SystemState.POD_IDLE:
            self.pod_idle_flag.set()

    def on_file_response(self, event):
        ev = GetFileResponse.parse(self.client_id, event.payload)
        self.orig_tx_config[ev.file_id] = ev.file_contents
        self.tx_config[ev.file_id] = yaml.safe_load(ev.file_contents)
        self.config_received_flag.set()

    def on_power_delivery(self, event):
        delay = self.config["delay_before_meas"]
        ev = PowerDeliveryEvent.parse(event.payload)
        #print(f"OPD -- {ev}")
        logging.debug(ev)
        logging.debug(f"Waiting {delay}ms and then measuring power")
        time.sleep(delay/1000)
        self.measure_rf_power()

    def on_power_delivery_done(self, event):
        logging.debug(PowerDeliveryDoneEvent())
        self.power_done_flag.set()

    def subscribe_topics(self):
        subs_dict = {
            CalibrationDoneEvent.topic:   self.on_cal_done,
            BeaconHeaderEvent.topic:      self.on_beacon_event,
            SystemStateUpdateEvent.topic: self.on_system_state_update,
            PowerDeliveryEvent.topic:     self.on_power_delivery,
            PowerDeliveryDoneEvent.topic: self.on_power_delivery_done,
            self.file_response_topic:     self.on_file_response,
        }
        for topic,callback in subs_dict.items():
            self.eb.subscribe(topic, callback)

    # Receiver methods
    def init_receiver_port(self):
        port = self.config['receiver_port']
        serial_ports = comports()
        for i, p in enumerate(serial_ports) :
            if p.manufacturer is None or p.manufacturer != "FTDI" :
                continue
            thePort = f"/dev/{p.name}"
            if not os.path.exists(port):
                logging.error(f"Error: device {port} not found. Trying {thePort}")
                if not os.path.exists(thePort) :
                    logging.error(f"Error: device {thePort} not found either.")
                    return
                port = thePort
            try:
                self.rx_serial = serial.Serial(port, 115200, timeout=1)
            except Exception as e:
                logging.error(f"Exception - {e}")

    def setup_receiver(self):
        if not self.rx_serial:
            logging.error("Receiver not connected")
            return
        if self.config['set_defaults']:
            logging.debug("Setting NVM defaults")
            self.send_rx_cmd("nvmsetdefaults", output=False)
            self.send_rx_cmd("reset", output=False)
            self.read_response()
            time.sleep(10) 
        else:
            logging.warning("NOT setting defaults, test may not succeed!")

        logging.debug("Setting config modes")
        for mode in self.config["cfg_modes"]:
            self.send_rx_cmd(mode, output=True)

        logging.debug("Checking battery voltage")
        if self.check_rx_battery() < self.config["minimum_batt_volt"]:
            logging.error("Error - battery voltage too low, please charge it")

        logging.debug("Setting Beacon power")
        self.send_rx_cmd(f"trantxpwr {self.config['beacon_power']}", output=True)
        logging.debug(f"Setting Beacon Frequency to {self.tx_rx_frequency}")
        self.send_rx_cmd(f"tranfreq {self.tx_rx_frequency}", output=True)

        logging.debug("Resetting receiver")
        self.send_rx_cmd("reset", output=False)
        self.read_response()
        logging.debug("Reset complete")
        if self.txrx_mode_test :
            self.send_rx_cmd(f"opmodeset {self.txrx_mode_test}", output=False)

    def send_rx_cmd(self, cmd, output=True):
        logging.debug(f"eending command: {cmd}")
        # Clear out the receive buffer before sending a command
        self.rx_serial.read(self.rx_serial.in_waiting)
        self.rx_serial.write(f"{cmd}\r".encode())
        response = self.read_response()
        if output and not self.looping:
            logging.info(response)
            print(f"RX CMD '{cmd}' {response}")
        return response

    def read_response(self):
        response = b''
        try:
            response = self.rx_serial.read_until(b'$ ')
        except Exception as e:
            logging.warning(f"Exception reading response - {e}")
        try :
            resp = response.decode()
        except :
            resp = response
        return resp

    def send_beacon(self):
        self.beacon_flag.clear()
        if not self.rx_serial:
            logging.error("Error: must setup receiver first")
            self.beacon_flag.set()
            return
        cmd = self.config["beacon_cmd"]
        self.waiting_for_beacon = True
        self.send_rx_cmd(cmd, output=False)

    def read_battery_voltage(self):
        voltage = self.check_rx_battery()
        logging.info(f"Receiver battery voltage = {voltage} mV")
        print(f"Receiver battery voltage = {voltage} mV")

    def send_rx_battery_stats(self) :
        if not self.rx_serial:
            logging.error("Error: must setup receiver first")
            return
        cmd = self.config["batstat_cmd"]
        stats = self.send_rx_cmd(cmd, output=False)
        logging.info(f"Receiver battery stats = {stats}")
        print(f"\n{stats}\n")

    def check_rx_battery(self):
        if not self.rx_serial:
            logging.error("Error: must setup receiver first")
            return
        cmd = self.config["batstat_cmd"]
        resp = self.send_rx_cmd(cmd, output=False)
        lines = resp.split('\r\n')
        for line in lines:
            if 'Voltage' in line:
                break
        try:
            voltage = line.split(':')[1].split()[0]
            return int(voltage)
        except Exception as e:
            logging.error(f"Exception reading voltage - {e}")
            return 0

    # Transmitter methods
    def wait_for_pod_idle(self, timeout):
        logging.info("Waiting for POD_IDLE")
        status = self.pod_idle_flag.wait(timeout)
        if not status:
            logging.error("System did not return to POD_IDLE")
        else:
            logging.info("Returned to POD_IDLE")
        return status
            
    def request_tx_config(self, file_id):
        logging.info(f"Requesting current configuration for {file_id}")
        self.config_received_flag.clear()
        self.eb.publish(
            GetFileRequest(self.client_id, file_id)
        )

    def registerReceiver(self, rx_id) :
        req = RegisterCprIdRequest(rx_id)
        self.eb.publish(req)

    def unRegisterReceiver(self, rx_id) :
        req = DeregisterCprIdRequest(rx_id)
        self.eb.publish(req)

    def setup_transmitter(self):
        for config_key, config in self.orig_tx_config.items() :
            self.request_tx_config(config_key)
            if self.config_received_flag.wait(5):
                self.publish_new_tx_config(config_key)
            else:
                logging.error(f"Error! Did not receive transmitter configuration for {config_key}")

    def publish_new_tx_config(self, config_key) :
        if not config_key :
            logging.error(f"Did not receive current TX config for {config_key}")
            return
        # new stuff from 2.3.0.3 tx apps
        if config_key == SCHEDULER_IDNAME :
            for i, txm in enumerate(self.tx_config[config_key]['tx_mode_presets']) :
                if txm['name'] == 'CUSTOM' and txm['editable'] :
                    break
            if self.txrx_mode_test :
                self.tx_config[config_key]['tx_mode'] = self.txrx_mode_test
            else :
                self.tx_config[config_key]['tx_mode'] = i+1
                self.tx_config[config_key]['tx_mode_presets'][i]['service_any_beacon']   = self.config['service_any_beacon']
                self.tx_config[config_key]['tx_mode_presets'][i]['silent_duration']      = self.config['silent_duration']
                self.tx_config[config_key]['tx_mode_presets'][i]['power_frame_duration'] = self.config['power_frame_duration']

            print(f"TX Config {config_key}   service_any_beacon={self.tx_config[config_key]['tx_mode_presets'][i]['service_any_beacon']}")
            print(f"TX Config {config_key}      silent_duration={self.tx_config[config_key]['tx_mode_presets'][i]['silent_duration']}")
            print(f"TX Config {config_key} power_frame_duration={self.tx_config[config_key]['tx_mode_presets'][i]['power_frame_duration']}")
            print(f"TX Config {config_key}              tx_mode={self.tx_config[config_key]['tx_mode']}")
        elif config_key == HALPROC_IDNAME :
            self.tx_config[config_key]['rx_freq_mhz'] = self.tx_rx_frequency 
            self.tx_config[config_key]['beacon_reception_mode'] = 1
            print(f"TX Config {config_key} rx_freq_mhz={self.tx_config[config_key]['rx_freq_mhz']}")
            sp_check_output(f"python3 /usr/sbin/clki2c.py --clkfreq {self.tx_rx_frequency - 1}", shell=True)

        # send the modified tx_config back to the TX
        req = SaveFileRequest(
            self.client_id,
            config_key,
            yaml.dump(self.tx_config[config_key]).encode()
        )
        logging.info(f"Publishing new transmitter configuration for {config_key}")
        self.eb.publish(req)

    def restore_original_config(self):
        for config_key, config in self.orig_tx_config.items() :
            if not config :
                logging.error("No configuration has been received for {config_key}!")
                return
        # send the original tx_config back to the TX
            req = SaveFileRequest(self.client_id, config_key, config)
            self.eb.publish(req)

    def run_calibration(self):
        self.cal_done_flag.clear()
        ev = StartCalibrationRequest()
        logging.info(f"Publishing event {ev}")
        self.eb.publish(ev)
        logging.info("Requested calibration, please wait!")

    def enable_charging(self):
        ev = EnablePowerDeliveryRequest(PowerDeliveryState.POWER_ENABLED)
        logging.info(f"Publishing event {ev}")
        self.eb.publish(ev)

    def disable_charging(self):
        ev = EnablePowerDeliveryRequest(PowerDeliveryState.POWER_DISABLED)
        logging.info(f"Publishing event {ev}")
        self.eb.publish(ev)

    # Power Meter methods
    def init_pwr_meter(self):
        try:
            self.pwr_meter = PowerMeter()
        except Exception as e:
            logging.error(f"Exception - {e}")
            
        else:
            logging.info(f"Model: {self.pwr_meter.model}")
            logging.info(f"Serial Number: {self.pwr_meter.sernum}")
            logging.info(f"Temperature: {self.pwr_meter.temperature}")

    def measure_rf_power(self):
        freq = self.tx_rx_frequency # self.config["transmit_frequency"]
        offset = self.config["measurment_offset"]
        logging.info(f"Measuring RF power at {freq} MHz")
        #print(f"Measuring RF power at {freq} MHz")
        if not self.looping :
            self.compensatedStr = ''
        if not self.pwr_meter:
            logging.warning("No power meter is connected, skipping measurement")
            print("No power meter is connected, skipping measurement")
            if self.looping:
                self.meas_complete_flag.set()
            return
        try:
            #tm = time.time()
            meas = self.pwr_meter.get_power(freq)
            #tm1 = time.time()
            #print(f"Measure time == {tm1-tm}")
        except Exception as e:
            logging.error(f"get_power -- Exception - {e}")
        else:
            compensated = round(meas+offset, 2)
            self.compensatedStr += f"{compensated},"
            logging.info(f"Measured power: raw={meas}, compensated={compensated}")
            if self.looping:
                self.meas_complete_flag.set()
            else :
                print(f"Measured Power: {self.compensatedStr}")

def main():
    hostName = os.uname().nodename
    parser = argparse.ArgumentParser(description=f'Mars System Test on {hostName}')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-i', '--interactive', action='store_true',
                        help="Run in menu driven interactive mode")
    group.add_argument('-l', '--loops', type=int, nargs=1,
                        help="Run test in looping mode")
    parser.add_argument('-c', '--config', default='system_test.yaml',
                        help='Config file to use (default = system_test.yaml)')
    parser.add_argument('-f', '--logfile', default=f'system_test_{hostName}.log',
                        help='Output file for saving results (default = system_test.log)')
    parser.add_argument('-n', '--nologging', action='store_true', default=False,
                        help='Turn off/filter logging. Used in automation scripts')
    parser.add_argument('-d', '--debugging', action='store_true', default=False,
                        help="Set log level to debug else it's at warn")

    args = parser.parse_args()

    handlerLst = [logging.FileHandler(args.logfile), logging.StreamHandler()] if not args.nologging else [logging.FileHandler(args.logfile)]
    loglevel = logging.WARN if not args.debugging else logging.DEBUG

    logging.basicConfig(
        format='%(asctime)s.%(msecs)03d - %(funcName)s - %(levelname)s - %(message)s',
                datefmt='%Y-%m-%d-%H:%M:%S',
                level=loglevel,
                handlers=handlerLst,
    )

    if not os.path.exists(args.config):
        logging.error("Unable to open config file")
        return

    tester = MarsSystemTest(args.config)
    try:
        tester.connect_eventbus()
    except Exception as e:
        logging.error(f"Exception in connect_eventbus - {e}")
        return

    if args.interactive:
        tester.interactive_mode(args.nologging)
    elif args.loops:
        tester.main_test_loop(args.loops[0])
    else:
        logging.error("Please run in interactive (-i) or looping (-l) mode")

if __name__ == "__main__":
    main()
