import serial
from serial.tools.list_ports import comports
import math
from time import sleep, time
from random import random
import os
import platform
import sys
import pylib.LIB.actuator as actip

windoz = platform.system() == "Windows"

version = "1.0.0"

TX_hostname_possibilities = ['mars-tx', 't4-a', 't4-b', 't4-c', 't5-a']
##### BEGINNING OF HELPER METHODS #########
# check for serial ports
# a tx port and an actuator port

def DECODEBYTES(bytestr) :
    if isinstance(bytestr, bytes) :
        gg = b''
        for dd in bytestr :
           gg += (dd&0x7f).to_bytes(1, sys.byteorder)
        return gg.decode('utf-8')
    else :
        return bytestr
        

def checkTxHostname(resp, typ=False) :
    resp = DECODEBYTES(resp)
    rtnval = False
    for tx in TX_hostname_possibilities :
        if not typ :
            if tx in resp :
                rtnval = True
                break
        else :
            if resp.startswith(tx) :
                rtnval = True
                break
    return rtnval

def getSerialPorts(force=False) :
    """ find the open port and check to see if any match the
        below criteria.
        A TX port should return a value that's in TX_hostname_possibilities
        The actuator should return an "ossiabbbn" type string
    """
    serial_ports = comports()
    tx_serial = None
    act_serial = None
    for i, p in enumerate(serial_ports) :
        if p.manufacturer is None or p.manufacturer != "FTDI" :
            continue
        thePort = f"/dev/{p.name}"
        if windoz :
            thePort = f"{p.name}"
        s = SerialCommands(inport=thePort, expectVal=' ', timeoutin=0.5)
        s.serial.write(b"\r")
        s.serial.write(b"\r")
        resp = s.serial.readall()
        try :
            resp = DECODEBYTES(resp)
        except :
            print(f"{thePort} Not responding as expected. Maybe restart the BBB??\nButton near the pcb edge near LEDs")
        else :
            #print(f"get serial ports --- port {thePort} --- resp {resp}")
            if '# ' in resp :
                s.serial.write(b"exit\r")
                s.serial.readall()
                #print(f"Exit from login {thePort}")
        s.serial.close()
        del s
        if checkTxHostname(resp, typ=False) :
            tx_serial = thePort 
        elif "ossiabbb" in resp :
            act_serial =  thePort
        if tx_serial and act_serial :
            break
    return tx_serial, act_serial

##### END OF HELPER METHODS #########

##### BEGINNING OF STUB CLASS #########
class StubClass() :
    def __init__(self) :
        self.timeout = 0
        self.is_open = True
        self.in_waiting = 0
    def close(self, val='') :
        return b''
    def write(self, val='') :
        return 0
    def readall(self, val='') :
        return b''
    def readline(self, val='') :
        return b'' 
    def read_until(self, val='') :
        return b'' 
    def read(self, val='') :
        return b''
##### END OF STUB METHODS #########
##### BEGINNING OF BASE CLASS #########
class SerialCommands() :

    def __init__(self, inport, expectVal='none', timeoutin=1.4) :

        self.expectVal = expectVal.encode('utf-8')
        self.isStub = False
        try :
            if inport is None :
                raise serial.serialutil.PortNotOpenError
            self.serial = serial.Serial(port=inport, baudrate=115200, timeout=timeoutin)
        except Exception as ee :
            print(f"Exception in serial connection to {inport} creating stub == {ee}")
            self.serial = StubClass()
            self.isStub = True
        else :
            if not self.serial.is_open :
                print(f"Could not open device {inport}")

    def __del__(self) :
        if self.serial and self.serial.is_open :
            self.serial.close()

    def sendReboot(self) :
        self.serial.readall()
        self.serial.write(b"\r\r reboot \r\r")

    def sendExit(self) :
        self.serial.readall()
        self.serial.write(b"\r\r exit \r\r")

    def sendShutdown(self) :
        self.serial.readall()
        self.serial.write(b"\rshutdown -h now\r")

    def checkForLogin(self) :
        rtnval = False
        self.serial.write(b'\r')
        self.serial.read(self.serial.in_waiting)
        sleep(0.5)
        self.serial.write(b'\r')
        for _ in range(2) :
            v = self.serial.readline()
            #print(f"Check for login = {v}")
            if b'login:' in v :
                rtnval = True
                break
        #self.serial.readall()
        return rtnval

    def checkForLoggedIn(self) :
        rtnval = False
        self.serial.write(b'\r')
        for _ in range(3) :
            v = self.serial.readline()
            #print(f"Check for logged in = {v}")
            rtnval = checkTxHostname(v, typ=False)
            if rtnval :
                break
        self.serial.readall()
        return rtnval

    def sendUsernamePassword(self, username, password=None) :
        self.serial.write(username.encode('utf-8')+"\r".encode('utf-8'))
        sleep(0.5)
        if password and len(password) > 0 :
            self.serial.write(password.encode('utf-8')+"\r".encode('utf-8'))
            sleep(2.5)
            self.serial.write("\r".encode('utf-8'))
        r=self.serial.read_until(self.expectVal)
        try :
            #r = r.decode().strip()
            r = DECODEBYTES(r).strip()
        except Exception as ee :
            print(f"sendUsernamePassword -- {repr(r)} {ee}\nexpectval = {self.expectVal}\n") 
        return r # .decode().strip()

    def _getSerialDataWithNotify(self, notifyFunc=None, howfarloops=None) :
        maxFunc = 101
        if notifyFunc is not None and howfarloops is not None :
            if howfarloops == 0 :
                howfarloops = maxFunc - 1
            nFunc, maxFunc = notifyFunc
            funcinc = maxFunc // howfarloops
        data = b''
        cnt = 100
        funccnt = 0
        while cnt > 0 :
            d = b''
            if self.serial.in_waiting :
                d = self.serial.read(self.serial.in_waiting)
                data += d
            else :
                cnt -= 1
                sleep(0.01)
            if d != b'' and notifyFunc is not None :
                nFunc(funccnt)
                funccnt += funcinc
            try :
                if DECODEBYTES(data).endswith(DECODEBYTES(self.expectVal)) :
                    break
            except :
                print(f"\nData Not Decoded ''' {data} '''\n")
                if DECODEBYTES(self.expectVal) in f"{data}" :
                    print("breaking on f'{data")
                    break
        if cnt < 1 :
            print(f"CNT expired")
        try :
            datastr = DECODEBYTES(data).lstrip().rstrip()
        except :
            datastr = f"{data}"
        if notifyFunc is not None :
            nFunc(maxFunc)
        return datastr

    def writeReadUntil(self, cmd, expVal=None, longread=None) :
        """ takes a string
            returns a string not bytes
        """
        ts = 0
        expectVal = self.expectVal
        if expVal :
            expectVal = expVal.encode('utf-8')
        if longread is not None :
            ts = self.serial.timeout
            self.serial.timeout = longread
            self.serial.write("\r".encode('utf-8'))
            sleep(0.1)
        self.serial.read(self.serial.in_waiting)
        cmd += '\r'
        self.serial.write(cmd.encode('utf-8'))
        data = self.serial.readline()
        data += self.serial.read_until(expectVal)
        #print(f"{expectVal} -- {cmd.strip()} == {data}\n")
        data =  DECODEBYTES(data)
        if longread is not None :
            self.serial.timeout = ts
            self.serial.write("\r".encode('utf-8'))
            sleep(0.1)
            self.serial.read(self.serial.in_waiting)
        return data.strip(expectVal.decode('utf-8')).strip()

    def printCommand(self, data) :
        """ just print the data
        """
        print(f"{data.strip()}")

##### END OF BASE CLASS #########

##### BEGINNING OF ACTUATOR CLASS #########
class Actuator(SerialCommands) :

    def __init__(self, inport, expectVal='# ') :

        self.serial     = None
        super(Actuator, self).__init__(inport)
        self.expectVal = expectVal.encode('utf-8')

        self.NET = False

    def sendExit(self) :
        if not self.NET :
            self.serial.write(b"\r\r exit \r\r")

    def getIpAddress(self) :
        myipcmd="ifconfig | grep '\<inet\>' | awk '{print $2}' | grep '10.10'\r" # assumed local area network subnet
        rval = self.writeReadUntil(myipcmd)
        rvallst = rval.split('\n')
        retv = None
        for rv in rvallst[1:] :
            if "10.10" in rv :
                retv = rv
                self.sendExit()
                self.NET = True
                break
        self.netIpAddress = retv.strip()

    def sendNetCommand(self, cmdstr) :
        args = cmdstr.split(' ')[1:]
        #print(f"sendNetCommand {args} {self.netIpAddress}")
        silence = False
        nowait  = False

        if len(args) > 1 :
            if args[0] == '-s' :
                silence = True
                args.pop(0)
            if args[0] == '-nw' :
                nowait = True
                args.pop(0)
            if args[0] == '-s' : # easily allows order independence
                silence = True
                args.pop(0)
        else :
            return None
        
        a = actip.Actuator(self.netIpAddress, 7000)
        for i in range(2, len(args), 1) :
            args[i] = args[i].replace('\\', '')

        strval = a.makeCommandStringFromList(args)
        retstr = ""
        if not silence :
            retstr += "CMD={}\n".format(strval)
        a.sendData(strval)
        cnt = 60
        data = b""
        while not nowait and a.testSock() is not None and cnt > 0 :
            d =  a.getData(nonBlock=True)
            if len(d) == 0 :
                cnt -= 1
                if not silence :
                    retstr += "  -- {}\n".format(cnt)
            else :
                data += d
        if silence or nowait :
            data = DECODEBYTES(data).replace("SUCCESS", '').strip()
        if not nowait :
            retstr += "\n{}".format(data)
        #print(f"retstr == {retstr}")
        return retstr

    def getLinOffset(self) :
        val = -1
        cmd = f"sendspi -s getLinearOffset mc1"
        if self.NET :
            rval = self.sendNetCommand(cmd)
        else :
            rval = self.writeReadUntil(cmd)
        if "Linear offset" in rval :
            val = int(rval.split('=')[-1].split()[0].strip())
        else :
            rval_err = f"LIN OFF ERROR {rval}"
            val = 0
        return val

    def setLinOffset(self, linearoffset) :
        retval = False
        cmd = f"sendspi -s setLinearOffset {linearoffset} mc1\r"
        if self.NET :
            rval = self.sendNetCommand(cmd)
        else :
            rval = self.writeReadUntil(cmd)
        if "DONE" in rval :
            retval = True
        return retval

    def sendPosRot(self, position, setPosNotifyFunc=None) :
        cnt = None
        if setPosNotifyFunc :
            val = self.getRotPosition() 
            rot = float(position)
            cnt = int(round(math.fabs(val-rot) / 13.3)) + 1
        cmd = f"sendspi setPosition {position} rot\r" 
        if self.NET :
            retval = self.sendNetCommand(cmd)
        else :
            self.serial.read(self.serial.in_waiting)
            self.serial.write(cmd.encode('utf-8'))
            retval = self._getSerialDataWithNotify(setPosNotifyFunc, cnt)
            if "command not found" in retval.lower() :
                # send a control c
                #print(f"ROT CNF {retval}")
                self.serial.write(b'\x03')
                self.serial.write(b'\r')
                self.serial.readall()
                # resend the command
                self.serial.write(cmd.encode('utf-8'))
                retval = self._getSerialDataWithNotify(setPosNotifyFunc, cnt)
        return retval

    def sendPosLin(self, position, setPosNotifyFunc=None) :
        cnt = None
        if setPosNotifyFunc :
            linos = self.getLinOffset()
            val = self.getLinPosition()
            cnt = ((val-linos) // 85) + 1
        cmd = f"sendspi setPosition {position} mc1\r" 
        if self.NET :
            retval = self.sendNetCommand(cmd)
        else :
            self.serial.read(self.serial.in_waiting)
            self.serial.write(cmd.encode('utf-8'))
            retval = self._getSerialDataWithNotify(setPosNotifyFunc, cnt)
            if "command not found" in retval.lower() :
               #print(f"LIN CNF {retval}")
                # send a control c
                self.serial.write(b'\x03')
                self.serial.write(b'\r')
                self.serial.readall()
                # resend the command
                self.serial.write(cmd.encode('utf-8'))
                retval = self._getSerialDataWithNotify(setPosNotifyFunc, cnt)
        return retval

    def _getPosition(self, mc) :
        val = -1
        cmd = f"sendspi -s getPosition {mc}\r" 
        if self.NET :
            rval = self.sendNetCommand(cmd)
        else :
            rval = self.writeReadUntil(cmd)
        if "Get Position" in rval :
            val = rval.split('=')[-1].split()[0].strip()
            val = float(val) if mc.lower() == 'rot' else int(val) 
        else :
            print(f"_getPosition cmd={cmd.strip()}\n###'{rval}'###")
        return val

    def getLinPosition(self) :
        return self._getPosition('mc1')
        
    def getRotPosition(self) :
        return self._getPosition('rot')
##### END OF ACTUATOR CLASS #########
 
##### BEGINNING OF TRANSMITTER CLASS #########
class Transmitter(SerialCommands) :

    def __init__(self, inport, expectVal='# ') :

        self.serial     = None
        super(Transmitter, self).__init__(inport)
        self.expectVal = expectVal.encode('utf-8')
        self.isMounted     = False
        self.deviceToMount = None
        self.mntPoint      = '/mnt/'

    def lsblkCommand(self) :
        """ finds the device name for the first partition of the sd[a,b,c,d, etc] device
            by eliminating those that start with mccb or disk
            data is the response from the "lsblk -r" command
        """

        # data is a CR delimited set of lines so split it on \n
          # NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
          # sda 8:0 1 1004.5M 0 disk 
          # sda1 8:1 1 1004.5M 0 part 
          # mmcblk1 179:0 0 3.6G 0 disk 
          # mmcblk1p1 179:1 0 3.6G 0 part /
          # mmcblk1boot0 179:8 0 2M 1 disk 
          # mmcblk1boot1 179:16 0 2M 1 disk 

        # send command
        cmd = "lsblk -r\r"
        data = self.writeReadUntil(cmd)
        #print(f"DATASTART\n{data}\nDATAEND\n")

        # process return
        dataLst = data.split('\n')
        for dev in dataLst :
            #print(f"lsblk dev ====> {dev}")
            if "disk" in dev or "mmc" in dev or 'part' not in dev :
                continue
            d = dev.split(' ')
            self.deviceToMount = f"/dev/{d[0].strip()}"
            if "/" in d[-1] :
                self.isMounted = True
                self.mntPoint = d[-1].strip()
            break
        #print(f"lsblk -r =>\ndeviceToMount={self.deviceToMount}\nisMounted={self.isMounted}\nmntPoint={self.mntPoint}")
        return True

    def isTxSwRunning(self) :
        """ send commands to determine if the Application software is up and running
            supervisorctl status | grep -c RUNNING > 6
        """
        num = 0
        rtnval = False
        cmd = "supervisorctl status | grep -c RUNNING"
        datalst = self.writeReadUntil(cmd, longread=2.5).split() #, longread=2.5)
        for data in datalst :
            try :
                num = int(data.strip())
            except :
                pass
            else :
                if num > 7 :
                    rtnval = True
                    break
        return rtnval
        

    def umountCommand(self) :
        """ unsets the isMounted flag
        """
        if not self.mntPoint :
            return False
        # send command to un-mount mounted device
        cmd = f"umount {self.mntPoint}\r"
        data = self.writeReadUntil(cmd)
        dataLst = data.split('\n')
        for dev in dataLst[1:] :
            #print(f"umount dev ====> {dev}")
            if not dev.startswith('umount') :
                self.isMounted = False
            elif dev.startswith('umount') and "not mounted" in dev :
                #umount: /mnt: not mounted.
                self.isMounted = False
        #print(f"umount => isMounted = {self.isMounted}")
        return True

    def mountCommand(self) :
        """ sets the isMounted flag
        """
        cmd = f"mount {self.deviceToMount} {self.mntPoint}\r"
        data = self.writeReadUntil(cmd)
        #print(f"DATA mount = {data}")
        dataLst = data.split('\n')
        for dev in dataLst[1:] :
            if not dev.startswith('mount') :
                self.isMounted = True
            elif dev.startswith('mount') and "mounted" in dev :
                #mount: /mnt: /dev/sda1 already mounted on /mnt.
                self.isMounted = True
            elif dev.startswith('mount') and "not exist" in dev :
               # mount: /mnt: special device /dev/sda2 does not exist.
                self.deviceToMount = None
        #print(f"mount => isMounted = {self.isMounted}")
        return True
    
    def getVersions(self) :
        osVersion = "Not Found"
        swVersion = "Not Found"
        buildId   = "Not Found"
        osVersionlst=self.sendArbCommand("cat /etc/os-release | grep BUILD_VERSION").split('\n')
        for osv in osVersionlst :
            if "BUILD_VERSION=" in osv :
                osVersion = osv.split('=')[-1].strip('\n\r" ')
                break
        buildIdlst=self.sendArbCommand("cat /etc/os-release | grep BUILD_ID").split('\n')
        for osv in buildIdlst :
            if "BUILD_ID=" in osv :
                buildId = osv.split('=')[-1].strip('\n\r" ')
                break
        swVersionlst=self.sendArbCommand("cat /opt/ossia/include/marstx_version.h | grep 'MARSTX_VERSION '").split('\n')
        for swv in swVersionlst :
            if "MARSTX_VERSION" in swv and '=' in swv :
                swVersion = swv.split('=')[-1].strip('\r\n ;"')
                break
       #print(f"osVersion === {osVersion}\n {osVersionlst}\n")
       #print(f"swVersion === {swVersion}\n {swVersionlst}\n")
        return osVersion, swVersion, buildId

    def getHostname(self) :
        cmd = "cat /etc/hostname\r"
        data = self.writeReadUntil(cmd)
        dlst = data.split('\n')
        brknow = False
        for v in dlst : # data loop
            # hostname possibility loop
            if checkTxHostname(v, typ=True) :
                data = v.strip()
                break
        return data

    def sendArbCommand(self, cmdstr, expVal=None, longread=None) :
        """ takes a string executes it then reads the response and
            returns a string
        """
        cmd = f"{cmdstr}"
        data = self.writeReadUntil(cmd, expVal, longread)
        return data

##### END OF TRANSMITTER CLASS #########

if __name__ == '__main__' :

    # this unit test is specific to a BBB Serial port attached and a CCB

    for i in range(10) :
        txPort, actPort =  getSerialPorts()
        print(f"{i}  txp={txPort}  act={actPort}\n")
        if not txPort and not actPort :
            sleep(4)
        else :
            break

    if actPort :
        ss = Actuator(actPort, '# ')
        if ss.serial.is_open and ss.checkForLogin() :
            print(ss.sendUsernamePassword('root'))
            ss.getIpAddress()
            print(f"\nIP ADDR == {ss.netIpAddress} &&&\n")
            ss.NET = False
            ss.serial.write("\r".encode('utf-8'))

        for x in range(100) :
            tm = time()
            v = ss.sendPosLin(0)
            print(f"POSLIN = {time()-tm:5.3f}")
            print(f"  -A-- {v.split()[-2]} &&&\n")
            tm = time()
            v = ss.sendPosRot(0)
            print(f"POSROT = {time()-tm:5.3f}")
            print(f"  -B-- {v.split()[-2]} &&&\n")
            tm = time()
            v = ss.getLinPosition()
            print(f"GETPOSLIN = {time()-tm:5.3f}")
            print(f"  -C-- Get POS Lin {v} &&&\n")
            tm = time()
            v = ss.getLinOffset()
            print(f"GETLINOFF = {time()-tm:5.3f}")
            print(f"  -D-- Get Lin {v} &&&\n")
            print(f"{x} EEEEEE\n")
            #try :
                #input("\nLook and CR")
            #except :
                #break
        ss.sendExit()
    """
    print(f"Lin {ss.getLinPosition()}")
    print(f"Rot {ss.getRotPosition()}")
    print(f"OS  {ss.getLinOffset()}")
    ss.sendExit()
    """

    if txPort :
        st = Transmitter(txPort)
        if st.serial.is_open and st.checkForLogin() :
            unpd=st.sendUsernamePassword('root', 'OssiaMars!')
            #print(f"unpd={list(unpd)}")
            print(f"unpd={unpd}")
            osVersion=st.sendArbCommand("cat /etc/os-release | grep BUILD_VERSION | cut -d '=' -f 2").split("\n")[-2].strip('\r\n ;"')
            print(f"OS={osVersion}")
            st.sendArbCommand("exit")
    #lsblk=st.lsblkCommand()
    #mntd=st.mountCommand()
    #mntd=st.umountCommand()
    #lsblk=st.lsblkCommand()
    #osVersion=st.sendArbCommand("cat /etc/os-release | grep BUILD_VERSION | cut -d '=' -f 2").split("\n")[-2].strip('\r\n ;"')
    #swVersion=st.sendArbCommand("cat /opt/ossia/include/marstx_version.h | grep 'MARSTX_VERSION ' | cut -d '=' -f 2").split("\n")[-2].strip('\r\n ;"')
    #osVersion, swVersion = st.getVersions()
    #print(f"OS={osVersion}")
    #print(f"SW={swVersion}")
    #hn = st.getHostname()
    #print(f"HN={hn}")
    #st.sendExit()

