#! /usr/bin/env python3

r"""
THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2022 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
"""

import base64
import PySimpleGUI as sg
from time      import sleep, time, localtime, strftime
from threading import Thread, Lock, Event
import os
import glob
import sys
import platform
import screeninfo
import SerialTxAct as STA
import parseLogFile as plf
#######################################################
# sg size calc methods

version = '1.0.1'

#m=Monitor(x=0, y=0, width=1792, height=1120, width_mm=None, height_mm=None, name=None, is_primary=True)
#m=Monitor(x=1792, y=40, width=1920, height=1080, width_mm=None, height_mm=None, name=None, is_primary=False)

ms = screeninfo.get_monitors()
screenX = [None for _ in range(len(ms))]
screenY = [None for _ in range(len(ms))]
index = 1
for m in ms :
    if m.is_primary :
        #            first x   last x
        screenX[0] = (m.x,      m.width+m.x)
        #            first y    last y
        screenY[0] = (m.y,      m.height+m.y)
    else :
        screenX[index] = (m.x, m.width+m.x)
        screenY[index] = (m.y, m.height+m.y)
        index +=1

def pixelsWFromChars(font, chars) :
    fontpixels = sg.Text.char_width_in_pixels(font)
    pixels = chars * fontpixels
    return pixels
def pixelsHFromChars(font, chars) :
    fontpixels = sg.Text.char_height_in_pixels(font)
    pixels = chars * fontpixels
    return pixels

def charsWFromPixels(font, pixels) :
    """ calculate the number of characters from the font size and 
        size in pixels
        returns num of chars wide X direction
    """
    fontpixels = sg.Text.char_width_in_pixels(font)
    chars = pixels//fontpixels
    return chars
def charsHFromPixels(font, pixels) :
    """ calculate the number of characters from the font size and 
        size in pixels
        returns num of chars high Y direction
    """
    fontpixels = sg.Text.char_height_in_pixels(font)
    chars = pixels//fontpixels
    return chars

#######################################################
# Button Name and text constants
#
SCHEDULEBTN      = "Schedule"
SCHEDULENOWBTN   = "End Wait and Start"
TIMETOSTARTFMT   = "{} -- Time left {}:{}:{:02}"

CONNECTBTN       = "Connect"
UNCONNECTBTN     = "Disconnect"

OPENLOGBTN       = "Open Log"
CLOSELOGBTN      = "Close Log"

STARTSYSTESTBTN  = "Start System Test"
STOPSYSTESTBTN   = "Stop System Test"

HOMEACTUATORBTN  = "Home Actuator"

SETLINOFFSETBTN  = "Set Linear Offset"

SELECTTESTFILE   = 'Test Script Select'
STARTTESTBTN     = 'Start Test'
STOPTESTBTN      = 'Stop Test'

PLOTLOGBTN       = 'Plot Log'
CLOSEPLOTLOGBTN  = 'Close Plot'

HELPOUTPUTNAME   = 'Help/Instructions'
RXTALKNAME       = 'Send commands to the RX'

SETPOSITIONBTN   = "Set Position"
GETPOSITIONBTN   = "Get Position"
LOGNOTESNAME     = "Log Notes"

BTNSIZE = max([len(CONNECTBTN), len(OPENLOGBTN), len(HOMEACTUATORBTN), len(SETLINOFFSETBTN), len(STARTTESTBTN), len(SETPOSITIONBTN), len(STARTSYSTESTBTN)]) + 1
######################################################
# Informational text constants (some use the constants above)
#
LOGNOTES_TT = """Right click to get antenna type selection.
"""
SCHEDULE_MSG = "Enter Hours:Mins to wait before starting."
SCHEDULE_MSG_ERR = "Invalid time. Ex.H:M, 4:51"

SHUTDOWNTHETXTEXT = """
Would you like to shutdown the Transmitter?

If you don't shut it down now you can use PuTTy to
connect and shutdown later, outside of the Application.
You may need to use Device Manager to determine the
COM Port for the TX."""

STUB_MESG = """
</red,bold/>The {0} Serial port{1} could not be found, so {2} running in stub mode. />
"""

LINEAROFFSETCHECK = 'Set the Linear Offset to:\n{}\nPlease Verify.'

CONNECT_STEP = """
        Welcome to the
</bold/>Characterization Graphical User Interface!/>.

The first step is to connect to the </italic/>TX and Actuator/> by selecting the </bold/>'{}'/> button.
""".format(CONNECTBTN)

OPEN_LOG_STEP = """
Open a log file to collect the results reported by the Control Script.

Simply select </bold/>{0}/> to open the default name or enter the desired name then select </bold/>{0}/>.
""".format(OPENLOGBTN)

HOME_STEP = """
Send the linear actuator to the Home position by selecting the
</bold/>'{0}'/> button. 

The bottom controls can be used now to get the current position or set an arbitrary position on either of the two actuators.
If no check boxes are selected the </bold/>'{1}'/> button will get the current location of each actuator.

Select a check box </bold/>(mutually exclusive)/> to enter and set a position on an actuator using the </bold/>'{2}'/> button.
""".format(HOMEACTUATORBTN, GETPOSITIONBTN, SETPOSITIONBTN)

LINEAR_OFFSET_STEP = """
Measure the distance from the </bold,blue/>Receiver/Antenna/> and the </bold,blue/>Face/> of the Transmitter under test.

Enter that value into the </bold/>'Linear Offset'/> input box and click </bold/>{}/>.""".format(SETLINOFFSETBTN)

# text for a blocking pop up
TESTEXECUTECHECK = """Select OK to continue with this test
Select Cancel to reject."""

SETOPFREQUENCY_STEP = """
Select the operating frequency. Pick a frequency that is at least 25 MHz from the other
frequencies that are being used around this test system.
"""
STARTSYSTEST_STEP = """
Select the </bold/>{}/> button. Starts up the system test script that runs on the TX.

This script interacts with the TX and RX to perform Beacon Transmit cycles and collect power meter data.
""".format(STARTSYSTESTBTN)
SETDELAYTEST_STEP = """
Now is the opportunity to set a Test Start delay.

Set an optional delay in "hours:minutes" in the provided pop up window. Select </italic/>Cancel/> if no delay is desired.

The wait time will begin when the </bold/>{}/> button is selected.
""".format(STARTTESTBTN)
SETDELAYTIMEDEFAULT="""Schedule a delayed start."""

theFont = ('Courier', 18)
if platform.system() == "Linux" :
    theFont = ('Courier', 12)
elif platform.system() == "Windows" :
    theFont = ('Courier', 9)

#######################################################
icon_file = "./cota.png"
with open(icon_file, "rb") as image_file:
    encoded_icon = base64.b64encode(image_file.read())

sg.theme('Default1')   # Add a touch of color
sg.SetOptions(icon=encoded_icon)

# All the stuff inside your window.
WIN_X  = 1200 if screenX[0][1] > 1220 else int(screenX[0][1] * 0.9) # pixels wide
WIN_Y  = 900  if screenY[0][1] > 920 else int(screenY[0][1] * 0.8) # pixels high
WIN_XY = (WIN_X, WIN_Y) # size as a tuple

SETPOSPROGBARLEN = 30

BUTTON_COLOR          = ('black',   '#EEEEEE') # black on gray
NEXT_BUTTON_COLOR     = ('green',   '#EEEEEE') # green on gray
DISABLED_BUTTON_COLOR = ('#AAAAAA', '#EEEEEE') # gray on gray
FRAME_TITLE_COLOR     = '#004F00' # darkish green
FRAME_TITLE_LOCATION  = sg.TITLE_LOCATION_TOP

CommonButtonDef = {
                   'button_color'          : BUTTON_COLOR,
                   'mouseover_colors'      : BUTTON_COLOR,
                   'highlight_colors'      : BUTTON_COLOR,
                   'disabled_button_color' : DISABLED_BUTTON_COLOR,
                   'use_ttk_buttons'       : True,
                   's'                     : BTNSIZE, # use the alias for size so individual sizes can be changed by setting size
                   'enable_events'         : True,
                   }
CommonMultiLineDef = {
                      'write_only'   : True,
                      'no_scrollbar' : False,
                      'auto_refresh' : True,
                      'autoscroll'   : True,
                      'horizontal_scroll' : False,
                     }
CommonInputDef = {
                   'readonly'                 : False,
                   'text_color'               : "black",
                   'use_readonly_for_disable' : False,
                   'do_not_clear'             : True,
                 }
TAB_COLOR_BG_SELECT    = '#EEEEEE' # keep the backgrounds the same
TAB_COLOR_TITLE_SELECT = '#000000' # only change the (Words, foreground, title)
TAB_COLOR_TITLE_UN     = '#777777'
TAB_COLOR_BG_UN        = '#cccccc'
TAB_COLOR_BG           = '#EEEEEE'

RX_TALK_START          = ":"
RX_TALK_PROMPT         = "Enter selection:"
TAB_RX_TALK_TEXT       = "Receiver Console"
TAB_RX_DISABLED_TEXT   = 'Waiting ...' # tab text color when disabled.
CommonTabGrpDef = {
                  'selected_background_color' : TAB_COLOR_BG_SELECT,    # sets the background color of the selected tab
                  'selected_title_color'      : TAB_COLOR_TITLE_SELECT, # sets the Words color of the selected tab
                  'title_color'               : TAB_COLOR_TITLE_UN,     # sets the Words color of an un-selected tab
                  'tab_background_color'      : TAB_COLOR_BG_UN,        # sets the background color of an un-selected tab
                  'background_color'          : TAB_COLOR_BG,           # sets the background color that's behind all tabs
                  'font'                      : theFont,
                  'tab_location'              : 'topleft',
               }
#########################################################
# devices, actuator and transmitter connection and maybe receiver
connection_devices_btns = [
                              [sg.Button(CONNECTBTN, key='ConnectBtn', disabled=False, pad=(5,0), **CommonButtonDef)],
                              [sg.Button(OPENLOGBTN, key='OpenLogBtn', disabled=True,  pad=(5,0), **CommonButtonDef)],
                              [sg.Input(default_text='testlog.log', key='LogFile',     size=(27,1), pad=((5,5),(5,5)), **CommonInputDef)],
                          ]
init_actuator_btns  = [
                        [sg.Button(HOMEACTUATORBTN, key='HomeActBtn',      disabled=True,    pad=(5,0), **CommonButtonDef)],
                        [sg.Button(SETLINOFFSETBTN, key='SetLinOffsetBtn', disabled=True,    pad=(5,0), **CommonButtonDef)],
                        [sg.Button("Op. Frequency", key='OpFrequency',     disabled=True,    pad=(5,0), **CommonButtonDef)],
                        [sg.Button(LOGNOTESNAME,    key='LogNotesBtn',     disabled=True,    pad=(5,0), **CommonButtonDef)],
                      ]
init_actuator_input = [
                        [sg.ProgressBar(max_value=100, key='HomeProgBar',  size=(20,15), pad=((5,5),(5,5)), orientation='horizontal', visible=True)],
                        [sg.Input(default_text='100',  key='LinearOffset', size=(5,1),   pad=((5,5),(5,5)), **CommonInputDef)],
                        [sg.Input(default_text='5751', key='FreqInput',    size=(5,1),   pad=((5,5),(5,5)), **CommonInputDef)],
                        [sg.Text("",                   key='Op-Frequency', size=(BTNSIZE,1), pad=(0,5), visible=True)],
                      ]
init_system_btns  = [
                        [sg.Button(STARTSYSTESTBTN,   key='StartSysTestBtn', disabled=True, pad=(15,0), **CommonButtonDef)],
                        [sg.Text(SETDELAYTIMEDEFAULT, key='ScheduleStart',   pad=(0,10),    size=(35,1), justification='center')],
                        [sg.Button(SCHEDULEBTN,       key='ScheduleTestBtn', disabled=True, pad=(15,0), **CommonButtonDef)],
                      ]

layout_inits = [
                   sg.Frame("Device Connect", 
                    [[
                       sg.Column(connection_devices_btns, element_justification='center',key='DC_col'),
                    ]], key='DC_Frm',vertical_alignment='top', relief=sg.RELIEF_FLAT, title_color=FRAME_TITLE_COLOR, title_location=FRAME_TITLE_LOCATION),
                   sg.Frame("Actuator Initialization", 
                    [[
                       sg.Column(init_actuator_btns,key='ACTBTN_col'),
                       sg.Column(init_actuator_input,key='ACTIN_col')
                    ]], vertical_alignment='top', relief=sg.RELIEF_FLAT, title_color=FRAME_TITLE_COLOR, title_location=FRAME_TITLE_LOCATION, key='AI_Frm'),
                   sg.Frame("System Test Initialization", 
                    [[
                       sg.Column(init_system_btns, element_justification='center',key='ISBtns_col'),
                    ]], key='STI_Rrm',vertical_alignment='top', relief=sg.RELIEF_FLAT, title_color=FRAME_TITLE_COLOR, title_location=FRAME_TITLE_LOCATION),
                ]

layout_Act_Position = [
                          sg.Checkbox("Lin",         key="LinChecked",    pad=(0,0),             enable_events=True),
                          sg.Checkbox("Rot",         key="RotChecked",    pad=(0,0),             enable_events=True),
                          sg.Button(GETPOSITIONBTN,  key='SetPosBtn',     pad=(0,0),             disabled=True, **CommonButtonDef),
                          sg.Input(default_text='0', key="SetPosition",   pad=(5,0), size=(5,1), **CommonInputDef),
                          sg.Text("Lin",             key='GetPosTextLin', pad=(0,0), size=(3,1)),
                          sg.Input(default_text='',  key="LinPosition",   pad=(5,0), size=(5,1), **CommonInputDef),
                          sg.Text("Rot",             key='GetPosTextRot', pad=(0,0), size=(3,1)),
                          sg.Input(default_text='',  key="RotPosition",   pad=(5,0), size=(5,1), **CommonInputDef),
                          sg.ProgressBar(max_value=100, key='SetPosProgBar', size=(SETPOSPROGBARLEN,15),
                                         visible=True, orientation='horizontal', bar_color=("#EEEEE", '#EEEEEE')),
              ]
layout_Exit_Btn = [
               sg.Button(PLOTLOGBTN, key='PlotLogBtn',size=len(PLOTLOGBTN)+1, **CommonButtonDef),
               sg.Button('Exit', key='Exit', size=len(PLOTLOGBTN)+1, **CommonButtonDef),
              ]
layout_exit = [
               sg.Frame("Act Pos Control", [layout_Act_Position], vertical_alignment='top', relief=sg.RELIEF_FLAT, key='forSizebot'),
               sg.Frame("                 Tools", [layout_Exit_Btn], title_location=sg.TITLE_LOCATION_TOP, expand_x=True, relief=sg.RELIEF_FLAT, vertical_alignment='top', element_justification='right',key='exit_frm')
              ]

layout_test = [
                layout_inits,
               [
                sg.HSeparator(color="#000000", key='HS1'),
               ],
               [
                sg.Column([ # Left Column has script selection elements
                            [sg.Frame(SELECTTESTFILE, 
                                      [
                                        [
                                            sg.Listbox(values=[], key="TestScripts", expand_y=True, expand_x=True, enable_events=True, select_mode=sg.LISTBOX_SELECT_MODE_SINGLE)
                                        ],
                                        [
                                         sg.Button(STARTTESTBTN, key='StartStopScriptBtn',   pad=((5,0),(5,10)), disabled=True, **CommonButtonDef),
                                        ]
                                     ], expand_y=True, expand_x=True
                                    )
                            ],
                            [sg.Frame(HELPOUTPUTNAME,
                                      [
                                       [
                                           sg.Multiline('', key="HelpOutput", expand_y=True, expand_x=True, do_not_clear=False, **CommonMultiLineDef)
                                       ]
                                      ]
                                    ,expand_y=True, expand_x=True)
                           ]
                         ],expand_y=True, expand_x=True, 
                        ),
                sg.VSeparator(color="#000000", key='VS1'),
                sg.Column([ # Right Column has an output window from the script player.
                            [sg.Frame('Test Output',
                                      [[
                                          sg.Multiline('', key='ScriptOutput', expand_y=True, expand_x=True, **CommonMultiLineDef)
                                      ]],
                                      expand_y=True, expand_x=True)
                            ],
                          ], key="col2", expand_y=True, expand_x=True)
               ]
              ]
rx_talk_layout = [
                   [sg.Frame(RXTALKNAME,
                             [[
                                 sg.Multiline('', key="RxTalkOut", enable_events=False,
                                              expand_y=True, expand_x=True,
                                              write_only=True, rstrip=True, autoscroll=True, disabled=True
                                             )
                            ]],expand_y=True, expand_x=True
                           )
                    ],
                   [sg.Text(RX_TALK_PROMPT, key='RxTalkPrompt',  pad=(5,0), size=(len(RX_TALK_PROMPT),1)),
                    sg.Input("",            key="RxTalkIn",      pad=(5,0), size=(50,1), **CommonInputDef),
                    # this button is hidden and responds to the Return Key of the main keyboard
                    sg.Button("Rtn Key",    key='SendRxBtn', enable_events=True, bind_return_key=True, visible=False)]
                 ]

layoutCCC = [
               [
                   sg.Image(data=encoded_icon, key="cotaImage"),
                   sg.Text("Cota Characterization Console", text_color='green', font=('Courier', 20), key='textCCC')
               ],
               [sg.HSeparator(color="#000000", key="HSx")],
               [
                   sg.TabGroup([
                                [
                                 sg.Tab("Execute Tests", layout_test, key="TestTab"),
                                 sg.Tab(TAB_RX_DISABLED_TEXT, rx_talk_layout, key="RxTalkTab", disabled=True), 
                                ]
                               ],key='TG', **CommonTabGrpDef, expand_x=True, expand_y=True),
               ],
               [sg.HSeparator(color="#000000", key="HS2")],
               layout_exit,
           ]

##########################################################
### HELPER METHODS

def uniqueFileName(fnamein, lastFm=False) :
    fnameout = fnamein
    fn, ext = os.path.splitext(fnameout)
    n=1
    lastFname = None
    while  os.path.exists(fnameout) :
        lastFname = fnameout
        fnameout = "{}-{}{}".format(fn, n, ext)
        n+=1
    if lastFm : # the last non unique file name for reading
        fnameout = lastFname
    return fnameout

def openFileName(guiObj, filenamein, howtoOpen='w') :
    fd = None
    lastFm = howtoOpen != 'w'
    try :
        # make sure we have a unique file name
        fnameout = uniqueFileName(filenamein, lastFm)
        if fnameout and howtoOpen != 'a' : # only None if reading and no file exists
            fd = open(fnameout, howtoOpen)
        elif howtoOpen == 'a' : # make sure we can append to a non existent file
            fd = open(filenamein, howtoOpen)
    except Exception as ee :
        tmpstr = "Exception when opening out file {} -- {}".format(filenamein, repr(ee))
        guiObj.outputStringUpdater(tmpstr, outputobj=guiObj.window["HelpOutput"], clear_first=True)
        fd = None
    return fd, fnameout

def getTestInstructions(fname) :
    from importlib import import_module
    mod = import_module(fname)
    testInstruct = None 
    try :
       testInstruct = mod.TestInstructions
       if not isinstance(testInstruct, str) or len(testInstruct) == 0 :
           testInstruct = None
    except :
        pass
    return testInstruct

def getWindowPlacement(winobj, start_loc=(500,100)) :
    x_loc, y_loc = start_loc
    w_loc = (0, 0, 0)
    if winobj :
        w_loc = winobj.current_location()
        if w_loc[0] > screenX[0][0] and w_loc[0] < screenX[0][1] :
            w_loc += (0,)
        else :
            w_loc += (1,)
        x_loc += w_loc[0]
        y_loc += w_loc[1]
    return (x_loc, y_loc), w_loc

### END HELPER METHODS
##########################################################

#### script play thread callback method ####
def playScript(selfobj) :
    # import the Control Script
    from ControlScript import ControlScript as CS
    from importlib import import_module
    selfobj.waitForSchedule()
    # now import the test list currently one list per testlist Location
    mod = import_module(selfobj.testlistLocation)
    # To simplify the test list dictionary is called testlist
    modTestList = getattr(mod, 'testlist')
    selfobj.executionTimeStart = time()
    if modTestList is not None :
        obj = CS(selfobj.outputStringUpdater, selfobj.stopScriptThreadEvent, selfobj.actSerialObj, selfobj.txSerialObj, selfobj.opFrequency, modTestList)
        obj.main()
    selfobj.window.write_event_value("ScriptDone", 0)
    del(obj)
    del(mod)


##########################################################
### begin window popup methods ###

def makePopupListBoxWindow(textval, messageLst, multi=False, rtn_data_only=False, plot2d=False, winloc=(10,10)) :

    if messageLst is None :
        messageLst = ["No info available"]
    if textval is None or textval == '' :
        textval = "No input provided"

    lengOfList = min(10,len(messageLst))
    widthOfList = max(25, len(messageLst[0]))

    thisSelectMode = sg.LISTBOX_SELECT_MODE_MULTIPLE if multi else sg.LISTBOX_SELECT_MODE_SINGLE
    layout = []
    tmp = []
    layout.append([sg.Text(textval, auto_size_text=True, text_color=None, background_color=None)])
    layout.append([sg.Listbox(values=messageLst, key="Names", pad=((5,0),(0,0)), size=(widthOfList, lengOfList), expand_y=False, enable_events=True, select_mode=thisSelectMode)])
    tmp.append(sg.Button('Ok', size=(6, 1), bind_return_key=True, enable_events=True))
    tmp.append(sg.Button('Cancel', size=(6, 1), enable_events=True))
    if rtn_data_only :
        tmp.append(sg.Checkbox("Data Only", key="dataOnlyChecked", pad=(0,0), enable_events=True))
    if plot2d :
        tmp.append(sg.Checkbox("Plot 2D", key="plot2dChecked", pad=(0,0), enable_events=True))
    layout.append(tmp)

    window = sg.Window(title="", layout=layout, icon=None, auto_size_text=True, button_color=None, no_titlebar=True,
                        background_color=None, grab_anywhere=True, keep_on_top=True,
                        location=winloc, relative_location=(None, None), finalize=True, modal=True, font=theFont,
                        disable_close=True)

    textLst = [None]
    while True :
        button, values = window.read()
        if rtn_data_only and button == 'dataOnlyChecked' :
            if window['dataOnlyChecked'].get() :
                window['plot2dChecked'].update(value=False)
                window['plot2dChecked'].update(disabled=True)
            else :
                window['plot2dChecked'].update(disabled=False)
            continue
        if plot2d and button == 'plot2dChecked' :
            continue
        if button != 'Ok' and button != "Cancel" :
            textLst = values['Names'] 
        else:
            if button == 'Cancel' :
                textLst = [None]
            dataOnlyck = False
            plot2dck = False
            if rtn_data_only and window['dataOnlyChecked'].get() :
                dataOnlyck = True
            if plot2d and window['plot2dChecked'].get() :
                plot2dck = True

            window.close()
            del window
            return textLst, dataOnlyck, plot2dck

def makePopupGetNotesAntenna(message, title=None, default_text='', password_char='', size=(None, None), button_color=None,
                      background_color=None, text_color=None, font=None, no_titlebar=False, grab_anywhere=False, 
                      keep_on_top=None, location=(None, None), relative_location=(None, None), modal=True, tooltip=None) :

    theRightClickMenu = None
    fd = open("antenna.txt", 'r')
    lineDict = {}
    if fd :
        linelst = []
        lines = fd.read().split("\n")
        fd.close()
        for line in lines :
            if line == '' :
                continue
            lineFields = line.split(',')
            menu_item = f"{lineFields[0]} {lineFields[1]} {lineFields[2]}"
            linelst.append(menu_item)
            lineDict.update({menu_item : line})
        theRightClickMenu = ['',linelst]

    layout = [
                [sg.Text(message, auto_size_text=True, text_color=text_color, background_color=background_color)],
                [sg.Multiline(default_text=default_text, size=size, key='-MYINPUT-', right_click_menu=theRightClickMenu, tooltip=tooltip)],
                [
                    sg.Button('Ok',     size=(6, 1), bind_return_key=False, enable_events=True),
                    sg.Button('Cancel', size=(6, 1), enable_events=True),
                ],
             ]

    window = sg.Window(title=title or message, layout=layout, icon=None, auto_size_text=True, button_color=button_color,
                    no_titlebar=no_titlebar, background_color=background_color, grab_anywhere=grab_anywhere,
                    keep_on_top=keep_on_top, location=location, relative_location=relative_location, finalize=True,
                    modal=modal, font=font)

    button = ''
    while button != 'Ok' :
        button, values = window.read()
        data = None
        if button == 'Cancel' :
            data = None
            break
        elif button == 'Ok' :
            data = values['-MYINPUT-']
            break
        else :
            antennatype = lineDict.get(button, None)
            if antennatype is not None :
                window['-MYINPUT-'].update("ANTENNATYPE="+antennatype)
    window.close()
    del window
    return data
### End window popup methods ###
##########################################################

###################################################################
#########  GUI Class ##############################################

LOG_FILE_DIRECTORY    = "/LogFiles/"
SCRIPT_FILE_DIRECTORY = "/TestScriptFiles/"

class CharSerialGui() :
    def __init__(self) :
        # Create the Window
        self.window = sg.Window('Cota Characterization Console',
                                layoutCCC,
                                finalize=True,
                                font = theFont,
                                size=WIN_XY,
                                location=(10,10),
                                margins=(2,2),
                                resizable=True,)
        #self.window.move(*self.window.mouse_location()) # * expands out the tuple
        # Initialize class variables
        self.lastCRCR         = 0
        self.rxRtnLeng        = 0
        self.txHostname       = ""
        self.txOsVersion      = ""
        self.txSwVersion      = "" 
        self.txBuildId        = "" 
        self.txSerialObj      = None
        self.actSerialObj     = None
        self.homeNotifyFunc   = None
        self.setPosNotifyFunc = None
        self.logFileFd        = None
        self.testlistLocation = None
        self.thdId            = None
        self.timeInSeconds    = None
        self.executionTimeStart = None
        self.opFrequency      = 5751

        self.saveOpenButtonColor     = (None, None)
        self.readyToSelectTestScript = False

        # initialize class objects
        self.startNowThreadEvent   = Event()
        self.stopScriptThreadEvent = Event()
        self.logFileDirectory      = os.path.dirname(__file__)+LOG_FILE_DIRECTORY

        scriptsDirectory           = os.path.dirname(__file__)+SCRIPT_FILE_DIRECTORY

        # First button of the initialization state sequence
        self.window['ConnectBtn'].update(button_color=NEXT_BUTTON_COLOR)

        # get the list of testlists ready for display
        listdir =  glob.glob(scriptsDirectory+"TestList*.py")
        listfname = []
        if not listdir :
            listdir = ["None found in:",f"{scriptsDirectory}"]
        else :
            # remove the directory from each file
            sys.path.append(scriptsDirectory)
            for pname in listdir :
                bname = os.path.basename(pname)
                if bname.startswith("TestList") :
                    listfname.append(bname)
        # fill the test scripts list box with the testlist files
        self.window['TestScripts'].update(values=listfname)

        # define the event dictionary. It will be used to find the Event method
        # to process the event
        self.Event_Dict = {
            'OpenLogBtn'         : self.OpenCloseLogFileEvent,
            'LogNotesBtn'        : self.LogNotesEvent,
            'HomeActBtn'         : self.SendActuatorHomeEvent,
            'SetLinOffsetBtn'    : self.SetLinearOffsetEvent,
            'TestScripts'        : self.SelectTestScriptFileEvent,
            'StartStopScriptBtn' : self.StartStopScriptEvent,
            'ScriptDone'         : self.ScriptDoneEvent,
            'RotChecked'         : self.CheckBoxEvent,
            'LinChecked'         : self.CheckBoxEvent,
            'SetPosBtn'          : self.SetLinearRotaryPositionEvent,
            'ConnectBtn'         : self.ProcessConnectEvent,
            'StartSysTestBtn'    : self.StartSystemTestEvent,
            'ScheduleTestBtn'    : self.ScheduleTestEvent,
            'PlotLogBtn'         : self.PlotLogEvent,
            'SendRxBtn'          : self.RxTalkEvent,
            'OpFrequency'        : self.SetOpFrequencyEvent,
            sg.WIN_CLOSED        : None,
            'Exit'               : None,
            }

    def __del__(self) :
        self.closeSerial()
        if self.logFileFd :
            self.logFileFd.close()
        if self.window :
            self.window.close()

    ####################################################################
    ######################## GUI SUPPORT METHODS ###########################

    def getTimeStrFromSeconds(self, timeval) :
        """ getTimeStrFromSeconds -- get the clock time and the hours mins and seconds.
        """
        tmstr = strftime("%-I:%M:%S %p", localtime(time()+timeval)).lower()
        hrs   = int(timeval // 3600)
        mins  = int((timeval - (hrs * 3600)) // 60)
        secs  = int(timeval - (hrs*3600) - (mins*60))
        return tmstr, hrs, mins, secs

    def waitForSchedule(self) :
        if self.timeInSeconds and self.timeInSeconds > 0 :
            sleeptime = self.timeInSeconds
            sleepsegment = 1 # 1 sec
            cur_sleep = min(sleeptime, sleepsegment)
            tplus, hrs, mins, secs  = self.getTimeStrFromSeconds(self.timeInSeconds)
            self.outputStringUpdater(f"Set a delay to start at {tplus}")
            self.window['ScheduleStart'].update(value=TIMETOSTARTFMT.format(tplus, hrs, mins, secs))
            while sleeptime > 0 and not self.startNowThreadEvent.wait(cur_sleep) :
                sleeptime -= sleepsegment
                cur_sleep = min(sleeptime, sleepsegment)
                _, hrs, mins, secs  = self.getTimeStrFromSeconds(sleeptime)
                self.window['ScheduleStart'].update(value=TIMETOSTARTFMT.format(tplus, hrs, mins, secs))
            if sleeptime <= 0 :
                self.window['ScheduleTestBtn'].update(disabled=True, text=SCHEDULEBTN, button_color=BUTTON_COLOR)

    def updateSetPosProgressBar(self, val) :
         self.window['SetPosProgBar'].update(current_count=val)

    def updateHomeProgressBar(self, val) :
         self.window['HomeProgBar'].update(current_count=val)

    def outputStringUpdater(self, strval, outputobj=None, clear_first=False) :
        """ Output a string to the Output Multiline element. Parse the string
            for a small subset of format commands. 'bold, italic' for character shape
            'red, blue, green, etc. etc. " for character color.
            format strings are preceeded by '</' and ended by '/>' the format is continued
            to the end of the string or another '/>'
        """
        strval = strval.replace('\r','')
        if outputobj is None :
            outputobj = self.window["ScriptOutput"]
        if clear_first :
            outputobj.update(value='')
        fgColor = None
        textFontChg = () 
        textFontStart = strval.find("</")
        while textFontStart >= 0 :
            textFontStop  = strval.find("/>")
            if textFontStart >= 0 and textFontStop >= 0 :
                textFontList = strval[textFontStart+2:textFontStop].split(',')
                for tmp in textFontList :
                    tmp = tmp.lower().strip()
                    if tmp in ['bold','italic'] :
                        textFontChg += (tmp,)
                    else :
                        fgColor = tmp
                # print out the string to the first font change chars
                outputobj.print(strval[:textFontStart], end='')
                if self.logFileFd :
                    self.logFileFd.write(strval[:textFontStart])
                # now get and print the chars with the changed formatting
                strval = strval[textFontStop+2:]
                newFont = theFont+(textFontChg,)
                newFontStop  = strval.find("/>") # the end of the current format change
                if newFontStop < 0 :
                    newFontStop = len(strval)
                else :
                    textFontChg = ()
                outputobj.print(strval[:newFontStop], font=newFont, text_color=fgColor, end='')
                if self.logFileFd :
                    self.logFileFd.write(strval[:newFontStop])
                if newFontStop >= 2 and newFontStop != len(strval) :
                    strval = strval[newFontStop+2:]
                    fgColor = None
                else :
                    strval = ''
                # check for more
                textFontStart = strval.find("</")
        # print what remains
        outputobj.print(strval)
        if self.logFileFd :
            self.logFileFd.write(strval+"\n")
            self.logFileFd.flush()

    def closeSerial(self) :
        if self.actSerialObj :
            #self.actSerialObj.sendReboot()
            self.actSerialObj.sendExit()
            del self.actSerialObj
            self.actSerialObj = None
        if self.txSerialObj :
            if self.readyToSelectTestScript :
                # this is the system_test script exit command
                self.txSerialObj.sendArbCommand("x")
                # move to the home directory and unmount the mem stick
                self.txSerialObj.sendArbCommand("cd; umount /mnt")
                self.readyToSelectTestScript = False
            val = sg.popup(SHUTDOWNTHETXTEXT, title="Get User Response", auto_close=False,
                     auto_close_duration=None, keep_on_top=True, font=theFont, relative_location=(10,10),
                     button_type=sg.POPUP_BUTTONS_OK_CANCEL, modal=False)
            if val == 'OK' :
                self.txSerialObj.sendShutdown()
            else :
                self.txSerialObj.sendExit()

            del self.txSerialObj
            self.txSerialObj = None

    def setButtonsAndTextDefault(self) :
        """ setButtonsAndTextDefault -- trys to get back to the initial start up state
        """
        self.closeSerial()
        self.window['ConnectBtn'].update(disabled=False,         text=CONNECTBTN, button_color=NEXT_BUTTON_COLOR)
        self.window['HomeActBtn'].update(disabled=True,                           button_color=BUTTON_COLOR)
        self.window['OpenLogBtn'].update(disabled=True,          text=OPENLOGBTN, button_color=BUTTON_COLOR)
        self.window['LogNotesBtn'].update(disabled=True,                          button_color=BUTTON_COLOR)
        self.window['SetLinOffsetBtn'].update(disabled=True,     text=SETLINOFFSETBTN, button_color=BUTTON_COLOR)
        self.window['StartSysTestBtn'].update(disabled=True,     text=STARTSYSTESTBTN, button_color=BUTTON_COLOR )
        self.window['RxTalkTab'].update(disabled=True, title=TAB_RX_DISABLED_TEXT)
        self.window['ScheduleTestBtn'].update(disabled=True,     text=SCHEDULEBTN,     button_color=BUTTON_COLOR )
        self.window['StartStopScriptBtn'].update(disabled=True,  text=STARTTESTBTN,  button_color=BUTTON_COLOR)
        self.window['ScheduleStart'].update(value=SETDELAYTIMEDEFAULT)
        self.window['LogFile'].update(value='testlog.log')
        self.outputStringUpdater(CONNECT_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
        if self.logFileFd :
            self.logFileFd.close()
        if self.thdId is not None :
            del(self.thdId)
        self.timeInSeconds    = None
        self.txHostname       = ""
        self.txOsVersion      = ""
        self.txSwVersion      = "" 
        self.txBuildId        = "" 
        self.txSerialObj      = None
        self.actSerialObj     = None
        self.homeNotifyFunc   = None
        self.setPosNotifyFunc = None
        self.logFileFd        = None
        self.testlistLocation = None
        self.thdId            = None
        self.saveOpenButtonColor     = (None, None)
        self.readyToSelectTestScript = False

    def sendGetRx(self, sendThis, myEvent) :
        cmd = f"t {sendThis}"
        resplst = self.txSerialObj.sendArbCommand(f"{cmd}",": ").replace('\r','').split('\n')
        resp = "\n".join(resplst[:-1])
        self.window[myEvent].update(value=resp, append=True)

    ######################## END GUI SUPPORT METHODS #######################

    ########################################################################
    ######################## GUI EVENT METHODS #############################

    def LogNotesEvent(self, _myEvent, _values) :
            text = makePopupGetNotesAntenna('Log Notes', default_text='', title="Add Test Notes To Log",
                      keep_on_top=False, font=theFont, size=(30, 10), relative_location=(10,10), tooltip=LOGNOTES_TT)
            if text is not None :
                self.outputStringUpdater(f"{text}")

    def OpenCloseLogFileEvent(self, _myEvent, values) :
        """ Open a log file to receive all the info send to the Output element
            See outputStringUpdater above
        """
        if self.logFileFd :
            self.window['OpenLogBtn'].update(button_color=self.saveOpenButtonColor,text=OPENLOGBTN)
            self.logFileFd.close()
            self.logFileFd = None
        else :
            filename = values['LogFile']     # get the log file name
            self.logFileFd, logFilename = openFileName(self, self.logFileDirectory+filename)
            self.window['LogFile'].update(value=f"{os.path.basename(logFilename)}")
            if self.logFileFd :
                self.saveOpenButtonColor = self.window['OpenLogBtn'].ButtonColor
                self.window['OpenLogBtn'].update(button_color=("green", "light blue"),text=CLOSELOGBTN)
                self.outputStringUpdater(f"OS Version = {self.txOsVersion}")
                self.outputStringUpdater(f"SW Version = {self.txSwVersion}")
                self.outputStringUpdater(f"Build ID   = {self.txBuildId}")
                self.window['LogNotesBtn'].update(disabled=False, button_color=NEXT_BUTTON_COLOR)
            else :
                self.outputStringUpdater(f"Could not open log file {logFilename}")
            # 3rd STATESTEP. Next step is Home Actuator
            self.outputStringUpdater(HOME_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
            self.window['HomeActBtn'].update(disabled=False, button_color=NEXT_BUTTON_COLOR)

    def ProcessConnectEvent(self, _myEvent, _values) :
        """ ProcessConnectEvent -- The connect button as been selected and this takes care
                                   of finding the Actuator serial port as well as the TX serial port.
                                   Once these port are found the configured for communication and
                                   are logged into. The credentials are stored in this Event method
                                   for now.
        """
        # get the TX and Actuator Serial objects
        if not self.actSerialObj and not self.txSerialObj :
            self.outputStringUpdater("", clear_first=True)
            txPort, actPort = (None, None)
            for i in range(3) :
                txPort, actPort   = STA.getSerialPorts()
               # if txPort and actPort :
                if actPort :
                    break
                name = "ActPort" if not actPort else "TxPort"
                self.outputStringUpdater(f"Try {i} -- {name} not yet available.")
                sleep(5)
            self.txSerialObj  = STA.Transmitter(txPort)
            self.actSerialObj = STA.Actuator(actPort)
            self.txHostname = "mar-tx-stub"

            if not self.actSerialObj.isStub :
                if self.actSerialObj.checkForLogin() :
                    self.actSerialObj.sendUsernamePassword('root')
                    self.actSerialObj.getIpAddress()
            if not self.txSerialObj.isStub :
                if self.txSerialObj.checkForLogin() :
                    self.txSerialObj.sendUsernamePassword('root','OssiaMars!')
                self.txHostname = self.txSerialObj.getHostname()
                self.txOsVersion, self.txSwVersion, self.txBuildId = self.txSerialObj.getVersions()
                cnt = 7
                while not self.txSerialObj.isTxSwRunning() and cnt > 0 :
                    sleep(15)
                    cnt -= 1

            # set the connect button text to disconnect.
            self.window['ConnectBtn'].update(button_color=BUTTON_COLOR, text=UNCONNECTBTN)
            self.window['SetPosBtn'].update(disabled=False)
            self.window['LogFile'].update(value=f"{self.txHostname}.log")
            self.outputStringUpdater(OPEN_LOG_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
            # 2nd STATESTEP. Next step is Open Log file
            self.window['OpenLogBtn'].update(disabled=False, button_color=NEXT_BUTTON_COLOR)

            mesg = ""
            if self.txSerialObj.isStub and self.actSerialObj.isStub :
                mesg = STUB_MESG.format("TX and ACT", "s", "are")
            elif self.txSerialObj.isStub :
                mesg = STUB_MESG.format("TX", "", "is")
            elif self.actSerialObj.isStub :
                mesg = STUB_MESG.format("ACT", "", "is")
            if mesg != '' :
                self.outputStringUpdater(mesg, outputobj=self.window["HelpOutput"], clear_first=False)
        else :
            # disconnect and set the all buttons to default states
            self.setButtonsAndTextDefault()

    def SendActuatorHomeEvent(self, _myEvent, _values) :
        """ Send the Linear Actuator and the rotary actuator to Home, zero,  in preparation to setting
            the Linear Offset value.
        """
        ## set the rotary
        self.outputStringUpdater("Sending Rotary Home")
        rtn = self.actSerialObj.sendPosRot('0.0', self.homeNotifyFunc)
        ## set the linear
        self.outputStringUpdater("Sending Linear Home")
        rtn = self.actSerialObj.sendPosLin('0', self.homeNotifyFunc)

        rval = self.actSerialObj.getLinOffset()
        self.window['LinearOffset'].update(value=rval)
        self.window['HomeActBtn'].update(button_color=BUTTON_COLOR)
        # 4th STATESTEP. Next step is Set Linear Offset
        self.outputStringUpdater(LINEAR_OFFSET_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
        self.window['SetLinOffsetBtn'].update(disabled=False, button_color=NEXT_BUTTON_COLOR)

    def SetOpFrequencyEvent(self, _myEvent, values) :
        """ Set the Operating frequency. It is required to at least select the default
            to go to the next step.
        """
        freq = self.opFrequency
        valstr = values['FreqInput']
        try :
            freq = int(valstr)
            freq = min(5875, max(freq, 5725))
        except ValueError :
            self.outputStringUpdater(f"OpFrequencyi ERROR={valstr} Try again.", clear_first=False)
        else :
            self.opFrequency = freq
            self.outputStringUpdater(f"OPFREQUENCY={freq}", clear_first=False)
            self.window['OpFrequency'].update(button_color=BUTTON_COLOR)
            # 6th STATESTEP. Next step is Start System Test
            self.outputStringUpdater(STARTSYSTEST_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
            self.window['StartSysTestBtn'].update(button_color=NEXT_BUTTON_COLOR, disabled=False)

    def SetLinearOffsetEvent(self, _myEvent, values) :
        """ Set the Linear Offset from the value in the Linear Offset input box.
            Must be a valid real positive number string.
        """
        linearoffset = None
        valstr = values['LinearOffset']
        try :
            linearoffset = int(valstr, 0)
            if linearoffset < 0 :
                raise ValueError
        except ValueError :
            tmpstr = f"Set Linear Offset -- </bold/>'{valstr}'/> is not a valid Positive integer"
            self.outputStringUpdater(tmpstr, outputobj=self.window["HelpOutput"], clear_first=True)
        else :
            rval = self.actSerialObj.setLinOffset(linearoffset)
            textval = LINEAROFFSETCHECK.format(linearoffset)
            # change button from green to normal 
            self.window['SetLinOffsetBtn'].update(button_color=BUTTON_COLOR)
            val = sg.popup(textval, title="Check Linear Offset", auto_close=False,
                     auto_close_duration=None, keep_on_top=True, font=theFont, relative_location=(10,10),
                     button_type=sg.POPUP_BUTTONS_OK_CANCEL, modal=False)
            self.outputStringUpdater(f"Set Linear Offset to {linearoffset}")
            # next step is to set the Op frequency
            self.outputStringUpdater(SETOPFREQUENCY_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
            self.window['OpFrequency'].update(button_color=NEXT_BUTTON_COLOR, disabled=False)

    def StartSystemTestEvent(self, _myEvent, _values) :
        """ StartSystemTestEvent -- An Event that starts the System Test Script on the Target TX
            The steps are to use lsblk to find the usb memory device e.g. /dev/sdb1
            Use mount to mount the device to a mount point.
            Move to the mount point and start running the system test python script in interactive mod.
        """
        if not self.readyToSelectTestScript :
            self.txSerialObj.sendArbCommand('systemctl stop openvpn-ossia-keepalive && systemctl stop openvpn')
            self.outputStringUpdater("Stopped OpenVpn", clear_first=False)
            self.txSerialObj.lsblkCommand()
            self.txSerialObj.mountCommand()
            self.txSerialObj.sendArbCommand("cd /mnt")
            resp = self.txSerialObj.sendArbCommand("python3 system_test.py -i -n", RX_TALK_START, 15) + RX_TALK_START
            if RX_TALK_PROMPT in resp.split('\n')[-1] or resp == RX_TALK_START :
                self.window['StartSysTestBtn'].update(button_color=BUTTON_COLOR, text=STOPSYSTESTBTN)
                self.window['RxTalkTab'].update(disabled=False, title=TAB_RX_TALK_TEXT)
                self.outputStringUpdater("System Test Running", clear_first=False)
                self.readyToSelectTestScript = True
                # enable the Schedule delay button and give it the "Next" button color
                self.outputStringUpdater(SETDELAYTEST_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
                self.window['ScheduleTestBtn'].update(button_color=NEXT_BUTTON_COLOR, disabled=False)
            else :
                self.outputStringUpdater(f"System Test Failed to Start Running -- '{resp}'", clear_first=False)

        else :
            # the Start button text was set to Stop so if pressed again it will stop the system test script on the TX
            data = self.txSerialObj.sendArbCommand("x")
            self.window['StartSysTestBtn'].update(button_color=BUTTON_COLOR, text=STARTSYSTESTBTN)
            self.window['RxTalkTab'].update(disabled=True, title=TAB_RX_DISABLED_TEXT)
            self.readyToSelectTestScript = False
            if "Quitting" in data or data == '' :
                self.outputStringUpdater("System Test Stopped", clear_first=False)
                self.txSerialObj.sendArbCommand("cd; umount /mnt")
            else :
                self.outputStringUpdater(f"System Test stop command did not respond as expected '{data}'", clear_first=False)

    def ScheduleTestEvent(self, _myEvent, _values) :
        """ SchecduleTestEvent -- If the button text is Schedule then accept a time
            in hours to wait for the script to start executing.
            If the button text is End wait then signal the end of the wait to allow the test
            to start.
        """
        timeval = ''
        mesgstr = SCHEDULE_MSG
        if self.window['ScheduleTestBtn'].ButtonText == SCHEDULENOWBTN :
           self.window['ScheduleTestBtn'].update(text=SCHEDULEBTN, disabled=True)
           self.startNowThreadEvent.set()
           self.window['ScheduleStart'].update(value="User ended wait")

        while not self.timeInSeconds :
            # textstr can be None if the user cancels the pop up.
            textstr = sg.popup_get_text(mesgstr, default_text=timeval, title="Set a Test Start Delay",
                      keep_on_top=True, font=theFont, relative_location=(10,10))
            if textstr :
                timeInSeconds = 0
                timeval = textstr.strip()
                try :
                    hr, mins      = tuple(timeval.strip().split(":"))
                    timeInSeconds = (float(hr.strip())*3600.0)+(float(mins.strip())*60.0)
                except ValueError :
                    mesgstr = SCHEDULE_MSG_ERR
                else :
                    self.timeInSeconds = int(timeInSeconds)
                    self.window['ScheduleTestBtn'].update(text=SCHEDULENOWBTN)
                    tplus = strftime("Approximately %-I:%M:%S %p", localtime(time()+self.timeInSeconds)).lower()
                    self.window['ScheduleStart'].update(value=tplus)
            else :
                self.timeInSeconds = None
                break

    def SelectTestScriptFileEvent(self, _myEvent, values) :
        """ Select a Test List Script file from the test scripts input box
            This only contains files that are in a pre-defined directory and
            begin with a prefix 'TestList'
        """
        # set the base results file name
        filename = values['TestScripts']     # get the script file name
        # make it python-ish, notice the dot on the end
        sfd = f"{SCRIPT_FILE_DIRECTORY.strip('/')}."
        self.testlistLocation = sfd+os.path.splitext(filename[0])[0]
        if self.testlistLocation and self.readyToSelectTestScript :
            testInstructionsStrval = getTestInstructions(self.testlistLocation)
            if not testInstructionsStrval :
                sg.popup("No instructions available.\nNeed to add these.", title="Test Instructions",
                         auto_close=True, keep_on_top=True, auto_close_duration=3, font=theFont, relative_location=(10,10),
                         button_type=sg.POPUP_BUTTONS_NO_BUTTONS)
                testInstructionsStrval = 'None available'
            self.outputStringUpdater(testInstructionsStrval, outputobj=self.window["HelpOutput"], clear_first=True)
            val = 'OK'
            val = sg.popup(TESTEXECUTECHECK, title="Execute Test", auto_close=False,
                     auto_close_duration=None, keep_on_top=True, font=theFont, relative_location=(10,10),
                     button_type=sg.POPUP_BUTTONS_OK_CANCEL, modal=False)
            if val == 'OK' :
                self.window['StartStopScriptBtn'].update(disabled=False, button_color=NEXT_BUTTON_COLOR)

    def SetLinearRotaryPositionEvent(self, _myEvent, values) :
        """ Set the linear or Rotary position depending on which checkbox is checked.
            It will always update the display to the current positions
            Assumes a float if the rot checkbox is set
            Assumes an int if the lin checkbox is set
            See the checkbox Event method to see the exclusivity logic.
        """
        position = values['SetPosition'] # could use _myEvent but it hides information from a reviewer
        self.window['SetPosProgBar'].update(bar_color=(None,None)) # set progress bar to visible
        self.window['SetPosBtn'].update(disabled=True) # set the button to disabled
        valGood = True
        if self.window['RotChecked'].get() :
            try :
                pos = float(position)
            except ValueError :
                tmpstr = f"Set Rotary Position -- </bold/>'{position}'/> is not a valid Float."
                self.outputStringUpdater(tmpstr, outputobj=self.window["HelpOutput"], clear_first=True)
                valGood = False
            else :
                self.actSerialObj.sendPosRot(position, self.setPosNotifyFunc)
        elif self.window['LinChecked'].get() :
            try :
                pos = int(position, 0)
            except ValueError :
                tmpstr = f"Set Linear Position -- </bold/>'{position}'/> is not a valid integer"
                self.outputStringUpdater(tmpstr, outputobj=self.window["HelpOutput"], clear_first=True)
                valGood = False
            else :
                self.actSerialObj.sendPosLin(position, self.setPosNotifyFunc)
        if valGood :
            self.window['LinPosition'].update(value=self.actSerialObj.getLinPosition())
            self.window['RotPosition'].update(value=self.actSerialObj.getRotPosition())
        self.window['SetPosBtn'].update(disabled=False) # set the button to enabled
        return position
        
    def CheckBoxEvent(self, myEvent, _values) :
        """ Positioning checkbox event. Need to implement mutual exclusivity
            and update the button name.
            if either checkbox is checked then the button name is Set position
            if neither are checked then the name is Get position
        """
        if myEvent == 'RotChecked' :
            if self.window['RotChecked'].get() :
                self.window['LinChecked'].update(value=False)
                self.window['SetPosBtn'].update(text=SETPOSITIONBTN)
            else :
                self.window['SetPosBtn'].update(text=GETPOSITIONBTN)
        elif myEvent == 'LinChecked' :
            if self.window['LinChecked'].get() :
                self.window['RotChecked'].update(value=False)
                self.window['SetPosBtn'].update(text=SETPOSITIONBTN)
            else :
                self.window['SetPosBtn'].update(text=GETPOSITIONBTN)

    def ScriptDoneEvent(self, _myEvent, _values) :
        """ Send the termination value to the output window to inform the tester.
        """
        self.outputStringUpdater("Script Done!")
        tm = time()
        mins = int(tm-self.executionTimeStart)//60
        secs = tm-self.executionTimeStart - (mins * 60)
        self.outputStringUpdater(f"Execution Time {mins} mins, {secs:5.2f} secs")
        # if self.testlistLocation is still set then the test list finished. A user stop will set the location to None
        if self.testlistLocation :
            self.setButtonsAndTextDefault()
        else :
            self.window['StartStopScriptBtn'].update(disabled=True, button_color=BUTTON_COLOR, text=STARTTESTBTN)
            self.window['ConnectBtn'].update(disabled=False)
            self.window['ScheduleTestBtn'].update(disabled=False, button_color=BUTTON_COLOR, text=SCHEDULEBTN)
            self.window['ScheduleStart'].update(value=SETDELAYTIMEDEFAULT)
            self.timeInSeconds = None
            self.thdId = None

    def StartStopScriptEvent(self, _myEvent,  _values) :
        """ StartStopScriptEvent -- If no script is started then start one
                                    if a script is started then stop it.
            Uses the Start Script button ButtonText to test if script is to be
            started or stopped.
        """
        # can't start if there is no testlist selected
        # check for stop text in the button
        if self.window["StartStopScriptBtn"].ButtonText == STOPTESTBTN :
            # set both signals so that it stops the script even if it's still waiting to start
            self.startNowThreadEvent.set()
            self.stopScriptThreadEvent.set()
            self.testlistLocation = None
        elif self.testlistLocation :
            # used to signal a stop event
            self.stopScriptThreadEvent.clear()
            # used to signal an end of wait to start event
            self.startNowThreadEvent.clear()
            self.window["StartStopScriptBtn"].update(disabled=False, button_color=BUTTON_COLOR, text=STOPTESTBTN)
            self.window['ConnectBtn'].update(disabled=True)
            self.thdId = Thread(name='playScript', target=playScript, args=(self,), daemon=True)
            self.thdId.start()

    def RxTalkEvent(self, _myEvent, values) :
        cmd = values['RxTalkIn'][len(RX_TALK_PROMPT):]
        self.sendGetRx(cmd.strip(), "RxTalkOut")
        self.window['RxTalkIn'].update(value=RX_TALK_PROMPT)


    def PlotLogEvent(self, _myEvent, values) :
        """ plots a selected log file
        """
        # open a popup with the contents of the logfile directory but only show the last 4 
        # can scroll to others
        if self.window[_myEvent].ButtonText == PLOTLOGBTN :
            filelst = plf.getLogFilenames(self.logFileDirectory) # returns the base names
            locX, locY = self.window.mouse_location()
            locX -= pixelsWFromChars(theFont, len(max(filelst)))
            locY -= pixelsHFromChars(theFont, min(len(filelst),20))
            popup_loc = (locX, locY)
            fnameLst, dataOnly, plot2d = makePopupListBoxWindow("Select a log file to process", filelst, rtn_data_only=True, plot2d=True, winloc=popup_loc)
            fname = fnameLst[0]
            if fname :
                filename = self.logFileDirectory+fname # make is a full path
                namelst, _ = plf.getBatStatListAndData(filename)
                bstatname, _, _ = makePopupListBoxWindow("Select up to 3 Battery Stats", namelst, multi=True, winloc=popup_loc)
                bstatname = bstatname[:min(3,len(bstatname))]
                _, win_curloc = getWindowPlacement(self.window)
                rtnDict, desigDict  = plf.plotLogFile(filename, True, win_curloc, batstatLst=bstatname, dataonly=dataOnly, plot3d=not plot2d)

                self.outputStringUpdater(f"{filename}", clear_first=True)
                for k, v in desigDict.items() :
                    self.outputStringUpdater(f"</bold/>{k}/>={v}")
                if dataOnly : 
                    for datk, dataValues in rtnDict.items() : #dataValues, beaconsValues, namelst, statsValues
                        if isinstance(dataValues, str) :
                            self.outputStringUpdater(f"{datk}\n{dataValues}")
                            continue
                        else :
                            self.outputStringUpdater(f"{datk}")

                        try :
                            distKeys = list(dataValues.keys())
                        except :
                            continue
                        try :
                            angleKeys = list(dataValues[distKeys[0]].keys())
                        except :
                            angleKeys = []
                        for ai,ak in enumerate(angleKeys) :
                            header = ''
                            data = f'{ak}'
                            for dk in distKeys :
                                header += f",{dk}"
                                if isinstance(dataValues[dk][ak], list) :
                                    data += f",{dataValues[dk][ak][0]}"
                                else :
                                    if len(bstatname) == 1 :
                                        if bstatname[0] is not None :
                                            data += f",{dataValues[dk][ak][bstatname[0]][0]}"
                                    else :
                                        data += ",["
                                        for bsn in bstatname :
                                             data += f"{dataValues[dk][ak][bsn]};"
                                        data += "]"
                            if ai == 0 :
                                self.outputStringUpdater(f"{header}")
                            self.outputStringUpdater(f"{data}")
                    #self.outputStringUpdater(f"{beaconsValues}")
                    #self.outputStringUpdater(f"{namelst}")
                    #self.outputStringUpdater(f"{statsValues}")
                else :
                    self.window[_myEvent].update(text=CLOSEPLOTLOGBTN)
        elif self.window[_myEvent].ButtonText == CLOSEPLOTLOGBTN :
            plf.closeLogs('all')
            self.window[_myEvent].update(text=PLOTLOGBTN)

        
    def passEvent(self, myEvent,  _values) :
        self.outputStringUpdater(f"Unknown event '{myEvent}'", outputobj=self.window["HelpOutput"])
        # used for any unknown events.
        pass
######################## END GUI EVENT METHODS #############################

######################## MAIN. THE GUI EVENT PROCESSING #########################

    def main(self) :
        # set the progress bar tuples
        self.homeNotifyFunc   = (self.updateHomeProgressBar, self.window['HomeProgBar'].MaxValue)
        self.setPosNotifyFunc = (self.updateSetPosProgressBar, self.window['SetPosProgBar'].MaxValue)

        # 1st STATESTEP. Next step is OpenLog
        self.outputStringUpdater(CONNECT_STEP, outputobj=self.window["HelpOutput"], clear_first=True)
        while True:
            event, values = self.window.read()
            # use the event
            function = self.Event_Dict.get(event, self.passEvent)
            if function :
                # All the even processing and state transitions are done in
                # the Event methods.
                # pass in the event and values
                function(event, values)
            else :
                break
        # GUI is closing
        self.closeSerial()
        if self.logFileFd :
            self.logFileFd.close()
            self.logFileFd = None
        self.window.close()
        self.window = None

if __name__ == "__main__" :
    m = CharSerialGui()
    m.main()

