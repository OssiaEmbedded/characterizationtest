
cfgLinear={
'cableloss' : '6.19',
'calibrate' : '0',
'console' : 'False',
'txipaddr' : '10.10.0.51',
'txport' : '50000',
'minlinkq' : '40',
'actuator' : None,
'maxrssi' : '28.0',
'genx1dist' : '',
'genx2dist' : '',
'saipaddr' : None,
'saport' : '5023',
'average' : 'False',
'logfile' : 'PmuCheckout_{}.csv',
'logtodb' : 'True',
'dbname' : 'PmuCheckout',
'tblname' : 'PmuData',
'directory' : '/tmp/Logs/',
}
