''' PwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback
if "/home/ossiadev/pylib" not in sys.path or "/home/ursusm/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")
    sys.path.insert(0, "/home/ursusm/pylib")

import json
from time import sleep, time, localtime
from optparse import OptionParser, Option, OptionValueError
from math import sin, pi

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm, gPWR_LEVEL_MAP 
from LIB.logResults import LogResults
from LIB.actuator import Actuator
from LIB.resultsEmail import EmailResults
from copy import copy
from socket import gethostbyname

emailer = EmailResults()

def check_bool(option, opt, value) :
    try :
        return bool(value)
    except ValueError :
        raise OptionValueError("option %s: invalid bool value: %r %s" % (opt,value,repr(option)))

class myOption (Option) :
    TYPES = Option.TYPES + ('bool',)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER['bool'] = check_bool

ROTARYON = 'ossiabbb2'

TESTDBNAME='IntegLabOne'

TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME = "SaTable"
TXTBLNAME="ChargerConfig"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(40)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["PmusEnabled","%s", 'VARCHAR(50)'],
         ["RecsEnabled","%s", 'VARCHAR(50)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'],
         ["calPhase", "%s", 'VARCHAR(4096)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["netCurrent", "%3.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%3.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%5f", 'VARCHAR(10)'],
             ["peakPower", "%5f", 'VARCHAR(10)'],
             ["TxAngle", "%5.1f",'VARCHAR(7)'],
             ['DistanceZ1', "%d", 'VARCHAR(10)'],
             ['DistanceZ2', "%d", 'VARCHAR(10)'],
             ['DistanceX1', "%d", 'VARCHAR(10)'],
             ['DistanceX2', "%d", 'VARCHAR(10)'],
             ['DistanceY1', "%d", 'VARCHAR(10)'],
             ['DistanceY2', "%d", 'VARCHAR(10)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["QueryTime", "%s", 'VARCHAR(20)'],
             ["MaxCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["MaxZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["PowerLevel", "%d", 'VARCHAR(5)'],
             ["WarmupTime", "%d",'VARCHAR(7)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["clientComRSSI", "%d", 'VARCHAR(6)'],
             ["proxyComRSSI", "%d", 'VARCHAR(6)'],
             ["AvgCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["AvgZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["Model", "%d",        'VARCHAR(10)'],
             ["version", "%d", 'VARCHAR(20)'],
             ["TPSMissed", "%d",    'VARCHAR(20)'],
             ["LongBeaconCnt", "%d",    'VARCHAR(20)'],
             ["NumBeaconCnt", "%d",    'VARCHAR(20)'],
             ["WatchDogRstCnt", "%d",    'VARCHAR(20)']]

sTXIP='SV2Tile_IntegLab'
sTXPORT='50000'
sSAIP='A-N9020A-11404.ossiainc.local'
sSAPORT='5023'
srvPORT='50081'


def getCalPhaseOffset(tx, clientList, option, amu=0) :
    from LIB.sendTestServer import SendTestServer
    sts=SendTestServer(option.txipaddr, int(srvPORT), option.debug)
    stopStartChargingList(tx, "stop", clientList)
    strjson = str(sts.sendCommand(['getCalPhaseOffset', str(amu)]))
    stopStartChargingList(tx, "start", clientList)
    pyval = json.loads(strjson)

    return pyval, strjson

def getCotaConfigValues(option) :
    from LIB.sendTestServer import SendTestServer

    sts=SendTestServer(option.txipaddr, int(srvPORT), option.debug)
    jVal = sts.sendCommand(['getConfigFile'])
    rtnstr, plst, flst = sts.parseCotaConfigValues(jVal)

    return rtnstr, plst, flst

def makeAngleDistance(angle, zVal, offset) :
    """ makeAngleDistance -- Take the angle from a perpendicular line to the transmiter
                             and the distance on the hypotinuse of the angle
                             and generate using the offset a perpendicular distance from the
                             transmitters perpendicular line to the hypotinuse (sine of the angle)
        returns a list of distances
    """
    try :
        rads = float(angle)/360.0 * 2.0 * pi
        x = int(zVal) * sin(rads) + int(offset)
    except ValueError as e :
        eprint("ERROR: ValueError in makeAngleDistance {}".format(repr(e)))
        x = 0
    return int(round(x))


def parseDistance(inStr) :
    """ parseDistance -- parses mut fields and returns a list of distances
                         range;step -- looks for a - between two numbers and a semicolon for the step
                         comma separated list of positions
    """
    rtnlist = list()
    if "-" in inStr and ";" in inStr : # do range parsing
        try :
            firstsplit = inStr.split(';')
            step       = int(firstsplit[1])
            secsplit   = firstsplit[0].split('-')
            start      = int(secsplit[0])
            end        = int(secsplit[1])
            expression = " >= "
            if (end - start) < 0 :
                expression = " <= "
            elif (end - start) == 0 :
                return rtnlist
            if eval("{}{}{}".format((end - start), expression,  step)) :
                while eval("{}{}{}".format(end, expression, start)) :
                    rtnlist.append(start)
                    start += step
        except ValueError :
            pass
    else : # do comma parsing
        rtnlist = inStr.split(',')
    return rtnlist

def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            print("Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            print(msg)
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getVersions(tx) :
    argv = ["versions"]
    v = tx.sendGetTransmitter(argv)
    vers = v['Result']
    swRel = int(vers["Release Version"])
    versionstr = "SWRel:{:X}.{:X}.{:X}\n".format(((swRel>>16) & 0xF), ((swRel >> 8) & 0xF), (swRel & 0xF))

    blddate = int(vers['Daemon Build Info']['Date'])
    bldnum  = int(vers['Daemon Build Info']['Number'])
    versionstr += "Daemon:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Driver Lib Build Info"]["Date"])
    bldnum  = int(vers["Driver Lib Build Info"]["Number"])
    versionstr += "DrvLib:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Message Manager Build Info"]["Date"])
    bldnum  = int(vers["Message Manager Build Info"]["Number"])
    versionstr += "MesMan:{}:{}\n".format(blddate, bldnum)

    bldnum = int(vers["FPGA Revision"])
    versionstr += "FPGA:{:X}.{:X}\n".format((bldnum>>8) & 0xF, bldnum & 0xF)

    bldnum = int(vers["Proxy FW Revision"])
    versionstr += "PROXY:0.{:d}\n".format(bldnum)

    versionstr += "OS:{}".format(str(vers["OS Version"]))
        
    return versionstr

def getOnChannels(tx) :
    #
    ch_on = 0
    argv = ["info"]
    v = tx.sendGetTransmitter(argv)
    chList = v['Result']['AMBS']
    rtnstr = ""
    for chan in chList :
        outStr = ""
        try :
            chOnStr = str(chan['Status'])
            chOn = 1 if chOnStr == 'ON' else 0
            chNum = int(chan['Channel Number'])
            rev = int(chan['AMB Revision'])
            # make the def smaller, take out all the extra unneeded characters
            outStr = "{}:{:02d}:{:04X}\n".format(chOnStr, chNum, rev)
            # chOn will be either 1 or 0, shifting a zero is benign.
            ch_on |= (chOn << chNum)
        except ValueError as ve :
            eprint("ERROR: Chan Info ValueError {} -- {}".format(v['Result']['Status'], ve))
            outStr = "{}:{}:{}\n".format("OFF", "ZZ", "XXXX")
        rtnstr += outStr

    chStr = "0x{:04X}".format(ch_on)
    return chStr, rtnstr

def stopStartChargingList(tx, ss, clientList, resetclient=False) :
    # ss must be either 'stop' or 'start' for this to work
    rtnval = False
    cmd = "{}_charging".format(ss)
    failword = "{}CHARGING".format("NOT_" if ss == "stop" else '')
    for client in clientList :
        sClientId = str(client[0])
        argv = [cmd, sClientId]
        try :
            v = tx.sendGetTransmitter(argv)
            if v['Result']['Devices'][0]['Status'] != failword :
                eprint("ERROR: Charging did not '{}': {}".format(ss, repr(v['Result']["Devices"][0]['Status'])))
                rtnval = True
            sleep(2)
        except Exception :
            rtnval = True
        if resetclient :
            argv = ['client_command', sClientId, '3']
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ERROR: Reset Client Failed -- {}".format(sClientId))
                rtnval = True
            else :
                eprint("INFO: Reset Client {}".format(sClientId))
    sleep(1)
    return rtnval

def getSAPower(sa, saimage, option, cChanFreq, cChanNum) :
    """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                      sapower is a float
                      safreq is a float
    """
    sapower = -60.0
    safreq = -2.0
    if sa.usesa :
        if option.average :
            sa.avgHold = ""
        else :
            sa.maxHold = ""
        # wait for data to get stable
        sleep(2)
        # find the freq of the peak
        sa.peakSearch = ""

        # get the SA data
        sa.triggerHold = ''
        SApower, SAfreq = sa.peakSearch
        if saimage > 0 :
            try :
                tstEval = "%6.4e != %6.4e" % (SAfreq, float(cChanFreq[str(cChanNum)]))
                if saimage == 2 or eval(tstEval) :
                    filename = "{}/saImage_{}.png".format(option.directory, int(time()))
                    saI = sacom.SACom(addr=option.saipaddr, port=80, debug=option.debug)
                    saI.getSaveImage(filename)
                    logger.logAddValue("ImageSA", filename)
            except KeyError as ke :
                eprint("ERROR: KeyError in comChanNum -- {}\n".format(repr(ke)))
            except ValueError as ve :
                eprint("ERROR: ValueError in comChanNum -- {}\n".format(repr(ve)))
            except TypeError as te :
                eprint("ERROR: TypeError in comChanNum -- {}\n".format(repr(te)))

        sa.triggerClear = ''
        sa.clearHold = ''
        try :
            sapower = float(SApower)
        except ValueError as ve :
            eprint("ERROR: SApower ValueError on '{}' -- {}".format(SApower, ve))
            sapower = -1.0
        try :
            safreq = float(SAfreq)
        except ValueError as ve :
            eprint("ERROR: SAfreq ValueError on '{}' -- {}".format(SAfreq, ve))
            safreq = -1.0
    return sapower, safreq

def calibrateAlgorithm(tx, sa, clientList, option) :
    """ calibrateAlgorithm -- first resets the tile and then performs calibrate after that.
                              calibrate 5 times getting SA PeakPower each time. The maximum of that
                              sequence will then be used as a goal for up  to 10 more calibrates.
                              When one of them is equal to (+- .5) or greater than the max the
                              algorithm finishes without calibrating again.
    """
    saMax = -100.0
    saPwr, _ = getSAPower(sa, 0, option, None, None)
    saMax = max(saMax, saPwr)
    rtnval = True
    rtnlst = list()

    pyDictOrg, _ = getCalPhaseOffset(tx, clientList, option, amu=1)
    pyDictOrg.update({"sapwr" : saPwr})
    rtnlst.append(pyDictOrg)
    argv = ['reset']
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        print("ERROR: calibrateAlgorithm1 - Transmitter did not Calibrate")
        rtnval = False
    sleep(8)
    argv = ['reset']
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        print("ERROR: calibrateAlgorithm2 - Transmitter did not Calibrate")
        rtnval = False
    sleep(8)

    if rtnval :
        for cal in range(5) :
            saPwr, _ = getSAPower(sa, 0, option, None, None)
            saMax = max(saMax, saPwr)
            pyDict, _ = getCalPhaseOffset(tx, clientList, option, amu=1)
            pyDict.update({"sapwr" : saPwr})
            rtnlst.append(pyDict)
            print("Seeking {} saMax={} saPwr={}".format(cal, saMax, saPwr))
            if pyDict.get('0', None) is None :
                for phs in range(1,5,1) :
                    if pyDict.get(str(phs), None) is not None :
                        print("\n{}\nOrg {}\nNew {}\n".format(phs, pyDictOrg[str(phs)], pyDict[str(phs)]))

            argv = ['reset']
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("calibrateAlgorithm3 - Transmitter did not Calibrate")
                rtnval = False
                break
            sleep(8)
    
            argv = ['reset']
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("calibrateAlgorithm4 - Transmitter did not Calibrate")
                rtnval = False
                break
            sleep(8)

        saPwr, _ = getSAPower(sa, 0, option, None, None)
        saMax = max(saMax, saPwr)
        #print("\ndd saMax={} saPwr={}\n".format(saMax, saPwr))

    if saMax > saPwr and rtnval :
        for cal in range(10) :
            argv = ['reset']
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("ERROR: Transmitter did not Calibrate")
                rtnval = False
            sleep(8)
            saPwr, _ = getSAPower(sa, 0, option, None, None)
            if (saMax - 0.3) < saPwr :
                break
            print("2 {} saMax - 0.3={} saPwr={}".format(cal, (saMax - 0.3), saPwr))

    eprint("\nINFO: Final saMax={} saPwr={}\n".format(saMax, saPwr))
    return rtnval, rtnlst

def transmitterCalibrate(tx, clientList, msg='None', prnt=True) :
    rtnval = False #stopStartChargingList(tx, 'stop', clientList)

    if not rtnval :
        argv = ['calibrate']
        v = tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ERROR: transmitterCalibrate - Transmitter did not Calibrate: {}".format(msg))
            rtnval = True

    sleep(3)
    if not rtnval :
        rtnval = False #stopStartChargingList(tx, 'start', clientList)

    if prnt :
        eprint("INFO: Calibration {} CL={}".format("Failed" if rtnval else "Done", repr(clientList)))

    return rtnval

def getClientCommandData(tx, sClientId, num, delayTime=2.5) :
    argv = ['client_command', sClientId, num]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        eprint("ERROR: getClientCommandData - Client Command {} request Failed : {}".format(num, repr(v)))
    sleep(delayTime)

    argv = ['client_command_data', sClientId]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        eprint("ERROR: getClientCommandData - Client Command Data request Failed ({}): {}".format(num, repr(v)))
    return v

def runthecharacter(sa, tx, clientList, testlist, logger, options, comChanNum) :

    finished = False
    iterateTime = time()
    iterateCnt = -1
    iterateCntUp = 1
    maxPeakPower = -100.0
    maxBatteryLevel = 0.0
    comChanFreq = { "24" : 2.4601E9, "25" : 2.4501E9, "26" : 2.4401E9}
    try :
        iterateCnt = int(testlist.get('logcount', 10))
    except ValueError :
        eprint("ERROR: logcount input error")
        return True, maxPeakPower, maxBatteryLevel

    try :
        xDistance1 = int(testlist.get('xdistance1', 0))
        yDistance1 = int(testlist.get('ydistance1', 0))
        zDistance1 = int(testlist.get('zdistance1', 0))

        xDistance2 = int(testlist.get('xdistance2', 0))
        yDistance2 = int(testlist.get('ydistance2', 0))
        zDistance2 = int(testlist.get('zdistance2', 0))
        txAngle = float(testlist.get('txangle', 0.0))
    except ValueError as ve :
        eprint("ERROR: ValueError on testlist Dist/angle access -- {}\n".format(repr(ve)))
        return True, maxPeakPower, maxBatteryLevel

    realZ1Distance = zDistance1
    realZ2Distance = zDistance2
    realTxAngle = txAngle
    if options.actuator is not None :
        act2 = None
        actuatorAry = options.actuator.split(",")
        act1 = Actuator(actuatorAry[0], None, debug=options.debug)
        act1.sendCommandString(arg0='setPosition', arg1=str(zDistance1), arg2='l')
        rtnlst = act1.sendCommandString(arg0='getPosition', arg1='l').split()
        for val in rtnlst :
            try :
                realZ1Distance = int(val)
                print("        Z1 real vs. set: abs({} - {}) = {}\n".format(realZ1Distance, zDistance1, abs(realZ1Distance-zDistance1)))
                break
            except ValueError :
                pass

        if len(actuatorAry) > 1 :
            act2 = Actuator(actuatorAry[1], None, debug=options.debug)
            act2.sendCommandString(arg0='setPosition', arg1=str(zDistance2), arg2='l')
            rtnlst = act2.sendCommandString(arg0='getPosition', arg1='l').split()
            for val in rtnlst :
                try :
                    realZ2Distance = int(val)
                    print("        Z2 real vs. set: abs({} - {}) = {}\n".format(realZ2Distance, zDistance2, abs(realZ2Distance-zDistance2)))
                    break
                except ValueError :
                    pass
        rotact = act2
        if actuatorAry[0] == ROTARYON :
            rotact = act1

        if rotact :
            rotact.sendCommandString(arg0='setPosition', arg1=str(txAngle), arg2='r')
            rtnlst = rotact.sendCommandString(arg0='getPosition', arg1='r').split()
            for val in rtnlst :
                try :
                    realTxAngle = float(val)
                    print("        Rot real vs. set: abs({} - {}) = {}\n".format(realTxAngle, txAngle, abs(realTxAngle-txAngle)))
                    break
                except ValueError :
                    pass

    #if iterateCnt <= 0 and not options.calibratealg :
        #print("Position Only No data taken txAngle={} distance {}\n".format(txAngle, zDistance1))
        #return False, maxPeakPower, maxBatteryLevel

    if stopStartChargingList(tx, 'start', clientList) :
        eprint("ERROR: stopStartChargingList error")
        return True, maxPeakPower, maxBatteryLevel

    # wait for the warmup period
    warmup = int(testlist.get('warmup', 30))
    tm_obj = localtime(time())
    tf_obj = localtime(time()+warmup)
    fmt = "INFO: Warmup time {} : Start at -- {:02d}:{:02d}:{:02d} finish at {:02d}:{:02d}:{:02d}\n"
    eprint(fmt.format(warmup, tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec,
                             tf_obj.tm_hour, tf_obj.tm_min, tf_obj.tm_sec))

    reportWait(warmup, prnt=options.console)

    if options.poscalibrate or testlist['caljustonce'] :
        if options.calibratealg and not options.poscalibrate :
            _, calData = calibrateAlgorithm(tx, sa, clientList, options)
            calDataJson = json.dumps(calData)
            eprint("INFO: calDataJson = '{}'\n".format(repr(calDataJson)))
            if options.logtodb :
                try :
                    logger.logUpdateTableColumn(options.dbname, options.tblname, "calPhase", calDataJson)
                    eprint("INFO: logUpdateTableColumn DONE")
                except Exception as e : 
                    eprint("ERROR: Logging the calPhase failed {}\n".format(repr(e)))

        else :
            transmitterCalibrate(tx, clientList, msg="After warmup", prnt=options.console)

    calibrationInterval = int(options.calibrate)

    while (iterateCnt > 0  or (iterateTime - time()) > 0.0) and not finished :

        timetowait = float(testlist.get('waitfordata', 16)) - 2.0
        timetowait = max(timetowait, 1.0)

        sleep(timetowait)

        if calibrationInterval > 0 and (iterateCntUp % calibrationInterval) == 0 :
            transmitterCalibrate(tx, clientList, msg="After interval time", prnt=options.console)
            logger.logAddValue('calibrate', "1")
            sleep(timetowait)
        elif calibrationInterval > 0 :
            logger.logAddValue('calibrate', "0")
        iterateCntUp += 1

        iterateCnt -= 1
        if options.console and iterateCnt >= 0 :
            print("{} ".format(iterateCnt), end='')

        # set SA to obtain peak or average value
        SAPower, SAFreq = getSAPower(sa, options.saimage, options, comChanFreq, comChanNum)

        # get system temperature datum
        argv = ['get_system_temp']
        v = tx.sendGetTransmitter(argv)
        systemTemp = 0
        if jcs.checkForSuccess(v) :
            eprint("ALERT: System Temperature not valid")
        else :
            systemTemp = jcs.getSystemTemp(v)

        # get charging client details
        for client in clientList :
            sClientId = str(client[0])
            argv = ['client_detail', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v, ckVal="CHARGING") and jcs.checkForSuccess(v, ckVal="LIMITED_CHARGING") :
                eprint("ALERT: Client Detail request shows client {} not charging {}".format(sClientId, repr(v['Result']['Status'])))

            version          = jcs.getClientVersion(v)
            netCurrent       = jcs.getClientNetCurrent(v)
            queryFailedCount = jcs.getClientQueryFailed(v)
            linkQuality      = jcs.getClientLinkQuality(v)
            batteryLevel     = jcs.getBatteryLevel(v)
            maxBatteryLevel  = max(maxBatteryLevel, batteryLevel)
            Model            = jcs.getDeviceModel(v)
            DeviceStatus     = jcs.getDeviceStatus(v)
            TPSMissed        = jcs.getTPSMissed(v)
            averagePower     = jcs.getAveragePower(v)
            peakPower        = jcs.getPeakPower(v)
            clientComRSSIVal = jcs.getClientComRSSI(v)
            proxyComRSSIVal  = jcs.getProxyComRSSI(v)
            queryTime        = jcs.getClientQueryTime(v)

            # changing to averagePower from peakPower per Jorge's request.
            maxPeakPower     = max(maxPeakPower, averagePower)


        # get charging client data
            argv = ['client_data', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ALERT: Client Data request Failed: " + repr(v['Result']['Status']))
                #break
            batteryVoltage = jcs.getClientBatteryVoltage(v)
            data = jcs.processClientData(v)
            MaxZCSCNT_1, MaxCalcPwr_1, AvgZCSCNT_1, AvgCalcPwr_1 =  jcs.getClientRFPower(data, 1)
            MaxZCSCNT_2, MaxCalcPwr_2, AvgZCSCNT_2, AvgCalcPwr_2 =  jcs.getClientRFPower(data, 2)
            MaxZCSCNT_3, MaxCalcPwr_3, AvgZCSCNT_3, AvgCalcPwr_3 =  jcs.getClientRFPower(data, 3)
            MaxZCSCNT_4, MaxCalcPwr_4, AvgZCSCNT_4, AvgCalcPwr_4 =  jcs.getClientRFPower(data, 4)

            logger.logAddValue("PowerLevel", testlist['powerlevel'])
            logger.logAddValue("WarmupTime", testlist['warmup'])
            logger.logAddValue("TxAngle", txAngle)
            # add the client ID to the log list
            logger.logAddValue('clientId', sClientId)
            # add version to the log list
            logger.logAddValue('version', version)
            # add linkQuality to the log list
            logger.logAddValue('linkQuality', linkQuality)
            logger.logAddValue('clientComRSSI', clientComRSSIVal)
            logger.logAddValue('proxyComRSSI', proxyComRSSIVal)
            # add netcurrent to the log list
            logger.logAddValue('netCurrent', netCurrent)
            # add batteryVoltage to the log list
            logger.logAddValue('batteryVoltage', batteryVoltage)
            # add system Temp to the log list
            logger.logAddValue('systemTemp', systemTemp)
            # add batteryLevel to the log list
            logger.logAddValue('batteryLevel', batteryLevel)
            # add averagePower to the log list
            logger.logAddValue('averagePower', averagePower)
            # add peakPower to the log list
            logger.logAddValue('peakPower', peakPower)
            # add queryFaileCount to the log list
            logger.logAddValue('queryFailedCount', queryFailedCount)
            logger.logAddValue('QueryTime', queryTime)
            # add Distances to the log list
            logger.logAddValue('DistanceZ1', int(zDistance1))
            logger.logAddValue('DistanceZ2', int(zDistance2))
            logger.logAddValue('DistanceX1', int(xDistance1))
            logger.logAddValue('DistanceX2', int(xDistance2))
            logger.logAddValue('DistanceY1', int(yDistance1))
            logger.logAddValue('DistanceY2', int(yDistance2))
            # add client data values to the log list
            logger.logAddValue('MaxZCSCNT_1', MaxZCSCNT_1)
            logger.logAddValue('MaxCalcPwr_1', MaxCalcPwr_1)
            logger.logAddValue('MaxZCSCNT_2', MaxZCSCNT_2)
            logger.logAddValue('MaxCalcPwr_2', MaxCalcPwr_2)
            logger.logAddValue('MaxZCSCNT_3', MaxZCSCNT_3)
            logger.logAddValue('MaxCalcPwr_3', MaxCalcPwr_3)
            logger.logAddValue('MaxZCSCNT_4', MaxZCSCNT_4)
            logger.logAddValue('MaxCalcPwr_4', MaxCalcPwr_4)
            logger.logAddValue('AvgZCSCNT_1', AvgZCSCNT_1)
            logger.logAddValue('AvgCalcPwr_1', AvgCalcPwr_1)
            logger.logAddValue('AvgZCSCNT_2', AvgZCSCNT_2)
            logger.logAddValue('AvgCalcPwr_2', AvgCalcPwr_2)
            logger.logAddValue('AvgZCSCNT_3', AvgZCSCNT_3)
            logger.logAddValue('AvgCalcPwr_3', AvgCalcPwr_3)
            logger.logAddValue('AvgZCSCNT_4', AvgZCSCNT_4)
            logger.logAddValue('AvgCalcPwr_4', AvgCalcPwr_4)
            logger.logAddValue('Model', Model)
            logger.logAddValue('DeviceStatus', DeviceStatus)
            logger.logAddValue('TPSMissed', TPSMissed)
            # add SA values to the log list
            if options.average :
                logger.logAddValue('SAPowerAvg', SAPower)
                logger.logAddValue('SAFreqAvg', SAFreq)
            else :
                logger.logAddValue('SAPowerPeak', SAPower)
                logger.logAddValue('SAFreqPeak', SAFreq)

            # add the time entry
            tm_obj = localtime(time())
            datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
            logger.logAddValue('timetaken', datatime)

            if options.loglongbcnt :
                v = getClientCommandData(tx, sClientId, '42')
                longBeaconCnt = jcs.getCommandData(v,42, 5, 4) # offset 5, num of bytes 4
                numBeaconCnt = jcs.getCommandData(v,42, 1, 4) # offset 1, num of bytes 4
                logger.logAddValue("LongBeaconCnt", longBeaconCnt)
                logger.logAddValue("NumBeaconCnt", numBeaconCnt)

                # get client command data 20
                v = getClientCommandData(tx, sClientId, '20')
                watchDogRstCnt = jcs.getCommandData(v, 20, 7, 2) # offset 7, num of bytes 2
                logger.logAddValue("WatchDogRstCnt", watchDogRstCnt)

            if logger.logResults() :
                eprint("ERROR: Couldn't log values for Client {}.".format(sClientId))
                finished = True
                break

            if peakPower >= options.maxrssi :
                eprint("ALERT: peakPower value exceeded peakPower limit: {} < {} cnt {}\n".format(options.maxrssi, peakPower, iterateCntUp-1))
                if (iterateCntUp-1) >= 3 :
                    finished = True

    return finished, maxPeakPower, maxBatteryLevel

def eprint(msg) :
    print("E {}".format(msg))
    emailer.setResults(msg)

def main(tstList, options, logger) :

    MYSTART_TIME = time()
    clientList = list()
    # init the TX communication object
    tx=TXComm(options.txipaddr, int(options.txport), options.debug)
    # setup the SA

    sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
    if sa.usesa :
        eprint("INFO: Using SA at {} port {}".format(options.saipaddr, sa.port))

    comChanNum = '25'
    cota_config = "ND"
    try :
        cota_config, plist, fullCfgDict = getCotaConfigValues(options)
        comChanNum = plist["Client COM Channel"]
    except Exception as ex :
        eprint("ERROR: getCotaConfigValues exception -- {}".format(repr(ex)))

    # If no client id specified then look for one with the Transmitter
    clientId = options.clientid
    errorCondition = False
    argv = ["get_charger_id"]
    v = tx.sendGetTransmitter(argv)
    txId = jcs.getTxId(v)
    if clientId is None :
        minLinkQ = int(options.minlinkq)
        # none specified so find one
        argv = ["client_list"]
        v = tx.sendGetTransmitter(argv)
        availableClients = v['Result']['Clients']

        # if one or more clients found then choose the first three with the highes LinkQ values greater than the minimum
        errorCondition = True
        if len(availableClients) > 0 :
            for client in availableClients :
                lnkq = 0
                sClientId = str("")
                try :
                    lnkq = int(client['LinkQuality'])
                    sClientId = str(client['Client ID'])
                except ValueError as ve :
                    eprint("ERROR: LinkQ ValueError {} -- {}".format(repr(client), ve))
                if minLinkQ < lnkq :
                    clientList.append([sClientId, lnkq])
                    if len(clientList) == 3:
                        break
                if options.debug :
                    print("found client {} lnkq {}".format(sClientId, lnkq))
            # report something
            if len(clientList) > 0 :
                errorCondition = False
                if options.console :
                    for client in clientList :
                        eprint("INFO: Using client {} LnkQ {}".format(client[0], client[1]))
            else :
                eprint("ERROR: No clients found that meet the criteria of > {}".format(minLinkQ))
                for client in availableClients :
                    eprint("INFO: Client {} LinkQuality {}".format(client['Client ID'], client['LinkQuality']))
        else :
            # none found and none specified
            eprint("ERROR: No clients found")
        # a client was specified so lets just use it
    else : # make a client list
        clst = clientId.split(',')
        for cl in clst :
            clientList.append([cl, "85"])

    if not errorCondition :
        # first register the client

        eprint("INFO: The Client list: {}".format(repr(clientList)))

        pmusEnabled = ""
        recsEnabled = ""
        clientFW = ""
        for client in clientList :
            clientId = client[0]
            try :
                sClientId = str(clientId)
                argv = ['register_client', sClientId]
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ERROR: Register client {} failed".format(sClientId))
                    return
                # configure the client
                argv = ['client_config', sClientId, '0x6']
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ERROR: Client config failed")
                    return
                sleep(12)
                # check for a non zero Short ID
                argv = ['client_detail', sClientId]
                v = tx.sendGetTransmitter(argv)
                shortID = jcs.getClientShortID(v)
                if shortID == "0x0000" :
                    eprint("ERROR: Short ID shows device is not registered: " + shortID)
                    return
                # get the PMUs and RECs that are enabled
                if len(pmusEnabled) > 0 :
                    pmusEnabled += " "
                    recsEnabled += " "
                v = getClientCommandData(tx, sClientId, '17')

                tmppmus = jcs.getCommandData(v, 17, 1, 4, False)
                pmusEnabled += "{}_{}".format(sClientId, tmppmus)

                v = getClientCommandData(tx, sClientId, '33')

                tmprecs = jcs.getCommandData(v, 33, 1, 4, False)
                recsEnabled += "{}_{}".format(sClientId, tmprecs)

                v = getClientCommandData(tx, sClientId, '61')

                tmpfw = jcs.getCommandData(v, 61, 1, 4)
                clientFW += "{}_{:08X}".format(sClientId, tmpfw)

                eprint("INFO: Client {} -- PMUs {} RECs {} FW {:08X}".format(sClientId, tmppmus, tmprecs, tmpfw))
                #
            except ValueError as ve :
                eprint("ERROR: ClientId ValueError {} -- {}".format(clientId, ve))
                return

        ## get the Transmiter data values for logging.

        # first get the Transmitter data that we want to log
        #    This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp

        logger.initFile(TXCMS)

        if options.logtodb :
            logger.initDB(options.dbname, hostnm=DEFAULTDBSERVER)
            logger.initDBTable(options.tblname)
        # add options Dict to log list
        val =  repr(options.__dict__).replace("'","").replace('"',"").replace(","," ").replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
        logger.logAddValue("options", val)

        # add txId to log list
        logger.logAddValue("txId", txId)
        logger.logAddValue("AntennaType",options.antennatype)
        logger.logAddValue("CfgComment",options.logcomments)
        logger.logAddValue("CotaConf",cota_config)
        # add OnChannels to log list
        logger.logAddValue("PmusEnabled", pmusEnabled)
        logger.logAddValue("RecsEnabled", recsEnabled)
        OnChannels, ChanList = getOnChannels(tx)
        verstr = getVersions(tx)
        logger.logAddValue("OnChannels", OnChannels)
        v = tx.sendGetTransmitter(['get_good_channels'])
        logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
        logger.logAddValue("Versions", str(verstr)+"\n"+clientFW)
        logger.logAddValue("AmbRev", str(ChanList))
        logger.logAddValue("LogFileName", logger.filename)
        tableTS = str(int(logger.timeStampNum))
        ipnum = gethostbyname(options.txipaddr).split(".")[-1]
        CLIENTTABLENAME = "client_{}_{}".format(ipnum, tableTS)
        logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
        SATABLENAME = "sa_{}_{}".format(ipnum, tableTS)
        logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
        logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)


        eprint("INFO: DataBase: {}, Tablename: {}, Test Timestamp: {}".format(options.dbname,
                                                                        options.tblname,
                                                                        logger.timeStamp))
        eprint("INFO: Charger name: {}, Antenna Type: {}, Comments: '{}'".format(options.txipaddr,
                                                                           options.antennatype,
                                                                           options.logcomments))
        eprint("INFO: System versions\n{}".format(verstr))

        eprint("INFO: Cota Config File:");

        boldlist = [ "System Type", "Client COM Channel", "Client Query Period", "Reference ACL"]
        for n in boldlist :
            eprint("INFO: {:30} == {}".format(n, fullCfgDict.get(n, n)))            

        for n, v in fullCfgDict.iteritems() :
            if n not in boldlist :
                eprint("INFO: {:30} == {}".format(n, v))            

        if logger.logResults() :
            eprint("ERROR: Couldn't log values for Transmitter.")
            return

        # make the logging list
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
            # the the SA
            sa.setSAValue(cmd_data[0], cmd_data[1])
        sa.referenceLevelOffset = 20.0 + options.cableloss
        sacms.append(["referenceLevelOffset", "%s", 'VARCHAR(35)'])


        # init the logging list
        logger.initFile(sacms,how='a')
        if options.logtodb :
            logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                logger.logAddValue(cmd_data[0], cmd_data[1])
        logger.logAddValue("referenceLevelOffset", str(20.0 + options.cableloss))
        if logger.logResults() :
            eprint("ERROR: Couldn't log values for SA.")
            return

        clientcms = CLIENTCMS
        clientcms.insert(4,["ImageSA", "%s", 'VARCHAR(100)'])
        if options.average :
            clientcms.insert(4,["SAPowerAvg", "%3.2f", 'VARCHAR(10)'])
            clientcms.insert(4,["SAFreqAvg", "%6.4e", 'VARCHAR(10)'])
        else :
            clientcms.insert(4,["SAPowerPeak", "%3.2f", 'VARCHAR(10)'])
            clientcms.insert(4,["SAFreqPeak", "%6.4e", 'VARCHAR(10)'])

        # setup the logging file
        if int(options.calibrate) > 0 :
            clientcms.append(["calibrate", "%s", 'VARCHAR(10)'])
        if options.debug :
            print("FNAME={}".format(logger.filename))
            print("COLFMT={}".format(logger.colformat))

        logger.initFile(clientcms,how='a')
        if options.logtodb :
            logger.initDBTable(CLIENTTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        X1Angle = 0.0
        X1Offset = 0
        X2Angle = 0.0
        X2Offset = 0
        if options.genx1dist is not '' :
            x1 = options.genx1dist.split(',')
            try :
                X1Angle = float(x1[0])
                X1Offset = int(x1[1])
            except Exception :
                pass
        if options.genx2dist is not '' :
            x2 = options.genx2dist.split(',')
            try :
                X2Angle = float(x2[0])
                X2Offset = int(x2[1])
            except Exception :
                pass

        lstDistance1 = 0
        lstDistance2 = 0
        finished = False
        lstAngle = 0.0
        #for i in range(len(tstList)) :
        #    itTstDict = tstList[i]
        calJustOnce = options.caljustonce
        for itTstDict  in tstList :

            try :
                itTstDict['warmup']     = int(itTstDict.get('warmup', 30))
                itTstDict['txangle']    = float(itTstDict.get('txangle', 0.0))
                itTstDict['zdistance1'] = int(itTstDict.get('zdistance1', 1000))
                itTstDict['zdistance2'] = int(itTstDict.get('zdistance2', 1000))
            except ValueError as e :
                eprint("ERROR: ValueError converting warmup and txangle {}".format(repr(e)))
                break

            hrs, mins, secs, _ = DurationCalc(MYSTART_TIME)

            Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
            print("{} Distance1 {} > {} Distance2 {} > {} finished {}\n".format(Duration, lstDistance1, itTstDict['zdistance1'],
                                                                             lstDistance2, itTstDict['zdistance2'], repr(finished)))
            # if we finished (maxpwr exceeded) and the next distance is closer the skip it
            if finished and (lstDistance1 > itTstDict['zdistance1'] or lstDistance2 > itTstDict['zdistance2']) :
                print("        Distance1 {} > {} Distance2 {} > {} finished {}\n".format(lstDistance1, itTstDict['zdistance1'],
                                                                                 lstDistance2, itTstDict['zdistance2'], repr(finished)))
                continue
            # if the next angle is smaller (more direct) and the next disance is closer or the same then skip
            print("        txAngle {} > {} Distance1 {} > {} Distance2 {} > {} finished {}\n".format(abs(lstAngle), abs(itTstDict['txangle']), 
                        lstDistance1, itTstDict['zdistance1'],
                        lstDistance2, itTstDict['zdistance2'], repr(finished)))
            if finished and abs(lstAngle) > abs(itTstDict['txangle']) and lstDistance1 > itTstDict['zdistance1'] and lstDistance2 > itTstDict['zdistance2'] :
                print("        txAngle {} > {} Distance1 {} > {} Distance2 {} > {} finished {}\n".format(abs(lstAngle), abs(itTstDict['txangle']), 
                        lstDistance1, itTstDict['zdistance1'],
                        lstDistance2, itTstDict['zdistance2'], repr(finished)))
                continue
            lstDistance1 = itTstDict['zdistance1']
            lstDistance2 = itTstDict['zdistance2']
            lstAngle = itTstDict['txangle']
            powerLevel = 13
            try :
                powerLevel = int(itTstDict.get('powerlevel', 13))
            except ValueError as e :
                eprint("ERROR: ValueError converting powerlevel {}".format(repr(e)))
                powerLevel = 13
            powerLevel = max(powerLevel, 13)
            powerLevel = min(powerLevel, 21)
            powerLevel = gPWR_LEVEL_MAP.get(powerLevel, 13)
            argv = ['set_power_level', str(powerLevel)]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ERROR: Set Power Level failed")
                return
            argv = ['get_power_level']
            v = tx.sendGetTransmitter(argv)
            pl =  jcs.getTransmitterPowerLevel(v)
            if pl != powerLevel :
                eprint("ALERT: Power level incorrect get={} vs set={}".format(pl, powerLevel))
                powerLevel = pl
            itTstDict['powerlevel'] = powerLevel
            itTstDict['xdistance1'] = makeAngleDistance(X1Angle, itTstDict['zdistance1'], X1Offset)
            itTstDict['xdistance2'] = makeAngleDistance(X2Angle, itTstDict['zdistance2'], X2Offset)

            # this function also logs all the client type data to the file and database.
            itTstDict.update({'caljustonce': calJustOnce})
            try :
                finished, peakPower, batteryLevel = runthecharacter(sa, tx, clientList, itTstDict, logger, options, comChanNum)
                eprint("INFO: Distance {} Angle {} PeakPower {}".format(itTstDict.get('zdistance1', 0), itTstDict.get('txangle', 0.0), peakPower))
                if options.holdatpeak > 0.0 and peakPower >= options.holdatpeak :
                    for _ in range( options.holdcount) :
                        finished, peakPower, batteryLevel = runthecharacter(sa, tx, clientList, itTstDict, logger, options, comChanNum)
                        eprint("INFO: Distance {} Angle {} PeakPower {}".format(itTstDict.get('zdistance1', 0), itTstDict.get('txangle', 0.0), peakPower))
                        if batteryLevel > options.batterylevel :
                            break
                    finished = True
            except KeyboardInterrupt as ki :
                eprint("ERROR: Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                finished = True
                break
            except Exception as ex :
                eprint("ERROR: Unexpected Exception exiting --- {} -- {}\n".format(repr(ex), repr(itTstDict)))
                eprint("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
                eprint("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
                finished = True
                break
            calJustOnce = False

        for client in clientList :
            sClientID = str(client[0])
            argv = ['stop_charging', sClientID]
            print("At End Stop charging client {} argv = {}\n".format(sClientID, repr(argv)))
            v = tx.sendGetTransmitter(argv)


def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict

def DurationCalc(START_TIME) :
    duration = int(time() - START_TIME)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    return hrs, mins, secs, Duration

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser(option_class=myOption)

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None, help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg", help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlistLin", help="Use the named list from the file (.py).")
    parser.add_option("-p","--saport",     dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    parser.add_option("-i","--saip",       dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")

    parser.add_option("","--sacentfreq",   dest='sacentfreq',   type=str,  action='store',default="2.45 GHZ",help="Set the SA Center Frequency")
    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=sTXIP,help="Transmitter network address.")

    parser.add_option("","--actip",        dest='actuator',     type=str,  action='store',default=None,help="Linear Actuator network address(es) (comma sep).")

    parser.add_option("","--holdatpeak",   dest='holdatpeak',   type=float,action='store',default=-1.0,help="Hold at the requested peak value and capture data.")
    parser.add_option("","--holdcount",    dest='holdcount',    type=int,  action='store',default=1,help="The number of data captures at the requested peak value.")
    parser.add_option("","--batterylevel", dest='batterylevel', type=int,  action='store',default=98,help="The battery level to quit test at.")

    parser.add_option("-c","--clientid",   dest='clientid',     type=str,  action='store',default=None,help="Device/client ID to use for characterization.")
    parser.add_option("","--minlinkq",     dest='minlinkq',     type=int,  action='store',default='85',help="Minimum link quality to validate a client.")
    parser.add_option("","--maxpwrlevel",  dest='maxrssi',      type=float,action='store',default='28.0',help="Maximum power level to quit test: a client protection.")

    parser.add_option("","--cableloss",    dest='cableloss',    type=float,action='store',default=0.0,help='Cable loss positive dB. Added to the 20 dB of Connector loss')
    parser.add_option("","--calibrate",    dest='calibrate',    type=int,  action='store',default="0",help="Number data points between calibrations.")
    parser.add_option("","--poscalibrate", dest='poscalibrate',            action='store_false',default=True,help="Calibrate at each position change.")
    parser.add_option("","--calibratealg", dest='calibratealg',            action='store_true',default=False,help="Use an algorithm to obtain a relatively good calibration.")
    parser.add_option("","--caljustonce",  dest='caljustonce' ,            action='store_false',default=True,help="Use an algorithm to obtain a relatively good calibration.")

    parser.add_option("","--genx1dist",    dest='genx1dist',    type=str,  action='store',default="0.0,0",help="Generate X1 distance from angle.[optional offset]")
    parser.add_option("","--genx2dist",    dest='genx2dist',    type=str,  action='store',default="0.0,0",help="Generate X2 distance from angle.[optional offset]")
    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="IntegLab1LogFile{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--noconsole",    dest='console',                 action='store_true',default=False,help="Turn off printing to console")
    parser.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    parser.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--loglongbcnt",  dest='loglongbcnt',             action='store_true',default=False,help="Log the long beacon count info")
    parser.add_option("","--saimage",      dest='saimage',      type=int,  action='store',default=0,help="Capture an SA Image on every data point.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        eprint("ERROR: Extra command line args, exiting")
        eprint("ERROR: {}".format(repr(args)))
    elif options.cfgfile is not None :
        TTYPE = 0
        TDEF = 1
        import imp
        fname = options.cfgfile
        if fname == '' :
            fname = sys.argv[0].replace('.', 'Conf.')
        cfgmod = imp.load_source(options.cfgname, fname)
        cfg = eval("cfgmod."+options.cfgname)
        optType = makeOptionTypeDict(parser)
        for k in cfg.keys() :
            try :
                if optType[k][TTYPE]  == 'int' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = int(str(cfg[k]))
                elif optType[k][TTYPE]  == 'float' :
                    if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = float(str(cfg[k]))
                elif optType[k][TTYPE]  is None :
                    options.__dict__[k] = optType[k][TDEF]  # set the default value
                    try :
                        options.__dict__[k] = eval(cfg[k])
                    except NameError :
                        options.__dict__[k] = False
                    except TypeError :
                        options.__dict__[k] = cfg[k]

                elif optType[k][TTYPE]  == 'string' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    elif str(cfg[k]) == 'None' :
                        options.__dict__[k] = None
                    else :
                        options.__dict__[k] = str(cfg[k])
                else :
                    options.__dict__[k] = str(cfg[k])
                #print("name={} def={} type={} in={} cur={}\n".format(k, optType[k][TDEF], optType[k][TTYPE], cfg[k], options.__dict__[k]))
            except ValueError as ve :
                eprint("ERROR: {} -- Incorrect option value {} -- {}\n".format(k, cfg[k], repr(ve)))
                exit(2)
            except IndexError as ie :
                eprint("ERROR: {} -- No such option -- {}\n".format(k, repr(ie)))
                exit(3)
            except Exception as e :
                eprint("ERROR: {} -- Unknown exception -- {}\n".format(k, repr(e)))
                exit(4)

    fname = sys.argv[0].replace('.', 'Lists.')

    if options.testlistfile is not None :
        fnm = options.testlistfile
        if fnm != '' :
            fname = fnm

    import imp
    tstlstmod = imp.load_source(options.testlistname, fname)
    testlistE = eval("tstlstmod."+options.testlistname)
    eprint("INFO: Starting a Characterization test on host {} using the specified client(s) {} on an actuator named {}\n".format(options.txipaddr, options.clientid, options.actuator))

    if options.debug :
        print("options = {}".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
        print("testlist = {}".format(repr(testlistE).replace('},','},\n')))
    eprint("INFO: Executing main\n")

    logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
    main(testlistE, options, logger)
    hrs, mins, secs, _ = DurationCalc(START_TIME)

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    fd = open(logger.filename, 'a')
    if fd :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()
    if options.logtodb :
        try :
            logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
        except Exception as e : 
            eprint("ERROR: Logging the Duration failed {}\n".format(repr(e)))

    eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    emailer.emailResults()
