''' PwrCharTestPassFail.py -- Defines the pass fail limits for the characterization test

Version : 1.0.0
Date : June 25 2018
Copyright Ossia Inc. 2018

The data is split into two sections. One for power measured from a Spectrum analyzer
and one for power reported by the client.
If no spectrum analyzer is present then the client data will be analyzed

There is a list of two values, [min, max] for each position that limits are important to be applied
In addition the min, max values can be set a client ID indexed dictionary of min, max lists.

'''
#
# pass fail criteria for PwrCharTest
#
pwrPF = {'SA'    : {1000 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1200 : {  0.0 : [{'Client_one'   : 30.0,
                                      'Client_three' : 32.0},
                                     {'Client_one'   : 40.0,
                                      'Client_three' : 35.0}],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1500 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1700 : {  0.0 : [{'Client_three' : 30.0,
                                      'Client_one'   : 20.0},
                                     {'Client_three' : 40.0,
                                      'Client_one'   : 35.0}],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [{'Client_two' : 35.0},
                                     {'Client_two' : 40.0}],
                            -45.0 : [-10.0, 40.0]},
                    2000 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                 },
        'Client' : {1000 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1200 : {  0.0 : [{'Client_one'   : 30.0,
                                      'Client_three' : 32.0},
                                     {'Client_one'   : 40.0,
                                      'Client_three' : 35.0}],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1500 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    1700 : {  0.0 : [{'Client_three' : 30.0,
                                      'Client_one'   : 20.0},
                                     {'Client_three' : 40.0,
                                      'Client_one'   : 35.0}],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [{'Client_two' : 35.0},
                                     {'Client_two' : 40.0}],
                            -45.0 : [-10.0, 40.0]},
                    2000 : {  0.0 : [ 30.0, 40.0],
                             90.0 : [-10.0, 40.0],
                            -90.0 : [-10.0, 40.0],
                             45.0 : [-10.0, 40.0],
                            -45.0 : [-10.0, 40.0]},
                    } }
