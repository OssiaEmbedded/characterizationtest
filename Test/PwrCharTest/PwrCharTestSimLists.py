testlistSimTest=[
{'logcount' : 3,'powerlevel' : 21,'warmup' :  2,'txangle' :  45,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :  45,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :  45,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :  45,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :  45,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :   0,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :   0,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :   0,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :   0,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' :   0,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' : -45,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' : -45,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' : -45,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' : -45,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  1,'txangle' : -45,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 1,},
]
testlistRealHW=[
{'logcount' : 0,'powerlevel' : 20,'warmup' :200,'txangle' :   0,'zdistance' : 1100,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  2,'txangle' :  45,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :  45,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :  45,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :  45,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :  45,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},

{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :   0,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :   0,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :   0,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :   0,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' :   0,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},

{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' : -45,'zdistance' : 1700,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' : -45,'zdistance' : 1500,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' : -45,'zdistance' : 1300,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' : -45,'zdistance' : 1000,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 5,'powerlevel' : 20,'warmup' :  1,'txangle' : -45,'zdistance' :  900,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
{'logcount' : 0,'powerlevel' : 13,'warmup' :  1,'txangle' :   0,'zdistance' :    0,'passfailmax' : 100,'passfailmin' : -100,'waitfordata' : 5,},
]
