#  'passfailmin'
#  'passfailmax'
#  'saleaecmd'
#  'saleaerdcmd'
#  'dutcmd'
#  'dutrdcmd'
#  'tstdevcmd'
#  'tstdevrdcmd'
#  'localcmd'
#  'processvcd'
cmdlist = [
                [{'tstdevrdcmd' : ['ps', 'aux', "| grep", '  Logic',      "| grep -v grep"]},
                 {'tstdevrdcmd' : ['/home/ossiadev/bin/startlogic']},
                 {'localcmd'    : ['sleep', "(5.2)"]},
                 {'dutcmd'      : ['test1', "Just a test"]},
                 #{'tstdevrdcmd' : ['ps', 'aux', "| grep", '  startlogic', "| grep -v grep"]},
                 #{'tstdevrdcmd' : ['ps', 'aux', "| grep", '  Logic',      "| grep -v grep"]},
                 {'saleaerdcmd' : ['GET_CONNECTED_DEVICES']},
                 #{'saleaerdcmd' : ['SELECT_ACTIVE_DEVICE','1']},
                 {'saleaerdcmd' : ['LOAD_FROM_FILE', "/home/ossiadev/logicSettings/tps_rssi_vote.logicsettings"]},
                 {'localcmd'    : ['sleep', "(1.2)"]},
                 {'saleaerdcmd' : ['SET_ACTIVE_CHANNELS', 'digital_channels', '0', '1', '2', '3', '4','5']},
                 {'saleaerdcmd' : ['GET_ACTIVE_CHANNELS']},
                 {'saleaecmd'   : ['CAPTURE']},
                 {'localcmd'    : ['sleep', "(3.2)"]},
                 #{'saleaerdcmd' : ['STOP_CAPTURE']},
                 #{'localcmd'    : ['sleep', "(1.2)"]},
                 {'saleaerdcmd' : ['IS_PROCESSING_COMPLETE']},
                 {'localcmd'    : ['sleep', "(1.2)"]},
                 {'saleaerdcmd' : ['EXPORT_DATA2', '/home/ossiadev/vcdData/tps_rssi_vote', 'ALL_CHANNELS', 'TIME_SPAN', '0.0', '0.080', 'VCD, ']},
                 {'saleaerdcmd' : ['EXIT']},
                 #{'tstdevrdcmd' : ['ps', 'aux', "| grep", ' Logic', "| grep -v grep"]},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'names',      'ms', 'ALL', 'None', 'arg0', 'None', 'None']},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'stats',      'us', 'stats', 'None', 'arg0', 'None', 'None']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'pulse_rise', 'us', 'RX_D5', '3,20', 'arg0', '599.0', '601.0']},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'pulse_fall', 'ms', 'RX_D5', '3,20', 'arg0', '19.0', '20.0']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'pulse_rise', 'ns', 'PU_D6', '1,0',  'arg0', '78.0', '88.0']},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'pulse_fall', 'us', 'PU_D6', '2,0',  'arg0', '290.0', '330.0']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'clock',      'us', 'SCLK_D13',    '17,96', 'arg0', '5.0', '100.0']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'spi',        'us', 'MOSI_D11,SCLK_D13,CS_D10', 96, 'arg0', '0xff:1','0xA0:0x7F:0x94:0x88:0x80:0x94']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'relative',   'us', 'RX_D5,PU_D6', 'rise,rise,20', 'arg0', '290.0', '301.0']},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'relative',   'us', 'RX_D5,PU_D6', 'fall,rise,20', 'arg0', '19.0', '21.0']},

                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'relative',   'us', 'PU_D6,PU_D6', 'rise,rise,0',    'arg0', '324.0', '328.0']},
                 {'processvcd'   : ['/home/ossiadev/Test/CLTH/vcdData/tps_rssi_vote.vcd', 'relative',   'ms', 'PU_D6,PU_D6', 'rise,rise,5000', 'arg0',  '19.0',  '20.0']},
                 ]

]
