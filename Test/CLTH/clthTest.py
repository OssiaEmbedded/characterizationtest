''' clthTest.py -- Setup saleae for signal acquisition acquire data and analyze 

Version : 0.0.0
Date : Aug 31 2019
Copyright Ossia Inc. 2019

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback, inspect
if "/home/ossiadev/pylib" not in sys.path  :
    sys.path.insert(0, "/home/ossiadev/pylib")

from LIB.utilmod import *
import json
from time import sleep, time, localtime
from optparse import OptionParser

from LIB.logResults import LogResults
from LIB.actuator import Actuator
import LIB.timestamp as TSUtil
from LIB.utilmod import *
import LIB.sshcomm as sshcomm
import SALEAE.SALEAECom as sal
import SALEAE.SaleaeVcd as salvcd

from subprocess import check_output as spcheck_output
from subprocess import STDOUT as spSTDOUT
from os.path import basename as BaseName

TESTDBNAME   = 'CLTHTest'

RESULTSTABLENAME = "ResultsTable"
TIMESTAMPCOLNAME = "DataTimeStamp"
DEFAULTDBSERVER  = 'ossia-build'

# data base definition constants
TESTSCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
            ["CfgComment", "%s", 'VARCHAR(512)'],
            ["PostTestNotes", "%s", 'VARCHAR(512)'],
            [RESULTSTABLENAME, "%s",'VARCHAR(40)'],
            ["LogFileName", "%s", 'VARCHAR(100)'],
            ["Duration", "%s", 'VARCHAR(20)'],
            ["CfgFilename", "%s", 'VARCHAR(100)'],
            ["CfgName", "%s", 'VARCHAR(100)'],
            ["CmdListFilename", "%s", 'VARCHAR(100)'],
            ["CmdListName", "%s", 'VARCHAR(100)'],
            ["TestName", "%s", 'VARCHAR(100)']]

RSLTSCMS = [["CmdType", "%s", 'VARCHAR(35)'],
            ["SignalList", "%s", 'VARCHAR(255)'],
            ["PassFailMin", "%s", 'VARCHAR(25)'],
            ["TestValue", "%s", 'VARCHAR(255)'],
            ["PassFailMax", "%s", 'VARCHAR(125)'],
            ["TestResult", "%s", 'VARCHAR(5)'],
            ["VCDIndex", "%d", 'VARCHAR(15)']]

class ClthTest(object) :

    def __init__(self, options) :

        optdict = options.__dict__
        self.actip      = optdict.get('dutip',None) 
        self.actport    = optdict.get('dutport',None) 
        self.saleaeip   = optdict.get('saleaeip',None) 
        self.saleaeport = optdict.get('saleaeport',None) 
        self.tstdevip   = optdict.get('tstdevip',None) 
        self.tstdevport = optdict.get('tstdevport',None) 
        self.debug      = options.debug

        eprint("INFO: CfgOpts actip={}, actport={}, saleaeip={}, saleaeport={}, tstdevip={}, tstdevport={}".format( self.actip,
                                                                                                                   self.actport,
                                                                                                                   self.saleaeip,
                                                                                                                   self.saleaeport,
                                                                                                                   self.tstdevip,
                                                                                                                   self.tstdevport))

    def dutCommand(self, cmdlist, rw=1) :
        """ dutCommand -- sends the cmdlist to the defined dut funtion if any.
                          Many of these may be defined depending on the DUT
        """
        rtnval = "Nothing done"
        if self.dutFunction is not None :
            rtnval = (self.dutFunction)(cmdlist, rw)
        return rtnval


    def actCommand(self, cmdlist, rw=1) :
        """ actCommand -- takes the cmdlist and format it correctly and
                          sends it to the act. Returns data if rw is 0
                          Many of these may be defined depending on the DUT
        """
        if self.actip is None or self.actport is None :
            return None

        a = Actuator(self.actip, self.actport)
            
        strval = a.makeCommandStringFromList(cmdlist)
        if self.debug :
            eprint("DEBUG: actCommand={}".format(strval))
        try :
            a.sendData(strval)
        except Exception as ee :
            eprint("ERROR: Exception in actCommand -- {}".format(ee))
        cnt = 60
        data = "None Expected"
        if rw == 0 :
            data = ""
            while a.testSock() is not None and cnt > 0 :
                d =  a.getData(nonBlock=True)
                if len(d) == 0 :
                    cnt -= 1
                    eprint("ALERT: actCommand read timeout  -- {}".format(cnt))
                    #sys.stdout.flush()
                else :
                    data += str(d)
        if self.debug :
            eprint("DEBUG: actCommand datatype={} data='{}'".format(type(data), data))
        return str(data)

    def tstDevCommand(self, cmdlist, rw=1) :
        """ tstDevCommand -- takes the cmdlist and formats it correctly and
                             sends it to the test computer. Returns data if rw is 0
                             Currently there are three functions:
                                start the Logic software on the test computer
                                exit the Logic software
                                copy the Logic output VCD file to the program for analysis
        """
        if self.tstdevip is None or self.tstdevport is None :
            return None

        cmd = list()
        cmdstr = str(cmdlist[0])
        for arg in cmdlist[1:] :
            try :
                if arg == ';' :
                    cmd.append(cmdstr)
                    cmdstr = ""
                    continue
                cmdstr += " "+str(arg)
            except ValueError as ve :
                eprint("ERROR: tstDevCommand ValueError -- {}".format(ve))

        cmd.append(cmdstr)
        if self.debug :
            eprint("DEBUG: tstDevCommand={}".format(cmd))

        rtnval = 1
        lines = ()
        errors = ()
        com = sshcomm.SSHlink(self.tstdevip, self.tstdevport, "ossiadev", 'OssiaDev!', False)
        if not com.connect() :
            eprint("ERROR: Could not connect to target '{}'".format("linux-pc-os0195"))
        else :
            for cmdstr in cmd :
                if self.debug :
                    eprint("DEBUG: tstDevCommand just before send cmdstr='{}'".format(cmdstr))
                rtnval += com.command(cmdstr)
                lines  += com.getlastreadlines()
                errors += com.getlastreaderrors()
            com.close()
            if self.debug :
                eprint("DEBUG: tstDevCommand rtnval={}".format(rtnval))
                eprint("DEBUG: tstDevCommand lines={}".format(lines))
                eprint("DEBUG: tstDevCommand errors={}".format(errors))
        return rtnval, lines, errors


    def saleaeCommand(self, cmdlist, rw=1) :
        """ saleaeCommand -- takes the cmdlist and format it correctly and
                             sends it to the saleae. Returns data if rw is 0
        """
        if self.saleaeip is None or self.saleaeport is None :
            return None

        saleae = sal.SALEAECom(self.saleaeip, self.saleaeport)
        cmdstr = cmdlist[0] 
        for arg in cmdlist[1:] :
            try :
                cmdstr += ", "+str(arg)
            except ValueError as ve :
                eprint("ERROR: saleaeCommand ValueError -- {}".format(ve))

        if self.debug :
            eprint("DEBUG: saleaeCommand={}".format(cmdstr))
        saleaecmdrtn = "None Expected"
        if rw == 0 :
            saleaecmdrtn = saleae.sendGetSal(cmdstr)
            if saleaecmdrtn.endswith("ACK") :
                saleaecmdrtn = saleaecmdrtn.rstrip("ACK").strip()
                saleaecmdrtn += " ACK"
        else :
            saleae.sendSal(cmdstr)
        if self.debug :
            eprint("DEBUG: saleaeCommand rtval='{}'".format(saleaecmdrtn))
        return saleaecmdrtn

    def localCommand(self, cmdlist, rw=1) :
        """ localCommand -- takes the cmdlist and format it correctly and
                             evaluates it. e.g. sleep or any valid python function. Returns data if rw is 0
        """
        cmdstr = cmdlist[0] 
        for arg in cmdlist[1:] :
            try :
                cmdstr += str(arg)
            except ValueError as ve :
                eprint("ERROR: localCommand ValueError -- {}".format(ve))

        if self.debug :
            eprint("DEBUG: localCommand={}".format(cmdstr))
        localcmdrtn = "None Expected"
        if rw == 0 :
            localcmdrtn = eval(cmdstr)
        else :
            eval(cmdstr)
        if self.debug :
            eprint("DEBUG: localCommand rtval='{}'".format(localcmdrtn))
        return localcmdrtn

            
    def runCmdEntry(self, cmdentrylist, option, logger) :

        finished = False

        passcnt = 0
        failcnt = 0
        for cmdentry in cmdentrylist :
            try :
                saleaecmd     = cmdentry.get('saleaecmd', None)
                saleaerdcmd   = cmdentry.get('saleaerdcmd', None)
                dutcmd        = cmdentry.get('dutcmd', None)
                dutrdcmd      = cmdentry.get('dutrdcmd', None)
                tstdevcmd     = cmdentry.get('tstdevcmd', None)
                tstdevrdcmd   = cmdentry.get('tstdevrdcmd', None)
                localcmd      = cmdentry.get('localcmd', None)
                processVcd    = cmdentry.get('processvcd', None)
            except ValueError as ve :
                eprint("ERROR: ValueError on cmdentry -- {}\n".format(ve.message), True)
                finished = True
            else :
                if localcmd is not None :
                    localrtnval = self.localCommand(localcmd)
                    eprint("INFO: Local command list {} returned {}".format(localcmd, localrtnval))

                elif saleaecmd is not None :
                    self.saleaeCommand(saleaecmd)
                elif saleaerdcmd is not None :
                    salrtnval = self.saleaeCommand(saleaerdcmd, 0)
                    eprint("INFO: Saleae command list {} returned {}".format(saleaerdcmd, salrtnval))

                elif dutcmd is not None :
                    self.dutCommand(dutcmd)
                elif dutrdcmd is not None :
                    dutrtnval = self.dutCommand(dutrdcmd, 0)
                    eprint("INFO: dut command list {} returned {}".format(dutrdcmd, dutrtnval))

                elif tstdevcmd is not None :
                    self.tstDevCommand(tstdevcmd)
                elif tstdevrdcmd is not None :
                    tstdevrtnval = self.tstDevCommand(tstdevrdcmd, 0)
                    eprint("INFO: tstdev command list {} returned {}".format(tstdevrdcmd, tstdevrtnval))

                elif processVcd is not None :
                    eprint("INFO: BEGIN VCD processing for '{}', using signal(s) '{}' with args {}, minVal {}, maxVal {}.".format(
                           processVcd[1], processVcd[3], processVcd[4], processVcd[-2], processVcd[-1]))
                    rtndict = salvcd.processVcd(processVcd)
                    if self.debug :
                        eprint("DEBUG: processVcd={}\nDEBUG: rtndict={}".format(processVcd, rtndict))
                    eprint("INFO: Filename = {}".format(processVcd[0]))
                    fmt = "INFO: "+rtndict['namefmt']
                    Value = rtndict[processVcd[-3]]
                    eprint(fmt.format(Value))
                    eprint("INFO: Index = {}".format(rtndict['index']))
                    for i in range(len(rtndict)) :
                        name = 'arg{}'.format(i+1)
                        val  = rtndict.get(name, None)
                        if val is not None :
                            eprint("INFO: {}".format(val))
                        else :
                            break

                    pfValStr = ''
                    if rtndict['passfail'] :
                        eprint("INFO: PASS VCD processing for '{}' using signal(s) '{}' with args {}, minVal {}, maxVal {}.\n".format(
                               processVcd[1], processVcd[3], processVcd[4], processVcd[-2], processVcd[-1]))
                        passcnt += 1
                        pfValStr = "PASS"
                    else :
                        eprint("ERROR: FAIL VCD processing for '{}' using signal(s) '{}' with args {}, minVal {}, maxVal {}.\n".format(
                               processVcd[1], processVcd[3], processVcd[4], processVcd[-2], processVcd[-1]))
                        failcnt += 1
                        pfValStr = "FAIL"

                    logger.logAddValue("CmdType", processVcd[1])
                    logger.logAddValue("SignalList", processVcd[3])
                    logger.logAddValue("PassFailMin", processVcd[-2])
                    logger.logAddValue("TestValue", "{}".format(Value))
                    logger.logAddValue("PassFailMax", processVcd[-1])
                    logger.logAddValue("TestResult", pfValStr)
                    logger.logAddValue("VCDIndex", rtndict['index'])
                    rtnval = logger.logResults()

                    if rtnval :
                        eprint("ERROR: Couldn't log values for Test Command List -- {} -- Exiting.".format(rtnval), True)
                        finished = True
                        break 

        return finished



    def main(self, cmdList, option, logger=None) :


        eprint("INFO: DataBase: {}, Tablename: {}, Test Timestamp: {}".format(option.dbname,
                                                                              option.tblname,
                                                                              logger.timeStamp))

        logger.initFile(TESTSCMS)
        if option.logtodb :
            logger.initDB(option.dbname, hostnm=option.dbservername)
            logger.initDBTable(option.tblname)

        logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)
        logger.logAddValue("CfgComment", option.logcomments)
        resultTablename = "{}_{}".format(RESULTSTABLENAME, str(int(logger.timeStampNum)))
        logger.logAddValue(RESULTSTABLENAME, resultTablename)
        logger.logAddValue("LogFileName", logger.filename)
        logger.logAddValue("CfgFilename", option.cfgfile)
        logger.logAddValue("CfgName", option.cfgname)
        logger.logAddValue("CmdListFilename", option.cmdlistfile)
        logger.logAddValue("CmdListName",  option.cmdlistname)
        logger.logAddValue("TestName",  option.testname)
        rtnval = logger.logResults()

        if rtnval :
            eprint("ERROR: Couldn't log values for Test Command List -- {} -- Exiting.".format(rtnval), True)
            return False

        logger.initFile(RSLTSCMS,how='a')
        if option.logtodb :
            logger.initDBTable(resultTablename)

        number_steps_left = len(cmdList)
        finished = True
        for cmdEntryList  in cmdList :

                # this function also logs all the client type data to the file and database.
            try :
                finished = self.runCmdEntry(cmdEntryList, option, logger)
                number_steps_left -= 1
                eprint("INFO: Number of Entries left to do = {}".format(number_steps_left))
            except KeyboardInterrupt as ki :
                eprint("INFO: Sigint = 2 received -- KeyboardInterrupt exiting --- {} iterleft={}".format(ki.message, number_steps_left), True)
                finished = True
                break
            except Exception as ex :
                eprint("ERROR: Unexpected Exception exiting --- {} -- {}\n".format(repr(ex), repr(cmdEntryList)), True)
                eprint("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
                eprint("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
                finished = True
                break
        return finished

if __name__ == '__main__' :

    START_TIME = time()
    tm = printTime("INFO: Starting Program Execution at :", noprnt=True)
    eprint(tm)
    parser = OptionParser()
    cmdstr = ""
    for arg in sys.argv :
        cmdstr += arg + " "
    eprint("INFO: Command line = {}\n".format(cmdstr)) 

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None,       help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default=None,       help="Use a config file (.py) for options")
    parser.add_option("","--cmdlistfile",  dest='cmdlistfile',  type=str,  action='store',default=None,       help="Use a list file (.py) for data collection positions")
    parser.add_option("","--cmdlistname",  dest='cmdlistname',  type=str,  action='store',default=None,       help="Use the named list from the file (.py).")
    parser.add_option("","--dutfuncfile",  dest='dutfuncfile',  type=str,  action='store',default=None,       help="Get the DUT communication function from this file. (.py)")
    parser.add_option("","--dutfuncname",  dest='dutfuncname',  type=str,  action='store',default=None,       help="Use the named function from the funcfile (.py).")

    parser.add_option("","--testname",     dest='testname',     type=str,  action='store',default="TPS",      help="The name of the test.")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",         help="Comments about the test.")

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default=None,       help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default=None,       help="Log file directory.")
    parser.add_option("","--noconsole",    dest='console',                 action='store_true',default=False, help="Turn off printing to console")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=None,       help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=None,       help="Set the DataBase name at ossia-build")
    parser.add_option("","--emailme",      dest='emailme',      type=str,  action='store',default=None,       help="Email me the test summary.")

    parser.add_option("","--dbservername", dest='dbservername', type=str,  action='store',default=DEFAULTDBSERVER,help="Default DBServer name.")

    parser.add_option("","--logtodb",      dest='logtodb',                 action="store_true",default=False,  help="Log data to a data base.")
    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,  help="print debug info.")

    (opts, args) = parser.parse_args()

    import imp
    options = None
    logger = None
    if len(args) > 0 :
        eprint("ERROR: Extra command line args, exiting")
        eprint("ERROR: {}".format(repr(args)))
    elif opts.cfgfile is not None :
        try :
            options = parseConfigFile(parser, opts)
        except RuntimeError :
            exit(1)
        except Exception as e :
            eprint("ERROR: Unknown exception from parseConfgFile -- {}\n".format(repr(e)))
            exit(2)

        eprint("INFO: Regression test '{}' with Comments '{}'".format(options.testname, options.logcomments))

        dutfunction = None
        if options.dutfuncfile is not None and options.dutfuncname is not None :
            try :
                dutfuncmod = imp.load_source(options.dutfuncname, options.dutfuncfile)
                dutfunction = eval("dutfuncmod."+options.dutfuncname)
            except Exception as ee :
                eprint("ERROR: tstlistmod creation did not succeed on cmdlist '{}' and filename '{}' -- {}".format(options.cmdlistname, options.cmdlistfile, repr(ee)))

        try :
            tstlstmod = imp.load_source(options.cmdlistname, options.cmdlistfile)
            cmdlistE = eval("tstlstmod."+options.cmdlistname)
        except Exception as ee :
            eprint("ERROR: tstlistmod creation did not succeed on cmdlist '{}' and filename '{}' -- {}".format(options.cmdlistname, options.cmdlistfile, repr(ee)))
        else :
            logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
            clth = ClthTest(options)
            clth.dutFunction = dutfunction
            eprint("INFO: Executing main\n")
            clth.main(cmdlistE, options, logger)

    hrs, mins, secs, Duration = TSUtil.DurationCalc(START_TIME)

    fd = None
    try :
        fd = open(logger.filename, 'a')
    except IOError as io :
        eprint("ERROR: Could not open file {} -- {}".format(logger.filename, io.message))
    except Exception as ee :
        eprint("INFO: Logger not available -- {}".format(repr(ee)))
    else :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()
        if options.logtodb :
            try :
                logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                eprint("ERROR: Logging the Duration  failed {}\n".format(repr(e)))

    eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    if options.emailme is not None :
        emailer.setToAddress(options.emailme)
    tm = printTime("INFO: Ended Program Execution at :", noprnt=True)
    eprint(tm)
    emailer.emailResults()
