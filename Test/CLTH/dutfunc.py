""" dutfunc.py contains functions that are used to control devices that
    may change from test to test. These functions can do whatever they want
    The function can call any number of sub functions in any module it wants to 
    import or create. The sky's the limit!!

    Version : 0.0.0
    Date : Sep 24 2019
    Copyright Ossia Inc. 2019

"""
from __future__ import absolute_import, division, print_function, unicode_literals

def dutTest(cmdlist, rw) :
    if cmdlist is not None :
        if cmdlist[0] == "test1" :
            print("{}".format(cmdlist[1]))
    return "Just a Test of a DUT function"

