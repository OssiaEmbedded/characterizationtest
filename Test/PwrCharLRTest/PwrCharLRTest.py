''' PwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback, inspect
if "/home/ossiadev/pylib" not in sys.path  :
    sys.path.insert(0, "/home/ossiadev/pylib")

if "/home/ursus/pylib" not in sys.path :
    sys.path.insert(0, "/home/ursus/pylib")

import json
from time import sleep, time, localtime
from optparse import OptionParser

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm, gPWR_LEVEL_MAP 
from LIB.logResults import LogResults
from LIB.actuator import Actuator
from LIB.resultsEmail import EmailResults
from LIB.timestamp import DurationCalc

ROTARYON     = 'ossiabbb2'
TESTDBNAME   = 'IntegLabOne'
COUPLERLOSS  = 19.8
FINDHOLDTIME = 2.0

TIME2CAL      = 4
CHARGERNEWCAL = False
CHARGERNEWSTATUS = False

TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME     = "SaTable"
TIMESTAMPCOLNAME      = "DataTimeStamp"
DEFAULTDBSERVER       = 'ossia-build'
TXTBLNAME             = "ChargerConfig"

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(40)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["PmusEnabled","%s", 'VARCHAR(50)'],
         ["RecsEnabled","%s", 'VARCHAR(50)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'],
         ["calPhase", "%s", 'VARCHAR(4096)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["clientStatusDisc", "%s", 'VARCHAR(220)'],
             ["netCurrent", "%1.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%1.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%1.0f", 'VARCHAR(10)'],
             ["peakPower", "%1.0f", 'VARCHAR(10)'],
             ["mWPower", "%1.0f", 'VARCHAR(20)'],
             ["TxAngle", "%1.1f",'VARCHAR(7)'],
             ['DistanceZ1', "%d", 'VARCHAR(10)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["QueryTime", "%s", 'VARCHAR(20)'],
             ["MaxCalcPwr_1", "%1.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_2", "%1.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_3", "%1.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_4", "%1.2f",   'VARCHAR(10)'],
             ["MaxZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["PowerLevel", "%d", 'VARCHAR(5)'],
             ["WarmupTime", "%d",'VARCHAR(7)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["clientComRSSI", "%d", 'VARCHAR(6)'],
             ["proxyComRSSI", "%d", 'VARCHAR(6)'],
             ["AvgCalcPwr_1", "%1.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_2", "%1.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_3", "%1.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_4", "%1.2f",   'VARCHAR(10)'],
             ["AvgZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["Model", "%d",        'VARCHAR(10)'],
             ["version", "%d", 'VARCHAR(20)'],
             ["TPSMissed", "%d",    'VARCHAR(20)']]

sTXIP='SV2Tile_IntegLab'
sTXPORT='50000'
sSAIP='A-N9020A-11404.ossiainc.local'
sSAPORT='5023'
srvPORT='50081'

# object to Email Results to a defined list 
emailer = EmailResults()

def eprint(msg, ln=False) :
    linemsg =  msg
    if ln :
        linemsg = "{} @@{}".format(msg, inspect.currentframe().f_back.f_lineno)
    print("E {}".format(linemsg))
    emailer.setResults(linemsg)

def getCotaConfigValues(option) :
    from LIB.sendTestServer import SendTestServer

    sts=SendTestServer(option.txipaddr, int(srvPORT), option.debug)
    jVal = sts.sendCommand(['getConfigFile'])
    rtnstr, plst, flst = sts.parseCotaConfigValues(jVal)

    return rtnstr, plst, flst

def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            eprint("INFO: Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            eprint("INFO: {}".format(msg))
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getVersions(tx) :
    argv = ["versions"]
    v = tx.sendGetTransmitter(argv)
    vers = v['Result']
    swRel = int(vers["Release Version"])
    versionstr = "SWRel:{:X}.{:X}.{:X}\n".format(((swRel>>16) & 0xF), ((swRel >> 8) & 0xF), (swRel & 0xF))

    blddate = int(vers['Daemon Build Info']['Date'])
    bldnum  = int(vers['Daemon Build Info']['Number'])
    versionstr += "Daemon:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Driver Lib Build Info"]["Date"])
    bldnum  = int(vers["Driver Lib Build Info"]["Number"])
    versionstr += "DrvLib:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Message Manager Build Info"]["Date"])
    bldnum  = int(vers["Message Manager Build Info"]["Number"])
    versionstr += "MesMan:{}:{}\n".format(blddate, bldnum)

    bldnum = int(vers["FPGA Revision"])
    versionstr += "FPGA:{:X}.{:X}\n".format((bldnum >> 8) & 0xf,bldnum & 0xF)

    bldnum = int(vers["Proxy FW Revision"])
    versionstr += "PROXY:0.{:d}\n".format(bldnum)

    versionstr += "OS:{}".format(str(vers["OS Version"]))
        
    return versionstr, swRel

def getOnChannels(tx) :
    #
    ch_on = 0
    argv = ["info"]
    v = tx.sendGetTransmitter(argv)
    chList = v['Result']['AMBS']
    rtnstr = ""
    chRevs = list()
    for chan in chList :
        outStr = ""
        try :
            chOnStr = str(chan['Status'])
            chOn = 1 if chOnStr == 'ON' else 0
            chNum = int(chan['Channel Number'])
            rev = int(chan['AMB Revision'])
            if chOn == 1 :
                chRevs.append([chNum, "{}.{}".format((rev>>8)&0xF, rev&0xF)])
            # make the def smaller, take out all the extra unneeded characters
            outStr = "{}:{:02d}:{:04X}\n".format(chOnStr, chNum, rev)
            # chOn will be either 1 or 0, shifting a zero is benign.
            ch_on |= (chOn << chNum)
        except ValueError as ve :
            eprint("ERROR: ValueError Chan Info {} -- {}".format(v['Result']['Status'], ve.message), True)
            outStr = "{}:{}:{}\n".format("OFF", "ZZ", "XXXX")
        rtnstr += outStr

    chStr = "0x{:04X}".format(ch_on)
    return chStr, rtnstr, chRevs

def stopStartChargingList(tx, ss, clientList, verbose=False) :
    """ returns True if the requested function failed
                false if it did not fail
    """
    # ss must be either 'stop' or 'start' for this to work
    rtnval = False
    cmd = "{}_charging".format(ss)

    for client in clientList :
        sClientId = str(client)
        if verbose :
            eprint("INFO: {} charging client {}".format(ss.capitalize(), sClientId))
        argv = [cmd, sClientId]
        try :
            v = tx.sendGetTransmitter(argv)
            rtnck, stat, disc = jcs.checkStatusReturn(v)
            rtnval = not rtnck
            if not rtnval :
                if ss == 'start' :
                    rtnval |= not (stat == 5 or stat == 11 or stat == 6 or stat == 12)
                elif ss == 'stop' :
                    rtnval |= not (stat == 4 or stat == 10)
                else :
                    rtnval = True
            if  rtnval :
                eprint("ALERT: Charging did not '{}': {}: {}".format(ss, stat, disc))
            sleep(1)
        except Exception :
            rtnval = True
    sleep(0.2)
    return rtnval

def getSAPower(sa, saimage, option, cChanFreq, cChanNum) :
    """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                      sapower is a float
                      safreq is a float
    """
    sapower = -60.0
    safreq = -2.0
    if sa.usesa :
        if option.average :
            sa.avgHold = ""
        else :
            sa.maxHold = ""
        # wait for data to get stable
        sleep(option.sadwelltime)
        # find the freq of the peak
        sa.peakSearch = ""
        # get the SA data
        sa.triggerHold = ''
        SApower, SAfreq = sa.peakSearch
        if saimage > 0 :
            try :
                tstEval = "%6.4e != %6.4e" % (SAfreq, float(cChanFreq[str(cChanNum)]))
                if saimage == 2 or eval(tstEval) :
                    filename = "{}/saImage_{}.png".format(option.directory, int(time()))
                    saI = sacom.SACom(addr=option.saipaddr, port=80, debug=option.debug)
                    saI.getSaveImage(filename)
                    logger.logAddValue("ImageSA", filename)
            except KeyError as ke :
                eprint("ERROR: KeyError in comChanNum -- {}\n".format(ke.message), True)
            except ValueError as ve :
                eprint("ERROR: ValueError in comChanNum -- {}\n".format(ve.message), True)
            except TypeError as te :
                eprint("ERROR: TypeError in comChanNum -- {}\n".format(te.message), True)

        sa.triggerClear = ''
        sa.clearHold = ''
        try :
            sapower = float(SApower)
        except ValueError as ve :
            eprint("ERROR: ValueError on SApower '{}' -- {}".format(SApower, ve.message), True)
            sapower = -1.0
        try :
            safreq = float(SAfreq)
        except ValueError as ve :
            eprint("ERROR: ValueError on SAfreq '{}' -- {}".format(SAfreq, ve.message), True)
            safreq = -1.0
        sa.saClose(True)
    return sapower, safreq

def calibrateAlgorithm(tx, sa, option) :
    """ calibrateAlgorithm -- first resets the tile and then performs calibrate after that.
                              calibrate 5 times getting SA PeakPower each time. The maximum of that
                              sequence will then be used as a goal for up  to 10 more calibrates.
                              When one of them is equal to (+- .5) or greater than the max the
                              algorithm finishes without calibrating again.
    """
    saMax    = -100.0
    saPwr, _ = getSAPower(sa, 0, option, None, None)
    saMax    = max(saMax, saPwr)
    rtnval   = True
    rtnlst   = list(saPwr)

    if not CHARGERNEWCAL :
        argv = ['reset']
        v = tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ALERT: calibrateAlgorithm1 - Transmitter did not Calibrate")
            rtnval = False
        sleep(TIME2CAL)

    argv = ['calibrate']
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        eprint("ALERT: calibrateAlgorithm2 - Transmitter did not Calibrate")
        rtnval = False
    sleep(TIME2CAL)

    if rtnval :
        for cal in range(5) :
            saPwr, _ = getSAPower(sa, 0, option, None, None)
            saMax = max(saMax, saPwr)
            rtnlst.append(saPwr)
            eprint("INFO: Finding {} saMax={} saPwr={}".format(cal, saMax, saPwr))

            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("INFO: calibrateAlgorithm3 - Transmitter did not Calibrate")
                rtnval = False
                break
            sleep(TIME2CAL)
    
            if not CHARGERNEWCAL :
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("INFO: calibrateAlgorithm4 - Transmitter did not Calibrate")
                    rtnval = False
                    break
                sleep(TIME2CAL)

        saPwr, _ = getSAPower(sa, 0, option, None, None)
        saMax = max(saMax, saPwr)
        rtnlst.append(saPwr)

        if saMax > saPwr :
            for cal in range(10) :
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ERROR: calibrateAlgorithm{} - Transmitter did not Calibrate".format(cal + 5))
                    rtnval = False
                sleep(TIME2CAL)
                saPwr, _ = getSAPower(sa, 0, option, None, None)
                rtnlst.append(saPwr)
                eprint("INFO: Seeking {} saMax - 0.3={} saPwr={}".format(cal, (saMax - 0.3), saPwr))
                if (saMax - 0.3) < saPwr :
                    break

    eprint("INFO: Final saMax={} saPwr={}\n".format(saMax, saPwr))
    return rtnval, rtnlst

def transmitterCalibrate(tx, clientList, msg='None', prnt=True) :
    rtnval = False #stopStartChargingList(tx, 'stop', clientList)

    if not rtnval :
        argv = ['calibrate']
        v = tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ALERT: transmitterCalibrate - Transmitter did not Calibrate: {}".format(msg))
            rtnval = True

    sleep(TIME2CAL)
    if not rtnval :
        rtnval = False #stopStartChargingList(tx, 'start', clientList)

    if prnt :
        eprint("INFO: Calibration {} CL={}".format("Failed" if rtnval else "Done", repr(clientList)))

    return rtnval

def getClientCommandData(tx, sClientId, num, delayTime=2.5) :
    argv = ['client_command', sClientId, num]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        eprint("ALERT: getClientCommandData - Client Command {} request Failed : {}".format(num, repr(v)))
    sleep(delayTime)

    argv = ['client_command_data', sClientId]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        eprint("ALERT: getClientCommandData - Client Command Data request Failed ({}): {}".format(num, repr(v)))
    return v

def makeMaxPwrList(clientList) :
    maxPwrList = dict()
    for c in clientList :
        maxPwrList.update({c : {"maxP" : -99.9, "maxSA" : -99.9}})

    return maxPwrList

def printTime(msg1, warmupin=None, noprnt=False) :
    tm_obj = localtime(time())
    if warmupin is not None :
        tf_obj = localtime(time()+warmupin)
        fmt = msg1 + " Start at -- {:02d}:{:02d}:{:02d} finish at {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(warmupin, tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec,
                                      tf_obj.tm_hour, tf_obj.tm_min, tf_obj.tm_sec)
    else :
        fmt = msg1 + " -- {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
    if not noprnt :
        print(strval)
    return strval

def getSystemTemperature(tx) :
    # get system temperature datum
    argv = ['get_system_temp']
    v = tx.sendGetTransmitter(argv)
    systemTemp = 0
    if jcs.checkForSuccess(v) :
        eprint("ALERT: System Temperature not valid", True)
    else :
        systemTemp = jcs.getSystemTemp(v)
    return systemTemp

maxSystemTemp = 0.0
def runthecharacter(sa, tx, clientList, testlist, logger, options, comChanNum) :

    global maxSystemTemp
    finished = False
    iterateTime = time()
    iterateCnt = -1
    iterateCntUp = 1
    iterateSave = -1 
    maxPeakPower = 0.0
    maxBatteryLevel = 0.0
    comChanFreq = { "24" : 2.4601E9, "25" : 2.4501E9, "26" : 2.4401E9}
    try :
        iterateCnt = int(testlist.get('logcount', 10))
        iterateSave = iterateCnt
    except ValueError as ve :
        eprint("ERROR: logcount input error -- {}".format(ve.message), True)
        return True, maxPeakPower, maxBatteryLevel

    try :
        zDistance   = int(testlist.get('zdistance', 0))
        txAngle     = float(testlist.get('txangle', 0.0))
        passfailmin = testlist.get('passfailmin',-100.0)
        passfailmax = testlist.get('passfailmax', 100.0)
    except ValueError as ve :
        eprint("ERROR: ValueError on testlist Dist/angle access -- {}\n".format(ve.message), True)
        return True, maxPeakPower, maxBatteryLevel

    realZDistance = zDistance
    realTxAngle = txAngle
    if options.actuator is not None :
        act2 = None
        actuatorAry = options.actuator.split(",")
        act1 = Actuator(actuatorAry[0], None, debug=options.debug)
        act1.sendCommandString(arg0='setPosition', arg1=str(zDistance), arg2='l')
        rtnlst = act1.sendCommandString(arg0='getPosition', arg1='l').split()
        for val in rtnlst :
            try :
                realZDistance = int(val)
                print("        Z real vs. set: abs({} - {}) = {}\n".format(realZDistance, zDistance, abs(realZDistance-zDistance)))
                break
            except ValueError :
                pass
        
        rotact = act2
        if actuatorAry[0] == ROTARYON :
            rotact = act1

        if rotact :
            rotact.sendCommandString(arg0='setPosition', arg1=str(txAngle), arg2='r')
            rtnlst = rotact.sendCommandString(arg0='getPosition', arg1='r').split()
            for val in rtnlst :
                try :
                    realTxAngle = float(val)
                    print("        Rot real vs. set: abs({} - {}) = {}\n".format(realTxAngle, txAngle, abs(realTxAngle-txAngle)))
                    break
                except ValueError :
                    pass

    if stopStartChargingList(tx, 'start', clientList, True) :
        eprint("ERROR: stopStartChargingList error", True)
        return True, maxPeakPower, maxBatteryLevel

    # wait for the warmup period
    warmup = int(testlist.get('warmup', 30))
    eprint(printTime("INFO: Warmup time {} :", warmupin=warmup, noprnt=True))

    reportWait(warmup, prnt=options.console)

    if options.poscalibrate or testlist['caljustonce'] :
        if options.calibratealg and not options.poscalibrate :
            _, calData = calibrateAlgorithm(tx, sa, options)
            calDataJson = json.dumps(calData)
            print("calDataJson = '{}'\n".format(repr(calDataJson)))
            if options.logtodb :
                try :
                    logger.logUpdateTableColumn(options.dbname, options.tblname, "calPhase", calDataJson)
                    print("logUpdateTableColumn DONE")
                except Exception as e : 
                    eprint("ALERT: Logging the calPhase failed {}\n".format(repr(e)), True)

        else :
            transmitterCalibrate(tx, clientList, msg="After warmup", prnt=options.console)

    calibrationInterval = int(options.calibrate)

    maxPowerList = makeMaxPwrList(clientList)
    systemTemp = getSystemTemperature(tx)
    maxSystemTemp = max(maxSystemTemp, systemTemp)

    while (iterateCnt > 0  or (iterateTime - time()) > 0.0) and not finished :

        #printTime("iteration started at :")
        timetowait = float(testlist.get('waitfordata', 5)) - options.sadwelltime 
        timetowait = max(timetowait, 0.0)

        if timetowait > 0.0 :
            sleep(timetowait)
        #printTime("time to wait finished at :")

        if calibrationInterval > 0 and (iterateCntUp % calibrationInterval) == 0 :
            transmitterCalibrate(tx, clientList, msg="After interval time", prnt=options.console)
            logger.logAddValue('calibrate', "1")
            sleep(timetowait)
        elif calibrationInterval > 0 :
            logger.logAddValue('calibrate', "0")
        iterateCntUp += 1

        iterateCnt -= 1
        if options.console and iterateCnt >= 0 :
            print("{} ".format(iterateCnt), end='')

        # set SA to obtain peak or average value

        #printTime("Get SA Power started at :")
        SAPower, SAFreq = getSAPower(sa, options.saimage, options, comChanFreq, comChanNum)

        #printTime("Get SA Power finished at :")
        systemTemp = getSystemTemperature(tx)
        maxSystemTemp = max(maxSystemTemp, systemTemp)

        # get charging client details
        #printTime("Stating client data collection :")
        for client in clientList :
            # add SA values to the log list
            tm_obj = localtime(time())
            datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
            logger.logAddValue('timetaken', datatime)

            if options.average :
                logger.logAddValue('SAPowerAvg', SAPower)
                logger.logAddValue('SAFreqAvg', SAFreq)
            else :
                logger.logAddValue('SAPowerPeak', SAPower)
                logger.logAddValue('SAFreqPeak', SAFreq)

            logger.logAddValue('DistanceZ1', int(zDistance))
            logger.logAddValue("PowerLevel", testlist['powerlevel'])
            logger.logAddValue("WarmupTime", testlist['warmup'])
            logger.logAddValue("TxAngle", txAngle)
            logger.logAddValue('systemTemp', systemTemp)

            sClientId = str(client)
            logger.logAddValue('clientId', sClientId)

            argv = ['client_detail', sClientId]
            v = tx.sendGetTransmitter(argv)
            status, statval, statdisc = jcs.checkStatusReturn(v)
            logger.logAddValue('clientStatusDisc', statdisc)

            if not status :
                eprint("ALERT: Client Detail request shows client {} returned status {} trying again -- {}".format(sClientId, statval, statdisc), True)
                status, statval, statdisc = jcs.checkStatusReturn(v)
                if not status :
                    eprint("ALERT: Client Detail request shows client {} returned status {} movining on -- {}".format(sClientId, statval, statdisc), True)
                    rtnval = logger.logResults()
                    if rtnval :
                        eprint("ERROR: Couldn't log values for Client {} -- {}.".format(sClientId, rtnval), True)
                        finished = True
                        continue

            version          = jcs.getClientVersion(v)
            logger.logAddValue('version', version)

            netCurrent       = jcs.getClientNetCurrent(v)
            logger.logAddValue('netCurrent', netCurrent)

            queryFailedCount = jcs.getClientQueryFailed(v)
            logger.logAddValue('queryFailedCount', queryFailedCount)

            linkQuality      = jcs.getClientLinkQuality(v)
            logger.logAddValue('linkQuality', linkQuality)

            batteryLevel     = jcs.getBatteryLevel(v)
            maxBatteryLevel  = max(maxBatteryLevel, batteryLevel)
            logger.logAddValue('batteryLevel', batteryLevel)

            Model            = jcs.getDeviceModel(v)
            logger.logAddValue('Model', Model)

            DeviceStatus     = jcs.getDeviceStatus(v)
            logger.logAddValue('DeviceStatus', DeviceStatus)

            TPSMissed        = jcs.getTPSMissed(v)
            logger.logAddValue('TPSMissed', TPSMissed)

            averagePower     = jcs.getAveragePower(v)
            logger.logAddValue('averagePower', averagePower)

            peakPower        = jcs.getPeakPower(v)
            logger.logAddValue('peakPower', peakPower)

            clientComRSSIVal = jcs.getClientComRSSI(v)
            logger.logAddValue('clientComRSSI', clientComRSSIVal)

            proxyComRSSIVal  = jcs.getProxyComRSSI(v)
            logger.logAddValue('proxyComRSSI', proxyComRSSIVal)

            queryTime        = jcs.getClientQueryTime(v)
            logger.logAddValue('QueryTime', queryTime)

            # changing to averagePower from peakPower per Jorge's request.
            maxPeakPower     = max(maxPeakPower, averagePower)
            # There is only one SA attache to one client
            maxPowerList[sClientId]['maxSA'] = max(maxPowerList[sClientId]['maxSA'], SAPower)
            maxPowerList[sClientId]['maxP']  = max(maxPowerList[sClientId]['maxP'], peakPower)


            # get charging client data
            argv = ['client_data', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ALERT: Client Data request Failed: {} -- trying again".format(repr(v['Result']['Status'])), True)
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ALERT: Client Data request Failed again: {} -- moving on".format(repr(v['Result']['Status'])), True)
                    rtnval = logger.logResults()
                    if rtnval :
                        eprint("ERROR: Couldn't log values for Client {} -- {}.".format(sClientId, rtnval), True)
                        finished = True
                        continue
            batteryVoltage = jcs.getClientBatteryVoltage(v)
            data = jcs.processClientData(v)
            MaxZCSCNT_1, MaxCalcPwr_1, AvgZCSCNT_1, AvgCalcPwr_1 =  jcs.getClientRFPower(data, 1)
            MaxZCSCNT_2, MaxCalcPwr_2, AvgZCSCNT_2, AvgCalcPwr_2 =  jcs.getClientRFPower(data, 2)
            MaxZCSCNT_3, MaxCalcPwr_3, AvgZCSCNT_3, AvgCalcPwr_3 =  jcs.getClientRFPower(data, 3)
            MaxZCSCNT_4, MaxCalcPwr_4, AvgZCSCNT_4, AvgCalcPwr_4 =  jcs.getClientRFPower(data, 4)

            logger.logAddValue('batteryVoltage', batteryVoltage)
            mWPower = batteryVoltage * netCurrent
            logger.logAddValue("mWPower", mWPower)

            logger.logAddValue('MaxZCSCNT_1', MaxZCSCNT_1)
            logger.logAddValue('MaxCalcPwr_1', MaxCalcPwr_1)
            logger.logAddValue('MaxZCSCNT_2', MaxZCSCNT_2)
            logger.logAddValue('MaxCalcPwr_2', MaxCalcPwr_2)
            logger.logAddValue('MaxZCSCNT_3', MaxZCSCNT_3)
            logger.logAddValue('MaxCalcPwr_3', MaxCalcPwr_3)
            logger.logAddValue('MaxZCSCNT_4', MaxZCSCNT_4)
            logger.logAddValue('MaxCalcPwr_4', MaxCalcPwr_4)
            logger.logAddValue('AvgZCSCNT_1', AvgZCSCNT_1)
            logger.logAddValue('AvgCalcPwr_1', AvgCalcPwr_1)
            logger.logAddValue('AvgZCSCNT_2', AvgZCSCNT_2)
            logger.logAddValue('AvgCalcPwr_2', AvgCalcPwr_2)
            logger.logAddValue('AvgZCSCNT_3', AvgZCSCNT_3)
            logger.logAddValue('AvgCalcPwr_3', AvgCalcPwr_3)
            logger.logAddValue('AvgZCSCNT_4', AvgZCSCNT_4)
            logger.logAddValue('AvgCalcPwr_4', AvgCalcPwr_4)
            # add the time entry
            if options.loglongbcnt :
                v = getClientCommandData(tx, sClientId, '42')
                longBeaconCnt = jcs.getCommandData(v,42, 5, 4) # offset 5, num of bytes 4
                numBeaconCnt = jcs.getCommandData(v,42, 1, 4) # offset 1, num of bytes 4
                logger.logAddValue("LongBeaconCnt", longBeaconCnt)
                logger.logAddValue("NumBeaconCnt", numBeaconCnt)

                # get client command data 20
                v = getClientCommandData(tx, sClientId, '20')
                watchDogRstCnt = jcs.getCommandData(v, 20, 7, 2) # offset 7, num of bytes 2
                logger.logAddValue("WatchDogRstCnt", watchDogRstCnt)

            rtnval = logger.logResults()
            if rtnval :
                eprint("ERROR: Couldn't log values for Client {} -- {}.".format(sClientId, rtnval), True)
                finished = True
                break

            if peakPower >= options.maxrssi :
                eprint("INFO: peakPower value exceeded peakPower limit: {} < {} cnt {}\n".format(options.maxrssi, peakPower, iterateCntUp-1), True)
                if (iterateCntUp-1) >= 3 :
                    finished = True
        #printTime("Finished client data collection :")

    #printTime("Finished Position at :")
    for client, data in maxPowerList.iteritems() :

        try :
            if type(passfailmin) is dict :
                minlim = float(passfailmin.get(client,-100.0))
            else :
                minlim = float(passfailmin)
        except ValueError as ve :
            eprint("ALERT: ValueError on passfail min limit -- {}".format(ve.message), True)
            minlim = -100.0

        try :
            if type(passfailmax) is dict :
                maxlim = float(passfailmax.get(client, 100.0))
            else :
                maxlim = float(passfailmax)
        except ValueError as ve :
            eprint("ALERT: ValueError on passfail max limit -- {}".format(ve.message), True)
            maxlim = 100.0

            # print and email some intermediate results
        eprint("INFO: Client {} at Z {:4d} Angle {:1.1f}".format(client, int(zDistance), float(txAngle)))
        if iterateSave == 0 : # don't print stats if no data taken
            break
        eprint("INFO: <b>Passfail criteria are Min {} Max {}</b>".format(minlim, maxlim))        
        eprint("INFO: <b>Max SA PeakPower : {:1.2f} dBm, Max Client PeakPower : {:1.2f} dBm</b>".format(data['maxSA'], data['maxP']))
        eprint("INFO: System Temperature {} / Max {}".format(systemTemp, maxSystemTemp))

        thePname = ""
        if sa.usesa :
            pwrval = data['maxSA']
            thePname = "SA"
        else :
            pwrval = data['maxP']
            thePname = "CR"

        if pwrval > maxlim :
            eprint("ERROR: Pass/Fail Maximum limit exceeded {} Power: {} > {}".format(thePname, pwrval, maxlim), True)
        elif pwrval < minlim :
            eprint("ERROR: Pass/Fail Minimum limit exceeded {} Power: {} < {}".format(thePname, pwrval, minlim), True)

    return finished, maxPeakPower, maxBatteryLevel

def main(tstList, passfailE, options, logger) :

    MYSTART_TIME = time()
    # init the TX communication object
    tx=TXComm(options.txipaddr, int(options.txport), options.debug)
    verstr = ""
    swRel = 0
    try :
        verstr, swRel = getVersions(tx)
        swRel = int(swRel)
    except ValueError as ve :
        eprint("ERROR: Release Version definition invalid {} -- {}".format(swRel, ve.message))
    except Exception as ee :
        eprint("ERROR: Release Version Unknown Exception -- {}".format(repr(ee)))
    else :
        global CHARGERNEWCAL
        global TIME2CAL
        global CHARGERNEWSTATUS
        if swRel >= 66825 : # 1.5.9
            CHARGERNEWCAL = True
            TIME2CAL = 1
        if swRel >= 67072 : # 1.6.0
            CHARGERNEWSTATUS = True

    # process client id CSL
    clientList = list()
    cl = options.clientid.split(',')
    for c in cl :
        clientList.append(c.strip())

    errorCondition = False
    # none specified so find one
    argv = ["client_list"]
    v = tx.sendGetTransmitter(argv)

    rtnval, rtndict = jcs.processClientList(v, clientList, CHARGERNEWSTATUS)
    if not rtnval :
        errorCondition = True
        for cid, stat in rtndict.iteritems() :
            eprint("ERROR: Client id {} unavailable with linkQuality {} and status {} --- {}".format(cid, stat[1], stat[0],
                                                                                              jcs.client_stat_dict.get(stat[0], stat[0])))
    else :
        clientList = list()
        for cid, stat in rtndict.iteritems() :
            clientList.append(cid)
            eprint("INFO: Continuing with Client ID {} status {} {}".format(cid, stat[0],
                                                                      jcs.client_stat_dict.get(stat[0], stat[0])))
    if not errorCondition :
        # Get some Charger data
        comChanNum = '25'
        cota_config = "ND"
        try :
            cota_config, pdict, fullCfgDict = getCotaConfigValues(options)
            comChanNum = pdict["Client COM Channel"]
        except Exception as ex :
            eprint("ALERT: getCotaConfigValues exception -- {}".format(repr(ex)), True)

        argv = ["get_charger_id"]
        v = tx.sendGetTransmitter(argv)
        txId = jcs.getTxId(v)

        pmusEnabled = ""
        recsEnabled = ""
        clientFW = ""
        for client in clientList :
            try :
                sClientId = str(client)
                argv = ['register_client', sClientId]
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ERROR: Register client {} failed".format(sClientId))
                    return
                # configure the client
                argv = ['client_config', sClientId, '0x6']
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    eprint("ERROR: Client config failed")
                    return
                sleep(2)
                # check for a non zero Short ID
                argv = ['client_detail', sClientId]
                v = tx.sendGetTransmitter(argv)
                shortID = jcs.getClientShortID(v)
                if shortID == "0x0000" :
                    eprint("ERROR: Short ID shows device is not registered: " + shortID)
                    return
                # get the PMUs and RECs that are enabled
                if len(pmusEnabled) > 0 :
                    pmusEnabled += " "
                    recsEnabled += " "
                v = getClientCommandData(tx, sClientId, '17')

                tmp = jcs.getCommandData(v, 17, 1, 4, False)
                pmusEnabled += "{}_{}".format(sClientId, tmp)

                v = getClientCommandData(tx, sClientId, '33')

                tmp = jcs.getCommandData(v, 33, 1, 4, False)
                recsEnabled += "{}_{}".format(sClientId, tmp)

                v = getClientCommandData(tx, sClientId, '61')

                tmpfw = jcs.getCommandData(v, 61, 1, 4)
                clientFW += "{}_{:08X} ".format(sClientId, tmpfw)

                #
            except ValueError as ve :
                eprint("ERROR: ClientId ValueError {} -- {}".format(client, ve.message))
                return

        ## get the Transmiter data values for logging.

        # first get the Transmitter data that we want to log
        #    This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp

        logger.initFile(TXCMS)

        if options.logtodb :
            logger.initDB(options.dbname, hostnm=options.dbservername)
            logger.initDBTable(options.tblname)
        # add options Dict to log list
        val =  repr(options.__dict__).replace("'","").replace('"',"").replace(","," ").replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
        logger.logAddValue("options", val)

        # add txId to log list
        logger.logAddValue("txId", txId)
        logger.logAddValue("AntennaType",options.antennatype)
        logger.logAddValue("CfgComment",options.logcomments)
        logger.logAddValue("CotaConf",cota_config)
        # add OnChannels to log list
        logger.logAddValue("PmusEnabled", pmusEnabled)
        logger.logAddValue("RecsEnabled", recsEnabled)
        OnChannels, ChanList, chRevs = getOnChannels(tx)
        logger.logAddValue("OnChannels", OnChannels)
        v = tx.sendGetTransmitter(['get_good_channels'])
        logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
        verstrPlusClient =  str(verstr)+"\n"+clientFW
        logger.logAddValue("Versions", verstrPlusClient)
        logger.logAddValue("AmbRev", str(ChanList))
        logger.logAddValue("LogFileName", logger.filename)
        tableTS = str(int(logger.timeStampNum))
        CLIENTTABLENAME = "client_{}".format(tableTS)
        logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
        SATABLENAME = "sa_{}".format(tableTS)
        logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
        logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)

        eprint("INFO: DataBase: {}, Tablename: {}, Test Timestamp: {}".format(options.dbname,
                                                                        options.tblname,
                                                                        logger.timeStamp))
        eprint("INFO: Charger name: {}, Antenna Type: {}, Comments: '{}'".format(options.txipaddr,
                                                                           options.antennatype,
                                                                           options.logcomments))
        eprint("INFO: System versions\n{}".format(verstrPlusClient))

        eprint("INFO: AMB QSR Version:")
        for ch, ver in chRevs :
            eprint("INFO: Channel: {}, Version: {}".format(ch, ver))

        eprint("INFO: Cota Config File:");

        boldlist = [ "System Type", "Client COM Channel", "Client Query Period", "Reference ACL"]
        for n in boldlist :
            eprint("INFO: {:30} == {}".format(n, fullCfgDict.get(n, n)))            

        for n, v in fullCfgDict.iteritems() :
            if n not in boldlist :
                eprint("INFO: {:30} == {}".format(n, v))            

        rtnval = logger.logResults()
        if rtnval :
            eprint("ERROR: Couldn't log values for Transmitter -- {} -- Exiting.".format(rtnval), True)
            return

        # make the logging list
        # setup the SA

        sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
        if sa.errorstr :
            eprint("ALERT: SA not connected with error : {}".format(sa.errorstr))
        if sa.usesa :
            eprint("INFO: Using SA {} port {}".format(options.saipaddr, options.saport))
            sacms = list()
            for cmd_data in SA_SETUP_LIST :
                if  cmd_data[2] is not None :
                    sacms.append([cmd_data[0], "%s", cmd_data[2]])
                # the the SA
                sa.setSAValue(cmd_data[0], cmd_data[1])
            sa.referenceLevelOffset = COUPLERLOSS + options.cableloss
            sacms.append(["referenceLevelOffset", "%s", 'VARCHAR(35)'])
            sa.saClose(True)

            # init the logging list
            logger.initFile(sacms,how='a')
            if options.logtodb :
                logger.initDBTable(SATABLENAME)
            # log the SA setup data
            for cmd_data in SA_SETUP_LIST :
                if  cmd_data[2] is not None :
                    logger.logAddValue(cmd_data[0], cmd_data[1])
            logger.logAddValue("referenceLevelOffset", str(COUPLERLOSS + options.cableloss))

            rtnval = logger.logResults()
            if rtnval :
                eprint("ERROR: Couldn't log values for SA. -- {}".format(rtnval), True)
                return
            passfailA = passfailE.get('SA', dict())
        else :
            passfailA = passfailE.get('Client', dict())
            eprint("ALERT: SA is not available")

        clientcms = CLIENTCMS
        clientcms.insert(5,["ImageSA", "%s", 'VARCHAR(100)'])
        if options.average :
            clientcms.insert(5,["SAPowerAvg", "%1.2f", 'VARCHAR(15)'])
            clientcms.insert(5,["SAFreqAvg", "%1.4e", 'VARCHAR(15)'])
        else :
            clientcms.insert(5,["SAPowerPeak", "%1.2f", 'VARCHAR(15)'])
            clientcms.insert(5,["SAFreqPeak", "%1.4e", 'VARCHAR(15)'])

        if options.loglongbcnt :
            clientcms.append(["NumBeaconCnt", "%d", 'VARCHAR(20)'])
            clientcms.append(["LongBeaconCnt", "%d", 'VARCHAR(20)'])
            clientcms.append(["WatchDogRstCnt", "%d", 'VARCHAR(20)'])

        # setup the logging file
        if int(options.calibrate) > 0 :
            clientcms.append(["calibrate", "%s", 'VARCHAR(10)'])
        if options.debug :
            print("FNAME={}".format(logger.filename))
            print("COLFMT={}".format(logger.colformat))

        logger.initFile(clientcms,how='a')
        if options.logtodb :
            logger.initDBTable(CLIENTTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        lstDistance = 0
        finished = False
        lstAngle = 0.0

        calJustOnce = options.caljustonce
        for itTstDict  in tstList :

            try :
                itTstDict['warmup']      = int(itTstDict.get('warmup', 30))
                itTstDict['txangle']     = float(itTstDict.get('txangle', 0.0))
                itTstDict['zdistance']   = int(itTstDict.get('zdistance', 1000))
            except ValueError as ve :
                eprint("ERROR: ValueError converting warmup and txangle {}".format(ve.message), True)
                break

        # put the pass fail criteria in the test list for convenience
            z = itTstDict.get('zdistance', -1)
            a = itTstDict.get('txangle', 180)
            angle = passfailA.get(z, None)
            if angle is not None :
                pf = angle.get(a, None)
                if pf is not None :
                    itTstDict['passfailmin'] = pf[0]
                    itTstDict['passfailmax'] = pf[1]

            _, _, _, Duration = DurationCalc(MYSTART_TIME)

            print("{} Distance {} > {} finished {}\n".format(Duration, lstDistance, itTstDict['zdistance'], repr(finished)))
            # if we finished (maxpwr exceeded) and the next distance is closer the skip it
            if finished and lstDistance > itTstDict['zdistance'] :
                print("        Distance {} > {} finished {}\n".format(lstDistance, itTstDict['zdistance'], repr(finished)))
                continue
            # if the next angle is smaller (more direct) and the next disance is closer or the same then skip
            print("        abs(txAngle) {} > {} Distance {} > {} finished {}\n".format(abs(lstAngle), abs(itTstDict['txangle']), 
                        lstDistance, itTstDict['zdistance'], repr(finished)))
            if finished and abs(lstAngle) > abs(itTstDict['txangle']) and lstDistance > itTstDict['zdistance'] :
                print("        abs(txAngle) {} > {} Distance {} > {} finished {}\n".format(abs(lstAngle), abs(itTstDict['txangle']), 
                        lstDistance, itTstDict['zdistance'], repr(finished)))
                continue
            lstDistance = itTstDict['zdistance']
            lstAngle = itTstDict['txangle']
            powerLevel = 13
            try :
                powerLevel = int(itTstDict.get('powerlevel', 13))
            except ValueError as ve :
                eprint("ERROR: ValueError converting powerlevel {}".format(ve.message), True)
                powerLevel = 13
            powerLevel = max(powerLevel, 13)
            powerLevel = min(powerLevel, 21)
            powerLevel = gPWR_LEVEL_MAP.get(powerLevel, 13)
            argv = ['set_power_level', str(powerLevel)]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ERROR: Set Power Level failed")
                return
            argv = ['get_power_level']
            v = tx.sendGetTransmitter(argv)
            pl =  jcs.getTransmitterPowerLevel(v)
            if pl != powerLevel :
                eprint("ALERT: Power level incorrect get={} vs set={}".format(pl, powerLevel))
                powerLevel = pl
            itTstDict['powerlevel'] = powerLevel

            # this function also logs all the client type data to the file and database.
            itTstDict.update({'caljustonce': calJustOnce})
            try :
                finished, peakPower, batteryLevel = runthecharacter(sa, tx, clientList, itTstDict, logger, options, comChanNum)
                if options.holdatpeak > 0.0 and peakPower >= options.holdatpeak :
                    for _ in range( options.holdcount) :
                        finished, peakPower, batteryLevel = runthecharacter(sa, tx, clientList, itTstDict, logger, options, comChanNum)
                        if batteryLevel > options.batterylevel :
                            break
                    finished = True
            except KeyboardInterrupt as ki :
                eprint("INFO: Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(ki.message), True)
                finished = True
                break
            except Exception as ex :
                eprint("ERROR: Unexpected Exception exiting --- {} -- {}\n".format(repr(ex), repr(itTstDict)), True)
                eprint("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
                eprint("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
                finished = True
                break
            calJustOnce = False

        eprint("INFO: End of test. Stop charging client list")
        if stopStartChargingList(tx, 'stop', clientList, True) :
            eprint("ERROR: stopStartChargingList error", True)


def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict


if __name__ == '__main__' :

    START_TIME = time()
    tm = printTime("INFO: Starting Program Execution at :", noprnt=True)
    eprint(tm)
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None, help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg", help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlistLin", help="Use the named list from the file (.py).")
    parser.add_option("","--passfailfile", dest='passfailfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--passfailname", dest='passfailname', type=str,  action='store',default="passfailLin", help="Use the named list from the file (.py).")
    parser.add_option("-p","--saport",     dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    parser.add_option("-i","--saip",       dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")

    parser.add_option("","--sadwelltime",  dest='sadwelltime',  type=float,action='store',default=1.5,help="Set the SA Peak/avg search dwell time")
    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=sTXIP,help="Transmitter network address.")

    parser.add_option("","--actip",        dest='actuator',     type=str,  action='store',default=None,help="Linear Actuator network address(es) (comma sep).")

    parser.add_option("","--holdatpeak",   dest='holdatpeak',   type=float,action='store',default=-1.0,help="Hold at the requested peak value and capture data.")
    parser.add_option("","--holdcount",    dest='holdcount',    type=int,  action='store',default=1,help="The number of data captures at the requested peak value.")
    parser.add_option("","--batterylevel", dest='batterylevel', type=int,  action='store',default=98,help="The battery level to quit test at.")

    parser.add_option("-c","--clientid",   dest='clientid',     type=str,  action='store',default=None,help="Device/client ID to use for characterization.")
    parser.add_option("","--minlinkq",     dest='minlinkq',     type=int,  action='store',default='85',help="Minimum link quality to validate a client.")
    parser.add_option("","--maxpwrlevel",  dest='maxrssi',      type=float,action='store',default='28.0',help="Maximum power level to quit test: a client protection.")

    parser.add_option("","--cableloss",    dest='cableloss',    type=float,action='store',default=0.0,help='Cable loss positive dB. Added to the 20 dB of Connector loss')
    parser.add_option("","--calibrate",    dest='calibrate',    type=int,  action='store',default="0",help="Number data points between calibrations.")
    parser.add_option("","--poscalibrate", dest='poscalibrate',            action='store_false',default=True,help="Calibrate at each position change.")
    parser.add_option("","--calibratealg", dest='calibratealg',            action='store_true',default=False,help="Use an algorithm to obtain a relatively good calibration.")
    parser.add_option("","--caljustonce",  dest='caljustonce' ,            action='store_false',default=True,help="Use an algorithm to obtain a relatively good calibration.")

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="IntegLab1LogFile{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--noconsole",    dest='console',                 action='store_true',default=False,help="Turn off printing to console")
    parser.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    parser.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--loglongbcnt",  dest='loglongbcnt',             action='store_true',default=False,help="Log the long beacon count info")
    parser.add_option("","--saimage",      dest='saimage',      type=int,  action='store',default=0,help="Capture an SA Image on every data point.")
    parser.add_option("","--emailme",      dest='emailme',      type=str,  action='store',default=None,help="Email me the test summary.")

    parser.add_option("","--dbservername", dest='dbservername', type=str,  action='store',default=DEFAULTDBSERVER,help="Default DBServer name.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    import imp
    if len(args) > 0 :
        eprint("ERROR: Extra command line args, exiting")
        eprint("ERROR: {}".format(repr(args)))
    elif options.cfgfile is not None :
        TTYPE = 0
        TDEF = 1
        fname = options.cfgfile
        if fname == '' :
            fname = sys.argv[0].replace('.', 'Conf.')
        cfgmod = imp.load_source(options.cfgname, fname)
        cfg = eval("cfgmod."+options.cfgname)
        optType = makeOptionTypeDict(parser)
        for k in cfg.keys() :
            try :
                if optType[k][TTYPE]  == 'int' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = int(str(cfg[k]))
                elif optType[k][TTYPE]  == 'float' :
                    if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = float(str(cfg[k]))
                elif optType[k][TTYPE]  is None :
                    options.__dict__[k] = optType[k][TDEF]  # set the default value
                    try :
                        options.__dict__[k] = eval(cfg[k])
                    except NameError :
                        options.__dict__[k] = False
                    except TypeError :
                        options.__dict__[k] = cfg[k]

                elif optType[k][TTYPE]  == 'string' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    elif str(cfg[k]) == 'None' :
                        options.__dict__[k] = None
                    else :
                        options.__dict__[k] = str(cfg[k])
                else :
                    options.__dict__[k] = str(cfg[k])
                #print("name={} def={} type={} in={} cur={}\n".format(k, optType[k][TDEF], optType[k][TTYPE], cfg[k], options.__dict__[k]))
            except ValueError as ve :
                eprint("ERROR: VE {} -- Incorrect option value {} -- {}\n".format(k, cfg[k], ve.message))
                exit(2)
            except IndexError as ie :
                eprint("ERROR: IE {} -- No such option -- {}\n".format(k, ie.message))
                exit(3)
            except Exception as e :
                eprint("ERROR: EX {} -- Unknown exception -- {}\n".format(k, repr(e)))
                exit(4)

        eprint("INFO: Characterization test of host '{}' using Receiver(s) '{}' and Actuator '{}'".format(options.txipaddr, options.clientid, options.actuator))

        try :
            tstlstmod = imp.load_source(options.testlistname, options.testlistfile)
            testlistE = eval("tstlstmod."+options.testlistname)
        except Exception as ee :
            eprint("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, fname, repr(ee)))
        else :
            if options.debug :
                print("options = {}".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
                print("testlist = {}".format(repr(testlistE).replace('},','},\n')))

            eprint("INFO: Executing main\n")

            passfailE = dict()
            if options.passfailname is not None and options.passfailfile is not None :
                try:
                    passfailmod = imp.load_source(options.passfailname, options.passfailfile)
                    passfailE = eval("passfailmod."+options.passfailname)
                except Exception  as ee :
                    eprint("ALERT: passfailmod creation did not succeed on passfail '{}' and filename '{}' -- {}".format(options.passfailname, options.passfailfile, repr(ee)))
                    passfailE = dict()
    
            try :
                cIdstr = str(options.clientid)
            except ValueError as ve :
                eprint("ERROR: Couldn't make clientid a string == '{}'".format(repr(ve)))
                eprint("ERROR: Invalid Client ID option. A clientid CSL string must be supplied -- '{}'".format(repr(ve)))
            else :
                options.clientid = cIdstr
                if options.clientid != "" :
                    logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
                    main(testlistE, passfailE, options, logger)
                else :
                    eprint("ERROR: Invalid Client ID option. A clientid CSL string must be supplied -- '{}'".format(repr(ve)))

    hrs, mins, secs, Duration = DurationCalc(START_TIME)
    fd = None
    try :
        fd = open(logger.filename, 'a')
    except IOError as io :
        eprint("ERROR: Could not open file {} -- {}".format(logger.filename, io.message))
    except Exception as ee :
        eprint("ERROR: Logger not available -- {}".format(repr(ee)))
    else :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()
        if options.logtodb :
            try :
                logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                eprint("ERROR: Logging the Duration failed {}\n".format(repr(e)))
    eprint("")
    eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    if options.emailme is not None :
        emailer.setToAddress(options.emailme)
    tm = printTime("INFO: Ended Program Execution at :", noprnt=True)
    eprint(tm)
    emailer.emailResults()
