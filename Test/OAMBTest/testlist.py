"""
   Here is the set of tests that can be run. The empty dictionary for each test
   can be used to set parameters for the test e.g. rwStressTEST could have the numbers
   of loops set to something other than the default. i.e. {'loops' : 100} 

   THE ORDER OF THIS FILE IS IMPORTANT.
   If multiple test lists are selected they will be executed in the order defined
   in this file.
"""
runalltests=[
        {'broadCastTEST'  : {'uvplist' : '17'}},
        {'rwStressTEST'   : {'loops' : 50, 'report' : 'failonly'}},
        {'lockDetectTEST' : {'uvp' : 17, 'cfs_start' : 25, 'cfs_end' : 48, 'del_val' : 0.001, 'nwb_sweep' : "b", 'report' : 'failonly'}}, # 17 does all 1-16 individual
        {'spiDisableTEST' : {'report' : 'all'}},
        {'calibrateTEST'  : {'setcalparams' : {'calPwrLevel' : '20', 'calA_uvp' : '1', 'calB_uvp' : '13', 'ref_uvp' : None, 'uvpA_list' : None, 'uvpB_list' : None},
                             'uvp'          : 17,
                             'report'       : "failonly"}}
]

runalltestsdef=[ # use defaults
        {'broadCastTEST'  : {}},
        {'rwStressTEST'   : {}},
        {'lockDetectTEST' : {}},
        {'spiDisableTEST' : {}},
        {'calibrateTEST'  : {}},
]

broadcast=[
        {'broadCastTEST'  : {}}, # use defaults
]

broadcastverbose=[
        {'broadCastTEST'  : {'report' : 'all'}},
]

stresstest=[
    {'rwStressTEST'   : {'loops' : 50}}, # use defaults
]

lockdetect=[
        {'lockDetectTEST' : {}}, # use defaults
]

lockdetectverbose=[
        {'lockDetectTEST' : {'uvp' : 17, 'cfs_start' : 25, 'cfs_end' : 48, 'del_val' : 0.001, 'nwb_sweep' : "b", 'report' : 'all'}}, # 17 does all 1-16 individual
]

spidisable=[
        {'spiDisableTEST' : {}}, # use defaults
]

spidisableverbose=[
        {'spiDisableTEST' : {'report' : 'all', 'uvplist' : '1:6,8:16'}},
]

caltest=[
        {'calibrateTEST'  : {}}, # use defaults
]

caltestverbose=[
        {'calibrateTEST'  : {'setcalparams' : {'calPwrLevel' : '20', 'calA_uvp' : '1', 'calB_uvp' : '13', 'ref_uvp' : None, 'uvpA_list' : None, 'uvpB_list' : None},
                             'uvp'          : 17,
                             'report'       : "all"}}
]
