''' OAMBRunAllTest.py -- Setup OAMB to execute all the self tests.
Copyright Ossia Inc. 2020
'''

from __future__ import absolute_import, division, print_function, unicode_literals

import socket, sys
from time import sleep, time, localtime
from optparse import OptionParser, Option, OptionValueError

if "/home/ossiadev/pylib" not in sys.path  :
    sys.path.insert(0, "/home/ossiadev/pylib")

from LIB.parseCommaColon import parseCommaColon

from LIB.timestamp import DurationCalc
from LIB.logResults import LogResults
from LIB.utilmod import *
import json


MAXTESTTIME = 1900 # seconds 30 mins per test

TESTDBNAME='OAMBTest'

TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'

VERSION="1.0.1"
VERDATE="1.13.20"

# data base definition constants
OAMBCMS = [[TIMESTAMPCOLNAME, "%s", 'VARCHAR(35)'],
           ["oambsn",         "%s", 'VARCHAR(35)'],
           ["CfgComment",     "%s", 'VARCHAR(200)'],
           ["PostTestNotes",  "%s", 'VARCHAR(512)'],
           ["broadCastTable", "%s", 'VARCHAR(40)'],
           ["spiDisableTable", "%s", 'VARCHAR(40)'],
           ["rwStressTable",    "%s", 'VARCHAR(40)'],
           ["lockDetectTable",  "%s", 'VARCHAR(40)'],
           ["calibrateTable",   "%s", 'VARCHAR(40)'],
           ["LogFileName",    "%s", 'VARCHAR(100)'],
           ["Duration",       "%s", 'VARCHAR(20)'],
           ["Versions",       "%s", 'VARCHAR(512)'],
           ["options",        "%s", 'VARCHAR(1024)'],
           ]

MEMTESTCMS = [["PassFail",  "%s", 'VARCHAR(10)'],
              ["UVP",       "%s", 'VARCHAR(9)'],
              ["Address",   "%s", 'VARCHAR(35)'],
              ["Page",      "%s", 'VARCHAR(10)'],
              ["Expected",  "%s", 'VARCHAR(15)'],
              ["Data",      "%s", 'VARCHAR(15)'],
              ["timetaken", "%s", 'VARCHAR(30)'],
              ]

LOCKTESTCMS = [
               ["PassFail",  '%s', 'VARCHAR(10)'],
               ["UVP",       '%s', 'VARCHAR(9)'],
               ["CFSValue",  '%s', 'VARCHAR(15)'],
               ['timetaken', '%s', 'VARCHAR(30)'],
               ]

CALTESTCMS = [
              ["PassFail",    '%s',    'VARCHAR(10)'],
              ["calAMU",      "%d",    'VARCHAR(9)'],
              ["autAMU",      "%d",    'VARCHAR(9)'],
              ["UVP",         "%d",    'VARCHAR(9)'],
              ["beaconRssi",  "%d",    'VARCHAR(10)'],
              ["receiveRssi", "%d",    'VARCHAR(10)'],
              ["reciprocity", "%5.3f", 'VARCHAR(10)'],
              ["calAUVP",     "%d",    'VARCHAR(9)'],
              ["calBUVP",     "%d",    'VARCHAR(9)'],
              ["refUVP",      "%d",    'VARCHAR(9)'],
              ["uvpAList",    "%s",    'VARCHAR(39)'],
              ["uvpBList",    "%s",    'VARCHAR(39)'],
              ['timetaken',   "%s",    'VARCHAR(30)'],
              ]


class spiServer(object) :

    def __init__(self, ipaddr, portnumber, debug=False, delimVal='/') :
        self.ipaddress = socket.gethostbyname(ipaddr)
        self.portnum = 7000 if portnumber is None else portnumber
        try :
            self.portnum = int(self.portnum)
        except ValueError as ve :
            eprint("ValueError on portnumber '{}' -- {}\n".format(self.portnum, repr(ve)))
            self.portnum = 7000
        self.debug = debug
        self.mySocket = None
        self.delimitValue = delimVal 
        self.COMMANDVAL = "COMMANDVAL"+self.delimitValue

    def sendData(self, sendthis) :
        """ sendData -- Send the input string to the ipaddress and port
                        of the spiServer.
        """
        retval = True
        if self.mySocket is None :
            self.mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                self.mySocket.connect((self.ipaddress, self.portnum))
                self.mySocket.settimeout(1.0)
            except :
                if self.debug :
                    print("There was a connect exception ")
                retval = False
                self.mySocket = None

        if self.mySocket is not None :
            self.mySocket.send( sendthis + "\r\n" )
        else :
            print("mySocket is None\n")

        return retval

    def getData(self, count=60, nonBlock=False, donotWait=False) :
        data = ""
        d = bytearray()
        cnt = count

        if self.debug :
            print("Socket Timeout = {}\n".format(self.mySocket.gettimeout()))
        while cnt > 0 :
            try :
                if self.debug :
                    print("1 cnt={} data={}".format(cnt, data[-15:]))
                d = bytearray(self.mySocket.recv(66000))
                data += d.decode("utf-8")
                if self.debug :
                    print("A cnt={} data={}".format(cnt, d[-15:]))
            except socket.timeout :
                if self.debug :
                    print("2 cnt={} data={}".format(cnt, data[-15:]))
                cnt -= 1
            except socket.error as se :
                data = "Error -- {}".format(se[1])
                if self.debug :
                    print("SocketError -- error No. {} -- {}\n".format(se[0], data))
                sys.stdout.flush()
                cnt = 0
                break
            except Exception as ee :
                print("SocketError UNKNOWN -- {}".format(repr(ee)))
                cnt = 0
                break
            if self.debug :
                print("DATA={}\n".format(data))
            if 'SUCCESS' in data :
                cnt = 0
                break
            if nonBlock or donotWait :
                break

        data = data.replace("SUCCESS", "").replace("Finished", "").strip()
        if not nonBlock or cnt <= 0 or donotWait :
            self.mySocket.close()
            self.mySocket = None

        return data

    def sendGetData(self, sendList, countin=60, donotwait=False) :
        """ sendGetData -- Send the input string to the ipaddress and port
                           of the spiServer.
        """
        sendstr = self.COMMANDVAL + self.delimitValue.join(sendList)
        self.sendData(sendstr)
        data = self.getData(count=countin, donotWait=donotwait)
        if self.debug :
            print(data)

        return data

    def makeCommandStringFromList(self, strlist) :
        """ makeCommandStringFromList -- Create a command string from a list of strings and return it.
                                 input arg is a list containing strings.
        """
        return self.COMMANDVAL + self.delimitValue.join(strlist)



def logPass(logger, uvp, timetaken, msg) :
    eprint("INFO: PASS {} UVP {}".format(msg, uvp))
    logger.logAddValue('PassFail',  "PASS")
    logger.logAddValue('timetaken', timetaken)
    logger.logAddValue('UVP',       str(uvp))
    logger.logAddValue('Address',   "NA")
    logger.logAddValue('Page',      "NA")
    logger.logAddValue('Expected',  "NA")
    logger.logAddValue('Data',      "NA")
    if logger.logResults() :
        eprint("ERROR: Couldn't log values for uvp {} {}.".format(uvp, msg))

def logFail(logger, timetaken, line, loglist, msg) :
    eprint("ERROR: FAIL {}".format(line))
    logger.logAddValue('PassFail',  "FAIL")
    logger.logAddValue('timetaken', timetaken)
    logger.logAddValue('UVP',       loglist[1])
    logger.logAddValue('Address',   loglist[3])
    logger.logAddValue('Page',      loglist[9])
    logger.logAddValue('Expected',  loglist[5])
    logger.logAddValue('Data',      loglist[7])
    if logger.logResults() :
        eprint("ERROR: Couldn't log values for uvp {} {}.".format(loglist[1], msg))


def listprint(mainmsgstr, mesg=None, mesgtype="INFO: ", delim="\n") :
    """ listprint -- takes a string and splits it using delim or
                     if mainmsgstr is a list then it eprints each
                     entry in the list.
    """
    if mesg is not None :
        eprint("{} {}".format(mesgtype, mesg))

    if type(mainmsgstr) is list :
        linelist = mainmsgstr
    else :
        linelist = mainmsgstr.strip().split(delim)

    if len(linelist) > 0 :
        for line in linelist :
            eprint("{} {}".format(mesgtype, line))
    else :
        eprint("{} None.".format(mesgtype))


class OAMBTest(object) :

    def __init__(self, options) :

        self.options  = options

        self.oambtf = spiServer(options.oambtfip, 7001, delimVal="&", debug=options.debug)
        self.logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)

        self.logtodb = self.options.logtodb

        self.OverAllPass    = True
        self.CMSINDEX       = 0
        self.FUNCINDEX      = 1
        self.TABLENAMEINDEX = 2


        self.testCmsDict = { #                   CMSINDEX      FUNCINDEX       TABLENAMEINDEX
                            "broadCastTEST"  : [MEMTESTCMS,  self.broadCastTest,   None],
                            "spiDisableTEST" : [MEMTESTCMS,  self.spiDisableTest,  None],
                            "rwStressTEST"   : [MEMTESTCMS,  self.stressTest,      None],
                            "lockDetectTEST" : [LOCKTESTCMS, self.lockDetectTest,  None],
                            "calibrateTEST"  : [CALTESTCMS,  self.calTest,         None],
                            }
        if self.logtodb :
            if self.options.dbname is None or self.options.tblname is None :
                self.logtodb = False

    def getVersions(self) :

        argv = ["version"]
        tfver = self.oambtf.sendGetData(argv)
        versionstr = "Test Fixture Version {} :: Script Version {}-{}".format(tfver, VERSION, VERDATE)
        return versionstr

    def broadCastTest(self, **kwargs) :
        """ broadcast test will send command to the spiServer to broadcast the init values to all 16 uvps
            then read back each one while checking.
            This is a type of Memory test and is only a data recorder. The test is done on the test fixture.
        """
        # init the board
        eprint("INFO: Broadcast init readback test")
        args = ['inituvp', '17']
        eprint("INFO: Initialize all UVPs -- {}".format(args))
        resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 
        listprint(resultstr, mesg="Initialization results")

        uvpliststr = kwargs.get('uvplist', '1:16')
        report     = kwargs.get('report', 'failonly')

        uvplist    = parseCommaColon(uvpliststr, sortlist=True, floatint=False) # returns list of ints
        if uvplist[0] == 17 :
            uvplist = [ i for i in range(1, 17, 1) ]

        for uvp in uvplist :
            tm_obj    = localtime(time())
            timetaken = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)

            args = ['broadcastck', "uvp", str(uvp), 'report', str(report)]
            resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 

            resultlist = resultstr.strip().split("\n")
            if resultlist[0].strip().startswith("PASS") :
                eprint("INFO: {} UVP {} -- {}".format(resultlist[0], uvp, args))
            else :
                eprint("ERROR: {} UVP {} -- {}".format(resultlist[0], uvp, args))
                self.OverAllPass = False

            for line in resultlist[1:] :
                loglist = line.split()
                eptype = "INFO:"
                if loglist[0].strip() == "FAIL" :
                    eptype = "ERROR:"
                    self.OverAllPass = False
                eprint("{} {}".format(eptype, line))
                self.logger.logAddValue('PassFail',  loglist[0])
                self.logger.logAddValue('timetaken', timetaken)
                self.logger.logAddValue('UVP',       loglist[2])
                self.logger.logAddValue('Address',   loglist[4])
                self.logger.logAddValue('Page',      loglist[6])
                self.logger.logAddValue('Expected',  loglist[10])
                self.logger.logAddValue('Data',      loglist[8])
                if self.logger.logResults() :
                    eprint("ERROR: Couldn't log values for uvp {} boadcast test.".format(loglist[2]))


    def spiDisableTest(self, **kwargs) :

        msg = "SPI Disable control test"
        eprint("INFO: {}".format(msg))

        uvpliststr = kwargs.get('uvplist', '1:16')
        uvpliststr = makeListToString(parseCommaColon(uvpliststr), delimit=',')

        outType    = kwargs.get('report', 'failonly')
        args = ['spidisabletest', 'report', str(outType), 'uvp', uvpliststr]

        tm_obj    = localtime(time())
        timetaken = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
        resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 

        resultslist = resultstr.split("\n")
        for line in resultslist :
            if not line.startswith("PASS") and not line.startswith("FAIL") :
                continue
            loglist = line.split("--")[1].split()
            if line.startswith("PASS") :
                logPass(self.logger, loglist[1], timetaken, line)
            else :
                logFail(self.logger, timetaken, line, loglist, line)
                self.OverAllPass = False
                 
    def stressTest(self, **kwargs) :
        msg = "UVP Register Write/Read Stress Test"
        eprint("INFO: {}".format(msg))

        loops  = kwargs.get('loops', 150)
        report = kwargs.get('report', "failonly")
        args   = ['stresstest', 'loops', str(loops), 'report', str(report)]

        tm_obj    = localtime(time())
        timetaken = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)

        resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 
        resultlist = resultstr.split("\n")

        uvp = 1
        loguvplst = 1

        for line in resultlist :

            if not line.startswith("PASS") and not line.startswith("FAIL") :
                continue
            datalist = line.split("--")
            loglist = datalist[1].strip().split()
            loguvp = int(loglist[1])

            if (loguvplst + 1) < loguvp :
                while uvp < loguvp :
                    logPass(self.logger, uvp, timetaken, msg) # msg comes from start of test
                    uvp += 1

            if loguvp <= 16 :
                logFail(self.logger, timetaken, line, loglist, msg)
                loguvplst = loguvp
                uvp = loguvp + 1
                self.OverAllPass = False

        while uvp < 17 :
            logPass(self.logger, uvp, timetaken, msg)
            uvp += 1

    def lockDetectTest(self, **kwargs) :
        eprint("INFO: UVP PLL Lock Detect Test")
        uvp = kwargs.get('uvp', 17)
        args = ['inituvp', str(uvp)]
        eprint("INFO: Initialize all UVPs -- {}".format(args))
        resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 
        listprint(resultstr, mesg="Initialization results")

        args = ['lockdetect', 'uvp', str(uvp)]
        for name, val in kwargs.iteritems() :
            if name == 'uvp' :
                continue
            args.append(name)
            args.append(str(val))
        tm_obj    = localtime(time())
        timetaken = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
        resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 

        if "ERROR" in resultstr :
            eprint("ERROR: {}".format(resultstr))
        else :
            resultlist = resultstr.strip().split("\n")
            for line in resultlist :
                loglist = line.strip().split()
                eptype = "INFO:"
                if loglist[0].strip() == "FAIL" :
                    eptype = "ERROR:"
                    self.OverAllPass = False
                eprint("{} {}".format(eptype, line))
                self.logger.logAddValue('PassFail',  loglist[0])
                self.logger.logAddValue('timetaken', timetaken)
                self.logger.logAddValue('UVP',       loglist[4])
                self.logger.logAddValue('CFSValue',  loglist[-1])
                if self.logger.logResults() :
                    eprint("ERROR: Couldn't log values for uvp {} lock detect.".format(loglist[4]))

    def calTest(self, **kwargs) :
        eprint("INFO: Calibration RSSI Validation Test")
        kwarg = kwargs.get('setcalparams', {})

        calPwrLevel = str(kwarg.get('calPwrLevel', '20'))
        calA_uvp    = str(kwarg.get('calA_uvp', '1'))
        calB_uvp    = str(kwarg.get('calB_uvp', '13'))
        ref_uvp     = kwarg.get('ref_uvp', None)
        uvpA_list   = kwarg.get('uvpA_list', None)
        uvpB_list   = kwarg.get('uvpB_list', None)
        args = ['setcalparams', 'calA_uvp', calA_uvp, 'calB_uvp', calB_uvp, 'calPwrLevel', calPwrLevel]
        if ref_uvp is not None :
            args += ['ref_uvp', str(ref_uvp)]
        if uvpA_list is not None :
            args += ['uvpA_list', str(uvpA_list)]
        if uvpB_list is not None :
            args += ['uvpB_list', str(uvpB_list)]
        resultstr = self.oambtf.sendGetData(args) 
        if "ERROR" in resultstr :
            eprint("ERROR: Process Error aborting test")
            listprint(resultstr, mesg="Set Cal Params results", mesgtype="ERROR:")
        else :
            listprint(resultstr, mesg="Set Cal Params results")
            uvp = kwargs.get('uvp', 17)
            args = ['inituvp', str(uvp)]

            eprint("INFO: Initialize all UVPs -- {}".format(args))
            resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 
            listprint(resultstr, mesg="Initialization results")

            args = ['lockdetect', 'uvp', str(uvp)]
            eprint("INFO: Lock Detect all UVPs -- {}".format(args))
            resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 
            listprint(resultstr, mesg="Lock Detect results")

            report = kwargs.get('report', 'failonly') 
            args = ['caltest', 'report', report]

            tm_obj    = localtime(time())
            timetaken = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)

            # execute the calTest
            resultstr = self.oambtf.sendGetData(args, countin=MAXTESTTIME) 

            caldata = json.loads(self.oambtf.sendGetData(['getlastcaldata']))

            amu_list = caldata['AMULIST']
            cpf = caldata['CPF']
            uvpAstrlist = makeListToString(caldata['uvpA_list'])
            uvpBstrlist = makeListToString(caldata['uvpB_list'])
            eprint("INFO:  calAMU, autAMU, UVP, beaconRssi, receiveRssi, reciprocity")
            for amus in amu_list :
                amuname = '{}{}'.format(amus['cal'], amus['aut'])
                beaconrssi = caldata[amuname]['beacon']
                receiverssi = caldata[amuname]['receive']
                for uvp in range(1, 17) :
                    b = beaconrssi[uvp]
                    r = receiverssi[uvp] 
                    pf = b < cpf['BeaconBaseVal'] or r < cpf['OffsetBaseVal']
                    self.OverAllPass &= not pf
                    recp = 0.0 
                    if max(b,r) > 0 :
                        recp = float(min(b,r))/float(max(b,r))
                    self.logger.logAddValue("PassFail",    "FAIL" if pf else "PASS")
                    self.logger.logAddValue("calAMU",      amus['cal'])
                    self.logger.logAddValue("autAMU",      amus['aut'])
                    self.logger.logAddValue("UVP",         uvp)
                    self.logger.logAddValue("beaconRssi",  b)
                    self.logger.logAddValue("receiveRssi", r)
                    self.logger.logAddValue("reciprocity", recp)
                    self.logger.logAddValue("calAUVP",     caldata['calA_uvp'])
                    self.logger.logAddValue("calBUVP",     caldata['calB_uvp'])
                    self.logger.logAddValue("refUVP",      caldata['ref_uvp'])
                    self.logger.logAddValue("uvpAList",    uvpAstrlist)
                    self.logger.logAddValue("uvpBList",    uvpBstrlist)
                    self.logger.logAddValue("timetaken",   timetaken)
                    if self.logger.logResults() :
                        eprint("ERROR: Couldn't log values for calAMU {} autAMU {} uvp {} lock detect.".format(amus['cal'], amus['aut'], uvp))
                    datastr = "{:9d} {:7d} {:6d} {:7d} {:9d} {:12.3f}".format(amus['cal'], amus['aut'], uvp, b, r, recp)
                    if pf :
                        eprint("ERROR: {}".format(datastr))
                    else :
                        eprint("INFO:  {}".format(datastr))

            resultList = resultstr.split("\n")
            for line in resultList :
                if "FAIL" in line :
                    eprint("ERROR: {}".format(line))
                    self.OverAllPass = False
                else :
                    eprint("INFO:  {}".format(line))


    def main(self, testlist) :

        self.logger.initFile(OAMBCMS)
    
        if self.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)
        # add options Dict to log list
        tableTS       = int(self.logger.timeStampNum)
        verstr        = self.getVersions()
        eprint("INFO: Test Version statement -- {}".format(verstr))
        optionval     = makeDictToString(self.options.__dict__)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)

        self.logger.logAddValue("oambsn",self.options.oambsn)
        self.logger.logAddValue("CfgComment",self.options.logcomments)
        self.logger.logAddValue("LogFileName", self.logger.filename)
        self.logger.logAddValue("Versions", str(verstr))
        self.logger.logAddValue("options", optionval)

        for k in testlist :
            name = k.keys()[0]
            if "TEST" in name :
                tname = name.replace("TEST", "Table")
                dbtname = "{}_{}".format(tname, tableTS)
                self.testCmsDict[name][self.TABLENAMEINDEX] = dbtname
                self.logger.logAddValue(tname, dbtname)
                eprint("INFO: Test Name={} tname={} dbtname={}".format(name, tname, dbtname))
                
        if self.logger.logResults() :
            eprint("ERROR: Couldn't log values for Transmitter.")
            return
        eprint("INFO: Finished DB setup. Executing the Test list\n")
        # the main CSV file and DB entries are done now the various tests can be executed.

        # the test list has a list of callable tests with names that point into the testCmsDict 
        # from there we get the test method, the CMS list and the name of the DB table to create.
        # now the test needs to interate the testlist

        for test in testlist : # this is a dict with one entry at Test Name to index into the testCmsDict

            self.oambtf.sendGetData(["enoamb"])

            TestIndex = test.keys()[0] # the index
            kw      = dict(test[TestIndex])
            cms     = self.testCmsDict[TestIndex][self.CMSINDEX]
            tblName = self.testCmsDict[TestIndex][self.TABLENAMEINDEX]
            func    = self.testCmsDict[TestIndex][self.FUNCINDEX]
            eprint("INFO: TestIndex={} kw={} tblName={}".format(TestIndex, repr(kw), tblName))

            # the client table is the last one inited so it will be the one where data is logged
            # it's data names will be expected to be used.
            self.logger.initFile(cms, how='a')
            if self.logtodb :
                self.logger.initDBTable(tblName)
            try :
                func(**kw)
            except (NameError, ValueError, RuntimeError, IndexError) as ee :
                eprint("ERROR: Exception while running TestIndex '{}' -- {}".format(TestIndex, repr(ee)))
                self.OverAllPass = False
                break
            eprint("INFO: Finished {}\n".format(TestIndex))




if __name__ == '__main__' :

    START_TIME = time()
    tm = printTime("INFO: Started Program Execution at :", noprnt=True)
    eprint(tm)
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None,                                 help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default=None,                                 help="Use the named config dict for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None,                                 help="Use a testlist file (.py) for the list of tests to execute")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default=None,                                 help="Use the named testlist for this test")

    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default=None,                                 help="Define an arbitrary log value")
    parser.add_option("","--oambtfip",     dest='oambtfip',     type=str,  action='store',default=None,                                 help="OAMB Test Fixture controller address.")
    parser.add_option("","--oambsn",       dest='oambsn',       type=str,  action='store',default=None,                                 help="OAMB Serial Number.")

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="OAMBTest_{}.csv",                    help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/home/ossiadev/Test/OAMBTest/Logs/", help="Log file directory.")
    parser.add_option("","--nologtodb",    dest='logtodb',                 action='store_false',default=True,                           help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=None,                                 help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=None,                                 help="Set the DB Table name at ossia-build")
    parser.add_option("","--emaillist",    dest='emaillist',    type=str,  action='store',default='',                                   help="Add email addressees.")
    parser.add_option("","--console",      dest='console',                 action='store_true',default=False,                           help="Output log data to console.")
    parser.add_option("","--sendcmd",      dest='sendcmd',                 action='store_true',default=False,                           help="Send a command to the test fixture and nothing else. Args are on the cmdline.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,                           help="print debug info.")

    (opts, args) = parser.parse_args()

    import imp
    processError = False
    errorString = ""
    options = None
    testlist = None
    hrs = mins = secs = -1 
    if opts.sendcmd :
        try :
            oambtf = spiServer(opts.oambtfip, 7001, delimVal="&", debug=opts.debug)
        except :
            print("ERROR on sendcmd")
        else :
            oambtf.sendGetData(args)
    else :
        if len(args) > 0 :
            errorString = "ERROR: Extra command line args, exiting {}".format(repr(args))
            eprint(errorString)
            processError = True
        elif opts.cfgfile is not None :
            try :
                eprint("INFO: parsing cfgfile '{}'".format(opts.cfgfile))
                options = parseConfigFile(parser, opts, cmdLinePred=True)
            except RuntimeError as re :
                processError = True
                errorString = "ERROR: Runtime Error parsing Cfg File -- {}".format(repr(re))
                eprint(errorString)
                sys.exit(1)
            except Exception as e :
                errorString = "ERROR: Unknown exception -- {}\n".format(repr(e))
                processError = True
                eprint(errorString)
                sys.exit(2)
    
        else :
            options = opts

        if options.testlistfile is not None :

            try :
                theTestList = list()
                tstlistList = options.testlistname.split(',')
                for tstName in tstlistList :
                    #tstlstmod   = imp.load_source(options.testlistname, options.testlistfile)
                    #testlist    = eval("tstlstmod."+options.testlistname)
                    tstlstmod   = imp.load_source(tstName, options.testlistfile)
                    testlist    = eval("tstlstmod."+tstName)
                    theTestList.extend(testlist)
            except Exception as ee :
                eprint("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, options.testlistfile, repr(ee)))
                processError = True
        else :
            eprint("ERROR: testlist filename was not specified")
            processError = True
    
        OAMB = None
        if not processError :
            eprint("INFO: OAMB Test Testlist {} : Comments '{}' using the test fixture at {}".format(theTestList, options.logcomments, options.oambtfip))
            listprint(sys.argv,                           mesg="CmdLine args :")
            listprint(makeDictToString(options.__dict__), mesg="Options List :")
    
            try :
                OAMB = OAMBTest(options)
            except Exception as ee :
                eprint("ERROR: Creation of OAMB Test object failed -- {}".format(repr(ee)))
                OAMB = None
            else :
                eprint("INFO: Executing main\n")
                OAMB.main(theTestList)
                args = ['disablebrds']
                resultstr = OAMB.oambtf.sendGetData(args, countin=MAXTESTTIME) 
        else :
            listprint(sys.argv,                           mesg="CmdLine args :")
            listprint(makeDictToString(options.__dict__), mesg="Options List :")
    
        days, hrs, mins, secs, Duration = DurationCalc(START_TIME, True)
        eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    
        try :
            OAMB.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
        except Exception as e : 
            eprint("ERROR: Logging the Duration failed {}\n".format(repr(e)))
    
        if options is not None :
            emailer.setToAddress(options.emaillist)
        tm = printTime("INFO: Ended Program Execution at :", noprnt=True)
        eprint(tm)
        emailer.emailResults()
        if OAMB is not None :
            if OAMB.OverAllPass :
                sys.exit(0)
            else :
                sys.exit(11)
        else :
            sys.exit(31)

