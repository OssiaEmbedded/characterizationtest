''' PwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")
from time import sleep, time
from optparse import OptionParser, OptionGroup

import SA.SACommunicationBase as sacom
from LIB.logResults import LogResults
from LIB.actuator import Actuator

from inspect import currentframe, getframeinfo

PEAKSEARCHTIME = 1.02 # seconds

DBNAME='Mars_5_8'
DBTBLNAME='FEB'

CELLTABLENAME = "CellTable"
SATABLENAME = "SaTable"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'

# data base definition constants
CELLCMS = [[TIMESTAMPCOLNAME, "%s", 'VARCHAR(35)'],
           [CELLTABLENAME,    "%s", 'VARCHAR(40)'],
           [SATABLENAME,      "%s", 'VARCHAR(40)'],
           ["LogFileName",    "%s", 'VARCHAR(100)'],
           ["Duration",       "%s", 'VARCHAR(20)'],
           ["options",        "%s", 'VARCHAR(1024)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '5.845 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

DATACMS = [["CellNumber",   "%s", 'VARCHAR(25)'],
           ["RxSwitch", "%s", 'VARCHAR(25)']]


sSAIP='k-n9020a-00217'
sSAPORT='5023'


def getSetActuators(option) :
    """ getSetActuators -- Uses option.actuator to create an actuator object for each
                           actuator in the list (comma separated).
                           returns the act1 and act2 objects as well as
                           the act[12]Type.
    """
    acts = option.actuator
    acthostname = None
    act = None
    try :
        acthostname = acts.strip()
    except IndexError :
        pass
    else :
        act = Actuator(acthostname, option.actport, debug=option.debug)

    return act

def parseList(strval) :

    commalist = strval.split(",")

    rtnlist = list()
    for dashval in commalist :
        dl = dashval.split('-')
        if len(dl) == 2 :
            for dd in range(int(dl[0]), int(dl[1])+1) :
                rtnlist.append(dd)
        else :
            rtnlist.append(int(dashval))
        
    rtnlist.sort()
    return rtnlist


class MarsRxSwitch(object) :
    """
    Class to define the Power Characterization test
    """

    def __init__(self, option) :

        self.errorCondition = False
        self.cellList = [0]

        try :
            self.cellList = parseList(option.celllist)
        except Exception as ex :
            print("Exception in cellist creation -- '{}'".format(repr(ex)))
            self.errorCondition = True
        else :
            self.debug   = option.debug

            option.debug = False
            self.act = getSetActuators(option)
            # setup the SA
            self.sa      = sacom.SACom(addr=option.saipaddr, port=int(option.saport), debug=option.debug)
            self.logger  = LogResults(option.logfile, option.directory, console=option.console, debug=option.debug)
            self.average = option.average

    def getSAPower(self) :
        """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                          sapower is a float
                          safreq is a float
        """
        sapower = -60.0
        safreq = -2.0
        if self.sa.usesa :
            if self.average :
                self.sa.avgHold = ""
            else :
                self.sa.maxHold = ""
            # wait for data to get stable
            sleep(PEAKSEARCHTIME)
            # find the freq of the peak
            self.sa.peakSearch = ""
    
            # get the SA data
            self.sa.triggerHold = ''
            SApower, SAfreq = self.sa.peakSearch
            self.sa.triggerClear = ''
            self.sa.clearHold = ''
            try :
                sapower = float(SApower)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SApower ValueError on '{}' fline {}.{} -- {}".format(SApower, frmInfo.filename, frmInfo.lineno, ve))
                sapower = -1.0
            try :
                safreq = float(SAfreq)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SAfreq ValueError on '{}' fline {}.{} -- {}".format(SAfreq, frmInfo.filename, frmInfo.lineno, ve))
                safreq = -1.0
        return sapower, safreq
    
    def shiftAll(self) :
        argv = ["shiftall", "1000"]
        rtnstr = self.act.sendCommandList(argv)
        return rtnstr

    def main(self, option) :

        finished = False
        self.logger.initFile(CELLCMS)

        if option.logtodb :
            self.logger.initDB(option.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(option.tblname)
        # add option Dict to log list

        tableTS = str(int(self.logger.timeStampNum))
        CELLTABLENAME_S = "rx_{}".format(tableTS)
        SATABLENAME_S = "sa_{}".format(tableTS)
        options_val =  str(repr(option.__dict__).replace("'","").replace('"',"").replace(","," "))

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue(CELLTABLENAME, CELLTABLENAME_S)
        self.logger.logAddValue(SATABLENAME, SATABLENAME_S)
        self.logger.logAddValue("LogFileName", self.logger.filename)
        self.logger.logAddValue("options", options_val)

        if self.logger.logResults() :
            print("Couldn't log values for Transmitter.")
            return

            # make the logging list
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
        # the the SA
            self.sa.setSAValue(cmd_data[0], cmd_data[1])

        # init the logging list
        self.logger.initFile(sacms,how='a')
        if option.logtodb :
            self.logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                self.logger.logAddValue(cmd_data[0], cmd_data[1])
        if self.logger.logResults() :
            print("Couldn't log values for SA.")
            return

        datacms = DATACMS
        if self.average :
            datacms.append(["SAPowerAvg", "%3.2f", 'VARCHAR(10)'])
            datacms.append(["SAFreqAvg", "%6.4e", 'VARCHAR(10)'])
        else :
            datacms.append(["SAPowerPeak", "%3.2f", 'VARCHAR(10)'])
            datacms.append(["SAFreqPeak", "%6.4e", 'VARCHAR(10)'])

        if self.debug :
            print("FNAME={}".format(self.logger.filename))
            print("COLFMT={}".format(self.logger.colformat))

        self.logger.initFile(datacms,how='a')

        if options.logtodb :
            self.logger.initDBTable(CELLTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        # set verbose off
        if not self.debug :
            argv = ["verbose", "off"]
            rtnstr = self.act.sendCommandList(argv)
            
        # init the act gpios
        argv = ["initoutputs"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Init act gpios returned '{}'".format(rtnstr)) 

        # init the expander gpios
        argv = ["configure","1000"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Configure GPIOS returned '{}'".format(rtnstr)) 

        # reset the internal data list
        argv = ["resetsetdsl"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Clear DSL returned '{}'".format(rtnstr)) 
        
        # put all the RXs in bypass
        paen_val = option.paen if option.paen in ['paon', 'lnaon', 'stdby', 'bypass'] else 'bypass'
        for cell in range(1, 33) :
            argv = ['setcell', str(cell), "paen", str(paen_val)]
            rtnstr = self.act.sendCommandList(argv)
            if self.debug :
                print("Set cell bypass returned '{}'".format(rtnstr)) 
        #shift it out
        rtnstr = self.shiftAll()
        if self.debug :
            print("Clear chain returned '{}'".format(rtnstr)) 

        for cell in self.cellList :
            try :

                if cell is not 0 :
                    argv = ["setcell", str(cell), "paen", "lnaon"]
                    rtnstr = self.act.sendCommandList(argv)
                    self.shiftAll()

                maxValue = -1000.0
                maxRX = 0
                for rx in range(32) :

                    argv = ["setrx", str(rx)]
                    rtnstr = self.act.sendCommandList(argv)
                    # add the client ID to the log list
                    self.logger.logAddValue('RxSwitch', rx)
                    self.logger.logAddValue('CellNumber', cell)

                    # set SA to obtain peak or average value
                    sapower, safreq = self.getSAPower()
                    if maxValue <  sapower :
                        maxValue = sapower
                        maxRX = rx
                    if self.debug :
                        print("Set RX return string='{}'".format(rtnstr))

                    print("Set RX Cell={} RX={} SAPower={:5.1f}".format(cell, rx,  sapower))

                    if self.average :
                        self.logger.logAddValue('SAPowerAvg', sapower)
                        self.logger.logAddValue('SAFreqAvg', safreq)
                    else :
                        self.logger.logAddValue('SAPowerPeak', sapower)
                        self.logger.logAddValue('SAFreqPeak', safreq)
    
                    if self.logger.logResults() :
                        lr = "Couldn't log values for Cell {} RX {} and sapower {}.".format(cell, rx, sapower)
                        print(lr)
                        finished = True
                        raise Exception(lr)

                print("Cell={}, maxRX={}, MaxValue={:5.1f}".format(cell, maxRX, maxValue))

                if cell is not 0 :
                    argv = ["setcell", str(cell), "paen", paen_val]
                    rtnstr = self.act.sendCommandList(argv)
                    self.shiftAll()

            except KeyboardInterrupt as ki :
                print("Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                finished = True
            except Exception as ex :
                print("Unexpected Exception exiting --- {}".format(repr(ex)))
                finished = True

        argv = ["resetsetdsl"]
        rtnstr = self.act.sendCommandList(argv)
        argv = ["setrx", '0']
        rtnstr = self.act.sendCommandList(argv)
        self.shiftAll()

        if not self.debug :
            argv = ["verbose", "on"]
            rtnstr = self.act.sendCommandList(argv)

        return finished


if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    equipconns = OptionGroup(parser, "Equipment IPs and port numbers", "Used to connect to the required test equipment.")
    equipconns.add_option("","--saport",   dest='saport',    type=int,  action='store',default=sSAPORT,help="Spectrum Analyzer telnet port number. (default='%default')")
    equipconns.add_option("","--saip",     dest='saipaddr',  type=str,  action='store',default=sSAIP,help="Spectrum Analyzer network address (default='%default')")
    equipconns.add_option("","--actip",    dest='actuator',  type=str,  action='store',default="ossiabbb8",help="SPI Server network address. (default='%default')")
    equipconns.add_option("","--actport",  dest='actport',   type=int,  action='store',default=7001,help="SPI Server. (default='%default')")
    equipconns.add_option("","--celllist", dest='celllist',  type=str,  action='store',default="1-32",help="List of cell to iterate over. (default='%default')")
    equipconns.add_option("","--paen",     dest='paen',      type=str,  action='store',default="bypass",help="PAEN state [bypass, paon, lnaon, stdby]. (default='%default')")
    parser.add_option_group(equipconns)

    booleanfunc = OptionGroup(parser, "True False flags", "Set option to enable function.")
    booleanfunc.add_option("","--average", dest='average',              action='store_true',default=False,help="Turn on average hold SA data (default='%default')")
    booleanfunc.add_option("","--logtodb", dest='logtodb',              action='store_true',default=False,help="Log data to the DataBase at ossia-build (default='%default')")
    booleanfunc.add_option("","--console", dest='console',              action='store_true',default=False,help="Turn off printing to console (default='$default')")
    parser.add_option_group(booleanfunc)

    parser.add_option("","--logfile",      dest='logfile',   type=str,  action='store',default="MarsRxSwitch_{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added. (default='%default')")
    parser.add_option("","--directory",    dest='directory', type=str,  action='store',default="/home/ossiadev/Test/MarsCellTests/Logs/",help="Log file directory. (default='%default')")
    parser.add_option("","--dbname",       dest='dbname',    type=str,  action='store',default=DBNAME,help="Set the DataBase name at ossia-build (default='%default')")
    parser.add_option("","--tblname",      dest='tblname',   type=str,  action='store',default=DBTBLNAME,help="Set the Data Table Name in the DataBase (default='%default')")

    parser.add_option("-d","--debug",      dest='debug',                action="store_true",default=False,help="print debug info. (default='%default')")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        print("Extra command line args, exiting")
        print(repr(args))
        exit(1)

    if options.debug :
        print("<pre>options = {}</pre>".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))

    rxSetter = MarsRxSwitch(options)
    if not rxSetter.errorCondition :
        rxSetter.main(options)
    else :
        print("ErrorCondition set")

    duration = int(time() - START_TIME)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005
    
    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    fd = open(rxSetter.logger.filename, 'a')
    if fd :
        fd.write("\nDuration, {}\n".format(Duration))
        fd.close()
    if options.logtodb :
        try :
            rxSetter.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
        except Exception as e : 
            print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {}\n".format(Duration))
