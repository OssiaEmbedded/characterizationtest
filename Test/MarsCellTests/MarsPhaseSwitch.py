''' PwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")
from time import sleep, time
from optparse import OptionParser, OptionGroup

import SA.SACommunicationBase as sacom
from LIB.logResults import LogResults
from LIB.actuator import Actuator

from inspect import currentframe, getframeinfo

PEAKSEARCHTIME = 2 # seconds

DBNAME='Mars_5_8'
DBTBLNAME='FEB'

CELLTABLENAME = "CellTable"
SATABLENAME = "SaTable"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'

# data base definition constants
CELLCMS = [[TIMESTAMPCOLNAME, "%s", 'VARCHAR(35)'],
           [CELLTABLENAME,    "%s", 'VARCHAR(40)'],
           [SATABLENAME,      "%s", 'VARCHAR(40)'],
           ["RefCellNumber",  "%s", 'VARCHAR(20)'],
           ["LogFileName",    "%s", 'VARCHAR(100)'],
           ["Duration",       "%s", 'VARCHAR(20)'],
           ["options",        "%s", 'VARCHAR(1024)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '5.845 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

DATACMS = [["CellNumber",   "%s", 'VARCHAR(25)'],
           ["PhaseShift", "%s", 'VARCHAR(25)']]


sSAIP='k-n9020a-00217'
sSAPORT='5023'


def getSetActuators(option) :
    """ getSetActuators -- Uses option.actuator to create an actuator object for each
                           actuator in the list (comma separated).
                           returns the act1 and act2 objects as well as
                           the act[12]Type.
    """
    acts = option.actuator
    acthostname = None
    act = None
    try :
        acthostname = acts.strip()
    except IndexError :
        pass
    else :
        act = Actuator(acthostname, option.actport, debug=option.debug)

    return act

def parseList(strval) :

    commalist = strval.split(",")

    rtnlist = list()
    for dashval in commalist :
        dl = dashval.split('-')
        if len(dl) == 2 :
            for dd in range(int(dl[0]), int(dl[1])+1) :
                rtnlist.append(dd)
        else :
            rtnlist.append(int(dashval))
        
    rtnlist.sort()
    return rtnlist



class MarsPhaseSwitch(object) :
    """
    Class to define the Power Characterization test
    """

    def __init__(self, option) :

        self.errorCondition = False

        try :
            self.cellList = parseList(option.celllist)
        except Exception as ex :
            print("Exception in cellist creation -- '{}'".format(repr(ex)))
            self.errorCondition = True
        else :

            self.debug   = option.debug

            option.debug = False
            self.act = getSetActuators(option)
        # setup the SA
            self.sa      = sacom.SACom(addr=option.saipaddr, port=int(option.saport), debug=option.debug)
            self.logger  = LogResults(option.logfile, option.directory, console=option.console, debug=option.debug)
            self.average = option.average
            self.refCell = option.refcell


    def getSAPower(self) :
        """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                          sapower is a float
                          safreq is a float
        """
        sapower = -60.0
        safreq = -2.0
        if self.sa.usesa :
            if self.average :
                self.sa.avgHold = ""
            else :
                self.sa.maxHold = ""
            # wait for data to get stable
            sleep(PEAKSEARCHTIME)
            # find the freq of the peak
            self.sa.peakSearch = ""
    
            # get the SA data
            self.sa.triggerHold = ''
            SApower, SAfreq = self.sa.peakSearch
            self.sa.triggerClear = ''
            self.sa.clearHold = ''
            try :
                sapower = float(SApower)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SApower ValueError on '{}' fline {}.{} -- {}".format(SApower, frmInfo.filename, frmInfo.lineno, ve))
                sapower = -1.0
            try :
                safreq = float(SAfreq)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SAfreq ValueError on '{}' fline {}.{} -- {}".format(SAfreq, frmInfo.filename, frmInfo.lineno, ve))
                safreq = -1.0
        return sapower, safreq
    

    def runthephase(self,  cell) :

        finished = False

        for phase in range(16) :

            argv = ["setcell", str(cell), "phsft", str(phase)]
            rtnstr = self.act.sendCommandList(argv)
            if self.debug :
                print("Set Cell return string = '{}'".format(rtnstr))

            rtnstr = self.shiftAll()
            if self.debug :
                print("Shift All return string = '{}'".format(rtnstr))

            #self.sa.referenceLevelOffset = refLvl

            # add the client ID to the log list
            self.logger.logAddValue('CellNumber', cell)
            self.logger.logAddValue('PhaseShift', phase)

                # set SA to obtain peak or average value
            sapower, safreq = self.getSAPower()
            if self.average :
                self.logger.logAddValue('SAPowerAvg', sapower)
                self.logger.logAddValue('SAFreqAvg', safreq)
            else :
                self.logger.logAddValue('SAPowerPeak', sapower)
                self.logger.logAddValue('SAFreqPeak', safreq)

            if self.logger.logResults() :
                print("Couldn't log values for Cell {} and phase {}.".format(cell, phase))
                finished = True
                break
            if cell == self.refCell :
                break

        return finished
    
    def shiftAll(self) :
        argv = ["shiftall", "1000"]
        rtnstr = self.act.sendCommandList(argv)
        return rtnstr

    def main(self, option) :

        self.logger.initFile(CELLCMS)

        if option.logtodb :
            self.logger.initDB(option.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(option.tblname)
        # add option Dict to log list

        tableTS = str(int(self.logger.timeStampNum))
        CELLTABLENAME_S = "cell_{}".format(tableTS)
        SATABLENAME_S = "sa_{}".format(tableTS)
        options_val =  str(repr(option.__dict__).replace("'","").replace('"',"").replace(","," "))

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue(CELLTABLENAME, CELLTABLENAME_S)
        self.logger.logAddValue(SATABLENAME, SATABLENAME_S)
        self.logger.logAddValue("RefCellNumber", self.refCell)
        self.logger.logAddValue("LogFileName", self.logger.filename)
        self.logger.logAddValue("options", options_val)

        if self.logger.logResults() :
            print("Couldn't log values for Transmitter.")
            return

            # make the logging list
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
        # the the SA
            self.sa.setSAValue(cmd_data[0], cmd_data[1])

        # init the logging list
        self.logger.initFile(sacms,how='a')
        if option.logtodb :
            self.logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                self.logger.logAddValue(cmd_data[0], cmd_data[1])
        if self.logger.logResults() :
            print("Couldn't log values for SA.")
            return

        datacms = DATACMS
        if self.average :
            datacms.append(["SAPowerAvg", "%3.2f", 'VARCHAR(10)'])
            datacms.append(["SAFreqAvg", "%6.4e", 'VARCHAR(10)'])
        else :
            datacms.append(["SAPowerPeak", "%3.2f", 'VARCHAR(10)'])
            datacms.append(["SAFreqPeak", "%6.4e", 'VARCHAR(10)'])

        if self.debug :
            print("FNAME={}".format(self.logger.filename))
            print("COLFMT={}".format(self.logger.colformat))

        self.logger.initFile(datacms,how='a')

        if options.logtodb :
            self.logger.initDBTable(CELLTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        # set verbose off
        if not self.debug :
            argv = ["verbose", "off"]
            rtnstr = self.act.sendCommandList(argv)
            
        # init the act gpios
        argv = ["initoutputs"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Init act gpios returned '{}'".format(rtnstr)) 

        # init the expander gpios
        argv = ["configure","1000"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Configure GPIOS returned '{}'".format(rtnstr)) 

        # reset the internal data list
        argv = ["resetsetdsl"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Clear DSL returned '{}'".format(rtnstr)) 

        #shift it out
        rtnstr = self.shiftAll()
        if self.debug :
            print("Clear chain returned '{}'".format(rtnstr)) 

        # set ref cell on
        argv = ["setcell",str(self.refCell), "paen", "paon", "phsft", "0"]
        rtnstr = self.act.sendCommandList(argv)
        if self.debug :
            print("Set reference cell returned '{}'".format(rtnstr)) 

        for cell in self.cellList :

            try :
                argv = ["setcell", str(cell), "paen", "paon", "phsft", "0"]
                rtnstr = self.act.sendCommandList(argv)

                finished = self.runthephase(cell)

                if cell != self.refCell : # leave ref cell on
                    argv = ["setcell", str(cell), "paen", "stdby", "phsft", "0"]
                    rtnstr = self.act.sendCommandList(argv)
                    if self.debug :
                        print("Reset cell = '{}'".format(rtnstr))

            except KeyboardInterrupt as ki :
                print("Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                finished = True
                break
            except Exception as ex :
                print("Unexpected Exception exiting --- {}".format(repr(ex)))
                finished = True
                break

        argv = ["resetsetdsl"]
        rtnstr = self.act.sendCommandList(argv)
        self.shiftAll()

        if not self.debug :
            argv = ["verbose", "on"]
            rtnstr = self.act.sendCommandList(argv)

        return finished


if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    equipconns = OptionGroup(parser, "Equipment IPs and port numbers", "Used to connect to the required test equipment.")
    equipconns.add_option("","--saport",   dest='saport',    type=int,  action='store',default=sSAPORT,help="Spectrum Analyzer telnet port number. (default='%default')")
    equipconns.add_option("","--saip",     dest='saipaddr',  type=str,  action='store',default=sSAIP,help="Spectrum Analyzer network address (default='%default')")
    equipconns.add_option("","--actip",    dest='actuator',  type=str,  action='store',default="ossiabbb8",help="SPI Server network address. (default='%default')")
    equipconns.add_option("","--actport",  dest='actport',   type=int,  action='store',default=7001,help="SPI Server. (default='%default')")
    equipconns.add_option("","--refcell",  dest='refcell',   type=int,  action='store',default=1,help="Phase Shift Reference cell. (default='%default')")
    equipconns.add_option("","--celllist", dest='celllist',  type=str,  action='store',default="1-32",help="List of cell to iterate over. (default='%default')")
    parser.add_option_group(equipconns)

    booleanfunc = OptionGroup(parser, "True False flags", "Set option to enable function.")
    booleanfunc.add_option("","--average", dest='average',              action='store_true',default=False,help="Turn on average hold SA data (default='%default')")
    booleanfunc.add_option("","--logtodb", dest='logtodb',              action='store_false',default=True,help="Log data to the DataBase at ossia-build (default='%default')")
    booleanfunc.add_option("","--console", dest='console',              action='store_true',default=False,help="Turn off printing to console (default='$default')")
    parser.add_option_group(booleanfunc)

    parser.add_option("","--logfile",      dest='logfile',   type=str,  action='store',default="MarsPhaseSwitch_{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added. (default='%default')")
    parser.add_option("","--directory",    dest='directory', type=str,  action='store',default="/home/ossiadev/Test/MarsCellTests/Logs/",help="Log file directory. (default='%default')")
    parser.add_option("","--dbname",       dest='dbname',    type=str,  action='store',default=DBNAME,help="Set the DataBase name at ossia-build (default='%default')")
    parser.add_option("","--tblname",      dest='tblname',   type=str,  action='store',default=DBTBLNAME,help="Set the Data Table Name in the DataBase (default='%default')")

    parser.add_option("-d","--debug",      dest='debug',                action="store_true",default=False,help="print debug info. (default='%default')")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        print("Extra command line args, exiting")
        print(repr(args))
        exit(1)

    if options.debug :
        print("<pre>options = {}</pre>".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))

    phaseShifter = MarsPhaseSwitch(options)
    Duration = "None"
    if not phaseShifter.errorCondition :
        phaseShifter.main(options)

        duration = int(time() - START_TIME)
        hrs = float(duration) / 3600.0
        mins = (hrs - int(hrs)) * 60.0
        secs = (mins - int(mins)) * 60.0
        secs += 0.000005
    
        Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    else :
        Duration = "Error: No Clients Specified"
    fd = open(phaseShifter.logger.filename, 'a')
    if fd :
        fd.write("\nDuration, {}\n".format(Duration))
        fd.close()
    if options.logtodb :
        try :
            phaseShifter.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
        except Exception as e : 
            print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {}\n".format(Duration))
