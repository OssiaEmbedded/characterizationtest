''' PwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys
#if "/home/ossiadev/pylib" not in sys.path :
#    sys.path.insert(0, "/home/ossiadev/pylib")
from time import sleep, time, localtime
from optparse import OptionParser, OptionGroup

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from LIB.actuator import Actuator
import LIB.MCSwitch as mcs

from inspect import currentframe, getframeinfo

SMAPORTLOSS = 19.8 # minus dB
PEAKSEARCHTIME = 2 # seconds

TESTDBNAME='TestLabOne'

TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME = "SaTable"
TXTBLNAME="ChargerConfig"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossiadevuwm.local'

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(40)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["PmusEnabled","%s", 'VARCHAR(50)'],
         ["RecsEnabled","%s", 'VARCHAR(50)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["aliasName", "%s", 'VARCHAR(30)'],
             ["netCurrent", "%3.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%3.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%5f", 'VARCHAR(10)'],
             ["peakPower", "%5f", 'VARCHAR(10)'],
             ['DistanceZ', "%d", 'VARCHAR(10)'],
             ['DistanceX', "%d", 'VARCHAR(10)'],
             ['DistanceY', "%d", 'VARCHAR(10)'],
             ['portNumber', "%d", 'VARCHAR(10)'],
             ["cableLoss", "%5.2f", 'VARCHAR(10)'],
             ["TxAngle", "%5.1f",'VARCHAR(7)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["PowerLevel", "%d", 'VARCHAR(5)'],
             ["WarmupTime", "%d",'VARCHAR(7)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["clientComRSSI", "%d", 'VARCHAR(6)'],
             ["proxyComRSSI", "%d", 'VARCHAR(6)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["TPSMissed", "%d",    'VARCHAR(20)'],
             ["version", "%d", 'VARCHAR(20)'],
             ["Model", "%d",        'VARCHAR(10)'],
             ["LongBeaconCnt", "%d",    'VARCHAR(20)'],
             ["NumBeaconCnt", "%d",    'VARCHAR(20)'],
             ["WatchDogRstCnt", "%d",    'VARCHAR(20)'],
             ["MaxCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["MaxZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["AvgCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["AvgZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_4", "%d",   'VARCHAR(10)']]


sTXIP='ossiadevuwm.local'
sTXPORT='50000'
sSAIP='ossiadevuwm.local'
sSAPORT='5023'
srvPORT='50081'


def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            print("Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            print(msg)
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getSetActuators(options) :
    """ getSetActuators -- Uses options.actuator to create an actuator object for each
                           actuator in the list (comma separated).
                           returns the act1 and act2 objects as well as
                           the act[12]Type.
    """
    acts = options.actuator.split(",")
    act1hostname = None
    act1Type = None
    act1 = None
    try :
        act1hostname = acts[0].strip()
    except IndexError :
        pass
    else :
        act1 = Actuator(act1hostname, None, debug=options.debug)
        if act1 != None :
            try : 
                act1Type = int(act1.sendCommandString(arg0='getConfigVal').split()[0])
            except ValueError :
                act1 = None

    act2hostname = None
    act2Type = None
    act2 = None
    try :
        act2hostname = acts[1].strip()
    except IndexError :
        pass
    else :
        act2 = Actuator(act2hostname, None, debug=options.debug)
        if act2 != None :
            try :
                act2Type = int(act2.sendCommandString(arg0='getConfigVal').split()[0])
            except ValueError :
                act2 = None
    return act1, act2, act1Type, act2Type

def getTxId(tx) :
    argv = ["get_charger_id"]
    v = tx.sendGetTransmitter(argv)
    txId = jcs.getTxId(v)
    return txId


def makeClientList(options) :
    """ makeClientList -- makes a list from specified input values or
                          if the input is None then it will discover the
                          client list.
                          Returns True if there was an Error
                          Returns False if no error condistion was present.
    """
    capcList = list()

    clientId = options.clientid
    errorCondition = False
    if clientId is None :
        print("No clients Specified")
        errorCondition = True
    # a client was specified so lets just use it
    else : # make a client list

        def chgNone(cl, al, pl, cll) :
            try :
                cl = cl.strip().lower()
            except :
                cl = ""
            try :
                al = al.strip()
            except :
                al = "No Alias"
            try :
                pl = int(pl)
            except :
                pl = 0
            try :
                cll = float(cll)
            except :
                cll = 0.0

            return cl, al, pl, cll

        clst = clientId.split(',')
        alst = options.aliasname.split(',')
        plst = options.portnumber.split(',')
        cllst = options.cableloss.split(',')
        capcList = map(chgNone, clst, alst, plst, cllst)

    return errorCondition, capcList

def getCotaConfigValues(options) :
    from LIB.sendTestServer import SendTestServer

    sts=SendTestServer(options.txipaddr, int(srvPORT), options.debug)
    jVal = sts.sendCommand(['getConfigFile'])
    rtnstr, plst, _ = sts.parseCotaConfigValues(jVal)

    return rtnstr, plst

def makeOptionTypeDict(myParse, options) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    valObj = options.__dict__
    for myObj in myParse.option_list :
        if myObj.dest is not None : # ignores the help option
            myAction = str(myObj.action)
            myTst = False
            if valObj[myObj.dest] is None :  # check for the default value
                if myAction == "store_true" :  # check for the default value
                    valObj[myObj.dest] = False  # check for the default value
                    myTst = True
                elif myAction == "store_false" :  # check for the default value
                    valObj[myObj.dest] = True  # check for the default value
                    myTst = True
            rtndict.update({myObj.dest: [myObj.type, myObj.default, myTst]})

    for j in range(len(myParse.option_groups)) :
        for myObj in myParse.option_groups[j].option_list :
            myAction = str(myObj.action)
            myTst = False
            if valObj[myObj.dest] is None :  # check for the default value
                if myAction == "store_true" :  # check for the default value
                    valObj[myObj.dest] = False  # check for the default value
                    myTst = True
                elif myAction == "store_false" :  # check for the default value
                    valObj[myObj.dest] = True  # check for the default value
                    myTst = True
            rtndict.update({myObj.dest: [myObj.type, myObj.default, myTst]})
    return rtndict


"""
Class to define the Power Characterization test
"""

class PwrCharTest(object) :

    def __init__(self, options) :

        # init the TX communication object
        self.errorCondition, self.capcList =  makeClientList(options)
        self.shapeNumber = -1


        if not self.errorCondition :
            self.shapeList = options.shapelist

            self.tx   = TXComm(options.txipaddr, int(options.txport), options.debug)
            self.txId = getTxId(self.tx)

            self.act1, self.act2, self.act1Type, self.act2Type = getSetActuators(options)
            if self.act1Type != 2 :
                self.shapeList = False

        # setup the SA
            self.sa     = sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
            self.logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)


    def getSAPower(self, saimage, option, cChanFreq, cChanNum) :
        """ getSAPower -- accesses the SA if enabled and returns the Power and freq
                          sapower is a float
                          safreq is a float
        """
        sapower = -60.0
        safreq = -2.0
        if self.sa.usesa :
            if option.average :
                self.sa.avgHold = ""
            else :
                self.sa.maxHold = ""
            # wait for data to get stable
            sleep(PEAKSEARCHTIME)
            # find the freq of the peak
            self.sa.peakSearch = ""
    
            # get the SA data
            self.sa.triggerHold = ''
            SApower, SAfreq = self.sa.peakSearch
            if saimage > 0 and cChanFreq is not None and cChanNum is not None :
                try :
                    tstEval = "%6.4e != %6.4e" % (SAfreq, float(cChanFreq[str(cChanNum)]))
                    if saimage == 2 or eval(tstEval) :
                        filename = "{}/saImage_{}.png".format(option.directory, int(time()))
                        saI = sacom.SACom(addr=option.saipaddr, port=80, debug=option.debug)
                        saI.getSaveImage(filename)
                        self.logger.logAddValue("ImageSA", filename)
                except KeyError as ke :
                    print("KeyError in comChanNum -- {}\n".format(repr(ke)))
                except ValueError as ve :
                    print("ValueError in comChanNum -- {}\n".format(repr(ve)))
                except TypeError as te :
                    print("TypeError in comChanNum -- {}\n".format(repr(te)))
    
            self.sa.triggerClear = ''
            self.sa.clearHold = ''
            try :
                sapower = float(SApower)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SApower ValueError on '{}' fline {}.{} -- {}".format(SApower, frmInfo.filename, frmInfo.lineno, ve))
                sapower = -1.0
            try :
                safreq = float(SAfreq)
            except ValueError as ve :
                frmInfo = getframeinfo(currentframe())
                print("SAfreq ValueError on '{}' fline {}.{} -- {}".format(SAfreq, frmInfo.filename, frmInfo.lineno, ve))
                safreq = -1.0
        return sapower, safreq
    
    def calibrateAlgorithm(self, option) :
        """ calibrateAlgorithm -- first resets the tile and then performs calibrate after that.
                                  calibrate 5 times getting SA PeakPower each time. The maximum of that
                                  sequence will then be used as a goal for up  to 10 more calibrates.
                                  When one of them is equal to (+- .5) or greater than the max the
                                  algorithm finishes without calibrating again.
        """
        saMax = -100.0
        saPwr, _ = self.getSAPower(0, option, None, None)
        saMax = max(saMax, saPwr)
        rtnval = True
    
        argv = ['reset']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Calibrate")
            rtnval = False
        sleep(6)
        argv = ['calibrate']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Calibrate")
            rtnval = False
        sleep(6)
    
        if rtnval :
            for cal in range(5) :
                saPwr, _ = self.getSAPower(0, option, None, None)
                saMax = max(saMax, saPwr)
                print("1 {} saMax={} saPwr={}".format(cal, saMax, saPwr))
        
                argv = ['calibrate']
                v = self.tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Transmitter did not Calibrate")
                    rtnval = False
                    break
                sleep(6)
    
            saPwr, _ = self.getSAPower(0, option, None, None)
            saMax = max(saMax, saPwr)
    
        if saMax > saPwr and rtnval :
            for cal in range(10) :
                argv = ['calibrate']
                v = self.tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Transmitter did not Calibrate")
                    rtnval = False
                sleep(6)
                saPwr, _ = self.getSAPower(0, option, None, None)
                if (saMax - 0.3) < saPwr :
                    break
                print("2 {} saMax - 0.3={} saPwr={}".format(cal, (saMax - 0.3), saPwr))
    
        print("\nFinal saMax={} saPwr={}\n".format(saMax, saPwr))
        return rtnval
    
    
    def getVersions(self) :
        argv = ["versions"]
        v = self.tx.sendGetTransmitter(argv)
        vers = v['Result']
        swRel = int(vers["Release Version"])
        versionstr = "SWRel:{:X}.{:X}.{:X}\n".format(((swRel>>16) & 0xF), ((swRel >> 8) & 0xF), (swRel & 0xF))
    
        blddate = int(vers['Daemon Build Info']['Date'])
        bldnum  = int(vers['Daemon Build Info']['Number'])
        versionstr += "Daemon:{}:{}\n".format(blddate, bldnum)

        blddate = int(vers["Driver Lib Build Info"]["Date"])
        bldnum  = int(vers["Driver Lib Build Info"]["Number"])
        versionstr += "DrvLib:{}:{}\n".format(blddate, bldnum)

        blddate = int(vers["Message Manager Build Info"]["Date"])
        bldnum  = int(vers["Message Manager Build Info"]["Number"])
        versionstr += "MesMan:{}:{}\n".format(blddate, bldnum)

        bldnum = int(vers["FPGA Revision"])
        versionstr += "FPGA:0x{:04X}\n".format(bldnum)

        bldnum = int(vers["Proxy FW Revision"])
        versionstr += "PROXY:0x{:04X}\n".format(bldnum)

        versionstr += "OS:{}".format(str(vers["OS Version"]))
            
        return versionstr

    def getOnChannels(self) :
        #
        ch_on = 0
        argv = ["info"]
        v = self.tx.sendGetTransmitter(argv)
        chList = v['Result']['AMBS']
        rtnstr = ""
        for chan in chList :
            outStr = ""
            try :
                chOnStr = str(chan['Status'])
                chOn = 1 if chOnStr == 'ON' else 0
                chNum = int(chan['Channel Number'])
                rev = int(chan['AMB Revision'])
                # make the def smaller, take out all the extra unneeded characters
                outStr = "{}:{:02d}:{:04X}\n".format(chOnStr, chNum, rev)
                # chOn will be either 1 or 0, shifting a zero is benign.
                ch_on |= (chOn << chNum)
            except ValueError as ve :
                print("Chan Info ValueError {} -- {}".format(v['Result']['Status'], ve))
                outStr = "{}:{}:{}\n".format("OFF", "ZZ", "XXXX")
            rtnstr += outStr

        chStr = "0x{:04X}".format(ch_on)
        return chStr, rtnstr

    def positionDeviceShape(self, testlist) :
        """ positionDeviceShape -- position the device according to the existing shape
                                   The test list will have a shape defined in it as well
                                   as any other normal parameters.
                                   It will return the current z and x distance as well as
                                   a flag to state if the shape is finished.
        """
        rtnval = True
        curZ = "0"
        curX = "0"
        sNum = testlist.get('shapenumber', None)
        if sNum != self.shapeNumber :
            sType = testlist.get('shape', None)
            step  = testlist.get('step', None)
            cmdlst = ['setShapeZX']
            if sType == 'cir' or sType == 'arc' :
                radius = testlist.get('radius', None)
                center = testlist.get('center', None)
                center = center.replace(' ','')
                cmdlst.extend(["cir", str(radius), str(step), str(center), "zx"])
                expectedShape = "CIRSHAPE"
                if sType == 'arc' :
                    startangle = testlist.get('startangle', None)
                    sweepangle = testlist.get('sweepangle', None)
                    cmdlst.extend(["arc", str(radius), str(step), str(center), str(startangle), str(sweepangle), "zx"])
                    expectedShape = "ARCSHAPE"
            elif sType == 'lin' :
                pointb = testlist.get('pointb', None)
                pointe = testlist.get('pointe', None)
                pointb = pointb.replace(' ','')
                pointe = pointe.replace(' ','')
                cmdlst.extend(["lin", str(pointb), str(pointe), str(step), "zx"])
                expectedShape = "LINSHAPE"
            cmd = self.act1.makeCommandStringFromList(cmdlst)
            rtnstr = self.act1.sendGetData(cmd).split()[0]
            if str(rtnstr) == expectedShape :
                cmd = self.act1.makeCommandStringFromList(['nextZXMove'])
                rtn, curZ, curX  = str(self.act1.sendGetData(cmd)).split()[0].split(",")
                if "STEPPED" == rtn :
                    self.shapeNumber = sNum
                    rtnval = False
        else :
            cmd = self.act1.makeCommandStringFromList(['nextZXMove'])
            rtn, curZ, curX = str(self.act1.sendGetData(cmd)).split()[0].split(",")
            if "STEPPED" == rtn :
                rtnval = False
            elif "RESET" == rtn :
                cmd = self.act1.makeCommandStringFromList(['shapeCleanUp'])
                rtnstr = self.act1.sendGetData(cmd)
                self.shapeNumer = -1

        return rtnval, curZ, curX
        

    def positionDevice(self, zPos, xPos, yPos) :
        """ positionDevice -- position the actuator at the XY position specified.
                              kwargs will have the following dictionary keys
                              act1 -- Actuator object for either a Z axis linear
                                      or a ZX linear/linear.
                              act2 -- Actuator object for an X axis linear will not be present
                                      or set to None if act1 is a ZX actuator.
                              Zpos -- position in mm on the Z axis
                              Xpos -- position in mm on the X axis
        """
        setPosRtnDict = dict()
        getPosRtnDict = dict()
        rtnval = ''
        if self.act1Type == 2 :
            # a single controller ZX actuator
            cmd = self.act1.makeCommandStringFromList(["setPositionZX", str(zPos), str(xPos), "zx"])
            setPosRtnDict.update({'setZX' : self.act1.sendGetData(cmd)})
            cmd = self.act1.makeCommandStringFromList(["getPosition", "mc1"])
            getPosRtnDict.update({'getZ' : self.act1.sendGetData(cmd)})
            cmd = self.act1.makeCommandStringFromList(["getPosition", "mc2"])
            getPosRtnDict.update({'getX' : self.act1.sendGetData(cmd)})
        elif self.act1Type == 3 and self.act2Type == 4 :
            # two controller ZX actuator mc1 is a tandem controller
            cmd = self.act1.makeCommandStringFromList(["setPosition", str(zPos), "mc1"])
            rtnval = self.act1.sendGetData(cmd)
            cmd = self.act2.makeCommandStringFromList(["setPosition", str(xPos), "mc1"])
            rtnval += self.act2.sendGetData(cmd)
            setPosRtnDict.update({'setZX' : rtnval})
            cmd = self.act1.makeCommandStringFromList(["getPosition", "mc1"])
            getPosRtnDict.update({'getZ' : self.act1.sendGetData(cmd)})
            getPosRtnDict.update({'getX' : self.act2.sendGetData(cmd)})
        if yPos is not None and self.act2Type == 4 :
            cmd = self.act2.makeCommandStringFromList(["setPosition", str(yPos), "mc2"])
            rtnval = self.act2.sendGetData(cmd)
            setPosRtnDict.update({'setY' : rtnval})
            cmd = self.act1.makeCommandStringFromList(["getPosition", "mc2"])
            getPosRtnDict.update({'getY' : self.act2.sendGetData(cmd)})
        elif yPos is not None and self.act2Type == 1 :
            cmd = self.act2.makeCommandStringFromList(["setPosition", str(yPos), "mc1"])
            rtnval = self.act2.sendGetData(cmd)
            setPosRtnDict.update({'setY' : rtnval})
            cmd = self.act1.makeCommandStringFromList(["getPosition", "mc1"])
            getPosRtnDict.update({'getY' : self.act2.sendGetData(cmd)})
        

        for k in getPosRtnDict :
            tmplst = getPosRtnDict[k].split()
            for val in tmplst :
                try :
                    getPosRtnDict[k] = int(val)
                    break
                except ValueError :
                    pass

        return setPosRtnDict, getPosRtnDict

    def stopStartChargingList(self, ss, client2charge) :
        # ss must be either 'stop' or 'start' for this to work
        rtnval = False
        cmd = "{}_charging".format(ss)
        failword = "{}CHARGING".format("NOT_" if ss == "start" else '')
        for sClientId, aliasname, _, _ in self.capcList :
            if client2charge != 'all' and sClientId != client2charge :
                continue
            argv = [cmd, sClientId]
            try :
                v = self.tx.sendGetTransmitter(argv)
                if v['Result']['Devices'][0]['Status'] == failword :
                    print("Charging did not '{}' on client {} {} : {}".format(ss, sClientId, aliasname, repr(v['Result']["Devices"][0]['Status'])))
                    rtnval = True
            except :
                rtnval = True
        return rtnval

    def transmitterCalibrate(self, msg='None', prnt=True) :
        rtnval = False #stopStartChargingList(tx, 'stop', capcList)

        if not rtnval :
            argv = ['calibrate']
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Transmitter did not Calibrate: {}".format(msg))
                rtnval = True

        sleep(8)
        if not rtnval :
            rtnval = False #stopStartChargingList(tx, 'start', capcList)

        if prnt :
            print("Calibration {} CL={}".format("Failed" if rtnval else "Done", repr(self.capcList)))

        return rtnval

    def getClientCommandData(self, sClientId, num, delayTime=2.5) :
        argv = ['client_command', sClientId, num]
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Client Command {} request Failed : {}".format(num, repr(v)))
        sleep(delayTime)

        argv = ['client_command_data', sClientId]
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Client Command Data request Failed ({}): {}".format(num, repr(v)))
        return v


    def runthecharacter(self, testlist, comChanNum, options) :

        finished = False
        iterateCnt = -1
        iterateCntUp = 1
        maxAvgPower = 0.0
        maxBatteryLevel = 0.0
        comChanFreq = { "24" : 2.4601E9, "25" : 2.4501E9, "26" : 2.4401E9}

        yDistance = None
        try :
            zDistance = int(testlist.get('zdistance', 0))
            xDistance = int(testlist.get('xdistance', 0))
            yDistance = int(testlist.get('ydistance', None))
        except ValueError as ve :
            print("ValueError on testlist Distance access -- {}\n".format(repr(ve)))
            return False 
        except TypeError as te :
            print("TypeError on yDistance, more than likely. -- {}\n".format(repr(te)))
    
        try :
            iterateCnt = int(testlist.get('logcount', 10))
        except ValueError :
            print("logcount input error")
            return True

        while True : # loop for moving the actuator through a shape if shapeList is set.
            if self.shapeList :
                stopMove, zDistance, xDistance = self.positionDeviceShape(testlist)
                if stopMove :
                    break
            else :
                self.positionDevice(zDistance, xDistance, yDistance)

    
            client2charge =  testlist.get('clientid', 'all').strip().lower()
            if self.stopStartChargingList('start', client2charge) :
                print("stopStartChargingList error")
                return True
    
            # wait for the warmup period
            warmup = int(testlist.get('warmup', 30))
            reportWait(warmup, prnt=options.console)
    
            if options.poscalibrate or testlist['caljustonce'] :
                if options.calibratealg and not options.poscalibrate :
                    self.calibrateAlgorithm(options)
                else :
                    self.transmitterCalibrate(msg="After warmup", prnt=options.console)
    
            while iterateCnt > 0 and not finished :
    
                timetowait = float(testlist.get('waitfordata', 16)) - (1.0 + int(PEAKSEARCHTIME))
                timetowait = max(timetowait, 1.0)
    
                sleep(timetowait)
    
                iterateCntUp += 1
                iterateCnt -= 1
    
                if options.console and iterateCnt >= 0 :
                    print("{} ".format(iterateCnt), end='')
    
                for sClientId, aliasname, portnumber, cableloss in self.capcList :
                    if client2charge != 'all' and client2charge != sClientId :
                        continue
                    if portnumber > 0 :
                        mcs.setSwitch(portnumber)
                        p = mcs.getSwitch()
                        if p != portnumber :
                            print("Portnumber set/get missmatch -- set {} get {}".format(portnumber, p))
                    refLvl = SMAPORTLOSS + cableloss
                    self.sa.referenceLevelOffset = refLvl
    
                    # add the client ID to the log list
                    self.logger.logAddValue('clientId', sClientId)
                    self.logger.logAddValue('aliasName', aliasname)
                    self.logger.logAddValue('portNumber', portnumber)
                    self.logger.logAddValue('cableLoss', cableloss)
    
                    # add Distances to the log list
                    self.logger.logAddValue('DistanceZ', int(zDistance))
                    self.logger.logAddValue('DistanceX', int(xDistance))
                    if yDistance is not None :
                        self.logger.logAddValue('DistanceY', int(yDistance))
    
                    self.logger.logAddValue("PowerLevel", testlist['powerlevel'])
                    self.logger.logAddValue("WarmupTime", testlist['warmup'])
                    #self.logger.logAddValue("TxAngle", txAngle)
                    self.logger.logAddValue('TxAngle', 0.0)
    
                    # set SA to obtain peak or average value
                    sapower, safreq = self.getSAPower(0, options,  comChanFreq, comChanNum)
                    if options.average :
                        self.logger.logAddValue('SAPowerAvg', sapower)
                        self.logger.logAddValue('SAFreqAvg', safreq)
                    else :
                        self.logger.logAddValue('SAPowerPeak', sapower)
                        self.logger.logAddValue('SAFreqPeak', safreq)
    
                    # get system temperature datum
                    argv = ['get_system_temp']
                    v = self.tx.sendGetTransmitter(argv)
                    systemTemp = 0
                    if jcs.checkForSuccess(v) :
                        print("System Temperature not valid")
                    else :
                        systemTemp = jcs.getSystemTemp(v)
                    # add system Temp to the log list
                    self.logger.logAddValue('systemTemp', systemTemp)
    
                # get charging client details
    
                    argv = ['client_detail', sClientId]
                    v = self.tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v, ckVal="CHARGING") and jcs.checkForSuccess(v, ckVal="LIMITED_CHARGING") :
                        print("Client Detail request shows client {} not charging {}".format(sClientId, repr(v['Result']['Status'])))
    
                    version          = jcs.getClientVersion(v)
                    netCurrent       = jcs.getClientNetCurrent(v)
                    queryFailedCount = jcs.getClientQueryFailed(v)
                    linkQuality      = jcs.getClientLinkQuality(v)
                    batteryLevel     = jcs.getBatteryLevel(v)
                    maxBatteryLevel  = max(maxBatteryLevel, batteryLevel)
                    Model            = jcs.getDeviceModel(v)
                    DeviceStatus     = jcs.getDeviceStatus(v)
                    TPSMissed        = jcs.getTPSMissed(v)
                    averagePower     = jcs.getAveragePower(v)
                    peakPower        = jcs.getPeakPower(v)
                    clientComRSSIVal = jcs.getClientComRSSI(v)
                    proxyComRSSIVal  = jcs.getProxyComRSSI(v)
    
                # changing to averagePower from peakPower per Jorge's request.
                    maxAvgPower     = max(maxAvgPower, averagePower)
                    # add version to the log list
                    self.logger.logAddValue('version', version)
                    # add linkQuality to the log list
                    self.logger.logAddValue('linkQuality', linkQuality)
                    self.logger.logAddValue('clientComRSSI', clientComRSSIVal)
                    self.logger.logAddValue('proxyComRSSI', proxyComRSSIVal)
                    # add netcurrent to the log list
                    self.logger.logAddValue('netCurrent', netCurrent)
                    # add batteryLevel to the log list
                    self.logger.logAddValue('batteryLevel', batteryLevel)
                    # add averagePower to the log list
                    self.logger.logAddValue('averagePower', averagePower)
                    # add peakPower to the log list
                    self.logger.logAddValue('peakPower', peakPower)
                    # add queryFaileCount to the log list
                    self.logger.logAddValue('queryFailedCount', queryFailedCount)
                    self.logger.logAddValue('Model', Model)
                    self.logger.logAddValue('DeviceStatus', DeviceStatus)
                    self.logger.logAddValue('TPSMissed', TPSMissed)
    
                    # add client data values to the log list
                    argv = ['client_data', sClientId]
                    v = self.tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v) :
                        print("Client Data request Failed: " + repr(v['Result']['Status']))
    
                    batteryVoltage = jcs.getClientBatteryVoltage(v)
                    # add batteryVoltage to the log list
                    self.logger.logAddValue('batteryVoltage', batteryVoltage)
    
                    data = jcs.processClientData(v)
                    MaxZCSCNT_1, MaxCalcPwr_1, AvgZCSCNT_1, AvgCalcPwr_1 =  jcs.getClientRFPower(data, 1)
                    MaxZCSCNT_2, MaxCalcPwr_2, AvgZCSCNT_2, AvgCalcPwr_2 =  jcs.getClientRFPower(data, 2)
                    MaxZCSCNT_3, MaxCalcPwr_3, AvgZCSCNT_3, AvgCalcPwr_3 =  jcs.getClientRFPower(data, 3)
                    MaxZCSCNT_4, MaxCalcPwr_4, AvgZCSCNT_4, AvgCalcPwr_4 =  jcs.getClientRFPower(data, 4)
    
                    self.logger.logAddValue('MaxZCSCNT_1',  MaxZCSCNT_1)
                    self.logger.logAddValue('MaxCalcPwr_1', MaxCalcPwr_1)
                    self.logger.logAddValue('MaxZCSCNT_2',  MaxZCSCNT_2)
                    self.logger.logAddValue('MaxCalcPwr_2', MaxCalcPwr_2)
                    self.logger.logAddValue('MaxZCSCNT_3',  MaxZCSCNT_3)
                    self.logger.logAddValue('MaxCalcPwr_3', MaxCalcPwr_3)
                    self.logger.logAddValue('MaxZCSCNT_4',  MaxZCSCNT_4)
                    self.logger.logAddValue('MaxCalcPwr_4', MaxCalcPwr_4)
                    self.logger.logAddValue('AvgZCSCNT_1',  AvgZCSCNT_1)
                    self.logger.logAddValue('AvgCalcPwr_1', AvgCalcPwr_1)
                    self.logger.logAddValue('AvgZCSCNT_2',  AvgZCSCNT_2)
                    self.logger.logAddValue('AvgCalcPwr_2', AvgCalcPwr_2)
                    self.logger.logAddValue('AvgZCSCNT_3',  AvgZCSCNT_3)
                    self.logger.logAddValue('AvgCalcPwr_3', AvgCalcPwr_3)
                    self.logger.logAddValue('AvgZCSCNT_4',  AvgZCSCNT_4)
                    self.logger.logAddValue('AvgCalcPwr_4', AvgCalcPwr_4)
    
                    # add the time entry
                    tm_obj = localtime(time())
                    datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
                    self.logger.logAddValue('timetaken', datatime)
    
                    if options.loglongbcnt :
                        v = self.getClientCommandData(sClientId, '42')
                        longBeaconCnt = jcs.getCommandData(v,42, 5, 4) # offset 5, num of bytes 4
                        numBeaconCnt = jcs.getCommandData(v,42, 1, 4) # offset 1, num of bytes 4
                        self.logger.logAddValue("LongBeaconCnt", longBeaconCnt)
                        self.logger.logAddValue("NumBeaconCnt", numBeaconCnt)
    
                    # get client command data 20
                        v = self.getClientCommandData(sClientId, '20')
                        watchDogRstCnt = jcs.getCommandData(v, 20, 7, 2) # offset 7, num of bytes 2
                        self.logger.logAddValue("WatchDogRstCnt", watchDogRstCnt)
    
                    if self.logger.logResults() :
                        print("Couldn't log values for Client {}.".format(sClientId))
                        finished = True
                        break
    
                    if maxAvgPower >= options.maxrssi :
                        print("peakPower value exceeded peakPower limit: {} < {} cnt {}\n".format(options.maxrssi, peakPower, iterateCntUp-1))
                        if (iterateCntUp-1) >= 3 :
                            finished = True
            if not self.shapeList :
                break
        return finished, maxAvgPower, maxBatteryLevel


    def main(self, tstList, options) :

        comChanNum = '25'
        cota_config = "ND"
        try :
            cota_config, plist = getCotaConfigValues(options)
            comChanNum = plist["Client COM Channel"]
        except Exception as ex :
            print("getCotaConfigValues exception -- {}".format(repr(ex)))
 
        print("The Client list: {}".format(repr(self.capcList)))

        pmusEnabled = ""
        recsEnabled = ""
        for sClientId, aliasname, _, _ in self.capcList :
            # first register the client
            argv = ['register_client', sClientId]
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Register client {} {} failed".format(sClientId, aliasname))
                return
            # configure the client
            argv = ['client_config', sClientId, '0x6']
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Client config failed")
                return
            # check for a non zero Short ID
            argv = ['client_detail', sClientId]
            v = self.tx.sendGetTransmitter(argv)
            shortID = jcs.getClientShortID(v)
            if shortID == "0x0000" :
                print("Short ID shows device is not registered: " + shortID)
                return
            # get the PMUs and RECs that are enabled
            if len(pmusEnabled) > 0 :
                pmusEnabled += " "
                recsEnabled += " "
            v = self.getClientCommandData(sClientId, '17')

            tmp = jcs.getCommandData(v, 17, 1, 4, False)
            pmusEnabled += "{}_{}".format(sClientId, tmp)

            v = self.getClientCommandData(sClientId, '33')

            tmp = jcs.getCommandData(v, 33, 1, 4, False)
            recsEnabled += "{}_{}".format(sClientId, tmp)

            ## get the Transmiter data values for logging.

            """ first get the Transmitter data that we want to log
                This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp
            """

        self.logger.initFile(TXCMS)

        if options.logtodb :
            self.logger.initDB(options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(options.tblname)
        # add options Dict to log list
        val =  str(repr(options.__dict__).replace("'","").replace('"',"").replace(","," ")) #.replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
        self.logger.logAddValue("options", val)

        # add txId to log list
        self.logger.logAddValue("txId", self.txId)
        self.logger.logAddValue("AntennaType",options.antennatype)
        self.logger.logAddValue("CfgComment",options.logcomments)
        self.logger.logAddValue("CotaConf",cota_config)
        # add OnChannels to log list
        self.logger.logAddValue("PmusEnabled", pmusEnabled)
        self.logger.logAddValue("RecsEnabled", recsEnabled)
        OnChannels, ChanList = self.getOnChannels()
        verstr = self.getVersions()
        self.logger.logAddValue("OnChannels", OnChannels)
        v = self.tx.sendGetTransmitter(['get_good_channels'])
        self.logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
        self.logger.logAddValue("Versions", str(verstr))
        self.logger.logAddValue("AmbRev", ChanList)
        self.logger.logAddValue("LogFileName", self.logger.filename)
        tableTS = str(int(self.logger.timeStampNum))
        CLIENTTABLENAME = "client_{}".format(tableTS)
        self.logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
        SATABLENAME = "sa_{}".format(tableTS)

        self.logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        if self.logger.logResults() :
            print("Couldn't log values for Transmitter.")
            return

            # make the logging list
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
        # the the SA
            self.sa.setSAValue(cmd_data[0], cmd_data[1])

        # init the logging list
        self.logger.initFile(sacms,how='a')
        if options.logtodb :
            self.logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                self.logger.logAddValue(cmd_data[0], cmd_data[1])
        if self.logger.logResults() :
            print("Couldn't log values for SA.")
            return

        clientcms = CLIENTCMS
        clientcms.insert(4,["ImageSA", "%s", 'VARCHAR(100)'])
        if options.average :
            clientcms.insert(4,["SAPowerAvg", "%3.2f", 'VARCHAR(10)'])
            clientcms.insert(4,["SAFreqAvg", "%6.4e", 'VARCHAR(10)'])
        else :
            clientcms.insert(4,["SAPowerPeak", "%3.2f", 'VARCHAR(10)'])
            clientcms.insert(4,["SAFreqPeak", "%6.4e", 'VARCHAR(10)'])

        if options.debug :
            print("FNAME={}".format(self.logger.filename))
            print("COLFMT={}".format(self.logger.colformat))

        self.logger.initFile(clientcms,how='a')
        if options.logtodb :
            self.logger.initDBTable(CLIENTTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        powerLevel = 0
        lstDistance1 = 0
        finished = False
        calJustOnce = options.caljustonce
        for itTstDict  in tstList :

            try :
                itTstDict['warmup']     = int(itTstDict.get('warmup', 30))
                itTstDict['zdistance'] = int(itTstDict.get('zdistance', 1000))
                itTstDict['xdistance'] = int(itTstDict.get('xdistance', 1000))
            except ValueError as e :
                print("ValueError converting warmup and txangle {}".format(repr(e)))
                break

            print("Distance1 {} > {} finished {}\n".format(lstDistance1, itTstDict['zdistance'], repr(finished)))

            # if we finished (maxpwr exceeded) and the next distance is closer then skip it
            if finished and lstDistance1 > itTstDict['zdistance'] :
                print("Distance1 {} > {} finished {}\n".format(lstDistance1, itTstDict['zdistance'], repr(finished)))
                continue
            lstDistance1 = itTstDict['zdistance']
            try :
                pwrLvl = int(itTstDict.get('powerlevel', 12))
            except ValueError as e :
                print("ValueError converting powerlevel {}".format(repr(e)))
                pwrLvl = 12
            pwrLvl = max(pwrLvl, 12)
            pwrLvl = min(pwrLvl, 20)
            if powerLevel != pwrLvl :
                powerLevel = pwrLvl
                argv = ['set_power_level', powerLevel]
                v = self.tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Set Power Level failed")
                    return
                argv = ['get_power_level']
                v = self.tx.sendGetTransmitter(argv)
                pl =  jcs.getTransmitterPowerLevel(v)
                if pl != powerLevel :
                    print("Power level incorrect get={} vs set={}".format(pl, powerLevel))
                    powerLevel = pl
            itTstDict['powerlevel'] = powerLevel

            # this function also logs all the client type data to the file and database.
            itTstDict.update({'caljustonce': calJustOnce})

            try :
                finished, _, _ = self.runthecharacter(itTstDict, comChanNum, options)
            except KeyboardInterrupt as ki :
                print("Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                finished = True
                break
            except Exception as ex :
                print("Unexpected Exception exiting --- {}".format(repr(ex)))
                finished = True
                break
            calJustOnce = False

        for client in self.capcList :
            sClientID = str(client)
            argv = ['stop_charging', sClientID]
            print("At End Stop charging client {} argv = {}\n".format(sClientID, repr(argv)))
            v = self.tx.sendGetTransmitter(argv)


if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    cfgtlfiles = OptionGroup(parser, "Conf and Testlist files and names", "Use these to set a testlist and config.")
    cfgtlfiles.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None, help="Use a config file (.py) for cfg lists")
    cfgtlfiles.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg", help="Use this config list for options")
    cfgtlfiles.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    cfgtlfiles.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlist", help="Use this list from the file.")
    parser.add_option_group(cfgtlfiles)
    
    equipconns = OptionGroup(parser, "Equipment IPs and port numbers", "Used to connect to the required test equipment.")
    equipconns.add_option("","--saport",       dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    equipconns.add_option("","--saip",         dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")
    equipconns.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    equipconns.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=sTXIP,help="Transmitter network address.")
    equipconns.add_option("","--actip",        dest='actuator',     type=str,  action='store',default=None,help="Actuator network address(es) (comma sep).")
    parser.add_option_group(equipconns)

    cfgcomments = OptionGroup(parser, "Test setup comments", "Used to record any specific/extra test conditions.")
    cfgcomments.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",help="Define a log value for antenna type")
    cfgcomments.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")
    parser.add_option_group(cfgcomments)

    clientdesc = OptionGroup(parser, "Client definitions", "Use these to define the client to specAn path.")
    clientdesc.add_option("","--clientid",     dest='clientid',     type=str,  action='store',default=None,help="Device/client ID to use for characterization.")
    clientdesc.add_option("","--portnumber",   dest='portnumber',   type=str,  action='store',default="",help="RF Switch port number 1 - 8, Set to 0 for no switch. Set to '' for no SA.")
    clientdesc.add_option("","--cableloss",    dest='cableloss',    type=str,  action='store',default="",help='Cable loss positive dB for each RF switch path. Added to the 20 dB of Connector loss')
    clientdesc.add_option("","--aliasname",    dest='aliasname',    type=str,  action='store',default="",help="Alias name to associate with a clientId.")
    parser.add_option_group(clientdesc)

    booleanfunc = OptionGroup(parser, "Boolean definitions", "Use these to turn on/off various functions.")
    booleanfunc.add_option("","--shapelist",    dest='shapelist',               action='store_true' ,default=False,help="Calibrate at each position change.")
    booleanfunc.add_option("","--poscalibrate", dest='poscalibrate',            action='store_false',default=True,help="Calibrate at each position change.")
    booleanfunc.add_option("","--caljustonce",  dest='caljustonce',             action='store_false',default=True,help="Calibrate or Don't calibrate once at the beginning.")
    booleanfunc.add_option("","--calibratealg", dest='calibratealg',            action='store_true',default=False,help="Use an algorithm to obtain a relatively good calibration.")

    booleanfunc.add_option("","--noconsole",    dest='console',                 action='store_true',default=False,help="Turn off printing to console")
    booleanfunc.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    booleanfunc.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    booleanfunc.add_option("","--loglongbcnt",  dest='loglongbcnt',             action='store_true',default=False,help="Log the long beacon count info")
    parser.add_option_group(booleanfunc)

    parser.add_option("","--batterylevel", dest='batterylevel', type=int,  action='store',default=98,help="The battery level to quit test at.")

    parser.add_option("","--maxpwrlevel",  dest='maxrssi',      type=float,action='store',default='28.0',help="Maximum power level to quit test: a client protection.")

    parser.add_option("","--logfile",      dest='logfile',      type=str,  action='store',default="IntegLab1LogFile{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the Data Table Name in the DataBase")
    parser.add_option("","--saimage",      dest='saimage',      type=int,  action='store',default=0,help="Capture an SA Image on some or every data point.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    import imp
    if len(args) > 0 :
        print("Extra command line args, exiting")
        print(repr(args))
        exit(1)
    elif options.cfgfile is not None :
        TTYPE = 0
        TDEF = 1
        TTEST = 2
        fname = options.cfgfile
        if fname == '' :
            fname = sys.argv[0].replace('.', 'Conf.')

        cfgmod = imp.load_source(options.cfgname, fname)
        cfg = eval("cfgmod."+options.cfgname)
        optType = makeOptionTypeDict(parser, options)

        for k in cfg.keys() :
            try :
                if optType[k][TTYPE] == 'int' :
                    if options.__dict__[k] == optType[k][TDEF] : # check if the default value
                        options.__dict__[k] = int(str(cfg[k]))

                elif optType[k][TTYPE] == 'float' :
                    if str(cfg[k]) == '' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    elif options.__dict__[k] == optType[k][TDEF] : # check if the default value
                        options.__dict__[k] = float(str(cfg[k]))

                elif optType[k][TTYPE] is None : # boolean values
                    if not optType[k][TTEST] :  # check for the default value
                        try :
                            options.__dict__[k] = eval(cfg[k])
                        except TypeError :
                            options.__dict__[k] = cfg[k]
                        except NameError :
                            options.__dict__[k] = False

                elif optType[k][TTYPE]  == 'string' :
                    if options.__dict__[k] == optType[k][TDEF] : # check for the default value
                        if str(cfg[k]) == 'None' :
                            options.__dict__[k] = None
                        else :
                            options.__dict__[k] = str(cfg[k])
                else :
                    options.__dict__[k] = str(cfg[k])
            except ValueError as ve :
                print("<pre>{} -- Incorrect option value {} -- {}</pre>\n".format(k, cfg[k], repr(ve)))
                exit(2)
            except IndexError as ie :
                print("<pre>{} -- No such option -- {}</pre>\n".format(k, repr(ie)))
                exit(3)
            except KeyError as ke :
                print("<pre>{} -- No such option key -- {}</pre>\n".format(k, repr(ke)))
                exit(4)
            except Exception as e :
                print("<pre>{} -- Unknown exception -- {}</pre>\n".format(k, repr(e)))
                exit(5)

    fname = sys.argv[0].replace('.', 'TestList.')

    if options.testlistfile is not None :
        fnm = options.testlistfile
        if fnm != '' :
            fname = fnm

    tstlstmod = imp.load_source(options.testlistname, fname)
    testlist = eval("tstlstmod."+options.testlistname)

    if options.debug :
        print("<pre>options = {}</pre>".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
        print("<pre>testlist = {}</pre>".format(repr(testlist).replace('},','},\n')))

    pwrCharTest = PwrCharTest(options)
    if not pwrCharTest.errorCondition :
        pwrCharTest.main(testlist, options)

        duration = int(time() - START_TIME)
        hrs = float(duration) / 3600.0
        mins = (hrs - int(hrs)) * 60.0
        secs = (mins - int(mins)) * 60.0
        secs += 0.000005
    
        Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    else :
        Duration = "Error: No Clients Specified"
    fd = open(pwrCharTest.logger.filename, 'a')
    if fd :
        fd.write("\nDuration, {}\n".format(Duration))
        fd.close()
    if options.logtodb :
        try :
            pwrCharTest.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
        except Exception as e : 
            print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
