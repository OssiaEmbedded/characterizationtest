cfgTestBringUp={
'console'      : False,
'txport'       : '50000',
'actuator'     : 'ossiadevuwm.local',
'maxrssi'      : '31.5',
'saipaddr'     : 'ossiadevuwm.local',
'saport'       : '5023',
'average'      : False,
'logfile'      : 'TestBringUpZX_{}.csv',
'logtodb'      : True,
'poscalibrate' : False,
'caljustonce'  : True,
'directory'    : '/home/ursusm/Test/PwrCharZXTest/Logs/',
#'debug'        : True,
#'cableloss'    : '12.19',
#'dbname'       : 'IntegLabRm',
#'tblname'      : 'B4Clients',
}

