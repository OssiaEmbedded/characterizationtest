testlistB4BUGrad=[
{'logcount' : 5,'powerlevel' : 21,'warmup' : 100,'txangle' :   0,'zdistance1' : 1400,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1200,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1150,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1100,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1075,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' :  950,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
]

testlistB4BringupNoWait=[
{'logcount' : 5,'powerlevel' : 21,'warmup' : 200,'txangle' :  45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :  45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :  45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :  45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :  45,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},

{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' :   0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},

{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' : -45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' : -45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' : -45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' : -45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
{'logcount' : 5,'powerlevel' : 21,'warmup' :   0,'txangle' : -45,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' :  5,},
]

testlistB4Bringup=[
{'logcount' : 3,'powerlevel' : 21,'warmup' : 200,'txangle' :  45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
]

testlistB4BringupOrg=[
{'logcount' : 3,'powerlevel' : 21,'warmup' : 200,'txangle' :  45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :  45,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' :   0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1700,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1300,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : -45,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
]

testlistBringup=[
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  550,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  500,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
]

testlistBringupExt=[
{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},

{'logcount' : 3,'powerlevel' : 21,'warmup' : 120,'txangle' : 0,'zdistance1' : 1900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1250,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' : 1000,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  900,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  800,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
{'logcount' : 3,'powerlevel' : 21,'warmup' :  10,'txangle' : 0,'zdistance1' :  600,'zdistance2' : 0,'xdistance1' : 0,'xdistance2' : 0,'ydistance1' : 0,'ydistance2' : 0,'waitfordata' : 10,},
]
