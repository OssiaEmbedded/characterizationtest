from __future__ import print_function
import statistics as ss
import sys
from optparse import OptionParser


def convertLogToDict(filename) :
    rtndict = dict()
    fd = open(filename)

    filelines = fd.readlines()

    txcol = None
    txdat = None
    sacol = None
    sadat = None
    clcol = None
    cldat = None
    rowId = 1
    for line in filelines :
        linelist = line.split(",")
        for i in range(len(linelist)) :
            linelist[i] = linelist[i].strip()
        if linelist[0] == "Duration" :
            rtndict.update({linelist[0] : linelist[1]})
            continue
        if txcol is None :
            txcol = linelist
            continue
        if txdat is None :
            txdat = linelist
            c = dict(zip(txcol,txdat))
            g = dict({"txdata":c})
            rtndict.update(g)
            #print repr(g)
            continue
        if sacol is None :
            sacol = linelist
            continue
        if sadat is None :
            sadat = linelist
            if sacol[0] != "clientId" :
                c = dict(zip(sacol,sadat))
                g = dict({"sadata":c})
                rtndict.update(g)
                #print repr(g)
            else :
                clcol = sacol
            continue
        if clcol is None :
            clcol = linelist
            continue
        if cldat is None :
            c = dict(zip(clcol,linelist))
            rid = "{}".format(rowId)
            rowId += 1
            g = dict({rid:c})
            rtndict.update(g)
            #print repr(g)


    fd.close()
    return rtndict

def getStatsFor(colname, dictname) :
    
    rowId = 1
    lastdist = None
    lastangle = None
    vallist = list()
    statdata = dict()
    for rowId in range(1,len(dictname)) :
        row = dictname.get(str(rowId), None)
        if row is None :
            continue
        #print("row={}".format(repr(row)))
        curdist  = row.get("DistanceZ1", None).strip()
        curangle = row.get("TxAngle", None).strip()
        try :
            curval   = float(row.get(colname, None).strip())
        except Exception as e :
            curval = 0.0
        if lastdist != curdist or lastangle != curangle :
            #print("curangle {} => lastangle {}, curdist {} => lastdist {}, vallist {}".format(curangle, lastangle, curdist, lastdist, repr(vallist)))
            if lastdist is not None and lastangle is not None :
                mean   = ss.mean(vallist)
                median = ss.median(vallist)
                maxval = max(vallist)
                minval = min(vallist)
                try :
                    mode   = ss.mode(vallist)
                except ss.StatisticsError as se :
                    #print("Mode error -- {}".format(repr(se)))
                    mode = None
                if statdata.get(lastangle, None) is None :
                    statdata.update({lastangle : dict()})
                if statdata.get(lastangle, {}).get(lastdist, None) is None :
                    statdata[lastangle].update({lastdist : dict()})
                statdata[lastangle][lastdist].update({"mean" : mean, "median" : median, "max" : maxval, "min" : minval, "mode" : mode, "vallist" : vallist})
            vallist   = list()
            lastdist  = curdist
            lastangle = curangle
        vallist.append(curval)
    return statdata

def getStat(datadict, vType, statname) :
    """ getStat finds all the values of statname in the datadict and returns
        the vType value and the angle and distance where it was found.
    """
    vallist = list()
    for angle, aDat in datadict.iteritems() : 
        for dist, dDat in aDat.iteritems() :
            vlist    = list([dDat[statname], angle, dist])
            vallist.append(vlist)
    index = 0
    if len(vallist) > 0 :
        if vType == "max" :
            for v in range(len(vallist)):
                if vallist[index][0] < vallist[v][0] :
                    index = v
        elif vType == 'min' :
            for v in range(len(vallist)):
                if vallist[index][0] > vallist[v][0] :
                    index = v
    else :
        vallist = [[None, None, None]]
    return vallist[index][0], vallist[index][1], vallist[index][2]

if __name__ == "__main__" :
    
    parser = OptionParser()

    parser.add_option("-u","--unittests",  dest='unittests',             action='store_true', default=False,    help="Run unit tests on the file")
    parser.add_option("-v","--valtype",    dest='valtype',    type=str,  action='store',      default="max",    help="Value type, max or min")
    parser.add_option("-s","--statname",   dest='statname',   type=str,  action='store',      default="median", help="Stat name max, min, mode, median, or mean")
    parser.add_option("-c","--columnname", dest='columnname', type=str,  action='store',      default="median", help="Stat name max, min, mode, median, or mean")
    
    (options, args) = parser.parse_args()

    if options.unittests :
        x = convertLogToDict("/home/ossiadev/Test/PwrCharMTest/Logs/VenusV3_B4_NewTileThu_Feb_21_14_46_30_2019.csv")
    
        y = getStatsFor("peakPower", x)
        v, a, d = getStat(y, "max", "median")
        print("maxval of median -- val={} at a={} d={}".format(v,a,d))

        v, a, d = getStat(y, "min", "median")
        print("minval of median -- val={} at a={} d={}".format(v,a,d))

        v, a, d = getStat(y, "max", "max")
        print("maxval of max    -- val={} at a={} d={}".format(v,a,d))

        v, a, d = getStat(y, "min", "max")
        print("minval of max    -- val={} at a={} d={}".format(v,a,d))

    else :
        for arg in sys.argv :

            try :
                x = convertLogToDict(arg)
            except Exception as e :
                print("Exception in convertLogToDict {} -- {}".format(arg, repr(e)), file=sys.stderr)
                continue
            try :
                y = getStatsFor(options.columnname, x)
            except Exception as e :
                print("Exception in getStatsFor {} -- {}".format(arg, repr(e)), file=sys.stderr)
                continue
            try :
                v, a, d = getStat(y, options.valtype, options.statname)
            except Exception as e :
                print("Exception in getStat {} -- {}".format(arg, repr(e)), file=sys.stderr)
                continue
            else :
                print("{} -- {}val of {} -- val={} at a={} d={}".format(arg, options.valtype, options.statname, v, a, d))


