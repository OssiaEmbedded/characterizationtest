''' logNardaAtLocation.py -- Setup narda and then request user input to go collect data
    An interactive test to allow environment modifications between steps to be accomplished.

Version : 1.0.0
Date :  Feb 28 2020
Copyright Ossia Inc. 2020

'''

from __future__ import absolute_import, division, print_function, unicode_literals

from optparse import OptionParser, Option, OptionValueError
from time import sleep, time, localtime

import LIB.NardaSerial as NS

import math

parser = OptionParser()
parser.add_option("","--comport",    dest='comport',   type=str,  action='store',default="", help="Narda Commport e.g. COM5.")
parser.add_option("","--nardatype",  dest='nardatype', type=str,  action='store',default="serial", help="Narda Type Serial or Ethernet.")
parser.add_option("-f","--filename", dest='filename',  type=str,  action='store',default="log.csv", help="Log filename.")
parser.add_option("-d","--debug",    dest='debug',     type=str,  action='store',default="", help="Debug flag.")
(options, args) = parser.parse_args()

logFileFD = None
try :
    logFileFD = open(options.filename, "w")
except :
    print("Could not open Log file {}".format(options.filename))
else :
    narda  = NS.NardaSerial(options.comport, commethod=options.nardatype, debug=options.debug)
    retval = narda.setupNardaProbe()
    retlst = narda.getupNardaProbe()
    strval = narda.getDeviceInfoString()
    logFileFD.write("Device Information\n{}\n".format(strval))
    logFileFD.write("Device Setup Information\n{}\n".format(retlst))
    logFileFD.write("Data Count, AVG Value, RSS Value, X Value, Y Value, Z Value, X Location, Y Location\n")
    datacnt = 1
    xxloc = 0
    yyloc = 0
    zzloc = 0
    while True :
        try :
            xloc = raw_input("\n{} Enter X location : ".format(datacnt)).strip()
            yloc = raw_input("{} Enter Y location : ".format(datacnt)).strip()
            zloc = raw_input("{} Enter Z location : ".format(datacnt)).strip()
        except KeyboardInterrupt :
            print("Key board interrupt. Exiting\n")
            break
        except EOFError :
            print("EOF Error. Exiting\n")
            break
        except Exception as ee :
            print("Something unexpected. Exiting\n")
            break
        else :
            if xloc != "" :
                xxloc = xloc
            if yloc != "" :
                yyloc = yloc
            if zloc != "" :
                zzloc = zloc

            r, valList = narda.getMeasuredData()
            #vv = (float(valList[2]) * float(valList[2])) + (float(valList[3]) * float(valList[3])) + (float(valList[4]) * float(valList[4]))
            #vv = ( float(valList[2])) + (float(valList[3])) + ( float(valList[4]))
            #vv = math.sqrt(vv)
            #print("vv={:6.3g}".format(vv))
            if r :
                val = "{}, {}, {}, {}, {}, {}, {}, {}, {}".format(datacnt, valList[0] ,valList[1] ,valList[2] ,valList[3] ,valList[4], xxloc, yyloc, zzloc)
                save = raw_input("Save this? {} :".format(val))
                if save.lower() != "no" :
                    logFileFD.write("{}\n".format(val))
                    datacnt += 1
            else :
                print("Error in getting Narda Data\n")

if logFileFD is not None :
    logFileFD.close()
    print("Closed log file")
