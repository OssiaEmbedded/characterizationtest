''' ExperLicense.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.
Controls a Three axis actuator to collect data from a Narda probe.

An interactive test to allow environment modifications between steps to be accomplished.

Version : 1.0.0
Date :  Dec 2 2019
Copyright Ossia Inc. 2019

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import json, re

from time import sleep, time, localtime
from optparse import OptionParser, Option, OptionValueError

import TXJSON.JSonCmdStrings as jcs
from TanXY.TandomXY import TandomXYZ

import LIB.NardaSerial as NS
import LIB.parseCommaColon as pcc
import LIB.sshcomm  as sshcomm

from LIB.timestamp import DurationCalc
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from LIB.utilmod import *
from LIB.sendTestServer import parseCotaConfigValuesFromList

TESTDBNAME='ExperLicense'

TXDATACLIENTTABLENAME = "ClientTable"
TXTBLNAME="ChargerConfig"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         ["NardProbeSetup", "%s", 'VARCHAR(300)'],
         ["NardDeviceInfo", "%s", 'VARCHAR(300)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'] ]


CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["nardaMaxHoldFS", "$7.4f", 'VARCHAR(10)'],
             ["nardaValueFS", "$7.4f", 'VARCHAR(10)'],
             ["nardaXValueFS", "$7.4f", 'VARCHAR(10)'],
             ["nardaYValueFS", "$7.4f", 'VARCHAR(10)'],
             ["nardaZValueFS", "$7.4f", 'VARCHAR(10)'],
             ['positionX', "%d", 'VARCHAR(10)'],
             ['positionY', "%d", 'VARCHAR(10)'],
             ['positionZ', "%d", 'VARCHAR(10)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["batteryVoltage", "%3.2f", 'VARCHAR(10)'],
             ["netCurrent", "%3.2f", 'VARCHAR(6)'],
             ["mWPower",    "%3.2f", 'VARCHAR(6)'],
             ["averagePower", "%5f", 'VARCHAR(10)'],
             ["peakPower", "%5f", 'VARCHAR(10)'],
             ["Model", "%d",'VARCHAR(9)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)']]


def makeStringFromDict(theDict) :
    rtnstr = "Input is not Dict"
    if type(theDict) is dict :
        rtnstr = ""
        for name, value in theDict.iteritems() :
            rtnstr += "{}={}\n".format(name, value)
    return rtnstr

def makeClientAliasList(option) :
    def myZip(c, a) :
        cc = ""
        aa = ""
        if isinstance(c, str) :
            cc = str(c).strip()
        elif isinstance(c, unicode) :
            cc = str(c.decode('utf-8')).strip()
        if isinstance(a, str) :
            aa = str(a).strip()
        elif isinstance(a, unicode) :
            aa = str(a.decode("utf-8")).strip()
        return [cc,aa]
    # process client id CSL
    cl = option.clientid.split(',')
    al = option.cidaliases.split(',')
    CAL = map(myZip, cl, al)
    return CAL


class ExperLicense(object) :

    def __init__(self, options) :

        self.options  = options
        self.com      = sshcomm.SSHlink(options.txipaddr, options.txsshport, "gumstix", 'gumstix', options.debug)
        self.tx       = TXComm(options.txipaddr, int(options.txport), options.debug)
        self.narda    = NS.NardaSerial(options.comport, options.nardatype, options.debug)

        self.position = {'X' : 0, 'Y' : 0, 'Z' : 0}

        tanip_list            = options.actuator.split(",")
        tanport_list          = options.actport.split(",")
        self.tandomXYZ        = TandomXYZ(ipaddr=tuple(tanip_list), portnumber=tuple(tanport_list), debug=options.debug)
        self.actuatorXYZ      = {'X' : None, 'Y' : None, 'Z' : None}
        self.actuatorXYZ['X'] = self.tandomXYZ.sendGetDataX
        self.actuatorXYZ['Y'] = self.tandomXYZ.sendGetDataY
        self.actuatorXYZ['Z'] = self.tandomXYZ.sendGetDataZ

        self.logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)

        self.logtodb = self.options.logtodb
        if self.logtodb :
            if self.options.dbname is None or self.options.tblname is None :
                self.logtodb = False

    def executeCmd(self, cmd, doClose=True) :
        rtnval = 1
        lines = ()
        errors = ()
        strval = ""
        if not self.com.connect() :
            eprint ("ERROR: Could not connect to target '{}'".format(self.options.txipaddr))
        else :
            rtnval = 0
            if isinstance(cmd, unicode) or isinstance(cmd, str) :
                rtnval = self.com.command(cmd)
                lines  = self.com.getlastreadlines()
                strval = "".join(self.com.getlastreadlines())
                errors = self.com.getlastreaderrors()
            elif isinstance(cmd, list) or isinstance(cmd, tuple) :
                for cmdstr in cmd :
                    rtnval += self.com.command(cmdstr)
                    lines  += self.com.getlastreadlines()
                    strval += "".join(self.com.getlastreadlines())
                    errors += self.com.getlastreaderrors()
            if doClose :
                self.com.close()
        return rtnval, lines, errors, strval

    def getCotaConfigValues(self) :

        cmd = 'cat /etc/cota/Cota_Config.json'
        _, _, _, strval = self.executeCmd(cmd)
        pyList = json.loads(strval)
        rtnstr, partiallst, _ = parseCotaConfigValuesFromList(pyList)
        return rtnstr, partiallst

    def getVersions(self) :
        argv = ["versions"]
        v = self.tx.sendGetTransmitter(argv)
        vers = v['Result']
        swRel = int(vers["Release Version"])
        versionstr = "SWRel:{:X}.{:X}.{:X}\n".format(((swRel>>16) & 0xF), ((swRel >> 8) & 0xF), (swRel & 0xF))

        blddate = int(vers['Daemon Build Info']['Date'])
        bldnum  = int(vers['Daemon Build Info']['Number'])
        versionstr += "Daemon:{}:{}\n".format(blddate, bldnum)

        blddate = int(vers["Driver Lib Build Info"]["Date"])
        bldnum  = int(vers["Driver Lib Build Info"]["Number"])
        versionstr += "DrvLib:{}:{}\n".format(blddate, bldnum)

        blddate = int(vers["Message Manager Build Info"]["Date"])
        bldnum  = int(vers["Message Manager Build Info"]["Number"])
        versionstr += "MesMan:{}:{}\n".format(blddate, bldnum)

        bldnum = int(vers["FPGA Revision"])
        versionstr += "FPGA:0x{:04X}\n".format(bldnum)

        bldnum = int(vers["Proxy FW Revision"])
        versionstr += "PROXY:0x{:04X}\n".format(bldnum)
    
        versionstr += "OS:{}".format(str(vers["OS Version"]))
        return versionstr

    def getOnChannels(self) :
        #
        ch_on = 0
        argv = ["info"]
        v = self.tx.sendGetTransmitter(argv)
        chList = v['Result']['AMBS']
        rtnstr = ""
        for chan in chList :
            outStr = ""
            try :
                chOnStr = str(chan['Status'])
                chOn = 1 if chOnStr == 'ON' else 0
                chNum = int(chan['Channel Number'])
                rev = int(chan['AMB Revision'])
                # make the def smaller, take out all the extra unneeded characters
                outStr = "{}:{:02d}:{:04X}\n".format(chOnStr, chNum, rev)
                # chOn will be either 1 or 0, shifting a zero is benign.
                ch_on |= (chOn << chNum)
            except ValueError as ve :
                eprint("ERROR: getOnChannels -- Chan Info ValueError {} -- {}".format(v['Result']['Status'], ve))
                outStr = "{}:{}:{}\n".format("OFF", "ZZ", "XXXX")
            rtnstr += outStr
    
        chStr = "0x{:04X}".format(ch_on)
        return chStr, rtnstr

    def stopStartChargingList(self, ss, clientList) :
        # ss must be either 'stop' or 'start' for this to work
        rtnval = False
        cmd = "{}_charging".format(ss)
        failword = "{}CHARGING".format("NOT_" if ss == "stop" else '')
        for client, alias in clientList :
            sClientId = str(client[0])
            argv = [cmd, sClientId]
            try :
                v = self.tx.sendGetTransmitter(argv)
                if v['Result']['Devices'][0]['Status'] != failword :
                    eprint("ERROR: Charging did not '{}': {}".format(ss, repr(v['Result']["Devices"][0]['Status'])))
                    rtnval = True
            except :
                rtnval = True
        return rtnval

    def transmitterCalibrate(self, msg='None') :

        rtnval = False
        argv = ['calibrate']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ERROR: Transmitter did not Calibrate: {}".format(msg))
            rtnval = True
        return rtnval

    def getClientCommandData(self, sClientId, num, delayTime=2.5) :
        argv = ['client_command', sClientId, num]
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ERROR: Client Command {} request Failed : {}".format(num, repr(v)))
        sleep(delayTime)

        argv = ['client_command_data', sClientId]
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            eprint("ERROR: Client Command Data request Failed ({}): {}".format(num, repr(v)))
        return v

    def collectData(self, clientList, TestList) :
        """ collectData is recursive. The testlist can have a "sublist" that will be passed
            recursively to this method.
            clientList  -- is a list of clients to acquire data from
            testlist    -- is a set of commands to the narda probe, the client, the actuators, and the transmitter
            actuatorXYZ -- is a dictionary of (xaxis, yaxis, zaxis) acutators. The test list will specify which one is
                           active during this recursion.
        """
        axis = "X"
        for testlist in TestList :
            # first get the steplist if present
            try :
                steplist    = testlist.get('steplist', None) # a set of positions to set the actuator specified by 'axis' to
                axis        = testlist.get('axis', axis)      # the X, Y, or Z actuator to use
                actuator    = self.actuatorXYZ[axis]
                sublist     = testlist.get('sublist', None)  # is there a sublist to recurse into?
                pwrLvl      = testlist.get('powerlevel', None) # is there a power level that needs to be set?
                calibrate   = testlist.get('calibrate', False) # is there a need to calibrate?
                setPosition = testlist.get('setposition', None) # is there a position that needs to be set on the XYZ actuator specified by axis?
                logDataHere = testlist.get('logdatahere', False) # should be log data here?
                logCount    = testlist.get('logcount', 1) # how many values to log?
                logDelay    = testlist.get('logdelay', 0.0) # how much time between logs
            except Exception as ee :
                eprint("ERROR: Test List input error -- {}".format(repr(ee)))
    
            else :
                try :
                    logCount = int(logCount)
                except ValueError :
                    logCount = 1
                    
                if setPosition is not None and actuator is not None :
                    actuator(["setPosition", str(setPosition)])
                    self.position[axis] = str(setPosition)
                if pwrLvl is not None :
                    argv = ["set_power_level", pwrLvl]
                    v = self.tx.sendGetTransmitter(argv)
                if calibrate :
                    argv = ["calibrate"]
                    v = self.tx.sendGetTransmitter(argv)
                if logDataHere :
                    for _ in range(logCount) :
                        self.logTheData(clientList)
                        sleep(logDelay)

                if steplist is not None :
                    stepList = pcc.expandStartEndStep(steplist)
                    for step in stepList :
                        self.position[axis] = step
                        if actuator is not None :
                            actuator(["setPosition", str(step)])
                        if sublist is not None :
                            self.collectData(clientList, sublist)
                        else :
                            for _ in range(logCount) :
                                self.logTheData(clientList)
                                sleep(logDelay)
                    

    def logTheData(self, clientList) :

        # get system temperature datum
        argv = ['get_system_temp']
        v = self.tx.sendGetTransmitter(argv)
        systemTemp = 0
        if jcs.checkForSuccess(v) :
            eprint("ERROR: System Temperature not valid")
        else :
            systemTemp = jcs.getSystemTemp(v)

        try :
            valList = self.narda.getMeassuredData()
            self.logger.logAddValue("nardaMaxHoldFS", valList[0])
            self.logger.logAddValue("nardaValueFS",   valList[1])
            self.logger.logAddValue("nardaXValueFS",  valList[2])
            self.logger.logAddValue("nardaYValueFS",  valList[3])
            self.logger.logAddValue("nardaZValueFS",  valList[4])
            valList = self.narda.getBattery()
            self.logger.logAddValue("nardaBattery", valList[0])
        except IndexError as ie :
            eprint("ERROR: index error on narda probe getMeasuredData -- {} -- {}".format(repr(ie), repr(valList)))

        # get charging client details
        for client, alias in clientList :
            sClientId = str(client[0])
            argv = ['client_detail', sClientId]
            v = self.tx.sendGetTransmitter(argv)
            status, statval, statdisc = jcs.checkStatusReturn(v)
            self.logger.logAddValue('clientStatusDisc', statdisc)
            if not status :
                eprint("ALERT: Client Detail request shows client {} returned status {} trying again -- {}".format(sClientId, statval, statdisc), True)

            netCurrent       = jcs.getClientNetCurrent(v)
            batteryLevel     = jcs.getBatteryLevel(v)
            DeviceStatus     = jcs.getDeviceStatus(v)
            TPSMissed        = jcs.getTPSMissed(v)
            averagePower     = jcs.getAveragePower(v)
            peakPower        = jcs.getPeakPower(v)
            Model            = jcs.getDeviceModel(v)
            self.logger.logAddValue('Model', Model)

            # get charging client data
            argv = ['client_data', sClientId]
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                eprint("ALERT: Client Data request Failed: {}".format(repr(v['Result']['Status'])), True)
                batteryVoltage = 0.0
            else :
                batteryVoltage = jcs.getClientBatteryVoltage(v)

            mWPower = batteryVoltage * netCurrent

            self.logger.logAddValue("mWPower", mWPower)
            # add the client ID to the log list
            self.logger.logAddValue('clientId', sClientId)
            self.logger.logAddValue('clientAlias', alias)
            # add netcurrent to the log list
            self.logger.logAddValue('netCurrent', netCurrent)
            # add batteryVoltage to the log list
            self.logger.logAddValue('batteryVoltage', batteryVoltage)
            # add system Temp to the log list
            self.logger.logAddValue('systemTemp', systemTemp)
            # add batteryLevel to the log list
            self.logger.logAddValue('batteryLevel', batteryLevel)
            # add averagePower to the log list
            self.logger.logAddValue('averagePower', averagePower)
            # add peakPower to the log list
            self.logger.logAddValue('peakPower', peakPower)
            # add Distances to the log list
            self.logger.logAddValue('DeviceStatus', DeviceStatus)
            self.logger.logAddValue('TPSMissed', TPSMissed)
            self.logger.logAddValue('positionX', self.position['X'])
            self.logger.logAddValue('positionY', self.position['Y'])
            self.logger.logAddValue('positionZ', self.position['Z'])

        # add the time entry
            tm_obj = localtime(time())
            datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
            self.logger.logAddValue('timetaken', datatime)

            if self.logger.logResults() :
                eprint("ERROR: Couldn't log values for Client {}.".format(sClientId))
                break

    def main(self, tstList) :

        # none specified so find one
        argv = ["client_list"]
        v = self.tx.sendGetTransmitter(argv)

        clientAliasList = makeClientAliasList(self.options)
        clientError, rtndict = jcs.processClientList(v, clientAliasList, True)

        clientList = list()
        noErrorCondition = True
        if not clientError :
            # there was an Error
            for cid, stat in rtndict.iteritems() :
                noErrorCondition = False
                eprint("ERROR: Client id {} unavailable with linkQuality {} and status {} --- {}".format(cid, stat[1], stat[0],
                                                                                              jcs.client_stat_dict.get(stat[0], stat[0])))
        else :
            # No error
            for cid, stat in rtndict.iteritems() :
                clientList.append([cid, stat[2]])
                eprint("INFO: Continuing with Client ID {} Alias '{}' status {} {}".format(cid, stat[2], stat[0],
                                                                      jcs.client_stat_dict.get(stat[0], stat[0])))
            # if one or more clients found then choose the first three with the highes LinkQ values greater than the minimum

        if noErrorCondition :
            # first register the client

            nardaSetup = self.narda.getSetupString()
            nardaDeviceInfo = self.narda.getDeviceInfoString()

            nardaRtn =  self.narda.setupNardaProbe()

            if not nardaRtn :
                eprint("ERROR: Narda probe failed setup routine -- exiting\n")
                return False

            eprint("INFO: The Client list: {}".format(clientList))

            for client, alias in clientList :
                try :
                    sClientId = str(client)
                    argv = ['register_client', sClientId]
                    v = self.tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v) :
                        eprint("ERROR: Register client {} failed".format(sClientId))
                        return
                    # configure the client
                    argv = ['client_config', sClientId, '0x6']
                    v = self.tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v) :
                        eprint("ERROR: Client config failed")
                        return
                    sleep(2)
                    # check for a non zero Short ID
                    argv = ['client_detail', sClientId]
                    v = self.tx.sendGetTransmitter(argv)
                    shortID = jcs.getClientShortID(v)
                    if shortID == "0x0000" :
                        eprint("ERROR: Client {} ({}) shows Short ID of 0x0000 : is not registered. Removing from list.".format(client, alias))
                        clientList.remove([client,alias])
                    else :
                        argv = ['start_charging', sClientId]
                        v = self.tx.sendGetTransmitter(argv)
                except ValueError as ve :
                    eprint("ERROR: ClientId ValueError {} -- {}".format(client, ve))
                    return False
            cota_config = "ND"
            try :
                cota_config, _ = self.getCotaConfigValues()
            except Exception as ex :
                eprint("ERROR: getCotaConfigValues exception -- {}".format(repr(ex)))

            self.logger.initFile(TXCMS)
    
            if self.logtodb :
                self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
                self.logger.initDBTable(self.options.tblname)
            # add options Dict to log list
            val =  makeStringFromDict(self.options.__dict__)
            self.logger.logAddValue("options", val)

            # add the Narda setup parm string
            self.logger.logAddValue("NardProbeSetup", nardaSetup)
            self.logger.logAddValue("NardDeviceInfo", nardaDeviceInfo)

            argv = ["get_charger_id"]
            v = self.tx.sendGetTransmitter(argv)
            txId = jcs.getTxId(v)

            # add txId to log list
            self.logger.logAddValue("txId", txId)
            self.logger.logAddValue("AntennaType",self.options.antennatype)
            self.logger.logAddValue("CfgComment",self.options.logcomments)
            self.logger.logAddValue("CotaConf",cota_config)
            # add OnChannels to log list
            OnChannels, _ = self.getOnChannels()
            verstr = self.getVersions()
            self.logger.logAddValue("OnChannels", OnChannels)
            v = self.tx.sendGetTransmitter(['get_good_channels'])
            self.logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
            self.logger.logAddValue("Versions", str(verstr))
            self.logger.logAddValue("LogFileName", self.logger.filename)
    
            tableTS = str(int(self.logger.timeStampNum))
            clientTableName = "client_{}".format(tableTS)
            self.logger.logAddValue(TXDATACLIENTTABLENAME, clientTableName)
            self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
            if self.logger.logResults() :
                eprint("ERROR: Couldn't log values for Transmitter.")
                return

            clientcms = CLIENTCMS
            # setup the logging file

            self.logger.initFile(clientcms, how='a')
            if self.logtodb :
                self.logger.initDBTable(clientTableName)
            # the client table is the last one inited so it will be the one where data is logged
            # it's data names will be expected to be used.

            warmuptime = self.options.__dict__.get("txwarmup", None)
            if warmuptime is not None :
                try :
                    val = float(warmuptime)
                except ValueError as ve :
                    eprint("ERROR: Warmup time failure '{}' -- {}".format(warmuptime, repr(ve)))
                else :
                    sleep(val)
            # we're setup and ready to go.
            for testlist  in tstList :

                try :
                    self.collectData(clientList, testlist)
                except KeyboardInterrupt as ki :
                    eprint("ALERT: Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                    break

            self.stopStartChargingList("stop", clientList)

if __name__ == '__main__' :

    START_TIME = time()
    tm = printTime("INFO: Started Program Execution at :", noprnt=True)
    eprint(tm)
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None,       help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg",      help="Use the named config dict for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None,       help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlist", help="Use the named list from the file.")

    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",         help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",         help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=50000,      help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=None,       help="Transmitter network address.")
    parser.add_option("","--txwarmup",     dest='txtxwarmup',   type=float,action='store',default=0.0,        help="Transmitter Warmup Time after start charging clients")

    parser.add_option("","--actip",        dest='actuator',     type=str,  action='store',default='',         help="Linear Actuator network address(es) (comma sep).")
    parser.add_option("","--sshport",      dest='sshport',      type=str,  action='store',default=2222,       help="SSH Port Router 2222 or no router 22.")

    parser.add_option("-c","--clientid",   dest='clientid',     type=str,  action='store',default=None,       help="CSL of client ID to use for characterization.")
    parser.add_option("","--cidaliases",   dest='cidaliases',   type=str,  action='store',default="",         help="CSL of Aliases in same order as clientIds to use for characterization.")

    parser.add_option("","--console",      dest='console',                 action='store_true',default=False, help="Turn off printing to console")
    parser.add_option("","--nologtodb",    dest='logtodb',                 action='store_false',default=True, help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=None,       help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=None,       help="Set the DataBase name at ossia-build")
    parser.add_option("","--emaillist",    dest='emaillist',    type=str,  action='store',default="",         help="Add email addressees.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False, help="print debug info.")

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="ExperLic_{}.csv",                        help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/home/ossiadev/Test/ExperLicTest/Logs/", help="Log file directory.")

    (opts, args) = parser.parse_args()

    import imp
    options = None
    hrs = mins = secs = -1 
    if len(args) > 0 :
        eprint("ERROR: Extra command line args, exiting {}".format(repr(args)))
    elif opts.cfgfile is not None :
        try :
            options = parseConfigFile(parser, opts)
        except RuntimeError as re :
            exit(1)
        except Exception as e :
            eprint("ERROR: Unknown exception -- {}\n".format(repr(e)))
            exit(2)

        eprint("INFO: Experimental License test of host {} and acuator list {} : Comments '{}'".format(options.txipaddr, options.actuator, options.logcomments))
        try :
            tstlstmod = imp.load_source(options.testlistname, options.testlistfile)
            testlist = eval("tstlstmod."+options.testlistname)
        except Exception as ee :
            eprint("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, options.testlistfile, repr(ee)))
        else :

            eprint("INFO: Executing main\n")

            EL = ExperLicense(options)

            EL.main(testlist)

            days, hrs, mins, secs, Duration = DurationCalc(START_TIME, True)
            eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
            try :
                EL.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                eprint("ERROR: Logging the Duration failed {}\n".format(repr(e)))

    eprint("")
    if options is not None :
        emailer.setToAddress(options.emaillist)
    tm = printTime("INFO: Ended Program Execution at :", noprnt=True)
    eprint(tm)
    emailer.emailResults()

