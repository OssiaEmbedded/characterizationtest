#! /usr/bin/python

import socket
from time import sleep, gmtime, strftime
from optparse import OptionParser

def sendgetdata(s, sendthis):
    s.send( sendthis )
    sleep(0.5)
    try :
        data= s.recv(2048)
    except socket.timeout :
        pass
    s.send( "\r\n" )
    #sleep(0.5)
    data = bytearray()
    d = bytearray()
    try :
        d = bytearray(s.recv(8192))
        while len(d) > 0 :
            data += d
            d = s.recv(8192)
    except socket.timeout :
        pass
    #print data
    #data = data.lstrip()
    #l = data.split('SCPI>')
    #rtndat = l[0].rstrip()

    return data
 
parser = OptionParser()

parser.add_option("-f","--filename",
                   dest="filename",
                   action="store",
                   default=None,
                   help="file name to save screen shots (default='%default')")

parser.add_option("-t","--tstequip",
                   dest="tstequip",
                   action="store",
                   default="A-N9020A-11404.ossiainc.local",
                   help="Network name of test equipment (default='%default')")

parser.add_option("-p","--port",
                   dest="portnumber",
                   type=int,
                   action="store",
                   default="5025",
                   help="Port number of the Test eqpipment telnet server (default='%default')")

parser.add_option("-s","--strtosend",
                   dest="strtosend",
                   action="store",
                   default=None,
                   help="SPCI Remote command string (default='%default')")

(options, args) = parser.parse_args()

mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
#err = mySocket.connect_ex((options.clthname, options.portnumber))
#print "IP {} port {}".format(options.tstequip, options.portnumber)

try :

    mySocket.connect((options.tstequip, options.portnumber))
    mySocket.settimeout(1.0)
except :
    print "There was a connect exception "
    exit(1)

# throw away the welcome message
sleep(0.15)
try :
    data= mySocket.recv(2048)
except socket.timeout :
    pass

if options.strtosend is None :

    # get the Agilent ID need to check for correctness
    datatosend="*IDN?"
    theid=sendgetdata(mySocket, datatosend)
    print "{}\n".format(str(theid))

    # get the default director
    datatosend=":MMEMory:CDIRectory?"
    thedir=sendgetdata(mySocket, datatosend)

    # the state filename to the directory and save the state
    y=thedir.split('"')[1]
    z='"'+y+"\\uwmtest.png"+'"'
    datatosend=':MMEM:STOR:SCR '+z
    sendgetdata(mySocket, datatosend)

    # now get the saved state data
    datatosend=':MMEM:DATA? '+z
    data = bytearray()
    data=sendgetdata(mySocket, datatosend)
    #print ""
    #print len(data)
    fd = open("./thesrc.png", "wb")
    fd.write(data)
    fd.close()
    
else :
    sDat = options.strtosend
    for i in range(len(args)) :
         sDat += " "+args[i]
    fsd = open("./.sendAuto.log",'a')
    fsd.write(sDat+"\n")
    fsd.close()
    data = sendgetdata(mySocket, sDat)
    if options.filename is not None :
        print repr(str(data[:26]))
        fp = open(options.filename, "wb")
        fp.write(data)
        fp.close()
    else :
        print str(data)

mySocket.close()
#sleep(7)
