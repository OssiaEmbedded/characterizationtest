E INFO: Starting a Characterization test on host orion9.ossiainc.local using the specified client(s) 0x124b0007a17283 on an actuator named ossiabbb6.ossiainc.local

E INFO: Executing main

talkTo -- line = 'server_start_state' argv = '[u'server_start_state']'
cmd json = 'None'
rtnstr = 'Unknown Command u'server_start_state''
E INFO: The Client list: [[u'0x124b0007a17283', u'85']]
talkTo -- line = 'register_rx 0x124b0007a17283' argv = '[u'register_rx', u'0x124b0007a17283']'
cmd json = '{'numofargs': [1, 2], 'NotSet': ['', '5'], 'help': ['[RX ID [QueryType 5 or 6]]', 'Register a new receiver with the system that has been discovered.', 1], 'jstr': '{"Command":{"Type":"RegisterRx","RX ID":"%s","QueryType":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283', '5']'
talkTo: jstrcmd={"Command":{"Type":"RegisterRx","RX ID":"0x124b0007a17283","QueryType":"5"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "RegisterRx", "RX ID": "0x124b0007a17283", "QueryType": "5"}, "Result": {"Status": "ERROR: COTA_ERROR_RCVR_ALREADY_REGISTERED -- \'\'"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'RegisterRx', u'QueryType': u'5'}, u'Result': {u'Status': u"ERROR: COTA_ERROR_RCVR_ALREADY_REGISTERED -- ''"}}

ARGUMENTS [u'0x124b0007a17283', '5']

rtnstr = 'ERROR: COTA_ERROR_RCVR_ALREADY_REGISTERED -- '''
talkTo -- line = 'rx_config 0x124b0007a17283 0x6' argv = '[u'rx_config', u'0x124b0007a17283', u'0x6']'
cmd json = '{'numofargs': 2, 'help': ['[RX ID] [QueryType(5=STD  6=EXT)]', 'Set configuration for a specific receiver.', 2], 'jstr': '{"Command":{"Type":"SetRxConfig","RX ID":"%s","QueryType":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283', u'0x6']'
talkTo: jstrcmd={"Command":{"Type":"SetRxConfig","RX ID":"0x124b0007a17283","QueryType":"0x6"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
NOT readable cnt=3789
NOT readable cnt=3788
NOT readable cnt=3787
readable cnt=3786

JSON u'{"Command": {"Type": "SetRxConfig", "RX ID": "0x124b0007a17283", "QueryType": "0x6"}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'SetRxConfig', u'QueryType': u'0x6'}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS [u'0x124b0007a17283', u'0x6']

rtnstr = 'SUCCESS'
talkTo -- line = 'rx_detail 0x124b0007a17283' argv = '[u'rx_detail', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[RX ID]', 'Get detailed information for a specific receiver.', 1], 'jstr': '{"Command":{"Type":"GetRxDetail","RX ID":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"GetRxDetail","RX ID":"0x124b0007a17283"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "GetRxDetail", "RX ID": "0x124b0007a17283"}, "Result": {"Status": "SUCCESS", "RX ID": "0x124b0007a17283", "Short ID": "0x1", "Version": "0.16.0-000000f8", "Model": "0xB0004", "LinkQuality": 13, "Comm RSSI": -82, "QueryType": "RCVR_QUERY_CUSTOM", "Peak Power": -1, "Avg Power": -1, "Net Current": -6, "State of Charge": 58, "Status Flags": {"powerRq": 1, "ReceiverRxCharge": 0, "targetRxCharge": 0, "errorStatus": 0, "missedTpsCycles": 0}, "Battery Voltage": 3816, "AvgRFPower1": 0, "AvgRFPower2": 0, "AvgRFPower3": 0, "AvgRFPower4": 0, "AvgRFPower1 mDbm": 0, "AvgRFPower2 mDbm": 0, "AvgRFPower3 mDbm": 0, "AvgRFPower4 mDbm": 0, "MaxRFPower1": 0, "MaxRFPower2": 0, "MaxRFPower3": 0, "MaxRFPower4": 0, "MaxRFPower1 mDbm": 0, "MaxRFPower2 mDbm": 0, "MaxRFPower3 mDbm": 0, "MaxRFPower4 mDbm": 0, "Last Error": "2 ERR_SRC_MISSING_TPS TPS message was never received for the sequence number in the last Go message", "State": "RCVR_STATE_READY", "Priority": 0, "Charging Desired": 0, "Not Queried Count": 0, "Query Needed": 1, "Comm Failed Count": 0, "AppCmd Tries left": 0, "Query Time": "27:23:23"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'GetRxDetail'}, u'Result': {u'LinkQuality': 13, u'Avg Power': -1, u'AvgRFPower4 mDbm': 0, u'Battery Voltage': 3816, u'MaxRFPower3 mDbm': 0, u'AppCmd Tries left': 0, u'State': u'RCVR_STATE_READY', u'Version': u'0.16.0-000000f8', u'Peak Power': -1, u'AvgRFPower2 mDbm': 0, u'Charging Desired': 0, u'Status': u'SUCCESS', u'RX ID': u'0x124b0007a17283', u'MaxRFPower2 mDbm': 0, u'Not Queried Count': 0, u'MaxRFPower4 mDbm': 0, u'Net Current': -6, u'Query Time': u'27:23:23', u'Status Flags': {u'missedTpsCycles': 0, u'targetRxCharge': 0, u'errorStatus': 0, u'ReceiverRxCharge': 0, u'powerRq': 1}, u'AvgRFPower2': 0, u'AvgRFPower3': 0, u'AvgRFPower1': 0, u'MaxRFPower1': 0, u'AvgRFPower4': 0, u'MaxRFPower3': 0, u'Comm Failed Count': 0, u'Comm RSSI': -82, u'AvgRFPower1 mDbm': 0, u'Short ID': u'0x1', u'Query Needed': 1, u'Model': u'0xB0004', u'QueryType': u'RCVR_QUERY_CUSTOM', u'Priority': 0, u'MaxRFPower2': 0, u'AvgRFPower3 mDbm': 0, u'MaxRFPower4': 0, u'Last Error': u'2 ERR_SRC_MISSING_TPS TPS message was never received for the sequence number in the last Go message', u'MaxRFPower1 mDbm': 0, u'State of Charge': 58}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
talkTo -- line = 'app_command 0x124b0007a17283 17' argv = '[u'app_command', u'0x124b0007a17283', u'17']'
cmd json = '{'numofargs': [2, 3], 'NotSet': ['0', '0', '[]'], 'help': ['[RX ID] [Data]', 'Send a generic command to a Receiver (or all Receiver).', 2], 'jstr': '{"Command":{"Type":"AppCommand","RX ID":"%s","Msg Num":"%s","Data":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283', u'17', '[]']'
talkTo: jstrcmd={"Command":{"Type":"AppCommand","RX ID":"0x124b0007a17283","Msg Num":"17","Data":"[]"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "AppCommand", "RX ID": "0x124b0007a17283", "Msg Num": "17", "Data": "[]"}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Msg Num': u'17', u'Type': u'AppCommand', u'Data': u'[]'}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS [u'0x124b0007a17283', u'17', '[]']

rtnstr = 'SUCCESS'
talkTo -- line = 'app_command_data 0x124b0007a17283' argv = '[u'app_command_data', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[RX ID]', 'Get Receiver command data.', 1], 'jstr': '{"Command":{"Type":"AppCommandData","RX ID":"%s"}, "Result" : {"Status":"NULL","Data":"[]"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"AppCommandData","RX ID":"0x124b0007a17283"}, "Result" : {"Status":"NULL","Data":"[]"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
readable cnt=3789

JSON u'{"Command": {"Type": "AppCommandData", "RX ID": "0x124b0007a17283"}, "Result": {"Status": "SUCCESS", "Values": [{"Type": "0x8b (139)"}, {"Data": "0x01 0x01 0x01 0x01 0x00 0x00 0x00 0x00  (1 1 1 1 0 0 0 0)"}, {"Uint16": "0x0101 0x0101 0x0000 0x0000  (257 257 0 0)"}, {"Uint32": "0x01010101 0x00000000 (16843009 0)"}, {"Uint64": "0x01010101  (16843009)"}]}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'AppCommandData'}, u'Result': {u'Status': u'SUCCESS', u'Values': [{u'Type': u'0x8b (139)'}, {u'Data': u'0x01 0x01 0x01 0x01 0x00 0x00 0x00 0x00  (1 1 1 1 0 0 0 0)'}, {u'Uint16': u'0x0101 0x0101 0x0000 0x0000  (257 257 0 0)'}, {u'Uint32': u'0x01010101 0x00000000 (16843009 0)'}, {u'Uint64': u'0x01010101  (16843009)'}]}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
IndexError in client command 17 data []
talkTo -- line = 'app_command 0x124b0007a17283 33' argv = '[u'app_command', u'0x124b0007a17283', u'33']'
cmd json = '{'numofargs': [2, 3], 'NotSet': ['0', '0', '[]'], 'help': ['[RX ID] [Data]', 'Send a generic command to a Receiver (or all Receiver).', 2], 'jstr': '{"Command":{"Type":"AppCommand","RX ID":"%s","Msg Num":"%s","Data":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283', u'33', '[]']'
talkTo: jstrcmd={"Command":{"Type":"AppCommand","RX ID":"0x124b0007a17283","Msg Num":"33","Data":"[]"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
readable cnt=3792

JSON u'{"Command": {"Type": "AppCommand", "RX ID": "0x124b0007a17283", "Msg Num": "33", "Data": "[]"}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Msg Num': u'33', u'Type': u'AppCommand', u'Data': u'[]'}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS [u'0x124b0007a17283', u'33', '[]']

rtnstr = 'SUCCESS'
talkTo -- line = 'app_command_data 0x124b0007a17283' argv = '[u'app_command_data', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[RX ID]', 'Get Receiver command data.', 1], 'jstr': '{"Command":{"Type":"AppCommandData","RX ID":"%s"}, "Result" : {"Status":"NULL","Data":"[]"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"AppCommandData","RX ID":"0x124b0007a17283"}, "Result" : {"Status":"NULL","Data":"[]"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
readable cnt=3790

JSON u'{"Command": {"Type": "AppCommandData", "RX ID": "0x124b0007a17283"}, "Result": {"Status": "SUCCESS", "Values": [{"Type": "0x95 (149)"}, {"Data": "0x04 0x00 0x00 0x00 0x00 0x00 0x00 0x00  (4 0 0 0 0 0 0 0)"}, {"Uint16": "0x0004 0x0000 0x0000 0x0000  (4 0 0 0)"}, {"Uint32": "0x00000004 0x00000000 (4 0)"}, {"Uint64": "0x00000004  (4)"}]}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'AppCommandData'}, u'Result': {u'Status': u'SUCCESS', u'Values': [{u'Type': u'0x95 (149)'}, {u'Data': u'0x04 0x00 0x00 0x00 0x00 0x00 0x00 0x00  (4 0 0 0 0 0 0 0)'}, {u'Uint16': u'0x0004 0x0000 0x0000 0x0000  (4 0 0 0)'}, {u'Uint32': u'0x00000004 0x00000000 (4 0)'}, {u'Uint64': u'0x00000004  (4)'}]}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
IndexError in client command 33 data []
talkTo -- line = 'app_command 0x124b0007a17283 61' argv = '[u'app_command', u'0x124b0007a17283', u'61']'
cmd json = '{'numofargs': [2, 3], 'NotSet': ['0', '0', '[]'], 'help': ['[RX ID] [Data]', 'Send a generic command to a Receiver (or all Receiver).', 2], 'jstr': '{"Command":{"Type":"AppCommand","RX ID":"%s","Msg Num":"%s","Data":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283', u'61', '[]']'
talkTo: jstrcmd={"Command":{"Type":"AppCommand","RX ID":"0x124b0007a17283","Msg Num":"61","Data":"[]"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
readable cnt=3789

JSON u'{"Command": {"Type": "AppCommand", "RX ID": "0x124b0007a17283", "Msg Num": "61", "Data": "[]"}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Msg Num': u'61', u'Type': u'AppCommand', u'Data': u'[]'}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS [u'0x124b0007a17283', u'61', '[]']

rtnstr = 'SUCCESS'
talkTo -- line = 'app_command_data 0x124b0007a17283' argv = '[u'app_command_data', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[RX ID]', 'Get Receiver command data.', 1], 'jstr': '{"Command":{"Type":"AppCommandData","RX ID":"%s"}, "Result" : {"Status":"NULL","Data":"[]"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"AppCommandData","RX ID":"0x124b0007a17283"}, "Result" : {"Status":"NULL","Data":"[]"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
NOT readable cnt=3789
readable cnt=3788

JSON u'{"Command": {"Type": "AppCommandData", "RX ID": "0x124b0007a17283"}, "Result": {"Status": "SUCCESS", "Values": [{"Type": "0xa7 (167)"}, {"Data": "0xc5 0x52 0xa0 0x02 0x00 0x00 0x00 0x00  (197 82 160 2 0 0 0 0)"}, {"Uint16": "0x52c5 0x02a0 0x0000 0x0000  (21189 672 0 0)"}, {"Uint32": "0x02a052c5 0x00000000 (44061381 0)"}, {"Uint64": "0x02a052c5  (44061381)"}]}}'

PY {u'Command': {u'RX ID': u'0x124b0007a17283', u'Type': u'AppCommandData'}, u'Result': {u'Status': u'SUCCESS', u'Values': [{u'Type': u'0xa7 (167)'}, {u'Data': u'0xc5 0x52 0xa0 0x02 0x00 0x00 0x00 0x00  (197 82 160 2 0 0 0 0)'}, {u'Uint16': u'0x52c5 0x02a0 0x0000 0x0000  (21189 672 0 0)'}, {u'Uint32': u'0x02a052c5 0x00000000 (44061381 0)'}, {u'Uint64': u'0x02a052c5  (44061381)'}]}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
IndexError in client command 61 data []
E INFO: Client 0x124b0007a17283 -- PMUs -3 RECs -3 FW -0000003
talkTo -- line = 'versions' argv = '[u'versions']'
cmd json = '{'numofargs': 0, 'help': ['[]', 'Get Driver  FW OS and build versions.', 0], 'jstr': '{"Command":{"Type":"GetTransmitterVersions"}, "Result" : {"Status":"NULL"}}'}'
args='[]'
talkTo: jstrcmd={"Command":{"Type":"GetTransmitterVersions"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "GetTransmitterVersions"}, "Result": {"Status": "SUCCESS", "Code Version": "1.0.165", "Proxy Version": "1.0.18-f416070a", "Compile Date": "Tue Oct 19 18:35:40 2021", "Build Type": "DEMO", "MM Code Version": "0.1.22-32613849", "MM Git Date": "Wed Oct 20 00:25:33 2021 +0000", "MM Git Branch": "HEAD", "Host Git Commit": "5fb68eef", "MM IP Address": "10.10.0.73", "OS Version": {"OS name": "Linux", "Host name": "Orion9", "Kernel ver": "4.19.97-v7l+", "Build Date": "#1294 SMP Thu Jan 30 13:21:14 GMT 2020"}}}'

PY {u'Command': {u'Type': u'GetTransmitterVersions'}, u'Result': {u'Status': u'SUCCESS', u'Build Type': u'DEMO', u'MM Git Date': u'Wed Oct 20 00:25:33 2021 +0000', u'Code Version': u'1.0.165', u'MM IP Address': u'10.10.0.73', u'Proxy Version': u'1.0.18-f416070a', u'Host Git Commit': u'5fb68eef', u'MM Code Version': u'0.1.22-32613849', u'Compile Date': u'Tue Oct 19 18:35:40 2021', u'MM Git Branch': u'HEAD', u'OS Version': {u'OS name': u'Linux', u'Kernel ver': u'4.19.97-v7l+', u'Build Date': u'#1294 SMP Thu Jan 30 13:21:14 GMT 2020', u'Host name': u'Orion9'}}}

ARGUMENTS []

rtnstr = 'SUCCESS'
talkTo -- line = 'get_valid_amb_mask' argv = '[u'get_valid_amb_mask']'
cmd json = '{'numofargs': 0, 'help': ['[]', 'Get the Valid AMB Mask setting.', 0], 'jstr': '{"Command":{"Type":"GetCfgParam","Param":"CFG_VALID_AMB_MASK"}, "Result" : {"Status":"NULL","Valid Ambs":"0x{:X}"}}'}'
args='[]'
talkTo: jstrcmd={"Command":{"Type":"GetCfgParam","Param":"CFG_VALID_AMB_MASK"}, "Result" : {"Status":"NULL","Valid Ambs":"0x{:X}"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "GetCfgParam", "Param": "CFG_VALID_AMB_MASK"}, "Result": {"Valid Ambs": "0xF", "Status": "SUCCESS"}}'

PY {u'Command': {u'Type': u'GetCfgParam', u'Param': u'CFG_VALID_AMB_MASK'}, u'Result': {u'Status': u'SUCCESS', u'Valid Ambs': u'0xF'}}

ARGUMENTS []

rtnstr = 'SUCCESS'
E INFO: DataBase: OrionChar21, Tablename: Nov2021, Test Timestamp: Wed_Nov_24_2021_14_27_02
E INFO: Charger name: orion9.ossiainc.local, Antenna Type: , Comments: ''
E INFO: System versions
Code Version : 1.0.165
Build Type : DEMO
MM Code Version : 0.1.22-32613849
Proxy Version : 1.0.18-f416070a
E INFO: Cota Config File:
00_00_22 Distance1 0 > 1000 finished False

        txAngle 0.0 > 0.0 Distance1 0 > 1000 finished False

talkTo -- line = 'set_power_level 20' argv = '[u'set_power_level', u'20']'
cmd json = '{'numofargs': 1, 'help': ['[PowerLevel]', 'Set the system power level in dBm. 13, 16, 19, 19.5, 20', 1], 'jstr': '{"Command":{"Type":"SetPowerLevel","PowerLevel":"%s"}, "Result" : {"Status":"NULL"}}'}'
args='[u'20']'
talkTo: jstrcmd={"Command":{"Type":"SetPowerLevel","PowerLevel":"20"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
NOT readable cnt=3789
NOT readable cnt=3788
NOT readable cnt=3787
NOT readable cnt=3786
NOT readable cnt=3785
NOT readable cnt=3784
NOT readable cnt=3783
NOT readable cnt=3782
readable cnt=3781

JSON u'{"Command": {"Type": "SetPowerLevel", "PowerLevel": "20"}, "Result": {"Status": "SUCCESS", "Actual PwrLvl": 20.0}}'

PY {u'Command': {u'PowerLevel': u'20', u'Type': u'SetPowerLevel'}, u'Result': {u'Status': u'SUCCESS', u'Actual PwrLvl': 20.0}}

ARGUMENTS [u'20']

rtnstr = 'SUCCESS'
talkTo -- line = 'get_power_level' argv = '[u'get_power_level']'
cmd json = '{'numofargs': 0, 'help': ['[]', 'Get current system power level in dBm.', 0], 'jstr': '{"Command": {"Type":"GetPowerLevel"}, "Result" : {"Status":"NULL"}}'}'
args='[]'
talkTo: jstrcmd={"Command": {"Type":"GetPowerLevel"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "GetPowerLevel"}, "Result": {"Status": "SUCCESS", "PowerLevel": 20.0}}'

PY {u'Command': {u'Type': u'GetPowerLevel'}, u'Result': {u'Status': u'SUCCESS', u'PowerLevel': 20.0}}

ARGUMENTS []

rtnstr = 'SUCCESS'
zDistance1 = 1000


        Z1 real vs. set: abs(1000 - 1000) = 0

talkTo -- line = 'start_charging 0x124b0007a17283' argv = '[u'start_charging', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[long id | all]', 'Changes a ready device to the charging state, making it eligible for TPS.', 1], 'jstr': '{"Command":{"Type":"StartChargingDevices","Devices":["%s"],"SWITCH":"START_CHARGE"}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"StartChargingDevices","Devices":["0x124b0007a17283"],"SWITCH":"START_CHARGE"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
NOT readable cnt=3789
NOT readable cnt=3788
NOT readable cnt=3787
NOT readable cnt=3786
NOT readable cnt=3785
NOT readable cnt=3784
NOT readable cnt=3783
readable cnt=3782

JSON u'{"Command": {"Type": "StartChargingDevices", "Devices": ["0x124b0007a17283"], "SWITCH": "START_CHARGE"}, "Result": {"Status": "SUCCESS", "Receivers": [{"RX ID": "0x124b0007a17283", "Short ID": "0x1", "State": "RCVR_STATE_CHARGING", "Version": "0.16.0-000000f8", "State of Charge": "58", "Status Flags": "1", "Average Power": "-1", "Peak Power": "-1", "RSSI": "-82", "Link Quality": "13", "Net Current": "-6"}]}}'

PY {u'Command': {u'SWITCH': u'START_CHARGE', u'Type': u'StartChargingDevices', u'Devices': [u'0x124b0007a17283']}, u'Result': {u'Status': u'SUCCESS', u'Receivers': [{u'Net Current': u'-6', u'RX ID': u'0x124b0007a17283', u'Link Quality': u'13', u'Short ID': u'0x1', u'State': u'RCVR_STATE_CHARGING', u'Version': u'0.16.0-000000f8', u'Peak Power': u'-1', u'Status Flags': u'1', u'RSSI': u'-82', u'State of Charge': u'58', u'Average Power': u'-1'}]}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
E INFO: Warmup time 100 : Start at -- 14:27:29 finish at 14:29:09

talkTo -- line = 'calibrate' argv = '[u'calibrate']'
cmd json = '{'numofargs': 0, 'help': ['[]', "Causes the transmitter to go through it's calibration process.", 0], 'jstr': '{"Command":{"Type":"Calibrate"}, "Result" : {"Status":"NULL"}}'}'
args='[]'
talkTo: jstrcmd={"Command":{"Type":"Calibrate"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
readable cnt=3790

JSON u'{"Command": {"Type": "Calibrate"}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'Type': u'Calibrate'}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS []

rtnstr = 'SUCCESS'
talkTo -- line = 'get_system_temp' argv = '[u'get_system_temp']'
cmd json = '{'numofargs': 0, 'help': ['[]', 'Get the current system and AMB/UVP temperatures.', 0], 'jstr': '{"Command":{"Type":"GetSystemTemp"}, "Result" : {"Status":"NULL"}}'}'
args='[]'
talkTo: jstrcmd={"Command":{"Type":"GetSystemTemp"}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
NOT readable cnt=3791
NOT readable cnt=3790
NOT readable cnt=3789
NOT readable cnt=3788
NOT readable cnt=3787
NOT readable cnt=3786
readable cnt=3785

JSON u'{"Command": {"Type": "GetSystemTemp"}, "Result": {"Status": "SUCCESS", "CCB0": "30.12 degs C", "CCB1": "28.56 degs C", "CCB2": "28.87 degs C", "AMB0": "34.07 degs C", "AMB1": "32.69 degs C", "AMB2": "35.54 degs C", "AMB3": "32.4 degs C"}}'

PY {u'Command': {u'Type': u'GetSystemTemp'}, u'Result': {u'Status': u'SUCCESS', u'CCB0': u'30.12 degs C', u'CCB2': u'28.87 degs C', u'CCB1': u'28.56 degs C', u'AMB0': u'34.07 degs C', u'AMB1': u'32.69 degs C', u'AMB2': u'35.54 degs C', u'AMB3': u'32.4 degs C'}}

ARGUMENTS []

rtnstr = 'SUCCESS'
E ERROR: Unexpected Exception exiting --- ValueError('invalid literal for float(): 30.12 degs C',) -- {'calthistime': True, 'warmup': 100, 'txangle': 0.0, 'zdistance1': 1000, 'waitfordata': 5, 'powerlevel': 20, 'logcount': 5}

E ERROR: Traceback -- Traceback (most recent call last):
  File "PwrCharTestOrion.py", line 927, in main
    finished, peakPower, batteryLevel = runthecharacter(sa, tx, clientList, itTstDict, logger, options, comChanNum)
  File "PwrCharTestOrion.py", line 501, in runthecharacter
    systemTemp = jcs.getSystemTemp(v)
  File "/home/ossiadev/pylib/Orion/OrionTX.py", line 358, in getSystemTemp
    temp += float(val)
ValueError: invalid literal for float(): 30.12 degs C
 

E ERROR: Sys Exception info -- (<type 'exceptions.ValueError'>, ValueError('invalid literal for float(): 30.12 degs C',), <traceback object at 0x7fbbde637ef0>)

At End Stop charging client 0x124b0007a17283 argv = [u'stop_charging', '0x124b0007a17283']

talkTo -- line = 'stop_charging 0x124b0007a17283' argv = '[u'stop_charging', u'0x124b0007a17283']'
cmd json = '{'numofargs': 1, 'help': ['[long id | all]', 'Changes a charging device to the ready state.', 1], 'jstr': '{"Command":{"Type":"StopChargingDevices","Devices":["%s"]}, "Result" : {"Status":"NULL"}}'}'
args='[u'0x124b0007a17283']'
talkTo: jstrcmd={"Command":{"Type":"StopChargingDevices","Devices":["0x124b0007a17283"]}, "Result" : {"Status":"NULL"}}
NOT readable cnt=3799
NOT readable cnt=3798
NOT readable cnt=3797
NOT readable cnt=3796
NOT readable cnt=3795
NOT readable cnt=3794
NOT readable cnt=3793
NOT readable cnt=3792
readable cnt=3791

JSON u'{"Command": {"Type": "StopChargingDevices", "Devices": ["0x124b0007a17283"]}, "Result": {"Status": "SUCCESS"}}'

PY {u'Command': {u'Type': u'StopChargingDevices', u'Devices': [u'0x124b0007a17283']}, u'Result': {u'Status': u'SUCCESS'}}

ARGUMENTS [u'0x124b0007a17283']

rtnstr = 'SUCCESS'
E INFO: Test duration = 0 hrs 2 mins 13 secs

