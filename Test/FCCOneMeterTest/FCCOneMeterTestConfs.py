QUERY_PERIOD = 5
cfgOrion1M_testday={
'setupcfgparams'   : [{'set_cfg_param' : [['CFG_AUTO_CALIBRATION_PERIOD', 0], ['CFG_SHORT_CALIBRATION_DURATION',1760], ['CFG_SHORT_CALIBRATION_PERIOD', 20], ['CFG_QUERY_PERIOD', QUERY_PERIOD]]},],
'setupqueryperiod' : QUERY_PERIOD,
'setupcommands'    : None,
'stopcharging'     : False,
'calibrate'        : '0',
'console'          : 'False',
'txipaddr'         : '10.10.1.55',
'txport'           : '50000',
'actuator'         : '10.10.1.53',
'maxrssi'          : '32.0',
'saipaddr'         : '10.10.1.63',
'saport'           : '5025',
'average'          : 'False',
'logfile'          : 'Orion_B9_FCC1m_Testday_{}.csv',
'logtodb'          : 'True',
'directory'        : '/home/ossiadev/Test/FCCOneMeterTest/Logs/',
'poscalibrate'     : False,
'calibratealg'     : True,
'dbname'           : "Orion1Meter",
'tblname'          : "ULFCCTestDay",
'testlistfile'     : 'FCCOneMeterTestLists.py',
}

cfgOrion1M_submittal={
'setupcfgparams'   : [{'set_cfg_param' : [['CFG_AUTO_CALIBRATION_PERIOD', 0], ['CFG_SHORT_CALIBRATION_DURATION',1190], ['CFG_SHORT_CALIBRATION_PERIOD', 20], ['CFG_QUERY_PERIOD', QUERY_PERIOD]]},],
'setupqueryperiod' : QUERY_PERIOD,
'setupcommands'    : None,
'stopcharging'     : False,
'calibrate'        : '0',
'console'          : 'False',
'txipaddr'         : '10.10.1.55',
'txport'           : '50000',
'actuator'         : '10.10.1.53',
'maxrssi'          : '32.0',
'saipaddr'         : '10.10.1.63',
'saport'           : '5025',
'average'          : 'False',
'logfile'          : 'Orion_B9_FCC1m_Submittal_{}.csv',
'logtodb'          : 'True',
'directory'        : '/home/ossiadev/Test/FCCOneMeterTest/Logs/',
'poscalibrate'     : False,
'calibratealg'     : True,
'dbname'           : "Orion1Meter",
'tblname'          : "ULFCCSubmittal",
'testlistfile'     : 'FCCOneMeterTestLists.py',
}

cfgOrion1M_auto_cal={
'setupcfgparams' : [{'set_cfg_param'      : [['CFG_AUTO_CALIBRATION_PERIOD', 0], ['CFG_SHORT_CALIBRATION_DURATION',1190], ['CFG_SHORT_CALIBRATION_PERIOD', 20], ['CFG_QUERY_PERIOD', QUERY_PERIOD]]},],
'setupqueryperiod' : QUERY_PERIOD,
'setupcommands'  : None,
'stopcharging' : False,
'calibrate' : '0',
'console' : 'False',
'txipaddr' : '10.10.1.55',
'txport' : '50000',
'actuator' : '10.10.1.53',
'maxrssi' : '32.0',
'saipaddr' : '10.10.1.63',
'saport' : '5025',
'average' : 'False',
'logfile' : 'Orion_B9_FCC1m_TC_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/FCCOneMeterTest/Logs/',
'poscalibrate' : False,
'calibratealg' : True,
'dbname' : "Orion1Meter",
'tblname' : "BackForthTest",
'testlistfile' : 'FCCOneMeterTestLists.py',
}

cfgVenusFCC1m_TC={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : '10.10.1.42',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
'saipaddr' : '10.10.1.51',
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B9_FCC1m_TC_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : False,
'logclientdistance' : False,
'cldistdebug' : False,
#'debug' : True,
}
cfgVenusFCC1mCalOnceLogNOSA={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : '10.10.1.42',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
#'saipaddr' : '10.10.1.51',
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B9_FCC1m_TC6{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : False,
'logclientdistance' : False,
'cldistdebug' : False,
#'debug' : True,
}
cfgVenusFCC1mCalOnceNOSA={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : 'ossiabbb2',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
'saipaddr' : None,
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B4_FCC1m_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : False,
'caljustonce' : True,
#'debug' : True,
}

cfgVenusFCC1mCalOnce={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : '10.10.1.46',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
'saipaddr' : '10.10.1.51',
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B4_FCC1m_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : False,
'caljustonce' : True,
#'debug' : True,
}
cfgVenusFCC1mCalOnceAlgNOSA={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : 'ossiabbb2',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
'saipaddr' : None,
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B4_FCC1m_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : True,
'caljustonce' : True,
#'debug' : True,
}

cfgVenusFCC1mCalOnceAlg={
'calibrate' : '0',
'console' : 'False',
'txport' : '50000',
'actuator' : '10.10.1.46',
'maxrssi' : '32.0',
#'saipaddr' : 'A-N9020A-11404.ossiainc.local',
'saipaddr' : '10.10.1.51',
'saport' : '5025',
'average' : 'False',
'logfile' : 'Venus_B4_FCC1m_{}.csv',
'logtodb' : 'True',
'directory' : '/home/ossiadev/Test/PwrCharMTestFCC/Logs/',
'poscalibrate' : False,
'calibratealg' : True,
'caljustonce' : True,
#'debug' : True,
}
