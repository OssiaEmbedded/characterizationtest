This is a brief description of the steps and commands necessary to execute the One Meter test plan for Orion.

1) All tests will be executed on a command line using an ssh connection to ossia-build (10.10.0.216 static ip)
ssh ossiadev@10.10.0.216 PW OssiaDev!

2) Type the alias "fcc" at the command line. This will take you to the directory where the python test scripts
are located. As of this writing (7/23/2020) the svn Commit revision is 502.

3) The FCC client with ID 0x124B0007BA0236 should be used for this test. If this client is unavailable then 
an identical client with FW git hash 018F0E53 can be used.

4) The Host MCU software must be built from the Orion repo with commit hash 1fd8bb7. The RPI must contain
mmServer.py and CLI.py updated from a jcdCliServerCmds.py file from the git commit ab40867704e3964dab245e16badedeb065c741d2
A hostMcuEnumDefs.py file must be generated from the Orion repo using the above commit hash.
These three files must be placed on the RPI ~pi/ directory and the RPi restarted.

5) The Tile must be mounted in the Anechoic chamber upon a table of suitable height. Tape the tile stands to the table securely.
Wired Ethernet and power must be available. The tile must be directly facing and perpendicular to the linear actuator.
The rotary actuator must be at the Zero degree sensor (a green LED lights on the controller when a magnet is over the zero degree sensor)

6) The FCC client must be securely mounted on a pole attached to the linear actuator where the client's center is lined up
on the horizontal and vertical axises with the center of the tile. (roughly)

7) Type or copy and paste (C/P) the following command lines:
sendOssia 10.10.1.53 setPosition 0 mc1 7000
sendOssia 10.10.1.53 setPosition 0 mc2 7000

8) Measure the distance in mm from the face of the tile to the face of the client. Record the number as <Value>.

9) Type or C/P the following command line:
sendOssia 10.10.1.53 setLinearOffset <Value> mc1 7000

10) Open a CLI session to the Message Manager on the tile. This can be done from the OrionPi repo using the command
python3 CLI.py -i <tile ip address> 
or on the RPi by opening an ssh to pi@<tile ip address> PW raspberry. At the command prompt type in CLI.

11) Using the CLI type reset_host. Go to the client and using an appropriate tool physically press the client's reset button.
Wait for the tile to discover the client and enter the ready state (light ring green with rotating red)

 ANGULAR LIMITS TEST PLAN STEP
12) Type or (C/P) the following command line:
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_testday -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_SingleDist_AngularPowerLimit --logcomments="UL FCC Submittal Angular Limit;log_angular_limit1.txt"  > log_angular_limit1.txt 2>&1 &
Replace the "-c" option value with the appropriate client ID number.
This test will take approximately 52 mins.

 REVERSE ANGULAR LIMITS TEST PLAN STEP
13) Turn the table with the tile securely attached to it, 180 degrees. Insure the power and Ethernet cables are free and clear and
arranged such that the rotary actuator has full range of movement from 90 to -90 degs.

14) Type or copy and paste (C/P) the following command lines:
sendOssia 10.10.1.53 setPosition 0 mc1 7000
sendOssia 10.10.1.53 setPosition 0 mc2 7000

15) Measure the distance in mm from the face of the tile to the face of the client. Record the number as <Value>.

16) Type or C/P the following command line:
sendOssia 10.10.1.53 setLinearOffset <Value> mc1 7000

17) Using the CLI type reset_host. Go to the client and using an appropriate tool physically press the client's reset button.
Wait for the tile to discover the client and enter the ready state (light ring green with rotating red)

18) Type or C/P the following command at the command line:
grep "setupClient74 options.client74" log_angular_limit1.txt
Observe and copy the value displayed.

19) Type or C/P the following command line Replacing VALUE with the value observed in step 12.
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_testday -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_SingleDist_AngularPowerLimit --logcomments="UL FCC Submittal Angular Limit Reverse; log_angular_limit2.txt"  > log_angular_limit2.txt 2>&1 &
Replace the "-c" option value with the appropriate client ID number.
This test will take approximately 52 mins.

 DISTANCE LIMIT VERIFICATION STEP
20) Turn the table with the tile securely attached to it, 180 degrees. Insure the power and Ethernet cables are free and clear and
arranged such that the rotary actuator has full range of movement from 90 to -90 degs.

21) Type or copy and paste (C/P) the following command lines:
sendOssia 10.10.1.53 setPosition 0 mc1 7000
sendOssia 10.10.1.53 setPosition 0 mc2 7000

22) Measure the distance in mm from the face of the tile to the face of the client. Record the number as <Value>.

23) Type or C/P the following command line:
sendOssia 10.10.1.53 setLinearOffset <Value> mc1 7000

24) Using the CLI type reset_host. Go to the client and using an appropriate tool physically press the client's reset button.
Wait for the tile to discover the client and enter the ready state (light ring green with rotating red)

25) Type or C/P the following command line.
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_submittal -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_DistanceLimitVerification --logcomments="UL FCC Submittal Distance Limit Verification" > log_alrev2.txt 2>&1 &
Replace the "-c" option value with the appropriate client ID number.
This test will take approximately 1 hr 16 mins.

The data can be found for review on http://10.10.0.216:8181

The Data log files are in ./Logs and will have todays date/time stamp and "Submittal" in the file name.
