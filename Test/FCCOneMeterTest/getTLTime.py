from optparse import OptionParser

linmove = 0.25 # time per cm
degmove = 0.25 # time per deg
def getTimeFromList(testlist, debug=False) :

    timesecs = 0
    lastTxAngle = None 
    lastDistance = None
    for testdict in testlist :
        count     = testdict.get("logcount", 0)
        warmup    = testdict.get("warmup", 0)
        waittime  = testdict.get("waitfordata", 0)
        distance  = testdict.get("zdistance", 0)
        txangle   = testdict.get("txangle", 0)
        timesecs += (count * waittime) + warmup
        if lastTxAngle is not None :
            timesecs += abs(lastTxAngle - txangle) * degmove
            lastTxAngle = txangle
        if lastDistance is not None :
            timesecs += abs(lastDistance - distance) * linmove
            lastDistance = distance
        if debug :
            print("count={} warmup={} waittime={} distance={} txangle={}".format(count, warmup, waittime, distance, txangle))

    return timesecs

def getTestList(tlfile, tlname) :
    import imp
    try :
        tstlstmod = imp.load_source(tlname, tlfile)
        testlistE = eval("tstlstmod."+tlname)
    except Exception as ee :
        print("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(tlname, tlfile, repr(ee)))
    return testlistE

if __name__ == '__main__' :

    parser = OptionParser()

    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default=None, help="Use the named list from the file (.py).")
    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    if options.debug :
        print("options {}".format(repr(options)))

testlistE = getTestList(options.testlistfile, options.testlistname)
timesecs = getTimeFromList(testlistE, options.debug)
hrs = timesecs / 3600.0
mins = (hrs - int(hrs)) * 60.0
secs = (mins - int(mins)) * 60.0
print("time = {}:{}:{}".format(int(hrs), int(mins), int(secs)))
    

