# Angular facing client
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_submittal -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_SingleDist_AngularPowerLimit --skiptonext --logcomments="log_AngularFacing.txt; alg;Hys100" > log_AngularFacing.txt 2>&1 &
# Angular non-facing client
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_submittal -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_SingleDist_AngularPowerLimit_Reverse --skiptonext --logcomments="log_AngularReverse.txt; alg;Hys100" > log_AngularReverse.txt 2>&1 &
# Angular Distance limit
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_submittal -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_DistanceLimitVerification --skiptonext --logcomments="log_DistanceLimit.txt; alg;Hys100" > log_DistanceLimit.txt 2>&1 &
# Angular Distance limit 90 deg rotation
python3 FCCOneMeterTest.py --cfgfile=FCCOneMeterTestConfs.py --cfgname=cfgOrion1M_submittal -c 0x124B0007BA0236 --testlistname=testlistOrionFCC_DistanceLimitVerification --skiptonext --logcomments="log_DistanceLimit90Deg.txt; alg;Hys100" > log_DistanceLimit90Deg.txt 2>&1 &
