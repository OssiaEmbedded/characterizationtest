''' FCCOneMeterTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by command line options and config options in a command line option defined file.
This is used for the 1 Meter test.

Version : 1.0.0
Date : July 08 2020
Copyright Ossia Inc. 2020

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback, inspect
if "/home/ossiadev/pylib" not in sys.path  :
    sys.path.insert(0, "/home/ossiadev/pylib")

#if "/home/ursus/pylib" not in sys.path :
#    sys.path.insert(0, "/home/ursus/pylib")

#if "/Users/ursusm/SVNSources/python/pylib" not in sys.path :
#    sys.path.insert(0, "/Users/ursusm/SVNSources/python//pylib")

import json
from time import sleep, time, localtime
from optparse import OptionParser

import Orion.OrionTX as otx 
import SA.SACom as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from LIB.actuator import Actuator
#from LIB.resultsEmail import EmailResults
from LIB.timestamp import DurationCalc

from subprocess import check_output as spcheck_output
from subprocess import STDOUT as spSTDOUT
from os.path import basename as BaseName

g_1MeterNormVal = 0.0
TESTDBNAME   = 'IntegLabOne'
COUPLERLOSS  = 19.8
FINDHOLDTIME = 2.0

TIME2CAL      = 1
CHARGERNEWSTATUS = False


TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME     = "SaTable"
TIMESTAMPCOLNAME      = "DataTimeStamp"
DEFAULTDBSERVER       = 'ossia-build'
TXTBLNAME             = "ChargerConfig"

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(40)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["PmusEnabled","%s", 'VARCHAR(50)'],
         ["RecsEnabled","%s", 'VARCHAR(50)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'],
         ["calPhase", "%s", 'VARCHAR(4096)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["clientAlias", "%s", 'VARCHAR(220)'],
             ["clientStatusDisc", "%s", 'VARCHAR(220)'],
             ["netCurrent", "%1.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%1.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%1.0f", 'VARCHAR(10)'],
             ["peakPower", "%1.0f", 'VARCHAR(10)'],
             ["mWPower", "%1.0f", 'VARCHAR(20)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["TxAngle", "%1.1f",'VARCHAR(7)'],
             ['DistanceZ1', "%d", 'VARCHAR(10)'],
             ["TPSMissed", "%d",    'VARCHAR(20)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["QueryTime", "%s", 'VARCHAR(20)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["clientComRSSI", "%d", 'VARCHAR(6)'],
             ["proxyComRSSI", "%d", 'VARCHAR(6)'],
             ["PowerLevel", "%5.2d", 'VARCHAR(7)'],
             ["WarmupTime", "%d",'VARCHAR(7)'],
             ["Model", "%s",        'VARCHAR(10)'],
             ["version", "%s", 'VARCHAR(20)'],
             ["MaxRfPwr_1",  "%d", 'VARCHAR(10)'],
             ["MaxRfmdBm_1", "%d", 'VARCHAR(10)'],
             ["MaxRfPwr_2",  "%d", 'VARCHAR(10)'],
             ["MaxRfmdBm_2", "%d", 'VARCHAR(10)'],
             ["MaxRfPwr_3",  "%d", 'VARCHAR(10)'],
             ["MaxRfmdBm_3", "%d", 'VARCHAR(10)'],
             ["MaxRfPwr_4",  "%d", 'VARCHAR(10)'],
             ["MaxRfmdBm_4", "%d", 'VARCHAR(10)'],
             ["AvgRfPwr_1",  "%d", 'VARCHAR(10)'],
             ["AvgRfmdBm_1", "%d", 'VARCHAR(10)'],
             ["AvgRfPwr_2",  "%d", 'VARCHAR(10)'],
             ["AvgRfmdBm_2", "%d", 'VARCHAR(10)'],
             ["AvgRfPwr_3",  "%d", 'VARCHAR(10)'],
             ["AvgRfmdBm_3", "%d", 'VARCHAR(10)'],
             ["AvgRfPwr_4",  "%d", 'VARCHAR(10)'],
             ["AvgRfmdBm_4", "%d", 'VARCHAR(10)'],
             ["Threshold", "%d", 'VARCHAR(10)'],
             ]

sTXIP='SV2Tile_IntegLab'
sTXPORT='50000'
sSAIP='A-N9020A-11404.ossiainc.local'
sSAPORT='5023'
srvPORT='50081'

# object to Email Results to a defined list 
#emailer = EmailResults()

def eprint(msg, ln=False) :
    linemsg =  msg
    if ln :
        linemsg = "{} @@{}".format(msg, inspect.currentframe().f_back.f_lineno)
    print("E {}".format(linemsg))
    sys.stdout.flush()
    #emailer.setResults(linemsg)

def setupClient74(tx, clientlst, options) :
    global g_1MeterNormVal
    CNT = 21
    qp = int(options.setupqueryperiod) + 2
    temp74 = options.client74
    percentval = 1.0
    if temp74 :
        if not temp74.isdigit() :
            if "%" == temp74[-1] :
                percentval -= float(temp74[:-1])/100.0 # get rid of the % character
            temp74 = None
        else :
            temp74 = int(temp74, 0)
    eprint("INFO: setupClient74 options.client74 = {} temp74 = {} percentval = {}".format(repr(options.client74), repr(temp74), percentval))
    for sClientId, _ in clientlst :
        avgVal = 0
        cntVal = 0
        avgRfmdBm_4 = temp74
        vallst = list()
        if temp74 is None :
            Cnt = CNT
            while Cnt > 0 :
                Cnt -= 1
                argv = ['client_detail', sClientId]
                v = tx.sendGetTransmitter(argv)
                status, statval, statdisc = otx.getClientState(v)
                if status :
                    data = otx.processClientData(v)
                    maxval, _, avgRfmdBm_4, _ =  otx.getClientRFPower(data, 4)
                    if avgRfmdBm_4 < 0 :
                        eprint("INFO: setupClient74 avgRfmdBm_4={} maxval {} NOT GOOD -- Cnt {}".format(avgRfmdBm_4, maxval, Cnt))
                        continue
                    vallst.append(avgRfmdBm_4)
                    cntVal += 1
                    sleep(qp) # query period Got a method of knowing what that is.
                else :
                    eprint("ERROR: setupClient74 v = {}\n".format(v))
            if cntVal > 0 :
                vallst.sort(reverse=True) # indext 0 is the max value
                index = 0
                bindex = cntVal - 1
                endv = cntVal//2 + 1
                #for _ in range(endv) : # go through half the array
                #    eprint("INFO for loop index={} bindex={}".format(index, bindex))
                #    if (vallst[index] - vallst[bindex]) < 175 : # compare the max - min again 250 arbitrary
                #        eprint("INFO index={} bindex={}".format(index, bindex))
                #        break
                #    index += 1
                #    bindex -= 1
                avgRfmdBm_4 = int(vallst[index] * percentval)
                eprint("INFO: setupClient74 Threshold value = {} index {} bindex {} -- {}".format(avgRfmdBm_4, index, bindex, repr(vallst)))
            
        argv = ["app_command",sClientId, 74, avgRfmdBm_4 & 0xff, avgRfmdBm_4 >> 8 & 0xff, 5]
        eprint("INFO: setupClient74 argv='{}'".format(repr(argv)))
        v = tx.sendGetTransmitter(argv)
        eprint("INFO: setupClient74 {}".format(repr(v)))
        g_1MeterNormVal = avgRfmdBm_4
        break

def sendMMCommands(cfglist, tx) :
    """ sendMMCommands -- send commands to the MM that are defined in a list of
                          dictionaries. Each dictionary can have multiple order independent commands.
                          For order dependent put command in separate dict entries.
                          Each Dict entry has a list of lists. Each list here is the full set of
                          parameters required for the command.
                          Returns None
    """
    try :
        cfglist
    except :
        pass
    else :
        if cfglist is not None :
            try :
                for cfgdict in cfglist :
                    for cmdname, arglistoflists in cfgdict.items() :
                        eprint("sendMMCommands cmdname {}".format(cmdname))
                        for arglist in arglistoflists :
                            argv = [cmdname] + arglist
                            eprint("sendMMCommands argv {}".format(argv))
                            rtn = tx.sendGetTransmitter(argv)
                            #eprint("sendMMCommands rtn {}".format(rtn))
                            if otx.checkForSuccess(rtn) :
                                eprint("ALERT: Set up command '{}' args {} failed".format(cmdname, arglist))
            except :
                eprint("ERROR: Command list failure in sendMMCommands -- '{}'".format(repr(cfglist)))

def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            eprint("INFO: Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            eprint("INFO: {}".format(msg))
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getOnChannels(tx) :
    #

    return "", "", []

def stopStartChargingList(tx, ss, clientList, cnt, verbose=False) :
    """ returns True if the requested function failed
                false if it did not fail
    """
    # ss must be either 'stop' or 'start' for this to work
    rtnval = False
    cmd = "{}_charging".format(ss)
    cnt += 1
    if verbose :
        print("Regression count = {}".format(cnt))
    if cnt > 2 :
        return True

    for client, _ in clientList :
        sClientId = str(client)
        if verbose :
            eprint("INFO: {} charging client {}".format(ss.capitalize(), sClientId))
        argv = [cmd, sClientId]
        try :
            v = tx.sendGetTransmitter(argv)
            rtnck, stat, disc = otx.checkStatusReturn(v)
            #rtnck is positive logic if true every thing's good
            if verbose :
                print("CHECK STATUS on START/STOP charging rtnck={} stat={} disc={}".format(repr(rtnck), stat, disc))
            rtnval = not rtnck
            if not rtnval :
                if ss == 'start' :
                    rtnval |= not (stat == 16)
                elif ss == 'stop' :
                    rtnval |= not (stat == 17)
                else :
                    rtnval = True
            # negative logic if rtnval is true then there was a failure
            if  rtnval :
                eprint("ALERT: Charging did not '{}': {}: {}".format(ss, stat, disc))
                eprint("INFO: Resetting client {}".format(sClientId))
                argv = ['client_command', sClientId, '3']
                v = tx.sendGetTransmitter(argv)
                if otx.checkForSuccess(v) :
                    eprint("ALERT: Start/Stop Charging - Client Command 3 request Failed : {}".format(repr(v)))
                sleep(1.5)
                if stopStartChargingList(tx, ss, [[sClientId, ""]], cnt, True) :
                    eprint("ERROR: stopStartChargingList error", True)
                    sleep(0.7)
                else :
                    rtnval = False
        except Exception :
            rtnval = True
    sleep(0.2)
    return rtnval

def getSAPower(sa, option, minmax=False) :
    def firstElm(lst) :
        return lst[0]

    p1, f1 = _getSAPower(sa, option)
    p2, f2 = _getSAPower(sa, option)
    p3, f3 = _getSAPower(sa, option)
    a=[[p1,f1],[p2,f2],[p3,f3]]
    a.sort(key=firstElm)
    if minmax == True : # max value
        g = 2
    elif minmax == False : # min value
        g = 0
    else : # minmax == None mid or majority value
        g = 1

    rtn = a[g][0]
    f   = a[g][1]
    eprint("INFO: getSAPower g={} val={} lst={}".format(g, rtn, repr(a)), True)
    return  rtn, f

def _getSAPower(sa, option) :
    """ _getSAPower -- accesses the SA if enabled and returns the Power and freq
                      sapower is a float
                      safreq is a float
    """
    sapower = -60.0
    safreq = -2.0
    if sa.usesa :
        #if option.average :
        #    sa.avgHold = ""
        #else :
        #    sa.maxHold = ""
        # wait for data to get stable
        #sleep(option.sadwelltime)
        # find the freq of the peak
        sa.peakSearch = ""
        # get the SA data
        sleep(option.sadwelltime)
        sa.triggerHold = ''
        SApower, SAfreq = sa.peakSearch

        sa.triggerClear = ''
        sa.clearHold = ''
        try :
            sapower = float(SApower)
        except ValueError as ve :
            eprint("ERROR: ValueError on SApower '{}' -- {}".format(SApower, ve), True)
            sapower = -1.0
        try :
            safreq = float(SAfreq)
        except ValueError as ve :
            eprint("ERROR: ValueError on SAfreq '{}' -- {}".format(SAfreq, ve), True)
            safreq = -1.0
        sa.saClose(True)
    return sapower, safreq/1E9

def calibrateAlgorithm(tx, option) :
    """ calibrateAlgorithm -- first resets the tile and then performs calibrate after that.
                              Calibrate 5 times getting SA PeakPower each time. The maximum of that
                              sequence will then be used as a goal for up  to 10 more calibrates.
                              When one of them is equal to (+- .5) or greater than the max the
                              algorithm finishes without calibrating again.
    """
    rtnval   = True
    rtnlst   = []
    avgRfmdBm_4 = 0

    print("calibrateAlg")
    argc = ['client_detail', option.clientid]
    argv = ['calibrate']
    v = tx.sendGetTransmitter(argv)
    if otx.checkForSuccess(v) :
        eprint("ALERT: calibrateAlgorithm1 - Transmitter did not Calibrate")
        rtnval = False
    sleep(TIME2CAL)

    if rtnval :
        for cal in range(3) :
            v = tx.sendGetTransmitter(argc)
            status, statval, statdisc = otx.getClientState(v)
            if status :
                data = otx.processClientData(v)
                maxval, _, avgRfmdBm_4, _ =  otx.getClientRFPower(data, 4)
            rtnlst.append(avgRfmdBm_4)
            eprint("INFO: Finding {} threshold={}".format(cal, avgRfmdBm_4))

            v = tx.sendGetTransmitter(argv)
            if otx.checkForSuccess(v) :
                eprint("INFO: calibrateAlgorithm2 - Transmitter did not Calibrate")
                rtnval = False
                break
            sleep(6)
    
        thresMax = max(rtnlst)
        eprint("INFO:  thresMax = {} vallist={}".format(thresMax, rtnlst))

        for cal in range(10) :
            v = tx.sendGetTransmitter(argc)
            status, statval, statdisc = otx.getClientState(v)
            if status :
                data = otx.processClientData(v)
                maxval, _, avgRfmdBm_4, _ =  otx.getClientRFPower(data, 4)
            eprint("INFO: Seeking {} thresMax - 300 ={} avgRfmdBm_4={}".format(cal, (thresMax - 300), avgRfmdBm_4))
            if (thresMax - 300) < avgRfmdBm_4 :
                break
            v = tx.sendGetTransmitter(argv)
            if otx.checkForSuccess(v) :
                eprint("ERROR: calibrateAlgorithm{} - Transmitter did not Calibrate".format(cal + 5))
                rtnval = False
                break
            sleep(6)

    eprint("INFO: Final thresMax={} avgRfmdBm_4 ={}\n".format(thresMax, avgRfmdBm_4))
    return rtnval, rtnlst

def transmitterCalibrate(tx, clientList, msg='None', option=None) :
    rtnval = False #stopStartChargingList(tx, 'stop', clientList)

    if not rtnval :
        argv = ['calibrate']
        tm = time()
        v = tx.sendGetTransmitter(argv)
        eprint("INFO: Calibrate time was {:3.2f} -- {}".format(time()-tm, repr(v)))
        if otx.checkForSuccess(v) :
            eprint("ALERT: transmitterCalibrate - Transmitter did not Calibrate: {}".format(msg))
            rtnval = True
    
    sleep(TIME2CAL)
    #if not rtnval :
        #rtnval = stopStartChargingList(tx, 'start', clientList)

    eprint("INFO: Calibration {} CL={}".format("Failed" if rtnval else "Done", repr(clientList)))

    return rtnval

def getClientCommandData(tx, sClientId, num, delayTime=2.5) :

    argv = ['app_command', sClientId, num]
    v = tx.sendGetTransmitter(argv)
    if otx.checkForSuccess(v) :
        eprint("ALERT: getClientCommandData - Client Command {} request Failed : {}".format(num, repr(v)))

    sleep(delayTime)

    argv = ['app_command_data', sClientId]
    v = tx.sendGetTransmitter(argv)
    if otx.checkForSuccess(v) :
        eprint("ALERT: getClientCommandData - Client Command Data request Failed ({}): {}".format(num, repr(v)))
    return v

def makeMaxPwrList(clientList) :
    maxPwrList = dict()
    for c, _ in clientList :
        maxPwrList.update({c : {"maxP" : -99.9, "maxSA" : -99.9}})

    return maxPwrList

def printTime(msg1, warmupin=None, noprnt=False) :
    tm_obj = localtime(time())
    if warmupin is not None :
        tf_obj = localtime(time()+warmupin)
        fmt = msg1 + " Start at -- {:02d}:{:02d}:{:02d} finish at {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(warmupin, tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec,
                                      tf_obj.tm_hour, tf_obj.tm_min, tf_obj.tm_sec)
    else :
        fmt = msg1 + " -- {:02d}:{:02d}:{:02d}\n"
        strval = fmt.format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
    if not noprnt :
        print(strval)
    return strval

def getSystemTemperature(tx) :
    # get system temperature datum
    argv = ['get_system_temp', 0]
    v = tx.sendGetTransmitter(argv)
    systemTemp = [0.0, []]
    if otx.checkForSuccess(v) :
        eprint("ALERT: System Temperature not valid {}".format(repr(v)), True)
    else :
        systemTemp = otx.getSystemTemp(v)
    return systemTemp[0]

def makeClientList(clientListIn, testlistStr) :
    """ makeClientList --  this produces a list that is a subset of clientListIn which came from the command line or the config file.
     if thisClientStr is empty clientListIn is returned. Any misspelled ids or aliases are ignored. If none of
     the ids or aliases in the CSL, in thisClientStr, exist in clientListIn then clientListIn will be returned.
    """
    rtnlist = list()
    tl = testlistStr.split(",")
    if len(tl) == 0 :
        rtnlist = clientListIn
    else :
        for ca in range(len(tl)) :
            for i in range(len(clientListIn)) :
                if tl[ca] in clientListIn[i] :
                    rtnlist.append(clientListIn[i])
                    break
    if len(rtnlist) == 0 :
        rtnlist = clientListIn
    return rtnlist

g_ThresholdShowsOff = -1 
maxSystemTemp = 0.0
def runthecharacter(sa, tx, clientListIn, testlist, logger, option) :

    global maxSystemTemp
    global g_ThresholdShowsOff
    finished = False
    iterateTime = time()
    iterateCnt = -1
    iterateCntUp = 1
    iterateSave = -1 
    comChanFreq = { "24" : 2.4601E9, "25" : 2.4501E9, "26" : 2.4401E9}
    try :
        iterateCnt = int(testlist.get('logcount', 10))
        iterateSave = iterateCnt
    except ValueError as ve :
        eprint("ERROR: logcount input error -- {}".format(ve), True)
        return True

    try :
        zDistance         = int(testlist.get('zdistance', 0))
        txAngle           = float(testlist.get('txangle', 0.0))
        passfailmin       = testlist.get('passfailmin',-100.0)
        passfailmax       = testlist.get('passfailmax', 100.0)
        thisClientStr     = str(testlist.get('clientlist', ""))
        calJustOnce       = testlist.get('caljustonce', False)
        setupcommands     = testlist.get('setupcommands', None)
        setupclient74     = testlist.get('setupclient74', None)
        startcharging     = testlist.get('startcharging', None)
    except ValueError as ve :
        eprint("ERROR: ValueError on testlist Dist/angle access -- {}\n".format(ve), True)
        return True

    realZDistance = zDistance
    realTxAngle = txAngle
    if option.actuator is not None :
        act2 = None
        actuatorAry = option.actuator.split(",")
        act1 = Actuator(actuatorAry[0], None, debug=option.debug)
        typestr = str(act1.sendCommandString(arg0='getType', arg1='mc1'))
        act1.sendCommandString(arg0='setPosition', arg1=str(zDistance), arg2='l')
        rtnlst = str(act1.sendCommandString(arg0='getPosition', arg1='l')).split()
        for val in rtnlst :
            try :
                realZDistance = int(val)
                print("        Z real vs. set: abs({} - {}) = {}\n".format(realZDistance, zDistance, abs(realZDistance-zDistance)))
                break
            except ValueError :
                pass
        
        rotact = act2
        if "RotaryConfig" in typestr :
            rotact = act1

        if rotact :
            rotact.sendCommandString(arg0='setPosition', arg1=str(txAngle), arg2='r')
            rtnlst = rotact.sendCommandString(arg0='getPosition', arg1='r').split()
            for val in rtnlst :
                try :
                    realTxAngle = float(val)
                    print("        Rot real vs. set: abs({} - {}) = {}\n".format(realTxAngle, txAngle, abs(realTxAngle-txAngle)))
                    break
                except ValueError :
                    pass

    clientList = makeClientList(clientListIn, thisClientStr)

    if clientList != clientListIn :
        clargs = [['all','']]
        if stopStartChargingList(tx, 'stop', clargs, 0, True) :
            eprint("ERROR: stopStartChargingList error when stoping all", True)
            return True
    if startcharging :
        if stopStartChargingList(tx, 'start', clientList, 0, True) :
            eprint("ERROR: stopStartChargingList error", True)
            return True

    # wait for the warmup period
    warmup = int(testlist.get('warmup', 30))
    eprint(printTime("INFO: Warmup time {} :", warmupin=warmup, noprnt=True))

    reportWait(warmup, prnt=option.console)

    if option.poscalibrate or calJustOnce :
        print("calalg")
        if option.calibratealg and not option.poscalibrate :
            print("calalg")
            _, calData = calibrateAlgorithm(tx, option)
            eprint("INFO: calDatJson = '{}'".format(repr(calData)))
            #if option.logtodb : 
            #    try :
            #        logger.logUpdateTableColumn(option.dbname, option.tblname, "calPhase", calDataJson)
            #        print("logUpdateTableColumn DONE")
            #    except Exception as e : 
            #        eprint("ALERT: Logging the calPhase failed {}\n".format(repr(e)), True)
        else :
            transmitterCalibrate(tx, clientList, msg="After warmup", option=option)

    calibrationInterval = int(option.calibrate)

    # send a set of MM commands that are contained in the test list entry.
    try :
        sendMMCommands(setupcommands, tx)
    except :
        pass

    if setupclient74 is not None :
        setupClient74(tx, clientList, option)

    maxPowerList = makeMaxPwrList(clientList)
    systemTemp = getSystemTemperature(tx)
    maxSystemTemp = max(maxSystemTemp, systemTemp)

    threshValue = 0
    while (iterateCnt > 0  or (iterateTime - time()) > 0.0) and not finished :

        timetowait = float(testlist.get('waitfordata', 5)) - (option.sadwelltime * 3.0)
        timetowait = max(timetowait, 0.0)

        if timetowait > 0.0 :
            sleep(timetowait)

        if calibrationInterval > 0 and (iterateCntUp % calibrationInterval) == 0 :
            transmitterCalibrate(tx, clientList, msg="After interval time", option=option)
            logger.logAddValue('calibrate', "1")
            sleep(timetowait)
        elif calibrationInterval > 0 :
            logger.logAddValue('calibrate', "0")
        iterateCntUp += 1

        iterateCnt -= 1
        if option.console and iterateCnt >= 0 :
            print("{} ".format(iterateCnt), end='')

        # set SA to obtain peak or average value
        a = None
        if option.checkpoint is not None :
            a = zDistance <= option.checkpoint

        eprint("INFO: getSAPower zDistance {} Angle {}".format(zDistance, txAngle))
        SAPower, SAFreq = getSAPower(sa, option, a)

        systemTemp = getSystemTemperature(tx)
        maxSystemTemp = max(maxSystemTemp, systemTemp)

        # get charging client details
        for client, alias in clientList :
            # add SA values to the log list
            tm_obj = localtime(time())
            datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
            logger.logAddValue('timetaken', datatime)

            if option.average :
                logger.logAddValue('SAPowerAvg', SAPower)
                logger.logAddValue('SAFreqAvg', SAFreq)
            else :
                logger.logAddValue('SAPowerPeak', SAPower)
                logger.logAddValue('SAFreqPeak', SAFreq)

            logger.logAddValue('DistanceZ1', int(zDistance))
            logger.logAddValue("PowerLevel", testlist['powerlevel'])
            logger.logAddValue("WarmupTime", testlist['warmup'])
            logger.logAddValue("TxAngle", txAngle)
            logger.logAddValue('systemTemp', systemTemp)

            sClientId = str(client)
            sAlias = str(alias)
            logger.logAddValue('clientId', sClientId)
            logger.logAddValue('clientAlias', sAlias)

            argv = ['client_detail', sClientId]
            v = tx.sendGetTransmitter(argv)
            status, statval, statdisc = otx.getClientState(v)

            if not status :
                eprint("ALERT: Client Detail request shows client {} returned status {} trying again -- {}".format(sClientId, statval, statdisc), True)
                v = tx.sendGetTransmitter(argv)
                status, statval, statdisc = otx.getClientState(v)
                if not status :
                    eprint("ERROR: Client Detail request shows client {} returned status {} throwing ValueError -- {}".format(sClientId, statval, statdisc), True)
                    raise ValueError("Could not complete client_detail")

            logger.logAddValue('clientStatusDisc', statdisc)

            version          = otx.getClientVersion(v)
            logger.logAddValue('version', version)

            netCurrent       = otx.getClientNetCurrent(v)
            logger.logAddValue('netCurrent', netCurrent)

            queryFailedCount = otx.getClientQueryFailed(v)
            logger.logAddValue('queryFailedCount', queryFailedCount)

            linkQuality      = otx.getClientLinkQuality(v)
            logger.logAddValue('linkQuality', linkQuality)

            batteryLevel     = otx.getBatteryLevel(v)
            logger.logAddValue('batteryLevel', batteryLevel)

            Model            = otx.getDeviceModel(v)
            logger.logAddValue('Model', Model)

            DeviceStatus     = otx.getDeviceStatus(v)
            logger.logAddValue('DeviceStatus', DeviceStatus)

            TPSMissed        = otx.getTPSMissed(v)
            logger.logAddValue('TPSMissed', TPSMissed)

            averagePower     = otx.getAveragePower(v)
            logger.logAddValue('averagePower', averagePower)

            peakPower        = otx.getPeakPower(v)
            logger.logAddValue('peakPower', peakPower)

            clientComRSSIVal = otx.getClientComRSSI(v)
            logger.logAddValue('clientComRSSI', clientComRSSIVal)

            proxyComRSSIVal  = otx.getProxyComRSSI(v)
            logger.logAddValue('proxyComRSSI', proxyComRSSIVal)

            queryTime        = otx.getClientQueryTime(v)
            logger.logAddValue('QueryTime', queryTime)

            # There is only one SA attache to one client
            maxPowerList[sClientId]['maxSA'] = max(maxPowerList[sClientId]['maxSA'], SAPower)
            maxPowerList[sClientId]['maxP']  = max(maxPowerList[sClientId]['maxP'], peakPower)


            # get charging client data
            #argv = ['client_data', sClientId]
            #v = tx.sendGetTransmitter(argv)
            #if otx.checkForSuccess(v) :
                #eprint("ALERT: Client Data request Failed: {} -- trying again".format(repr(v['Result']['Status'])), True)
                #v = tx.sendGetTransmitter(argv)
                #if otx.checkForSuccess(v) :
                    #eprint("ALERT: Client Data request Failed again: {} -- moving on".format(repr(v['Result']['Status'])), True)
                    #rtnval = logger.logResults()
                    #if rtnval :
                        #eprint("ERROR: Couldn't log values for Client {} -- {}.".format(sClientId, rtnval), True)
                        #finished = True
                        #continue
            batteryVoltage = otx.getClientBatteryVoltage(v)

            logger.logAddValue('batteryVoltage', batteryVoltage)
            mWPower = batteryVoltage * netCurrent
            logger.logAddValue("mWPower", mWPower)

            data = otx.processClientData(v)
            maxRfPwr_1, maxRfmdBm_1, avgRfPwr_1, avgRfmdBm_1 =  otx.getClientRFPower(data, 1)
            maxRfPwr_2, maxRfmdBm_2, avgRfPwr_2, avgRfmdBm_2 =  otx.getClientRFPower(data, 2)
            maxRfPwr_3, maxRfmdBm_3, avgRfPwr_3, avgRfmdBm_3 =  otx.getClientRFPower(data, 3)
            maxRfPwr_4, maxRfmdBm_4, avgRfPwr_4, avgRfmdBm_4 =  otx.getClientRFPower(data, 4)
            eprint("INFO: Last Power Measured        avgRfPwr_4 = {}".format(avgRfPwr_4))
            eprint("INFO: Last Power below threshold maxRfPwr_4 = {} -- Threshold {}".format(maxRfPwr_4, g_1MeterNormVal))
            threshValue += avgRfPwr_4

            logger.logAddValue('MaxRfPwr_1',  maxRfPwr_1)
            logger.logAddValue('MaxRfmdBm_1', maxRfmdBm_1)
            logger.logAddValue('MaxRfPwr_2',  maxRfPwr_2)
            logger.logAddValue('MaxRfmdBm_2', maxRfmdBm_2)
            logger.logAddValue('MaxRfPwr_3',  maxRfPwr_3)
            logger.logAddValue('MaxRfmdBm_3', maxRfmdBm_3)
            logger.logAddValue('MaxRfPwr_4',  maxRfPwr_4)
            logger.logAddValue('MaxRfmdBm_4', maxRfmdBm_4)
            logger.logAddValue('AvgRfPwr_1',  avgRfPwr_1)
            logger.logAddValue('AvgRfmdBm_1', avgRfmdBm_1)
            logger.logAddValue('AvgRfPwr_2',  avgRfPwr_2)
            logger.logAddValue('AvgRfmdBm_2', avgRfmdBm_2)
            logger.logAddValue('AvgRfPwr_3',  avgRfPwr_3)
            logger.logAddValue('AvgRfmdBm_3', avgRfmdBm_3)
            logger.logAddValue('AvgRfPwr_4',  avgRfPwr_4)
            logger.logAddValue('AvgRfmdBm_4', avgRfmdBm_4)
            logger.logAddValue('Threshold',   g_1MeterNormVal)

            # add the time entry
            if option.loglongbcnt :
                v = getClientCommandData(tx, sClientId, '42')
                longBeaconCnt = otx.getCommandData(v,42, 5, 4) # offset 5, num of bytes 4
                numBeaconCnt = otx.getCommandData(v,42, 1, 4) # offset 1, num of bytes 4
                logger.logAddValue("LongBeaconCnt", longBeaconCnt)
                logger.logAddValue("NumBeaconCnt", numBeaconCnt)

                # get client command data 20
                v = getClientCommandData(tx, sClientId, '20')
                watchDogRstCnt = otx.getCommandData(v, 20, 7, 2) # offset 7, num of bytes 2
                logger.logAddValue("WatchDogRstCnt", watchDogRstCnt)

            rtnval = logger.logResults()
            if rtnval :
                eprint("ERROR: Couldn't log values for Client {} -- {}.".format(sClientId, rtnval), True)
                finished = True
                break

            if peakPower >= option.maxrssi :
                eprint("INFO: peakPower value exceeded peakPower limit: {} < {} cnt {}\n".format(option.maxrssi, peakPower, iterateCntUp-1), True)
                if (iterateCntUp-1) >= 3 :
                    finished = True
    eprint("INFO: threshValue {} g_ThresholdShowsOff {}".format(threshValue, g_ThresholdShowsOff))
    if g_ThresholdShowsOff >= 0 and threshValue < 0 :
        g_ThresholdShowsOff += 1

    for client, data in maxPowerList.items() :

        try :
            if isinstance(passfailmin, dict) :
                minlim = float(passfailmin.get(client,-100.0))
            else :
                minlim = float(passfailmin)
        except ValueError as ve :
            eprint("ALERT: ValueError on passfail min limit -- {}".format(ve), True)
            minlim = -100.0

        try :
            if isinstance(passfailmax, dict):
                maxlim = float(passfailmax.get(client, 100.0))
            else :
                maxlim = float(passfailmax)
        except ValueError as ve :
            eprint("ALERT: ValueError on passfail max limit -- {}".format(ve), True)
            maxlim = 100.0

            # print and email some intermediate results
        eprint("INFO: Client {} at Z {:4d} Angle {:1.1f}".format(client, int(zDistance), float(txAngle)))
        if iterateSave == 0 : # don't print stats if no data taken
            break
        eprint("INFO: <b>Passfail criteria are Min {} Max {}</b>".format(minlim, maxlim))        
        eprint("INFO: <b>Max SA PeakPower : {:1.2f} dBm, Max Client PeakPower : {:1.2f} dBm</b>".format(data['maxSA'], data['maxP']))
        eprint("INFO: System Temperature {} / Max {}".format(systemTemp, maxSystemTemp))

        thePname = ""
        if sa.usesa :
            pwrval = data['maxSA']
            thePname = "SA"
        else :
            pwrval = data['maxP']
            thePname = "CR"

        if pwrval > maxlim :
            eprint("ERROR: Pass/Fail Maximum limit exceeded {} Power: {} > {}".format(thePname, pwrval, maxlim), True)
        elif pwrval < minlim :
            eprint("ERROR: Pass/Fail Minimum limit exceeded {} Power: {} < {}".format(thePname, pwrval, minlim), True)

    return finished

def main(tstList, passfailE, option, logger) :

    global g_ThresholdShowsOff
    MYSTART_TIME = time()
    # init the TX communication object
    tx=otx.OrionTx(option.txipaddr, option.txport, option.debug)

    # send a set of setup cfg commands that are contained in the conf file
    try :
        sendMMCommands(option.setupcfgparams, tx)
    except :
        pass
    verstr = "error"
    try :
        argv = ["versions"]
        v = tx.sendGetTransmitter(argv)
        verstr, _ = otx.getVersions(v)
    except Exception as ee :
        eprint("ERROR: Release Version Unknown Exception -- {}".format(repr(ee)))

    def myZip(c, a) :
        cc = ""
        aa = ""
        if isinstance(c, str) :
            cc = str(c).strip()
        if isinstance(a, str) :
            aa = str(a).strip()
        return [cc,aa]
    # process client id CSL
    cl = option.clientid.split(',')
    al = option.cidaliases.split(',')
    clientAliasesList = map(myZip, cl, al)

    errorCondition = False
    # none specified so find one
    argv = ["client_list"]
    v = tx.sendGetTransmitter(argv)

    clientList = list()
    #                            0,       1,     2,      3
    # rtndict has this {cid : [stat Num, lnkQ, statMsg, alias]}
    rtnval, rtndict = otx.processClientList(v, clientAliasesList)
    if not rtnval :
        errorCondition = True
        for cid, stat in rtndict.items() :
            eprint("ERROR: Client id {} unavailable with linkQuality {} and status {} --- {}".format(cid, stat[1], stat[0], stat[2]))
    else :
        for cid, stat in rtndict.items() :
            clientList.append([cid, stat[2]])
            eprint("INFO: Continuing with Client ID {} Alias '{}' status {} {}".format(cid, stat[3], stat[0], stat[2]))

    if not errorCondition :
        # Get some Charger data
        cota_config = "ND"
        fullCfgDict = dict()
        clientConfig = dict()

        argv = ["get_charger_id"]
        v = tx.sendGetTransmitter(argv)
        txId = otx.getTxId(v)

        pmusEnabled = ""
        recsEnabled = ""
        clientFW = ""
        for client, alias in clientList :
            try :
                sClientId = str(client)
                sAlias = str(alias)
                argv = ['register_client', sClientId]
                v = tx.sendGetTransmitter(argv)
                if otx.checkForSuccess(v,['SUCCESS', 'COTA_ERROR_CLIENT_ALREADY_REGISTERED']) :
                    eprint("ERROR: Register client {} {}failed".format(sClientId, sAlias + " "))
                    return
                # configure the client
                argv = ['client_config', sClientId, '6']
                v = tx.sendGetTransmitter(argv)
                if otx.checkForSuccess(v) :
                    eprint("ERROR: Client config failed")
                    return
                sleep(2)
                # check for a non zero Short ID
                argv = ['client_detail', sClientId]
                v = tx.sendGetTransmitter(argv)
                shortID = otx.getClientShortID(v)
                versionCR = otx.getClientVersion(v)
                if shortID == "0x0000" :
                    eprint("ERROR: Short ID shows device is not registered: " + shortID)
                    return
                # get the PMUs and RECs that are enabled
                if len(pmusEnabled) > 0 :
                    pmusEnabled += " "
                    recsEnabled += " "
                v = getClientCommandData(tx, sClientId, '17')

                tmp = otx.getCommandData(v, 17, 1, 4, False)
                pmusEnabled += "{}_{}".format(sClientId, tmp)

                v = getClientCommandData(tx, sClientId, '33')

                tmp = otx.getCommandData(v, 33, 1, 4, False)
                recsEnabled += "{}_{}".format(sClientId, tmp)

                v = getClientCommandData(tx, sClientId, '61')

                tmpfw = otx.getCommandData(v, 61, 1, 4)
                clientFW += "{}{}_{:08X}_Rel{} ".format(sAlias + "_", sClientId, tmpfw, versionCR)

                #
            except ValueError as ve :
                eprint("ERROR: ClientId ValueError {} -- {}".format(client, ve))
                return

        ## get the Transmitter data values for logging.

        # first get the Transmitter data that we want to log
        #    This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp

        logger.initFile(TXCMS)

        if option.logtodb :
            logger.initDB(option.dbname, hostnm=option.dbservername)
            logger.initDBTable(option.tblname)
        # add option Dict to log list
        val =  repr(option.__dict__).replace("'","").replace('"',"").replace(","," ").replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
        logger.logAddValue("options", val)

        # add txId to log list
        logger.logAddValue("txId", txId)
        logger.logAddValue("AntennaType",option.antennatype)
        logger.logAddValue("CfgComment",option.logcomments)
        logger.logAddValue("CotaConf",cota_config)
        # add OnChannels to log list
        logger.logAddValue("PmusEnabled", pmusEnabled)
        logger.logAddValue("RecsEnabled", recsEnabled)
        OnChannels, ChanList, chRevs = getOnChannels(tx)
        logger.logAddValue("OnChannels", OnChannels)
        v = tx.sendGetTransmitter(['get_valid_ambs'])
        logger.logAddValue("GoodChannels", v['Result']['Valid Ambs'])
        verstrPlusClient =  str(verstr)+"\n"+clientFW
        logger.logAddValue("Versions", verstrPlusClient)
        logger.logAddValue("AmbRev", str(ChanList))
        logger.logAddValue("LogFileName", logger.filename)
        tableTS = str(int(logger.timeStampNum))
        CLIENTTABLENAME = "client_{}".format(tableTS)
        logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
        SATABLENAME = "sa_{}".format(tableTS)
        logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
        logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)

        eprint("INFO: DataBase: {}, Tablename: {}, Test Timestamp: {}".format(option.dbname,
                                                                        option.tblname,
                                                                        logger.timeStamp))
        eprint("INFO: Charger name: {}, Antenna Type: {}, Comments: '{}'".format(option.txipaddr,
                                                                           option.antennatype,
                                                                           option.logcomments))
        eprint("INFO: System versions\n{}".format(verstrPlusClient))

        rtnval = logger.logResults()
        if rtnval :
            eprint("ERROR: Couldn't log values for Transmitter -- {} -- Exiting.".format(rtnval), True)
            return

        # setup the SA

        sa=sacom.SACom(addr=option.saipaddr, port=int(option.saport), debug=option.debug)
        if sa.errorstr :
            eprint("ALERT: SA not connected with error : {} ipaddr {}".format(sa.errorstr, option.saipaddr))
        if sa.usesa :
            eprint("INFO: Using SA {} port {}".format(option.saipaddr, option.saport))
            sacms = list()
            for cmd_data in SA_SETUP_LIST :
                if  cmd_data[2] is not None :
                    sacms.append([cmd_data[0], "%s", cmd_data[2]])
                # the the SA
                sa.setSAValue(cmd_data[0], cmd_data[1])
            sa.referenceLevelOffset = COUPLERLOSS + option.cableloss
            sacms.append(["referenceLevelOffset", "%s", 'VARCHAR(35)'])
            sa.saClose(True)

            # init the logging list
            logger.initFile(sacms,how='a')
            if option.logtodb :
                logger.initDBTable(SATABLENAME)
            # log the SA setup data
            for cmd_data in SA_SETUP_LIST :
                if  cmd_data[2] is not None :
                    logger.logAddValue(cmd_data[0], cmd_data[1])
            logger.logAddValue("referenceLevelOffset", str(COUPLERLOSS + option.cableloss))

            rtnval = logger.logResults()
            if rtnval :
                eprint("ERROR: Couldn't log values for SA. -- {}".format(rtnval), True)
                return
            passfailA = passfailE.get('SA', dict())
        else :
            passfailA = passfailE.get('Client', dict())
            eprint("ALERT: SA is not available")

        clientcms = CLIENTCMS

        if option.average :
            clientcms.insert(5,["SAPowerAvg", "%7.3f", 'VARCHAR(15)'])
            clientcms.insert(5,["SAFreqAvg", "%1.4e", 'VARCHAR(15)'])
        else :
            clientcms.insert(5,["SAPowerPeak", "%7.3f", 'VARCHAR(15)'])
            clientcms.insert(5,["SAFreqPeak", "%1.4e", 'VARCHAR(15)'])

        if option.loglongbcnt :
            clientcms.append(["NumBeaconCnt", "%d", 'VARCHAR(20)'])
            clientcms.append(["LongBeaconCnt", "%d", 'VARCHAR(20)'])
            clientcms.append(["WatchDogRstCnt", "%d", 'VARCHAR(20)'])

        # setup the logging file
        if int(option.calibrate) > 0 :
            clientcms.append(["calibrate", "%s", 'VARCHAR(10)'])
        if option.debug :
            print("FNAME={}".format(logger.filename))
            print("COLFMT={}".format(logger.colformat))

        logger.initFile(clientcms,how='a')
        if option.logtodb :
            logger.initDBTable(CLIENTTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.

        lstDistance = 0
        finished = False
        lstAngle = 0.0

        # if there are setup commands in the config input file then execute them
        try :
            sendMMCommands(option.setupcommands, tx)
        except :
            pass
        number_steps_left = len(tstList)
        lastPowerLevel = -1
        if option.skiptonext :
            g_ThresholdShowsOff = 0
        for itTstDict  in tstList :

            try :
                itTstDict['warmup']      = int(itTstDict.get('warmup', 30))
                itTstDict['txangle']     = float(itTstDict.get('txangle', 0.0))
                itTstDict['zdistance']   = int(itTstDict.get('zdistance', 1000))
            except ValueError as ve :
                eprint("ERROR: ValueError converting warmup and txangle {}".format(ve), True)
                break

        # put the pass fail criteria in the test list for convenience
            curZ = itTstDict.get('zdistance', -1)
            curA = itTstDict.get('txangle', 180)

            eprint("INFO: Last Distance {} Current Distance {} g_ThresholdShowsOff {}".format(lstDistance, curZ, g_ThresholdShowsOff))
            eprint("INFO: Last Angle    {} Current Angle    {}\n".format(lstAngle, curA)) 

            if lstAngle == curA and g_ThresholdShowsOff >= 2 :
                eprint("ALERT: Skipping distance {} angle {}".format(curZ, curA))
                continue
            elif lstAngle != curA and g_ThresholdShowsOff >= 0 :
                g_ThresholdShowsOff = 0

            _, _, _, Duration = DurationCalc(MYSTART_TIME)
            eprint("INFO: {} Duration".format(Duration))

            lstDistance = curZ
            lstAngle    = curA
            powerLevel  = 13
            try :
                powerLevel = int(itTstDict.get('powerlevel', 13))
            except ValueError as ve :
                eprint("ERROR: ValueError converting powerlevel {}".format(ve), True)
                powerLevel = 8.5 if option.mars_system else 13
            if lastPowerLevel != powerLevel :
                argv = ['set_power_level', str(powerLevel)]
                v = tx.sendGetTransmitter(argv)
                if otx.checkForSuccess(v) :
                    eprint("ERROR: Set Power Level failed")
                    return
                lastPowerLevel = powerLevel
                argv = ['get_power_level']
                v = tx.sendGetTransmitter(argv)
                pl =  otx.getTransmitterPowerLevel(v)
                if pl != powerLevel :
                    eprint("ALERT: Power level incorrect get={} vs set={}".format(pl, powerLevel))
                    powerLevel = pl
            eprint("INFO: Power Level set to : {}".format(powerLevel))
            itTstDict['powerlevel'] = powerLevel

            # this function also logs all the client type data to the file and database.
            try :
                finished = runthecharacter(sa, tx, clientList, itTstDict, logger, option)
                number_steps_left -= 1
                printTime("INFO: Number of test list iterations left to do = {} ".format(number_steps_left))
            except KeyboardInterrupt as ki :
                eprint("INFO: Sigint = 2 received -- KeyboardInterrupt exiting --- {} iterleft={}".format(ki, number_steps_left), True)
                finished = True
                break
            except Exception as ex :
                eprint("ERROR: Unexpected Exception exiting --- {} -- {}\n".format(repr(ex), repr(itTstDict)), True)
                eprint("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
                eprint("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
                finished = True
                break

        stopcharge = False
        try :
            stopcharge = option.stopcharging
        except :
            pass
        else :
            if stopcharge :
                clargs = [['all','']]
                if stopStartChargingList(tx, 'stop', clargs, 0, True) :
                    eprint("ERROR: stopStartChargingList error when stopping all at main", True)
                    stopcharge = False
        if stopcharge :
            eprint("INFO: End of test. Stop charging client list")
        else :
            eprint("INFO: End of test. Keep charging client list")
                

def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        Returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict


if __name__ == '__main__' :

    START_TIME = time()
    tm = printTime("INFO: Starting Program Execution at :", noprnt=True)
    eprint(tm)
    parser = OptionParser()
    cmdstr = ""
    for arg in sys.argv :
        cmdstr += arg + " "
    print("\nCommand line = {}\n".format(cmdstr)) 

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None, help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg", help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlistLin", help="Use the named list from the file (.py).")
    parser.add_option("","--passfailfile", dest='passfailfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--passfailname", dest='passfailname', type=str,  action='store',default="passfailLin", help="Use the named list from the file (.py).")
    parser.add_option("-p","--saport",     dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    parser.add_option("-i","--saip",       dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")

    parser.add_option("","--sadwelltime",  dest='sadwelltime',  type=float,action='store',default=1.0,help="Set the SA Peak/avg search dwell time")
    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=sTXIP,help="Transmitter network address.")

    parser.add_option("","--actip",        dest='actuator',     type=str,  action='store',default=None,help="Linear Actuator network address(es) (comma sep).")

    parser.add_option("-c","--clientid",   dest='clientid',     type=str,  action='store',default=None,help="CSL of client ID to use for characterization.")
    parser.add_option("","--cidaliases",   dest='cidaliases',   type=str,  action='store',default="",help="CSL of Aliases in same order as clientIds to use for characterization.")
    parser.add_option("","--maxpwrlevel",  dest='maxrssi',      type=float,action='store',default='28.0',help="Maximum power level to quit test: a client protection.")

    parser.add_option("","--cableloss",    dest='cableloss',    type=float,action='store',default=0.0,help='Cable loss positive dB. Added to the 20 dB of Connector loss')
    parser.add_option("","--calibrate",    dest='calibrate',    type=int,  action='store',default="0",help="Number data points between calibrations.")
    parser.add_option("","--poscalibrate", dest='poscalibrate',            action='store_true',default=False,help="Calibrate at each position change.")
    parser.add_option("","--calibratealg", dest='calibratealg',            action='store_true',default=False,help="Use an algorithm to obtain a relatively good calibration.")
    parser.add_option("","--checkpoint",   dest='checkpoint',   type=int,  action='store',default=None,help="Checkpoint in mm for close or not close to Tile.")
    parser.add_option("","--client74",     dest='client74',     type=str,  action='store',default=None,help="Power Value to send to the client with command 74 or a percent value (e.g.5%) to reduce the capture value by..")

    parser.add_option("","--skiptonext",   dest='skiptonext',              action='store_true',default=False,help="Skip remaining distances to the next angle in list.")

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="IntegLab1LogFile{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--console",      dest='console',                 action='store_true',default=False,help="Turn off printing to console")
    parser.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    parser.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--loglongbcnt",  dest='loglongbcnt',             action='store_true',default=False,help="Log the long beacon count info")

    parser.add_option("","--dbservername", dest='dbservername', type=str,  action='store',default=DEFAULTDBSERVER,help="Default DBServer name.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    print("options {}".format(repr(options)))
    
    import imp
    if len(args) > 0 :
        eprint("ERROR: Extra command line args, exiting")
        eprint("ERROR: {}".format(repr(args)))
    elif options.cfgfile is not None :
        TTYPE = 0
        TDEF = 1
        fname = options.cfgfile
        if fname == '' :
            fname = sys.argv[0].replace('.', 'Conf.')
        cfgmod = imp.load_source(options.cfgname, fname)
        cfg = eval("cfgmod."+options.cfgname)
        optType = makeOptionTypeDict(parser)

        for k in cfg.keys() :
            try :
                if optType.get(k, None) is None :
                    options.__dict__.update({k : cfg[k]})
                elif optType[k][TTYPE]  == 'int' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = int(str(cfg[k]))
                elif optType[k][TTYPE]  == 'float' :
                    if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = float(str(cfg[k]))
                elif optType[k][TTYPE]  is None :
                    options.__dict__[k] = optType[k][TDEF]  # set the default value
                    try :
                        options.__dict__[k] = eval(cfg[k])
                    except NameError :
                        options.__dict__[k] = False
                    except TypeError :
                        options.__dict__[k] = cfg[k]

                elif optType[k][TTYPE]  == 'string' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    elif str(cfg[k]) == 'None' :
                        options.__dict__[k] = None
                    else :
                        options.__dict__[k] = str(cfg[k])
                else :
                    options.__dict__[k] = str(cfg[k])

            except ValueError as ve :
                eprint("ERROR: VE {} -- Incorrect option value {} -- {}\n".format(k, cfg[k], ve))
                exit(2)
            except IndexError as ie :
                eprint("ERROR: IE {} -- No such option -- {}\n".format(k, ie))
                exit(3)
            except Exception as e :
                eprint("ERROR: EX {} -- Unknown exception -- {}\n".format(k, repr(e)))
                exit(4)

        eprint("INFO: Characterization test of host '{}' and Actuator '{}' with Comments '{}'".format(options.txipaddr, options.actuator, options.logcomments))

        try :
            tstlstmod = imp.load_source(options.testlistname, options.testlistfile)
            testlistE = eval("tstlstmod."+options.testlistname)
        except Exception as ee :
            eprint("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, fname, repr(ee)))
        else :
            if options.debug :
                print("options = {}".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
                print("testlist = {}".format(repr(testlistE).replace('},','},\n')))

            eprint("INFO: Executing main\n")

            passfailE = dict()
            if options.passfailname is not None and options.passfailfile is not None :
                try:
                    passfailmod = imp.load_source(options.passfailname, options.passfailfile)
                    passfailE = eval("passfailmod."+options.passfailname)
                except Exception  as ee :
                    eprint("ALERT: passfailmod creation did not succeed on passfail '{}' and filename '{}' -- {}".format(options.passfailname, options.passfailfile, repr(ee)))
                    passfailE = dict()
    
            try :
                cIdstr = str(options.clientid)
            except ValueError as ve :
                eprint("ERROR: Couldn't make clientid a string == '{}'".format(repr(ve)))
                eprint("ERROR: Invalid Client ID option. A clientid CSL string must be supplied -- '{}'".format(repr(ve)))
            else :
                options.clientid = cIdstr
                if options.clientid != "" :
                    logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
                    # HERE's main
                    main(testlistE, passfailE, options, logger)
                else :
                    eprint("ERROR: Invalid Client ID option. A clientid CSL string must be supplied -- '{}'".format(repr(ve)))

    hrs, mins, secs, Duration = DurationCalc(START_TIME)
    cal_data = None
    cal_data_txt = ""
    fd = None
    try :
        fd = open(logger.filename, 'a')
    except IOError as io :
        eprint("ERROR: Could not open file {} -- {}".format(logger.filename, io))
    except Exception as ee :
        eprint("ERROR: Logger not available -- {}".format(repr(ee)))
    else :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()
        if options.logtodb :
            try :
                logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                eprint("ERROR: Logging the Duration  failed {}\n".format(repr(e)))

    eprint("")
    eprint("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    tm = printTime("INFO: Ended Program Execution at :", noprnt=True)
    eprint(tm)
