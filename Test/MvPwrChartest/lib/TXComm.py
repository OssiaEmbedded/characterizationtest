from __future__ import absolute_import, division, print_function, unicode_literals
import sys
import json
from socket import socket, AF_INET, SOCK_STREAM, gethostbyname, SHUT_RDWR, timeout
from TXJSON.JSonCmdStrings import JSON_CMD_DICT as jcd
from time import sleep

class TXComm(object) :
    ''' TXComm Class -- Ossia specific class to provide telnet socket communication to
    a Message Manager sending predefined JSon strings.
    '''

    def __init__(self, txipaddr, txport, debug=False) :
        ''' __init__ -- Initialize class variables.
        '''
        self.ip = gethostbyname(txipaddr)
        self.port = txport
        self.debug = debug
        self.delay = 0.0

    def _sendCommand(self, cmd_in) :
        ''' _sendCommand -- Sends the handed in string to the address and port of this
        instance of the class.
        It also receives the response, if any, from the server at the address and port.
        '''
        sock = socket(AF_INET, SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.settimeout(0.75)
        cmd = cmd_in
        if sys.version_info[0] == 3 :
            cmd = bytes(cmd_in, encoding='utf-8')
        if self.debug :
            print("sendall '{}'".format(cmd_in))
        sock.sendall(cmd)
        if float(self.delay) > 0.0 :
            sleep(self.delay)
        retry = 0
        data = ""
        while retry < 2 and len(data) == 0 :
            try :
                d = sock.recv(2048)
                data += d
            except timeout :
                retry += 1
        if sys.version_info[0] == 3 :
            data = data.decode('utf-8')
        sock.close()
        return data

    def sendGetTransmitter(self, argv) :
        ''' sendGetTransmitter -- Wrapper for _sendCommand to provide command/argument validation.
        '''
        # get the input command from the JSON string dictionary
        cmd = jcd.get(argv[0], None)
        if self.debug :
            print("CMD = {}\n".format(cmd))
        pyVal = None
        if cmd is not None :
            # Since the command was found in the dictionary now check if enough
            # arguments were passed in for it.
            argc = len(argv[1:])
            argsNeeded = cmd.get('numofargs', -1)
            if argsNeeded == argc :
                jstr = cmd.get('jstr', "")
                jstr_cmd = jstr % (tuple(argv[1:]))
                if self.debug :
                    print("jstr_cmd="+repr(jstr_cmd))
                jval = self._sendCommand(jstr_cmd)
                if self.debug :
                    print("jstr_rtn="+repr(jval))
                pyVal = json.loads(jval)
                if self.debug :
                    print("pyVal="+repr(pyVal))
            else :
                print("Arg count incorrect. {} needs exactly {} (sent {})".format(argv[0], argsNeeded, argc))
        return pyVal

if __name__ == "__main__" :

    import threading
    from time import sleep
    PORT_NUM = 50000
    HOST_NM = '10.10.0.143'
    import sys

    endthread = False
    debug = False
    if len(sys.argv) > 1 :
        debug = True


    """
        Here defines a simple server for the TXComm class to connect to.
    """
    def _receiveSend(client, addr) :
        url = client.recv(2048)
        client.send(url)
        sleep(0.15)
        client.close()

    def startServer() :
        hostName = gethostbyname(HOST_NM)
        server = (hostName, PORT_NUM)
        listenSocket = socket(AF_INET, SOCK_STREAM)
        listenSocket.bind(server)
        listenSocket.listen(5)
        threadme = threading.Thread(target=_runServer, args=(listenSocket, ))
        threadme.start()
        return listenSocket
    
    def _runServer(listenSocket) :
        while not endthread :
            client, addr = listenSocket.accept()
            threadme = threading.Thread(target=_receiveSend, args=(client, addr))
            threadme.start()

    '''
        Here begins the unit test for this module.
        It should run through without throwing exceptions.
    '''
    tx = TXComm(HOST_NM, PORT_NUM, debug)
    l=startServer()

    for k in jcd :
        argv = list()
        argv.append(k)
        for i in range(jcd[k]['numofargs']) :
            argv.append(0)
        r=tx.sendGetTransmitter(argv)
        print(argv)
        print("CMD: '{}' got this: '{}'".format(k, r["Command"]["Type"]))

    # one more send to close the listen thread
    endthread = True
    r=tx.sendGetTransmitter(["client_list"])

    l.close()

