""" timestamp.py: A set of methods to generate, reverse and regenerate a time stamp
string based on the defined format string.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
import time

TSFORMATSTR = "%a_%b_%d_%H_%M_%S_%Y"

def createTimeStamp(inTime=None) :
    ''' createTimeStamp -- make a time string from the current time or a passed in integer
    returns: a formated date time string according to TSFORMATSTR.
    '''
    ts = ''
    if inTime is None :
        inTime = time.time()
    ts = time.strftime(TSFORMATSTR, time.localtime(inTime))
    return str(ts)

def getTimeNumberFromStr(timestamp) :
    ''' getTimeNumberFromStr -- takes a time stamp string in the defined format
                                above and reverses that to get the original time
                                integer from the Epoch.
        input:   A string in the format TSFORMATSTR
        returns: An integer in seconds since the Epoch derived from the string.
    '''
    timeval = int(time.time())
    try :
        st = time.strptime(timestamp, TSFORMATSTR)
        timeval  = time.mktime(st)
    except ValueError :
        print("Time Stamp {} does not match the format '{}'".format(timestamp, time.strftime(TSFORMATSTR, time.localtime(timeval))))
    return timeval

