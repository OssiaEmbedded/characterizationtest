""" database.py: A class to connect to a MySql database server. The functions included are
specific to their application at Ossia Inc.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import os
import sys
import json
if sys.version_info[0] == 3 :
    import pymysql  as mydb
else :
    import MySQLdb as mydb

class sqlConnect(object) :
    ''' sqlConnect: An Ossia Inc. test data base connector class
    '''

    def __init__(self, dbname=None, hostname='localhost', debug=False):
        ''' init: intialize the class variables.
        user and password are declared here. A code update is necessary to change
        them.
        '''
        self.sql_conn_kwargs  = dict()

        self.db_name = dbname
        self.sql_conn_kwargs["host"]   = hostname
        self.sql_conn_kwargs["user"]   = 'testdata'
        self.sql_conn_kwargs["passwd"] = 'testdata'
        self.db_cursor = None
        self.db_conn   = None
        self.debug = debug

    def __del__(self) :
        ''' __del__ - Be sure the connection to the server is closed on object scope end.
        '''
        if self.db_conn is not None :
            self._closeDB()

    def _openDB(self) :
        ''' _openDB -- send the MySQL connect string. Set autocommit True and get the
        cursor object.
        '''
        self.db_conn = mydb.connect(**self.sql_conn_kwargs)
        self.db_conn.autocommit = True
        self.db_cursor = self.db_conn.cursor()
        return self.db_conn

    def _closeDB(self) :
        ''' _closeDB -- do a final commit, close the cursor delete it and close the connection.
        Sets the class variables to None.
        '''
        self.db_conn.commit() 
        self.db_cursor.close()
        del self.db_cursor
        self.db_conn.close()
        self.db_conn = None
        self.db_cursor = None

    def setDBData(self, istr) :
        ''' setDBData -- open the MySQL connection and execute the command string.
        Then close the connection.
        '''
        self._openDB()
        val = self.db_cursor.execute(istr)
        self._closeDB()
        return str(val)

    def makeDataBase(self, dbname) :
        ''' makeDataBase -- Given the data base name open a connection, generate a create database
        command string and execute it. The close the db connection.
        '''
        self._openDB()
        rtnval = None
        cmdstr = "SHOW DATABASES LIKE '{}';".format(dbname) 
        rtnval = self.db_cursor.execute(cmdstr)
        if self.debug :
            print("makeDataBase '{}' rtnval={}".format(dbname, rtnval))
        if rtnval == 0 :
            cmdstr = "CREATE DATABASE IF NOT EXISTS {};".format(dbname)
            rtnval = self.db_cursor.execute(cmdstr)
        self.db_name = dbname
        self._closeDB()
        return rtnval

    def _makeColDefString(self, colListOfDefs) :
        """ _makeColDefString Take an input defintion and convert it to a MySql table definition string
        colListOfDefs: a list of lists, tuples or dictionarys. 
                         lists and tuples are name, Col def pairs
                         dicts are single entry name, Col def
        """
        rtnstr = ''
        try :
            for coldef in colListOfDefs :
                if type(coldef) == tuple or type(coldef) == list :
                    k, val = coldef
                elif type(coldef) == dict :
                    k = coldef.keys()[0] # keys returns a list so get the first element
                    val = coldef.get(k, 'VARCHAR(255)')
                else :
                    rtnstr = None
                    break
                if len(rtnstr) > 0 :
                    rtnstr += ", "
                rtnstr += "{} {}".format(k, val)
        except ValueError as e :
            print("Bad column definition list. {}".format(e))
            rtnstr = None
        return rtnstr

    def addRowToCol(self, tablename, colName, dataval) :
        rtnval = None
        self._openDB()
        if self.db_cursor is not None :
            val = "NULL" if dataval is None else "'"+str(dataval)+"'"
            qstrval = "insert into {}.{} ({}) values({});".format(self.db_name, tablename, colName, val)
            if self.debug :
                print("qstrval='{}'\n".format(qstrval))
            rtnval = self.db_cursor.execute(qstrval)
            if self.debug :
                print("rtnval={}\n".format(repr(rtnval)))
        return rtnval

    def updateRowInCol(self, tablename, colName, dataval, rowId) :
        rtnval = None
        self._openDB()
        if self.db_cursor is not None :
            val = "NULL" if dataval is None else dataval
            qstrval = "update {}.{} set {}={} where rowId={};".format(self.db_name, tablename, colName, val, rowId)
            if self.debug :
                print("qstrval='{}'\n".format(qstrval))
            rtnval = self.db_cursor.execute(qstrval)
            if self.debug :
                print("rtnval={}\n".format(repr(rtnval)))
        return rtnval

    def addNewColToTable(self, tablename, colName, colDef) :
        rtnval = None
        self._openDB()
        if self.db_cursor is not None :
            # check if column exists
            qstrval = "show columns from {}.{} like '{}';".format(self.db_name, tablename, colName)
            if self.debug :
                print("qstrval='{}'\n".format(qstrval))
            rtnval = self.db_cursor.execute(qstrval)
            if self.debug :
                print("rtnval={}\n".format(repr(rtnval)))
            if rtnval == 0 :
                qstrval = "alter table {}.{} add {} {};".format(self.db_name, tablename, colName, colDef) 
                if self.debug :
                    print("qstrval='{}'\n".format(qstrval))
                rtnval = self.db_cursor.execute(qstrval)
                if self.debug :
                    print("rtnval={}\n".format(repr(rtnval)))
            self._closeDB()
        return rtnval

    def setNewColData(self, tablename, colName,  datalist) :
        rtnval = None
        self._openDB()
        if self.db_cursor is not None :
            # check if column exists
            qstrval = "select rowId from {}.{};".format(self.db_name, tablename)
            rtnval = self.db_cursor.execute(qstrval)
            if self.debug :
                print("qstrval='{}'\nrtnval={}\n".format(qstrval, repr(rtnval)))
            db_data = self.db_cursor.fetchall()
            if self.debug :
                print("db_data={}\n".format(repr(db_data)))
            dbdatalen = len(db_data)
            dlcnt = len(datalist)
            if self.debug :
                print("dbdatalen={} dlcnt={}\n".format(dbdatalen, dlcnt))
            for i in range(dlcnt) :
                if i < dbdatalen :
                    rowId = db_data[i][0]
                    if self.debug :
                        print("Row{}={} nd={}\n".format(i, rowId, datalist[i]))
                    self.updateRowInCol(tablename, colName, datalist[i], rowId)
                else :
                    if self.debug :
                        print("Datalist[{}]={}\n".format(i, datalist[i]))
                    self.addRowToCol(tablename, colName, datalist[i])
                self.db_conn.commit() 
            self._closeDB()
        return rtnval

    def makeTable(self, tablename, colListOfDefs) :
        ''' makeTable -- given a table name and a col list generate a command string to
        create the table if it does not already exist, with the columns defined in the col list.
        returns None or Zero if exists or other if created.
        '''
        rtnval = None
        col_definition_str = self._makeColDefString(colListOfDefs)
        if self.debug :
            print("maketable col def str '{}'".format(repr(col_definition_str)))
        if col_definition_str is not None and tablename is not None:
            self._openDB()
            cmdstr = "SHOW TABLES FROM {} LIKE '{}';".format(self.db_name, tablename) 
            rtnval = self.db_cursor.execute(cmdstr)
            if self.debug :
                print("makeTable '{}.{}' rtnval={}".format(self.db_name, tablename, rtnval))
            if rtnval == 0 :
                cmdstr = "CREATE TABLE IF NOT EXISTS " + \
                         "{}.{} (rowId INTEGER(32) AUTO_INCREMENT, primary key (rowId), {});".format(self.db_name, tablename, col_definition_str)
                rtnval = self.db_cursor.execute(cmdstr)
            self._closeDB()
        return rtnval

    """
    def makeInsertString(self, colDef, dataDict) :
        tblname = dataDict.get('TBLNAME', None)
        nmdict = dataDict.get('DATA', None)
        if tblname is None or nmdict is None or type(nmdict) is not dict :
            print("Data input configuration error. 'TBLNAME' must exist, 'DATA' must exist and point to a dict")
            return None
        names = ''
        values = ''
        for coldef in colDef :
            if type(coldef) == tuple or type(coldef) == list :
                k, _ = coldef
            elif type(coldef) == dict :
                k = coldef.keys()[0] # keys returns a list so get the first element
            else :
                rtnstr = None
                break
            val = nmdict.get(k, "No Data")
            if len(names) > 0 :
                names += ","
                values += ","
            names += "{}".format(k)
            values += "'{}'".format(val)
        rtnstr = "INSERT INTO {}.{} ({}) values({});".format(self.db_name, tblname, names, values)
        return rtnstr
    """

    def makeInsertStringOL(self, tblname, orderedDataList) :
        ''' makeInsertStringOL -- Using the given ordered list generate an insertion string and return it.
        The ordered list is either a list of lists or list of tuples or tuple of tuples. Each sublist is a
        name, value pair -- colname, value
        '''
        names = ''
        values = ''
        for name, value in orderedDataList :
            if len(names) > 0 :
                names += ","
                values += ","
            names += "{}".format(name)
            values += "'{}'".format(value)
        rtnstr = "INSERT INTO {}.{} ({}) values({});".format(self.db_name, tblname, names, values)
        return rtnstr

    def makeSelectString(self, tblname=None, colname=None, colvalue=None) :
        ''' makeSelectString -- Generate a search string that can contain wild card charaters.
        The database is a class variable for this object instance. The table name and colname are passed in
        as is the search string with wild card characters.
        '''

        if tblname is None or colname is None or colvalue is None :
            print("Data input error to makeSelectString tblname {} colname {} colvalue {}".format(tblname, colname, colvalue))
            rtnstr = None
        else :
            str_ser = ''
            if colvalue[0] == '*' :
                str_ser = '%%'
            elif colvalue[0] != '^' :
                str_ser = '%'+colvalue+'%'
            else :
                str_ser = colvalue+'%'
            rtnstr = "select * from {}.{} where {} like '{}';".format(self.db_name, tblname, colname, str_ser)
        return rtnstr

    def getDBData(self, sstr) :
        ''' getDBData -- execute the passed in search string and fetchall data from the selection.
        '''
        self._openDB()
        strval = None
        try :
            db_data = self.db_cursor.execute(sstr)
            strval = self.db_cursor.fetchall()
        except Exception as e :
            print(repr(e))
            strval = None
        self._closeDB()

        return strval

    """
    def getTableDataDict(self, tblName, colName, colValue="*"):
        nameList = list()
        sstr = "show columns from {}.{};".format(self.db_name, tblName)
        col_names_list = self.getDBData(sstr)
        for col in col_names_list :
            nameList.append(col[0])
        sstr = self.makeSelectString(tblname=tblName, colname=colName, colvalue=colValue)
        data_list = self.getDBData(sstr)
        outDict = dict()
        for dl in data_list :
            for nm, dat in zip(nameList, dl) :
                if outDict.get(nm, None) is None :
                    outDict.update({nm: list()})
                outDict[nm].append(dat)
        return outDict
    """

if __name__ == "__main__" :

    db = sqlConnect(hostname="ossia-build.ossiainc.local", debug=True)
    db.makeDataBase("RoomDB")
    xx = list([9,8,7,4,3,2,1])
    db.addNewColToTable("testdata", "newcol1", "integer(32)")
    db.setNewColData("testdata", "newcol1", xx)
    
