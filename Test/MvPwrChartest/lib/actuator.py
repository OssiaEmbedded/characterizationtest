""" actuator.py: A class to connect to an Ossia linear actuator server. The functions included are
specific to their application at Ossia Inc.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function #, unicode_literals
import socket, sys
from time import sleep, gmtime, strftime

PORT = 7000
COMMANDVAL = "COMMANDVAL"

class Actuator(object) :

    def __init__(self, ipaddr, debug=False) :
        self.ipaddress = socket.gethostbyname(ipaddr)
        #self.debug = debug
        self.debug = True

    def sendGetData(self, sendthis) :
        """ sendGetData -- Send the input string to the ipaddress and port
                           of the actuator.
        """
        mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        try :
            mySocket.connect((self.ipaddress, PORT))
            mySocket.settimeout(1.0)
        except :
            if self.debug :
                print("There was a connect exception ")
            return None

        cnt = 60
        mySocket.send( sendthis + "\r\n" )
        sleep(0.5)
        data = bytearray()
        d = bytearray()
        while cnt > 0 and 'nonblocking' not in sendthis :
            try :
                d = bytearray(mySocket.recv(8192))
                data += d
            except socket.timeout :
                if self.debug :
                    print(cnt)
                cnt -= 1
            if 'SUCCESS' in data :
                break
        if self.debug :
            print(data)
        data = data.lstrip().rstrip()
        mySocket.close()
        mySocket = None

        return data

    def sendCommandString(self, **kwargs) :
        """ sendCommandString -- Create a command string from input args and send it.
                                 input args are arg0=, arg1=, etc.
        """
        if self.debug :
            print("Argument list kwargs={}".format(repr(kwargs)))
        strval = COMMANDVAL
        for i in range(len(kwargs)) :
            key = 'arg'+str(i)
            if kwargs.get(key, None) is not None :
                strval += "/"+kwargs.get(key, "")
            else :
                print("Unknown argument {} kwargs={}".format(key, repr(kwargs)))
        data = self.sendGetData(strval)
        return data

if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        a = Actuator(sys.argv[1], debug=True)

        #str1 = "COMMANDVAL/setMSResolution/16th"
        #str2 = "COMMANDVAL/setStepTime/1500/l"
        strlst = [ "COMMANDVAL/setPosition/0",
                   "COMMANDVAL/setPosition/1180",
                   "COMMANDVAL/setPosition/1500",
                   "COMMANDVAL/setPosition/1000",
                   "COMMANDVAL/setPosition/1200",
                   "COMMANDVAL/setPosition/1900"]

        print(a.sendCommandString(arg0="setMSResolution", arg1='16th', arg2='l'))
        print(a.sendCommandString(arg0="setStepTime", arg1='1500', arg2='l'))

        for i in range(15) :
            try :
                index = i % len(strlst)
                print("Command is '{}'".format(strlst[index]))
                data = a.sendGetData(strlst[index])
                print("DATA = ''{}''".format(str(data)))
                print("sleep {} secs".format(index+2))
                sleep(index + 2)
            except KeyboardInterrupt :
                break

        
   

