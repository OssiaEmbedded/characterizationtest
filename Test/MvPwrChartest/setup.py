from distutils.core import setup

setup(
        #application name
        name='PwrCharTest',
        #Version
        version='X.3.X',
        #Author
        author='Ursus Marsden',
        author_email='ursusm@ossia.com',
        #Packages
        # packages=['PwrChar'],
        # Include Addition files into package
        include_package_data=True,
        #Details
        url="localhost",
        #Licensing
        license='LICENSE.txt',
        description='''Test and libs to access Transmiter, Client, and Signal
        Analyzer data for Transmitter/Client characterization. Sends the data to
        a MySql server on ossia-build''',
        # Dependencies
        install_requires=[
            'json',
            'socket',
            'optparse',
            ],
        )
