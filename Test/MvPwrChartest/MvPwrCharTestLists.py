testlistRotLin=[
{'powerlevel' : 21,'warmup' : 900,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' : 25,'start1' : 1900,'stop1' :  600,'movetime1' : 25,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' :  90,'txanglestop' : -90,'anglemovetime' : 20,'start1' :  600,'stop1' : 1900,'movetime1' : 20,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' : 15,'start1' : 1900,'stop1' :  600,'movetime1' : 15,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' :  90,'txanglestop' : -90,'anglemovetime' : 10,'start1' :  600,'stop1' : 1900,'movetime1' : 10,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' :  5,'start1' : 1900,'stop1' :  600,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
]

testlistRot=[
{'powerlevel' : 21,'warmup' : 900,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' : 25,'start1' : 1900,'stop1' : 1900,'movetime1' : 0,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' :  90,'txanglestop' : -90,'anglemovetime' : 20,'start1' : 1575,'stop1' : 1575,'movetime1' : 0,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' : 15,'start1' : 1250,'stop1' : 1250,'movetime1' : 0,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' :  90,'txanglestop' : -90,'anglemovetime' : 10,'start1' :  925,'stop1' :  925,'movetime1' : 0,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : -90,'txanglestop' :  90,'anglemovetime' :  5,'start1' :  600,'stop1' :  600,'movetime1' : 0,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
]

testlistLin=[
{'powerlevel' : 21,'warmup' : 120,'txanglestart' : 0,'txanglestop' : 0,'anglemovetime' : 0,'start1' : 1900,'stop1' :  600,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : 0,'txanglestop' : 0,'anglemovetime' : 0,'start1' :  600,'stop1' : 1900,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : 0,'txanglestop' : 0,'anglemovetime' : 0,'start1' : 1900,'stop1' :  600,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : 0,'txanglestop' : 0,'anglemovetime' : 0,'start1' :  600,'stop1' : 1900,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
{'powerlevel' : 21,'warmup' :  10,'txanglestart' : 0,'txanglestop' : 0,'anglemovetime' : 0,'start1' : 1900,'stop1' :  600,'movetime1' :  5,'start2' : 0,'stop2' : 0,'movetime2' : 0,'timeperpoint' : 0.03,'timetowait' : 10,},
]

