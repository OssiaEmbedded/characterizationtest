""" SACommunicationBase.py: A base class of sorts to connect to and communicate with
the Agilent A-N9020A Signal Analyzer. It makes use of the SACommStrings dictionary.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals

import socket, sys
from time import sleep, gmtime, strftime
from SA.SACommStrings import SA_CMD_DICT as sacd

class SACom(object) :
    ''' SACom class -- Can be used as a base class but at this time is used as it.
    '''

    def __init__(self, addr='A-N9020A-11404.ossiainc.local', port=5023, debug=False, marker=1) :
        ''' __init__ -- Initialize the class variables
        '''
        self.addr = socket.gethostbyname(addr)
        self.port = port
        self.mySock = None
        self.debug = debug
        self.marker = marker
        self.usesa = True
        if self.debug :
            print("addr={} port={}\n".format(self.addr, self.port))

    def __del__(self) :
        ''' __del__ -- Insure that the connection is closed when the object scope ends.
        '''
        self.saClose()

    def saOpen(self) :
        ''' saOpen -- Open the port connection to the specified Agilent Signal Analizer
        '''
        if self.mySock is None :
            self.mySock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            err = self.mySock.connect_ex((self.addr, self.port))
            if self.debug :
                print("\n\nerr={}, sock={}\n\n".format(err, repr(self.mySock)))
            if err != 0 :
                print("There was an error "+repr(err))
                self.usesa = False
                return False
            self.mySock.settimeout(0.5)
            sleep(0.5)
            # throw away the welcome message
            try :
                data = self.mySock.recv(2048)
                if self.debug :
                    print(data)
            except socket.timeout :
                pass
        return self.usesa

    def saClose(self) :
        ''' saClose -- Close the port connection to the specified Agilent Signal Analizer
        '''
        if self.mySock is not None and self.usesa:
            self.mySock.close()
        self.mySock = None

    def sendgetdata(self, sendthis):
        ''' sendgetdata -- Send the command string to the and receive the response, if any.
        Strip off any un-necessary white space and prompt data.
        '''
        rtndat = None
        if self.debug :
            print("SA Stringdata='{}' usesa={}".format(sendthis, repr(self.usesa)))
        if self.mySock is not None and self.usesa :
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis)
            sleep(0.15)
            try :
                data= self.mySock.recv(256)
            except socket.timeout :
                pass
            sendthis = "\r\n"
            if sys.version_info[0] == 3 :
                sendthis = bytes(sendthis,encoding="utf-8")
            self.mySock.send(sendthis )
            sleep(0.15)
            data = ""
            try :
                d = self.mySock.recv(256)
                while len(d) > 0 :
                    data += d.decode('utf-8')
                    d = self.mySock.recv(256)
            except socket.timeout :
                if len(data) == 0 :
                    print("Socket Timeout without any data recieved")
                pass
            data = data.lstrip()
            l = data.split('SCPI>')
            rtndat = l[0].rstrip()
    
            if self.debug :
                print(len(l))
                print("rtndat='" + repr(rtndat) + "'")
        return rtndat
 

    @property
    def resetsa(self) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resetsa']['get'].format(self.marker))
        return d
    @resetsa.setter
    def resetsa(self, marker) :
        ''' resetsa -- get/set pair to reset the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resetsa']['set'].format(marker))
        self.marker = marker
        return d

    @property
    def selectMarker(self) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['selectMarker']['get'].format(self.marker))
        return d
    @selectMarker.setter
    def selectMarker(self, marker) :
        ''' selectMarker -- get/set pair to select the marker.
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['selectMarker']['set'].format(marker))
        self.marker = marker
        return d

    @property
    def centerFrequency(self) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['centerFrequency']['get'])
        return d
    @centerFrequency.setter
    def centerFrequency(self, value) :
        ''' centerFrequency -- get/set pair to set/get the center frequency of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['centerFrequency']['set'] + str(value))

    @property
    def frequencySpan(self) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['frequencySpan']['get'])
        return d
    @frequencySpan.setter
    def frequencySpan(self, value) :
        ''' frequencySpan -- get/set pair to set/get the frequency span of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['frequencySpan']['set'] + str(value))

    @property
    def videoBW(self) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBW']['get'])
        return d
    @videoBW.setter
    def videoBW(self, value) :
        ''' videoBW -- get/set pair to set/get the video Bandwidth of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['videoBW']['set'] + str(value))

    @property
    def videoBWAuto(self) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['videoBWAuto']['get'])
        return d
    @videoBWAuto.setter
    def videoBWAuto(self, value) :
        ''' videoBWAuto -- get/set pair to set/get the auto video Bandwidth of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['videoBWAuto']['set'] + str(value))

    @property
    def resolutionBWAuto(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBWAuto']['get'])
        return d
    @resolutionBWAuto.setter
    def resolutionBWAuto(self, value) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['resolutionBWAuto']['set'] + str(value))

    @property
    def averageCount(self) :
        ''' resolutionBWAuto -- get/set pair to set/get the auto resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['averageCount']['get'])
        return d
    @averageCount.setter
    def averageCount(self, value) :
        ''' averageCount -- get/set pair to set/get the measure average/holt count number
        '''
        self.saOpen()
        return self.sendgetdata(sacd['averageCount']['set'] + str(value))

    @property
    def resolutionBW(self) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['resolutionBW']['get'])
        return d
    @resolutionBW.setter
    def resolutionBW(self, value) :
        ''' resolutionBW -- get/set pair to set/get the resolution Bandwidth of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['resolutionBW']['set'] + str(value))

    @property
    def referenceLevel(self) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevel']['get'])
        return d
    @referenceLevel.setter
    def referenceLevel(self, value) :
        ''' referenceLevel -- get/set pair to set/get the reference level of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['referenceLevel']['set'] + str(value))

    @property
    def referenceLevelOffset(self) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['referenceLevelOffset']['get'])
        return d
    @referenceLevelOffset.setter
    def referenceLevelOffset(self, value) :
        ''' referenceLevelOffset -- get/set pair to set/get the reference level offset of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['referenceLevelOffset']['set'] + str(value))

    @property
    def displayUnits(self) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        d = self.sendgetdata(sacd['displayUnits']['get'])
        return d
    @displayUnits.setter
    def displayUnits(self, value) :
        ''' displayUnits -- get/set pair to set/get the display units of the SA
        '''
        self.saOpen()
        return self.sendgetdata(sacd['displayUnits']['set'] + str(value))

    @property
    def triggerHold(self) :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerHold']['get'])
        return ampl
    @triggerHold.setter
    def triggerHold(self, value='') :
        ''' triggerHold -- get/set pair to set/get the trigger hold of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['triggerHold']['set'])

    @property
    def triggerClear(self) :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['triggerClear']['get'])
        return ampl
    @triggerClear.setter
    def triggerClear(self, value='') :
        ''' triggerClear -- get/set pair to set/get the trigger clear of the SA
        Allows the current markers X and Y value to be read.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['triggerClear']['set'])

    @property
    def sweepTime(self) :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepTime']['get'])
        return ampl
    @sweepTime.setter
    def sweepTime(self, value='') :
        ''' sweepTime -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        return self.sendgetdata(sacd['sweepTime']['set'] + str(value))

    @property
    def sweepPoints(self) :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['sweepPoints']['get'])
        return ampl
    @sweepPoints.setter
    def sweepPoints(self, value='') :
        ''' sweepPoints -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        return self.sendgetdata(sacd['sweepPoints']['set'] + str(value))

    @property
    def traceData(self) :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        self.saOpen()
        trace = " trace{}".format(self.marker)
        rtnval = self.sendgetdata(sacd['traceData']['get'] + trace)
        return rtnval
    @traceData.setter
    def traceData(self, value='') :
        ''' traceData -- get/set pair to set/get the SA trace data
        '''
        rtnval = ""
        if sacd['traceData']['argc'] > 0 :
           self.saOpen()
           rtnval = self.sendgetdata(sacd['traceData']['set'] + str(value))
        return rtnval

    @property
    def singleContinuousSweep(self) :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['singleContinuousSweep']['get'])
        return ampl
    @singleContinuousSweep.setter
    def singleContinuousSweep(self, value='') :
        ''' singleContinuousSweep -- get/set pair to set/get the SA sweep mode
        '''
        self.saOpen()
        return self.sendgetdata(sacd['singleContinuousSweep']['set'] + str(value))

    @property
    def peakSearch(self) :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker))
        freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker))
        return ampl, freq
    @peakSearch.setter
    def peakSearch(self, value='') :
        ''' peakSearch -- get/set pair to control the SA signal search mode
        '''
        self.saOpen()
        return self.sendgetdata(sacd['peakSearch']['set'].format(self.marker))

    @property
    def continuousPeakSearch(self) :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['continuousPeakSearch']['get'].format(self.marker))
    @continuousPeakSearch.setter
    def continuousPeakSearch(self, value='') :
        ''' continuousPeakSearch -- get/set pair to set/get the SA in continuous peak search.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['continuousPeakSearch']['set'].format(self.marker))

    @property
    def clearHold(self) :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['clearHold']['get'])
        return rtn
    @clearHold.setter
    def clearHold(self, value='') :
        ''' clearHold -- get/set pair to clear a signal hold on the SA.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['clearHold']['set'])

    @property
    def avgHold(self) :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['avgHold']['get'])
        return rtn
    @avgHold.setter
    def avgHold(self, value='') :
        ''' avgHold -- get/set pair to set/get the SA in average signal hold mode.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['avgHold']['set'])

    @property
    def maxHold(self) :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        rtn = self.sendgetdata(sacd['maxHold']['get'])
        return rtn
    @maxHold.setter
    def maxHold(self, value='') :
        ''' maxHold -- get/set pair to set/get the SA in maximum signal hold mode.
        '''
        self.saOpen()
        return self.sendgetdata(sacd['maxHold']['set'])

    def getSAValue(self, name) :
        ''' getSAValue -- Get the value from the SA for the named SA function.
        '''
        rtnval = None
        self.saOpen()
        if name == 'peakSearch' :
            ampl = self.sendgetdata(sacd['peakSearch']['get']['ampl'].format(self.marker))
            freq = self.sendgetdata(sacd['peakSearch']['get']['freq'].format(self.marker))
            rtnval = (ampl, freq)
        else :
            rtnval = self.sendgetdata(sacd[name]['get'].format(self.marker))
        return rtnval

    def setSAValue(self, name, value="") :
        ''' getSAValue -- Set the value of the SA for the named SA function.
        '''
        rtnval = None
        if self.saOpen() :
            argc = sacd[name]['argc']
            if argc > 1 :
                nval = ""
                for v in value :
                    if len(nval) > 0 :
                        nval += ","
                    nval += str(v)
                value = nval
            if argc == 0 : 
                rtnval = self.sendgetdata(sacd[name]['set'].format(self.marker))
            else :
                rtnval = self.sendgetdata(sacd[name]['set'].format(self.marker) + str(value))
        return rtnval

if __name__ == '__main__' :

    from LIB.logResults import LogResults
    # addr='A-N9020A-11404.ossiainc.local', port=5023

    sa = SACom(addr='A-N9020A-11404.ossiainc.local', port=5025, debug=True)
    logger = LogResults("satest", "./", console=True, debug=True)
    logger.initDB("satestDB", hostnm="ossia-build")
    sa.saOpen()
    # specific tests for time sweep mode and data gathering.
    tmp = sa.resetsa
    sa.resolutionBWAuto = "OFF"
    sa.videoBWAuto = "OFF"
    sa.frequencySpan = '0HZ'
    sa.singleContinuousSweep = "ON"
    sa.referenceLevelOffset = "26.4 dB"
    
    secperpoint = 0.03
    st = [30, 20, 10, 5]
    hdr = [{0 : 156, 1 : 554, 2 : 909, 5 :3434},
           {0 : 256, 1 : 654, 2 : 919, 5 :3435},
           {0 : 356, 1 : 754, 2 : 929, 5 :3436},
           {0 : 456, 1 : 854, 2 : 939, 5 :3437}]

    for i in range(len(hdr)) :
        sa.sweepTime = "{}s".format(st[i])
        sa.sweepPoints = int(st[i] / secperpoint) + 1
        sleep(st[i]-0.5)
        data = sa.traceData
        logger.saveTraceData(i, data, hdr[i], "satestdata", True)

    """
    print(sa.resetsa)
    sa.singleContinuousSweep = "ON"
    sa.centerFrequency = '2.45GHZ'
    print("centerFrequency=" + sa.centerFrequency)

    sa.frequencySpan = '2.0MHZ'
    print("frequencySpan=" + sa.frequencySpan)

    sa.sweepPoints = 500
    print("sweep Points {}".format(sa.sweepPoints))

    print("referenceLevel=" + sa.referenceLevel)

    print("resolutionBWAuto=" + sa.resolutionBWAuto)
    sa.resolutionBWAuto = "OFF"
    print("resolutionBWAuto=" + sa.resolutionBWAuto)

    print("selectMarker=" + sa.selectMarker)
    sa.selectMarker = 2
    print("selectMarker=" + sa.selectMarker)

    sa.peakSearch = ''
    print("peakSearch=" + repr(sa.peakSearch))
    print(sa.resetsa)
    print("centerFrequency=" + sa.centerFrequency)
    sa.singleContinuousSweep = "ON"
    """
    sa.saClose()
