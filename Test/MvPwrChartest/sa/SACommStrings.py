""" SACommStrings.py: Ossia specific dictionary of the Agilent A-N9020A Signal Analizer's SCPI
commands that are currently being used or likely to be used.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals

SA_CMD_DICT = {

# get the Identification string of the SA
'ident': {'get' : '*IDN?',
          'set' : '*IDN?',
          'argc': 0},

# reset the SA Should be done at the beginning of each setup cycle
'resetsa': {'get' : '*RST',
            'set' : '*RST',
            'argc': 0},

# turn on/off continuous sweep
'sweepPoints': {'get' : ':SWEep:POINts?',
                'set' : ':SWEep:POINts ',
                'argc': 1},

'traceData' : {'get' : 'TRACe:DATA?',
               'set' : 'trace:data ',
               'argc' : 0},

# turn on/off continuous sweep
'singleContinuousSweep': {'get' : ':INITiate:CONTinuous?',
                          'set' : ':INITiate:CONTinuous ',
                          'argc': 1},
# used to get/set the Uints that the y axis is displayed in
'displayUnits': {'get' : ':UNIT:POW?',
                 'set' : ':UNIT:POW ',
                 'argc': 1},

# :DISPlay:WINDow[1]:TRACe:Y[:SCALe]:RLEVel <real>
# :DISPlay:WINDow[1]:TRACe:Y[:SCALe]:RLEVel?

'referenceLevel': {'get': ':DISPlay:WINDow1:TRACe:Y:SCALe:RLEVel?',
                   'set': ':DISPlay:WINDow1:TRACe:Y:SCALe:RLEVel ',
                   'argc': 1},
'referenceLevelOffset': {'get': ':DISPlay:WINDow1:TRACe:Y:SCALe:RLEVel:OFFSet?',
                         'set': ':DISPlay:WINDow1:TRACe:Y:SCALe:RLEVel:OFFSet ',
                         'argc': 1},



#[:SENSe]:FREQuency:CENTer <freq>
#[:SENSe]:FREQuency:CENTer?
#Example FREQ:CENT 50 MHz
#FREQ:CENT UP changes the center frequency to 150 MHz if you use
#FREQ:CENT:STEP 100 MHz to set the center frequency step size to 100 MHz
#FREQ:CENT?
'centerFrequency': {'get': ':SENSe:FREQuency:CENTer?',
                    'set': ':SENSe:FREQuency:CENTer ',
                    'argc': 1},

#[:SENSe]:FREQuency:SPAN <freq>
#[:SENSe]:FREQuency:SPAN?
#Example FREQ:SPAN 2GHz sets the span to 2GHz
#FREQ:SPAN 0 Hz Sets the span to 0 Hz and puts the instrument in Zero Span
'frequencySpan': {'get': ':SENSe:FREQuency:SPAN?',
                  'set': ':SENSe:FREQuency:SPAN ',
                  'argc': 1},

#[:SENSe]:SWEep:TIME <time>
#[:SENSe]:SWEep:TIME?
#[:SENSe]:SWEep:TIME:AUTO OFF|ON|0|1
#[:SENSe]:SWEep:TIME:AUTO?
'sweepTime': {'get': ':SENSe:SWEep:TIME?',
              'set': ':SENSe:SWEep:TIME ',
              'argc': 1},
'sweepTimeAuto': {'get': ':SENSe:SWEep:TIME:AUTO?',
                  'set': ':SENSe:SWEep:TIME:AUTO ',
                  'argc': 1},


#[:SENSe]:BANDwidth|BWIDth:VIDeo <freq>
'videoBW': {'get': ':SENSe:BANDwidth:VIDeo?',
            'set': ':SENSe:BANDwidth:VIDeo ',
            'argc': 1},
'videoBWAuto': {'get': ':SENSe:BANDwidth:VIDeo:AUTO?',
                'set': ':SENSe:BANDwidth:VIDeo:AUTO ',
                'argc': 1},

#[:SENSe]:BANDwidth|BWIDth[:RESolution] <freq>
#[:SENSe]:BANDwidth|BWIDth[:RESolution]?
#[:SENSe]:BANDwidth|BWIDth[:RESolution]:AUTO OFF|ON|0|1
#BAND 1 KHZ
#BAND?
#BWID:AUTO ON
#BWID:AUTO?
'resolutionBW': {'get': ':SENSe:BANDwidth:RESolution?',
                 'set': ':SENSe:BANDwidth:RESolution ',
                 'argc': 1},
'resolutionBWAuto': {'get': ':SENSe:BANDwidth:RESolution:AUTO?',
                     'set': ':SENSe:BANDwidth:RESolution:AUTO ',
                     'argc': 1},

'averageCount': {'get': ':SENSe:AVERage:COUNt?',
                 'set': ':SENSe:AVERage:COUNT ',
                 'argc': 1},

#Max Hold makes the trace represent the maximum data value on a point-by-point basis
# of the new trace data and the previous trace data. Max hold enables a simplified
# (albeit lower-speed) measurement of the TOI of a pulsed signal.
#Pressing the Max Hold key, or sending the :TRAC:TOI:TYPE MAXH command, sets the trace
# type to Max Hold. The max hold trace uses peak detection; in zero-span measurement the
# metrics are calculated using the peak value of the zero-span data.
#When in Max Hold, if a measurement-related instrument setting is changed, the Max Hold
# sequence restarts and a new sweep is initiated but the trace is not cleared.
#Key Path Trace/Detector
#Example TRAC:TOI:TYPE MAXHold
#sets the trace to max hold Couplings Selects the peak detector, and uses the peak value for zero-span measurements.
#Initial S/W Revision Prior to A.02.00
'maxHold': {'get': 'TRAC1:TYPE?',
            'set': 'TRAC1:TYPE MAXHold',
            'argc': 0},
'avgHold': {'get': 'TRAC1:TYPE?',
            'set': 'TRAC1:TYPE AVER',
            'argc': 0},
'clearHold': {'get': 'TRAC1:TYPE?',
            'set': 'TRAC1:TYPE WRIT',
            'argc': 0},

'triggerHold': {'get': 'trig:sour?',
                'set': 'trig:sour ext1',
                'argc': 0},
'triggerClear': {'get': 'trig:sour?',
                 'set': 'trig:sour imm',
                 'argc': 0},
#Pressing the Peak Search key displays the Peak Search menu and places the selected
# marker on the trace point with the maximum y-axis value for that markers trace.
#The Peak Search features allow you to define specific search criteria to determine
#which signals can be considered peaks, excluding unwanted signals from the search. For
# all Peak Search functions, if you are in the Trace Zoom View of the Swept SA 
# measurement, and the bottom window is selected, the search function will operate
# ONLY within that window. This allows you to perform a Peak Search over a specified,
# limited frequency range, while still viewing the larger frequency range in the top window.

#See "More Information" on page 704.
#Key Path Front-panel key
#Remote Command :CALCulate:MARKer[1]|2|...|12:MAXimum
#Example CALC:MARK2:MAX performs a peak search using marker 2.
#CALC:MARK2:Y? queries the marker amplitude (Y-axis) value for marker 2.
#CALC:MARK2:X? queries the marker frequency or time (X-axis) value for marker 2.
#SYST:ERR?can be used to query the errors to determine if a peak is found. The
#message No peak found will be returned after an unsuccessful search.
#Notes Sending this command selects the subopcoded marker.
#Initial S/W Revision Prior to A.02.00
'peakSearch': {'get': {'ampl': ':CALCulate:MARKer{}:Y?', 'freq': ':CALCulate:MARKer{}:X?'},
               'set': ':CALCulate:MARKer{}:MAXimum ',
               'argc': 0},

#:CALCulate:MARKer[1]|2|...|12:CPSearch[:STATe] ON|OFF|1|0
#:CALCulate:MARKer[1]|2|...|12:CPSearch[:STATe]?
'continuousPeakSearch': {'get': ':CALCulate:MARKer{}:CPSearch:STATe?',
                         'set': ':CALCulate:MARKer{}:CPSearch:STATe ',
                         'argc': 1},

'selectMarker' : {'get': ':CALCulate:MARKer{}:MODE?',
                  'set': ':CALCulate:MARKer{}:MODE POS',
                  'argc': 0},
}

if __name__ == "__main__" :
    print(SA_CMD_DICT.keys())
