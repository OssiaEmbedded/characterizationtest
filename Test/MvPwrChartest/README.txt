Usage: PwrCharTest.py [options]

Options:
  -h, --help            show this help message and exit
  -p SAPORT, --saport=SAPORT
                        Signal Analyzer telnet port number.
  -i SAIPADDR, --saip=SAIPADDR
                        Signal Analyzer network address
  --txport=TXPORT       Transmitter telnet port number.
  --txip=TXIPADDR       Transmitter network addresst.
  --actip=ACTUATOR      Linear Actuator network addresst.
  -l LOGCOUNT, --logcount=LOGCOUNT
                        Number of logging iterations if int or loggin time if
                        float.
  -c CLIENTID, --clientid=CLIENTID
                        Device/client ID to use for characterization.
  --minlinkq=MINLINKQ   Minimum link quality to validate a client.
  --cableloss=CABLELOSS
                        Cable loss positive dB. Added to the 20 dB of
                        Connector loss
  --powerlevel=POWERLEVEL
                        Transmitter Power Level
  -w WARMUP, --warmup=WARMUP
                        Number of seconds at the start of the test to let the
                        transmitter warm up.
  --txangle=TXANGLE     Angle from the perpendicular TX to Client.
  --distance=DISTANCE   The Z, X, Y distance from the TX to Client.
  --waitfordata=WAITFORDATA
                        Number of seconds to wait for the client to average
                        data.
  --calibrate=CALIBRATE
                        Number data points between calibrations.
  -f LOGFILE, --logfile=LOGFILE
                        Log file name. If {} is added in the file name, a
                        timestamp will be added.
  --directory=DIRECTORY
                        Log file directory.
  --noconsole           Turn off printing to console
  --interact            Turn on interacting with the program
  --average             Turn on average hold SA data
  --logtodb             Log data to the DataBase at ossia-build
  -d, --debug           print debug info.
