''' MvPwrCharTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 1.0.0
Date : Jun 09 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")
import json, re
from time import sleep, time
from optparse import OptionParser, Option, OptionValueError
from math import sin, pi

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from LIB.actuator import Actuator
from copy import copy

def check_bool(option, opt, value) :
    try :
        return bool(value)
    except ValueError :
        raise OptionValueError("option %s: invalid bool value: %r %s" % (opt,value,repr(option)))

class myOption (Option) :
    TYPES = Option.TYPES + ('bool',)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER['bool'] = check_bool

ROTARYON = '10.10.0.230'

NONBLOCKING = "nonblocking"

TESTDBNAME='MvPwrCharTest'

TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME = "SaTable"
TXTBLNAME="MoveData"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = 'ossia-build'
TXDATAMOVETABLENAME = "MoveTable"

# data base definition constants
TXCMS = [
         [TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATAMOVETABLENAME, "%s",'VARCHAR(20)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(20)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(20)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["txId", "%s", 'VARCHAR(25)'],
         ["PmusEnabled","%s", 'VARCHAR(50)'],
         ["RecsEnabled","%s", 'VARCHAR(50)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(600)']]

SA_SETUP_LIST = [
                 ['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['frequencySpan', "100 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['sweepPoints', None, 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['peakSearch', '', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['maxHold', '', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['averageCount', '1', 'VARCHAR(3)'],
                 ['referenceLevel', '0dbm', 'VARCHAR(35)'],
                 ['referenceLevelOffset', None, 'VARCHAR(35)'],
                 ['singleContinuousSweep', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "0 Hz", None]] # sa_span="100.0 MHz"

CLIENTCMS = [
             ["clientId", "%s", 'VARCHAR(25)'],
             ["netCurrent", "%3.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%3.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%5f", 'VARCHAR(10)'],
             ["peakPower", "%5f", 'VARCHAR(10)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["MaxRSSI_1", "%3.2f", 'VARCHAR(10)'],
             ["MaxRSSI_2", "%3.2f", 'VARCHAR(10)'],
             ["MaxRSSI_3", "%3.2f", 'VARCHAR(10)'],
             ["MaxRSSI_4", "%3.2f", 'VARCHAR(10)'],
             ["MaxmRSSI_1", "%d",   'VARCHAR(10)'],
             ["MaxmRSSI_2", "%d",   'VARCHAR(10)'],
             ["MaxmRSSI_3", "%d",   'VARCHAR(10)'],
             ["MaxmRSSI_4", "%d",   'VARCHAR(10)'],
             ["PowerLevel", "%d", 'VARCHAR(5)'],
             ["WarmupTime", "%d",'VARCHAR(7)'],
             ["timetaken", "%7.4f", 'VARCHAR(25)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["AvgRSSI_1", "%3.2f", 'VARCHAR(10)'],
             ["AvgRSSI_2", "%3.2f", 'VARCHAR(10)'],
             ["AvgRSSI_3", "%3.2f", 'VARCHAR(10)'],
             ["AvgRSSI_4", "%3.2f", 'VARCHAR(10)'],
             ["AvgmRSSI_1", "%d",   'VARCHAR(10)'],
             ["AvgmRSSI_2", "%d",   'VARCHAR(10)'],
             ["AvgmRSSI_3", "%d",   'VARCHAR(10)'],
             ["AvgmRSSI_4", "%d",   'VARCHAR(10)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["Model", "%d",        'VARCHAR(10)'],
             ["Version", "%d", 'VARCHAR(6)'],
             ["TPSMissed", "%d",    'VARCHAR(6)'],
             ["TimeToWait", "%d",    'VARCHAR(6)']]

sTXIP='10.10.0.226'
sTXPORT='50000'
sSAIP='A-N9020A-11404.ossiainc.local'
sSAPORT='5023'


def makeAngleDistance(angle, zVal, offset) :
    """ makeAngleDistance -- Take the angle from a perpendicular line to the transmiter
                             and the distance on the hypotinuse of the angle
                             and generate using the offset a perpendicular distance from the
                             transmitters perpendicular line to the hypotinuse (sine of the angle)
        returns a list of distances
    """
    try :
        rads = float(angle)/360.0 * 2.0 * pi
        x = int(zVal) * sin(rads) + int(offset)
    except ValueError as e :
        print("ValueError in makeAngleDistance {}".format(repr(e)))
        x = 0
    return int(round(x))


def parseDistance(inStr) :
    """ parseDistance -- parses mut fields and returns a list of distances
                         range;step -- looks for a - between two numbers and a semicolon for the step
                         comma separated list of positions
    """
    rtnlist = list()
    if "-" in inStr and ";" in inStr : # do range parsing
        try :
            firstsplit = inStr.split(';')
            step       = int(firstsplit[1])
            secsplit   = firstsplit[0].split('-')
            start      = int(secsplit[0])
            end        = int(secsplit[1])
            expression = " >= "
            if (end - start) < 0 :
                expression = " <= "
            elif (end - start) == 0 :
                return rtnlist
            if eval("{}{}{}".format((end - start), expression,  step)) :
                while eval("{}{}{}".format(end, expression, start)) :
                    rtnlist.append(start)
                    start += step
        except ValueError :
            pass
    else : # do comma parsing
        rtnlist = inStr.split(',')
    return rtnlist

def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            print("Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            print(msg)
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getVersions(tx) :
    argv = ["versions"]
    v = tx.sendGetTransmitter(argv)
    vers = v['Result']
    nvers = dict()
    for k in vers :
        nk=''.join(part[:-1] if part[-1] in set('aeiou') else part for part in re.compile('[A-Z][a-z]{0,3}').findall(k))
        kvers = vers[k]
        if isinstance(kvers, dict) :
            nkvers = dict()
            for dk in kvers :
                ndk=''.join(part[:-1] if part[-1] in set('aeiou') else part for part in re.compile('[A-Z][a-z]{0,3}').findall(dk))
                nkvers.update({ndk: kvers[dk]})
            kvers = nkvers
        nvers.update({nk: kvers})
    outvers = json.dumps(nvers).replace(' ','').replace("u'","'")
    return outvers

def getOnChannels(tx) :
    #
    ch_on = 0
    argv = ["info"]
    v = tx.sendGetTransmitter(argv)
    chList = v['Result']['AMBS']
    outList = list()
    for chan in chList :
        try :
            chOn = 1 if str(chan['Status']) == 'ON' else 0
            chNum = int(chan['Channel Number'])
            rev = int(chan['AMB Revision'])
            # make the def smaller, take out all the extra unneeded characters
            outList.append({"s": chOn, "c" : chNum, 'r': rev})
        except ValueError as ve :
            print("Chan Info ValueError {} -- {}".format(v['Result']['Status'], ve))
            chOn =  0
        # chOn will be either 1 or 0, shifting a zero is benign.
        ch_on |= (chOn << chNum)
    # take out the spaces that the json.dumps puts in
    outStr = json.dumps(outList).replace(' ','').replace("u'","'")
    chStr = "0x%08X" % ch_on
    return chStr, outStr

def stopStartChargingList(tx, ss, clientList) :
    # ss must be either 'stop' or 'start' for this to work
    rtnval = False
    cmd = "{}_charging".format(ss)
    failword = "{}CHARGING".format("NOT_" if ss == "stop" else '')
    for client in clientList :
        sClientId = client[0]
        argv = [cmd, sClientId]
        try :
            v = tx.sendGetTransmitter(argv)
            if v['Result']['Devices'][0]['Status'] != failword :
                print("Charging did not '{}': {}".format(ss, repr(v['Result']["Devices"][0]['Status'])))
                rtnval = True
        except :
            rtnval = True
    return rtnval

def transmitterCalibrate(tx, clientList, prnt=True) :
    rtnval = False #stopStartChargingList(tx, 'stop', clientList)

    if not rtnval :
        argv = ['calibrate']
        v = tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Calibrate")
            rtnval = True

    sleep(8)
    if not rtnval :
        rtnval = False #stopStartChargingList(tx, 'start', clientList)

    if prnt :
        print("Calibration {} clientlist {}".format("Failed" if rtnval else "Done", repr(clientList)))

    return rtnval


def runthemove(sa, tx, clientList, testlist, logger, options) :

    finished = False
    nn = 0
    TraceLenIdx   = nn; nn +=1
    TraceTimeIdx  = nn; nn +=1
    TimePerPntIdx = nn; nn +=1
    Lin1StartIdx  = nn; nn +=1
    Lin1StopIdx   = nn; nn +=1
    Lin1TimeIdx   = nn; nn +=1
    Lin2StartIdx  = nn; nn +=1
    Lin2StopIdx   = nn; nn +=1
    Lin2TimeIdx   = nn; nn +=1
    RotStartIdx   = nn; nn +=1
    RotStopIdx    = nn; nn +=1
    RotTimeIdx    = nn; nn +=1
    rtndict = dict({TraceLenIdx  : None, TraceTimeIdx : None, TimePerPntIdx : None,
                    Lin1StartIdx : None, Lin1StopIdx  : None, Lin1TimeIdx   : None,
                    Lin2StartIdx : None, Lin2StopIdx  : None, Lin2TimeIdx   : None,
                    RotStartIdx  : None, RotStopIdx   : None, RotTimeIdx    : None})

    try :
        zDist1Start   = int(testlist.get('start1', 0))
        zDist1Stop    = int(testlist.get('stop1', 0))
        zDist2Start   = int(testlist.get('start2', 0))
        zDist2Stop    = int(testlist.get('stop2', 0))
        txAngleStart  = float(testlist.get('txanglestart', 0.0))
        txAngleStop   = float(testlist.get('txanglestop', 0.0))
        moveTime1     = float(testlist.get('movetime1', 0.0))
        moveTime2     = float(testlist.get('movetime2', 0.0))
        anglemoveTime = float(testlist.get('anglemovetime', 0.0))
        timePerPoint  = float(testlist.get('timeperpoint', 0.06))
    except ValueError :
        return True # means we're finished.

    iterateDur  = max(moveTime1, moveTime2)
    iterateDur  = max(anglemoveTime, iterateDur)
    timetowait  = min(iterateDur, float(testlist.get('timetowait', 12))) * 0.9
    sp = ((iterateDur / timePerPoint) / 10) + 0.5
    sweepPoints = min((int(sp) * 10), 5000)
    rtndict[TraceLenIdx]   = sweepPoints
    rtndict[TraceTimeIdx]  = iterateDur
    rtndict[TimePerPntIdx] = iterateDur / sweepPoints

    # go to the initial position
    realZ1Start = zDist1Start
    realZ2Start = zDist2Start
    realTxAngle = txAngleStart
    act2 = None
    actuatorAry = options.actuator.split(",")
    act1 = Actuator(actuatorAry[0], debug=options.debug)
    act1.sendCommandString(arg0='setPosition', arg1=str(zDist1Start), arg2='l')
    rtnlst = act1.sendCommandString(arg0='getPosition', arg1='l').split()
    for val in rtnlst :
        try :
            realZ1Start = int(val)
            rtndict[Lin1StartIdx] = realZ1Start
            break
        except ValueError :
            pass

    if len(actuatorAry) > 1 :
        act2 = Actuator(actuatorAry[1], debug=options.debug)
        act2.sendCommandString(arg0='setPosition', arg1=str(zDist2Start), arg2='l')
        rtnlst = act2.sendCommandString(arg0='getPosition', arg1='l').split()
        for val in rtnlst :
            try :
                realZ2Start = int(val)
                rtndict[Lin2StartIdx] = realZ2Start
                break
            except ValueError :
                pass

    rotact = act2
    if actuatorAry[0] == ROTARYON :
        rotact = act1

    if rotact :
        rotact.sendCommandString(arg0='setPosition', arg1=str(txAngleStart), arg2='r')
        rtnlst = rotact.sendCommandString(arg0='getPosition', arg1='r').split()
        for val in rtnlst :
            try :
                realTxAngle = float(val)
                rtndict[RotStartIdx] = realTxAngle
                break
            except ValueError :
                pass

    if stopStartChargingList(tx, 'start', clientList) :
        return True

    # wait for the warmup period
    warmup = int(testlist.get('warmup', 30))
    reportWait(warmup, prnt=options.console)

    transmitterCalibrate(tx, clientList, prnt=options.console)

    # set up the SA for the next data acquisition
    sa.sweepTime = iterateDur
    sa.sweepPoints = sweepPoints
    sa.clearHold = None
    sa.maxHold = None
    sa.singleContinuousSweep = "ON"
    sa.singleContinuousSweep = "OFF"

    if act1 and zDist1Start != zDist1Stop :
        byliststr = "{},{},{}".format(zDist1Stop,0,moveTime1)
        act1.sendCommandString(arg0='setPositionByList', arg1=byliststr, arg2='l', arg3=NONBLOCKING)
        rtndict[Lin1StopIdx] = zDist1Stop
        rtndict[Lin1TimeIdx] = moveTime1
    if act2 and zDist2Start != zDist2Stop :
        byliststr = "{},{},{}".format(zDist2Stop,0,moveTime2)
        act2.sendCommandString(arg0='setPositionByList', arg1=byliststr, arg2='l', arg3=NONBLOCKING)
        rtndict[Lin2StopIdx] = zDist2Stop
        rtndict[Lin2TimeIdx] = moveTime2
    if rotact and txAngleStart != txAngleStop :
        byliststr = "{},{},{}".format(txAngleStop,0,anglemoveTime)
        rotact.sendCommandString(arg0='setPositionByList', arg1=byliststr, arg2='r', arg3=NONBLOCKING)
        rtndict[RotStopIdx] = txAngleStop
        rtndict[RotTimeIdx] = anglemoveTime

    print("\n#$#$#$#$#$#$#$ going to collect client data\n")
    cnt = 0
    iterateTime = time()
    while (time() - iterateTime) < iterateDur and not finished :

        sleep(timetowait)

        if options.console :
            print("{} ".format(cnt), end='')
            cnt += 1

        # get system temperature datum
        argv = ['get_system_temp']
        v = tx.sendGetTransmitter(argv)
        systemTemp = 0
        if jcs.checkForSuccess(v) :
            print("System Temperature not valid")
        else :
            systemTemp = jcs.getSystemTemp(v)

        # get charging client details
        for client in clientList :
            sClientId = client[0]
            argv = ['client_detail', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v, ckVal="CHARGING") and jcs.checkForSuccess(v, ckVal="LIMITED_CHARGING") :
                print("Client Detail request shows client {} not charging {}".format(sClientId, repr(v['Result']['Status'])))
                #break
            #if not jcs.checkForSuccess(v, ckVal="LIMITED_CHARGING") :
            #    vv = tx.sendGetTransmitter(['send_disc'])
            #    if jcs.checkForSuccess(vv) :
            #        print("Send Discovery request failed" + repr(vv['Result']['Status']))

            version          = jcs.getClientVersion(v)
            netCurrent       = jcs.getClientNetCurrent(v)
            queryFailedCount = jcs.getClientQueryFailed(v)
            linkQuality      = jcs.getClientLinkQuality(v)
            batteryLevel     = jcs.getBatteryLevel(v)
            Model            = jcs.getDeviceModel(v)
            DeviceStatus     = jcs.getDeviceStatus(v)
            TPSMissed        = jcs.getTPSMissed(v)
            averagePower     = jcs.getAveragePower(v)
            peakPower        = jcs.getPeakPower(v)

        # get charging client data
            argv = ['client_data', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Client Data request Failed: " + repr(v['Result']['Status']))
                #break
            batteryVoltage = jcs.getClientBatteryVoltage(v)
            MaxmRSSI_1, MaxRSSI_1, AvgmRSSI_1, AvgRSSI_1 =  jcs.getClientRFPower(v, 1)
            MaxmRSSI_2, MaxRSSI_2, AvgmRSSI_2, AvgRSSI_2 =  jcs.getClientRFPower(v, 2)
            MaxmRSSI_3, MaxRSSI_3, AvgmRSSI_3, AvgRSSI_3 =  jcs.getClientRFPower(v, 3)
            MaxmRSSI_4, MaxRSSI_4, AvgmRSSI_4, AvgRSSI_4 =  jcs.getClientRFPower(v, 4)


            logger.logAddValue("PowerLevel", testlist['powerlevel'])
            logger.logAddValue("WarmupTime", testlist['warmup'])
            # add the time entry
            logger.logAddValue('timetaken', (time() - iterateTime))
            # add the client ID to the log list
            logger.logAddValue('clientId', sClientId)
            # add version to the log list
            logger.logAddValue('Version', version)
            # add linkQuality to the log list
            logger.logAddValue('linkQuality', linkQuality)
            # add netcurrent to the log list
            logger.logAddValue('netCurrent', netCurrent)
            # add batteryVoltage to the log list
            logger.logAddValue('batteryVoltage', batteryVoltage)
            # add system Temp to the log list
            logger.logAddValue('systemTemp', systemTemp)
            # add batteryLevel to the log list
            logger.logAddValue('batteryLevel', batteryLevel)
            # add averagePower to the log list
            logger.logAddValue('averagePower', averagePower)
            # add peakPower to the log list
            logger.logAddValue('peakPower', peakPower)
            # add queryFaileCount to the log list
            logger.logAddValue('queryFailedCount', queryFailedCount)
            # add Distances to the log list
            # add RSSI values to the log list
            logger.logAddValue('MaxmRSSI_1', MaxmRSSI_1)
            logger.logAddValue('MaxRSSI_1', MaxRSSI_1)
            logger.logAddValue('MaxmRSSI_2', MaxmRSSI_2)
            logger.logAddValue('MaxRSSI_2', MaxRSSI_2)
            logger.logAddValue('MaxmRSSI_3', MaxmRSSI_3)
            logger.logAddValue('MaxRSSI_3', MaxRSSI_3)
            logger.logAddValue('MaxmRSSI_4', MaxmRSSI_4)
            logger.logAddValue('MaxRSSI_4', MaxRSSI_4)
            logger.logAddValue('AvgmRSSI_1', AvgmRSSI_1)
            logger.logAddValue('AvgRSSI_1', AvgRSSI_1)
            logger.logAddValue('AvgmRSSI_2', AvgmRSSI_2)
            logger.logAddValue('AvgRSSI_2', AvgRSSI_2)
            logger.logAddValue('AvgmRSSI_3', AvgmRSSI_3)
            logger.logAddValue('AvgRSSI_3', AvgRSSI_3)
            logger.logAddValue('AvgmRSSI_4', AvgmRSSI_4)
            logger.logAddValue('AvgRSSI_4', AvgRSSI_4)
            logger.logAddValue('Model', Model)
            logger.logAddValue('DeviceStatus', DeviceStatus)
            logger.logAddValue('TPSMissed', TPSMissed)
            logger.logAddValue('TimeToWait', timetowait)
            # add SA values to the log list
            # add TimeStamp value to the log list
            if logger.logResults() :
                print("Couldn't log values for Client {}.".format(sClientId))
                finished = True
                break

            tmp = max(MaxRSSI_1, MaxRSSI_2)
            tmp = max(tmp, MaxRSSI_3)
            tmp = max(tmp, MaxRSSI_4)
            if tmp >= options.maxrssi or peakPower >= options.maxrssi :
                print("MaxRSSI value exceeded {} peakPower {}\n".format(tmp, peakPower))
                finished = True

    return (finished, rtndict)

    # when the "while" is done we're finished

def main(tstList, options) :

    clientList = list()

    # init  SA Communications
    sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)

    # init the TX communication object
    tx=TXComm(options.txipaddr, int(options.txport), options.debug)

    # If no client id specified then look for one with the Transmitter
    clientId = options.clientid
    errorCondition = False

    argv = ["get_charger_id"]
    v = tx.sendGetTransmitter(argv)
    txId = jcs.getTxId(v)
    if clientId is None :
        minLinkQ = int(options.minlinkq)
        # none specified so find one
        argv = ["client_list"]
        v = tx.sendGetTransmitter(argv)
        availableClients = v['Result']['Clients']
        l = len(availableClients)
        # if one or more clients found then choose the first three with the highest LinkQuality values greater than the minimum
        errorCondition = True
        if l > 0 :
            for aclient in availableClients :
                lnkq = 0
                sClientId = ''
                try :
                    lnkq = int(aclient['LinkQuality'])
                    sClientId = str(aclient['Client ID'])
                except ValueError as ve :
                    print("LinkQuality ValueError {} -- {}".format(repr(aclient), ve))
                if minLinkQ < lnkq :
                    clientList.append([sClientId, lnkq])
                    if len(clientList) == 3:
                        break
                if options.debug :
                    print("found client {} lnkq {}".format(sClientId, lnkq))
            # report something
            if len(clientList) > 0 :
                errorCondition = False
                if options.console :
                    for aclient in clientList :
                        print("Using client {} LnkQ {}".format(aclient['Client ID'], aclient['LinkQuality']))
            else :
                print("No clients found that meet the criteria of > {}".format(minLinkQ))
                for aclient in availableClients :
                    print("Client {} LinkQuality {}".format(aclient['Client ID'], aclient['LinkQuality']))
        else :
            # none found and none specified
            print("No clients found")
        # a client was specified so lets just use it
    else : # make a client list
        clst = clientId.split(',')
        for cl in clst :
            clientList.append([cl, "85"])

    if not errorCondition :
        # first register the client

        print("The Client list: {}".format(repr(clientList)))

        for client in clientList :
            clientId = client[0]
            try :
                sClientId = str(clientId)
                argv = ['register_client', sClientId]
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Register client {} failed".format(sClientId))
                    return
                # configure the client
                argv = ['client_config', sClientId, '0x6']
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Client config failed")
                    return
                # check for a non zero Short ID
                argv = ['client_detail', sClientId]
                v = tx.sendGetTransmitter(argv)
                shortID = jcs.getClientShortID(v)
                if shortID == "0x0000" :
                    print("Short ID shows device is not registered: " + shortID)
                    return
                #
            except ValueError as ve :
                print("ClientId ValueError {} -- {}".format(clientId, ve))
                return

        ## get the Transmiter data values for logging.

        # first get the Transmitter data that we want to log
        # This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp

        logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
        logger.initFile(TXCMS)

        if options.logtodb :
            logger.initDB(options.dbname, hostnm=DEFAULTDBSERVER)
            logger.initDBTable(options.tblname)
        # add options Dict to log list
        val =  repr(options.__dict__).replace("'","").replace('"',"")
        logger.logAddValue("options", val)

        # add txId to log list
        logger.logAddValue("txId", txId)
        logger.logAddValue("AntennaType",options.antennatype)
        logger.logAddValue("CfgComment",options.logcomments)
        # add OnChannels to log list
        OnChannels, ChanList = getOnChannels(tx)
        VerStr = getVersions(tx)
        logger.logAddValue("OnChannels", OnChannels)
        v = tx.sendGetTransmitter(['get_good_channels'])
        logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
        logger.logAddValue("Versions", str(VerStr))
        logger.logAddValue("AmbRev", str(ChanList))
        logger.logAddValue("LogFileName", logger.filename)
        tableTS = str(int(time()))
        CLIENTTABLENAME = "client_" + tableTS
        logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
        SATABLENAME = "sa_" + tableTS
        logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
        MOVETABLENAME = "move_" + tableTS
        logger.logAddValue(TXDATAMOVETABLENAME, MOVETABLENAME)
        logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)
        if logger.logResults() :
            print("Couldn't log values for Transmitter.")
            return


        # make the logging list
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
            # the the SA
            if cmd_data[1] is not None :
                sa.setSAValue(cmd_data[0], cmd_data[1])
        sa.referenceLevelOffset = 20.0 + options.cableloss

        # init the logging list
        logger.initFile(sacms,how='a')
        if options.logtodb :
            logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                logger.logAddValue(cmd_data[0], cmd_data[1])
        logger.logAddValue("referenceLevelOffset", str(20.0 + options.cableloss))
        if logger.logResults() :
            print("Couldn't log values for SA.")
            return

        clientcms = CLIENTCMS
        # setup the logging file
        if options.debug :
            print("FNAME={}".format(logger.filename))
            print("COLFMT={}".format(logger.colformat))

        logger.initFile(clientcms,how='a')
        if options.logtodb :
            logger.initDBTable(CLIENTTABLENAME)
        # the client table is the last one inited so it will be the one where data is logged
        # it's data names will be expected to be used.
        # a special function in the logger will take care of the move table.

        powerLevel = 0
        finished = False
        i = 222
        for i in range(len(tstList)) :
            if finished :
                break
            itTstDict = tstList[i]

            # if the rail is at an angle then add that to this check.
            try :
                pwrLvl = int(itTstDict.get('powerlevel', 17))
            except ValueError as e :
                print("ValueError converting powerlevel {}\n".format(repr(e)))
                pwrLvl = 17
            pwrLvl = max(pwrLvl, 17)
            pwrLvl = min(pwrLvl, 21)
            if powerLevel != pwrLvl :
                powerLevel = pwrLvl
                argv = ['set_power_level', powerLevel]
                v = tx.sendGetTransmitter(argv)
                if jcs.checkForSuccess(v) :
                    print("Set Power Level failed\n")
                    return
                argv = ['get_power_level']
                v = tx.sendGetTransmitter(argv)
                pl =  jcs.getTransmitterPowerLevel(v)
                if pl != powerLevel :
                    print("Power level incorrect get={} vs set={}\n".format(pl, powerLevel))
                    powerLevel = pl

            # this function also logs all the client type data to the file and database.
            finished, rtndict = runthemove(sa, tx, clientList, itTstDict, logger, options)

            # now get the SA Time sweep data
            saTraceData = sa.traceData
            logger.saveTraceData(i, saTraceData, rtndict, MOVETABLENAME, options.logtodb)


        for client in clientList :
            argv = ['stop_charging', client[0]]
            v = tx.sendGetTransmitter(argv)


def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser(option_class=myOption)

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default=None, help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfg", help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default=None, help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlist", help="Use the named list from the file (.py).")
    parser.add_option("-p","--saport",     dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    parser.add_option("-i","--saip",       dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")

    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="",help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default=sTXIP,help="Transmitter network addresst.")

    parser.add_option("","--actip",        dest='actuator',     type=str,  action='store',default='',help="Linear Actuator network address(es) (comma sep).")

    parser.add_option("-c","--clientid",   dest='clientid',     type=str,  action='store',default='',help="Device/client ID to use for characterization.")
    parser.add_option("","--minlinkq",     dest='minlinkq',     type=int,  action='store',default='85',help="Minimum link quality to validate a client.")
    parser.add_option("","--maxrssilevel", dest='maxrssi',      type=float,action='store',default='28.0',help="Minimum link quality to validate a client.")

    parser.add_option("","--cableloss",    dest='cableloss',    type=float,action='store',default=0.0, help='Cable loss positive dB. Added to the 20 dB of Connector loss')

    parser.add_option("-f","--logfile",    dest='logfile',      type=str,  action='store',default="MoveLogFile{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--noconsole",    dest='console',                 action='store_true',default=False,help="Turn off printing to console")
    parser.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    parser.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the DataBase's table name at ossia-build")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")
    parser.add_option("-g","--gencfg",     dest='gencfg',                  action="store_true",default=False,help="Generate a default config file.")

    (options, args) = parser.parse_args()
    if len(args) > 0 :
        print("Extra command line args, exiting")
        print(repr(args))
    else :
        if options.gencfg :
            fname = 'templConf.py'
            fd = open(fname, 'w')
            if fd :
                options.__dict__['gencfg'] = False
                fstr = "cfg={"
                for k in options.__dict__ :
                    fstr += "'{}':'{}'\n".format(k, options.__dict__[k])
                fstr += "}\n"
                fd.write(fstr)
                fd.close()
            fname = 'templTestList.py'
            fd = open(fname, 'w')
            if fd :
                fstr = "testlist=[\n"
                for k in range(10) :
                    fstr += "{"
                    fstr += "'powerlevel' : 17, "
                    fstr += "'warmup' : 30, "
                    fstr += "'txangle' : 0.0, "
                    fstr += "'start' : 500, "
                    fstr += "'stop' : 1900, "
                    fstr += "'movetime' : 30.0, "
                    fstr += "'points' : 501, "
                    fstr += "'waitfordata' : 12, "
                    fstr += "},\n"
                fstr += "]\n"
                fd.write(fstr)
                fd.close()

        else :
            if options.cfgfile is not None :
                TTYPE = 0
                TDEF = 1
                import imp
                fname = options.cfgfile
                if fname is '' :
                    fname = sys.argv[0].replace('.', 'Conf.')
                cfgmod = imp.load_source(options.cfgname, fname)
                cfg = eval("cfgmod."+options.cfgname)
                optType = makeOptionTypeDict(parser)
                for k in cfg.keys() :
                    try :
                        if optType[k][TTYPE]  == 'int' :
                            if str(cfg[k]) == 'DeFault' : # check for a default value
                                options.__dict__[k] = optType[k][TDEF] # set the default value
                            else :
                                options.__dict__[k] = int(str(cfg[k]))
                        elif optType[k][TTYPE]  == 'float' :
                            if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                                options.__dict__[k] = optType[k][TDEF] # set the default value
                            else :
                                options.__dict__[k] = float(str(cfg[k]))
                        elif optType[k][TTYPE]  is None :
                            options.__dict__[k] = optType[k][TDEF]  # set the default value
                            try :
                                options.__dict__[k] = eval(cfg[k])
                            except NameError :
                                options.__dict__[k] = False

                        elif optType[k][TTYPE]  == 'string' :
                            if str(cfg[k]) == 'DeFault' : # check for a default value
                                options.__dict__[k] = optType[k][TDEF] # set the default value
                            elif str(cfg[k]) == 'None' :
                                options.__dict__[k] = None
                            else :
                                options.__dict__[k] = str(cfg[k])
                        else :
                            options.__dict__[k] = str(cfg[k])
                        #print("<pre>name={} def={} type={} in={} cur={}</pre>\n".format(k, optType[k][TDEF], optType[k][TTYPE], cfg[k], options.__dict__[k]))
                    except ValueError as ve :
                        print("<pre>{} -- Incorrect option value {} -- {}</pre>\n".format(k, cfg[k], repr(ve)))
                        exit(2)
                    except IndexError as ie :
                        print("<pre>{} -- No such option -- {}</pre>\n".format(k, repr(ie)))
                        exit(3)
                    except Exception as e :
                        print("<pre>{} -- Unknown exception -- {}</pre>\n".format(k, repr(e)))
                        exit(4)

            fname = sys.argv[0].replace('.', 'TestList.')
            #print("<pre> fname def = {}</pre>".format(fname))
            if options.testlistfile is not None :
                #print("<pre> fname testlistfile = {}</pre>".format(options.testlistfile))
                fnm = options.testlistfile
                if fnm != '' :
                    fname = fnm
            import imp
            tstlstmod = imp.load_source(options.testlistname, fname)
            testlist = eval("tstlstmod."+options.testlistname)

            if options.debug :
                print("<pre>options = {}</pre>".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
                print("<pre>testlist = {}</pre>".format(repr(testlist).replace('},','},\n')))
            print("<pre>Executing main</pre>\n")
            #print("<pre>options = {}</pre>".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
            main(testlist, options)
            duration = int(time() - START_TIME)
            hrs = float(duration) / 3600.0
            mins = (hrs - int(hrs)) * 60.0
            secs = (mins - int(mins)) * 60.0
            secs += 0.000005

            print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
