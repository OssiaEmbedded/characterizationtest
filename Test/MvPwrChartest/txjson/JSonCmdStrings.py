""" JSonCmdStrings.py: A dictionary and set of methods to provide communication command strings
and interpretation of the returned value with the Message Manager software running on an Ossia Inc.
Transmiter.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function, unicode_literals
import json

JSON_CMD_DICT = {
''' Set of strings take from the Message Manager server C-Code to communicate with the Message Manager.
'''
'channel_on': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Channel ON"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'channel_off': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Channel OFF"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'calibrate': {'jstr': '{"Command":{"Type":"Calibrate"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'add_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Add Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'register_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Register Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'remove_client': {'jstr': '{"Command":{"Client ID":"%s","Type":"Remove Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'identify_client': {'jstr': '{"Command":{"Client ID":"%s","Duration":15,"Type":"Identify Client"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'client_command': {'jstr': '{"Command":{"Client ID":"%s","Data":"%s","Type":"Client Command"},"Result":{"Status":"NULL"}}', 'numofargs': 2},
'client_command_data': {'jstr': '{"Command":{"Client ID":"%s","Type":"Client Command Data"},"Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'set_good_channels': {'jstr': '{"Command":{"Good Channels":"%s","Type":"Set Good Channels"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'run': {'jstr': '{"Command":{"Type":"Run"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'pause': {'jstr': '{"Command":{"Type":"Pause"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reboot': {'jstr': '{"Command":{"Type":"Reboot CCB"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_charger_id': {'jstr': '{"Command":{"Type":"GetChargerId"},"Result":{"Status":"NULL", "ChargerId": "xxxx"}}', 'numofargs': 0},
'reset': {'jstr': '{"Command":{"Type":"Reset Array"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_FPGA': {'jstr': '{"Command":{"Type":"Reset FPGA"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'reset_proxy': {'jstr': '{"Command":{"Type":"Reset Proxy"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'shutdown': {'jstr': '{"Command":{"Type":"Shutdown CCB"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_bbs': {'jstr': '{"Command":{"Type":"Send BBS"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_cqt': {'jstr': '{"Command":{"Type":"Send CQT"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_disc': {'jstr': '{"Command":{"Type":"Send Discovery Message"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'send_tpc': {'jstr': '{"Command":{"Client ID":"%s","Type":"Send TPC"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'send_tps': {'jstr': '{"Command":{"Type":"Send TPS"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'stop_disc': {'jstr': '{"Command":{"Type":"Stop Discovery Message"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'info': {'jstr': '{"Command":{"Type":"Get System Info"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
'get_good_channels': {'jstr': '{"Command":{"Type":"Get Good Channels"},"Result":{"Good Channels":"NULL"}}', 'numofargs': 0},
'channel_info': {'jstr': '{"Command":{"Channel Number":"%s","Type":"Get Channel Info"},"Result":{"AMB Revision":0,"Status":"Off"}}', 'numofargs': 1},
'proxy_info': {'jstr': '{"Command":{"Type":"Get Proxy Info"},"Result":{"Proxy FW Revision":0}}', 'numofargs': 0},
'versions': {'jstr': '{"Command":{"Type":"GetChargerFirmwareVersion"},"Result":{"Daemon Build Info":{"Date":0,"Number":0},"Driver Lib Build Info":{"Date":0,"Number":0},"FPGA Revision":0,"Message Manager Build Info":{"Date":0,"Number":0},"OS Version":"0","Proxy FW Revision":0,"Release Version":0}}', 'numofargs': 0},
'client_list': {'jstr': '{"Command":{"Type":"Get List of Clients"},"Result":{"Clients":[{"Short ID":"1","Client ID":"2","Version":"3","Status":"0","LinkQ":"34"}]}}', 'numofargs': 0},
'client_detail': {'jstr': '{"Command":{"Client ID":"%s","Type":"Get Client Detail"},"Result":{"AveragePower":0,"BatteryLevel":0,"Client ID":"0x0","DeviceStatus":0,"LinkQuality":0,"Model":0,"NetCurrent":123,"PeakPower":0,"QueryFailedCount":5,"QueryType":5,"RSSIValue":0,"Short ID":"0x0","Status":"NULL","TPSMissed":0,"Version":"NULL"}}', 'numofargs': 1},
'client_data': {'jstr': '{"Command":{"Client ID":"%s","Type":"GetClientData"},"Result":{"Data":[],"Status":"NULL"}}', 'numofargs': 1},
'client_config': {'jstr': '{"Command":{"Client ID":"%s","QueryType":"%s","Type":"SetClientConfig"},"Result":{"Status":"NULL"}}', 'numofargs': 2},
'devices_in_range': {'jstr': '{"Command":{"Type":"GetAllDevicesInRange"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 0},
'check_in_range': {'jstr': '{"Command":{"Client ID":"%s","Type":"CheckDeviceInRange"},"Result":{"BatteryLevel":0,"Client ID":"0x0","Status":"NULL"}}', 'numofargs': 1},
'check_status': {'jstr': '{"Command":{"Client ID":"%s","Type":"CheckDeviceChargingStatus"},"Result":{"BatteryLevel":0,"Client ID":"0x0","Status":"NULL"}}', 'numofargs': 1},
'start_charging': {'jstr': '{"Command":{"Devices":"%s","Type":"StartChargingDevices"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'stop_charging': {'jstr': '{"Command":{"Devices":"%s","Type":"StopChargingDevices"},"Result":{"Devices":[],"Status":"NULL"}}', 'numofargs': 1},
'get_charging_info': {'jstr': '{"Command":{"Devices":"%s","Type":"GetDeviceChargeInfo"},"Result":{"Devices":[{"AveragePower":0,"Client ID":"0x0","DeviceStatus":0,"LinkQuality":0,"NetCurrent":0,"PeakPower":0,"RSSIValue":0}],"Status":"NULL"}}', 'numofargs': 1},
'motion_config': {'jstr': '{"Command":{"Sensitivity":"%s","Type":"SetMotionEngine"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'motion_devices': {'jstr': '{"Command":{"Devices":"%s","Type":"TrackMotionForDevices"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'set_power_level': {'jstr': '{"Command":{"PowerLevel":%d,"Type":"SetPowerLevel"},"Result":{"Status":"NULL"}}', 'numofargs': 1},
'get_power_level': {'jstr': '{"Command":{"Type":"GetPowerLevel"},"Result":{"PowerLevel":22,"Status":"NULL"}}', 'numofargs': 0},
'get_system_temp': {'jstr': '{"Command":{"Type":"GetSystemTemp"},"Result":{"Status":"NULL","Temp":0}}', 'numofargs': 0},
'identify_charger': {'jstr': '{"Command":{"Type":"IdentifyCharger"},"Result":{"Status":"NULL"}}', 'numofargs': 0},
}

def getTxId(pyData) :
    ''' getTxId -- requires data from a previous call to get_charger_id
    returns the Transmitters idetification string.
    '''
    rtnval = pyData['Result'].get('ChargerId', "NotFound")
    return rtnval

def checkForSuccess(pyData, ckVal='SUCCESS') :
    ''' checkForSuccess -- returns true if the status of in input data matches the ckVal.
    False otherwise.
    '''
    rtnval = False
    if processSingleReturn(pyData, 'Status') != ckVal :
        rtnval =  True
    return rtnval

def getClientID(pyData, requestedClientID=None) :
    """ getClientID -- Reguires data from a "client_list" operation
        @param: pyData the result of a json loads operation
        @param: requestedClientID is the ID of a particular expected client
        returns: two values:
                  1) None or requestedClientID
                  2) the list of clients as strings
    """

    clientId, idList = _processClientList(pyData, "Client ID", requestedClientID)
    return (clientId, idList)

def _processClientList(pyData, field, checkDat=None) :
    ''' _processClientList -- auxiliary routine to support getClientId
    '''
    rtnval = None
    size = len(pyData['Result']['Clients'])
    rtnlist = list()
    for i in range(size) :
        cid = str(pyData['Result']['Clients'][i][field])
        if checkDat is not None and cid == checkDat :
            rtnval = cid
        rtnlist.append(cid)
    return rtnval, rtnlist

def processSingleReturn(pyData, strval) :
    rtnval = -1234 
    try :
        tmp = pyData.get('Result', None)
        if tmp is not None :
            rtnval = tmp.get(strval, -12345)
    except KeyError :
        rtnval =  -2345 
    return rtnval

def getClientVersion(pyData) :
    """ getClientVersion -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Client version
    """

    version = int(processSingleReturn(pyData, "Version"))
    return version

def getBatteryLevel(pyData) :
    """ getBatteryLevel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Battery Level
    """
    val =  processSingleReturn(pyData, "BatteryLevel")
    return val

def getDeviceStatus(pyData) :
    """ getDeviceStatus -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Status
    """
    val =  processSingleReturn(pyData, "DeviceStatus")
    return val

def getDeviceModel(pyData) :
    """ getDeviceModel -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Device Model number 
    """
    val =  processSingleReturn(pyData, "Model")
    return val

def getTPSMissed(pyData) :
    """ getTPSMissed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: TPSMissed
    """
    val =  processSingleReturn(pyData, "TPSMissed")
    return val

def getClientShortID(pyData) :
    """ getClientShortID -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: Short ID
    """
    shortID =  processSingleReturn(pyData, "Short ID")
    return shortID

def getClientLinkQuality(pyData) :
    """ getClientLinkQuality -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns: LinkQuality
    """
    linkQuality =  processSingleReturn(pyData, "LinkQuality")
    return int(linkQuality)

def getClientNetCurrent(pyData) :
    """ getClientNetCurrent -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns Net current as a float in units mA
    """
    return float(processSingleReturn(pyData, "NetCurrent"))

def getClientQueryFailed(pyData) :
    """ getClientQueryFailed -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the number of failed queries as an int 
    """
    return int(processSingleReturn(pyData, "QueryFailedCount"))

def getAveragePower(pyData) :
    """ getAveragePower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the Average power as an int 
    """
    return float(processSingleReturn(pyData, "AveragePower"))

def getPeakPower(pyData) :
    """ getPeakPower -- Requires data from a "client_detail" operation
        @param: pyData the result of a json loads operation
        returns the peak power as an int
    """
    return float(processSingleReturn(pyData, "PeakPower"))

def getClientBatteryVoltage(pyData) :
    """ getClientBatteryVoltage -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        returns the Battery Voltage as a float in units Volts
    """
    bv = (int(pyData['Result']['Data'][1]) << 8) + int(pyData['Result']['Data'][0])
    return float(bv) / 1000.0

def getClientRFPower(pyData, whichone) : # returns a tuple of RSSI values and the entire list
    """ getClientRFPower -- Requires data from a "client_data" operation
        @param: pyData the result of a json loads operation
        @param: whichone - is an index from 1 to 4. Designating a port on the antenna.
        returns: two values, as a tuple:
                 a tuple of RSSI and RSSI-1
    """
    rawData = processSingleReturn(pyData, 'Data')
    tLen = len(rawData)
    data = list()
    if (tLen % 2) != 0 : # just in case
        tLen -= 1
    # this takes a data list of 34 and combines hi and low order bytes
    # into a list of 17. The zeroth value is the battery voltage
    for i in range(0, tLen, 2) :
        tmp = ((int(rawData[i+1]) << 8) + int(rawData[i]))
        if tmp > 0x7FFF :
            tmp = 0x8000 - tmp
        #if tmp == 32767 :
        #    tmp = -0.0001
        data.append(tmp)

    # the avg RF values are at 1/5,   2/6,   3/7,  and  4/8
    # the max RF values are at 9/13, 10/14, 11/15, and 12/16
    if len(data) > (12 + whichone) :
        avgRSSI = data[0 + whichone]
        avgRSSI_1 = float(data[4 + whichone])/1000.0
        maxRSSI = data[8 + whichone]
        maxRSSI_1 = float(data[12 + whichone])/1000.0
    else :
        maxRSSI = -1
        maxRSSI_1 = -1.0
        avgRSSI = -1
        avgRSSI_1 = -1.0
    return (maxRSSI, maxRSSI_1, avgRSSI, avgRSSI_1)

def getTransmitterPowerLevel(pyData) :
    """ getTransmitterPowerLevel -- Requires data from a "get_power_level" operation
        @param: pyData the result of a json loads operation
        returns: the Transmitter power level as an int in units dBm
    """
    return int(processSingleReturn(pyData, 'PowerLevel'))

def getSystemTemp(pyData) :
    """ getTransmitterTemp -- Requires data from a "get_system_temp" operation
        @param: pyData the result of a json loads operation
        returns: the Transmitter power level as an int in units dBm
    """
    return int(processSingleReturn(pyData, 'Temp'))

if __name__ == '__main__' :
    for k in JSON_CMD_DICT.keys() :
        print ("@@@@ " + k)
        print (JSON_CMD_DICT[k].keys())
        print (JSON_CMD_DICT[k]['numofargs'])
        print (JSON_CMD_DICT[k]['jstr'])

        pyData = json.loads(JSON_CMD_DICT[k]['jstr'])
        print ("\n###################\n{}\n###################\n".format(repr(pyData)))
        if k == 'client_detail' :
            print( "NetCurnt  " + repr(getClientNetCurrent(pyData)))
            print( "Fail Qry  " + repr(getClientQueryFailed(pyData)))
        elif k == 'client_list' :
            print( "Short ID  " + repr(getClientShortID(pyData)))
            print( "Client ID " + repr(getClientID(pyData, requestedClientID=None)))
            print( "Version   " + repr(getClientVersion(pyData)))
        elif k == 'client_data' :
            print( "BatVolt   " + repr(getClientBatteryVoltage(pyData)))
            print( "MaxRFPwr  " + repr(getClientRFPower(pyData, 1)))
        elif k == 'get_power_level' :
            print( "Chgr Pwr  " + repr(getTransmitterPowerLevel(pyData)))

