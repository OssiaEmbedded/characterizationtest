#! /usr/bin/python

import socket
from time import sleep, gmtime, strftime
from optparse import OptionParser


def sendgetdata(sendthis):
    mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    try :
        mySocket.connect((options.tstequip, options.portnumber))
        mySocket.settimeout(1.0)
    except :
        print "There was a connect exception "
        exit(1)

    s = mySocket

    cnt = 60
    s.send( sendthis + "\r\n" )
    sleep(0.5)
    data = bytearray()
    d = bytearray()
    while cnt > 0 :
        try :
            d = bytearray(s.recv(8192))
            data += d
        except socket.timeout :
            print cnt
            cnt -= 1
        if 'SUCCESS' in data :
            break
    #print data
    data = data.lstrip().rstrip()
    mySocket.close()

    return data
 
parser = OptionParser()

parser.add_option("-t","--tstequip",
                   dest="tstequip",
                   action="store",
                   default="ossiatest0.local",
                   help="Network name or IP address of ossiaServer (default='%default')")

parser.add_option("-p","--port",
                   dest="portnumber",
                   type=int,
                   action="store",
                   default="7000",
                   help="Port number of the ossiaServer telnet server (default='%default')")

(options, args) = parser.parse_args()


if len(args) > 0 :
    sDat = "COMMANDVAL"
    for i in range(len(args)) :
        sDat += "/"+args[i].replace('\\','')
    data = sendgetdata(sDat)
    print "DATA = ''{}''\n".format(str(data))

else :
    str1 = "COMMANDVAL/setLinearMSResolution/16th"
    str2 = "COMMANDVAL/setStepTime/1500/l"
    strlst = [ "COMMANDVAL/setLinearPosition/0",
               "COMMANDVAL/setLinearPosition/1180",
               "COMMANDVAL/setLinearPosition/1500",
               "COMMANDVAL/setLinearPosition/1000",
               "COMMANDVAL/setLinearPosition/1200",
               "COMMANDVAL/setLinearPosition/1900"]

    data = sendgetdata(str1)
    print "DATA = ''{}''".format(str(data))
    data = sendgetdata(str2)
    print "DATA = ''{}''".format(str(data))
    for i in range(1000) :
        try :
            index = i % len(strlst)
            print "Command is '{}'\n".format(strlst[index])
            data = sendgetdata(strlst[index])
            print "DATA = ''{}''".format(str(data))
            print "sleep {} secs".format(index+2)
            sleep(index + 2)
        except KeyboardInterrupt :
            break

        
   

