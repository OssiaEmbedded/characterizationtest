''' MCSwitch.py -- Interface to control a Mini-Circuits 8 port RF Switch

Makes use of a Windows executable.

Date :  Mar 06 2018
Copyright Ossia Inc. 2018

'''

from __future__ import absolute_import, division, print_function, unicode_literals

from subprocess import call as spcall
from subprocess import check_output as spcheck_output

cmdroot = "c:\\Users\\Public\\MothersonTest\\hidaccess.exe"

VERSION = '0.0.1'

def getSwitch() :

    cmd = cmdroot+" r 1"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[0]
    return rtnval

def setSwitch(port) :

    cmd = cmdroot+" w "+str(port)
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))

def getSerialNumber() :

    cmd = cmdroot+" r 41"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[1]
    return rtnval[1:]

def getModelNumber() :

    cmd = cmdroot+" r 40"
    rtnstr = spcheck_output(cmd, shell=True)
    #print("rtnstr = {}".format(rtnstr))
    rtnval = rtnstr.split(",")[1]
    return rtnval[1:]

if __name__ == "__main__" :
    print("sw {}".format(getSwitch()))
    print("sn {}".format(getSerialNumber()))
    print("ml {}".format(getModelNumber()))

