''' ReflectChamberTest.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

An interactive test to allow environment modifications between steps to be accomplished.

Version : 1.0.0
Date :  Oct 20 2017
Copyright Ossia Inc. 2017

'''

from __future__ import absolute_import, division, print_function, unicode_literals

import sys

from time import sleep, time, localtime
from optparse import OptionParser

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from socket import gethostbyname
import MCSwitch as mcs
import winsound

COUPLERLOSS=19.8
ROOTDIRPATH="C:\\Users\\Public\\MothersonTest\\"
PEAKHOLDTIME=5
TESTDBNAME='IntegLabOne'

TXDATACLIENTTABLENAME = "ClientTable"
TXDATASATABLENAME = "SaTable"
TXTBLNAME="ChargerConfig"
TIMESTAMPCOLNAME="DataTimeStamp"
DEFAULTDBSERVER = '10.10.0.216'

# data base definition constants
TXCMS = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
         ["AntennaType", "%s", 'VARCHAR(100)'],
         ["CfgComment", "%s", 'VARCHAR(200)'],
         ["PostTestNotes", "%s", 'VARCHAR(512)'],
         [TXDATACLIENTTABLENAME, "%s",'VARCHAR(40)'],
         [TXDATASATABLENAME, "%s",'VARCHAR(40)'],
         ["LogFileName", "%s", 'VARCHAR(100)'],
         ["Duration", "%s", 'VARCHAR(20)'],
         ["txId", "%s", 'VARCHAR(50)'],
         ["CotaConf", "%s", 'VARCHAR(512)'],
         ["OnChannels", "%s",'VARCHAR(15)'],
         ["GoodChannels", "%s",'VARCHAR(15)'],
         ["Versions", "%s",'VARCHAR(512)'],
         ["AmbRev", "%s",'VARCHAR(512)'],
         ["options", "%s", 'VARCHAR(1024)'] ]

SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', "100.0 MHz", 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '470 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

CLIENTCMS = [["clientId", "%s", 'VARCHAR(25)'],
             ["clientAlias", "%s", 'VARCHAR(256)'],
             ["MCSwitchPort", "%s", 'VARCHAR(6)'],
             ["netCurrent", "%3.2f", 'VARCHAR(6)'],
             ["batteryVoltage", "%3.2f", 'VARCHAR(10)'],
             ["batteryLevel", "%d", 'VARCHAR(5)'],
             ["averagePower", "%5f", 'VARCHAR(10)'],
             ["peakPower", "%5f", 'VARCHAR(10)'],
             ["cableLoss", "%5.3f",'VARCHAR(7)'],
             ["timetaken", "%s", 'VARCHAR(30)'],
             ["systemTemp", "%d", 'VARCHAR(15)'],
             ["PowerLevel", "%d", 'VARCHAR(5)'],
             ["WarmupTime", "%d",'VARCHAR(10)'],
             ["linkQuality", "%d", 'VARCHAR(6)'],
             ["clientComRSSI", "%d", 'VARCHAR(6)'],
             ["proxyComRSSI", "%d", 'VARCHAR(6)'],
             ["queryFailedCount", "%d", 'VARCHAR(6)'],
             ["DeviceStatus", "%d", 'VARCHAR(6)'],
             ["TPSMissed", "%d",    'VARCHAR(20)'],
             ["referenceLevelOffset", "%5.2f", 'VARCHAR(10)'],
             ["MaxCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["MaxCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["MaxZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["MaxZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["AvgCalcPwr_1", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_2", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_3", "%5.2f",   'VARCHAR(10)'],
             ["AvgCalcPwr_4", "%5.2f",   'VARCHAR(10)'],
             ["AvgZCSCNT_1", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_2", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_3", "%d",   'VARCHAR(10)'],
             ["AvgZCSCNT_4", "%d",   'VARCHAR(10)'],
             ["Model", "%d",        'VARCHAR(10)'],
             ["version", "%d", 'VARCHAR(20)']]


#sTXIP='Tile26'
sTXIP='10.10.1.170'
sTXPORT='50000'
sSAIP='A-N9020A-11404.ossiainc.local'
sSAPORT='5023'
srvPORT='50081'

def isTaperRunning() :
    from LIB.sendTestServer import SendTestServer

    sts=SendTestServer(options.txipaddr, int(srvPORT), options.debug)
    jVal = sts.sendCommand(['isTaperRunning'])
    rtnval = False
    if str(jVal).find("NONETAPER") < 0 :
        rtnval = True
    return rtnval


def getCotaConfigValues(options) :
    from LIB.sendTestServer import SendTestServer

    sts=SendTestServer(options.txipaddr, int(srvPORT), options.debug)
    jVal = sts.sendCommand(['getConfigFile'])
    rtnstr, plst, _ = sts.parseCotaConfigValues(jVal)

    return rtnstr, plst


def reportWait(waittime, msg=None, reportval=None, prnt=True) :
    ''' reportWait -- console display of any extended wait times.
    '''
    if prnt :
        if msg is None :
            esm = '' if int(waittime/60) == 1 else 's'
            ess = '' if int(waittime%60) == 1 else 's'
            print("Warming up the Transmitter for {} min{} {} sec{}".format(int(waittime/60), esm, waittime%60, ess))
        else :
            print(msg)
    NUM = 40.0
    wt = float(waittime)/NUM
    stm = time()
    while waittime > 0 :
        sleep(wt)
        if reportval is None :
            if prnt :
                print("{:2d} ".format(int(NUM)), end="")
            NUM = NUM - 1.0
        else :
            if prnt :
                print(reportval + " ", end="")
        sys.stdout.flush()
        waittime -= wt
    if prnt :
        print(" {}".format(time() - stm))
        print('Wait finished')

def getVersions(tx) :
    argv = ["versions"]
    v = tx.sendGetTransmitter(argv)
    vers = v['Result']
    swRel = int(vers["Release Version"])
    versionstr = "SWRel:{:X}.{:X}.{:X}\n".format(((swRel>>16) & 0xF), ((swRel >> 8) & 0xF), (swRel & 0xF))

    blddate = int(vers['Daemon Build Info']['Date'])
    bldnum  = int(vers['Daemon Build Info']['Number'])
    versionstr += "Daemon:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Driver Lib Build Info"]["Date"])
    bldnum  = int(vers["Driver Lib Build Info"]["Number"])
    versionstr += "DrvLib:{}:{}\n".format(blddate, bldnum)

    blddate = int(vers["Message Manager Build Info"]["Date"])
    bldnum  = int(vers["Message Manager Build Info"]["Number"])
    versionstr += "MesMan:{}:{}\n".format(blddate, bldnum)

    bldnum = int(vers["FPGA Revision"])
    versionstr += "FPGA:0x{:04X}\n".format(bldnum)

    bldnum = int(vers["Proxy FW Revision"])
    versionstr += "PROXY:0x{:04X}\n".format(bldnum)

    versionstr += "OS:{}".format(str(vers["OS Version"]))

    return versionstr

def getOnChannels(tx) :
    #
    ch_on = 0
    argv = ["info"]
    v = tx.sendGetTransmitter(argv)
    chList = v['Result']['AMBS']
    rtnstr = ""
    for chan in chList :
        outStr = ""
        try :
            chOnStr = str(chan['Status'])
            chOn = 1 if chOnStr == 'ON' else 0
            chNum = int(chan['Channel Number'])
            rev = int(chan['AMB Revision'])
            # make the def smaller, take out all the extra unneeded characters
            outStr = "{}:{:02d}:{:04X}\n".format(chOnStr, chNum, rev)
            # chOn will be either 1 or 0, shifting a zero is benign.
            ch_on |= (chOn << chNum)
        except ValueError as ve :
            print("Chan Info ValueError {} -- {}".format(v['Result']['Status'], ve))
            outStr = "{}:{}:{}\n".format("OFF", "ZZ", "XXXX")
        rtnstr += outStr

    chStr = "0x{:04X}".format(ch_on)
    return chStr, rtnstr

def stopStartChargingList(tx, ss, clientList) :
    # ss must be either 'stop' or 'start' for this to work
    rtnval = False
    cmd = "{}_charging".format(ss)
    failword = "{}CHARGING".format("NOT_" if ss == "stop" else '')
    for client in clientList :
        sClientId = str(client[0])
        argv = [cmd, sClientId]
        try :
            v = tx.sendGetTransmitter(argv)
            if v['Result']['Devices'][0]['Status'] != failword :
                print("Charging did not '{}': {}".format(ss, repr(v['Result']["Devices"][0]['Status'])))
                rtnval = True
        except Exception as ex :
            print("There was an exception for client {} when sending the {} command -- {}\n".format(sClientId, ss, repr(ex)))
            rtnval = True
    return rtnval

def transmitterCalibrate(tx, clientList, msg='None', prnt=True) :
    rtnval = False #stopStartChargingList(tx, 'stop', clientList)

    if not rtnval :
        argv = ['calibrate']
        v = tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Calibrate: {}".format(msg))
            rtnval = True

    sleep(8)
    if not rtnval :
        rtnval = False #stopStartChargingList(tx, 'start', clientList)

    if prnt :
        print("Calibration {} CL={}".format("Failed" if rtnval else "Done", repr(clientList)))

    return rtnval

def getClientCommandData(tx, sClientId, num, delayTime=2.5) :
    argv = ['client_command', sClientId, num]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        print("Client Command {} request Failed : {}".format(num, repr(v)))
    sleep(delayTime)

    argv = ['client_command_data', sClientId]
    v = tx.sendGetTransmitter(argv)
    if jcs.checkForSuccess(v) :
        print("Client Command Data request Failed ({}): {}".format(num, repr(v)))
    return v

def runthecharacter(sa, tx, cacsList, testlist, logger, options, comChanNum) :

    finished = False
    iterateCnt = -1
    sClientId = ""
    iterateCntUp = 1
    comChanFreq = { "24" : 2.4601E9, "25" : 2.4501E9, "26" : 2.4401E9}
    try :
        iterateCnt = int(testlist.get('logcount', 10))
        clientId   = str(testlist.get('client', '')).strip()
        timetowait = float(testlist.get('waitfordata', 16))
    except ValueError as ve :
        print("testlist input error -- {}".format(repr(ve)))
    else :
        # start or stop charging clients.
        timetowait = max(timetowait, 1.0)

        for client, aname, _, _ in cacsList :
            sClientId = str(client).strip()
            stopstartstr = "start"
            ckval = "CHARGING"
            if clientId.lower() != "all" and clientId != sClientId :
                argv = ['stop_charging', sClientId]
                stopstartstr = "stop"
                ckval = "NOT_CHARGING"
            else :
                argv = ['start_charging', sClientId]
            v = tx.sendGetTransmitter(argv)
            if v['Result']['Devices'][0]['Status'] != ckval :
                print("Client {} at location {} did not {} Charging -- {}".format(sClientId, aname, stopstartstr, repr(v)))

        # wait for the warmup period
        warmup = int(testlist.get('warmup', 10))
        reportWait(warmup, prnt=options.console)

        if testlist.get('caljustonce', False) :
            transmitterCalibrate(tx, None, msg="After warmup", prnt=options.console)

        # now iterate through the client lists
        if iterateCnt > 0 :

            for client, aliasName, cableloss, port in cacsList :

                sClientId = client.strip()
                if clientId.lower() != 'all' and clientId != sClientId :
                    #print("Skipping data for client {}".format(sClientId))
                    continue
                #print("Getting data for client {}".format(sClientId))
                iterateCntDn = iterateCnt
                if port == "" :
                    port = "1"
                mcs.setSwitch(port.strip())
                setPort = mcs.getSwitch()
                refLvlOS = -1.0
                try :
                    cableloss = float(cableloss)
                    refLvlOS = COUPLERLOSS + cableloss
                except ValueError :
                    print("ValueError on cableloss cast to float {}".format(cableloss))
                    cableloss = -190.0
                else :
                    sa.referenceLevelOffset = refLvlOS
                logger.logAddValue("referenceLevelOffset", refLvlOS)

                while iterateCntDn > 0 and not finished :

                    sleep(timetowait)

                    iterateCntUp += 1

                    iterateCntDn -= 1
                    if options.console and iterateCntDn >= 0 :
                        print("{} ".format(iterateCntDn), end='')

                    # get system temperature datum
                    argv = ['get_system_temp']
                    v = tx.sendGetTransmitter(argv)
                    systemTemp = 0
                    if jcs.checkForSuccess(v) :
                        print("System Temperature not valid")
                    else :
                        systemTemp = jcs.getSystemTemp(v)
                    # add system Temp to the log list
                    logger.logAddValue('systemTemp', systemTemp)


                    # set SA to obtain peak or average value
                    SAPower = '-60.0'
                    SAFreq = '-2.0'
                    if sa.usesa :
                        if options.average :
                            sa.avgHold = ""
                        else :
                            sa.maxHold = ""
                        # wait for data to get stable
                        sleep(PEAKHOLDTIME)
                        # find the freq of the peak
                        sa.peakSearch = ""

                        # get the SA data
                        sa.triggerHold = ''
                        SAPower, SAFreq = sa.peakSearch
                        if options.saimage > 0 :
                            try :
                                tstEval = "%6.4e != %6.4e" % (float(SAFreq), float(comChanFreq[str(comChanNum)]))
                                if options.saimage == 2 or eval(tstEval) :
                                    filename = "{}/saImage_{}.png".format(options.directory, int(time()))
                                    saI = sacom.SACom(addr=options.saipaddr, port=80, debug=options.debug)
                                    if saI.usesa :
                                        saI.getSaveImage(filename)
                                        logger.logAddValue("ImageSA", filename)
                            except KeyError as ke :
                                print("KeyError in comChanNum -- {}\n".format(repr(ke)))
                            except ValueError as ve :
                                print("ValueError in comChanNum -- {}\n".format(repr(ve)))
                            except TypeError as te :
                                print("TypeError in comChanNum -- {}\n".format(repr(te)))

                        sa.triggerClear = ''
                        sa.clearHold = ''

                    logger.logAddValue("MCSwitchPort", setPort)
                    # add the client ID to the log list
                    logger.logAddValue('clientId', sClientId)
                    logger.logAddValue('clientAlias', aliasName.strip())
                    logger.logAddValue("PowerLevel", testlist['powerlevel'])
                    logger.logAddValue("WarmupTime", testlist['warmup'])
                    logger.logAddValue("cableLoss", cableloss)

                    argv = ['client_detail', sClientId]
                    v = tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v, ckVal="CHARGING") and jcs.checkForSuccess(v, ckVal="LIMITED_CHARGING") :
                        print("Client Detail request shows client {} not charging {}".format(sClientId, repr(v['Result']['Status'])))

                    netCurrent       = jcs.getClientNetCurrent(v)
                    batteryLevel     = jcs.getBatteryLevel(v)
                    DeviceStatus     = jcs.getDeviceStatus(v)
                    TPSMissed        = jcs.getTPSMissed(v)
                    averagePower     = jcs.getAveragePower(v)
                    peakPower        = jcs.getPeakPower(v)
                    version          = jcs.getClientVersion(v)
                    queryFailedCount = jcs.getClientQueryFailed(v)
                    linkQuality      = jcs.getClientLinkQuality(v)
                    Model            = jcs.getDeviceModel(v)
                    clientComRSSIVal = jcs.getClientComRSSI(v)
                    proxyComRSSIVal  = jcs.getProxyComRSSI(v)

                    logger.logAddValue('Model', Model)
                    logger.logAddValue('queryFailedCount', queryFailedCount)
                    # add version to the log list
                    logger.logAddValue('version', version)
                    # add linkQuality to the log list
                    logger.logAddValue('linkQuality', linkQuality)
                    logger.logAddValue('clientComRSSI', clientComRSSIVal)
                    logger.logAddValue('proxyComRSSI', proxyComRSSIVal)
                    # add netcurrent to the log list
                    logger.logAddValue('netCurrent', netCurrent)
                    # add batteryLevel to the log list
                    logger.logAddValue('batteryLevel', batteryLevel)
                    # add averagePower to the log list
                    logger.logAddValue('averagePower', averagePower)
                    # add peakPower to the log list
                    logger.logAddValue('peakPower', peakPower)
                    # add Distances to the log list
                    logger.logAddValue('DeviceStatus', DeviceStatus)
                    logger.logAddValue('TPSMissed', TPSMissed)
                    # get charging client data
                    argv = ['client_data', sClientId]
                    v = tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v) :
                        print("Client Data request Failed: " + repr(v['Result']['Status']))
                        #break

                    batteryVoltage = jcs.getClientBatteryVoltage(v)
                    # add batteryVoltage to the log list
                    logger.logAddValue('batteryVoltage', batteryVoltage)

                    data = jcs.processClientData(v)
                    MaxZCSCNT_1, MaxCalcPwr_1, AvgZCSCNT_1, AvgCalcPwr_1 =  jcs.getClientRFPower(data, 1)
                    MaxZCSCNT_2, MaxCalcPwr_2, AvgZCSCNT_2, AvgCalcPwr_2 =  jcs.getClientRFPower(data, 2)
                    MaxZCSCNT_3, MaxCalcPwr_3, AvgZCSCNT_3, AvgCalcPwr_3 =  jcs.getClientRFPower(data, 3)
                    MaxZCSCNT_4, MaxCalcPwr_4, AvgZCSCNT_4, AvgCalcPwr_4 =  jcs.getClientRFPower(data, 4)

                    logger.logAddValue('MaxZCSCNT_1',  MaxZCSCNT_1)
                    logger.logAddValue('MaxCalcPwr_1', MaxCalcPwr_1)
                    logger.logAddValue('MaxZCSCNT_2',  MaxZCSCNT_2)
                    logger.logAddValue('MaxCalcPwr_2', MaxCalcPwr_2)
                    logger.logAddValue('MaxZCSCNT_3',  MaxZCSCNT_3)
                    logger.logAddValue('MaxCalcPwr_3', MaxCalcPwr_3)
                    logger.logAddValue('MaxZCSCNT_4',  MaxZCSCNT_4)
                    logger.logAddValue('MaxCalcPwr_4', MaxCalcPwr_4)
                    logger.logAddValue('AvgZCSCNT_1',  AvgZCSCNT_1)
                    logger.logAddValue('AvgCalcPwr_1', AvgCalcPwr_1)
                    logger.logAddValue('AvgZCSCNT_2',  AvgZCSCNT_2)
                    logger.logAddValue('AvgCalcPwr_2', AvgCalcPwr_2)
                    logger.logAddValue('AvgZCSCNT_3',  AvgZCSCNT_3)
                    logger.logAddValue('AvgCalcPwr_3', AvgCalcPwr_3)
                    logger.logAddValue('AvgZCSCNT_4',  AvgZCSCNT_4)
                    logger.logAddValue('AvgCalcPwr_4', AvgCalcPwr_4)

                    if sa.usesa :
                        # add SA values to the log list
                        try :
                            sapower = float(SAPower)
                        except ValueError as ve :
                            print("SAPower ValueError on '{}' -- {}".format(SAPower, ve))
                            sapower = -1.0
                        try :
                            safreq = float(SAFreq)
                        except ValueError as ve :
                            print("SAFreq ValueError on '{}' -- {}".format(SAFreq, ve))
                            safreq = -1.0
                        if options.average :
                            logger.logAddValue('SAPowerAvg', sapower)
                            logger.logAddValue('SAFreqAvg', safreq)
                        else :
                            logger.logAddValue('SAPowerPeak', sapower)
                            logger.logAddValue('SAFreqPeak', safreq)

                    # add the time entry
                    tm_obj = localtime(time())
                    datatime = "{:02d}:{:02d}:{:02d}".format(tm_obj.tm_hour, tm_obj.tm_min, tm_obj.tm_sec)
                    logger.logAddValue('timetaken', datatime)

                    if logger.logResults() :
                        print("Couldn't log values for Client {}.".format(sClientId))
                        finished = True
                        break

                    if peakPower >= options.maxrssi :
                        print("peakPower value exceeded peakPower limit: {} < {} cnt {}\n".format(options.maxrssi, peakPower, iterateCntUp-1))
                        if (iterateCntUp-1) >= 3 :
                            finished = True

    return finished

    # when the "while" is done we're finished

def main(tstList, options, logger) :

    # init the TX communication object
    tx=TXComm(options.txipaddr, int(options.txport), options.debug)
    # setup the SA

    sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)

    comChanNum = '25'
    cota_config = "ND"
    try :
        cota_config, plist = getCotaConfigValues(options)
        comChanNum = plist["Client COM Channel"]
    except Exception as ex :
        print("getCotaConfigValues exception -- {}".format(repr(ex)))

    # Get Transmitter ID
    argv = ["get_charger_id"]
    v = tx.sendGetTransmitter(argv)
    txId = jcs.getTxId(v)

    clientList    = options.clientid.split(',')
    swportList    = options.swports.split(',')
    cablelossList = options.cableloss.split(',')
    aliasList     = options.aliasnames.split(',')

    cacsList = zip(clientList, aliasList, cablelossList, swportList)
    # first register the clients

    print("The Client list: {}".format(repr(clientList)))

    for clientId in clientList :
        try :
            sClientId = str(clientId).strip()
        except ValueError as ve :
            print("ClientId ValueError {} -- {}".format(clientId, ve))
            return
        else :
            argv = ['register_client', sClientId]
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Register client {} failed".format(sClientId))
                return
            # configure the client
            argv = ['client_config', sClientId, '0x6']
            v = tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Client config failed")
                return
            sleep(2)
            # check for a non zero Short ID
            argv = ['client_detail', sClientId]
            v = tx.sendGetTransmitter(argv)
            shortID = jcs.getClientShortID(v)
            if shortID == "0x0000" :
                print("Short ID shows device is not registered: " + shortID)
                return
            #

        ## get the Transmiter data values for logging.

    """ first get the Transmitter data that we want to log
        This will be: TX Id, power level, On AMBs, Good AMBs, Warmup time, Angle, Timestamp
    """
    logger.initFile(TXCMS)

    if options.logtodb :
        logger.initDB(options.dbname, hostnm=DEFAULTDBSERVER)
        logger.initDBTable(options.tblname)
    # add options Dict to log list
    val =  repr(options.__dict__).replace("'","").replace('"',"").replace(","," ").replace("\r\n", " ").replace("\n\r", " ").replace("\n", " ").replace("\r", " ")
    logger.logAddValue("options", val)

    # add txId to log list
    logger.logAddValue("txId", txId)
    logger.logAddValue("AntennaType",options.antennatype)
    logger.logAddValue("CfgComment",options.logcomments)
    logger.logAddValue("CotaConf",cota_config)
    # add OnChannels to log list
    OnChannels, ChanList = getOnChannels(tx)
    verstr = getVersions(tx)
    logger.logAddValue("OnChannels", OnChannels)
    v = tx.sendGetTransmitter(['get_good_channels'])
    logger.logAddValue("GoodChannels", v['Result']['Good Channels'])
    logger.logAddValue("Versions", str(verstr))
    logger.logAddValue("AmbRev", str(ChanList))
    logger.logAddValue("LogFileName", logger.filename)

    tableTS = str(int(logger.timeStampNum))
    ipnum = gethostbyname(options.txipaddr).split(".")[-1]
    CLIENTTABLENAME = "client_{}_{}".format(ipnum, tableTS)
    logger.logAddValue(TXDATACLIENTTABLENAME, CLIENTTABLENAME)
    if sa.usesa :
        SATABLENAME = "sa_{}_{}".format(ipnum, tableTS)
        logger.logAddValue(TXDATASATABLENAME, SATABLENAME)
    logger.logAddValue(TIMESTAMPCOLNAME, logger.timeStamp)
    if logger.logResults() :
        print("Couldn't log values for Transmitter.")
        return

    # make the logging list
    if sa.usesa :
        sacms = list()
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                sacms.append([cmd_data[0], "%s", cmd_data[2]])
            # the the SA
            sa.setSAValue(cmd_data[0], cmd_data[1])

        # init the logging list
        logger.initFile(sacms,how='a')
        if options.logtodb :
            logger.initDBTable(SATABLENAME)
        # log the SA setup data
        for cmd_data in SA_SETUP_LIST :
            if  cmd_data[2] is not None :
                logger.logAddValue(cmd_data[0], cmd_data[1])
        if logger.logResults() :
            print("Couldn't log values for SA.")
            return

        clientcms = CLIENTCMS
        if sa.usesa :
            clientcms.insert(4,["ImageSA", "%s", 'VARCHAR(100)'])

            if options.average :
                clientcms.insert(4,["SAPowerAvg", "%3.2f", 'VARCHAR(10)'])
                clientcms.insert(4,["SAFreqAvg", "%6.4e", 'VARCHAR(10)'])
            else :
                clientcms.insert(4,["SAPowerPeak", "%3.2f", 'VARCHAR(10)'])
                clientcms.insert(4,["SAFreqPeak", "%6.4e", 'VARCHAR(10)'])

        # setup the logging file
    if options.debug :
        print("FNAME={}".format(logger.filename))
        print("COLFMT={}".format(logger.colformat))

    logger.initFile(clientcms,how='a')
    if options.logtodb :
        logger.initDBTable(CLIENTTABLENAME)
    # the client table is the last one inited so it will be the one where data is logged
    # it's data names will be expected to be used.

    calJustOnce = False
    finished = False
    LoopStartTime = time()
    beginingListIndex = 0
    while not finished :
        for _ in range(7) :
            winsound.Beep(440,200)
            sleep(0.2)
            winsound.Beep(880,200)
            sleep(0.2)

        _, _, _, elapsedTime = DurationCalc(LoopStartTime)
        userIn = ""
        while userIn.strip() == "" :
            userIn = raw_input("""
Type:
 'run' to continue.
 'quit' to end this test.
 'cal' to calibrate at the beginning of this iteration -- current={}.
 'warm' to toggle skiping the long warmup wait -- current={}.
 (iteration time {})

 """.format(repr(calJustOnce), "Don't skip" if beginingListIndex == 0 else "Skip", elapsedTime))

            if userIn.strip() != "" :
                if userIn.strip().lower() == "quit" :
                    print("\nExiting per user input '{}'\n".format(userIn.strip()))
                    finished = True
                    break
                elif userIn.strip().lower() == 'run' :
                    break
                elif userIn.strip().lower() == 'cal' :
                    calJustOnce = not calJustOnce
                    userIn = ''
                elif userIn.strip().lower() == 'warm' :
                    beginingListIndex = 1 - beginingListIndex
                    userIn = ''
                elif userIn.strip() == "" :
                    break
        if finished :
            break
        LoopStartTime = time()

        powerLevel = 0
        for itTstDict  in tstList[beginingListIndex:] :

            try :
                itTstDict['warmup'] = int(itTstDict.get('warmup', 10))
            except ValueError as e :
                print("ValueError converting warmup and txangle {}".format(repr(e)))
                finished = True
                break

            try :
                pwrLvl = int(itTstDict.get('powerlevel', 13))
            except ValueError as e :
                print("ValueError converting powerlevel {}".format(repr(e)))
                pwrLvl = 16
            pwrLvl = max(pwrLvl, 13)
            pwrLvl = min(pwrLvl, 20)

            if powerLevel != pwrLvl :
                powerLevel = pwrLvl
                if not isTaperRunning() :
                    argv = ['set_power_level', powerLevel]
                    v = tx.sendGetTransmitter(argv)
                    if jcs.checkForSuccess(v) :
                        print("Set Power Level failed")
                        finished = True
                        break
                argv = ['get_power_level']
                v = tx.sendGetTransmitter(argv)
                pl =  jcs.getTransmitterPowerLevel(v)
                if pl != powerLevel :
                    print("Power level incorrect get={} vs set={}".format(pl, powerLevel))
                    powerLevel = pl
            itTstDict['powerlevel'] = powerLevel

            # this function also logs all the client type data to the file and database.
            # get charging client details
            itTstDict.update({'caljustonce': calJustOnce})
            calJustOnce = False
            try :

                finished = runthecharacter(sa, tx, cacsList, itTstDict, logger, options, comChanNum)
                if finished :
                    print("There was an error in the runthecharacter routine\n")
            except KeyboardInterrupt as ki :
                print("Sigint = 2 received -- KeyboardInterrupt exiting --- {}".format(repr(ki)))
                finished = True

            if finished :
                break

    for client in clientList :
        sClientID = str(client)
        argv = ['stop_charging', sClientID]
        print("At End : Stop charging client {} argv = {}\n".format(sClientID, repr(argv)))
        v = tx.sendGetTransmitter(argv)


def makeOptionTypeDict(myParse) :
    """ makeOptionTypeDict -- creates a dictionary of option keys associated with a list
                              of the option type and the options default value.
        returns a dictionary of {option name => [ option type, option default]}
    """
    rtndict = dict()
    for i in range(len(myParse.option_list)) :
        if myParse.option_list[i].dest is not None : # ignores the help option
            rtndict.update({myParse.option_list[i].dest: [myParse.option_list[i].type, myParse.option_list[i].default]})
    return rtndict

def DurationCalc(START_TIME) :
    duration = int(time() - START_TIME)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    return hrs, mins, secs, Duration

if __name__ == '__main__' :

    START_TIME = time()
    #parser = OptionParser(option_class=myOption)
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',default="C:\\Users\\Public\\ReflectChamber\\ReflectChamberConfs.py", help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',default="cfgReflectNarda", help="Use a config from the file specified by --cfgfile")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',default="C:\\Users\\Public\\ReflectChamber\\ReflectChamberLists.py", help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',default="testlistNardaTest", help="Use the named list from the file (.py).")
    parser.add_option("","--saport",       dest='saport',       type=int,  action='store',default=sSAPORT,help="Signal Analyzer telnet port number.")
    parser.add_option("","--saip",         dest='saipaddr',     type=str,  action='store',default=sSAIP,help="Signal Analyzer network address")

    parser.add_option("","--antennatype",  dest='antennatype',  type=str,  action='store',default="4 Circular Patches, one per port",help="Define a log value for antenna type")
    parser.add_option("","--logcomments",  dest='logcomments',  type=str,  action='store',default="",help="Define an arbitrary log value")

    parser.add_option("","--txport",       dest='txport',       type=int,  action='store',default=sTXPORT,help="Transmitter telnet port number.")
    parser.add_option("","--txip",         dest='txipaddr',     type=str,  action='store',default='10.10.1.170',help="Transmitter network address.")

    parser.add_option("","--clientid",     dest='clientid',     type=str,  action='store',default=None,help="Client IDs to use (comma separated list).")
    parser.add_option("","--swports",      dest='swports',      type=str,  action='store',default=None,help="RF switch port to use (comma separated list).")
    parser.add_option("","--cableloss",    dest='cableloss',    type=str,  action='store',default=None,help="Cable loss for each port to use (comma separated list).")
    parser.add_option("","--aliasnames",   dest='aliasnames',   type=str,  action='store',default=None,help="Alias name for each client Id to (comma separated list).")

    parser.add_option("","--maxrssilevel", dest='maxrssi',      type=float,action='store',default='28.0',help="Minimum link quality to validate a client.")

    parser.add_option("","--logfile",      dest='logfile',      type=str,  action='store',default="NardaTestData_{}.csv",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",    dest='directory',    type=str,  action='store',default="/var/www/www/Logs/",help="Log file directory.")
    parser.add_option("","--console",      dest='console',                 action='store_true',default=False,help="Turn on printing to console")
    parser.add_option("","--average",      dest='average',                 action='store_true',default=False,help="Turn on average hold SA data")
    parser.add_option("","--logtodb",      dest='logtodb',                 action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("","--dbname",       dest='dbname',       type=str,  action='store',default=TESTDBNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--tblname",      dest='tblname',      type=str,  action='store',default=TXTBLNAME,help="Set the DataBase name at ossia-build")
    parser.add_option("","--saimage",      dest='saimage',      type=int,  action='store',default=0,help="Capture an SA Image on every data point.")

    parser.add_option("-d","--debug",      dest='debug',                   action="store_true",default=False,help="print debug info.")

    (options, args) = parser.parse_args()

    import imp
    if len(args) > 0 :
        print("Extra command line args, exiting")
        print(repr(args))
    elif options.cfgfile is not None :
        TTYPE = 0
        TDEF = 1
        fname = "{}{}".format(ROOTDIRPATH, options.cfgfile)
        if fname == '' :
            fname = sys.argv[0].replace('.', 'Conf.')
        cfgmod = imp.load_source(options.cfgname, fname)
        cfg = eval("cfgmod."+options.cfgname)
        optType = makeOptionTypeDict(parser)
        for k in cfg.keys() :
            try :
                if optType[k][TTYPE]  == 'int' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = int(str(cfg[k]))
                elif optType[k][TTYPE]  == 'float' :
                    if str(cfg[k]) == '' or str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    else :
                        options.__dict__[k] = float(str(cfg[k]))
                elif optType[k][TTYPE]  is None :
                    options.__dict__[k] = optType[k][TDEF]  # set the default value
                    try :
                        options.__dict__[k] = eval(cfg[k])
                    except NameError :
                        options.__dict__[k] = False
                    except TypeError :
                        options.__dict__[k] = cfg[k]

                elif optType[k][TTYPE]  == 'string' :
                    if str(cfg[k]) == 'DeFault' : # check for a default value
                        options.__dict__[k] = optType[k][TDEF] # set the default value
                    elif str(cfg[k]) == 'None' :
                        options.__dict__[k] = None
                    else :
                        options.__dict__[k] = str(cfg[k])
                else :
                    options.__dict__[k] = str(cfg[k])
                #print("<pre>name={} def={} type={} in={} cur={}</pre>\n".format(k, optType[k][TDEF], optType[k][TTYPE], cfg[k], options.__dict__[k]))
            except ValueError as ve :
                print("<pre>{} -- Incorrect option value {} -- {}</pre>\n".format(k, cfg[k], repr(ve)))
                exit(2)
            except IndexError as ie :
                print("<pre>{} -- No such option -- {}</pre>\n".format(k, repr(ie)))
                exit(3)
            except Exception as e :
                print("<pre>{} -- Unknown exception -- {}</pre>\n".format(k, repr(e)))
                exit(4)

    fname = sys.argv[0].replace('.', 'TestList.')

    if options.testlistfile is not None :
        fnm = "{}{}".format(ROOTDIRPATH, options.testlistfile)
        #fnm = options.testlistfile
        if fnm != '' :
            fname = fnm
    if options.debug :
        print("testlistfile = {}".format(fname))

    tstlstmod = imp.load_source(options.testlistname, fname)
    testlist = eval("tstlstmod."+options.testlistname)

    if options.debug :
        print("options = {}".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
        print("testlist = {}".format(repr(testlist).replace('},','},\n')))

    logger = LogResults(options.logfile, options.directory, console=options.console, debug=options.debug)
    main(testlist, options, logger)
    hrs, mins, secs, Duration = DurationCalc(START_TIME)

    try :
        logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
    except Exception as e :
        print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
