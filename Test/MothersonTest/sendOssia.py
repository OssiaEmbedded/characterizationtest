#! /usr/bin/python

import socket, errno
from time import sleep, gmtime, strftime
from optparse import OptionParser
from cgi import escape as cgiEscape
import sys


def sendgetdata(options, sendthis, shootandgo=False):
    myIpaddr = socket.gethostbyname(options.tstequip)
    mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    try :
        mySocket.connect((myIpaddr, int(options.portnumber)))
        mySocket.settimeout(1.0)
    except Exception() as ee :
        print cgiEscape("There was a connect exception {}".format(repr(ee)))
        exit(1)

    xx = mySocket.send( sendthis + "\r\n" )
    if options.debug :
        print "socket send returned {} -- {}\n".format(repr(xx), sendthis)
    data = bytearray('shootandgo')
    if not shootandgo :
        cnt = 60
        #sleep(0.5)
        data = bytearray()
        d = bytearray()
        while cnt > 0 :
            try :
                d = bytearray(mySocket.recv(8192))
                if options.debug :
                    print cgiEscape(str(d))
                sys.stdout.flush()
                data += d
            except socket.timeout :
                print cgiEscape(str(cnt))
                sys.stdout.flush()
                cnt -= 1
            except socket.error as se :
                if se.errno == errno.WSAECONNRESET :
                    print "socket error {} -- {}\n".format(cgiEscape(str(cnt)), repr(se))
                sys.stdout.flush()
                cnt -= 1
            if 'SUCCESS' in data :
                break
        data = data.lstrip().rstrip()
    mySocket.close()
    return data
 
parser = OptionParser()

parser.add_option("-t","--tstequip",
                   dest="tstequip",
                   action="store",
                   default="10.10.1.160",
                   help="Network name or IP address of ossiaServer (default='%default')")

parser.add_option("-p","--port",
                   dest="portnumber",
                   type=int,
                   action="store",
                   default="7000",
                   help="Port number of the ossiaServer telnet server (default='%default')")

parser.add_option("-d","--debug",
                   dest="debug",
                   action="store_true",
                   default=False,
                   help="Turn on debugging")

(option, args) = parser.parse_args()


if len(args) > 0 :
    sDat = "COMMANDVAL"
    shootandgo = False
    for i in range(len(args)) :
        if args[i] == 'shootandgo' :
            shootandgo = True
            continue
        sDat += "/"+args[i].replace('\\','')
    if option.debug :
        print "sDat = '{}' shootandgo = {}\n".format(sDat, repr(shootandgo))
    data = sendgetdata(option, sDat, shootandgo)
    print "{}".format(data)
