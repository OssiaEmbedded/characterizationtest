""" actuator.py: A class to connect to an Ossia linear actuator server. The functions included are
specific to their application at Ossia Inc.

Version : 1.0.0
Date : Jan 27 2017
Copyright Ossia Inc. 2017

"""
from __future__ import absolute_import, division, print_function #, unicode_literals
import socket, sys
from time import sleep, gmtime, strftime

COMMANDVAL = "COMMANDVAL"

class Actuator(object) :

    def __init__(self, ipaddr, portnumber, debug=False) :
        self.ipaddress = socket.gethostbyname(ipaddr)
        self.portnum = 7000 if portnumber is None else portnumber
        try :
            self.portnum = int(self.portnum)
        except ValueError as ve :
            print("ValueError on portnumber '{}' -- {}\n".format(self.portnum, repr(ve)))
            self.portnum = 7000
        self.debug = debug
        self.mySocket = None

    def sendData(self, sendthis) :
        """ sendData -- Send the input string to the ipaddress and port
                        of the actuator.
        """
        retval = True
        if self.mySocket is None :
            self.mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
            try :
                self.mySocket.connect((self.ipaddress, self.portnum))
                self.mySocket.settimeout(1.0)
            except :
                if self.debug :
                    print("There was a connect exception ")
                retval = False

        self.mySocket.send( sendthis + "\r\n" )

        return retval

    def testSock(self) :
        return self.mySocket

    def getData(self, nonBlock=False) :
        data = bytearray()
        d = bytearray()
        cnt = 60
        while cnt > 0 :
            try :
                d = bytearray(self.mySocket.recv(8192))
                data += d
            except socket.timeout :
                if self.debug :
                    print(cnt)
                cnt -= 1
            except socket.error as se :
                data = "Error -- {}".format(se[1])
                if self.debug :
                    print("SocketError -- error No. {} -- {}\n".format(se[0], data))
                sys.stdout.flush()
            if 'SUCCESS' in data :
                cnt = 0
                break
            if nonBlock :
                break

        if self.debug :
            print(data)
        data = data.lstrip().rstrip()
        if not nonBlock or cnt <= 0 :
            self.mySocket.close()
            self.mySocket = None

        return data

    def sendGetData(self, sendthis) :
        """ sendGetData -- Send the input string to the ipaddress and port
                           of the actuator.
        """

        cnt = 60
        self.mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        try :
            self.mySocket.connect((self.ipaddress, self.portnum))
            self.mySocket.settimeout(1.0)
        except :
            if self.debug :
                print("There was a connect exception ")
            return None

        self.mySocket.send( sendthis + "\r\n" )
        sleep(0.5)
        data = bytearray()
        d = bytearray()
        while cnt > 0 and 'nonblocking' not in sendthis :
            try :
                d = bytearray(self.mySocket.recv(8192))
                data += d
            except socket.timeout :
                if self.debug :
                    print(cnt)
                cnt -= 1
            if 'SUCCESS' in data :
                break
        if self.debug :
            print(data)
        data = data.lstrip().rstrip()
        self.mySocket.close()
        self.mySocket = None

        return data

    def makeCommandStringFromList(self, strlist) :
        """ sendCommandString -- Create a command string from a list of strings and return it.
                                 input arg is a list containing strings.
        """
        if self.debug :
            print("Argument list kwargs={}".format(repr(strlist)))
        strval = COMMANDVAL
        for i in range(len(strlist)) :
            strval += "/" + strlist[i]
        return strval

    def makeCommandString(self, **kwargs) :
        """ sendCommandString -- Create a command string from input args and return it.
                                 input args are arg0=, arg1=, etc.
        """
        if self.debug :
            print("Argument list kwargs={}".format(repr(kwargs)))
        strval = COMMANDVAL
        for i in range(len(kwargs)) :
            key = 'arg'+str(i)
            if kwargs.get(key, None) is not None :
                strval += "/"+kwargs.get(key, "")
            else :
                print("Unknown argument {} kwargs={}".format(key, repr(kwargs)))
        return strval

    def sendCommandString(self, **kwargs) :
        """ sendCommandString -- Create a command string from input args and send it.
                                 input args are arg0=, arg1=, etc.
        """
        if self.debug :
            print("Argument list kwargs={}".format(repr(kwargs)))
        strval = COMMANDVAL
        for i in range(len(kwargs)) :
            key = 'arg'+str(i)
            if kwargs.get(key, None) is not None :
                strval += "/"+kwargs.get(key, "")
            else :
                print("Unknown argument {} kwargs={}".format(key, repr(kwargs)))
        data = self.sendGetData(strval)
        return data

if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        a = Actuator(sys.argv[1], 7000, debug=True)

        #str1 = "COMMANDVAL/setMSResolution/16th"
        #str2 = "COMMANDVAL/setStepTime/1500/l"
        strlst = [ "COMMANDVAL/setPosition/0",
                   "COMMANDVAL/setPosition/1180",
                   "COMMANDVAL/setPosition/1500",
                   "COMMANDVAL/setPosition/1000",
                   "COMMANDVAL/setPosition/1200",
                   "COMMANDVAL/setPosition/1900"]

        print(a.sendCommandString(arg0="setMSResolution", arg1='16th', arg2='l'))
        print(a.sendCommandString(arg0="setStepTime", arg1='1500', arg2='l'))

        for i in range(15) :
            try :
                index = i % len(strlst)
                print("Command is '{}'".format(strlst[index]))
                data = a.sendGetData(strlst[index])
                print("DATA = ''{}''".format(str(data)))
                print("sleep {} secs".format(index+2))
                sleep(index + 2)
            except KeyboardInterrupt :
                break

        
   

