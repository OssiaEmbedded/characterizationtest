BEGIN{thenext=0; print "JSON_CMD_DICT = {";}
thenext == 3     { thenext=4; jstr=$0; gsub("1234", "%s", jstr)}
$1 ~ /JSON/      { thenext=3}
thenext == 1     { thenext=2; name = $1; numargs = NF - 1;}
$1 == "Command:" { thenext=1}
thenext == 4     { thenext=0; print "'"name"': {'jstr': ""'"jstr"', 'numofargs': "numargs"},";}
END {print "}"; 
     print ""; 
     print ""; 
     print "if __name__ == '__main__' :"; 
     print "    for k in JSON_CMD_DICT.keys() :";
     print "        print k";
     print "        print JSON_CMD_DICT[k].keys()";
     print "        print JSON_CMD_DICT[k]['numofargs']";
     print "        print JSON_CMD_DICT[k]['jstr']";
}


