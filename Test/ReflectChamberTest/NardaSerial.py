''' Narda Serial -- a module to provide a connection class to other test scrips

Unit tests are run by executing this module separately from other users.

Version : 1.0.0
Date :  Oct 20 2017
Copyright Ossia Inc. 2017

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

import serial

class NardaSerial(object) :

    nardaErrorCodes = {# "0"   : "no error",
                       "401" : "remote command is not implemented in the remote module",
                       "402" : "invalid parameter",
                       "403" : "invalid count of parameters",
                       "404" : "invalid parameter range",
                       "405" : "last command is not completed",
                       "406" : "answer time between remote module and application module is too high",
                       "407" : "wrong quit message from application module",
                       "408" : "invalid or corrupt data",
                       "409" : "error while accessing the EEPROM",
                       "410" : "error while accessing hardware resources",
                       "411" : "command is not supported in this version of the firmware",
                       "412" : "remote is not activated (please send 'REMOTE ON;' first)",
                       "413" : "command is not supported in the selected mode",
                       "414" : "memory of data logger is full",
                       "415" : "defragmentation of flash file system is required",
                       "416" : "option code is invalid",
                       "417" : "incompatible version",
                       "418" : "no Probe",}

    nardSetupCommands = ["auto_light off;",
                         "auto_power off;",
                         "result_type max;",
                         "result_unit nw/cm^2;",
                         "meas_view x-y-z;",
                         "sample_rate 50;",
                         "freq 2450100000;",
                         "avg_ime 2;",
                         "zero;"]
        
    def __init__(self, wincomport, baudrate=460800) :

        self.serial = serial.Serial()
        self.comport = wincomport
        self.baudrate = baudrate
        self.serial.comport = wincomport
        self.serial.baudrate = baudrate
        self.serial.timeout = 0.5

    def openConnection(self) :
        rtnval = self.serial.is_open
        if not rtnval :
            self.serial.open()
            rtnval =  self.serial.is_open
        if not rtnval :
            print("Serial port {} did not open".format(self.comport))
        return rtnval

    def closeConnection(self) :
        self.serial.close()
        rtnval =  self.serial.is_open
        if rtnval :
            print("Serial port {} did not close".format(self.comport))
        return rtnval

    def sendGetCommand(self, command) :
        retvalList = list()
        retval = self.openConnection()
        if retval :
            try :
                rval = self.serial.write(command)
                rtnval = self.serial.read(200).strip(" ;\n\r\t")
                rvalList = rtnval.split(',')
                rval = rvalList[-1].strip(" ;\n\r\t")
                if self.nardaErrorCodes.get(rval, None) is not None :
                    # error
                    print("Error returned by probe = {}\n".format(self.nardaErrorCodes[rtnval]))
                    rtnval = False
            except IOError as ioe :
                print("IO Error on write or read -- {}\n".format(repr(ioe)))
                rtnval = False
            retval = self.closeConnection()
        return rtnval, rvalList

    def setUpNardaProbe(self) :
        for command in self.nardSetupCommands :
            retval, _ = self.sendGetCommand(command)
            if not retval  :
                break
        return retval

    def getDeviceInfo(self) :
        retval, retvalList = self.sendGetCommand("device_info?;")
        if not retval :
            retvalList = list()
        else :
            for i in range(len(retvalList)) :
                retvalList[i] = retvalList[i].strip()
        return retvalList, retval

    def getMeassuredData(self) :
        retval, retvalList = self.sendGetCommand("meas?;")
        if not retval :
            retvalList = list()
        return retvalList, retval

    def getBattery(self) :
        retval, retvalList = self.sendGetCommand("battery?;")
        if not retval :
            retvalList = list()
        return retvalList, retval

    def exitCommMode(self) :
        # set the auto power back to 6 secs.
        retval, _ = self.sendGetCommand("auto_power 6;")
        retval, _ = self.sendGetCommand("remote off;")

if __name__ == "__main__" :
    print("Unit tests start\n")

    comPort = 'COM8'
    if len(sys.argv) > 1 :
        # expect first arg to be the com port
        comPort = sys.argv[1]

    nard = NardaSerial(wincomport=comPort)

    retval = nard.setUpNardaProbe()
    print("setup returned {}\n", repr(retval))
    retvallst = nard.getDeviceInfo()
    print("device info returned {}\n", repr(retvallst))
    retvallst = nard.getMeassuredData()
    print("meassured data returned {}\n", repr(retvallst))
    retvallst = nard.getBattery()
    print("battery returned {}\n", repr(retvallst))
    nard.exitCommMode()
    print("Unit tests done\n")

