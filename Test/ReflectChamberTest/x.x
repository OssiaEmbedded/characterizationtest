0 no error
401 remote command is not implemented in the remote module
402 invalid parameter
403 invalid count of parameters
404 invalid parameter range
405 last command is not completed
406 answer time between remote module and application module is too high
407 wrong quit message from application module
408 invalid or corrupt data
409 error while accessing the EEPROM
410 error while accessing hardware resources
411 command is not supported in this version of the firmware
412 remote is not activated (please send "REMOTE ON;" first)
413 command is not supported in the selected mode
414 memory of data logger is full
415 defragmentation of flash file system is required
416 option code is invalid
417 incompatible version
418 no Probe
