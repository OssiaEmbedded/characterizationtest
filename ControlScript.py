r"""
THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2023 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
"""

from time import sleep
"""
   This script is the main/only control script. Its main purpose is to
take the test list and iterate through it. 

The test list is a list of dictionaries. Each dictionary describes in Known to ControlScript
names what should take place. The names are :
    linposition  -- The linear actuator distance from the TX in millimeters.
    rotposition  -- The rotary actuator angle between 90 and -90 degrees
    numbcnpwrcyc -- The number of beacon power cycles and power readings to perform for this entry
    rotshaketime -- If not None will tell the rotary position to oscillate at the rotposition for
                     rotshaketime secs (float)
    dwelltime    -- Number of seconds (float) to wait at the position between beacon/power sets
                    If zero they are controlled by the system_test script running on the TX.
    enablepwr    -- If True Enables power on the TX If false does nothing.
    disablepwr   -- If True Disables power on the TX If false does nothing.
    measurepwr   -- Command the system_test script to measure the power at the power meter
    measurerxbat -- Command the system_test script to get the RX battery level in mv
    calibrate    -- Command the system_test script to send a calibration event to the TX
    fastloop     -- An interactive loop to tell the system_test script to send n number of beacons
                    and collect n number of power values one for each beacon.
"""
version = '1.0.0'

SYS_TST_RESP_TERM="Enter selection: "
searchKey = 'Loop Measurements'
def parseFastLoopResponse(response) :
    """ find the index of searchKey split on : then comma
    """
    index = response.find(searchKey)
    rtnStr = ''
    if index < 0 :
        rtnStr = f"Error '{searchKey}' not found {response}"
        rtnStr = f"{35.01}, {rtnStr}"
    else :
        measLst = response[index:].split(':')[1].strip().split(',')
        pwrStr = ""
        dSum = 0.0
        cnt = 0
        # return a list that only has the pwr data in it.
        for _, m in enumerate(measLst) :
            try :
                pwr =float(m.strip())
            except :
                pass
            else :
                dSum += pwr
                cnt += 1
                pwrStr += f",{pwr:5.2f}"
        avg = 35.00
        if cnt > 0 :
            avg = dSum / cnt
        rtnStr = f"{avg:5.2f}{pwrStr}"
    return rtnStr 

def parseBatteryStatResponse(response) :
    """ Collect the returned values into a dictionary
    """
    lineLst = response.split("\n")
    rtnStr = ""
    for line in lineLst :
        if ":" in line :
            key, data = tuple(line.split(':'))
            rtnStr += f"{key.strip()},{data.strip()},"
    return rtnStr.rstrip(',')

def parsePositionResponse(response) :
    """ CMD=COMMANDVAL/setPosition/0/mc1
          -- 59

        Set Position True
    """
    debugVal = None
    resp = "Set Position not found."
    if "COMMANDVAL" in response :
        resp = ""
        lst = response.split("\n")
        for i, line in enumerate(lst) :
            indx = line.find("COMMANDVAL")
            if indx > 0 :
                resp += line[indx+11:] + " "
            indx = line.find("SUCCESS")
            if indx > 0 :
                resp += lst[i-1] + " "
                resp += line[indx:]
    else :
        debugVal = f"response == '{response}'\n"
    return resp, debugVal

class ControlScript() :
    def __init__(self, outputupdate, stopScript, actObj, txObj, txrxFreq, testlist) :
        self.printStr        = outputupdate
        self.actSerial       = actObj
        self.txSerial        = txObj
        self.testlist        = testlist
        self.stopScriptEvent = stopScript
        self.txrxFreq        = txrxFreq

    def main(self) :
        # get the versions from the TX

        for indexnum, testdict in enumerate(self.testlist) :
            self.printStr(f"{indexnum} testdict={testdict}")
            # first get the test config values
            linposition  = testdict.get("linposition",  None)
            rotposition  = testdict.get("rotposition",  None)

            registerrx   = testdict.get("registerrx", None)
            unregisterrx = testdict.get("unregisterrx", None)
            restoretx    = testdict.get("restoretx", None)

            numbcnpwrcyc = testdict.get("numbcnpwrcyc", 0)
            fastloop     = testdict.get("fastloop",     0)

            rotshaketime = testdict.get("rotshaketime", None)
            dwelltime    = testdict.get("dwelltime",    0.0)

            enablepwr    = testdict.get("enablepwr",    False)
            disablepwr   = testdict.get("disablepwr",   False)

            setuptxrx    = testdict.get("setuptxrx",    None)

            measurepwr   = testdict.get("measurepwr",   False)
            measurerxbat = testdict.get("measurerxbat", False)

            calibrate    = testdict.get("calibrate",    False)
            rxcommand    = testdict.get("rxcommand",    None)

            if self.stopScriptEvent.wait(dwelltime+0.5) :
                break
            if linposition is not None:
                response = self.actSerial.sendPosLin(linposition)
                response, dbg = parsePositionResponse(response)
                self.printStr(f"{indexnum} Set Linear Position = {linposition}. {response}") # print out 
                if dbg is not None :
                    self.printStr(f"{indexnum} Set Linear Position DEBUG = {linposition}. {dbg}") # print out 
            if rotposition is not None :
                response = self.actSerial.sendPosRot(rotposition)
                response, dbg = parsePositionResponse(response)
                self.printStr(f"{indexnum} Set Rotary Position = {rotposition}. {response}") # print out 
                if dbg is not None :
                    self.printStr(f"{indexnum} Set Rotary Position DEBUG = {rotposition}. {dbg}") # print out 

            if rxcommand is not None :
                response = self.txSerial.sendArbCommand(f"t {rxcommand}", SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} {rxcommand} == {response}")
            #
            if registerrx is not None :
                response = self.txSerial.sendArbCommand(f"reg", SYS_TST_RESP_TERM).strip()
                resp = response.split('\n')[-1]
                self.printStr(f"{indexnum} Register rx == {resp}")
            if unregisterrx is not None :
                response = self.txSerial.sendArbCommand(f"unreg", SYS_TST_RESP_TERM).strip()
                resp = response.split('\n')[-1]
                self.printStr(f"{indexnum} Unregister rx == {resp}")
            if restoretx is not None :
                response = self.txSerial.sendArbCommand(f"rest", SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Restore tx == {response}")
            #
            # setup and deal with TX/RX
            if setuptxrx :
                response = self.txSerial.sendArbCommand(f"freq {self.txrxFreq}", SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Set Frequency TX/RX == {response}") # print out 
                response = self.txSerial.sendArbCommand('s', SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Setup TX/RX == {response}") # print out 

            if enablepwr :
                response = self.txSerial.sendArbCommand("e", SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Enable Charging. Response -- {response}") # print out 
            elif disablepwr :
                response = self.txSerial.sendArbCommand("d", SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Disable Charging. Response -- {response}") # print out 

            if calibrate :
                response = self.txSerial.sendArbCommand('c', SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum} Calibrate TX. Response -- {response}") # print out 

            if measurerxbat :
                response = self.txSerial.sendArbCommand('w', '$ ')
                resStr = parseBatteryStatResponse(response)
                self.printStr(f"{indexnum},{linposition},{rotposition},RXBATTERYSTAT,{resStr}") # print out 

            if fastloop > 0 :
                # expect power values back
                response = self.txSerial.sendArbCommand(f'l {fastloop}', SYS_TST_RESP_TERM, 18)
                if len(response) > 0 and "Error" not in response :
                    resp = parseFastLoopResponse(response)
                    self.printStr(f"{indexnum},{linposition},{rotposition},POWERDATA,{resp}") # print out 
                else :
                    self.printStr(f"{indexnum},{linposition},{rotposition},Error Fast Loop RF Power,{response}") # print out 
                    self.printStr(f"{indexnum} Error Fast Loop RF Power. Exiting ControScript") # print out 
                    break # end the test
            elif numbcnpwrcyc > 0 :
                pwrvalues = ''
                # send new command to start a loop of numbcnpwrcyc
                for i in range(numbcnpwrcyc) :
                    response += self.txSerial.sendArbCommand('b', SYS_TST_RESP_TERM)
                    pwrvalues += response
                    self.printStr(f"{indexnum} Controlled loop RF Power {i}. Response -- {response}") # print out 
                self.printStr(f"{indexnum},{linposition},{rotposition},Controlled loop RF Power. Power Values -- {pwrvalues}") # print out 

            if measurepwr   :
                response = self.txSerial.sendArbCommand('m', SYS_TST_RESP_TERM)
                self.printStr(f"{indexnum},{linposition},{rotposition},Measure RF Power,{response}") # print out 

