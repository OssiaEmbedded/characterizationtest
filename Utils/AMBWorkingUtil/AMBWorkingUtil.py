''' AMBWorking.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 0.0.1
Date : July 12 2017
Copyright Ossia Inc. 2017

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from subprocess import check_output as spcheck_output
from LIB.sendTestServer import SendTestServer

DEFAULTDBSERVER = 'ossia-build'
TXTBLNAME = 'TxAmbCheck'
AMBDBNAME = 'CheckDB'
AMBCOLNAME = 'AmbCheckTable'
TIMESTAMPCOLNAME="DataTimeStamp"


SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', '9.0 MHz', 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '100 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

TX_CMS  = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
           ['IPAddress', '%s', 'VARCHAR(16)'],
           ['Tolerance', '%5.2f', 'VARCHAR(8)'],
           ['PostTestNotes', '%s', 'VARCHAR(512)'],
           ["Duration", "%s", 'VARCHAR(20)'],
           [AMBCOLNAME, '%s', 'VARCHAR(50)'],
           ['LogFileName', '%s', 'VARCHAR(150)'],
           ['SACenterFreq', '%s', 'VARCHAR(15)']]

AMB_CMS = [['DiffValue', '%5.2f','VARCHAR(8)'],
           ['BaseDiff', '%5.2f','VARCHAR(8)'],
           ['XmitDiff', '%5.2f','VARCHAR(8)'],
           ['BaseMin', '%5.2f','VARCHAR(8)'],
           ['BaseMax', '%5.2f','VARCHAR(8)'],
           ['XmitMin', '%5.2f','VARCHAR(8)'],
           ['XmitMax', '%5.2f','VARCHAR(8)'],
           ['XmitFreq', '%6.4e','VARCHAR(10)'],
           ['AmbNumber', '%d','VARCHAR(8)'],
           ['Cluster', '%d','VARCHAR(8)'],
           ['Unit', '%d','VARCHAR(8)'],
           ['StatusRev', '%s','VARCHAR(50)'],
           ['OutOfTol', '%s', 'VARCHAR(8)'],
           ['ComChan', '%s','VARCHAR(50)']]

class AMBTest(object):

    def __init__(self, options) :
        self.txhostname = options.txipaddr.lower()
        self.txip = gethostbyname(self.txhostname)
        self.txport = options.txport
        self.srvport = options.srvport

        self.sahostname = options.saipaddr
        self.saip = gethostbyname(self.sahostname)
        self.saport = options.saport
        self.debug = options.debug
        self.options = options
        self.cotaConfig = None

        self.tx=TXComm(options.txipaddr, int(options.txport), options.debug)
        self.sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
        self.sts=SendTestServer(self.txip, self.srvport, self.debug)

        logFile = "{}_{}".format(options.logfile, options.txipaddr)+"_{}.csv"

        self.logger = LogResults(logFile, options.directory, console=options.console, debug=options.debug)

    def main(self) :
        try :
            self.Tolerance = float(options.tolerance)
        except ValueError as ve :
            print("ValueError on Tolerance conversion -- {}\n".format(repr(ve)))
            self.Tolerance = 10.0

        self.logger.initFile(TX_CMS)

        if options.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('IPAddress', self.txhostname)
        self.logger.logAddValue('Tolerance', self.Tolerance)
        self.logger.logAddValue('PostTestNotes', "")
        #self.logger.logAddValue('SACenterFreq', options.sacentfreq)

        AMBTABLENAME = "amb_{}_{}".format("Tile", int(self.logger.timeStampNum))
        self.logger.logAddValue(AMBCOLNAME, AMBTABLENAME) 
        self.logger.logAddValue('LogFileName', self.logger.filename)
        self.logger.logResults()

        self.logger.initFile(AMB_CMS, how='a')
        if self.options.logtodb :
            self.logger.initDBTable(AMBTABLENAME)

        chanlist = options.channumber.split(",")

        for chan, freq in [['24', '2.4601 GHZ'], ['25', '2.4501 GHZ'], ['26', '2.4401 GHZ']] :
            if options.channumber.lower() != 'all' and chan not in chanlist :
                continue

            for cmd_data in SA_SETUP_LIST :
                if cmd_data[0] == 'centerFrequency' :
                        cmd_data[1] = freq
                if cmd_data[0] == 'referenceLevel' :
                        cmd_data[1] = self.options.cableloss
            # set the SA
                self.sa.setSAValue(cmd_data[0], cmd_data[1])
            sleep(1)

            self.cotaConfig = None
            self.changeComChannel(chan)
            self.iterateXMTRs()
            if options.channumber.lower() != 'all' :
                break

    def setXMTRon(self, cmd) :
        strval = "sudo xcvr_mode -t {}".format(cmd)
        if self.debug :
            print("strval = '{}'\n".format(strval))
        args = ["execCommand", strval]
        val = self.sts.sendCommand(args)
        if self.debug :
            print("val={}\n".format(val))
        rtnval = True
        if "refused" in val :
            rtnval = False
        return rtnval
    
    def setXMTRoff(self) :
        strval = " sudo xcvr_mode -r"
        if self.debug :
            print("strval = '{}'\n".format(strval))
        args = ["execCommand",  strval]
        val = self.sts.sendCommand(args)
        if self.debug :
            print("val={}\n".format(val))
    
        rtnval = True
        if "refused" in val :
            rtnval = False
        return rtnval
    
    def getSAPwrPeak(self) :
        self.sa.avgHold = ""
        sleep(1)
        self.sa.peakSearch = ""
    
        # get the SA data
        self.sa.triggerHold = ''
        SAP, SAF = self.sa.peakSearch
        try :
            SAPower = float(str(SAP))
            SAFreq  = float(str(SAF))
        except ValueError :
            SAPower =  0.0
            SAFreq  =  0.0
    
        self.sa.triggerClear = ''
        self.sa.clearHold = ''
    
        if self.debug :
            print("SAPeakPower = '{} SAFreq = {}'\n".format(SAPower, SAFreq))
        return SAPower, SAFreq

    def getSAPwrMin(self) :
        self.sa.avgHold = ""
        sleep(1)
        self.sa.minSearch = ""
    
        # get the SA data
        self.sa.triggerHold = ''
        SAP, SAF = self.sa.minSearch
        try :
            SAPower = float(str(SAP))
            SAFreq  = float(str(SAF))
        except ValueError :
            SAPower =  0.0
            SAFreq  =  0.0
    
        self.sa.triggerClear = ''
        self.sa.clearHold = ''
    
        if self.debug :
            print("SAMinPower = '{} SAFreq = {}'\n".format(SAPower, SAFreq))
        return SAPower, SAFreq
    
    def testXMTR(self, cmd, ambcu) :
    
        BaseMin, _ = self.getSAPwrMin()
        BaseMax, _ = self.getSAPwrPeak()
        self.setXMTRon(cmd)
        XmitMin, _ = self.getSAPwrMin()
        XmitMax, XmitFreq = self.getSAPwrPeak()
        self.setXMTRoff()
        Error = "False"
        BaseDiff = BaseMax - BaseMin
        XmitDiff  = XmitMax  - XmitMin
        DiffNorm = XmitMax - BaseMin
        if  DiffNorm <= self.Tolerance :
            Error = "True"
        amb = ambcu >> 4
        cluster = (ambcu >> 2) & 3
        unit = (ambcu & 3)
        tmstart = 0
        tmstart1 = 0
        if self.debug :
            tmstart = time()
        self.logger.logAddValue('DiffValue', DiffNorm)
        self.logger.logAddValue('BaseDiff', BaseDiff)
        self.logger.logAddValue('XmitDiff', XmitDiff)
        self.logger.logAddValue('BaseMin', BaseMin)
        self.logger.logAddValue('BaseMax', BaseMax)
        self.logger.logAddValue('XmitMin', XmitMin)
        self.logger.logAddValue('XmitMax', XmitMax)
        self.logger.logAddValue('XmitFreq', XmitFreq)
        self.logger.logAddValue('AmbNumber', amb)
        self.logger.logAddValue('OutOfTol', Error)
        comChannel = self.getCotaConfig("Client COM Channel")
        self.logger.logAddValue('ComChan', comChannel)
        argv = ['channel_info', amb]
        v = self.tx.sendGetTransmitter(argv)
        chstat, chrev = jcs.getChanStatRev(v)
        if self.debug :
            tmstart1 = time()
        try :
            if self.debug :
                print("Ch Stat {} chrev {} {} {:04x}\n".format(repr(chstat), repr(chrev), int(chrev), int(chrev)))
            chrev = "{:04X}".format(int(chrev))
        except ValueError as ve :
            print("Value error in getChanStatRev {} -- {}".format(chrev, repr(ve)))
        val = "{}; Rev {}".format(chstat, chrev)
        self.logger.logAddValue('StatusRev', val)
    
        strval = "Error {} DiffNorm {:.2f}, BaseDiff {:.2f} XmitDiff {:.2f} XmitFreq {:.2f} amb {}"
        if self.options.clusters or self.options.units :
            strval += " cluster {}"
            self.logger.logAddValue('Cluster', cluster)
        if self.options.units :
            strval += " unit {}"
            self.logger.logAddValue('Unit', unit)
        if self.options.console :
            print(strval.format(Error, DiffNorm, BaseDiff, XmitDiff, XmitFreq, amb, cluster, unit))
        self.logger.logResults()
        if self.debug :
            tm = time()
            print("logging duration is tm {} tm1 {} seconds\n".format(tm - tmstart, tm - tmstart1))
    
    
    def iterateXMTRs(self) :
    
        ambStart = 0
        ambEnd   = 16
        if self.options.rangeamb.lower() != 'all' :
            seAry = self.options.rangeamb.split(',')
            try :
                ambStart = int(seAry[0])
            except ValueError :
                ambStart = 0
                pass
            except IndexError :
                ambStart = 0
                pass
            try :
                ambEnd = int(seAry[1])
            except ValueError :
                ambEnd = ambStart + 1
                pass
            except IndexError :
                ambEnd = ambStart + 1
                pass
        startACU = ambStart * 16
        endACU   = ambEnd   * 16
        step = 16
        if self.options.clusters :
            step = 4
        if self.options.units :
            step = 1
        self.setXMTRoff() # make sure all AMBs are in receive mode
        _, _ = self.getSAPwrPeak()
        for ambcu in range(startACU, endACU, step) :
            ambmask = 1 << (ambcu >> 4)
            if (ambmask & 0x9fff) == 0 :
                continue
            cmd = "-m {}".format(ambmask)
            if self.options.clusters or self.options.units :
                clustermask = 1 << ((ambcu >> 2) & 3)
                cmd += " -c {}".format(clustermask)
                if self.options.units :
                    unitmask = 1 << (ambcu & 3)
                    cmd += " -u {}".format(unitmask)
            self.testXMTR(cmd, ambcu)

    def getCotaConfig(self, name) :
        if self.cotaConfig is None :
            if self.debug :
                print('Getting CotaCofig File\n')
            jVal = self.sts.sendCommand(['getConfigFile'])
            _, self.cotaConfig, _ = self.sts.parseCotaConfigValues(jVal)
        rtnval = "NotKnown"
        try :
            rtnval = str(self.cotaConfig[name])
        except ValueError as ve:
            print("Error getCotaConfig {}".format(repr(ve)))
        return rtnval
        
    def changeComChannel(self, chan) :
        args = ['setNewComChan', chan]
        self.sts.sendCommand(args)
        argv = ["reboot"]
        v = self.tx.sendGetTransmitter(argv)
        sleep(30)
        argv = ["reset"]
        v = self.tx.sendGetTransmitter(argv)


if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("-p","--saport",    dest='saport',     type=int,   action='store',      default=5023,  help="Signal Analyzer telnet port number")
    parser.add_option("-i","--saip",      dest='saipaddr',   type=str,   action='store',      default="",    help="Signal Analyzer network address")
    parser.add_option("-I","--txip",      dest='txipaddr',   type=str,   action='store',      default="",    help="Transmitter network address.")
    parser.add_option("-P","--txport",    dest='txport',     type=int,   action='store',      default=50000,  help="Transmitter telnet port number")
    parser.add_option("-s","--srvport",   dest='srvport',    type=int,   action='store',      default=50081,  help="Transmitter Test Server port number")
    parser.add_option("-t","--tolerance", dest='tolerance',  type=float, action='store',      default=10.0,  help="Tolerance for tx power diff.")
    parser.add_option("-r","--rangeamb",  dest='rangeamb',   type=str,   action='store',      default="all",  help="Range Of AMBs to iterate over.")
    parser.add_option("","--dbname",      dest='dbname',     type=str,   action='store',      default=AMBDBNAME,  help="Range Of AMBs to iterate over.")
    parser.add_option("","--tblname",     dest='tblname',    type=str,   action='store',      default=TXTBLNAME,  help="Range Of AMBs to iterate over.")
    parser.add_option("","--cableloss",   dest='cableloss',  type=float, action='store',default=0.0,help='Cable loss positive dB. Should include the 20 dB Connector loss')

    parser.add_option("","--channumber",  dest='channumber', type=str,   action="store",      default="all", help="Set the SAs Center Frequency based on COM channel number.")
    parser.add_option("-c","--clusters",  dest='clusters',               action="store_true", default=False, help="Iterate of clusters")
    parser.add_option("-u","--units",     dest='units',                  action="store_true", default=False, help="Iterate of units")
    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store',      default="AmbCheck",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",   dest='directory',  type=str,   action='store',      default="/home/ossiadev/Utils/AMBWorkingUtil/Logs/",help="Log file directory.")
    parser.add_option("","--console",     dest='console',                action='store_true', default=False,help="Turn on printing to console")
    parser.add_option("","--logtodb",     dest='logtodb',                action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("-d","--debug",     dest='debug',                  action="store_true", default=False, help="print debug info.")

    (options, args) = parser.parse_args()

    tA = None
    if options.saipaddr is not "" and options.txipaddr is not "" :
        tA = AMBTest(options)
        tA.main()
    else :
        print("Please provide IP addresses or FQDNs for a \nSpectrum analyzer (-i) and a target Charger (-I).\n")

    duration = int(time() - START_TIME)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    try :
        tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
    except Exception as e : 
        print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
