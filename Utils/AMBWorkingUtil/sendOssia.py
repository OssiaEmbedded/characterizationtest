#! /usr/bin/python
from __future__ import absolute_import, division, print_function, unicode_literals

import socket
from time import sleep
from optparse import OptionParser
import sys

def sendgetdata(sendthis, ipaddr, portnumber):
    mySocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    try :
        mySocket.connect((ipaddr, portnumber))
        mySocket.settimeout(1.0)
    except socket.error as ee :
        return "There was a connect exception {}".format(repr(ee))

    cnt = 60
    cmd = sendthis + "\r\n"
    data = ""
    if sys.version_info[0] == 3 :
        cmd = bytes(sendthis + "\r\n", encoding='utf-8')
        data = bytes("", encoding='utf-8')
    mySocket.sendall( cmd )
    sleep(0.5)
    datack = ""
    while cnt > 0 :
        try :
            d = mySocket.recv(8192)
            data += d
        except socket.timeout :
            cnt -= 1
        datack = data
        if sys.version_info[0] == 3 :
            datack = data.decode('utf-8')
        if 'SUCCESS' in datack :
            break
    dataout =  datack.lstrip().rstrip()
    mySocket.close()

    return dataout

def sendCommand(args, ipaddr, port) :
    sDat = "COMMANDVAL"
    for i in range(len(args)) :
        sDat += "/"+args[i].replace('\\','')
    data = sendgetdata(sDat, ipaddr, port)
    return data
 
if __name__ == "__main__" :
    parser = OptionParser()

    parser.add_option("-t","--tstequip",
                       dest="tstequip",
                       action="store",
                       default="ossiatest0.local",
                       help="Network name or IP address of ossiaServer (default='%default')")

    parser.add_option("-p","--port",
                       dest="portnumber",
                       type=int,
                       action="store",
                       default="7000",
                       help="Port number of the ossiaServer telnet server (default='%default')")

    (options, args) = parser.parse_args()

    if len(args) > 0 :
        rtnval = sendCommand(args, options.tstequip, options.portnumber)
        print(rtnval)

