''' VAMBWorking.py -- Setup test equipment, Transmitter, and client to log data as
defined by commandline options.

Version : 0.0.1
Date : July 12 2017
Copyright Ossia Inc. 2017

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname

import TXJSON.JSonCmdStrings as jcs
import SA.SACommunicationBase as sacom
from LIB.TXComm import TXComm
from LIB.logResults import LogResults
from subprocess import check_output as spcheck_output
from LIB.sendTestServer import SendTestServer

DEFAULTDBSERVER = 'ossia-build'
TXTBLNAME = 'TxAmuCheck'
AMUDBNAME = 'CheckDB'
AMUCOLNAME = 'AmuCheckTable'
TIMESTAMPCOLNAME="DataTimeStamp"

AMU_LIST = ['EN_AMU1', 'EN_AMU2', 'EN_AMU3', 'EN_AMU4'] 


SA_SETUP_LIST = [['resetsa', '', None],
                 ['resolutionBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['videoBWAuto', 'OFF', 'VARCHAR(5)'],
                 ['frequencySpan', '9.0 MHz', 'VARCHAR(15)'], # sa_span="100.0 MHz"
                 ['centerFrequency', '2.45 GHZ', 'VARCHAR(15)'],  # sa_center='2.45 GHZ'
                 ['referenceLevel', '0.00 dBm', 'VARCHAR(15)'], # sa_reflevel='31.47 dBm'
                 #['resolutionBW', '910 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['resolutionBW', '100 KHz', 'VARCHAR(15)'],   # sa_resBW='910 KHz'
                 ['videoBW', '50 MHz', 'VARCHAR(15)'],    # sa_vidBW='50 MHz'
                 ['singleContinuousSweep', 'ON', 'VARCHAR(5)']]

TX_CMS  = [[TIMESTAMPCOLNAME, "%s",'VARCHAR(35)'],
           ['IPAddress', '%s', 'VARCHAR(16)'],
           ['Tolerance', '%5.2f', 'VARCHAR(8)'],
           ['PostTestNotes', '%s', 'VARCHAR(512)'],
           ["Duration", "%s", 'VARCHAR(20)'],
           [AMUCOLNAME, '%s', 'VARCHAR(50)'],
           ['LogFileName', '%s', 'VARCHAR(150)'],
           ['SACenterFreq', '%s', 'VARCHAR(15)']]

AMU_CMS = [['DiffValue', '%5.2f','VARCHAR(8)'],
           ['BaseMax', '%5.2f','VARCHAR(8)'],
           ['XmitMax', '%5.2f','VARCHAR(8)'],
           ['XmitFreq', '%6.4e','VARCHAR(10)'],
           ['DaemonNum', '%d', 'VARCHAR(5)'],
           ['AMU', '%s','VARCHAR(8)'],
           ['AMB', '0x%X','VARCHAR(8)'],
           ['UVP', '0x%04X','VARCHAR(8)'],
           ['StatusRev', '%s','VARCHAR(50)'],
           ['OutOfTol', '%s', 'VARCHAR(8)'],
           ['ComChan', '%s','VARCHAR(50)']]

class AMUTest(object):

    def __init__(self, options) :
        self.txhostname = options.txipaddr.lower()
        self.txip = gethostbyname(self.txhostname)
        self.txport = options.txport
        self.srvport = options.srvport

        self.sahostname = options.saipaddr
        self.saip = gethostbyname(self.sahostname)
        self.saport = options.saport
        self.debug = options.debug
        self.options = options
        self.cotaConfig = None

        self.tx=TXComm(options.txipaddr, int(options.txport), options.debug)
        self.sa=sacom.SACom(addr=options.saipaddr, port=int(options.saport), debug=options.debug)
        self.sts=SendTestServer(self.txip, self.srvport, self.debug)

        logFile = "{}_{}".format(options.logfile, options.txipaddr)+"_{}.csv"

        self.logger = LogResults(logFile, options.directory, console=options.console, debug=options.debug)

    def main(self) :
        try :
            self.Tolerance = float(options.tolerance)
        except ValueError as ve :
            print("ValueError on Tolerance conversion -- {}\n".format(repr(ve)))
            self.Tolerance = 10.0

        self.logger.initFile(TX_CMS)

        if options.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('IPAddress', self.txhostname)
        self.logger.logAddValue('Tolerance', self.Tolerance)
        self.logger.logAddValue('PostTestNotes', "")
        #self.logger.logAddValue('SACenterFreq', options.sacentfreq)

        AMUTABLENAME = "amu_{}_{}".format(self.txhostname.replace("-", "_"), int(self.logger.timeStampNum))
        self.logger.logAddValue(AMUCOLNAME, AMUTABLENAME) 
        self.logger.logAddValue('LogFileName', self.logger.filename)
        self.logger.logResults()

        self.logger.initFile(AMU_CMS, how='a')
        if self.options.logtodb :
            self.logger.initDBTable(AMUTABLENAME)

        chanlist = options.channumber.split(",")

        saSetupDone = False
        for chan, freq in [['24', '2.4601 GHZ'], ['25', '2.4501 GHZ'], ['26', '2.4401 GHZ']] :
            if options.channumber.lower() != 'all' and chan not in chanlist :
                continue

            for cmd_data in SA_SETUP_LIST :
                if cmd_data[0] == 'centerFrequency' :
                    cmd_data[1] = freq
                elif cmd_data[0] == 'referenceLevel' :
                    cmd_data[1] = self.options.cableloss
            # set the SA
                if not saSetupDone or cmd_data[0] == 'centerFrequency' :
                    self.sa.setSAValue(cmd_data[0], cmd_data[1])
            sleep(1)
            saSetupDone = True

            self.cotaConfig = None
            self.changeComChannel(chan)
            self.iterateXMTRs()
            if options.channumber.lower() != 'all' :
                break

    def setXMTRon(self, cmd) :
        strval = "sudo uvp_util {} -v 1".format(cmd)
        tm = 0
        if self.debug :
            tm = time()
            print("strval = '{}'\n".format(strval))
        args = ["execCommand", strval]
        val = self.sts.sendCommand(args)
        print("setXMTRon cmd={} val={}".format(strval, val))
        if self.debug :
            tm = time() - tm
            print("val={} time secs {}\n".format(val, tm))
        rtnval = True
        if "refused" in val :
            rtnval = False
        return rtnval
    
    def setXMTRoff(self, cmd) :
        strval = "sudo uvp_util {} -v 0".format(cmd)
        if self.debug :
            print("strval = '{}'\n".format(strval))
        args = ["execCommand",  strval]
        val = self.sts.sendCommand(args)
        print("setXMTRoff cmd={} val={}".format(strval, val))
        if self.debug :
            print("val={}\n".format(val))
    
        rtnval = True
        if "refused" in val :
            rtnval = False
        return rtnval
    
    def getSAPwrPeak(self) :
        self.sa.avgHold = ""
        sleep(1.5)
        self.sa.peakSearch = ""
    
        # get the SA data
        self.sa.triggerHold = ''
        SAP, SAF = self.sa.peakSearch
        try :
            SAPower = float(str(SAP))
            SAFreq  = float(str(SAF))
        except ValueError :
            SAPower =  0.0
            SAFreq  =  0.0
    
        self.sa.triggerClear = ''
        self.sa.clearHold = ''
    
        if self.debug :
            print("SAPeakPower = '{} SAFreq = {}'\n".format(SAPower, SAFreq))
        return SAPower, SAFreq

    def getSAPwrMin(self) :
        self.sa.avgHold = ""
        sleep(1.5)
        self.sa.minSearch = ""
    
        # get the SA data
        self.sa.triggerHold = ''
        SAP, SAF = self.sa.minSearch
        try :
            SAPower = float(str(SAP))
            SAFreq  = float(str(SAF))
        except ValueError :
            SAPower =  0.0
            SAFreq  =  0.0
    
        self.sa.triggerClear = ''
        self.sa.clearHold = ''
    
        if self.debug :
            print("SAMinPower = '{} SAFreq = {}'\n".format(SAPower, SAFreq))
        return SAPower, SAFreq
    
    def testXMTR(self, cmd, daemonNum, amu, amb, uvp, tmIn) :
    
        tmstart = time()
        tmstart1 = 0
        if self.debug :
            tmstart = time()

        BaseMax, _ = self.getSAPwrPeak()
        print("\nTime to acquire BaseMax = {} SAVal={}".format(time()-tmstart, BaseMax))
        tmstart = time()
        self.setXMTRon(cmd)
        print("Time to Turn on xmitter = {}".format(time()-tmstart))
        tmstart = time()
        XmitMax, XmitFreq = self.getSAPwrPeak()
        print("Time to acquire XmitMax = {} SAVal={}".format(time()-tmstart, XmitMax))
        tmstart = time()
        self.setXMTRoff(cmd)
        print("Time to Turn off xmitter = {}".format(time()-tmstart))
        tmstart = time()

        Error = "False"
        DiffNorm = XmitMax - BaseMax
        if  DiffNorm <= self.Tolerance :
            Error = "True"
        self.logger.logAddValue('DiffValue', DiffNorm)
        self.logger.logAddValue('BaseMax', BaseMax)
        self.logger.logAddValue('XmitMax', XmitMax)
        self.logger.logAddValue('XmitFreq', XmitFreq)
        self.logger.logAddValue('DaemonNum', daemonNum)
        self.logger.logAddValue('AMU', amu)
        self.logger.logAddValue('AMB', (1<<amb))
        self.logger.logAddValue('UVP', uvp)
        self.logger.logAddValue('OutOfTol', Error)
        comChannel = self.getCotaConfig("Client COM Channel")
        self.logger.logAddValue('ComChan', comChannel)
        argv = ['channel_info', amb]
        v = self.tx.sendGetTransmitter(argv)
        chstat, chrev = jcs.getChanStatRev(v)
        if self.debug :
            tmstart1 = time()
            print("time to get data and log it {}\n".format(tmstart1-tmstart))
        try :
            if self.debug :
                print("Ch Stat {} chrev {} {} {:04x}\n".format(repr(chstat), repr(chrev), int(chrev), int(chrev)))
            chrev = "{:04X}".format(int(chrev))
        except ValueError as ve :
            print("Value error in getChanStatRev {} -- {}".format(chrev, repr(ve)))
        val = "{}; Rev {}".format(chstat, chrev)
        self.logger.logAddValue('StatusRev', val)
    
        strval = "Error {} DiffNorm {:.2f}, XmitFreq {:.2f} amu {} amb {} uvp {}"
        if self.options.console :
            print(strval.format(Error, DiffNorm, XmitFreq, amu, amb, uvp))
        self.logger.logResults()
        if self.debug :
            tm = time()
            print("logging duration is tm {} tm1 {} seconds\n".format(tm - tmstart, tm - tmstart1))
        print("Time to Log Data = {}".format(time()-tmstart))
    
    
    def iterateXMTRs(self) :
    
        iterStart = 0
        iterEnd = 256
        step = 16
        if self.options.uvp :
            step = 1
        cmd = "sudo fpga -w 0x214 -m 0xf -c 0xFFFF -v 1"
        args = ["execCommand",  cmd]
        val = self.sts.sendCommand(args)
        print("\nFirst iterateXMTRs Cmd = {} result={}".format(cmd, val))
        sleep(2)

        self.setXMTRoff(" -w {} -m 0xF -c 0xFFFF ".format(AMU_LIST[3])) # make sure all AMUs are in receive mode
        self.setXMTRoff(" -w {} -m 0xF -c 0xFFFF ".format(AMU_LIST[2])) # make sure all AMUs are in receive mode
        self.setXMTRoff(" -w {} -m 0xF -c 0xFFFF ".format(AMU_LIST[1])) # make sure all AMUs are in receive mode
        self.setXMTRoff(" -w {} -m 0xF -c 0xFFFF ".format(AMU_LIST[0])) # make sure all AMUs are in receive mode
        SAval, _ = self.getSAPwrPeak()
        print("AT start of iterateXMTRs SA Val = {}\n\n".format(SAval))

        tmS = time()
        for daemonNum in range(iterStart, iterEnd, step) :
            tm = time()
            uvpVal = 0xFFFF if not self.options.uvp else (1 << int((daemonNum >> 2) % 16))
            amuVal = AMU_LIST[(daemonNum & 3)]
            amb =  (daemonNum >> 6)&3
            ambVal = 1 << amb
            cmd = "-w {} -m 0x{:X} -c 0x{:X}".format(amuVal, ambVal, uvpVal)
            print("Testing uvp=0x{:04X} amu={} amb={} daemonNum={} cmd='{}'".format(uvpVal,
                                                                                    amuVal,
                                                                                    ambVal,
                                                                                    daemonNum + 1,
                                                                                    cmd))
            self.testXMTR(cmd, daemonNum + 1, amuVal, amb, uvpVal, tm)
            print("Elapsed time {} iteration time {}\n".format(time()-tmS, time()-tm))
        print("\nTotal Elapsed time {}\n".format(time()-tmS))

    def getCotaConfig(self, name) :
        if self.cotaConfig is None :
            jVal = self.sts.sendCommand(['getConfigFile'])
            if self.debug :
                print('Getting CotaCofig File {}\n'.format(jVal))
            _, self.cotaConfig, _ = self.sts.parseCotaConfigValues(jVal)
        rtnval = "NotKnown"
        try :
            rtnval = str(self.cotaConfig[name])
        except ValueError as ve:
            print("Error getCotaConfig {}".format(repr(ve)))
        return rtnval
        
    def changeComChannel(self, chan) :
        print("Changing COM channel to {}\n".format(chan))
        #argv = ["set_com_channel", chan]
        #v = self.tx.sendGetTransmitter(argv)
        args = ['setNewComChan', chan]
        self.sts.sendCommand(args)
        argv = ["reboot"]
        v = self.tx.sendGetTransmitter(argv)
        sleep(65)
        argv = ["reset"]
        v = self.tx.sendGetTransmitter(argv)
        print("Return Val from 'reset' {}\n".format(repr(v)))
        sleep(11)
        v = self.tx.sendGetTransmitter(argv)
        print("Return Val from 'reset' {}\n".format(repr(v)))
        sleep(11)
        argv = ["stop_charging", "all"]
        v = self.tx.sendGetTransmitter(argv)
        print("Return Val from 'stop_charging' {}\n".format(repr(v)))
        #argv = ["reset"]
        #v = self.tx.sendGetTransmitter(argv)
        #print("Return Val from 'reset' {}\n".format(repr(v)))
        #sleep(11)
        #argv = ["reset_FPGA"]
        #v = self.tx.sendGetTransmitter(argv)
        #print("Return Val from 'reset_FPGA' {}\n".format(repr(v)))
        #sleep(11)
        #argv = ["channel_off", "all"]
        #v = self.tx.sendGetTransmitter(argv)
        #print("Return Val from 'channel_off' {}\n".format(repr(v)))
        #sleep(11)
        #argv = ["pause"]
        #v = self.tx.sendGetTransmitter(argv)
        #print("Return Val from 'pause' {}\n".format(repr(v)))
        #sleep(2)


if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("-p","--saport",    dest='saport',     type=int,   action='store',      default=5023,  help="Signal Analyzer telnet port number")
    parser.add_option("-i","--saip",      dest='saipaddr',   type=str,   action='store',      default="",    help="Signal Analyzer network address")
    parser.add_option("-I","--txip",      dest='txipaddr',   type=str,   action='store',      default="",    help="Transmitter network address.")
    parser.add_option("-P","--txport",    dest='txport',     type=int,   action='store',      default=50000,  help="Transmitter telnet port number")
    parser.add_option("-s","--srvport",   dest='srvport',    type=int,   action='store',      default=50081,  help="Transmitter Test Server port number")
    parser.add_option("-t","--tolerance", dest='tolerance',  type=float, action='store',      default=10.0,  help="Tolerance for tx power diff.")
    parser.add_option("-r","--rangeamb",  dest='rangeamb',   type=str,   action='store',      default="all",  help="Range Of AMUs to iterate over.")
    parser.add_option("","--dbname",      dest='dbname',     type=str,   action='store',      default=AMUDBNAME,  help="Range Of AMUs to iterate over.")
    parser.add_option("","--tblname",     dest='tblname',    type=str,   action='store',      default=TXTBLNAME,  help="Range Of AMUs to iterate over.")
    parser.add_option("","--cableloss",   dest='cableloss',  type=float, action='store',default=0.0,help='Cable loss positive dB. Should include the 20 dB Connector loss')

    parser.add_option("","--channumber",  dest='channumber', type=str,   action="store",      default="all", help="Set the SAs Center Frequency based on COM channel number.")
    parser.add_option("-u","--uvp",       dest='uvp',                    action="store_true", default=False, help="Iterate over UVPs")
    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store',      default="AmbCheck",help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--directory",   dest='directory',  type=str,   action='store',      default="/home/ossiadev/Utils/VAMBWorkingUtil/Logs/",help="Log file directory.")
    parser.add_option("","--console",     dest='console',                action='store_true', default=False,help="Turn on printing to console")
    parser.add_option("","--logtodb",     dest='logtodb',                action='store_false',default=True,help="Log data to the DataBase at ossia-build")
    parser.add_option("-d","--debug",     dest='debug',                  action="store_true", default=False, help="print debug info.")

    (options, args) = parser.parse_args()

    tA = None
    if options.saipaddr is not "" and options.txipaddr is not "" :
        tA = AMUTest(options)
        tA.main()
    else :
        print("Please provide IP addresses or FQDNs for a \nSpectrum analyzer (-i) and a target Charger (-I).\n")

    duration = int(time() - START_TIME)
    hrs = float(duration) / 3600.0
    mins = (hrs - int(hrs)) * 60.0
    secs = (mins - int(mins)) * 60.0
    secs += 0.000005

    Duration = "{:02d}_{:02d}_{:02d}".format(int(hrs), int(mins), int(secs))
    try :
        tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
    except Exception as e : 
        print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
