''' RectEffUtil.py -- Read test equipment setup and log data as defined by commandline options.
                     The test will assume that the test equipment is setup and ready. It will read
                     the setup and record it, then proceed with the data collection process.
                     It will set one value as directed by command line arguments.
                     It will read a voltage from one meter and a current from another.
                     It will record these as well as calculate some others Power, load resistance, power gain.

Version : 0.0.1
Date : July 10 2019
Copyright Ossia Inc. 2019

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname

import SIGGEN.SIGGENCom as siggen
import DMM.DMMCom       as dmm
import PWRAMP.PWRAMPCom as pwramp
import PWRANA.PWRANACom as pwrana
from LIB.logResults import LogResults
from subprocess import check_output as spcheck_output
import LIB.parseCommaColon as pcc
import LIB.timestamp as TSUtil
import LIB.utilmod as utilmod 
import math

DEFAULTDBSERVER  = 'ossia-build'
DEFAULTDBNAME    = 'RecEffCollect'
DEFAULTDBTABLE   = 'RecEffData'
DEFAULTLOGDIR    ="/home/ossiadev/Utils/RectEffUtil/Logs/"

SGCOLNAME        = 'SigGenSetup'
PACOLNAME        = 'PASetup'
PWRANACOLNAME    = 'PwrAnaSetup'
DMMVCOLNAME      = 'DMMVoltSetup'
DMMICOLNAME      = 'DMMCurrentSetup'
RECEFFCOLNAME    = 'RecEffTable'
TIMESTAMPCOLNAME = 'DataTimeStamp'


RECEFF_CMS  = [[TIMESTAMPCOLNAME, '%s', 'VARCHAR(35)'],
               ["CfgComment",     '%s', 'VARCHAR(512)'],
               ['PostTestNotes',  '%s', 'VARCHAR(512)'],
               ['LogFileName',    '%s', 'VARCHAR(150)'],
               [RECEFFCOLNAME,    '%s', 'VARCHAR(35)'],
               [SGCOLNAME,        '%s', 'VARCHAR(35)'],
               [PACOLNAME,        '%s', 'VARCHAR(35)'],
               [PWRANACOLNAME,    '%s', 'VARCHAR(35)'],
               [DMMVCOLNAME,      '%s', 'VARCHAR(35)'],
               [DMMICOLNAME,      '%s', 'VARCHAR(35)'],
               ["Duration",       '%s', 'VARCHAR(20)'],
               ["options",        '%s', 'VARCHAR(1024)']]

REDATA_CMS = [['PwrAmpGain',     '%5.2f','VARCHAR(15)'],
              ['SigGenOutPwr',   '%5.2f','VARCHAR(15)'],
              ['PIN_dBm',        '%5.2f','VARCHAR(15)'],
              ['PIN_mW',         '%5.2f','VARCHAR(15)'],
              ['VOUT',           '%5.2f','VARCHAR(15)'],
              ['IOUT_mA',        '%5.2f','VARCHAR(15)'],
              ['RLOAD',          '%5.2f','VARCHAR(15)'],
              ['POUT_mW',        '%5.2f','VARCHAR(15)'],
              ['PWREFF_pct',     '%5.2f','VARCHAR(15)'],
              ['PwrAnaVoltSet',  '%5.2f','VARCHAR(15)']]

class RecEffUtil(object):

    def __init__(self, options) :
        self.siggen  = None
        self.pwramp  = None
        self.pwrana  = None
        self.dmmvolt = None
        self.dmmcurr = None

        self.debug = options.debug
        self.options = options

        logFile = options.logfile+".csv"

        self.logger = LogResults(logFile, options.directory, console=True, debug=options.debug)

    def checkEquipment(self) :
        rtnval = True
        strval  = ""
        strval += "" if self.siggen  is not None else "Sig Gen "
        strval += "" if self.pwrana  is not None else "Pwr Amp "
        strval += "" if self.pwramp  is not None else "Pwr Ana "
        strval += "" if self.dmmvolt is not None else "DMM Volt "
        strval += "" if self.dmmcurr is not None else "DMM Current "
        if len(strval) > 0 :
            rntval = False
        return rtnval, strval

    def main(self, testlist) :

        self.logger.initFile(RECEFF_CMS)

        # define the various table names that go into the data base and logfile
        recefftablename = "{}_{}".format(RECEFFCOLNAME, int(self.logger.timeStampNum))
        sgtablename     = "{}_{}".format(SGCOLNAME,     int(self.logger.timeStampNum))
        patablename     = "{}_{}".format(PACOLNAME,     int(self.logger.timeStampNum))
        atablename      = "{}_{}".format(PWRANACOLNAME, int(self.logger.timeStampNum))
        vtablename      = "{}_{}".format(DMMVCOLNAME,   int(self.logger.timeStampNum))
        itablename      = "{}_{}".format(DMMICOLNAME,   int(self.logger.timeStampNum))

        if not self.options.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('CfgComment', self.options.cfgcomments)
        self.logger.logAddValue('LogFileName', self.logger.filename)

        self.logger.logAddValue(RECEFFCOLNAME, recefftablename) 
        self.logger.logAddValue(SGCOLNAME, sgtablename) 
        self.logger.logAddValue(PACOLNAME, patablename) 
        self.logger.logAddValue(PWRANACOLNAME, atablename) 
        self.logger.logAddValue(DMMVCOLNAME, vtablename) 
        self.logger.logAddValue(DMMICOLNAME, itablename) 
        self.logger.logAddValue('Duration', "ST {}".format(self.logger.timeStampStr))
        self.logger.logAddValue('options', utilmod.optionsDictToString(self.options))
        self.logger.logResults()

        # now do the various instruments
        ###########################################################################################
        # First the signal Generator
        instrument_cms = [["SiggenName", "%s",    'VARCHAR(512)'],
                          ["OutputPwr",  "%5.2f", 'VARCHAR(25)'],
                          ["CfgFreq",    "%6.3g", 'VARCHAR(50)'],
                          ["InputFreq",  "%6.3g", 'VARCHAR(50)'],
                          ["OutputFreq", "%6.3g", 'VARCHAR(50)']]

        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(sgtablename)
        # use frequency value from cfgfile
        frequencySet = 2.45E09
        try :
            frequencyCfg = float(self.options.__dict__.get('frequency', 2.45E09))
        except ValueError as ve :
            print("ERROR: Getting Frequency value failed. Using 2.45GHz -- {}".format(repr(ve)))
            frequencyCfg = -1.0
        else :
            frequencySet = frequencyCfg

        self.siggen.frequency = [frequencySet, "HZ"]

        siggenname = self.siggen.identity
        pwrlevel = self.siggen.powerLevel
        freqval  = self.siggen.frequency

        self.logger.logAddValue('SiggenName', siggenname)
        self.logger.logAddValue('OutputPwr',  pwrlevel)
        self.logger.logAddValue('CfgFreq',    frequencyCfg)
        self.logger.logAddValue('InputFreq',  frequencySet)
        self.logger.logAddValue('OutputFreq', freqval)
        self.logger.logResults()

        ###########################################################################################
        # now the Pwr Amp 
        instrument_cms = [["PwrAmpName", "%s",    'VARCHAR(512)'],
                          ["PwrAmpGain", "%5.2f", 'VARCHAR(15)']]
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(patablename)
        pwrampname = self.pwramp.identity
        pwrAmpGain = self.pwramp.gain
        self.logger.logAddValue('PwrAmpName', pwrampname)
        self.logger.logAddValue('PwrAmpGain', pwrAmpGain)
        self.logger.logResults()
        
        if self.options.__dict__.get('pwranacouple', None) is None :
            self.pwrana.curlimitcouple = "OFF"
        else :
            try :
                curlim = float(self.options.pwranacouple)
            except ValueError as ve :
                print("ERROR: Power Analyzer  Current Limit coupling -- '{}'".format(repr(ve)))
                curlim = "OFF" 
            else :
                print("INFO: Positive Current limit coupling is {}".format(curlim))
            self.pwrana.curlimitcouple = curlim 

        ###########################################################################################
        # now the Pwr Analyzer

        instrument_cms = [["PwrAnaName",        "%s",    'VARCHAR(512)'],
                          ["fourWire",          "%s",    'VARCHAR(35)'],
                          ["quadrant",          "%s",    'VARCHAR(35)'],
                          ["priority",          "%s",    'VARCHAR(35)'],
                          ["posCurLimit",       "%7.4f", 'VARCHAR(35)'],
                          ["posCurLimitMinMax", "%s",    'VARCHAR(35)'],
                          ["negCurLimit",       "%7.4f", 'VARCHAR(35)'],
                          ["negCurLimitMinMax", "%s",    'VARCHAR(35)'],
                          ["voltProtDelay",     "%7.4f", 'VARCHAR(35)'],
                          ["voltProtPos",       "%7.4f", 'VARCHAR(35)'],
                          ["voltRange",         "%7.4f", 'VARCHAR(35)'],
                          ["voltLimit",         "%7.4f", 'VARCHAR(35)']]

        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(atablename)

        # set positve current limit
        if self.options.__dict__.get('pwranaposcurlimit', None) is not None :
            try :
                curlim = float(self.options.pwranaposcurlimit)
            except ValueError as ve :
                print("ERROR: Power Analyzer Positive Current Limit input value -- '{}'".format(repr(ve)))
                curlim = 0.02 
            else :
                print("INFO: Positive Current limit value is {}".format(curlim))
            self.pwrana.poscurlimit = curlim

        # set negative current limit
        if self.options.__dict__.get('pwrananegcurlimit', None) is not None :
            try :
                curlim = float(self.options.pwrananegcurlimit)
            except ValueError as ve :
                print("ERROR: Power Analyzer Negative Current Limit input value -- '{}'".format(repr(ve)))
                curlim = -0.2 
            else :
                print("INFO: Negative Current limit value is {}".format(curlim))
            self.pwrana.negcurlimit = curlim

        # set voltage protection delay
        if self.options.__dict__.get('pwranavoltprotdelay', None) is not None :
            try :
                vpd = float(self.options.pwranavoltprotdelay)
            except ValueError as ve :
                print("ERROR: Power Analyzer Voltage Protection Delay value -- '{}'".format(repr(ve)))
                vpd = 0.001 
            self.pwrana.voltprotdelay = [vpd, 1]

        # set voltagle protextion positive value
        if self.options.__dict__.get('pwranavoltprotpos', None) is not None :
            try :
                vpl = float(self.options.pwranavoltprotpos)
            except ValueError as ve :
                print("ERROR: Power Analyzer Voltage Protection pos value -- '{}'".format(repr(ve)))
                vpl = "MAX" 
            self.pwrana.voltprotpos = [vpl, 1]

        # set voltage out range value
        if self.options.__dict__.get('pwranavoltrange', None) is not None :
            vlim = 20.4
            try :
                vrange = float(self.options.pwranavoltrange)
            except Exception as ee :
                print("ERROR: Power Analyzer voltage range config value not valid -- {}".format(repr(ee)))
            else :
                self.pwrana.setvoltrange = vrange

        posminmax = self.pwrana.poscurrlimminmax
        negminmax = self.pwrana.negcurrlimminmax

        self.logger.logAddValue('PwrAnaName',    self.pwrana.identity)
        self.logger.logAddValue("fourWire",      self.pwrana.fourwire)
        self.logger.logAddValue("quadrant",      self.pwrana.quadrant)
        self.logger.logAddValue("priority",      self.pwrana.priority)
        self.logger.logAddValue("posCurLimit",   self.pwrana.poscurlimit)
        self.logger.logAddValue("posCurLimit",   self.pwrana.poscurlimit)
        self.logger.logAddValue("voltProtDelay", self.pwrana.voltprotdelay)
        self.logger.logAddValue("voltProtPos",   self.pwrana.voltprotpos)
        self.logger.logAddValue("negCurLimit",   self.pwrana.negcurlimit)
        self.logger.logAddValue("voltRange",     self.pwrana.setvoltrange)
        self.logger.logAddValue("voltLimit",     self.pwrana.setvoltlim)
        self.logger.logAddValue("posCurLimitMinMax", "Min {:6.3f}, Max {:6.3f}".format(posminmax[0],posminmax[1]))
        self.logger.logAddValue("negCurLimitMinMax", "Min {:6.3f}, Max {:6.3f}".format(negminmax[0],negminmax[1]))
        self.logger.logResults()

        ###########################################################################################
        # Voltage DMM
        instrument_cms = [["DMMName", "%s", 'VARCHAR(112)'],
                          ["config",  "%s", 'VARCHAR(15)'],
                          ["rngVal",  "%s", 'VARCHAR(25)'],
                          ["resVal",  "%s", 'VARCHAR(25)']]
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(vtablename)

        cfg, rng, res = self.dmmvolt.getconfig
        self.logger.logAddValue('DMMName', self.dmmvolt.identity)
        self.logger.logAddValue("config",  cfg)
        self.logger.logAddValue("rngVal",  rng)
        self.logger.logAddValue("resVal",  res)
        self.logger.logResults()

        ###########################################################################################
        # Current DMM
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(itablename)
        cfg, rng, res = self.dmmcurr.getconfig
        self.logger.logAddValue('DMMName', self.dmmcurr.identity)
        self.logger.logAddValue("config",  cfg)
        self.logger.logAddValue("rngVal",  rng)
        self.logger.logAddValue("resVal",  res)
        self.logger.logResults()

        ###########################################################################################
        # now get ready to log the data
        self.logger.initFile(REDATA_CMS, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(recefftablename)

        # Power Analyzer initial voltage out value from config file
        g_volts = 0.1 
        if self.options.__dict__.get('pwranavolt', None) is not None :
            try :
                g_volts = float(self.options.pwranavolt)
            except ValueError as ve :
                print("ERROR: Power Analyzer Initial Voltage value -- '{}'".format(repr(ve)))

        self.pwrana.enablechan = ['OFF', '2,3']
        self.pwrana.enablechan = ['ON', '1']
        self.pwrana.setvoltout = g_volts
        self.siggen.rfonoff = "ON"
        self.pwramp.poweronoff = "ON"
        sleep(0.75)
        listcount = len(testlist)

        ###########################################################################################
        # Start the testlist
        for testitem in testlist :
            minCurrentValue = float(self.options.mincurrent) * 1000.0 # make it milliamps
            minCurrentCheck = False
            overVoltageError = False
            print("INFO: Test list iterations left {}".format(listcount))
            listcount -= 1
            try :
                siggenpwr     = float(testitem.get('siggenpwr',  0.0))
                pwrampout     = float(testitem.get('pwrampout',  0.0))
                pwrInVal      = float(testitem.get('inputpwr',   self.options.inputpwr))
                voltSweepStr  =   str(testitem.get('sweepvolts', self.options.sweepvolts))
                logcount      =   int(testitem.get('logcount',   self.options.logcount))
                stabilize     = float(testitem.get('stabilize',  self.options.stabilize))
                avgcount      =       testitem.get('avgcount',   self.options.avgcount)
                pwrampwarmup  = float(testitem.get('pwrampwarmup', 0.0))
                siggenoffon   =       testitem.get('siggenoffon', None)
                pwrampoffon   =       testitem.get('pwrampoffon', None)

                voltSweepList = pcc.expandStartEndStep(voltSweepStr)
            except Exception as ee :
                print("ERROR: Could not read in and convert testitem values -- '{}' -- '{}'".format(repr(ee), repr(testitem)))
            else :
            ###########################################################################################
            # Begin the Voltage sweep

                # calc mW from dBm
                pIn = math.pow(10, (pwrInVal / 10.0))

                if pwrampoffon is not None :
                    self.siggen.poweronoff = pwrampoffon

                if siggenoffon is None or siggenoffon.upper() == "ON" :
                    self.pwrana.setvoltout = voltSweepList[0]


                self.siggen.powerLevel = [siggenpwr, "DBM"]
                pwrlevel               = self.siggen.powerLevel

                self.pwramp.gain       = pwrampout
                sleep(2.0)
                pwrAmpGain             = self.pwramp.gain

                if siggenoffon is not None :
                    self.siggen.rfonoff = siggenoffon

                sweepcount = len(voltSweepList)
                sleep(pwrampwarmup)
                print("INFO: voltSweepList = {}\n".format(repr(voltSweepList)))
                for volts in voltSweepList :
                    print("INFO: Sweep list iterations left {} volts={}".format(sweepcount, volts))
                    sweepcount -= 1
                    self.pwrana.setvoltout = volts
                    avgv = 0
                    avgi = 0
                    for iii in range(logcount) :
                        sleep(stabilize)
                        if self.pwrana.checkOutErrors(self.pwrana.quescond) :
                            self.pwrana.setvoltout = voltSweepList[0]
                            sleep(stabilize/2.0)

                            f = self.pwrana.clearoutputs
                            sleep(stabilize/2.0)
                            overVoltageError = True

                        v = self.dmmvolt.measurevolt
                        i = self.dmmcurr.measurecurr * 1000.0 # convert to mA
                        if avgcount :
                            avgv += v
                            avgi += i
                            if iii < logcount - 1 :
                                continue
                            else :
                                v = avgv / float(logcount)
                                i = avgi / float(logcount)

                        print("INFO: PwrAnaVoltSet {}".format(volts))
                        print("INFO: SigGenOutPwr  {}".format(pwrlevel))
                        print("INFO: PwrAmpGain    {}".format(pwrAmpGain))
                        print("INFO: PIN_dBm       {}".format(pwrInVal))
                        print("INFO: PIN_mW        {}".format(pIn))
                        print("INFO: VOUT          {}".format(v))
                        print("INFO: IOUT_mA       {}".format(i))
                        print("INFO: RLOAD         {}".format(v/i*1000.0))
                        print("INFO: POUT_mW       {}".format(v*i))
                        print("INFO: PWREFF_pct    {}".format(v*i/pIn*100.0))

                        self.logger.logAddValue('PwrAnaVoltSet', volts)
                        self.logger.logAddValue('PwrAmpGain',    pwrAmpGain)
                        self.logger.logAddValue('SigGenOutPwr',  pwrlevel)
                        self.logger.logAddValue('PIN_dBm',       pwrInVal)
                        self.logger.logAddValue('PIN_mW',        pIn)
                        self.logger.logAddValue('VOUT',          v)
                        self.logger.logAddValue('IOUT_mA',       i)
                        self.logger.logAddValue('RLOAD',         v/i*1000.0)
                        self.logger.logAddValue('POUT_mW',       v*i)
                        self.logger.logAddValue('PWREFF_pct',    v*i/pIn*100.0)
                        self.logger.logAddValue('PwrAnaVoltSet', volts)
                        self.logger.logResults()
                        pwrlevel = pwrAmpGain = pwrInVal = 'ND'

                        if minCurrentValue >= i :
                            minCurrentCheck = True
                            minCurrentValue = [minCurrentValue, i]

                        if minCurrentCheck or overVoltageError :
                            # break out of the log count loop
                            break

                    # still in the sweep looop
                    if minCurrentCheck or overVoltageError :
                        #break out of the sweep loop
                        if minCurrentCheck :
                            self.pwrana.setvoltout = voltSweepList[0]
                            line = "ALERT: Minimum Current limit point met at voltage {}. Terminating current sweep -- min {}, val {}".format(volts, minCurrentValue[0], minCurrentValue[1])
                            self.logger.logResults()
                            self.logger.logAddMesLine(line)
                        if overVoltageError :
                            line = "ALERT: Over Voltage. Set PwrAna out voltage to {}, reset output errors and terminated current sweep".format(voltSweepList[0])
                            self.logger.logResults()
                            self.logger.logAddMesLine(line)
                        break

                self.logger.logAddMesLine()
                self.pwrana.setvoltout = voltSweepList[0]

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("","--siggen",      dest='siggen',     type=str,   action='store', default=None,   help="Signal Generator network address (def='%default')")
    parser.add_option("","--sgport",      dest='sgport',     type=int,   action='store', default=5025,   help="Signal Generator telnet port number (def='%default')")

    parser.add_option("","--pwramp",      dest='pwramp',     type=str,   action='store', default=None,   help="Power Amplifier network address (def='%default')")
    parser.add_option("","--paport",      dest='paport',     type=int,   action='store', default=10001,  help="Power Amplifier telnet port number (def='%default')")

    parser.add_option("","--pwrana",      dest='pwrana',     type=str,   action='store', default=None,   help="Power Analyzer network address (def='%default')")
    parser.add_option("","--aport",       dest='aport',      type=int,   action='store', default=5025,   help="Power Analyzer telnet port number (def='%default')")

    parser.add_option("","--dmmvolt",     dest='dmmvolt',    type=str,   action='store', default=None,   help="DMM Voltage network address (def='%default')")
    parser.add_option("","--vport",       dest='vport',      type=int,   action='store', default=5025,   help="DMM Voltage telnet port number (def='%default')")

    parser.add_option("","--dmmcurr",     dest='dmmcurr',    type=str,   action='store', default=None,   help="DMM Current network address (def='%default')")
    parser.add_option("","--iport",       dest='iport',      type=int,   action='store', default=5025,   help="DMM Current telnet port number (def='%default')")

    parser.add_option("","--cfgfile",     dest='cfgfile',    type=str,   action='store', default=None,   help="Use this config file (.py) to find the config for some or all of the options (def='%default')")
    parser.add_option("","--cfgname",     dest='cfgname',    type=str,   action='store', default="cfg",  help="Use this config for some or all of the options (def='%default')")

    parser.add_option("","--tlfile",      dest='tlfile',     type=str,   action='store', default=None,   help="Use this list file (.py) to find the list to direct the flow of the test (def='%default')")
    parser.add_option("","--tlname",      dest='tlname',     type=str,   action='store', default="tl",   help="Use this testlist to direct the flow of the test (def='%default')")

    parser.add_option("","--dbname",      dest='dbname',     type=str,   action='store', default=DEFAULTDBNAME,   help="The MySQL data base name to use. (def='%default')")
    parser.add_option("","--tblname",     dest='tblname',    type=str,   action='store', default=DEFAULTDBTABLE,  help="The DB table to write data to. (def='%default')")

    parser.add_option("","--cfgcomments", dest='cfgcomments',type=str,   action='store', default="",              help="Test run configuration comments are added to the log. (def='%default')")
    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store', default="RectEff_{}",    help="Log file name. If {} is added in the file name, a timestamp will be added. (def='%default')")
    parser.add_option("","--directory",   dest='directory',  type=str,   action='store', default=DEFAULTLOGDIR,   help="Log file directory. (def='%default')")

    parser.add_option("","--sweepvolts",  dest='sweepvolts', type=str,   action='store', default="0.1,10.0,0.1",  help="Comma separated list: start,end,step. (def='%default')")
    parser.add_option("","--inputpwr",    dest='inputpwr',   type=float, action='store', default=-15.0,           help="Input power in dBm (def='%default')")
    parser.add_option("","--stabilize",   dest='stabilize',  type=float, action='store', default=1.0,             help="A stabilization delay from sweep set to data collection (def='%default')")
    parser.add_option("","--mincurrent",  dest='mincurrent', type=float, action='store', default=50E-06,          help="A Minimum current value that will stop the current sweep. (def='%default')")
    parser.add_option("","--logcount",    dest='logcount',   type=int,   action='store', default=10,              help="Number of data per set to acquire and log. (def='%default')")

    parser.add_option("","--avgcount",    dest='avgcount',               action='store_true', default=False,      help="Average logcount values and record just the average (def='%default')")
    parser.add_option("","--nologtodb",   dest='logtodb',                action='store_true', default=False,      help="Do not Log data to the DataBase at ossia-build (def='%default')")
    parser.add_option("","--debug",       dest='debug',                  action="store_true", default=False,      help="print debug info. (def='%default')")

    (options, args) = parser.parse_args()

    if options.cfgfile is not None and options.tlfile is not None :
        try :
            options = utilmod.parseConfigFile(parser, options)
        except RuntimeError :
            exit(1)
        except Exception as e :
            print("ERROR: Unknown exception from parseConfgFile -- {}\n".format(repr(e)))
            exit(2)
        try :
            import imp
            tstlstmod = imp.load_source(options.tlname, options.tlfile)
            testlistE = eval("tstlstmod."+options.tlname)
        except Exception as ee :
            print("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.tlname, options.tlfile, repr(ee)))
        else :

            tA = RecEffUtil(options)
            tA.pwramp  = None if options.pwramp  is None else pwramp.PWRAMPCom(addr=options.pwramp, port=options.paport, debug=options.debug)
            tA.siggen  = None if options.siggen  is None else siggen.SIGGENCom(addr=options.siggen, port=options.sgport, debug=options.debug)
            tA.pwrana  = None if options.pwrana  is None else pwrana.PWRANACom(addr=options.pwrana, port=options.aport, debug=options.debug)
            tA.dmmvolt = None if options.dmmvolt is None else dmm.DMMCom(addr=options.dmmvolt, port=options.vport, debug=options.debug)
            tA.dmmcurr = None if options.dmmcurr is None else dmm.DMMCom(addr=options.dmmcurr, port=options.iport, debug=options.debug)
            go_on, ckval = tA.checkEquipment()
            if go_on :
                tA.main(testlistE)
            else :
                print("Please provide IP addresses or FQDNs for all required eqipment\nMissing equipment is {}".format(ckval))

    hrs, mins, secs, Duration = TSUtil.DurationCalc(START_TIME)
    fd = None
    try :
        fd = open(tA.logger.filename, 'a')
    except IOError as io :
        print("ERROR: Could not open file {} -- {}".format(tA.logger.filename, io.message))
    except Exception as ee :
        print("ERROR: Logger not available -- {}".format(repr(ee)))
    else :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()

    try :
        tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
    except Exception as e : 
        print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))

