''' RectEffUtil.py -- Read test equipment setup and log data as defined by commandline options.
                     The test will assume that the test equipment is setup and ready. It will read
                     the setup and record it, then proceed with the data collection process.
                     It will set one value as directed by command line arguments.
                     It will read a voltage from one meter and a current from another.
                     It will record these as well as calculate some others Power, load resistance, power gain.

Version : 0.0.1
Date : July 10 2019
Copyright Ossia Inc. 2019

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import os
import sys
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname

import SIGGEN.SIGGENCom as siggen
import DMM.DMMCom       as dmm
import PWRAMP.PWRAMPCom as pwramp
import PWRANA.PWRANACom as pwrana
from LIB.logResults import LogResults
from subprocess import check_output as spcheck_output
import LIB.parseCommaColon as pcc
import LIB.timestamp as TSUtil
import LIB.utilmod as utilmod 
import math

DEFAULTDBSERVER  = 'ossia-build'
DEFAULTDBNAME    = 'RecEffCollect'
DEFAULTDBTABLE   = 'RecEffData'
DEFAULTLOGDIR    ="/home/ossiadev/Utils/RectEffUtil/Logs/"

SGCOLNAME        = 'SigGenSetup'
PACOLNAME        = 'PASetup'
PWRANACOLNAME    = 'PwrAnaSetup'
DMMVCOLNAME      = 'DMMVoltSetup'
DMMICOLNAME      = 'DMMCurrentSetup'
RECEFFCOLNAME    = 'RecEffTable'
TIMESTAMPCOLNAME = 'DataTimeStamp'


RECEFF_CMS  = [[TIMESTAMPCOLNAME, '%s', 'VARCHAR(35)'],
               ["CfgComment",     '%s', 'VARCHAR(512)'],
               ['PostTestNotes',  '%s', 'VARCHAR(512)'],
               ['LogFileName',    '%s', 'VARCHAR(150)'],
               [RECEFFCOLNAME,    '%s', 'VARCHAR(35)'],
               [SGCOLNAME,        '%s', 'VARCHAR(35)'],
               [PACOLNAME,        '%s', 'VARCHAR(35)'],
               [PWRANACOLNAME,    '%s', 'VARCHAR(35)'],
               [DMMVCOLNAME,      '%s', 'VARCHAR(35)'],
               [DMMICOLNAME,      '%s', 'VARCHAR(35)'],
               ["Duration",       '%s', 'VARCHAR(20)'],
               ["options",        '%s', 'VARCHAR(1024)']]

REDATA_CMS = [['PwrAmpGain',     '%5.2f','VARCHAR(15)'],
              ['SigGenOutPwr',   '%5.2f','VARCHAR(15)'],
              ['PIN_dBm',        '%5.2f','VARCHAR(15)'],
              ['PIN_mW',         '%5.2f','VARCHAR(15)'],
              ['VOUT',           '%5.2f','VARCHAR(15)'],
              ['IOUT_mA',        '%5.2f','VARCHAR(15)'],
              ['RLOAD',          '%5.2f','VARCHAR(15)'],
              ['POUT_mW',        '%5.2f','VARCHAR(15)'],
              ['PWREFF_pct',     '%5.2f','VARCHAR(15)'],
              ['PwrAnaVoltSet',  '%5.2f','VARCHAR(15)']]

class RecEffUtil(object):

    def __init__(self, options) :
        self.siggen  = None
        self.pwramp  = None
        self.pwrana  = None
        self.dmmvolt = None
        self.dmmcurr = None

        self.debug = options.debug
        self.options = options

        logFile = options.logfile+".csv"

        self.logger = LogResults(logFile, options.directory, console=options.console, debug=options.debug)

    def checkEquipment(self) :
        rtnval = True
        strval  = ""
        strval += "" if self.siggen  is not None else "Sig Gen "
        strval += "" if self.pwrana  is not None else "Pwr Amp "
        strval += "" if self.pwramp  is not None else "Pwr Ana "
        strval += "" if self.dmmvolt is not None else "DMM Volt "
        strval += "" if self.dmmcurr is not None else "DMM Current "
        if len(strval) > 0 :
            rntval = False
        return rtnval, strval

    def main(self) :

        self.logger.initFile(RECEFF_CMS)

        # define the various table names that go into the data base and logfile
        recefftablename = "{}_{}".format(RECEFFCOLNAME, int(self.logger.timeStampNum))
        sgtablename     = "{}_{}".format(SGCOLNAME,     int(self.logger.timeStampNum))
        patablename     = "{}_{}".format(PACOLNAME,     int(self.logger.timeStampNum))
        atablename      = "{}_{}".format(PWRANACOLNAME, int(self.logger.timeStampNum))
        vtablename      = "{}_{}".format(DMMVCOLNAME,   int(self.logger.timeStampNum))
        itablename      = "{}_{}".format(DMMICOLNAME,   int(self.logger.timeStampNum))

        if not self.options.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('CfgComment', self.options.cfgcomments)
        self.logger.logAddValue('LogFileName', self.logger.filename)

        self.logger.logAddValue(RECEFFCOLNAME, recefftablename) 
        self.logger.logAddValue(SGCOLNAME, sgtablename) 
        self.logger.logAddValue(PACOLNAME, patablename) 
        self.logger.logAddValue(PWRANACOLNAME, atablename) 
        self.logger.logAddValue(DMMVCOLNAME, vtablename) 
        self.logger.logAddValue(DMMICOLNAME, itablename) 
        self.logger.logAddValue('Duration', "ST {}".format(self.logger.timeStampStr))
        self.logger.logAddValue('options', utilmod.optionsDictToString(self.options))
        self.logger.logResults()

        # now do the various instruments
        # First the signal Generator
        instrument_cms = [["SiggenName", "%s",    'VARCHAR(512)'],
                          ["OutputPwr",  "%5.2f", 'VARCHAR(25)'],
                          ["OutputFreq", "%6.3g", 'VARCHAR(50)']]

        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(sgtablename)
        siggenname = self.siggen.identity
        pwrlevel = self.siggen.powerLevel
        freqval  = self.siggen.frequency
        self.logger.logAddValue('SiggenName', siggenname)
        self.logger.logAddValue('OutputPwr', pwrlevel)
        self.logger.logAddValue('OutputFreq', freqval)
        self.logger.logResults()

        # now the Pwr Amp 
        instrument_cms = [["PwrAmpName", "%s",    'VARCHAR(512)'],
                          ["PwrAmpGain", "%5.2f", 'VARCHAR(15)']]
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(patablename)
        pwrampname = self.pwramp.identity
        pwrAmpGain = self.pwramp.gain
        self.logger.logAddValue('PwrAmpName', pwrampname)
        self.logger.logAddValue('PwrAmpGain', pwrAmpGain)
        self.logger.logResults()
        
        # now the Pwr Analyzer
        instrument_cms = [["PwrAnaName",  "%s",    'VARCHAR(512)'],
                          ["fourWire",    "%s",    'VARCHAR(35)'],
                          ["quadrant",    "%s",    'VARCHAR(35)'],
                          ["priority",    "%s",    'VARCHAR(35)'],
                          ["posCurLimit", "%5.2f", 'VARCHAR(35)'],
                          ["negCurLimit", "%5.2f", 'VARCHAR(35)'],
                          ["voltRange",   "%5.2f", 'VARCHAR(35)']]
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(atablename)
        self.logger.logAddValue('PwrAnaName',  self.pwrana.identity)
        self.logger.logAddValue("fourWire",    self.pwrana.fourwire)
        self.logger.logAddValue("quadrant",    self.pwrana.quadrant)
        self.logger.logAddValue("priority",    self.pwrana.priority)
        self.logger.logAddValue("posCurLimit", self.pwrana.poscurlimit)
        self.logger.logAddValue("negCurLimit", self.pwrana.negcurlimit)
        self.logger.logAddValue("voltRange",   self.pwrana.setvoltrange)
        self.logger.logResults()

        # Voltage DMM
        instrument_cms = [["DMMName", "%s", 'VARCHAR(112)'],
                          ["config",  "%s", 'VARCHAR(15)'],
                          ["rngVal",  "%s", 'VARCHAR(25)'],
                          ["resVal",  "%s", 'VARCHAR(25)']]
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(vtablename)
        cfg, rng, res = self.dmmvolt.getconfig
        self.logger.logAddValue('DMMName', self.dmmvolt.identity)
        self.logger.logAddValue("config",  cfg)
        self.logger.logAddValue("rngVal",  rng)
        self.logger.logAddValue("resVal",  res)
        self.logger.logResults()

        # Current DMM
        self.logger.initFile(instrument_cms, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(itablename)
        cfg, rng, res = self.dmmvolt.getconfig
        self.logger.logAddValue('DMMName', self.dmmcurr.identity)
        self.logger.logAddValue("config",  cfg)
        self.logger.logAddValue("rngVal",  rng)
        self.logger.logAddValue("resVal",  res)
        self.logger.logResults()

        # now get ready to log the data
        self.logger.initFile(REDATA_CMS, how='a')
        if not self.options.logtodb :
            self.logger.initDBTable(recefftablename)

        voltSweepList = pcc.expandStartEndStep(self.options.sweepvolts)

        pwrInVal = self.options.inputpwr
        while True :
            if self.options.console :
                val = raw_input("\nEnter the Power Input value (float) or 'done' CR:")
                if val.lower() == 'done' :
                    break
                try :
                    pwrInVal = float(val)
                except ValueError as ve :
                    print("Power Input value is not valid. Exiting. -- '{}'".format(repr(ve)))
                    break
                else :
                    pwrlevel = self.siggen.powerLevel
                    pwrAmpGain = self.pwramp.gain
                    pIn = math.pow(10, (pwrInVal / 10.0))

                val = raw_input("\nEnter the Power Analyzer Voltage Sweep parameters (start,end,step) or 'done' CR:")
                if val.lower() == 'done' :
                    break
                try :
                    voltSweepList = pcc.expandStartEndStep(val)
                except Exception as ve :
                    print("Power Analyzer sweeplist is not valid. Exiting. -- '{}'".format(repr(ve)))
                    break


            for volts in voltSweepList :
                self.pwrana.setvoltout = volts
                for iii in range(self.options.logcount) :
                    sleep(self.options.stabilize)
                    v = self.dmmvolt.measurevolt
                    i = self.dmmcurr.measurecurr * 1000.0 # convert to mA
                    self.logger.logAddValue('SigGenOutPwr',  pwrlevel)
                    self.logger.logAddValue('PwrAmpGain',    pwrAmpGain)
                    self.logger.logAddValue('PIN_dBm',       pwrInVal)
                    self.logger.logAddValue('PIN_mW',        pIn)
                    self.logger.logAddValue('VOUT',          v)
                    self.logger.logAddValue('IOUT_mA',       i)
                    self.logger.logAddValue('RLOAD',         v/i*1000.0)
                    self.logger.logAddValue('POUT_mW',       v*i)
                    self.logger.logAddValue('PWREFF_pct',    v*i/pIn*100.0)
                    self.logger.logAddValue('PwrAnaVoltSet', volts)
                    self.logger.logResults()
                    pwrlevel = pwrAmpGain = pwrInVal = 'ND'

            self.logger.logAddBlankLine()
            self.pwrana.setvoltout = 0.0 # reset to zero to not stress diode

            if not self.options.console :
                break

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("","--siggen",        dest='siggen',     type=str,   action='store', default=None,   help="Signal Generator network address (def='%default')")
    parser.add_option("","--sgport",        dest='sgport',     type=int,   action='store', default=5025,   help="Signal Generator telnet port number (def='%default')")

    parser.add_option("","--pwramp",        dest='pwramp',     type=str,   action='store', default=None,   help="Power Amplifier network address (def='%default')")
    parser.add_option("","--paport",        dest='paport',     type=int,   action='store', default=10001,  help="Power Amplifier telnet port number (def='%default')")

    parser.add_option("","--pwrana",        dest='pwrana',     type=str,   action='store', default=None,   help="Power Analyzer network address (def='%default')")
    parser.add_option("","--aport",         dest='aport',      type=int,   action='store', default=5025,   help="Power Analyzer telnet port number (def='%default')")

    parser.add_option("","--dmmvolt",       dest='dmmvolt',    type=str,   action='store', default=None,   help="DMM Voltage network address (def='%default')")
    parser.add_option("","--vport",         dest='vport',      type=int,   action='store', default=5025,   help="DMM Voltage telnet port number (def='%default')")

    parser.add_option("","--dmmcurr",       dest='dmmcurr',    type=str,   action='store', default=None,   help="DMM Current network address (def='%default')")
    parser.add_option("","--iport",         dest='iport',      type=int,   action='store', default=5025,   help="DMM Current telnet port number (def='%default')")

    parser.add_option("-f","--cfgfile",     dest='cfgfile',    type=str,   action='store',default=None,    help="Use this config file (.py) to find the config for some or all of the options (def='%default')")
    parser.add_option("-n","--cfgname",     dest='cfgname',    type=str,   action='store',default="cfg",   help="Use this config for some or all of the options (def='%default')")


    parser.add_option("-i","--inputpwr",    dest='inputpwr',   type=float, action='store', default=-15.0,  help="Input power in dBm (def='%default')")
    parser.add_option("-s","--stabilize",   dest='stabilize',  type=float, action='store', default=1.0,    help="A stabilization delay from sweep set to data collection (def='%default')")
    parser.add_option("-c","--logcount",    dest='logcount',   type=int,   action='store', default=10,     help="Number of data per set to acquire and log. (def='%default')")

    parser.add_option("","--dbname",        dest='dbname',     type=str,   action='store', default=DEFAULTDBNAME,   help="The MySQL data base name to use. (def='%default')")
    parser.add_option("","--tblname",       dest='tblname',    type=str,   action='store', default=DEFAULTDBTABLE,  help="The DB table to write data to. (def='%default')")
    parser.add_option("-v","--sweepvolts",  dest='sweepvolts', type=str,   action='store', default="0.1,10.0,0.1",  help="Comma separated list: start,end,step. (def='%default')")

    parser.add_option("","--cfgcomments",   dest='cfgcomments',type=str,   action='store', default="",              help="Test run configuration comments are added to the log. (def='%default')")
    parser.add_option("","--logfilebase",   dest='logfile',    type=str,   action='store', default="RectEff_{}",     help="Log file name. If {} is added in the file name, a timestamp will be added. (def='%default')")
    parser.add_option("","--directory",     dest='directory',  type=str,   action='store', default=DEFAULTLOGDIR,   help="Log file directory. (def='%default')")

    parser.add_option("-t","--console",     dest='console',                action='store_true', default=False,      help="Turn on printing to console and looping on console input. (def='%default')")
    parser.add_option("-l","--nologtodb",   dest='logtodb',                action='store_true', default=False,      help="Do not Log data to the DataBase at ossia-build (def='%default')")
    parser.add_option("-d","--debug",       dest='debug',                  action="store_true", default=False,      help="print debug info. (def='%default')")

    (options, args) = parser.parse_args()

    if options.cfgfile is not None :
        try :
            options = utilmod.parseConfigFile(parser, options)
        except RuntimeError :
            exit(1)
        except Exception as e :
            print("ERROR: Unknown exception from parseConfgFile -- {}\n".format(repr(e)))
            exit(2)

    tA = RecEffUtil(options)
    if options.pwramp  is not None :
        while True :
            tA.pwramp = pwramp.PWRAMPCom(addr=options.pwramp, port=options.paport, debug=options.debug)
            if tA.pwramp is None :
                if options.console :
                    val = raw_input("\nPower Amplifier not found. Is the mode switch in Remote?\nPress CR when ready, 'e'CR to exit.")
                    if val == 'e' :
                        break
                    print("Waiting 5 secs for DHCP to complete")
                    sleep(5)
                else :
                    break
            else :
                break
    tA.siggen  = None if options.siggen  is None else siggen.SIGGENCom(addr=options.siggen, port=options.sgport, debug=options.debug)
    tA.pwrana  = None if options.pwrana  is None else pwrana.PWRANACom(addr=options.pwrana, port=options.aport, debug=options.debug)
    tA.dmmvolt = None if options.dmmvolt is None else dmm.DMMCom(addr=options.dmmvolt, port=options.vport, debug=options.debug)
    tA.dmmcurr = None if options.dmmcurr is None else dmm.DMMCom(addr=options.dmmcurr, port=options.iport, debug=options.debug)
    go_on, ckval = tA.checkEquipment()
    if go_on :
        tA.main()
    else :
        print("Please provide IP addresses or FQDNs for all required eqipment\nMissing equipment is {}".format(ckval))

    hrs, mins, secs, Duration = TSUtil.DurationCalc(START_TIME)
    fd = None
    try :
        fd = open(tA.logger.filename, 'a')
    except IOError as io :
        print("ERROR: Could not open file {} -- {}".format(tA.logger.filename, io.message))
    except Exception as ee :
        print("ERROR: Logger not available -- {}".format(repr(ee)))
    else :
        fd.write("Duration, {}\n".format(Duration))
        fd.close()

    try :
        tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
    except Exception as e : 
        print("Logging the Duration failed {}\n".format(repr(e)))

    print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))

