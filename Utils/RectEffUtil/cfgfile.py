cfg24={
    'frequency' : 2.450e09,
    'siggen'  : 'a-n5182b-50381.ossiainc.local',
    'pwramp'  : 'ca914f3.ossiainc.local',
    'pwrana'  : '10.10.0.149',
    #'pwrana'  : 'a-n6705b-00331.ossiainc.local',
    'dmmvolt' : 'k-34465a-01969.ossiainc.local',
    'dmmcurr' : 'k-34465a-04576.ossiainc.local',
    'pwranaposcurlimit'   : 0.0200, # amps
    'pwrananegcurlimit'   : -0.500, # amps
    'pwranavoltprotdelay' : 0.001,
    'pwranavoltprotpos'   : 24.0,
    'pwranavoltrange'     : 20.4,
    'pwranavolt'          : 0.2, # initial voltage before PAmp and Siggen are turned on
    'pwrana2pwrampdelay' : 4.5,    
}

cfg58={
    'frequency' : 5.845e09,
    'siggen'  : 'a-n5182b-50381.ossiainc.local',
    'pwramp'  : 'ca914f3.ossiainc.local',
    #'pwrana'  : 'a-n6705b-00331.ossiainc.local',
    'pwrana'  : '10.10.0.149',
    'dmmvolt' : 'k-34465a-01969.ossiainc.local',
    'dmmcurr' : 'k-34465a-04576.ossiainc.local',
    'pwranaposcurlimit' : 0.0200, # amps
    'pwrananegcurlimit' : -0.800, # amps
    'pwranavoltprotdelay' : 0.001,
    'pwranavoltprotpos'   : 24.0,
    'pwranavoltrange'     : 20.4,
    'pwranavolt'          : 0.1, # initial voltage before PAmp and Siggen are turned on
    'pwrana2pwrampdelay' : 3.5,
}
