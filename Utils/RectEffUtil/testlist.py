
testlist_5_0=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -39.10, 'pwrampout' :  10.00, 'inputpwr' :  -5.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  19.00, 'inputpwr' :  -3.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  32.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]

testlist_0_30=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -39.10, 'pwrampout' :  32.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  49.00, 'inputpwr' :   3.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  71.00, 'inputpwr' :   6.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  28.00, 'inputpwr' :   9.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  63.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  19.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  31.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  48.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -15.90, 'pwrampout' : 100.00, 'inputpwr' :  30.00, 'sweepvolts' : '0.3,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]

testlist_12_27=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -39.10, 'pwrampout' :  32.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  49.00, 'inputpwr' :   3.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  71.00, 'inputpwr' :   6.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  28.00, 'inputpwr' :   9.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  63.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  19.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  31.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  48.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -15.90, 'pwrampout' : 100.00, 'inputpwr' :  30.00, 'sweepvolts' : '0.30,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]
#Below are SIGGEN settings when using 1.61 dB loss u.fl cables with 180 deg Hybrid Coupler which has an insertion loss of 0.425 dB from 1->3.
#This will provide a combined power equal to that into one single antenna port. P+1.807dB -> P without splitter and cables losses.P'=P+1.087dB
testlist_Orion_0_30=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -37.29, 'pwrampout' :  32.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -37.29, 'pwrampout' :  49.00, 'inputpwr' :   3.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -37.29, 'pwrampout' :  71.00, 'inputpwr' :   6.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -27.49, 'pwrampout' :  28.00, 'inputpwr' :   9.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -27.49, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -27.49, 'pwrampout' :  63.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -16.09, 'pwrampout' :  19.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -16.09, 'pwrampout' :  31.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -16.09, 'pwrampout' :  48.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -16.09, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -14.09, 'pwrampout' : 100.00, 'inputpwr' :  30.00, 'sweepvolts' : '0.30,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]
#Below are SIGGEN settings when using 1.61 dB loss u.fl cables with 180 deg Hybrid Coupler which has an insertion loss of 0.425 dB from 1->3; also a 3dB pad at input 3 on
#hybrid coupler. These settings provide same power P on hybrid port 4 (antenna 1) and -3dB on port 3 (antenna 2).P"=P+5.035dB
testlist_Orion__3dB_attn_0_30=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -34.07, 'pwrampout' :  32.00, 'inputpwr' :   1.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -34.07, 'pwrampout' :  49.00, 'inputpwr' :   4.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -34.07, 'pwrampout' :  71.00, 'inputpwr' :   7.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -24.27, 'pwrampout' :  28.00, 'inputpwr' :  10.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -24.27, 'pwrampout' :  45.00, 'inputpwr' :  13.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -24.27, 'pwrampout' :  63.00, 'inputpwr' :  16.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.12, 'pwrampout' :  19.00, 'inputpwr' :  19.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -12.87, 'pwrampout' :  31.00, 'inputpwr' :  22.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -12.87, 'pwrampout' :  48.00, 'inputpwr' :  25.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -12.87, 'pwrampout' :  68.00, 'inputpwr' :  28.91, 'sweepvolts' : '0.10,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -10.87, 'pwrampout' : 100.00, 'inputpwr' :  31.91, 'sweepvolts' : '0.30,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]

testlist_21_24_27=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -17.90, 'pwrampout' :  31.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  48.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.05,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]

testlist5p8GHz_27_36=[
          {'pwrampwarmup' : 15.0,
         #  'siggenpwr' : -39.10, 'pwrampout' :  33.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -39.10, 'pwrampout' :  48.00, 'inputpwr' :   3.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -39.10, 'pwrampout' :  71.00, 'inputpwr' :   6.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -29.30, 'pwrampout' :  30.00, 'inputpwr' :   9.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -29.30, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -29.30, 'pwrampout' :  63.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -17.90, 'pwrampout' :  21.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -17.90, 'pwrampout' :  32.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
         # {'siggenpwr' : -17.90, 'pwrampout' :  47.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
           'siggenpwr' : -17.90, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.30,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -15.80, 'pwrampout' : 100.00, 'inputpwr' :  30.00, 'sweepvolts' : '0.30,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -12.90, 'pwrampout' : 100.00, 'inputpwr' :  33.00, 'sweepvolts' : '1.00,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' :  -9.90, 'pwrampout' : 100.00, 'inputpwr' :  36.00, 'sweepvolts' : '1.00,20.3,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]
testlist5p8GHz_12_27=[
          {'pwrampwarmup' : 15.0,
           'siggenpwr' : -29.30, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  63.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  21.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  32.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  47.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  68.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.1,19.5,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]         

testlist=[
          {'pwrampwarmup' : 1800.0,
           'siggenpwr' : -54.90, 'pwrampout' :   8.00, 'inputpwr' : -18.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -54.90, 'pwrampout' :  29.00, 'inputpwr' : -15.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -54.90, 'pwrampout' :  49.00, 'inputpwr' : -12.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -54.90, 'pwrampout' : 100.00, 'inputpwr' :  -9.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  10.00, 'inputpwr' :  -6.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  22.00, 'inputpwr' :  -3.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  34.00, 'inputpwr' :   0.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  50.00, 'inputpwr' :   3.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -39.10, 'pwrampout' :  76.00, 'inputpwr' :   6.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  30.00, 'inputpwr' :   9.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  35.00, 'inputpwr' :  10.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  45.00, 'inputpwr' :  12.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -29.30, 'pwrampout' :  65.00, 'inputpwr' :  15.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  22.00, 'inputpwr' :  18.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  33.00, 'inputpwr' :  21.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  48.00, 'inputpwr' :  24.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  54.00, 'inputpwr' :  25.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  61.00, 'inputpwr' :  26.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -17.90, 'pwrampout' :  70.00, 'inputpwr' :  27.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -15.50, 'pwrampout' : 100.00, 'inputpwr' :  30.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
          {'siggenpwr' : -12.60, 'pwrampout' : 100.00, 'inputpwr' :  33.00, 'sweepvolts' : '0.1,10.0,0.1', 'logcount' : 1, 'stabilize' : 1.0, 'avgcount' : False},
]
