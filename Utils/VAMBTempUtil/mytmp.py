''' VAMBPhaseTempUtil.py -- Setup test equipment, Transmitter, and log temperture data as
defined by commandline options. Set phase of UVPs
Changed to make use of the Tenny Temperature Chamber

Version : 0.0.1
Date : May 9 2019
Copyright Ossia Inc. 2019

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import sys, traceback
#if "/home/ursusm/pylib" not in sys.path :
#    sys.path.insert(0, "/home/ursusm/pylib")
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname
import json, datetime

import SA.SACom as sacom
import OSC.OSCCom as osccom
import SIGGEN.SIGGENCom as siggencom
import TXJSON.JSonCmdStrings as jcs

import LIB.TXComm as txcomm
import LIB.logResults as logresults
import LIB.timestamp as TSUtil
import LIB.sshcomm  as sshcomm
import LIB.parseCommaColon as pcc
import LIB.TempChamber as tempcham
from LIB.utilmod import *

#DEFAULTDBSERVER = 'ossiadevuwm'
DEFAULTDBSERVER = 'ossia-build'
EXECUTEABLEDIR = ""

AMUCOLNAME = 'AmuTempPhaseTable'
TIMESTAMPCOLNAME="DataTimeStamp"
DEFSIGGENLVL = "17.0"

SIGGEN_SETUP_LIST = [
                      ['reset', '', '', None],
                      ['frequency', '2.45', 'GHZ', 'VARCHAR(9)'],
                      ['powerLevel', DEFSIGGENLVL, 'DBM', 'VARCHAR(9)'],
                      ['rfonoff', 'on', 'VARCHAR(5)'],
                    ]
OSC_SETUP_LIST = [
                   ['reset', None],
                   ['systemHeader', 'off', 'VARCHAR(15)'],
                   ['displayChan', '1','ON', 'VARCHAR(15)'],
                   ['displayChan', '2','ON', 'VARCHAR(15)'],
                   ['displayChan', '3','ON', 'VARCHAR(15)'],
                   ['displayChan', '4','ON', 'VARCHAR(15)'],

                   ['inputChanCouple', '1','DC50', 'VARCHAR(15)'],
                   ['inputChanCouple', '2','DC50', 'VARCHAR(15)'],
                   ['inputChanCouple', '3','DC50', 'VARCHAR(15)'],
                   ['inputChanCouple', '4','DC50', 'VARCHAR(15)'],
                   
                   ['displayChanUnits', '1', 'VOLT', 'VARCHAR(15)'],
                   ['displayChanUnits', '2', 'VOLT', 'VARCHAR(15)'],
                   ['displayChanUnits', '3', 'VOLT', 'VARCHAR(15)'],
                   ['displayChanUnits', '4', 'VOLT', 'VARCHAR(15)'],
                   
                   ['displayChanScale', '1', '1', 'VARCHAR(15)'],
                   ['displayChanScale', '2', '1', 'VARCHAR(15)'],
                   ['displayChanScale', '3', '1', 'VARCHAR(15)'],
                   ['displayChanScale', '4', '1', 'VARCHAR(15)'],

                   ['displayChanOffset', '1', '0', 'VARCHAR(15)'],
                   ['displayChanOffset', '2', '0', 'VARCHAR(15)'],
                   ['displayChanOffset', '3', '0', 'VARCHAR(15)'],
                   ['displayChanOffset', '4', '0', 'VARCHAR(15)'],

                   ['timeBaseScale',    '200E-12', 'VARCHAR(15)'],

                   ['triggerSource', "",'1', 'HIGH', 'VARCHAR(15)'],
                   ['triggerLevel', '1', '0', 'VARCHAR(15)'],
                   ['triggerSweep', 'TRIGgered', 'VARCHAR(15)'],
                   ['analyzeAllEdges', 'on', 'VARCHAR(15)'],

                   ['run', None],

                 ]


TX_CMS  = [[TIMESTAMPCOLNAME,   '%s', 'VARCHAR(35)'],
           ['CfgComment',       '%s', 'VARCHAR(512)'],
           ['PostTestNotes',    '%s', 'VARCHAR(512)'],
           ["Duration",         '%s', 'VARCHAR(20)'],
           [AMUCOLNAME,         '%s', 'VARCHAR(50)'],
           ['OscSetupTable',    '%s', 'VARCHAR(25)'],
           ['SiggenSetupTable', '%s', 'VARCHAR(25)'],
           ['LogFileName',      '%s', 'VARCHAR(150)'],
           ['PowerLevel',       '%d', 'VARCHAR(15)'],
           ['Hostname',         '%s', 'VARCHAR(106)'],
           ['TileSetupRslts',   '%s', 'VARCHAR(8092)'],
           ]

def checkForSuccessOrErrors(lines, errors) :
    rtnerr = ""
    if isinstance(errors, tuple) or isinstance(errors, list) :
        for i in range(len(errors)) :
            if len(errors[i]) > 0 :
                rtnerr += errors[i]
    elif isinstance(errors, unicode) or isinstance(errors, str) :
        if len(errors) > 0 :
            rtnerr += errors
    rtnline = ""
    if isinstance(lines, tuple) or isinstance(lines, list) :
        for i in range(len(lines)) :
            ll = str(lines[i])
            if len(ll) > 0 :
                rtnline += "" if "SUCCESS" in ll else ll+","
    elif isinstance(lines, unicode) or isinstance(lines, str) :
        ll = str(lines)
        if len(ll) > 0 :
            rtnline += "" if "SUCCESS" in ll else ll+","
    return str(rtnline), str(rtnerr)


class AMUTempTest(object):

    def __init__(self, opts, testlist=None) :

        self.txcmdfmt   = ["sudo {}fpga -w 0x214 -m 0x{:X} -c 0x{:X} -v 1"]
        self.uvptempfmt = ["sudo {}uvp_util -w  LDOADC1EN_IDLE -m 0x{:X} -c 0x{:X} -v 1",
                           "sudo {}uvp_util -w  TEMPADC_GAIN -m 0x{:X} -c 0x{:X} -v 32",
                           "sudo {}uvp_util -w  TMPSNSCMBUFFEN -m 0x{:X} -c 0x{:X} -v 1",
                           "sudo {}uvp_util -w  TMPSNSCOMPENCNT -m 0x{:X} -c 0x{:X} -v 4",
                           "sudo {}uvp_util -w  TMPSNSCOMPSELEN -m 0x{:X} -c 0x{:X} -v 1",
                           "sudo {}uvp_util -w  TMPSNSEN -m 0x{:X} -c 0x{:X} -v 1",
                           "sudo {}uvp_util -w  TEMPADCEN -m 0x{:X} -c 0x{:X} -v 1",
                           "sudo {}uvp_util -w  LDOADC1EN_SENDPWR -m 0x{:X} -c 0x{:X} -v 1",
                          ]
        self.options = opts
        self.TX_CMS = list(TX_CMS)

        self.ambuvpName = ['Temp', 'Raw', 'TComp']
        
        self.testlist   = testlist if testlist is not None else [{"tempcham" : 25.0}]
        self.txhostname = self.options.txipaddr
        self.txport     = int(self.options.txport)
        self.txip       = gethostbyname(self.txhostname)
        self.txsshport  = int(self.options.sshport)
        self.phaseList  = [0, 45, 90, 135, 180, 225, 270, 315]

        if self.options.__dict__.get('phaselist', None) is not None and self.options.__dict__.get('phaseliststep', None) is not None :
            print("ERROR: input options 'phaselist' and 'phaseliststep' are mutually exclusive")
            raise RuntimeError("Phase list exclusivity conflict")
        
        if self.options.__dict__.get('phaselist', None) is not None :
            self.phaseList  = pcc.parseCommaColon(self.options.phaselist, True, False) # sort the returned list first
        elif self.options.__dict__.get('phaseliststep', None) is not None :
            self.phaseList  = pcc.expandStartEndStep(self.options.phaseliststep, False) # sort the returned list first

        print("INFO: phase list = {}\n".format(repr(self.phaseList)))

        self.oscipaddr  = self.options.oscipaddr
        try :
            self.oscport    = int(self.options.oscport)
            self.oscip      = gethostbyname(self.oscipaddr)
            print("INFO: OSC ({}, {}, {})".format(self.options.oscipaddr, self.oscip, self.oscport))
        except Exception :
            self.oscport = None
            self.oscip   = None

        self.sgipaddr   = self.options.siggenipaddr
        try :
            self.sgport     = int(self.options.siggenport)
            self.sgip       = gethostbyname(self.sgipaddr)
            print("INFO: SIGGEN ({}, {}, {})".format(self.options.siggenipaddr, self.sgip, self.sgport))
        except Exception :
            self.sgport = None
            self.sgip   = None

        self.tehostname = self.options.teipaddr
        try :
            self.teip       = gethostbyname(self.tehostname)
            self.teport     = int(self.options.teport)
            print("INFO: TE  ({}, {}, {})".format(self.options.teipaddr, self.teip, self.teport))
        except Exception :
            self.teip       = None
            self.teport     = None

        try :
            self.tempchamip = gethostbyname(self.options.tempchamip)
            self.tcport     = int(self.options.tcport)
            print("INFO: TC  ({}, {}, {})".format(self.options.tempchamip, self.tempchamip, self.tcport))
        except Exception :
            self.tempchamip = None
            self.tcport     = None

        self.debug      = self.options.debug

        self.tx         = txcomm.TXComm(self.txip, self.txport, self.debug)
        self.com        = sshcomm.SSHlink(self.txip, self.txsshport, "gumstix", 'gumstix', self.debug)
        self.osc        = None if self.oscip is None else osccom.OSCCom(addr=self.oscip,port=self.oscport,debug=self.debug)
        self.siggen     = None if self.sgip  is None else siggencom.SIGGENCom(addr=self.sgip, port=self.sgport, debug=self.debug)
        self.tempcham   = None if self.tempchamip is None else tempcham.TempChamber(port=4001, ethAddr=self.tempchamip, slaveaddress=1, debug=self.debug)

        self.te         = None
        self.chanlist   = None

        self.AmbLst     = pcc.parseCommaColon(self.options.amblist, True, False) # sort the returned list first
        self.UvpLst     = pcc.parseCommaColon(self.options.uvplist, True, False) # do not sort the returned list first
        self.chanlist   = pcc.parseCommaColon(self.options.chanlist, True, False) # do not sort the returned list first
        self.TxOnUvpLst = pcc.parseCommaColon(self.options.txonuvplist, True, False) # sort the returned list first
        # it will be assumed here that the order of uvp channels and thermocouple channels are matched
        # the following will associate them together then sort the arrays.
        
        self.chanMap = self.options.__dict__.get('chanmap', None)
        print("INFO: uvpList  = '{}'".format(self.UvpLst))
        print("INFO: chanlist = '{}'".format(self.chanlist))
        print("INFO: chanMap  = '{}'".format(self.chanMap))
        print("INFO: TxOnUvp  = '{}'".format(self.TxOnUvpLst))
        self.chanliststr = makeListToString(self.chanlist, ',')

        if self.teip is not None :
            self.te  = sacom.SACom(addr=self.options.teipaddr, port=self.teport, debug=self.debug)
            self.te.mySockTimeout = 0.45
            self.TX_CMS = self.TX_CMS[:-1] + \
                          [['DAcqName','%s', 'VARCHAR(100)'], ['DAcqSN','%s', 'VARCHAR(20)'], ['DAcqHostName','%s', 'VARCHAR(100)'], ['DAcqIP', '%s', 'VARCHAR(16)']] + \
                          self.TX_CMS[-1:]
        logFile = "{}".format(self.options.logfile)+"_{}.csv"

        self.logger = logresults.LogResults(logFile, self.options.directory, console=self.options.console, debug=self.options.debug)
        self.amutablename = "amu_temp_{}".format(int(self.logger.timeStampNum))


    def executeCmd(self, cmd) :
        rtnval = 1
        lines = ()
        errors = ()
        if not self.com.connect() :
            print ("ERROR: Could not connect to target '{}'".format(self.txhostname))
        else :
            rtnval = 0
            if isinstance(cmd, unicode) or isinstance(cmd, str) :
                rtnval = self.com.command(cmd)
                lines  = self.com.getlastreadlines()
                errors = self.com.getlastreaderrors()
            elif isinstance(cmd, list) or isinstance(cmd, tuple) :
                for cmdstr in cmd :
                    rtnval += self.com.command(cmdstr)
                    lines  += self.com.getlastreadlines()
                    errors += self.com.getlastreaderrors()
            self.com.close()
        return rtnval, lines, errors


    def newsetPowerLevel(self, pwrlvlIn) :

        try :
            pwrlvl = str(pwrlvlIn)
        except ValueError as e :
            print("ValueError converting powerlevel {} -- {}".format(pwrlvlIn, repr(e)))
            pwrlvl = '-13'

        pwr_bits = { '13'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 0",
                     '14'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 0",
                     '15'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 1",
                     '16'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 1",
                     '17'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 1",
                     '18'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 2",
                     '19'   : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 2",
                     '19.5' : "sudo {}uvp_util -w  AMU{}POWERMODE -m 0x{:X} -c 0x{:X} -v 3",
                     '20'   : "sudo {}uvp_util -w  AMU{}HIGHPOWER -m 0x{:X} -c 0x{:X} -v 1",
                      '0'   : "sudo {}uvp_util -w  AMU{}HIGHPOWER -m 0x{:X} -c 0x{:X} -v 0",
                   }

        cmdfmt = pwr_bits.get(pwrlvl, None)
        print("INFO: pwrlvl={} cmdfmt={}".format(pwrlvl, cmdfmt))
        rtnval = 1
        lines = ''
        errors = ''
        if cmdfmt is not None :
            ambval = 0
            for a in self.AmbLst :
                a = int(a)
                ambval |= 1 << (a-1)
            aclval = 0
            for u in self.TxOnUvpLst :
                u = int(u)
                aclval |= 1 << (u-1)

            cmdstrlst = list()
            for amu in range(1, 5, 1) :
                cmdstr = cmdfmt.format(EXECUTEABLEDIR, amu, ambval, aclval)
                cmdstrlst.append(cmdstr)
                if pwrlvl != '20' :
                    cmdstr = pwr_bits['0'].format(EXECUTEABLEDIR, amu, ambval, aclval)
                    cmdstrlst.append(cmdstr)
            rtnval, lines, errors = self.executeCmd(cmdstrlst)
            
        return pwrlvl, rtnval, lines, errors

    def setPowerLevel(self, pwrlvl) :

        try :
            pwrlvl = int(pwrlvl)
        except ValueError as e :
            print("ValueError converting powerlevel {}".format(repr(e)))
            pwrlvl = 13

        if pwrlvl > 0 :
            pwrlvl = max(pwrlvl, 13)
            pwrlvl = min(pwrlvl, 20)
            argv = ['set_power_level', str(pwrlvl)]
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Set Power Level failed -- {}".format(repr(v)))
            argv = ['get_power_level']
            v = self.tx.sendGetTransmitter(argv)
            pl =  jcs.getTransmitterPowerLevel(v)
            if pl != pwrlvl :
                print("ALERT: Power level incorrect get={} vs set={}".format(pl, pwrlvl))
                pwrlvl = pl
            print("INFO: Set powerlevel to {}".format(pwrlvl))
        return pwrlvl, None, None, None
    
        #self.txcmdfmt   = ["sudo {}fpga -w 0x214 -m 0x{:X} -c 0x{:X} -v 1"]
    def setupUVP(self, cmdlst, uvpList=None) :
        rtnstr = ""
        errstr = ""
        cmdstrlst = list()
        UvpLst = self.UvpLst if uvpList is None else self.TxOnUvpLst
        for cmdfmt in cmdlst :
            if "sleep" in cmdfmt :
                eval(cmdfmt) 
            else :
                for a in self.AmbLst :
                    a = int(a)
                    ambval = 1 << (a-1)
                    aclval = 0
                    for u in UvpLst :
                        u = int(u)
                        aclval |= 1 << (u-1)
                    cmdstr = cmdfmt.format(EXECUTEABLEDIR, ambval, aclval)
                    cmdstrlst.append(cmdstr)
                    print("INFO: setupUVP cmdstrlst='{}' UvpLst='{}'".format(cmdstrlst, UvpLst))
        rtnval, lines, errors = self.executeCmd(cmdstrlst)
        if self.debug or rtnval > 0 :
            if rtnval > 0 :
                print("ERROR: rtnval indicates errors")
            print("INFO: setupUVP  lines={}".format(repr(lines)))
            print("INFO: setupUVP errors={}".format(repr(errors)))
        rtnstr, errstr = checkForSuccessOrErrors(lines, errors)

        if self.debug :
            print("setupUVP rtnstr={}".format(rtnstr))
            print("setupUVP errstr={}".format(errstr))
        return rtnstr, errstr

    def setUVPPhaseCalc(self, phase) :
        cmdlst = [
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w ARB_PHASE_SEL -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w ARBIT_PHASE_AMU1_CLNT1 -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w ARBIT_PHASE_AMU2_CLNT1 -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w ARBIT_PHASE_AMU3_CLNT1 -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w ARBIT_PHASE_AMU4_CLNT1 -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w PUCTRL -v {}",
                  "sudo {}uvp_util -m 0x{:X} -c 0x{:X} -w PUCTRL -v {}",
                  ]

        phaseValueDec = int((phase * 64.0 / 45.0) + 0.5)
        phaseValueDec = min(511, phaseValueDec)

        # the model dlist = [255,   0,   0,   0,   0, 1, 0]
        dlist = [phaseValueDec for _ in range(4)]
        dlist.insert(0, 255)
        dlist.append(1)
        dlist.append(0)

        rtnstr = ""
        errstr = ""
        cmdstrlst = list()
        for amb in self.AmbLst :
            ambval = 1 << (amb-1)
            aclval = 0
            for u in self.UvpLst :
                u = int(u)
                aclval |= 1 << (u-1)
            for i in range(len(cmdlst)) :
                cmdstr = cmdlst[i].format(EXECUTEABLEDIR, ambval, aclval, dlist[i])
                cmdstrlst.append(cmdstr)
        rtnval, lines, errors = self.executeCmd(cmdstrlst)
        if self.debug or rtnval > 0 :
            if rtnval > 0 :
                print("ERROR: setUVPPhaseCalc rtnval indicates errors")
            print("INFO: setUVPPhaseCalc  lines={}".format(repr(lines)))
            print("INFO: setUVPPhaseCalc errors={}".format(repr(errors)))
        rtnstr, errstr = checkForSuccessOrErrors(lines, errors)
        if self.debug :
            print("setUVPPhaseCalc amb {} rtnval {} rtnstr {} errstr {}".format(amb, repr(rtnval), rtnstr, errstr))
        return rtnstr, errstr


    def getUVPTemperature(self) :
        """ getUVPTemperature -- get the temperature from 1 or more uvps
                                 AMB 1,2,3,4, 0=all or a CSL 
                                 UVP 1 - 16, 0=all or a CSL
        """

        errstr = ""
        rtndict = dict()
        rtnval = 0
        for amb in self.AmbLst :
            try :
                amb = int(amb)-1
            except ValueError as ve :
                errstr = "ValueError amb {} -- {}".format(amb, repr(ve))
                rtval = None
                break
            else :
                ambmask = 1 << amb
                uvpmask = 0
                for uvp in self.UvpLst :
                    try :
                        uvp = int(uvp)-1
                    except ValueError as ve :
                        errstr += "ValueError uvp {} -- {}".format(uvp, repr(ve))
                        rtval = None
                        break
                    else :
                        uvpmask |= 1 << uvp

                # now make the call for this AMB
                cmd = "sudo {}uvp_util -r TEMPADC_OUT -m 0x{:X} -c 0x{:X} -d0".format(EXECUTEABLEDIR, ambmask, uvpmask)
                rtnval, lines, errors = self.executeCmd(cmd)
                if self.debug or rtnval > 0 :
                    if rtnval > 0 :
                        print("ERROR: rtnval indicates errors")
                    print("INFO: getUVPTemperatures -- rtnval={} lines={} errors={}".format(rtnval,repr(lines), repr(errors)))
                rtnstr, errstr = checkForSuccessOrErrors(lines, errors)
                if self.debug :
                    print("getUVPTemperatures -- rtnstr={} errstr={}".format(rtnstr, errstr))
                for i in range(len(self.UvpLst)) :
                    value = -99
                    num = -99
                    num2comp = -99
                    try :
                        num = int(lines[i])
                    except ValueError as ve :
                        print("getUVPTemperatures ValueError -- lines[{}]={} -- {}".format(i, repr(lines[i]), repr(ve)))
                        rtval = 1
                    except IndexError as ie :
                        print("getUVPTemperatures IndexError -- lines[{}]={} -- {}".format(i, repr(lines[i]), repr(ie)))
                        rtval = 1
                    except TypeError as te :
                        print("getUVPTemperatures TypeError -- lines[{}]={} -- {}".format(i, repr(lines[i]), repr(te)))
                        rtval = 1
                    except Exception as ee :
                        print("getUVPTemperatures Unexpected exception type  -- lines[{}]={} -- {}".format(i, repr(lines[i]), repr(te)))
                        rtval = 1
                    else :
                        num2comp = num if (0xF000 & num) == 0 else num - 8192
                        value = num2comp * 0.0155 + 43.55
                    index = (amb << 4) + int(self.UvpLst[i])-1
                    if self.debug :
                        print("amb={} uvp={} index={} value={}".format(amb, uvp, index, value))
                    rtndict.update({index : [value, num, num2comp]})
        return rtnval, rtndict, errstr


    def setup(self) :

        tmtime = time()
        if self.options.reboot :
            argv = ["reboot"]
            v = self.tx.sendGetTransmitter(argv)
            print("INFO: Waiting 65 secs for reboot to finish! -- {}".format(repr(v)))
            sleep(65)
            argv = ["reset"]
            v = self.tx.sendGetTransmitter(argv)
            print("INFO: Return Val from 'reset' {}\n".format(repr(v)))
            sleep(11)

        OSCLogDBTablename = 'oscSetup_{}'.format(int(self.logger.timeStampNum))
        SIGGENLogDBTablename = 'siggenSetup_{}'.format(int(self.logger.timeStampNum))

        self.logger.initFile(TX_CMS)

        if self.options.logtodb and self.options.dbname is not None :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('Hostname', self.txhostname)
        self.logger.logAddValue('CfgComment', self.options.logcomments)
        self.logger.logAddValue('PostTestNotes',  '')
        self.logger.logAddValue('OscSetupTable',  OSCLogDBTablename)
        self.logger.logAddValue('SiggenSetupTable',  SIGGENLogDBTablename)

        self.logger.logAddValue(AMUCOLNAME, self.amutablename)
        self.logger.logAddValue('LogFileName', self.logger.filename)

        # setup Thermocouple reader if included.

        if self.te is not None and self.te.instrumentOpen() :
            self.logger.logAddValue('DAcqHostName', self.tehostname)
            self.logger.logAddValue('DAcqIP',       self.teip)
            self.te.sendgetdata("*IDN?")
            vallst = self.te.lastData.split(',')
            dacqname = vallst[0]+'_'+vallst[1]+'_'+vallst[3]
            dacqsn   = vallst[2]
            self.logger.logAddValue('DAcqName', dacqname)
            self.logger.logAddValue('DAcqSN',   dacqsn)
            tm = datetime.datetime.now()
            self.te.senddata("system:time {},{},{:5.3f}".format(tm.hour, tm.minute, float(tm.second)))
            self.te.senddata("system:date {},{:02d},{:02d}".format(tm.year, tm.month, tm.day))

            self.te.senddata("configure:temp tc,j,1,0.1,(@{})".format(self.chanliststr))

            self.te.senddata("format:reading:time:type abs")
            self.te.senddata("format:reading:time on")

            self.te.senddata("format:reading:channel on")

            self.te.instrumentClose()

        powerLevel, _, _, _ = self.setPowerLevel(str(self.options.powerlevel))
        if float(powerLevel) > 0 :
            self.logger.logAddValue('PowerLevel', powerLevel)

        # Pause the transmitter
        argv = ['pause']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Pause.")

        # setup the tile for UVP temperature collection
        rtn = ""
        err = ""
        if self.options.loguvptemps :
            rtn,  err  = self.setupUVP(self.uvptempfmt)
        # turn on the transmiters
        rtn1, err1 = self.setupUVP(self.txcmdfmt, 0)
        rtn += rtn1
        err += err1
        self.logger.logAddValue('TileSetupRslts', str("VALUES="+rtn+"ERRORS"+err))

        # log the TX table data
        self.logger.logResults()
        if self.debug :
            print("\n###################################### OSC ################################################\n\n")

        # setup Oscilloscope
        oscloglst = list()
        for cmd in OSC_SETUP_LIST :
            self.osc.setOSCValue(cmd[:-1]) 
            if cmd[-1] is not None :
                name = cmd[0] if "Chan" not in cmd[0] else cmd[0]+str(cmd[1])
                oscloglst.append([name, '%s', cmd[-1]])
        self.logger.initFile(oscloglst, how='a')
        if self.options.logtodb and self.options.dbname is not None :
            self.logger.initDBTable(OSCLogDBTablename)
        for cmd in OSC_SETUP_LIST :
            if cmd[-1] is not None :
                name = cmd[0] if "Chan" not in cmd[0] else cmd[0]+str(cmd[1])
                self.logger.logAddValue(name, makeListToString(cmd[1:-1],debug=self.debug))
        self.logger.logResults()

        if self.debug :
            print("\n######################################## SIGGEN ##############################################\n\n")
        # setup Signal Generator
        siggenloglst = list()
        for cmd in SIGGEN_SETUP_LIST :
            self.siggen.setSIGGENValue(cmd[:-1])
            if cmd[-1] is not None :
                siggenloglst.append([cmd[0], '%s', cmd[-1]])
        self.logger.initFile(siggenloglst, how='a')
        if self.options.logtodb and self.options.dbname is not None :
            self.logger.initDBTable(SIGGENLogDBTablename)
        for cmd in SIGGEN_SETUP_LIST :
            if cmd[-1] is not None :
                args = cmd[1:-1]
                self.logger.logAddValue(cmd[0], makeListToString(cmd[1:-1],debug=self.debug))
        siggenlevel = DEFSIGGENLVL
        try :
            siggenlevel = float(self.options.__dict__.get('siggenlevel', DEFSIGGENLVL))
        except ValueError as ve :
            print("ERROR: SigGen level failer. -- {}".format(repr(ve)))
        else :
            self.siggen.powerLevel = [siggenlevel, "DBM"]
            self.logger.logAddValue('powerlevel', siggenlevel)
        self.logger.logResults()
        if self.debug :
            print("\n######################################## AMU CMS ##############################################\n\n")

        # make amu table column string (CMS) list
        amu_cms = list()
        amu_cmsts = list()

        amu_cms.append(["TimeStamp", '%d', 'VARCHAR(20)'])
        amu_cms.append(["TCStatus",  '%s', 'VARCHAR(20)'])

        if self.tempcham is not None :
            amu_cms.append(["TempChamSetPoint", '%3.1f', 'VARCHAR(20)'])
            amu_cms.append(["TempChamTemp", '%3.1f', 'VARCHAR(20)'])

        amu_cms.append(["SetPhase", '%d', 'VARCHAR(20)'])

        if self.osc is not None :
            for i in [2,3,4] :
                name  = "PhaseChan1to{}".format(i)
                namer = "PhaseChan1to{}Raw".format(i)
                amu_cms.append([name, '%6.3f', 'VARCHAR(20)'])
                amu_cms.append([namer,'%s', 'VARCHAR(20)'])

        #amu_cms.append(["systemTemp", "%d", 'VARCHAR(15)'])
        if self.te is not None :
            for ch in self.chanlist :
                amu_cms.append(['ChNum{}Temp{}'.format(ch, self.chanMap[ch]), '%6.3g','VARCHAR(20)'])
                if self.options.logdacqts :
                    amu_cmsts.append(['ChNum{}TS{}'.format(ch, self.chanMap[ch]), '%s','VARCHAR(30)'])
        print("INFO amu_cms = '{}'\n".format(amu_cms))

        if self.options.loguvptemps :
            for uvp in self.UvpLst :
                amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[0], uvp), '%s','VARCHAR(20)'])
                if self.options.tindex > 0 :
                    amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[1], uvp), '%s','VARCHAR(20)'])
                if self.options.tindex > 1 :
                    amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[2], uvp), '%s','VARCHAR(20)'])
            # now add on the Dacq channel time stamps
            amu_cms.extend(amu_cmsts)
            #for a in amu_cms :
                #print("amu_cms -- {} == {}".format(a[0], repr(a)))

        self.logger.initFile(amu_cms, how='a')
        if self.options.logtodb and self.options.dbname is not None :
            self.logger.initDBTable(self.amutablename)
        # run the transmitter
        argv = ['run']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not run.")

        if self.debug :
            print("Time to do setup {}".format(time()-tmtime))
            print("\n######################################## SETUP END ##############################################\n\n")
    # END def setup(self)

    def getDacqData(self) :
        rtndict = dict()
        if self.te is not None and self.te.instrumentOpen() :
            rtn  = self.te.sendgetdata("read?", True)
            rlst = rtn.split(",")
            # this list is completely dependent on how the unit was setup.
            for i in range(0, len(rlst), 8) :
                # this set of 8 values are
                #  -- temp, yr, mon, day, hrs, mins, secs, chan
                temp  = float(rlst[i+0])
                tsstr = str(rlst[i+4])+"_"+str(rlst[i+5])+"_"+str(rlst[i+6])
                chan  = int(rlst[i+7])
                rtndict.update({chan : [temp, tsstr]}) 
            self.te.instrumentClose()
        return rtndict

    def getSystemTemp(self) :
        # get system temperature datum
        systemTemp = 0
        argv = ['get_system_temp']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("System Temperature not valid")
        else :
            systemTemp = jcs.getSystemTemp(v)
        return systemTemp

    def mainTest(self) :

        tindex = self.options.tindex + 1
        # iterate through setcount 
        countOpt = int(self.options.setcount)
        optTemp = self.tempcham.currentTemperature
        setTempDir = 0.0
        testlistlen = len(self.testlist)
        phaseMeasureLoop =  zip(range(3), [2,3,4])
        if self.debug :
            print("Org testlist length = {}".format(testlistlen))
        for testlist in self.testlist :
            phaseListToUse = self.phaseList
            if self.debug :
                print("testlist = {} testlistlen = {}".format(repr(testlist), testlistlen))
            try :
                setPointTemp    = float(testlist.get('tempcham', optTemp))
                testTempPoint   =       testlist.get('testtemppoint', False)
                count           = float(testlist.get('logcount', countOpt))
                checkTollerance = float(testlist.get('checktollerance', 1.0))
                contTime        =   int(testlist.get('continuetime', -1))
                phaseToUse      =   int(testlist.get('phasetouse', -1))
                powerLVL        =   str(testlist.get('powerlevel', "-1"))
                ContDelay       = float(testlist.get('contdelay', 0.0))
                iterationDelay  = float(testlist.get('iterationdelay', 0.0))
                if phaseToUse >= 0 :
                    phaseListToUse  = list([phaseToUse])
            except Exception  as ee :
                print("Testlist input error -- ERROR '{}'".format(repr(ee)))
                print("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
                print("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
                return False
            else :
                self.setPowerLevel(powerLVL)
                self.tempcham.temperatureSetpoint = setPointTemp
                setTempDir    = setPointTemp - optTemp
                optTemp       = setPointTemp
                beginTime     = time()
                endTime       = beginTime + contTime

                print("INFO: testTempPoint={} contTime={} setpoint={} count={} phaseToUse={} checkTol={} beginTime={} endtime={} {}".format(
                        repr(testTempPoint), contTime, setPointTemp, count, phaseToUse, checkTollerance, beginTime, endTime, TSUtil.getTimeStrFromNumber(endTime)))

                if testTempPoint :
                    tt = self.tempcham.currentTemperature
                    TCStatusStr = "Changing_0"
                    msg = "INFO: Temperature set to {} -- Waiting for the current temp {} to reach {} or higher :".format(setPointTemp,tt, setPointTemp-checkTollerance)
                    print(printTime(msg, noprnt=True))
                else :
                    msg = "INFO: Temperature resting at Set point {} for ".format(setPointTemp)+"{} secs :"
                    print(printTime(msg, contTime, noprnt=True))
                    TCStatusStr = "Steady_1"

            cnt = 1 if count < 0 else count
            while True :

                for phase in phaseListToUse :
                    print("INFO: Using phase {} out of {}".format(phase, repr(phaseListToUse)))
                    tmtime = time()
                    self.setUVPPhaseCalc(phase)
                    if self.debug :
                        print("Time to do setUVPhase {}".format(time()-tmtime))
                    tcTemp = self.tempcham.currentTemperature
                    for ii in range(int(cnt)) :
                        timeStamp = time() - beginTime
                        tcTemp = self.tempcham.currentTemperature
                        self.logger.logAddValue("TimeStamp", timeStamp)
                        self.logger.logAddValue("TempChamSetPoint", setPointTemp)
                        self.logger.logAddValue("TempChamTemp", tcTemp)
                        self.logger.logAddValue("SetPhase", phase)
                        self.logger.logAddValue("TCStatus", TCStatusStr)

                # next get data from the Data Acquisition unit
                        if self.te is not None :
                            rtnval = self.getDacqData()
                            if rtnval is not None and len(rtnval) > 0 :
                                for ch in self.chanlist :
                                    tempName = 'ChNum{}Temp{}'.format(ch, self.chanMap[ch])
                                    temp     = rtnval[ch][0]
                                    self.logger.logAddValue(tempName, temp)
                                    if self.options.logdacqts :
                                        tsName   = 'ChNum{}TS{}'.format(ch, self.chanMap[ch])
                                        ts       = rtnval[ch][1]
                                        self.logger.logAddValue(tsName, ts)

                        tmtime = time()
                        if self.osc is not None :
                            self.osc.measurePhase = [1, [2,3,4],'RISING']
                            phaselst = self.osc.measurePhase
                            for j, i in phaseMeasureLoop :
                                name  = "PhaseChan1to{}".format(i)
                                namer = "PhaseChan1to{}Raw".format(i)
                                phaser = phaselst[j]
                                phasei = 0.0

                                if self.debug :
                                    print("measurePhase -- type={} phase={}".format(type(phaser), phaser))
                                try :
                                    phasei = float(phaser) % 360
                                except Exception  as ee :
                                    print("Exception when reading OSC phase -- {}".format(repr(ee)))
                                    phasei = 0.0
                                    phaser = "Exception"
                                print("Measure Phase -- Ch1to{} Phase raw {} Phase {}".format(i, phaser, phasei))
                                self.logger.logAddValue(name,  phasei)
                                self.logger.logAddValue(namer, phaser)
                            if self.debug :
                                print("Time to getOSCPhase {}".format(time()-tmtime))


                        # next get temp data from all the listed AMBs and uVPs
                        if self.options.loguvptemps :
                            tmtime = time()
                            rtnval, rtndict, errstr = self.getUVPTemperature()

                            if rtnval is not None and len(rtndict) > 0 :
                                for amb in self.AmbLst :
                                    for uvp in self.UvpLst :
                                        index = ((int(amb)-1) << 4) + int(uvp)-1
                                        tempVal = rtndict.get(index, [99.99,1,-1])
                                        # convert the temp number to a string for logging it may be a float or an int
                                        for indx in range(tindex) :
                                            if indx == 0 :
                                                temp = "{:5.2f}".format(float(tempVal[indx]))
                                            else :
                                                temp = "{:d}".format(int(tempVal[indx]))
                                
                                            tempName = 'amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15),self.ambuvpName[indx], uvp)
                                            self.logger.logAddValue(tempName, temp)
                            if self.debug :
                                print("Time to getUVPTemps {}".format(time()-tmtime))

                        tmtime = time()
                        self.logger.logResults()
                        if self.debug :
                            print("Time to logResults {}".format(time()-tmtime))
                        # end of count loop
                        if iterationDelay > 0 :
                            msg = "INFO: Iteration sleep -- iterationDelay '{}' :"
                            print(printTime(msg, iterationDelay, noprnt=True))
                            sleep(iterationDelay)
                # end of phase loop

                if ContDelay > 0 :
                    msg = "INFO: ContDelay sleep  -- ContDelay {} :"
                    print(printTime(msg, ContDelay, noprnt=True))
                    sleep(ContDelay)
                # check if it's exit time
                print("INFO: testlistlen={} testTempPoint={} tcTemp={} setpointTemp={} contTime={} endTime-time={}".format(testlistlen, repr(testTempPoint), tcTemp, setPointTemp, contTime, endTime-time()))

                if testTempPoint and (checkWithin(tcTemp, setPointTemp, checkTollerance) or (tcTemp > setPointTemp and setTempDir >= 0) or (tcTemp < setPointTemp and setTempDir < 0)) :
                    print("INFO: Temperature set point {} reached {} time={}".format(setPointTemp, tcTemp, printTime("", noprnt=True)))
                    break
                elif contTime > 0 and endTime - time() <= 0 :
                    print("INFO: Temperature resting at {} time reached{}".format(setPointTemp, printTime("", noprnt=True)))
                    break
                elif not testTempPoint and contTime < 0 :
                    break
            testlistlen = testlistlen - 1

        return True
                    

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("","--cfgfile",      dest='cfgfile',      type=str,  action='store',    default=None,  help="Use a config file (.py) for options")
    parser.add_option("","--cfgname",      dest='cfgname',      type=str,  action='store',    default=None,  help="Use a config file (.py) for options")
    parser.add_option("","--testlistfile", dest='testlistfile', type=str,  action='store',    default=None,  help="Use a list file (.py) for data collection positions")
    parser.add_option("","--testlistname", dest='testlistname', type=str,  action='store',    default=None,  help="Use the named list from the file (.py).")

    parser.add_option("","--dbname",      dest='dbname',     type=str,   action='store',      default=None,  help="Database name to log data to.")
    parser.add_option("","--tblname",     dest='tblname',    type=str,   action='store',      default=None,  help="Table name  to log data to.")
    parser.add_option("","--logcomments", dest='logcomments',type=str,   action='store',      default="",    help='Comments to describe important aspects of this test occurances')
 
    parser.add_option("","--powerlevel",  dest='powerlevel', type=str,   action='store',      default='-1',  help='Powerlevel of the transmiter')
    parser.add_option("","--tindex",      dest='tindex',     type=int,   action='store',      default=0,     help="Temperature=0, Raw UVP data=1, Two's Comp data=2, all else=Temperature")

    parser.add_option("","--setcount",    dest='setcount',   type=int,   action='store',      default=10,    help="Number of temperature sets to collect \
                                                                                                                  0         = setup but don't take data.\
                                                                                                                  Greater   = gather count data.\
                                                                                                                  Less than = gather data forever, Cntl-C to stop")
    parser.add_option("-r","--reboot",    dest='reboot',                 action='store_true', default=False, help="Reboot the Transmitter at the start")


    parser.add_option("","--chanlist",    dest='chanlist',   type=str,   action='store',      default=None,  help="List of DAcq Channels to acquire. e.g. 101:120")
    parser.add_option("","--amblist",     dest='amblist',    type=str,   action='store',      default=None,  help="List of AMBs to iterate over. e.g. 1:4 or 1,2")
    parser.add_option("","--uvplist",     dest='uvplist',    type=str,   action='store',      default=None,  help="List of UVPs to iterate over. e.g. 1:64 or 3,5,14")
    parser.add_option("","--txonuvplst",  dest='txonuvplst', type=str,   action='store',      default=None,  help="List of UVPs to iterate over. e.g. 1:64 or 3,5,14")
    parser.add_option("","--loguvptemps", dest='loguvptemps',            action='store_true', default=False, help="Log UVP temperature if set.")

    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store',      default=None,  help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--console",     dest='console',                action='store_true', default=False, help="Turn on printing to console")
    parser.add_option("","--logtodb",     dest='logtodb',                action='store_false',default=True,  help="Log data to the DataBase at ossia-build")
    parser.add_option("","--logdacqts",   dest='logdacqts',              action='store_true', default=False, help="Log the Dacq data timestamps")
    parser.add_option("-d","--debug",     dest='debug',                  action="store_true", default=False, help="print debug info.")

    parser.add_option("","--directory",   dest='directory',  type=str,   action='store',      default="/home/ossiadev/Utils/VAMBTempUtil/Logs/",help="Log file directory.")

    (options, args) = parser.parse_args()

    if options.cfgfile is not None :
        try :
            options = parseConfigFile(parser, options)
        except RuntimeError :
            exit(1)
        except Exception as e :
            print("ERROR: Unknown exception from parseConfgFile -- {}\n".format(repr(e)))
            exit(2)
    testlistE = None
    if options.testlistfile is not None :
        import imp
        try :
            tstlstmod = imp.load_source(options.testlistname, options.testlistfile)
            testlistE = eval("tstlstmod."+options.testlistname)
        except Exception as ee :
            print("ERROR: tstlistmod creation did not succeed on testlist '{}' and filename '{}' -- {}".format(options.testlistname, options.testlistfile, repr(ee)))
        else :
            if options.debug :
                print("options = {}".format(repr(options).replace("<","").replace(">","").replace(',',',\n')))
                print("testlist = {}".format(repr(testlistE).replace('},','},\n')))

    # got all the options, run the test
    try :
        tA = AMUTempTest(options, testlistE)
        tA.setup()
        tindex = int(options.tindex)
        tindex = 0 if tindex > 2 else tindex
        options.tindex = tindex
        tA.mainTest()
    except KeyboardInterrupt :
        pass
    except RuntimeError as rt :
        print("ERROR: {}".format(repr(rt)))
    except Exception as ee :
        print("ERROR: An unexpected exception occured! Was an IP address or FQDN of the target Charger provided?\n")
        print("ERROR:                                  Was a valid port number for the target Charger provided?\n")
        print("ERROR: Exception -- {}".format(repr(ee)))
        print("ERROR: Traceback -- {} \n".format(traceback.format_exc()))
        print("ERROR: Sys Exception info -- {}\n".format(repr(sys.exc_info())))
    else :
        argv = ['reset']
        v = tA.tx.sendGetTransmitter(argv)
        if options.debug :
            print("Return Val from END 'reset' {}\n".format(repr(v)))
 
        hrs, mins, secs, Duration = TSUtil.DurationCalc(START_TIME)
        fd = None
        try :
            fd = open(tA.logger.filename, 'a')
        except IOError as io :
            print("ERROR: Could not open file {} -- {}".format(tA.logger.filename, io.message))
        except Exception as ee :
            print("ERROR: Logger not available -- {}".format(repr(ee)))
        else :
            fd.write("Duration, {}\n".format(Duration))
            fd.close()
        if options.logtodb :
            try :
                tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                print("Logging the Duration failed {}\n".format(repr(e)))

    hrs, mins, secs, Duration = TSUtil.DurationCalc(START_TIME)
    print("INFO: Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))

