''' VAMBTempUtil.py -- Setup test equipment, Transmitter, and log temperture data as
defined by commandline options.
Changed to make use of the Tenny Temperature Chamber

Version : 0.0.1
Date : May 9 2019
Copyright Ossia Inc. 2019

'''
from __future__ import absolute_import, division, print_function, unicode_literals

import sys
#if "/home/ursusm/pylib" not in sys.path :
#    sys.path.insert(0, "/home/ursusm/pylib")
if "/home/ossiadev/pylib" not in sys.path :
    sys.path.insert(0, "/home/ossiadev/pylib")

from time import sleep, time
from optparse import OptionParser
from socket import gethostbyname
import json, datetime

import SA.SACom as sacom
import TXJSON.JSonCmdStrings as jcs
import LIB.timestamp as TSUTil
from LIB.TXComm import TXComm, gPWR_LEVEL_MAP
from LIB.logResults import LogResults
from LIB.sendTestServer import SendTestServer

#DEFAULTDBSERVER = 'ossiadevuwm'
DEFAULTDBSERVER = 'ossia-build'

TXTBLNAME = 'TxAmuTemp'
AMUCOLNAME = 'AmuTempTable'
TIMESTAMPCOLNAME="DataTimeStamp"


TX_CMS  = [[TIMESTAMPCOLNAME, '%s', 'VARCHAR(35)'],
           ['CfgComment',     '%s', 'VARCHAR(512)'],
           ['PostTestNotes',  '%s', 'VARCHAR(512)'],
           ["Duration",       '%s', 'VARCHAR(20)'],
           [AMUCOLNAME,       '%s', 'VARCHAR(50)'],
           ['LogFileName',    '%s', 'VARCHAR(150)'],
           ['PowerLevel',     '%d', 'VARCHAR(5)'],
           ['Hostname',       '%s', 'VARCHAR(106)'],
           ['IPAddress',      '%s', 'VARCHAR(16)'],
           ['DAcqName',       '%s', 'VARCHAR(100)'],
           ['DAcqSN',         '%s', 'VARCHAR(20)'],
           ['DAcqHostName',   '%s', 'VARCHAR(100)'],
           ['DAcqIP',         '%s', 'VARCHAR(16)'],
           ['TileSetupRslts', '%s', 'VARCHAR(4092)'],
           ]

class AMUTempTest(object):

    def __init__(self, options) :

        self.chanMap = { 101 : [ "A5", 57], 102 : [ "A6", 55], 103 : ["A17", 50], 104 : ["A18", 49], 105 : ["A11", 52],
                         106 : ["A10", 02], 107 : [ "A8",  0], 108 : [ "A9",  5], 109 : ["A13", 18], 110 : ["A14", 23],
                         111 : ["A19", 40], 112 : ["A20", 37], 113 : ["A16", 34], 114 : ["A15", 32], 115 : [ "P7", 23],
                         116 : ["P12", 16], 117 : [ "C4", 64], 118 : [ "C2", 64], 119 : [ "C1", 64], 120 : [ "C3", 64]}

        self.ambuvpName = ['Temp', 'Raw', 'TComp']

        self.options = options
        
        self.txhostname = self.options.txipaddr
        self.txip       = gethostbyname(self.txhostname)
        self.txport     = self.options.txport
        self.srvport    = self.options.srvport
        
        chanlist        = self.options.chanlist.split(":")
        self.chanStart  = int(chanlist[0])
        self.chanEnd    = int(chanlist[1])+1

        self.tehostname = self.options.teipaddr
        self.teip       = gethostbyname(self.tehostname)
        self.teport     = self.options.teport
        self.debug      = self.options.debug
        self.cotaConfig = None

        self.te = None
        self.tx=TXComm(self.options.txipaddr, int(self.options.txport), self.options.debug)
        if self.options.teipaddr != "" :
            self.te  = sacom.SACom(addr=self.options.teipaddr, port=int(self.options.teport), debug=self.options.debug)
            self.te.mySockTimeout = 0.45
        self.sts = SendTestServer(self.txip, self.srvport, self.debug)

        logFile = "{}_{}".format(self.options.logfile, self.txhostname)+"_{}.csv"

        self.logger = LogResults(logFile, self.options.directory, console=self.options.console, debug=self.options.debug)
        self.amutablename = "amu_temp_{}".format(int(self.logger.timeStampNum))

    def setup(self) :

        if self.options.reboot :
            argv = ["reboot"]
            v = self.tx.sendGetTransmitter(argv)
            print("Waiting 65 secs for reboot to finish! -- {}".format(repr(v)))
            sleep(65)
            argv = ["reset"]
            v = self.tx.sendGetTransmitter(argv)
            print("Return Val from 'reset' {}\n".format(repr(v)))
            sleep(11)

        self.logger.initFile(TX_CMS)

        if options.logtodb :
            self.logger.initDB(self.options.dbname, hostnm=DEFAULTDBSERVER)
            self.logger.initDBTable(self.options.tblname)

        self.logger.logAddValue(TIMESTAMPCOLNAME, self.logger.timeStamp)
        self.logger.logAddValue('Hostname', self.txhostname)
        self.logger.logAddValue('IPAddress', self.txip)
        self.logger.logAddValue('CfgComment', self.options.logcomments)
        self.logger.logAddValue('PostTestNotes',  '')

        self.logger.logAddValue(AMUCOLNAME, self.amutablename)
        self.logger.logAddValue('LogFileName', self.logger.filename)

        if self.te is not None and self.te.instrumentOpen() :
            self.logger.logAddValue('DAcqHostName', self.tehostname)
            self.logger.logAddValue('DAcqIP',       self.teip)
            self.te.sendgetdata("*IDN?")
            vallst = self.te.lastData.split(',')
            dacqname = vallst[0]+'_'+vallst[1]+'_'+vallst[3]
            dacqsn   = vallst[2]
            self.logger.logAddValue('DAcqName', dacqname)
            self.logger.logAddValue('DAcqSN',   dacqsn)
            tm = datetime.datetime.now()
            self.te.sendgetdata("system:time {},{},{:5.3f}".format(tm.hour, tm.minute, float(tm.second)))
            self.te.sendgetdata("system:date {},{:02d},{:02d}".format(tm.year, tm.month, tm.day))

            self.te.sendgetdata("configure:temp tc,j,1,0.1,(@{})".format(self.options.chanlist))

            self.te.sendgetdata("format:reading:time:type abs")
            self.te.sendgetdata("format:reading:time on")

            self.te.sendgetdata("format:reading:channel on")

            self.te.instrumentClose()

        powerLevel = self.options.powerlevel 
        try :
            powerLevel = int(powerLevel)
        except ValueError as e :
            print("ValueError converting powerlevel {}".format(repr(e)))
            powerLevel = 13
        else :
            powerLevel = max(powerLevel, 13)
            powerLevel = min(powerLevel, 21)
            powerLevel = gPWR_LEVEL_MAP.get(powerLevel, 13)
        finally :
            argv = ['set_power_level', str(powerLevel)]
            v = self.tx.sendGetTransmitter(argv)
            if jcs.checkForSuccess(v) :
                print("Set Power Level failed")
            argv = ['get_power_level']
            v = self.tx.sendGetTransmitter(argv)
            pl =  jcs.getTransmitterPowerLevel(v)
            if pl != powerLevel :
                print("Power level incorrect get={} vs set={}".format(pl, powerLevel))
                powerLevel = pl
            self.logger.logAddValue('PowerLevel', powerLevel)

        # Pause the transmitter
        argv = ['pause']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not Pause.")

        # setup the tile for UVP temperature collection
        rtn = self.sts.sendCommand(['setupForUVPTemp'])
        #self.logger.logAddValue('TileSetupRslts', rtn)

        # log the TX table data
        self.logger.logResults()

        # make amu table column string (CMS) list
        amu_cms = list()
        amu_cmsts = list()
        amu_cms.append(["systemTemp", "%d", 'VARCHAR(15)'])
        if self.te is not None :
            for ch in range(self.chanStart, self.chanEnd, 1) :
                amu_cms.append(['ChNum{}Temp{:02d}'.format(ch, self.chanMap[ch][1]), '%6.3g','VARCHAR(20)'])
                if self.options.logdacqts :
                    amu_cmsts.append(['ChNum{}TS{:02d}'.format(ch, self.chanMap[ch][1]), '%s','VARCHAR(30)'])

        for uvp in range(64) :
            amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[0], uvp), '%s','VARCHAR(20)'])
            if self.options.tindex > 0 :
                amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[1], uvp), '%s','VARCHAR(20)'])
            if self.options.tindex > 1 :
                amu_cms.append(['amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15), self.ambuvpName[2], uvp), '%s','VARCHAR(20)'])
        # now add on the Dacq channel time stamps
        amu_cms.extend(amu_cmsts)

        self.logger.initFile(amu_cms, how='a')
        if self.options.logtodb :
            self.logger.initDBTable(self.amutablename)
        # run the transmitter
        argv = ['run']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("Transmitter did not run.")


    def getUVPTemperatures(self) :
        """ Access the gumstixServer to acquire the temperatures from the UVPs
        """

        jTemperature = self.sts.sendCommand(['getUVPTemperature', '0', '0'])
        return json.loads(jTemperature)

    def getDacqData(self) :
        rtndict = dict()
        if self.te is not None and self.te.instrumentOpen() :
            rtn  = self.te.sendgetdata("read?", True)
            rlst = rtn.split(",")
            # this list is completely dependent on how the unit was setup.
            for i in range(0, len(rlst), 8) :
                # this set of 8 values are
                #  -- temp, yr, mon, day, hrs, mins, secs, chan
                temp  = float(rlst[i+0])
                tsstr = str(rlst[i+4])+"_"+str(rlst[i+5])+"_"+str(rlst[i+6])
                chan  = int(rlst[i+7])
                rtndict.update({chan : [temp, tsstr]}) 
            self.te.instrumentClose()
        return rtndict

    def getSystemTemp(self) :
        # get system temperature datum
        systemTemp = 0
        argv = ['get_system_temp']
        v = self.tx.sendGetTransmitter(argv)
        if jcs.checkForSuccess(v) :
            print("System Temperature not valid")
        else :
            systemTemp = jcs.getSystemTemp(v)
        return systemTemp

    def mainTest(self) :

        setdelay = float(self.options.setdelay)
        tindex = self.options.tindex + 1
        # iterate through setcount 
        cnt = int(self.options.setcount)
        while cnt > 0 or cnt < 0 :

            # first get System Temperature from the CCB
            systemTemp = self.getSystemTemp()
            self.logger.logAddValue("systemTemp", systemTemp)

            # next get data from the Data Acquisition unit
            rtnval = self.getDacqData()

            # iterate through the channel data from the Dacq unit
            if rtnval is not None and len(rtnval) > 0 :
                for ch in range(self.chanStart, self.chanEnd, 1) :
                    tempName = 'ChNum{}Temp{:02d}'.format(ch, self.chanMap[ch][1])
                    temp     = rtnval[ch][0]
                    self.logger.logAddValue(tempName, temp)
                    if self.options.logdacqts :
                        tsName   = 'ChNum{}TS{:02d}'.format(ch, self.chanMap[ch][1])
                        ts       = rtnval[ch][1]
                        self.logger.logAddValue(tsName, ts)

            # next get data from the Data from all 64 uVPs
            rtnval = self.getUVPTemperatures()
            if rtnval is not None and len(rtnval) > 0 :
                for uvp in range(64) :
                    tempVal = rtnval.get(str(uvp),[99.99,1,-1])
                    # convert the temp number to a string for logging it may be a float or an int
                    for indx in range(tindex) :
                        if indx == 0 :
                            temp = "{:5.2f}".format(float(tempVal[indx]))
                        else :
                            temp = "{:d}".format(int(tempVal[indx]))
                    
                        tempName = 'amb{}uvp{}{}{:02d}'.format((uvp>>4)&3, (uvp&15),self.ambuvpName[indx], uvp)
                        self.logger.logAddValue(tempName, temp)

            self.logger.logResults()
            sleep(setdelay)
            if cnt >= 0 and (cnt % 10) == 0 :
                print("Count down is at {}".format(cnt))
            if cnt > 0 :
                cnt -= 1
                    

if __name__ == '__main__' :

    START_TIME = time()
    parser = OptionParser()

    parser.add_option("","--teip",        dest='teipaddr',   type=str,   action='store',      default=None, help="Signal Analyzer network name or address")
    parser.add_option("","--teport",      dest='teport',     type=int,   action='store',      default=5024, help="Signal Analyzer telnet port number")
    parser.add_option("","--txip",        dest='txipaddr',   type=str,   action='store',      default=None, help="Transmitter network name or address.")
    parser.add_option("","--txport",      dest='txport',     type=int,   action='store',      default=50000,help="Transmitter telnet port number")
    parser.add_option("","--srvport",     dest='srvport',    type=int,   action='store',      default=50081,help="Transmitter Test Server port number")

    parser.add_option("","--tempchamip",  dest='tempchamip',  type=str,   action='store',     default=None, help="Temperature Chamber Com IP or host name")
    parser.add_option("","--tcport",      dest='tcport',      type=int,   action='store',     default=4001, help="Temperature Chamber Com port number")

    parser.add_option("","--dbname",      dest='dbname',     type=str,   action='store',      default=None, help="Database name to log data to.")
    parser.add_option("","--tblname",     dest='tblname',    type=str,   action='store',      default=None, help="Table name  to log data to.")
    parser.add_option("","--logcomments", dest='logcomments',type=str,   action='store',      default="",   help='Comments to describe important aspects of this test occurances')
 
    parser.add_option("","--powerlevel",  dest='powerlevel', type=int,   action='store',      default=20,   help='Powerlevel of the transmiter')
    parser.add_option("","--tindex",      dest='tindex',     type=int,   action='store',      default=0,    help="Temperature=0, Raw UVP data=1, Two's Comp data=2, all else=Temperature")

    parser.add_option("","--setcount",    dest='setcount',   type=int,   action='store',      default=10,    help="Number of temperature sets to collect \
                                                                                                                  0         = setup but don't take data.\
                                                                                                                  Greater   = gather count data.\
                                                                                                                  Less than = gather data forever, Cntl-C to stop")
    parser.add_option("","--setdelay",    dest='setdelay',   type=float, action='store',      default=1.0,   help='Time to wait between temperature set collections (seconds)')
    parser.add_option("-r","--reboot",    dest='reboot',                 action='store_true', default=False, help="Reboot the Transmitter at the start")


    parser.add_option("","--chanlist",    dest='chanlist',   type=str,   action='store',      default=None,  help="List of DAcq Channels to acquire. e.g. 101:120")

    parser.add_option("","--logfilebase", dest='logfile',    type=str,   action='store',      default=None,  help="Log file name. If {} is added in the file name, a timestamp will be added.")
    parser.add_option("","--console",     dest='console',                action='store_true', default=False, help="Turn on printing to console")
    parser.add_option("","--logtodb",     dest='logtodb',                action='store_false',default=True,  help="Log data to the DataBase at ossia-build")
    parser.add_option("","--logdacqts",   dest='logdacqts',              action='store_true', default=False, help="Log the Dacq data timestamps")
    parser.add_option("-d","--debug",     dest='debug',                  action="store_true", default=False, help="print debug info.")

    parser.add_option("","--directory",   dest='directory',  type=str,   action='store',      default="/home/ossiadev/Utils/VAMBTempUtil/Logs/",help="Log file directory.")

    (options, args) = parser.parse_args()

    tA = None
    if options.txipaddr is not "" :
        tA = AMUTempTest(options)
        tA.setup()
        try :
            tindex = int(options.tindex)
            tindex = 0 if tindex > 2 else tindex
            options.tindex = tindex
            tA.mainTest()
        except KeyboardInterrupt :
            pass

        finally :
            argv = ['reset']
            v = tA.tx.sendGetTransmitter(argv)
            print("Return Val from END 'reset' {}\n".format(repr(v)))

            hrs, mins, secs, Duration = TSUtil.Duration(START_TIME)
            try :
                tA.logger.logUpdateTableColumn(options.dbname, options.tblname, "Duration", Duration)
            except Exception as e : 
                print("Logging the Duration failed {}\n".format(repr(e)))

            print("Test duration = {} hrs {} mins {} secs\n".format(int(hrs), int(mins), int(secs)))
    else :
        print("Please provide the IP address or FQDN of the target Charger (-I).\n")
