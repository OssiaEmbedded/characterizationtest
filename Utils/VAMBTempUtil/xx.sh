#!/bin/bash

if [ $# -ne 1 ]; then
   echo "Usage: $0 value(2'S).\n"
   exit
fi

value=$((~4095))

if [ $1 -gt "4095" ]; then
    value=$(($value | $1))
else
    value=$1
fi
 
temp=$(echo "(.0155 * $value) + 43.55" | bc -l)
#temp=$(echo "(.018 * $value) + 41.63" | bc -l)
printf "%X\n" $value
printf "%d\n" $value
printf "%.2f\n" $temp

exit

