sudo amb_off.sh

#the line below turns on, calibrates and locks the uVPs on a V3 Tile
sudo cal_test -m $1 -c $2  -t -i -l -v3

#Required settings for reading temperature
#sudo uvp_util -w TEMPADCINTF_RST -m $1 -c $2 -v 1 > null
sudo uvp_util -w  LDOADC1EN_IDLE -m $1 -c $2 -v 1 > null
sudo uvp_util -w  TEMPADC_GAIN -m $1 -c $2 -v 32 > null 
sudo uvp_util -w  TMPSNSCMBUFFEN -m $1 -c $2 -v 1 > null 
sudo uvp_util -w  TMPSNSCOMPENCNT -m $1 -c $2 -v 4 > null
sudo uvp_util -w  TMPSNSCOMPSELEN -m $1 -c $2 -v 1 > null
sudo uvp_util -w  TMPSNSEN -m $1 -c $2 -v 1 > null
sudo uvp_util -w  TEMPADCEN -m $1 -c $2 -v 1 > null
sudo uvp_util -w  LDOADC1EN_SENDPWR -m $1 -c $2 -v 1 > null

#turns on TX on all uVPs
sudo fpga -w 0x214 -m $1 -c $2 -v 0

#loops through all uVPs to take temp reading from all uVPs
while true
do
     rm temperature.txt
     for uvp in 0x1 0x2 0x4 0x8 0x10 0x20 0x40 0x80 0x100 0x200 0x400 0x800 0x1000 0x2000 0x4000 0x8000
     do
          temp=`sudo uvp_util -r TEMPADC_OUT -m 0x1 -c $uvp -d0`  
          echo $temp
          sh convert.sh $temp  >> temperature.txt
     done
     scp temperature.txt luisp@10.10.0.88:/Users/luisp/Desktop
done

echo "done"
