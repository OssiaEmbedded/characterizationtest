from __future__ import absolute_import, division, print_function, unicode_literals

import json
from optparse import (OptionParser,BadOptionError,AmbiguousOptionError)
from subprocess import check_output

def getCotaConfigValues(ipaddr) :
    cotaConfNames = ["Calibration AMB",     "Calibration ACL",    "Reference AMB",            "Reference ACL",
                     "Client Query Period", "Client COM Channel", "Critical Temperature (C)", "Power Level",
                     "TPS Number of BB",    "TPS TX Duration"]
    strval = "/home/ossiadev/bin/mkKeyAndCopy.sh {}".format(ipaddr)
    lstval = ["/home/ossiadev/Utils/SSHToTileUtil/mkKeyAndCopy.sh", ipaddr]
    print("{}\n".format(repr(strval)))
    val = check_output(lstval)
    print("mkKeyAndCopy output : {}\n".format(repr(val)))
    pyDict = json.loads(val)
    print("{}\n".format(repr(pyDict)))
    tmpdict = dict() 
    rtnstr = ""
    for nDict in pyDict :
        tmpdict.update({nDict['Name'] : nDict['Value']})
    for nm in cotaConfNames :
        rtnstr += "{}={}\n".format(nm, tmpdict[nm])
    return rtnstr

class PassThroughOptionParser(OptionParser):
    """
        An unknown option pass-through implementation of OptionParser.

        When unknown arguments are encountered, bundle with largs and try again,
        until rargs is depleted.  

        sys.exit(status) will still be called if a known argument is passed
        incorrectly (e.g. missing arguments or bad argument types, etc.)        
    """
    def _process_args(self, largs, rargs, values):
        while rargs:
            try:
                OptionParser._process_args(self,largs,rargs,values)
            except (BadOptionError,AmbiguousOptionError), e:
                largs.append(e.opt_str)

if __name__ == "__main__" :
    
    parser = PassThroughOptionParser()
    parser.add_option("", "--txip", dest='txipaddr', type=str, action='store', default="10.10.0.226", help="Transmitter network address.")
    (options, args) = parser.parse_args()

#    cota_config = "ND"
#    try :
    cota_config = getCotaConfigValues(options.txipaddr)
#    except Exception as ex :
#        print("getCotaConfigValues exception -- {}".format(repr(ex)))

    print("return value = {}\n".format(repr(cota_config)))

