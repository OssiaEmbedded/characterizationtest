#!/bin/bash

# $1 is the IP address to a gumstix tile



fname="/home/ossiadev/sshkeys/key_id_rsa"
fnamepub="/home/ossiadev/sshkeys/key_id_rsa.pub"
rfnamepub="/home/gumstix/.ssh/key_id_rsa.pub"
afname="/home/gumstix/.ssh/authorized_keys"

ss=eval `ssh-agent`

if [ -e $fname ]
then
    #echo "[{\"Key Exists\":\"$ss\"}]"
    sshadd=`ssh-add $fname 2>&1`
    #sshkeycopy=`ssh-copy-id -i $fnamepub -p 2222 -o batchmode=yes gumstix@$1 2>&1 `
    #sshrcp=`rcp -P 2222 $fnamepub gumstix@$1:/home/gumstix/.ssh`
    #sshssh=`ssh  gumstix@$1 -p 2222 cat $rfnamepub >> $afname`
    #echo $sshadd
    #echo $sshrcp
    #echo $sshssh
else
    sshkeygen=`ssh-keygen -t rsa -N "" -f $fname  2>&1`
    sshadd=`ssh-add $fname 2>&1`
    #sshkeycopy=`ssh-copy-id -i $fnamepub -p 2222 -o batchmode=yes gumstix@$1 2>&1 `
    sshrcp=`rcp -P 2222 $fnamepub gumstix@$1:/home/gumstix/.ssh`
    sshssh=`ssh  gumstix@$1 -p 2222 cat $rfnamepub >> $afname`
    #echo $sshkeygen
    #echo $sshadd
    #echo $sshrcp
    #echo $sshssh
    #echo $sshkeycopy
fi

sshcat=`ssh gumstix@$1 -p 2222 cat /etc/cota/Cota_Config.json`
echo $sshcat

